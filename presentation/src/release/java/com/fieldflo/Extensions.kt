package com.fieldflo

import com.fieldflo.common.timberTrees.ReleaseLogTree
import com.orhanobut.hawk.Hawk
import timber.log.Timber

fun App.init() {
    Hawk.init(this).build()
    ShipBook.start(this, "5d264dd8119e210f77b3f98f", "03a536048aaa1ffc8b37c0d5fc234943", {
        FirebaseCrashlytics.getInstance().setCustomKey("shipbookSession", it)
    })
    ShipBook.addWrapperClass(Timber::class.java.name)
    Timber.plant(ReleaseLogTree())
}