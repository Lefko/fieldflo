package com.fieldflo.di

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.chuckerteam.chucker.api.RetentionManager
import com.fieldflo.BuildConfig
import com.fieldflo.common.networkConnection.IConnectionData
import com.fieldflo.common.networkConnection.NetworkConnection
import com.fieldflo.data.api.endpoints.*
import com.fieldflo.data.api.interceptors.*
import com.fieldflo.data.api.serialization.*
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Lazy
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
object NetworkModule {

    @Singleton
    @Provides
    fun provideConnectionData(context: Context): IConnectionData = NetworkConnection(context)

    @Singleton
    @Provides
    fun provideOkHttpClient(interceptors: ArrayList<Interceptor>): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()
            .callTimeout(90, TimeUnit.SECONDS)
            .readTimeout(90, TimeUnit.SECONDS)
            .connectTimeout(90, TimeUnit.SECONDS)
            .writeTimeout(90, TimeUnit.SECONDS)
        if (!interceptors.isEmpty()) {
            interceptors.forEach { interceptor ->
                clientBuilder.addInterceptor(interceptor)
            }
        }
        return clientBuilder.build()
    }

    @Provides
    fun provideInterceptors(
        baseUrlInterceptor: BaseUrlInterceptor,
        headersInterceptor: HeadersInterceptor,
        userTokenRefreshInterceptor: UserTokenRefreshInterceptor,
        httpLoggingInterceptor: HttpLoggingInterceptor,
        fieldFloLogger: LoggingInterceptor,
        chuckerInterceptor: ChuckerInterceptor
    ): ArrayList<Interceptor> {
        return arrayListOf(
            baseUrlInterceptor,
            headersInterceptor,
            userTokenRefreshInterceptor,
            httpLoggingInterceptor,
            fieldFloLogger,
            chuckerInterceptor
        )
    }

    @Provides
    @Singleton
    fun provideUserTokenRefreshInterceptor(
        localStorage: ILocalStorage,
        tokenRefresher: ITokenRefresher
    ): UserTokenRefreshInterceptor {
        return UserTokenRefreshInterceptor(localStorage, tokenRefresher)
    }

    @Provides
    @Singleton
    fun provideTokenRefresher(authApi: Lazy<AuthApi>): ITokenRefresher {
        return TokenRefresher(authApi)
    }

    @Provides
    @Singleton
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor { message -> doLog(message) }.apply {
            when {
                BuildConfig.DEBUG -> HttpLoggingInterceptor.Level.BODY
                BuildConfig.BUILD_TYPE == "beta" -> HttpLoggingInterceptor.Level.BODY
                else -> HttpLoggingInterceptor.Level.BASIC
            }
            redactHeader(USER_TOKEN_HEADER_KEY)
            redactHeader(API_TOKEN_HEADER_KEY)
            redactHeader(SECURITY_KEY_HEADER_KEY)
        }
    }

    private fun doLog(message: String) {
        Timber.i(message)
    }

    @Provides
    @Singleton
    fun provideChuckerInterceptor(
        context: Context,
        collector: ChuckerCollector
    ): ChuckerInterceptor {
        return ChuckerInterceptor(
            context = context,
            collector = collector,
            headersToRedact = mutableSetOf(
                USER_TOKEN_HEADER_KEY,
                API_TOKEN_HEADER_KEY,
                SECURITY_KEY_HEADER_KEY
            )
        )
    }

    @Provides
    @Singleton
    fun provideChuckerCollector(context: Context): ChuckerCollector {
        return ChuckerCollector(
            context = context,
            showNotification = true,
            retentionPeriod = RetentionManager.Period.ONE_HOUR
        )
    }

    @Singleton
    @Provides
    fun provideMoshi(
        dateAdapter: DateAdapter,
        emptyStringToDouble: EmptyStringToDoubleTypeAdapter,
        emptyStringToFloat: EmptyStringToFloatTypeAdapter,
        emptyStringToInt: EmptyStringToIntTypeAdapter,
        emptyStringToLong: EmptyStringToLongTypeAdapter,
        emptyStringToString: EmptyStringToStringTypeAdapter,
        modificationAdapter: ModificationsDataTypeAdapter
    ): Moshi {
        return Moshi.Builder()
            .add(dateAdapter)
            .add(emptyStringToDouble)
            .add(emptyStringToFloat)
            .add(emptyStringToInt)
            .add(emptyStringToLong)
            .add(emptyStringToString)
            .add(modificationAdapter)
            .add(KotlinJsonAdapterFactory())
            .build()
    }

    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient, moshi: Moshi): Retrofit =
        Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient())
            .baseUrl("http://google.com")
            .build()

    @Singleton
    @Provides
    fun providesAppSyncApi(retrofit: Retrofit): AppSyncApi = retrofit.create(AppSyncApi::class.java)

    @Singleton
    @Provides
    fun provideAuthApi(retrofit: Retrofit): AuthApi = retrofit.create(AuthApi::class.java)

    @Singleton
    @Provides
    fun provideCertsApi(retrofit: Retrofit): CertsApi = retrofit.create(CertsApi::class.java)

    @Singleton
    @Provides
    fun provideCompanyDataApi(retrofit: Retrofit): CompanyDataApi =
        retrofit.create(CompanyDataApi::class.java)

    @Singleton
    @Provides
    fun provideImageUploadApi(retrofit: Retrofit): ImageUploadApi =
        retrofit.create(ImageUploadApi::class.java)


    @Singleton
    @Provides
    fun provideManageFormsApi(retrofit: Retrofit): ManageFormsApi =
        retrofit.create(ManageFormsApi::class.java)
}