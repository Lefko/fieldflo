package com.fieldflo.di

import android.content.Context
import com.fieldflo.AppScopeHolder
import com.fieldflo.common.networkConnection.IConnectionData
import com.fieldflo.data.sync.FieldFloWorkerFactory
import com.fieldflo.screens.addCompany.AddCompanyPresenter
import com.fieldflo.screens.displayPicture.DisplayPicturePresenter
import com.fieldflo.screens.drawer.DrawerPresenter
import com.fieldflo.screens.employeesCertificate.printedProjectCerts.PrintedCertificatePresenter
import com.fieldflo.screens.employeesCertificate.projectCertsList.EmployeesCertificatePresenter
import com.fieldflo.screens.formDailyDemoLog.DailyDemoLogFormViewModel
import com.fieldflo.screens.formDailyDemoLog.employees.DailyDemoLogEmployeesViewModel
import com.fieldflo.screens.formDailyDemoLog.employees.addEmployees.AddEmployeesPresenter
import com.fieldflo.screens.formDailyDemoLog.equipment.DailyDemoLogEquipmentViewModel
import com.fieldflo.screens.formDailyDemoLog.equipment.addEquipment.AddEquipmentViewmodel
import com.fieldflo.screens.formDailyDemoLog.generalInformation.GeneralInformationViewModel
import com.fieldflo.screens.formDailyDemoLog.materials.DailyDemoLogMaterialViewModel
import com.fieldflo.screens.formDailyDemoLog.materials.addMaterial.AddMaterialViewModel
import com.fieldflo.screens.formDailyDemoLog.miscComments.MiscCommentsViewModel
import com.fieldflo.screens.formDailyDemoLog.miscItems.MiscItemsViewModel
import com.fieldflo.screens.formDailyDemoLog.productionNarrative.ProductionNarrativeViewModel
import com.fieldflo.screens.formDailyDemoLog.subContractors.DailyDemoLogSubContractorsViewModel
import com.fieldflo.screens.formDailyDemoLog.subContractors.addSubcontractor.AddSubcontractorViewmodel
import com.fieldflo.screens.formDailyDemoLog.trucking.DailyDemoLogTruckingViewModel
import com.fieldflo.screens.formDailyDemoLog.trucking.addTruck.AddTruckingViewModel
import com.fieldflo.screens.formDailyDemoLog.verifySupervisor.DailyDemoVerifySupervisorViewModel
import com.fieldflo.screens.formDailyFieldReport.FormDailyFieldReportPresenter
import com.fieldflo.screens.formDailyLog.DailyLogFormPresenter
import com.fieldflo.screens.formDailyLog.verifySupervisor.VerifySupervisorPresenter
import com.fieldflo.screens.formPsi.PsiPresenter
import com.fieldflo.screens.formPsi.signatures.addEmployeesToSignatures.AddEmployeesToSignaturesPresenter
import com.fieldflo.screens.formPsi.taskSteps.addTask.AddTasksPresenter
import com.fieldflo.screens.formTimesheet.TimeSheetFormPresenter
import com.fieldflo.screens.formTimesheet.addEditAdditionalPositionDialog.AdditionalPositionPresenter
import com.fieldflo.screens.formTimesheet.addEditDefaultPositionDialog.DefaultPositionsPresenter
import com.fieldflo.screens.formTimesheet.addEmployees.AddTimeSheetEmployeesPresenter
import com.fieldflo.screens.formTimesheet.assetDialog.AssetPresenter
import com.fieldflo.screens.formTimesheet.editTime.EditTimePresenter
import com.fieldflo.screens.formTimesheet.notes.TimeSheetNotesPresenter
import com.fieldflo.screens.formTimesheet.perDiemDialog.PerDiemPresenter
import com.fieldflo.screens.formTimesheet.phases.PhasesPresenter
import com.fieldflo.screens.formTimesheet.stopTrackingMultipleEmployees.StopTrackingMultipleEmployeesPresenter
import com.fieldflo.screens.formTimesheet.stopTrackingSingleEmployee.StopTrackingSingleEmployeePresenter
import com.fieldflo.screens.formTimesheet.timesheetEmployee.TimeSheetEmployeePresenter
import com.fieldflo.screens.formTimesheet.verify.VerifyPresenter
import com.fieldflo.screens.formTimesheet.verifyMultiple.VerifyMultiplePresenter
import com.fieldflo.screens.initialDownload.InitialDownloadPresenter
import com.fieldflo.screens.login.LoginPresenter
import com.fieldflo.screens.manualAddCompany.ManualAddCompanyPresenter
import com.fieldflo.screens.projectDetails.ProjectDetailsPresenter
import com.fieldflo.screens.projectDetails.airMonitoring.AirMonitoringSpecialistViewModel
import com.fieldflo.screens.projectDetails.billing.ProjectBillingViewModel
import com.fieldflo.screens.projectDetails.buildingInformation.BuildingInformationViewModel
import com.fieldflo.screens.projectDetails.consultant.ConsultantViewModel
import com.fieldflo.screens.projectDetails.designer.ProjectDesignerViewModel
import com.fieldflo.screens.projectDetails.emergencyInfo.EmergencyInfoViewModel
import com.fieldflo.screens.projectDetails.generalInfo.GeneralInfoViewModel
import com.fieldflo.screens.projectDetails.insuranceCertificate.InsuranceCertificateViewModel
import com.fieldflo.screens.projectDetails.lienInfo.LienInformationViewModel
import com.fieldflo.screens.projectDetails.other.OtherViewModel
import com.fieldflo.screens.projectDetails.ownerInformation.ProjectOwnerViewModel
import com.fieldflo.screens.projectDetails.payroll.PayrollViewModel
import com.fieldflo.screens.projectDetails.perDiem.ProjectPerDiemViewModel
import com.fieldflo.screens.projectDetails.permits.ProjectPermitsViewModel
import com.fieldflo.screens.projectDetails.procedures.ProceduresViewModel
import com.fieldflo.screens.projectDetails.projectDetails.ProjectDetailsViewModel
import com.fieldflo.screens.projectDetails.projectSite.ProjectSiteViewModel
import com.fieldflo.screens.projectHome.ProjectHomePresenter
import com.fieldflo.screens.projectHome.activeForms.ActiveFormsPresenter
import com.fieldflo.screens.projectHome.addFrom.AddFormPresenter
import com.fieldflo.screens.projectHome.closedForms.ClosedFormsPresenter
import com.fieldflo.screens.projectHome.deletedForms.DeletedFormsPresenter
import com.fieldflo.screens.projects.ProjectsListPresenter
import com.fieldflo.screens.selectEmployee.SelectEmployeePresenter
import com.fieldflo.screens.selectSupervisor.SelectSupervisorPresenter
import com.fieldflo.screens.settings.SettingsPresenter
import com.fieldflo.screens.splashScreen.SplashScreenPresenter
import com.fieldflo.usecases.ServerUpdatesUseCases
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        (AnalyticsModule::class),
        (DataModule::class),
        (HelpersModule::class),
        (NetworkModule::class),
        (RepoModule::class)
    ]
)
interface ApplicationComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun applicationContext(applicationContext: Context): Builder

        @BindsInstance()
        fun appScopeHolder(holder: AppScopeHolder): Builder

        fun build(): ApplicationComponent
    }

    fun fieldFloWorkerFactory(): FieldFloWorkerFactory

    // Establish WorkerSubcomponent as subcomponent
    fun workerSubcomponentBuilder(): WorkerSubcomponent.Builder


    fun splashScreenViewModelFactory(): ViewModelFactory<SplashScreenPresenter>

    fun loginViewModelFactory(): ViewModelFactory<LoginPresenter>

    fun addCompanyViewModelFactory(): ViewModelFactory<AddCompanyPresenter>

    fun manualAddCompanyViewModelFactory(): ViewModelFactory<ManualAddCompanyPresenter>

    fun initialDownloadViewModelFactory(): ViewModelFactory<InitialDownloadPresenter>

    fun drawerViewModelFactory(): ViewModelFactory<DrawerPresenter>

    fun projectListsViewModelFactory(): ViewModelFactory<ProjectsListPresenter>

    fun projectDetailsViewModelFactory(): ViewModelFactory<ProjectDetailsPresenter>
    fun projectDetailsGeneralInfoViewModelFactory(): ViewModelFactory<GeneralInfoViewModel>
    fun projectDetailsDetailsViewModelFactory(): ViewModelFactory<ProjectDetailsViewModel>
    fun projectSiteViewModelFactory(): ViewModelFactory<ProjectSiteViewModel>
    fun projectPermitsViewModelFactory(): ViewModelFactory<ProjectPermitsViewModel>
    fun projectPerDiemViewModelFactory(): ViewModelFactory<ProjectPerDiemViewModel>
    fun buildingInformationViewModelFactory(): ViewModelFactory<BuildingInformationViewModel>
    fun projectOwnerViewModelFactory(): ViewModelFactory<ProjectOwnerViewModel>
    fun proceduresViewModelFactory(): ViewModelFactory<ProceduresViewModel>
    fun projectBillingViewModelFactory(): ViewModelFactory<ProjectBillingViewModel>
    fun airMonitoringSpecialistViewModelFactory(): ViewModelFactory<AirMonitoringSpecialistViewModel>
    fun consultantViewModelFactory(): ViewModelFactory<ConsultantViewModel>
    fun lienInformationViewModelFactory(): ViewModelFactory<LienInformationViewModel>
    fun emergencyInfoViewModelFactory(): ViewModelFactory<EmergencyInfoViewModel>
    fun insuranceCertificateViewModelFactory(): ViewModelFactory<InsuranceCertificateViewModel>
    fun otherViewModelFactory(): ViewModelFactory<OtherViewModel>
    fun payrollViewModelFactory(): ViewModelFactory<PayrollViewModel>
    fun projectDesignerViewModelFactory(): ViewModelFactory<ProjectDesignerViewModel>
    fun displayPicturePresenterFactory(): ViewModelFactory<DisplayPicturePresenter>

    fun employersCertificateViewModelFactory(): ViewModelFactory<EmployeesCertificatePresenter>
    fun printedCertificateViewModelFactory(): ViewModelFactory<PrintedCertificatePresenter>

    fun settingsViewModelFactory(): ViewModelFactory<SettingsPresenter>
    fun selectEmployeeViewModelFactory(): ViewModelFactory<SelectEmployeePresenter>
    fun selectSupervisorViewModelFactory(): ViewModelFactory<SelectSupervisorPresenter>

    fun dailyLogFormViewModelFactory(): ViewModelFactory<DailyLogFormPresenter>
    fun verifySupervisorViewModelFactory(): ViewModelFactory<VerifySupervisorPresenter>

    fun dailyDemoLogFormViewModelFactory(): ViewModelFactory<DailyDemoLogFormViewModel>
    fun dailyDemoLogGeneralInformationViewModelFactory(): ViewModelFactory<GeneralInformationViewModel>
    fun dailyDemoLogMiscItemsViewModelFactory(): ViewModelFactory<MiscItemsViewModel>
    fun dailyDemoLogMiscCommentsViewModelFactory(): ViewModelFactory<MiscCommentsViewModel>
    fun dailyDemoVerifySupervisorViewModelFactory(): ViewModelFactory<DailyDemoVerifySupervisorViewModel>
    fun dailyDemoLogProductionNarrativeViewModelFactory(): ViewModelFactory<ProductionNarrativeViewModel>
    fun dailyDemoLogEmployeesViewModelFactory(): ViewModelFactory<DailyDemoLogEmployeesViewModel>
    fun dailyDemoLogAddEmployeesPresenterFactory(): ViewModelFactory<AddEmployeesPresenter>
    fun dailyDailyDemoLogSubContractorsViewModelFactory(): ViewModelFactory<DailyDemoLogSubContractorsViewModel>
    fun addSubcontractorViewmodelFactory(): ViewModelFactory<AddSubcontractorViewmodel>
    fun addEquipmentViewmodelFactory(): ViewModelFactory<AddEquipmentViewmodel>
    fun addMaterialViewModelFactory(): ViewModelFactory<AddMaterialViewModel>
    fun addTruckingViewModelFactory(): ViewModelFactory<AddTruckingViewModel>
    fun dailyDemoLogEquipmentViewModelFactory(): ViewModelFactory<DailyDemoLogEquipmentViewModel>
    fun dailyDemoLogMaterialViewModelFactory(): ViewModelFactory<DailyDemoLogMaterialViewModel>
    fun dailyDemoLogTruckingViewModelFactory(): ViewModelFactory<DailyDemoLogTruckingViewModel>

    fun dailyFormViewModelFactory(): ViewModelFactory<FormDailyFieldReportPresenter>

    fun projectHomeViewModelFactory(): ViewModelFactory<ProjectHomePresenter>

    fun activeFormsViewModelFactory(): ViewModelFactory<ActiveFormsPresenter>

    fun closedFormViewModelFactory(): ViewModelFactory<ClosedFormsPresenter>

    fun deletedFormViewModelFactory(): ViewModelFactory<DeletedFormsPresenter>
    fun addFormViewModelFactory(): ViewModelFactory<AddFormPresenter>
    fun perDiemViewModelFactory(): ViewModelFactory<PerDiemPresenter>

    fun positionViewModelFactory(): ViewModelFactory<AdditionalPositionPresenter>
    fun assetViewModelFactory(): ViewModelFactory<AssetPresenter>

    fun timeSheetEmployeeViewModelFactory(): ViewModelFactory<TimeSheetEmployeePresenter>

    fun timeSheetFormViewModelFactory(): ViewModelFactory<TimeSheetFormPresenter>
    fun timeSheetNotesViewModelFactory(): ViewModelFactory<TimeSheetNotesPresenter>

    fun verifyViewModelFactory(): ViewModelFactory<VerifyPresenter>
    fun verifyMultipleViewModelFactory(): ViewModelFactory<VerifyMultiplePresenter>
    fun addTimeSheetEmployeesViewModelFactory(): ViewModelFactory<AddTimeSheetEmployeesPresenter>
    fun positionsViewModelFactory(): ViewModelFactory<DefaultPositionsPresenter>
    fun phasesViewModelFactory(): ViewModelFactory<PhasesPresenter>
    fun stopTrackingViewModelFactory(): ViewModelFactory<StopTrackingSingleEmployeePresenter>
    fun stopTrackingMultipleViewModelFactory(): ViewModelFactory<StopTrackingMultipleEmployeesPresenter>
    fun editTimeViewModelFactory(): ViewModelFactory<EditTimePresenter>

    fun psiFormViewModelFactory(): ViewModelFactory<PsiPresenter>
    fun psiFormAddEmployeesToSignaturesViewModelFactory(): ViewModelFactory<AddEmployeesToSignaturesPresenter>
    fun psiAddTasksViewModelFactory(): ViewModelFactory<AddTasksPresenter>
    fun serverUpdatesUseCase(): ServerUpdatesUseCases
    fun dataConnection(): IConnectionData
    fun appScopeHolder(): AppScopeHolder
}