package com.fieldflo.di

import android.content.Context
import com.fieldflo.common.Analytics
import com.fieldflo.common.FirebaseAnalyticsReporter
import dagger.Module
import dagger.Provides

@Module
object AnalyticsModule {

    @Provides
    fun provideAnalytics(context: Context): Analytics {
        return FirebaseAnalyticsReporter(context.applicationContext)
    }
}