package com.fieldflo.di

import android.content.Context
import androidx.fragment.app.Fragment

interface DaggerComponentProvider {
    val component: ApplicationComponent
}

val Context.injector get() = (applicationContext as DaggerComponentProvider).component

val Fragment.injector get() = (activity?.application as DaggerComponentProvider).component