package com.fieldflo.di

import android.content.Context
import androidx.room.Room
import com.fieldflo.BuildConfig
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.migrations.Migration1to2
import com.fieldflo.data.persistence.fileSystem.FileManager
import com.fieldflo.data.persistence.fileSystem.FileProvider
import com.fieldflo.data.persistence.fileSystem.IFileProvider
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.persistence.keyValue.LocalStorage
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object DataModule {

    @Singleton
    @Provides
    fun provideRoomDatabase(
        context: Context,
        migration1to2: Migration1to2
    ): FieldFloDatabase {
        return Room.databaseBuilder(
            context.applicationContext,
            FieldFloDatabase::class.java,
            "field_flo_db ${BuildConfig.APPLICATION_ID}"
        ).addMigrations(migration1to2)
            .build()
    }

    @Singleton
    @Provides
    fun provideLocalStorage(): ILocalStorage {
        return LocalStorage()
    }

    @Singleton
    @Provides
    fun provideFileProvider(context: Context): IFileProvider {
        return FileProvider(context)
    }

    @Singleton
    @Provides
    fun provideFileManager(fileProvider: IFileProvider): FileManager {
        return FileManager(fileProvider)
    }
}
