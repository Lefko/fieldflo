package com.fieldflo.di

import com.fieldflo.common.Analytics
import com.fieldflo.data.api.endpoints.*
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.fileSystem.FileManager
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.*
import com.fieldflo.data.repos.interfaces.*
import com.fieldflo.data.sync.WorkScheduler
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Singleton

@Module
object RepoModule {

    @Provides
    @Singleton
    fun provideAuthRepo(
        db: FieldFloDatabase,
        localStorage: ILocalStorage,
        authApi: AuthApi,
        analytics: Analytics
    ): IAuthRepo {
        return AuthRepo(db, localStorage, authApi, analytics)
    }

    @Provides
    @Singleton
    fun provideAvailableFormsRepo(
        db: FieldFloDatabase,
        companyDataApi: CompanyDataApi
    ): IAvailableFormsRepo {
        return AvailableFormRepo(db, companyDataApi)
    }

    @Provides
    @Singleton
    fun provideCompaniesRepo(db: FieldFloDatabase): ICompaniesRepo {
        return CompaniesRepo(db)
    }

    @Provides
    @Singleton
    fun provideCompanyCertificatesRepo(
        db: FieldFloDatabase,
        companyDataApi: CompanyDataApi
    ): ICompanyCertificatesRepo {
        return CompanyCertificatesRepo(db, companyDataApi)
    }

    @ExperimentalCoroutinesApi
    @Singleton
    @Provides
    fun provideDailyFieldReportRepo(
        db: FieldFloDatabase,
        manageForms: ManageFormsApi,
        fileManager: FileManager,
        workScheduler: WorkScheduler,
        moshi: Moshi
    ): IDailyFieldReportRepo =
        DailyFieldReportRepo(db, manageForms, fileManager, workScheduler, moshi)

    @Singleton
    @Provides
    fun provideDailyLogRepo(
        db: FieldFloDatabase,
        manageForms: ManageFormsApi,
        moshi: Moshi
    ): IDailyLogRepo =
        DailyLogRepo(db, manageForms, moshi)

    @Singleton
    @Provides
    fun provideDailyDemoLogRepo(
        db: FieldFloDatabase,
        manageForms: ManageFormsApi,
        fileManager: FileManager,
        workScheduler: WorkScheduler,
        moshi: Moshi
    ): IDailyDemoLogRepo =
        DailyDemoLogRepo(db, manageForms, fileManager, workScheduler, moshi)

    @Provides
    @Singleton
    fun provideEmployeesRepo(
        db: FieldFloDatabase,
        companyDataApi: CompanyDataApi
    ): IEmployeesRepo {
        return EmployeesRepo(db, companyDataApi)
    }

    @Provides
    @Singleton
    fun provideInventoryItemsRepo(
        db: FieldFloDatabase,
        companyDataApi: CompanyDataApi
    ): IInventoryItemsRepo {
        return InventoryItemsRepo(db, companyDataApi)
    }

    @Provides
    @Singleton
    fun providePhasesRepo(
        db: FieldFloDatabase,
        companyDataApi: CompanyDataApi
    ): IPhasesRepo {
        return PhasesRepo(db, companyDataApi)
    }

    @Provides
    @Singleton
    fun providePositionsRepo(
        db: FieldFloDatabase,
        companyDataApi: CompanyDataApi
    ): IPositionsRepo {
        return PositionsRepo(db, companyDataApi)
    }

    @Provides
    @Singleton
    fun provideProjectCertificatesRepo(
        db: FieldFloDatabase,
        companyDataApi: CompanyDataApi,
        certsApi: CertsApi,
        fileManager: FileManager,
        fileDownloader: FileDownloader
    ): IProjectCertificatesRepo {
        return ProjectCertificatesRepo(db, companyDataApi, certsApi, fileManager, fileDownloader)
    }

    @Singleton
    @Provides
    fun provideProjectFormsRepo(
        db: FieldFloDatabase,
        manageForms: ManageFormsApi
    ): IProjectFormsRepo =
        ProjectFormsRepo(db, manageForms)

    @Singleton
    @Provides
    fun provideProjectsRepo(db: FieldFloDatabase, companyDataApi: CompanyDataApi): IProjectsRepo =
        ProjectsRepo(db, companyDataApi)

    @Singleton
    @Provides
    fun providePsiRepo(
        db: FieldFloDatabase,
        manageForms: ManageFormsApi,
        fileManager: FileManager,
        workScheduler: WorkScheduler,
        moshi: Moshi
    ): IPsiRepo = PsiRepo(db, manageForms, fileManager, workScheduler, moshi)

    @Singleton
    @Provides
    fun provideServerUpdatesRepo(
        workScheduler: WorkScheduler,
        appSyncApi: AppSyncApi
    ): IServerUpdatesRepo = ServerUpdatesRepo(workScheduler, appSyncApi)

    @Provides
    @Singleton
    fun provideTasksRepo(db: FieldFloDatabase, companyDataApi: CompanyDataApi): ITasksRepo {
        return TasksRepo(db, companyDataApi)
    }

    @Provides
    @Singleton
    fun provideSubContractorsRepo(db: FieldFloDatabase, companyDataApi: CompanyDataApi): ISubContractorsRepo {
        return SubContractorsRepo(db, companyDataApi)
    }

    @Provides
    @Singleton
    fun provideHaulersRepo(db: FieldFloDatabase, companyDataApi: CompanyDataApi): IHaulersRepo {
        return HaulersRepo(db, companyDataApi)
    }

    @Singleton
    @Provides
    fun provideTimesheetRepo(
        db: FieldFloDatabase,
        manageForms: ManageFormsApi,
        fileManager: FileManager,
        workScheduler: WorkScheduler,
        moshi: Moshi
    ): ITimesheetRepo = TimesheetRepo(db, fileManager, manageForms, workScheduler, moshi)

}
