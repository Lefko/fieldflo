package com.fieldflo.di

import android.content.Context
import com.fieldflo.common.StringProvider
import dagger.Module
import dagger.Provides

@Module
object HelpersModule {

    @Provides
    fun provideStringProvider(context: Context) = StringProvider(context.applicationContext)
}