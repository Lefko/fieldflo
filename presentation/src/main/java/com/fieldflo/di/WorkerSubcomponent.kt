package com.fieldflo.di

import androidx.work.ListenableWorker
import androidx.work.WorkerParameters
import com.fieldflo.data.sync.workers.*
import dagger.BindsInstance
import dagger.Subcomponent
import javax.inject.Provider

@Subcomponent(
    modules = [
        UpdateFcmTokenWorker.Builder::class,
        AppToServerUpdatesWorker.Builder::class,

        UploadDailyFieldReportImageWorker.Builder::class,
        UploadTimesheetPinOutImageWorker.Builder::class,
        UploadPsiPinOutImageWorker.Builder::class,
        UploadDailyDemoSupervisorPinOutImageWorker.Builder::class,
        UploadDailyDemoImageWorker.Builder::class
    ]
)
interface WorkerSubcomponent {

    fun workers(): Map<Class<out ListenableWorker>, Provider<ListenableWorker>>

    @Subcomponent.Builder
    interface Builder {
        @BindsInstance
        fun workerParameters(param: WorkerParameters): Builder

        fun build(): WorkerSubcomponent
    }
}