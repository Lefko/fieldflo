package com.fieldflo.screens.splashScreen

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.fieldflo.common.getPermission
import com.fieldflo.common.hasPermission
import com.fieldflo.di.injector
import com.fieldflo.screens.initialDownload.InitialDownloadActivity
import com.fieldflo.screens.login.LoginActivity
import com.fieldflo.screens.projects.ProjectsListActivity
import com.fieldflo.usecases.LoggedInState

class SplashScreen : AppCompatActivity() {

    companion object {
        fun start(sender: Activity) {
            val intent = Intent(sender, SplashScreen::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or
                    Intent.FLAG_ACTIVITY_CLEAR_TASK or
                    Intent.FLAG_ACTIVITY_SINGLE_TOP or
                    Intent.FLAG_ACTIVITY_CLEAR_TOP
            sender.startActivity(intent)
        }
    }

    private val presenter by viewModels<SplashScreenPresenter> { injector.splashScreenViewModelFactory() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkCameraPermission()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CAMERA_REQUEST_CODE) {
            checkCameraPermission()
        }
    }

    private fun checkCameraPermission() {
        if (!hasPermission(Manifest.permission.CAMERA)) {
            getPermission(Manifest.permission.CAMERA, CAMERA_REQUEST_CODE)
        } else {
            loadNextScreen()
        }
    }

    private fun loadNextScreen() {
        lifecycleScope.launchWhenCreated {
            when (val loggedInState = presenter.loggedInState()) {
                LoggedInState.LoggedOut -> LoginActivity.start(this@SplashScreen)
                is LoggedInState.InitialDownload -> InitialDownloadActivity.start(
                    this@SplashScreen, loggedInState.securityKey, loggedInState.token
                )
                LoggedInState.Projects -> {
                    ProjectsListActivity.start(this@SplashScreen)
                    presenter.deleteExpiredPhotoPins()
                }
            }
            finish()
        }
    }
}

private const val CAMERA_REQUEST_CODE = 1337
