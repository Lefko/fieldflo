package com.fieldflo.screens.splashScreen

import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.AuthUseCases
import com.fieldflo.usecases.LoggedInState
import com.fieldflo.usecases.PinUseCases
import javax.inject.Inject

class SplashScreenPresenter @Inject constructor(
    private val authUseCases: AuthUseCases,
    private val pinUseCases: PinUseCases
): ViewModel() {

    suspend fun loggedInState(): LoggedInState {
        return authUseCases.getLoggedInState()
    }

    fun deleteExpiredPhotoPins() = pinUseCases.deletePhotoPins()
}