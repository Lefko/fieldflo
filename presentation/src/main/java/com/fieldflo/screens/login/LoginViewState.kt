package com.fieldflo.screens.login

data class LoginViewState(
    val validEmail: Boolean = false,
    val validPassword: Boolean = false,
    val companySelected: Boolean = false,
    val loading: Boolean = false,
    val error: LoginError = LoginError.None,
    val companies: List<BasicCompanyUiModel> = listOf()
) {
    val loginEnabled
        get() = validEmail && validPassword && companySelected
}

sealed class LoginError {
    object GenericError : LoginError() {
        override fun toString() = "LoginError.GenericError"
    }

    object BadUnPwError : LoginError() {
        override fun toString() = "LoginError.BadUnPwError"
    }

    object None : LoginError() {
        override fun toString() = "LoginError.None"
    }
}

data class BasicCompanyUiModel(
    val securityKey: String,
    val token: String,
    val companyName: String
) {
    override fun toString() = companyName
}