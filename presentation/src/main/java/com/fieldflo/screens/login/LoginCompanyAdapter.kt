package com.fieldflo.screens.login

import android.content.Context
import android.widget.ArrayAdapter
import com.fieldflo.R

class LoginCompanyAdapter(context: Context) :
    ArrayAdapter<BasicCompanyUiModel>(context, R.layout.row_spinner_login_company) {

    init {
        setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    }

    fun submitList(companies: MutableList<BasicCompanyUiModel>) {
        clear()
        addAll(companies)
        notifyDataSetChanged()
    }
}