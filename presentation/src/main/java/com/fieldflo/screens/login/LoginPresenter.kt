package com.fieldflo.screens.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fieldflo.data.persistence.db.entities.CompanyData
import com.fieldflo.data.repos.interfaces.LoginResponse
import com.fieldflo.usecases.AuthUseCases
import com.fieldflo.usecases.CompanyUseCases
import com.fieldflo.usecases.LoginParams
import com.fieldflo.usecases.PinUseCases
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

class LoginPresenter @Inject constructor(
    private val authUseCases: AuthUseCases,
    private val photoPinUseCases: PinUseCases,
    private val companiesUseCases: CompanyUseCases
) : ViewModel() {

    private val viewStateObservable = MutableStateFlow(LoginViewState())

    init {
        viewModelScope.launch {
            companiesUseCases.getCompaniesListFlow()
                .catch { Timber.e(it) }
                .collect { companiesDataList ->
                    val previousState = viewStateObservable.value
                    val newState = previousState.copy(
                        companies = companiesDataList.map { it.toUiModel() }
                    )
                    viewStateObservable.value = newState
                }
        }
    }

    fun onEmailEntered(email: String) {
        val previousState = viewStateObservable.value
        viewStateObservable.value = previousState.copy(
            validEmail = authUseCases.isEmailValid(email),
            error = LoginError.None
        )
    }

    fun onPasswordEntered(password: String) {
        val previousState = viewStateObservable.value
        viewStateObservable.value = previousState.copy(
            validPassword = authUseCases.isPasswordValid(password),
            error = LoginError.None
        )
    }

    fun onCompanySelected(selected: Boolean) {
        val previousState = viewStateObservable.value
        viewStateObservable.value = previousState.copy(
            companySelected = selected
        )
    }

    suspend fun onLoginClicked(loginParams: LoginParams): Boolean {
        viewStateObservable.value = viewStateObservable.value.copy(
            loading = true,
            error = LoginError.None
        )
        return withContext(Dispatchers.IO) {
            val result = authUseCases.login(loginParams)
            withContext(Dispatchers.Main) {
                when (result) {
                    is LoginResponse.Success -> {
                        viewStateObservable.value = viewStateObservable.value.copy(
                            loading = false,
                            error = LoginError.None
                        )
                        true
                    }

                    LoginResponse.InvalidPw -> {
                        viewStateObservable.value = viewStateObservable.value.copy(
                            loading = false,
                            error = LoginError.BadUnPwError
                        )
                        false
                    }

                    LoginResponse.GeneralError -> {
                        viewStateObservable.value = viewStateObservable.value.copy(
                            loading = false,
                            error = LoginError.GenericError
                        )
                        false
                    }
                }
            }
        }
    }

    suspend fun didInitialDownload(securityKey: String, token: String) =
        companiesUseCases.didInitialDownload(securityKey, token)

    fun viewState(): StateFlow<LoginViewState> = viewStateObservable

    fun deleteExpiredPhotoPins() = photoPinUseCases.deletePhotoPins()

    private fun CompanyData.toUiModel(): BasicCompanyUiModel =
        BasicCompanyUiModel(this.securityKey, this.token, this.companyName)
}
