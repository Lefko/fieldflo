package com.fieldflo.screens.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.*
import com.fieldflo.di.injector
import com.fieldflo.screens.addCompany.AddCompanyActivity
import com.fieldflo.screens.initialDownload.InitialDownloadActivity
import com.fieldflo.screens.projects.ProjectsListActivity
import com.fieldflo.usecases.LoginParams
import com.google.firebase.analytics.FirebaseAnalytics
import io.shipbook.shipbooksdk.ShipBook
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.flow.collect
import timber.log.Timber

class LoginActivity : AppCompatActivity(R.layout.activity_login) {

    companion object {
        fun start(sender: Activity) {
            val intent = Intent(sender, LoginActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            sender.startActivity(intent)
        }
    }

    private val presenter by viewModels<LoginPresenter> { injector.loginViewModelFactory() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity_login_add_companyTV.setOnClickListener {
            AddCompanyActivity.start(this)
        }

        activity_login_add_company_btnTV.setOnClickListener {
            AddCompanyActivity.start(this)
        }

        LoginCompanyAdapter(this).also {
            activity_login_form_select_companySPNR.adapter = it
        }

        observeUserInteraction()

        lifecycleScope.launchWhenCreated {
            presenter.viewState().collect { render(it) }
        }
    }

    private fun render(viewState: LoginViewState) {
        Timber.d("$viewState")
        activity_login_has_companyGroup.isVisible = viewState.companies.isNotEmpty()
        activity_login_no_companyGroup.isVisible = !viewState.companies.isNotEmpty()

        renderLoading(viewState.loading)

        renderCompanies(viewState.companies)

        enableLogin(viewState)

        renderError(viewState.error)
    }

    override fun onResume() {
        super.onResume()
        activity_login_form_usernameET.text = null
        activity_login_form_passwordET.text = null
    }

    private fun renderLoading(loading: Boolean) {
        activity_login_loading_group.isVisible = loading
    }

    private fun renderCompanies(companies: List<BasicCompanyUiModel>) {
        activity_login_form_select_companySPNR.isEnabled = companies.isNotEmpty()
        updateAdapter(companies)
    }

    private fun updateAdapter(companies: List<BasicCompanyUiModel>) {
        val mutableCompanies = companies.toMutableList()

        if (companies.size > 1) {
            val selectOne =
                BasicCompanyUiModel(
                    "ignore",
                    "ignore",
                    getString(R.string.activity_login_select_company_hint)
                )
            mutableCompanies.add(0, selectOne)
        }

        (activity_login_form_select_companySPNR.adapter as LoginCompanyAdapter)
            .submitList(mutableCompanies)

        if (companies.size == 1) {
            activity_login_form_select_companySPNR.setSelection(0)
        }
    }

    private fun enableLogin(viewState: LoginViewState) {
        activity_login_loginTV.isEnabled = viewState.loginEnabled
    }

    private fun renderError(error: LoginError) {
        when (error) {
            is LoginError.None -> {
                activity_login_form_passwordET.error = null
                activity_login_errorTV.hidden()
            }

            is LoginError.BadUnPwError -> {
                activity_login_form_passwordET.error = getString(R.string.activity_login_bad_un_pw)
                activity_login_errorTV.hidden()
            }

            is LoginError.GenericError -> {
                activity_login_form_passwordET.error = null
                activity_login_errorTV.visible()
            }
        }
    }

    private fun observeUserInteraction() {
        addCompanySelectedListener()
        observeEmailEntered()
        observePasswordEntered()
        observeLogin()
    }

    private fun addCompanySelectedListener() {
        activity_login_form_select_companySPNR.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position != -1) {
                        val company =
                            (activity_login_form_select_companySPNR.adapter as LoginCompanyAdapter)
                                .getItem(position)

                        presenter.onCompanySelected(company?.securityKey != "ignore")
                    } else {
                        presenter.onCompanySelected(false)
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }
    }

    private fun observeEmailEntered() {
        activity_login_form_usernameET.afterTextChanges { email, _ ->
            presenter.onEmailEntered(email)
        }
    }

    private fun observePasswordEntered() {
        activity_login_form_passwordET.afterTextChanges { password, _ ->
            presenter.onPasswordEntered(password)
        }
    }

    private fun observeLogin() {
        activity_login_loginTV.safeClickListener(800) {
            hideKeyboard()
            val email = activity_login_form_usernameET.text.toString()
            val pw = activity_login_form_passwordET.text.toString()
            val selectedCompany =
                activity_login_form_select_companySPNR.selectedItem as BasicCompanyUiModel
            val securityKey = selectedCompany.securityKey
            val token = selectedCompany.token
            lifecycleScope.launchWhenCreated {
                val loginSuccess = presenter
                    .onLoginClicked(LoginParams(securityKey, token, email, pw))
                if (loginSuccess) {
                    val didInitialDownload = presenter.didInitialDownload(securityKey, token)
                    ShipBook.registerUser(email, email = email)
                    FirebaseAnalytics.getInstance(this@LoginActivity).setUserId(email)
                    if (didInitialDownload) {
                        ProjectsListActivity.start(this@LoginActivity)
                        presenter.deleteExpiredPhotoPins()
                    } else {
                        InitialDownloadActivity.start(this@LoginActivity, securityKey, token)
                    }
                    finish()
                }
            }
        }
    }
}
