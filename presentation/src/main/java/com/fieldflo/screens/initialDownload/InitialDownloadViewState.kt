package com.fieldflo.screens.initialDownload

import com.fieldflo.usecases.DownloadProgress

data class InitialDownloadViewState(
    val isWifiConnected: Boolean = false,
    val downloadUpdate: DownloadProgress = DownloadProgress.NotStarted(false)
) {
   val isDownloading
        get() = downloadUpdate.isDownloading
}
