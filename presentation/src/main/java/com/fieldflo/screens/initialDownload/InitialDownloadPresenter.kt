package com.fieldflo.screens.initialDownload

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fieldflo.common.networkConnection.IConnectionData
import com.fieldflo.usecases.CompanyUseCases
import com.fieldflo.usecases.DownloadProgress
import com.fieldflo.usecases.InitialDownloader2
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class InitialDownloadPresenter @Inject constructor(
    private val companyUseCases: CompanyUseCases,
    private val initialDownloader: InitialDownloader2,
    private val networkConnection: IConnectionData
) : ViewModel() {

    private val viewStateObservable = MutableStateFlow(
        InitialDownloadViewState(
            isWifiConnected = networkConnection.isWifiConnected
        )
    )

    init {
        observeWifiConnection()
    }

    private fun observeWifiConnection() {
        viewModelScope.launch {
            networkConnection.getWifiConnected()
                .onEach { Timber.d("Wifi connected: $it") }
                .distinctUntilChanged()
                .collect { wifiConnected ->
                    val previousState = viewStateObservable.value
                    if (wifiConnected) {
                        if (previousState.downloadUpdate !is DownloadProgress.NotStarted) {
                            initialDownloader.resumeInitialDownload()
                        }
                    } else {
                        initialDownloader.pauseInitialDownload()
                    }


                    val newState = previousState.copy(isWifiConnected = wifiConnected)
                    viewStateObservable.value = newState
                }
        }

        viewModelScope.launch(Dispatchers.IO) {
            initialDownloader.getProgressUpdatesFlow()
                .collect { downloadProgress ->
                    val previousState = viewStateObservable.value
                    val newState = previousState.copy(downloadUpdate = downloadProgress)
                    viewStateObservable.value = newState
                }
        }
    }

    fun observeViewState(): Flow<InitialDownloadViewState> = viewStateObservable

    fun pauseDownload() =
        viewModelScope.launch(Dispatchers.Default) { initialDownloader.pauseInitialDownload() }

    fun startOrResumeDownload() {
        val notStarted = viewStateObservable.value.downloadUpdate is DownloadProgress.NotStarted
        viewModelScope.launch(Dispatchers.IO) {
            if (notStarted) {
                initialDownloader.startInitialDownload()
            } else {
                initialDownloader.resumeInitialDownload()
            }
        }
    }

    suspend fun cancelDownload(securityKey: String, token: String) {
        companyUseCases.deleteCompanyData(securityKey, token)
    }
}
