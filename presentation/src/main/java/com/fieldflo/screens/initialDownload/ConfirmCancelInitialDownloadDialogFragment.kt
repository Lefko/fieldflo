package com.fieldflo.screens.initialDownload

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.fieldflo.R

class ConfirmCancelInitialDownloadDialogFragment : DialogFragment() {

    interface ConfirmDownloadListener {
        fun onConfirmDownloadCancel()
    }

    private var listener: ConfirmDownloadListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(activity as Context)
            .setTitle(R.string.dialog_initial_download_cancel_title)
            .setMessage(R.string.dialog_initial_download_cancel_message)
            .setNegativeButton(
                R.string.dialog_initial_download_negative_text,
                { _, _ -> dismiss() })
            .setPositiveButton(R.string.dialog_initial_download_positive_text,
                { _, _ -> listener?.onConfirmDownloadCancel() }
            ).show()
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    private fun onAttachToContext(context: Context) {
        if (context is ConfirmDownloadListener) {
            listener = context
        } else {
            throw IllegalArgumentException(
                "${context.javaClass.simpleName} " +
                        "must be an instance of ConfirmDownloadListener"
            )
        }
    }
}