package com.fieldflo.screens.initialDownload

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.animation.LinearInterpolator
import androidx.activity.OnBackPressedCallback
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.CONFIRM_CANCEL_INITIAL_DOWNLOAD_DIALOG_FRAGMENT_TAG
import com.fieldflo.common.safeClickListener
import com.fieldflo.di.injector
import com.fieldflo.screens.projects.ProjectsListActivity
import com.fieldflo.screens.splashScreen.SplashScreen
import com.fieldflo.usecases.DownloadProgress
import kotlinx.android.synthetic.main.activity_initial_download_paused.*
import kotlinx.coroutines.flow.collect

class InitialDownloadActivity : AppCompatActivity(),
    ConfirmCancelInitialDownloadDialogFragment.ConfirmDownloadListener {

    companion object {
        @JvmStatic
        fun start(sender: Activity, securityKey: String, token: String) {
            val intent = Intent(sender, InitialDownloadActivity::class.java).apply {
                putExtra(SECURITY_KEY_KEY, securityKey)
                putExtra(TOKEN_KEY, token)
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            sender.startActivity(intent)
        }

        private const val SECURITY_KEY_KEY = "security_key_key"
        private const val TOKEN_KEY = "token_key"
    }

    private val notResumedSet = ConstraintSet()
    private val resumedSet = ConstraintSet()

    private val presenter by viewModels<InitialDownloadPresenter> { injector.initialDownloadViewModelFactory() }

    private val securityKey by lazy {
        intent!!.extras!!.getString(SECURITY_KEY_KEY, "")
    }

    private val token by lazy {
        intent!!.extras!!.getString(TOKEN_KEY, "")
    }

    private val progressBarAnimator by lazy {
        ValueAnimator.ofFloat(0f, 360f).apply {
            repeatCount = ValueAnimator.INFINITE
            duration = 1200
            interpolator = LinearInterpolator()
            addUpdateListener {
                activity_initial_download_downloading_loadingIV.rotation = it.animatedValue as Float
            }
        }
    }

    private val backWhenDownloading = object : OnBackPressedCallback(false) {
        override fun handleOnBackPressed() {
            presenter.pauseDownload()
        }
    }

    private val backWhenPaused = object : OnBackPressedCallback(false) {
        override fun handleOnBackPressed() {
            showConfirmationDialog()
        }
    }

    private val backBeforeStarted = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            cancelDownload()
        }
    }

    private fun showConfirmationDialog() {
        ConfirmCancelInitialDownloadDialogFragment()
            .show(supportFragmentManager, CONFIRM_CANCEL_INITIAL_DOWNLOAD_DIALOG_FRAGMENT_TAG)
    }

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_initial_download_paused)

        val resumedView = LayoutInflater.from(this)
            .inflate(R.layout.activity_initial_download_resumed, null)
        val resumedRoot: ConstraintLayout =
            resumedView.findViewById(R.id.activity_initial_download_rootCL)

        notResumedSet.clone(activity_initial_download_rootCL)
        resumedSet.clone(resumedRoot)

        progressBarAnimator.start()

        onBackPressedDispatcher.addCallback(this, backWhenDownloading)
        onBackPressedDispatcher.addCallback(this, backWhenPaused)
        onBackPressedDispatcher.addCallback(this, backBeforeStarted)

        initListeners()

        present()
    }

    override fun onDestroy() {
        progressBarAnimator.removeAllUpdateListeners()
        progressBarAnimator.cancel()
        super.onDestroy()
    }

    override fun onConfirmDownloadCancel() {
        cancelDownload()
    }

    private fun cancelDownload() {
        lifecycleScope.launchWhenCreated {
            presenter.cancelDownload(securityKey, token)
            SplashScreen.start(this@InitialDownloadActivity)
            finish()
        }
    }

    private fun initListeners() {
        activity_initial_download_pre_download_backTV.safeClickListener {
            onBackPressed()
        }

        activity_initial_download_downloading_cancelTV.safeClickListener {
            showConfirmationDialog()
        }

        activity_initial_download_pre_download_continueTV.safeClickListener {
            presenter.startOrResumeDownload()
        }

        activity_initial_download_downloading_pauseTV.safeClickListener { presenter.pauseDownload() }
    }

    private fun present() {
        lifecycleScope.launchWhenCreated {
            presenter.observeViewState().collect { render(it) }
        }
    }

    private fun render(viewState: InitialDownloadViewState) {
        renderDownloadProgress(viewState.downloadUpdate)
        if (viewState.isDownloading) {
            backWhenDownloading.isEnabled = true
            backWhenPaused.isEnabled = false
            backBeforeStarted.isEnabled = false
        } else {
            backWhenDownloading.isEnabled = false
            if (viewState.downloadUpdate is DownloadProgress.NotStarted) {
                backWhenPaused.isEnabled = false
                backBeforeStarted.isEnabled = true
            } else {
                backWhenPaused.isEnabled = true
                backBeforeStarted.isEnabled = false
            }
        }

        // wifi related views
        if (viewState.isWifiConnected) {
            activity_initial_download_pre_download_wifi_iconIV
                .setImageResource(R.drawable.ic_wifi_detected)

            activity_initial_download_pre_download_statusTV.setText(
                R.string.activity_initial_download_pre_download_connected
            )

            activity_initial_download_pre_download_continueTV.isEnabled = true
        } else {
            activity_initial_download_pre_download_wifi_iconIV
                .setImageResource(R.drawable.ic_wifi_not_detected)

            activity_initial_download_pre_download_statusTV.setText(
                R.string.activity_initial_download_pre_download_not_connected
            )

            activity_initial_download_pre_download_continueTV.isEnabled = false
        }

        // Animate to appropriate screen
        TransitionManager.beginDelayedTransition(activity_initial_download_rootCL)
        val constraint = if (viewState.isDownloading) resumedSet else notResumedSet
        constraint.applyTo(activity_initial_download_rootCL)
    }

    private fun renderDownloadProgress(progress: DownloadProgress) {
        when (progress) {
            is DownloadProgress.NotStarted,
            is DownloadProgress.Started -> {
                activity_initial_download_downloading_generalCB.isChecked = false
                activity_initial_download_downloading_projectsCB.isChecked = false
            }

            is DownloadProgress.DownloadedGeneral -> {
                activity_initial_download_downloading_generalCB.isChecked = true
                activity_initial_download_downloading_projectsCB.isChecked = false
            }

            is DownloadProgress.DownloadedProjects -> {
                activity_initial_download_downloading_generalCB.isChecked = true
                activity_initial_download_downloading_projectsCB.isChecked = true
            }

            is DownloadProgress.Complete -> {
                activity_initial_download_downloading_generalCB.isChecked = true
                activity_initial_download_downloading_projectsCB.isChecked = true
                ProjectsListActivity.start(this)
                finish()
            }
        }
    }
}
