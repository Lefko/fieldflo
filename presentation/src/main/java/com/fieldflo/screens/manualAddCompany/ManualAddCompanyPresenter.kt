package com.fieldflo.screens.manualAddCompany

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fieldflo.usecases.AddCompanyParams
import com.fieldflo.usecases.CompanyUseCases
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class ManualAddCompanyPresenter @Inject constructor(private val companiesUseCases: CompanyUseCases) :
    ViewModel() {

    private val viewStateObservable = MutableStateFlow(ManualAddCompanyViewState())

    fun viewState(): StateFlow<ManualAddCompanyViewState> = viewStateObservable

    fun addCompany(companyName: String, baseUrl: String, apiToken: String, secToken: String) {
        val addCompanyParam = AddCompanyParams(companyName, baseUrl, apiToken, secToken)
        viewModelScope.launch(Dispatchers.IO) {
            companiesUseCases.addCompany(addCompanyParam)
            viewStateObservable.value = viewStateObservable.value.copy(companyAddSuccess = true)
        }
    }

    fun companyNameInput(companyName: String) {
        viewStateObservable.value = viewStateObservable.value.copy(companyName = companyName)
    }

    fun urlInput(url: String) {
        viewStateObservable.value = viewStateObservable.value.copy(url = url)
    }

    fun securityTokenInput(securityKey: String) {
        viewStateObservable.value = viewStateObservable.value.copy(securityKey = securityKey)
    }

    fun tokenInput(token: String) {
        viewStateObservable.value = viewStateObservable.value.copy(token = token)
    }
}
