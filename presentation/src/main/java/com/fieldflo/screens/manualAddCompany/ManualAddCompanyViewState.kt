package com.fieldflo.screens.manualAddCompany

data class ManualAddCompanyViewState(
    val companyAddSuccess: Boolean = false,
    private val companyName: String = "",
    private val url: String = "",
    private val securityKey: String = "",
    private val token: String = ""
) {
    val saveEnabled
        get() = companyName.isNotEmpty() && url.isNotEmpty() && securityKey.isNotEmpty() && token.isNotEmpty()
}
