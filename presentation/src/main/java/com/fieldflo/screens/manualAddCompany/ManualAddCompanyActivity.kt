package com.fieldflo.screens.manualAddCompany

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.afterTextChanges
import com.fieldflo.common.safeClickListener
import com.fieldflo.di.injector
import kotlinx.android.synthetic.main.activity_add_company_manual_add.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ManualAddCompanyActivity : AppCompatActivity(R.layout.activity_add_company_manual_add) {

    companion object {
        @JvmStatic
        fun start(sender: Activity) {
            val intent = Intent(sender, ManualAddCompanyActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            sender.startActivity(intent)
        }
    }

    private val presenter by viewModels<ManualAddCompanyPresenter> { injector.manualAddCompanyViewModelFactory() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initObservables()

        present()
    }

    private fun initObservables() {
        activity_add_company_manual_add_saveTV.safeClickListener {
            presenter.addCompany(
                activity_add_company_manual_add_company_name_inputET.text.toString(),
                activity_add_company_manual_add_url_inputET.text.toString(),
                activity_add_company_manual_add_token_inputET.text.toString(),
                activity_add_company_manual_add_key_inputET.text.toString()
            )
        }
        activity_add_company_manual_add_company_name_inputET.afterTextChanges { s, _ ->
            presenter.companyNameInput(s)
        }

        activity_add_company_manual_add_url_inputET.afterTextChanges { s, _ ->
            presenter.urlInput(s)
        }

        activity_add_company_manual_add_token_inputET.afterTextChanges { s, _ ->
            presenter.tokenInput(s)
        }

        activity_add_company_manual_add_key_inputET.afterTextChanges { s, _ ->
            presenter.securityTokenInput(s)
        }

        activity_add_company_manual_add_backTV.safeClickListener { finish() }
    }

    private fun present() {
        lifecycleScope.launch {
            presenter.viewState().collect {
                if (it.companyAddSuccess) {
                    finish()
                } else {
                    activity_add_company_manual_add_saveTV.isEnabled = it.saveEnabled
                }
            }
        }
    }
}
