package com.fieldflo.screens.formTimesheet

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.fieldflo.R
import com.fieldflo.common.safeClickListener
import com.fieldflo.common.toast
import com.fieldflo.di.injector
import com.fieldflo.screens.drawer.DrawerManager
import com.fieldflo.screens.formTimesheet.addEditDefaultPositionDialog.DefaultPositionsDialogFragment
import com.fieldflo.screens.formTimesheet.addEmployees.AddTimeSheetEmployeesDialogFragment
import com.fieldflo.screens.formTimesheet.confirmStartTracking.ConfirmStartTrackingDialogFragment
import com.fieldflo.screens.formTimesheet.confirmStartTracking.ConfirmStartTrackingMultipleDialogFragment
import com.fieldflo.screens.formTimesheet.delete.DeleteTimeSheetEmployeeDialogFragment
import com.fieldflo.screens.formTimesheet.editTime.EditTimeDialogFragment
import com.fieldflo.screens.formTimesheet.notes.TimeSheetNotesDialogFragment
import com.fieldflo.screens.formTimesheet.phases.PhasesDialogFragment
import com.fieldflo.screens.formTimesheet.stopTrackingMultipleEmployees.StopTrackingMultipleEmployeesDialogFragment
import com.fieldflo.screens.formTimesheet.stopTrackingSingleEmployee.StopTrackingSingleEmployeeDialogFragment
import com.fieldflo.screens.formTimesheet.timesheetEmployee.TimeSheetEmployeeActivity
import com.fieldflo.screens.formTimesheet.verify.VerifySingleEmployeeDialogFragment
import com.fieldflo.screens.formTimesheet.verifyMultiple.VerifyMultipleEmployeesDialogFragment
import com.fieldflo.screens.selectEmployee.SelectEmployeeDialog
import com.fieldflo.screens.selectSupervisor.SelectSupervisorDialog
import com.fieldflo.screens.selectSupervisor.SupervisorEmployeeUiRow
import com.fieldflo.usecases.ProjectStatus
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.view_activity_daily_timesheet_content.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter

class TimeSheetFormActivity : AppCompatActivity(R.layout.activity_daily_timesheet),
    VerifySingleEmployeeDialogFragment.VerifyDialogCallback,
    VerifyMultipleEmployeesDialogFragment.VerifyMultipleDialogCallback,
    DefaultPositionsDialogFragment.PositionSelectedListener,
    PhasesDialogFragment.PhaseSelectedListener,
    AddTimeSheetEmployeesDialogFragment.EmployeesSelectedListener,
    DeleteTimeSheetEmployeeDialogFragment.DeleteTimeSheetEmployeeConfirmedListener,
    SelectSupervisorDialog.SupervisorSelectedCallback,
    ConfirmStartTrackingDialogFragment.StartTrackingConfirmedListener,
    ConfirmStartTrackingMultipleDialogFragment.StartTrackingMultipleConfirmedListener,
    StopTrackingMultipleEmployeesDialogFragment.StopTrackingFailedListener,
    EditTimeDialogFragment.EditTimeListener {

    companion object {
        private const val PROJECT_ID_KEY = "project_id"
        private const val INTERNAL_FORM_ID_KEY = "internal_form_id"

        fun start(
            sender: Activity,
            projectId: Int,
            internalFormId: Int
        ) {
            val intent = Intent(sender, TimeSheetFormActivity::class.java).apply {
                putExtra(INTERNAL_FORM_ID_KEY, internalFormId)
                putExtra(PROJECT_ID_KEY, projectId)
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            sender.startActivity(intent)
        }
    }

    private val projectId: Int by lazy { intent!!.getIntExtra(PROJECT_ID_KEY, 0) }
    private val internalFormId: Int by lazy { intent!!.getIntExtra(INTERNAL_FORM_ID_KEY, 0) }
    private val presenter by viewModels<TimeSheetFormPresenter> { injector.timeSheetFormViewModelFactory() }

    private val drawerManager by lazy {
        DrawerManager(
            { findViewById(R.id.drawer_layout) }, { this },
            { findViewById(R.id.activity_daily_timesheet_list_content_toolbar) }
        )

    }
    private lateinit var adapter: TimeSheetFormAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycleScope.launchWhenCreated {
            presenter.getProjectStatusFlow(projectId)
                .filter { it == ProjectStatus.DELETED }
                .collect { finish() }
        }

        initRecyclerView()

        initListeners()

        present()
    }

    override fun onBackPressed() {
        if (!drawerManager.onBackPress()) {
            super.onBackPressed()
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawerManager.onConfigChange(newConfig)
    }

    private fun initListeners() {
        activity_daily_timesheet_list_content_backTV.safeClickListener {
            finish()
        }

        activity_daily_timesheet_list_content_add_supervisorTV.safeClickListener {
            lifecycleScope.launchWhenCreated {
                val errorMessage = presenter.addSupervisorToTimeSheet(internalFormId)
                if (errorMessage.isNotEmpty()) {
                    Snackbar.make(
                        activity_daily_timesheet_list_rootCL,
                        errorMessage,
                        Snackbar.LENGTH_LONG
                    ).show()
                }
            }
        }

        activity_daily_timesheet_list_content_select_allRB.safeClickListener {
            presenter.setAllEmployeesSelected(activity_daily_timesheet_list_content_select_allRB.isChecked)
        }

        activity_daily_timesheet_list_content_addTV.safeClickListener {
            AddTimeSheetEmployeesDialogFragment.newInstance()
                .show(supportFragmentManager, AddTimeSheetEmployeesDialogFragment.TAG)
        }

        activity_daily_timesheet_list_content_notesIV.safeClickListener {
            TimeSheetNotesDialogFragment.newInstance(internalFormId)
                .show(supportFragmentManager, TimeSheetNotesDialogFragment.TAG)
        }

        activity_daily_timesheet_list_content_supervisor_click_mask.safeClickListener {
            SelectSupervisorDialog.newInstance()
                .show(supportFragmentManager, SelectEmployeeDialog.TAG)
        }

        activity_daily_timesheet_list_content_startTV.safeClickListener {
            ConfirmStartTrackingMultipleDialogFragment.newInstance(
                adapter.selectedRows.map { it.timeSheetEmployeeId }.toIntArray(),
                adapter.selectedRows.map { it.employeeFullName }.toTypedArray()
            ).show(supportFragmentManager, ConfirmStartTrackingMultipleDialogFragment.TAG)
        }

        activity_daily_timesheet_list_content_stopTV.safeClickListener {
            StopTrackingMultipleEmployeesDialogFragment.newInstance(
                projectId,
                internalFormId,
                adapter.selectedRows.map { it.timeSheetEmployeeId }.toIntArray()
            ).show(supportFragmentManager, StopTrackingMultipleEmployeesDialogFragment.TAG)
        }

        activity_daily_timesheet_list_content_time_edit_title.safeClickListener {
            EditTimeDialogFragment.newInstance(
                projectId,
                internalFormId,
                adapter.selectedRows.map { it.timeSheetEmployeeId }.toIntArray()
            ).show(supportFragmentManager, EditTimeDialogFragment.TAG)
        }

        activity_daily_timesheet_list_content_verifyTV.safeClickListener {
            val timeSheetEmployees = adapter.selectedRows
            val timeSheetEmployeeIds =
                timeSheetEmployees.map { it.timeSheetEmployeeId }.toIntArray()
            if (timeSheetEmployeeIds.count() > 1) {
                VerifyMultipleEmployeesDialogFragment.newInstance(
                    projectId,
                    internalFormId,
                    adapter.selectedRows.map { it.timeSheetEmployeeId }.toIntArray()
                ).show(supportFragmentManager, VerifyMultipleEmployeesDialogFragment.TAG)
            } else {
                VerifySingleEmployeeDialogFragment.newInstance(
                    timeSheetEmployeeId = timeSheetEmployeeIds[0],
                    employeeId = timeSheetEmployees[0].employeeId,
                    internalFormId = internalFormId
                ).show(supportFragmentManager, VerifySingleEmployeeDialogFragment.TAG)
            }
        }
    }

    private fun present() {
        lifecycleScope.launchWhenCreated {
            val usePhases: Boolean = presenter.getProjectUsesPhases(projectId)
            adapter = TimeSheetFormAdapter(usePhases, ::onEmployeeRowAction)
            activity_daily_timesheet_list_employeesRV.adapter = adapter
            presenter.timeSheetViewState.collect { render(it) }
        }
        presenter.loadTimeSheetData(internalFormId, projectId)
    }

    private fun onEmployeeRowAction(
        employee: TimeSheetFormViewState.TimeSheetEmployeeUiModel,
        action: TimeSheetFormAdapter.EmployeeRowAction
    ) {
        when (action) {
            TimeSheetFormAdapter.EmployeeRowAction.ROW_SELECT_CLICKED ->
                presenter.toggleEmployeeSelected(employee.timeSheetEmployeeId)

            is TimeSheetFormAdapter.EmployeeRowAction.ACTION_EMPLOYEE_CLICKED ->
                TimeSheetEmployeeActivity.start(
                    sender = this,
                    timeSheetEmployeeId = employee.timeSheetEmployeeId,
                    projectId = projectId,
                    usePhases = action.usePhases,
                    internalFormId = internalFormId
                )

            TimeSheetFormAdapter.EmployeeRowAction.ACTION_DELETED ->
                DeleteTimeSheetEmployeeDialogFragment.newInstance(
                    employee.timeSheetEmployeeId,
                    employee.employeeFullName
                ).show(supportFragmentManager, DeleteTimeSheetEmployeeDialogFragment.TAG)

            TimeSheetFormAdapter.EmployeeRowAction.PHASE_CLICKED ->
                PhasesDialogFragment.newInstance(employee.timeSheetEmployeeId, projectId)
                    .show(supportFragmentManager, PhasesDialogFragment.TAG)

            TimeSheetFormAdapter.EmployeeRowAction.POSITION_CLICKED ->
                DefaultPositionsDialogFragment.newInstance(employee.timeSheetEmployeeId, projectId)
                    .show(supportFragmentManager, DefaultPositionsDialogFragment.TAG)

            TimeSheetFormAdapter.EmployeeRowAction.STATUS_CLICKED -> onStatusClick(employee)
        }
    }

    private fun initRecyclerView() {
        activity_daily_timesheet_list_employeesRV.layoutManager = LinearLayoutManager(this)
    }

    private fun render(viewState: TimeSheetFormViewState) {
        activity_daily_timesheet_list_content_project_nameTV.text = viewState.projectName
        activity_daily_timesheet_list_content_today_dateTV.text = viewState.formDate
        activity_daily_timesheet_list_content_employee_name.text =
            viewState.supervisorInfo?.supervisorFullName

        activity_daily_timesheet_list_content_add_supervisorTV.isEnabled =
            viewState.addSupervisorEnabled

        activity_daily_timesheet_list_content_select_allRB.isChecked = viewState.allSelected

        activity_daily_timesheet_list_content_startTV.isEnabled = viewState.startEnabled

        activity_daily_timesheet_list_content_stopTV.isEnabled = viewState.stopEnabled

        activity_daily_timesheet_list_content_verifyTV.isEnabled = viewState.verifyEnabled

        activity_daily_timesheet_list_content_time_edit_title.isEnabled = viewState.editTimeEnabled

        adapter.submitList(viewState.timeSheetEmployees)
    }

    private fun onStatusClick(employee: TimeSheetFormViewState.TimeSheetEmployeeUiModel) {
        when (employee.employeeState) {
            TimeSheetEmployeeState.NOT_READY -> {
                val positionMissing = employee.positionName.isEmpty()
                val phaseMissing = employee.phaseName.isEmpty() && employee.usePhases
                val textRes: Int = when {
                    positionMissing && phaseMissing -> R.string.activity_time_sheet_position_and_phase_missing
                    phaseMissing -> R.string.activity_time_sheet_phase_missing
                    positionMissing -> R.string.activity_time_sheet_position_missing
                    employee.isTracking -> R.string.activity_time_sheet_tracking_somewhere
                    else -> -1
                }
                if (textRes != -1) {
                    toast(textRes)
                }
            }

            TimeSheetEmployeeState.READY ->
                ConfirmStartTrackingDialogFragment.newInstance(
                    employee.timeSheetEmployeeId,
                    employee.employeeFullName
                ).show(supportFragmentManager, ConfirmStartTrackingDialogFragment.TAG)

            TimeSheetEmployeeState.TRACKING ->
                StopTrackingSingleEmployeeDialogFragment.newInstance(
                    timeSheetEmployeeId = employee.timeSheetEmployeeId,
                    employeeId = employee.employeeId,
                    projectId = projectId,
                    internalFormId = internalFormId
                ).show(supportFragmentManager, StopTrackingSingleEmployeeDialogFragment.TAG)

            TimeSheetEmployeeState.MUST_VERIFY ->
                VerifySingleEmployeeDialogFragment.newInstance(
                    timeSheetEmployeeId = employee.timeSheetEmployeeId,
                    employeeId = employee.employeeId,
                    internalFormId = internalFormId
                ).show(supportFragmentManager, VerifySingleEmployeeDialogFragment.TAG)

            TimeSheetEmployeeState.VERIFIED,
            TimeSheetEmployeeState.VERIFIED_WITH_INJURY -> {
                // ignore... no action to take
            }
        }
    }

    override fun onEmployeesSelected(employeeIds: List<Int>) {
        lifecycleScope.launchWhenCreated {
            val errorMessage =
                presenter.addEmployeesToTimeSheet(employeeIds, internalFormId)
            if (errorMessage.isNotEmpty()) {
                Snackbar
                    .make(activity_daily_timesheet_list_rootCL, errorMessage, Snackbar.LENGTH_LONG)
                    .show()
            }
        }
    }

    override fun onPositionSelected(timeSheetEmployeeId: Int, positionId: Int) {
        presenter.setEmployeePosition(timeSheetEmployeeId, positionId)
    }

    override fun onPhaseSelected(timeSheetEmployeeId: Int, phaseId: Int) {
        presenter.setEmployeePhase(timeSheetEmployeeId, phaseId)
    }

    override fun onVerifyTracking(
        timeSheetEmployeeId: Int,
        isInjured: Boolean,
        tookFederalBreak: Boolean,
        verifiedByEmployeeId: Int
    ) {
        presenter.verifyEmployees(
            listOf(timeSheetEmployeeId),
            isInjured,
            tookFederalBreak,
            verifiedByEmployeeId
        )
    }

    override fun onVerifyMultiple(
        timeSheetEmployeeIds: IntArray,
        isInjured: Boolean,
        tookFederalBreak: Boolean,
        verifiedByEmployeeId: Int
    ) {
        presenter.verifyEmployees(
            timeSheetEmployeeIds.toList(),
            isInjured,
            tookFederalBreak,
            verifiedByEmployeeId
        )
    }

    override fun onDeleteTimeSheetEmployeeConfirmed(timeSheetEmployeeId: Int) {
        presenter.removeEmployee(timeSheetEmployeeId)
    }

    override fun onSupervisorSelected(employee: SupervisorEmployeeUiRow) {
        presenter.updateSupervisor(employee.employeeId, internalFormId)
    }

    override fun onStartTrackingConfirmed(timeSheetEmployeeId: Int) {
        presenter.startTracking(listOf(timeSheetEmployeeId))
    }

    override fun onStartTrackingMultipleConfirmed(timeSheetEmployeeIds: IntArray) {
        presenter.startTracking(timeSheetEmployeeIds.asList())
    }

    override fun onStopTrackingFailed(timeSheetEmployeeInfo: List<String>) {
        ErrorDialogFragment.newInstance(
            timeSheetEmployeeInfo.toTypedArray(),
            R.string.activity_time_sheet_stop_tracking_error
        ).show(supportFragmentManager, ErrorDialogFragment.TAG)
    }

    override fun onEditFailed(timeSheetEmployeeInfo: List<String>) {
        ErrorDialogFragment.newInstance(
            timeSheetEmployeeInfo.toTypedArray(),
            R.string.activity_time_sheet_time_edit_error
        ).show(supportFragmentManager, ErrorDialogFragment.TAG)
    }
}
