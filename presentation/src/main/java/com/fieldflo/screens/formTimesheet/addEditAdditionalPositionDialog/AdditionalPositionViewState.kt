package com.fieldflo.screens.formTimesheet.addEditAdditionalPositionDialog

data class AdditionalPositionViewState(
    val positions: List<PositionUiRow>,
    val phases: List<PhaseUiRow>,
    val currentPosition: TimeSheetEmployeePositionUiModel?,
    val totalWorkTime: Int,
    private val availableTime: Int
) {
    val okEnabled: Boolean
        get() = totalWorkTime in 1 until availableTime

    data class PositionUiRow(
        val positionId: Int,
        val positionName: String
    ) {
        override fun toString() = positionName
    }

    data class TimeSheetEmployeePositionUiModel(
        val totalWorkTime: Int,
        val positionId: Int,
        val phaseId: Int
    )

    data class PhaseUiRow(
        val phaseId: Int,
        val phaseName: String,
        val phaseCode: String
    ) {
        override fun toString() = phaseName
    }
}