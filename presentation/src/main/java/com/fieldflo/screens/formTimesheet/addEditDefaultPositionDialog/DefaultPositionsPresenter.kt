package com.fieldflo.screens.formTimesheet.addEditDefaultPositionDialog

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.Position
import com.fieldflo.usecases.PositionUseCases
import javax.inject.Inject

class DefaultPositionsPresenter @Inject constructor(
    private val positionUseCases: PositionUseCases
) : ViewModel() {

    suspend fun getAllPositions(projectId: Int, timeSheetEmployeeId: Int) =
        positionUseCases.getPositionsForTimeSheetDefaultPosition(projectId, timeSheetEmployeeId)
            .map { it.toUiModel() }
}

data class PositionUiRow(
    val positionId: Int = 0,
    val positionName: String = ""
)

private fun Position.toUiModel() = PositionUiRow(
    positionId = this.positionId,
    positionName = this.positionName
)