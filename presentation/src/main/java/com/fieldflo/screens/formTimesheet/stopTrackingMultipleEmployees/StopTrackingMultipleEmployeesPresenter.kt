package com.fieldflo.screens.formTimesheet.stopTrackingMultipleEmployees

import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.TimeSheetTimeClockUseCases
import com.fieldflo.usecases.TimeSheetUseCases
import com.fieldflo.usecases.VerifyTimeSheetPinResult
import javax.inject.Inject

class StopTrackingMultipleEmployeesPresenter @Inject constructor(
    private val timeSheetUseCases: TimeSheetUseCases,
    private val timeSheetTimeClockUseCases: TimeSheetTimeClockUseCases
) : ViewModel() {

    suspend fun stopTrackingEmployees(
        projectId: Int,
        internalFormId: Int,
        timeSheetEmployeeIds: List<Int>,
        breakDurationSeconds: Int,
        enteredPin: String
    ): StopTrackingMultipleResult {
        val pinResult = timeSheetUseCases.verifyTimeSheetPin(internalFormId, pin = enteredPin)
        if (pinResult === VerifyTimeSheetPinResult.VerifyError) {
            return StopTrackingMultipleResult.PinFailure
        }

        val failedToStop = timeSheetTimeClockUseCases.stopTrackingEmployees(
            projectId,
            internalFormId,
            timeSheetEmployeeIds,
            breakDurationSeconds
        )
        return if (failedToStop.isEmpty()) {
            StopTrackingMultipleResult.Success
        } else {
            StopTrackingMultipleResult.BreakTooLongFailure(failedToStop)
        }
    }

}

sealed class StopTrackingMultipleResult {
    object Success : StopTrackingMultipleResult()
    object PinFailure : StopTrackingMultipleResult()
    data class BreakTooLongFailure(
        val timeSheetEmployees: List<String>
    ) : StopTrackingMultipleResult()
}