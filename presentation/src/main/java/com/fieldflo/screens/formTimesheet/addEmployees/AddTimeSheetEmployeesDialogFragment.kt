package com.fieldflo.screens.formTimesheet.addEmployees

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.ListView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector

class AddTimeSheetEmployeesDialogFragment : DialogFragment() {

    private val presenter by viewModels<AddTimeSheetEmployeesPresenter> { injector.addTimeSheetEmployeesViewModelFactory() }

    private val employeesAdapter by lazy { EmployeesAdapter(activity!!) }

    private lateinit var employeesSelectedListener: EmployeesSelectedListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val dialog = AlertDialog.Builder(activity as Context)
            .setTitle(R.string.activity_daily_timesheet_add_employees)
            .setAdapter(employeesAdapter, null)
            .setPositiveButton(android.R.string.ok, { _, _ ->
                val listView = (dialog as AlertDialog).listView

                val selectedEmployees = (0 until listView.adapter.count)
                    .filter { listView.checkedItemPositions[it] }
                    .mapNotNull { employeesAdapter.getItem(it) }
                    .map { it.employeeId }

                if (selectedEmployees.isEmpty()) {
                    dismiss()
                } else {
                    employeesSelectedListener.onEmployeesSelected(selectedEmployees)
                }
            })
            .setNegativeButton(android.R.string.cancel, { _, _ ->
                dismiss()
            })
            .show()

        dialog.listView.itemsCanFocus = false
        dialog.listView.choiceMode = ListView.CHOICE_MODE_MULTIPLE

        return dialog
    }

    override fun onStart() {
        super.onStart()
        present()
    }

    private fun present() {
        lifecycleScope.launchWhenCreated {
            render(presenter.getEmployees())
        }
    }

    private fun render(employees: List<EmployeeUiRow>) {
        employeesAdapter.clear()
        employeesAdapter.addAll(employees)
        employeesAdapter.notifyDataSetChanged()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    private fun onAttachToContext(context: Context) {
        if (context is EmployeesSelectedListener) {
            employeesSelectedListener = context
        } else {
            throw IllegalArgumentException("${context.javaClass.simpleName} must implement EmployeesSelectedListener")
        }
    }


    companion object {
        const val TAG = "AddTimeSheetEmployeesDialogFragment"

        @JvmStatic
        fun newInstance() =
            AddTimeSheetEmployeesDialogFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }

    interface EmployeesSelectedListener {
        fun onEmployeesSelected(employeeIds: List<Int>)
    }
}

