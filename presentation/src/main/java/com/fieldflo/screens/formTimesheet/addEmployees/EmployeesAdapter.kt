package com.fieldflo.screens.formTimesheet.addEmployees

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class EmployeesAdapter(context: Context) :
    ArrayAdapter<EmployeeUiRow>(
        context,
        android.R.layout.simple_list_item_multiple_choice,
        mutableListOf()
    ) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val listItem = convertView ?: LayoutInflater.from(context)
            .inflate(android.R.layout.simple_list_item_multiple_choice, parent, false)

        (listItem as TextView).text = getItem(position)?.employeeName

        return listItem
    }
}