package com.fieldflo.screens.formTimesheet.assetDialog

data class AssetViewState(
    val assets: List<InventoryItemUiRow> = listOf(),
    val asset: TimeSheetAssetUiModel? = null
) {

    data class TimeSheetAssetUiModel(
        val timeSheetEmployeeAssetId: Int,
        val assetName: String,
        val assetNumber: Int,
        val totalWorkTime: Int,
        val assetId: Int,
        val notes: String
    ) {
        override fun toString() = assetName
    }

    data class InventoryItemUiRow(
        val inventoryItemId: Int,
        val inventoryItemName: String,
        val inventoryItemNumber: Int
    ) {
        override fun toString() = inventoryItemName
    }
}