package com.fieldflo.screens.formTimesheet.stopTrackingSingleEmployee

import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.TimeSheetTimeClockUseCases
import com.fieldflo.usecases.TimeSheetUseCases
import com.fieldflo.usecases.VerifyTimeSheetPinResult
import javax.inject.Inject

class StopTrackingSingleEmployeePresenter @Inject constructor(
    private val timeSheetUseCases: TimeSheetUseCases,
    private val timeSheetTimeClockUseCases: TimeSheetTimeClockUseCases
) : ViewModel() {

    suspend fun stopTrackingEmployee(
        projectId: Int,
        internalFormId: Int,
        employeeId: Int,
        timeSheetEmployeeId: Int,
        breakDurationSeconds: Int,
        enteredPin: String
    ): StopTrackingResult {
        val pinResult = timeSheetUseCases.verifyTimeSheetPin(internalFormId, employeeId, enteredPin)
        if (pinResult === VerifyTimeSheetPinResult.VerifyError) {
            return StopTrackingResult.PIN_FAILURE
        }
        val failedToStop = timeSheetTimeClockUseCases.stopTrackingEmployees(
            projectId,
            internalFormId,
            listOf(timeSheetEmployeeId),
            breakDurationSeconds
        )
        return if (failedToStop.isEmpty()) {
            StopTrackingResult.SUCCESS
        } else {
            StopTrackingResult.BREAK_TOO_LONG_FAILURE
        }
    }
}

enum class StopTrackingResult {
    SUCCESS, PIN_FAILURE, BREAK_TOO_LONG_FAILURE
}