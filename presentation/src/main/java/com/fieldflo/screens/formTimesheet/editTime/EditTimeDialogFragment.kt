package com.fieldflo.screens.formTimesheet.editTime

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.view.children
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.*
import com.fieldflo.common.dateTimePickers.DateTimePickerDialog
import com.fieldflo.di.injector
import com.fieldflo.usecases.EditTimeResult
import java.text.SimpleDateFormat
import java.util.*

class EditTimeDialogFragment : DialogFragment() {

    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }
    private val internalFormId by lazy { arguments!!.getInt(INTERNAL_FORM_ID) }
    private val timeSheetEmployeeIds by lazy {
        arguments!!.getIntArray(TIME_SHEET_EMPLOYEE_IDS)?.toList() ?: emptyList()
    }

    private lateinit var dontChangeStartCB: CheckBox
    private lateinit var startTimeContainerLL: LinearLayout
    private lateinit var startTimeTV: TextView
    private lateinit var dontChangeEndCB: CheckBox
    private lateinit var endTimeContainerLL: LinearLayout
    private lateinit var endTimeTV: TextView
    private lateinit var dontChangeBreakCB: CheckBox
    private lateinit var breakTimeSpinner: Spinner
    private lateinit var breakTimeDisabled: View
    private lateinit var pinET: EditText
    private lateinit var pinErrorTV: TextView

    private lateinit var positiveButton: Button
    private lateinit var editTimeListener: EditTimeListener

    private val presenter by viewModels<EditTimePresenter> { injector.editTimeViewModelFactory() }
    private val positiveEnabled: Boolean
        get() {
            return pinET.text.isNotEmpty() &&
                    (!dontChangeStartCB.isChecked || !dontChangeEndCB.isChecked || !dontChangeBreakCB.isChecked)
        }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_edit_time, null)

        findViews(view)

        initViews()

        return AlertDialog.Builder(activity as Context)
            .setView(view)
            .setPositiveButton(R.string.dialog_edit_time_save) { _, _ -> }
            .setNegativeButton(android.R.string.cancel) { _, _ -> dismiss() }
            .show()
    }

    private fun findViews(root: View) {
        dontChangeStartCB = root.findViewById(R.id.dialog_edit_time_dont_change_startCB)
        startTimeContainerLL = root.findViewById(R.id.dialog_edit_time_start_time_containerLL)
        startTimeTV = root.findViewById(R.id.dialog_edit_time_start_timeTV)
        dontChangeEndCB = root.findViewById(R.id.dialog_edit_time_dont_change_endCB)
        endTimeContainerLL = root.findViewById(R.id.dialog_edit_time_end_time_containerLL)
        endTimeTV = root.findViewById(R.id.dialog_edit_time_end_timeTV)
        dontChangeBreakCB = root.findViewById(R.id.dialog_edit_time_dont_change_breakCB)
        breakTimeSpinner = root.findViewById(R.id.dialog_edit_time_break_time_spinner)
        breakTimeDisabled = root.findViewById(R.id.dialog_edit_time_break_time_disabled)
        pinET = root.findViewById(R.id.dialog_edit_time_pinET)
        pinErrorTV = root.findViewById(R.id.dialog_edit_time_incorrect_pinTV)
    }

    private fun initViews() {
        dontChangeStartCB.setOnClickListener {
            enableContainer(startTimeContainerLL, !dontChangeStartCB.isChecked)
            positiveButton.isEnabled = positiveEnabled
        }

        dontChangeEndCB.setOnClickListener {
            enableContainer(endTimeContainerLL, !dontChangeEndCB.isChecked)
            positiveButton.isEnabled = positiveEnabled
        }

        dontChangeBreakCB.setOnClickListener {
            breakTimeSpinner.isEnabled = !dontChangeBreakCB.isChecked
            breakTimeDisabled.visibility =
                if (dontChangeBreakCB.isChecked) View.VISIBLE else View.GONE
            positiveButton.isEnabled = positiveEnabled
        }

        startTimeContainerLL.setOnClickListener {
            val date = extractDateFromTextForPicker(startTimeTV, Date())
            DateTimePickerDialog.newInstance(
                date,
                object : DateTimePickerDialog.DateSelectedListener {
                    override fun onDateSelected(newDate: Date) {
                        startTimeTV.text = newDate.toFullString()
                        positiveButton.isEnabled = positiveEnabled

                    }
                }).show(childFragmentManager, DateTimePickerDialog.TAG)
        }

        endTimeContainerLL.setOnClickListener {
            val date = extractDateFromTextForPicker(endTimeTV, Date())
            DateTimePickerDialog.newInstance(
                date,
                object : DateTimePickerDialog.DateSelectedListener {
                    override fun onDateSelected(newDate: Date) {
                        endTimeTV.text = newDate.toFullString()
                        positiveButton.isEnabled = positiveEnabled
                    }
                }).show(childFragmentManager, DateTimePickerDialog.TAG)
        }

        breakTimeSpinner.adapter = ArrayAdapter.createFromResource(
            breakTimeSpinner.context,
            R.array.dialog_stop_tracking_break_time_list,
            R.layout.row_spinner_dropdown
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }

        pinET.afterTextChanges { _, _ ->
            run {
                pinErrorTV.gone()
                positiveButton.isEnabled = positiveEnabled
            }
        }
    }

    private fun enableContainer(container: ViewGroup, enable: Boolean) {
        container.children.forEach {
            it.visibility = if (enable) View.VISIBLE else View.INVISIBLE
        }
        container.isEnabled = enable
    }

    private fun extractDateFromTextForPicker(dateTV: TextView, defaultDate: Date = Date(0)): Date {
        val dateText = dateTV.text.toString()
        return if (dateText == "") {
            defaultDate
        } else {
            return try {
                SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US).parse(dateText)!!
            } catch (e: Exception) {
                Date()
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    private fun onAttachToContext(context: Context) {
        editTimeListener = context as? EditTimeListener
            ?: throw IllegalArgumentException("${context::class.java.simpleName} must implement EditTimeListener")
    }

    override fun onStart() {
        super.onStart()
        positiveButton = (dialog as AlertDialog).getButton(Dialog.BUTTON_POSITIVE).apply {
            isEnabled = false
        }
        positiveButton.safeClickListener {
            val editTimeData = EditTimeData(
                projectId = projectId,
                internalFormId = internalFormId,
                timeSheetEmployeeIds = timeSheetEmployeeIds,
                pin = pinET.text.toString(),
                startDate = if (dontChangeStartCB.isChecked) null else extractDateFromTextForPicker(
                    startTimeTV
                ),
                endDate = if (dontChangeEndCB.isChecked) null else extractDateFromTextForPicker(
                    endTimeTV
                ),
                breakDurationSeconds = if (dontChangeBreakCB.isChecked) null else convertToSecond(
                    breakTimeSpinner.selectedItemPosition
                ),
                dontChangeStart = dontChangeStartCB.isChecked,
                dontChangeEnd = dontChangeEndCB.isChecked,
                dontChangeBreak = dontChangeBreakCB.isChecked
            )
            lifecycleScope.launchWhenCreated {
                val result = presenter.editTime(editTimeData)
                when (result) {
                    is EditTimeResult.Success -> dismiss()

                    is EditTimeResult.PinFailure -> pinErrorTV.visible()

                    is EditTimeResult.EditFailedFor -> {
                        editTimeListener.onEditFailed(result.timeSheetEmployeeNames)
                        dismiss()
                    }
                }
            }
        }
        present()
    }

    private fun present() {
        lifecycleScope.launchWhenCreated {
            render(presenter.getTimeSheetEmployeeStartStop(timeSheetEmployeeIds))
        }
    }

    private fun render(viewState: EditTimeViewState) {
        startTimeTV.text = viewState.startTime.toFullString()
        endTimeTV.text = viewState.endTime.toFullString()
        breakTimeSpinner.setSelection(convertToPosition(viewState.breakDurationSeconds))
    }

    private fun convertToSecond(position: Int): Int {
        return when (position) {
            0 -> 0 // dialog_stop_tracking_break_time_no
            1 -> 300 // dialog_stop_tracking_break_time_5
            2 -> 600 // dialog_stop_tracking_break_time_10
            3 -> 900 // dialog_stop_tracking_break_time_15
            4 -> 1200 // dialog_stop_tracking_break_time_20
            5 -> 1500 // dialog_stop_tracking_break_time_25
            6 -> 1800 // dialog_stop_tracking_break_time_30
            7 -> 2100 // dialog_stop_tracking_break_time_35
            8 -> 2400 // dialog_stop_tracking_break_time_40
            9 -> 2700 // dialog_stop_tracking_break_time_45
            10 -> 3000 // dialog_stop_tracking_break_time_50
            11 -> 3300 // dialog_stop_tracking_break_time_55
            12 -> 3600 // dialog_stop_tracking_break_time_60
            13 -> 3900 // dialog_stop_tracking_break_time_65
            14 -> 4200 // dialog_stop_tracking_break_time_70
            15 -> 4500 // dialog_stop_tracking_break_time_75
            16 -> 4800 // dialog_stop_tracking_break_time_80
            17 -> 5100 // dialog_stop_tracking_break_time_85
            18 -> 5400 // dialog_stop_tracking_break_time_90
            19 -> 5700 // dialog_stop_tracking_break_time_95
            20 -> 6000 // dialog_stop_tracking_break_time_100
            21 -> 6300 // dialog_stop_tracking_break_time_105
            22 -> 6600 // dialog_stop_tracking_break_time_110
            23 -> 6900 // dialog_stop_tracking_break_time_115
            24 -> 7200  // dialog_stop_tracking_break_time_120
            25 -> 9000 // dialog_stop_tracking_break_time_2h_30
            26 -> 10800 // dialog_stop_tracking_break_time_3h
            27 -> 12600 // dialog_stop_tracking_break_time_3h_30
            28 -> 14400 // dialog_stop_tracking_break_time_4h
            29 -> 16200 // dialog_stop_tracking_break_time_4h_30
            30 -> 18000 // dialog_stop_tracking_break_time_5h
            31 -> 19800 // dialog_stop_tracking_break_time_5h_30
            32 -> 21600 // dialog_stop_tracking_break_time_6h
            33 -> 23400 // dialog_stop_tracking_break_time_6h_30
            34 -> 25200 // dialog_stop_tracking_break_time_7h
            35 -> 27000 // dialog_stop_tracking_break_time_7h_30
            36 -> 28800 // dialog_stop_tracking_break_time_8h
            37 -> 30600 // dialog_stop_tracking_break_time_8h_30
            else -> 0
        }
    }

    private fun convertToPosition(breakDuration: Int): Int {
        val position = if (breakDuration < 0) {
            0
        } else {
            when (breakDuration) {
                in (0 until 300) -> 0 // dialog_stop_tracking_break_time_no
                in (300 until 600) -> 1 // dialog_stop_tracking_break_time_5
                in (600 until 900) -> 2 // dialog_stop_tracking_break_time_10
                in (900 until 1200) -> 3 // dialog_stop_tracking_break_time_15
                in (1200 until 1500) -> 4 // dialog_stop_tracking_break_time_20
                in (1500 until 1800) -> 5 // dialog_stop_tracking_break_time_25
                in (1800 until 2100) -> 6 // dialog_stop_tracking_break_time_30
                in (2100 until 2400) -> 7 // dialog_stop_tracking_break_time_35
                in (2400 until 2700) -> 8 // dialog_stop_tracking_break_time_40
                in (2700 until 3000) -> 9 // dialog_stop_tracking_break_time_45
                in (3000 until 3300) -> 10 // dialog_stop_tracking_break_time_50
                in (3300 until 3600) -> 11 // dialog_stop_tracking_break_time_55
                in (3600 until 3900) -> 12 // dialog_stop_tracking_break_time_60
                in (3900 until 4200) -> 13 // dialog_stop_tracking_break_time_65
                in (4200 until 4500) -> 14 // dialog_stop_tracking_break_time_70
                in (4500 until 4800) -> 15 // dialog_stop_tracking_break_time_75
                in (4800 until 5100) -> 16 // dialog_stop_tracking_break_time_80
                in (5100 until 5400) -> 17 // dialog_stop_tracking_break_time_85
                in (5400 until 5700) -> 18 // dialog_stop_tracking_break_time_90
                in (5700 until 6000) -> 19 // dialog_stop_tracking_break_time_95
                in (6000 until 6300) -> 20 // dialog_stop_tracking_break_time_100
                in (6300 until 6600) -> 21 // dialog_stop_tracking_break_time_105
                in (6600 until 6900) -> 22 // dialog_stop_tracking_break_time_110
                in (6900 until 7200) -> 23 // dialog_stop_tracking_break_time_115
                in (7200 until 9000) -> 24 // dialog_stop_tracking_break_time_120
                in (9000 until 10800) -> 25 // dialog_stop_tracking_break_time_2h_30
                in (10800 until 12600) -> 26 // dialog_stop_tracking_break_time_3h
                in (12600 until 14400) -> 27 // dialog_stop_tracking_break_time_3h_30
                in (14400 until 16200) -> 28 // dialog_stop_tracking_break_time_4h
                in (16200 until 18000) -> 29 // dialog_stop_tracking_break_time_4h_30
                in (18000 until 19800) -> 30 // dialog_stop_tracking_break_time_5h
                in (19800 until 21600) -> 31 // dialog_stop_tracking_break_time_5h_30
                in (21600 until 23400) -> 32 // dialog_stop_tracking_break_time_6h
                in (23400 until 25200) -> 33 // dialog_stop_tracking_break_time_6h_30
                in (25200 until 27000) -> 34 // dialog_stop_tracking_break_time_7h
                in (27000 until 28800) -> 35 // dialog_stop_tracking_break_time_7h_30
                in (28800 until 30600) -> 36 // dialog_stop_tracking_break_time_8h
                else -> 37
            }
        }
        return position
    }

    companion object {
        const val TAG = "EditTimeDialogFragment"
        private const val PROJECT_ID = "project_id"
        private const val INTERNAL_FORM_ID = "internal_form_id"
        private const val TIME_SHEET_EMPLOYEE_IDS = "time_sheet_employee_ids"

        fun newInstance(
            projectId: Int,
            internalFormId: Int,
            timeSheetEmployeeIds: IntArray
        ): EditTimeDialogFragment {
            return EditTimeDialogFragment().apply {
                arguments = Bundle().apply {
                    putInt(PROJECT_ID, projectId)
                    putInt(INTERNAL_FORM_ID, internalFormId)
                    putIntArray(TIME_SHEET_EMPLOYEE_IDS, timeSheetEmployeeIds)
                }
            }
        }
    }

    interface EditTimeListener {
        fun onEditFailed(timeSheetEmployeeInfo: List<String>)
    }
}