package com.fieldflo.screens.formTimesheet.addEditDefaultPositionDialog

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector

class DefaultPositionsDialogFragment : DialogFragment() {

    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }
    private val timeSheetEmployeeId by lazy { arguments!!.getInt(TIMESHEET_EMPLOYEE_ID) }

    private val presenter by viewModels<DefaultPositionsPresenter> { injector.positionsViewModelFactory() }

    private var selectedPosition: PositionUiRow? = null

    private val positionsAdapter by lazy { DefaultPositionsAdapter(context!!) }

    private lateinit var positionSelectedListener: PositionSelectedListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = AlertDialog.Builder(activity as Context)
            .setTitle(R.string.activity_daily_timesheet_dialog_add_positions_title)
            .setSingleChoiceItems(positionsAdapter, -1, { _, selectedPos ->
                selectedPosition = positionsAdapter.getItem(selectedPos)
            })
            .setPositiveButton(android.R.string.ok, { _, _ ->
                if (selectedPosition == null) {
                    dismiss()
                } else {
                    positionSelectedListener.onPositionSelected(
                        timeSheetEmployeeId,
                        selectedPosition!!.positionId
                    )
                }
            })
            .setNegativeButton(android.R.string.cancel, { _, _ ->

            })
            .show()

        return dialog
    }

    override fun onStart() {
        super.onStart()
        present()
    }

    private fun present() {
        lifecycleScope.launchWhenCreated {
            render(presenter.getAllPositions(projectId, timeSheetEmployeeId))
        }
    }

    private fun render(positions: List<PositionUiRow>) {
        positionsAdapter.clear()
        positionsAdapter.addAll(positions)
        positionsAdapter.notifyDataSetChanged()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    private fun onAttachToContext(context: Context) {
        if (context is PositionSelectedListener) {
            positionSelectedListener = context
        } else {
            throw IllegalArgumentException("${context::class.java.simpleName} must implement PositionSelectedListener")
        }
    }

    companion object {
        const val TAG = "DefaultPositionsDialogFragment"
        private const val PROJECT_ID = "project_id"
        private const val TIMESHEET_EMPLOYEE_ID = "timesheet_employee_id"

        @JvmStatic
        fun newInstance(timeSheetEmployeeId: Int, projectId: Int) =
            DefaultPositionsDialogFragment().apply {
                arguments = Bundle().apply {
                    putInt(PROJECT_ID, projectId)
                    putInt(TIMESHEET_EMPLOYEE_ID, timeSheetEmployeeId)
                }
            }
    }

    interface PositionSelectedListener {
        fun onPositionSelected(timeSheetEmployeeId: Int, positionId: Int)
    }
}