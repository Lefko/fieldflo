package com.fieldflo.screens.formTimesheet.timesheetEmployee

import java.util.*

sealed class TimeSheetEmployeeIntents {
    data class GetEmployeeDataIntent(val timeSheetEmployeeId: Int) : TimeSheetEmployeeIntents()

    data class SetTimeSheetEmployeeDefaultPositionIntent(
        val positionId: Int,
        val timeSheetEmployeeId: Int
    ) : TimeSheetEmployeeIntents()

    data class SetTimeSheetEmployeeDefaultPositionPhaseIntent(
        val timeSheetEmployeeId: Int,
        val phaseId: Int,
        val phaseCode: String,
        val phaseName: String
    ) : TimeSheetEmployeeIntents()

    data class PhaseDataIntent(val usesPhases: Boolean, val phaseCode: String) :
        TimeSheetEmployeeIntents()

    data class DeleteTimeSheetEmployeeIntent(val timeSheetEmployeeId: Int) : TimeSheetEmployeeIntents()

    data class StartTrackingEmployeeIntent(val timeSheetEmployeeId: Int) : TimeSheetEmployeeIntents()

    data class VerifyEmployeeIntent(
        val timeSheetEmployeeId: Int,
        val isInjured: Boolean,
        val tookFederalBreak: Boolean,
        val verifiedByEmployeeId: Int
    ) : TimeSheetEmployeeIntents()

    data class EditTimeEmployeeIntent(
        val projectId: Int,
        val internalFormId: Int,
        val timeSheetEmployeeId: Int,
        val pin: String,
        val startDate: Date?,
        val endDate: Date?,
        val workedTime: Int?,
        val breakDurationSeconds: Int?,
        val dontChangeStart: Boolean,
        val dontChangeEnd: Boolean,
        val dontChangeBreak: Boolean
    ) : TimeSheetEmployeeIntents()

    data class GetPhotoPinFileIntent(
        val projectId: Int,
        val internalFormId: Int,
        val timeSheetEmployeeId: Int
    ) :
        TimeSheetEmployeeIntents()

    data class DeletePhotoPinAfter30(val formDate: Date) : TimeSheetEmployeeIntents()

    data class DeleteAsset(val timeSheetEmployeeAssetId: Int) : TimeSheetEmployeeIntents()

    data class DeletePosition(
        val timeSheetEmployeeId: Int,
        val timeSheetEmployeePositionId: Int
    ) : TimeSheetEmployeeIntents()

    data class DeletePerDiem(val timeSheetEmployeeId: Int, val timeSheetEmployeePerDiemId: Int) :
        TimeSheetEmployeeIntents()

    data class GetIsActiveIntent(val internalFormId: Int) : TimeSheetEmployeeIntents()
}