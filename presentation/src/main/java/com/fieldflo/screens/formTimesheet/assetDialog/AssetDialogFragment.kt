package com.fieldflo.screens.formTimesheet.assetDialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.afterTextChanges
import com.fieldflo.common.safeClickListener
import com.fieldflo.di.injector
import kotlinx.android.synthetic.main.dialog_activity_add_assets.view.*

class AssetDialogFragment : DialogFragment() {

    private val timeSheetEmployeeAssetId by lazy { arguments?.getInt(TIME_SHEET_EMPLOYEE_ASSET_ID) }
    private val timeSheetEmployeeId by lazy { arguments!!.getInt(TIME_SHEET_EMPLOYEE_ID) }

    private lateinit var fragmentView: View
    private lateinit var positiveButton: Button

    private lateinit var assetsAdapter: AssetAdapter

    private var selectedAsset: AssetViewState.InventoryItemUiRow? = null

    private val positiveEnabled
        get() = selectedAsset != null && getWorkTimeAsset() > 0

    private val presenter by viewModels<AssetPresenter> { injector.assetViewModelFactory() }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        fragmentView = View.inflate(context, R.layout.dialog_activity_add_assets, null)

        assetsAdapter = AssetAdapter(context!!)

        val dialog = AlertDialog.Builder(activity as Context)
            .setView(fragmentView)
            .setPositiveButton(android.R.string.ok, null)
            .setNegativeButton(android.R.string.cancel) { _, _ -> dismiss() }
            .show()

        initView(fragmentView)

        return dialog
    }

    private fun initView(view: View) {
        view.dialog_add_asset_assets.apply {
            threshold = 1
            setAdapter(assetsAdapter)
            setOnItemClickListener { parent, _, position, _ ->
                selectedAsset =
                    parent.getItemAtPosition(position) as AssetViewState.InventoryItemUiRow
                positiveButton.isEnabled = positiveEnabled
            }
            afterTextChanges { _, _ ->
                selectedAsset = null
                positiveButton.isEnabled = positiveEnabled
            }
        }

        val timeSelections = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                positiveButton.isEnabled = positiveEnabled
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                positiveButton.isEnabled = positiveEnabled
            }
        }

        view.dialog_add_asset_hours.apply {
            adapter = ArrayAdapter.createFromResource(
                context!!,
                R.array.dialog_add_position_hours_list,
                R.layout.row_spinner_dropdown
            ).apply {
                setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            }
            onItemSelectedListener = timeSelections
        }
        view.dialog_add_asset_minutes.apply {
            adapter = ArrayAdapter.createFromResource(
                context!!,
                R.array.dialog_add_asset_minute_list,
                R.layout.row_spinner_dropdown
            ).apply {
                setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            }
            onItemSelectedListener = timeSelections
        }
        view.dialog_activity_add_asset_notes_edit_value.requestFocus()
    }

    override fun onStart() {
        super.onStart()
        positiveButton = (dialog as AlertDialog).getButton(Dialog.BUTTON_POSITIVE).apply {
            isEnabled = timeSheetEmployeeAssetId != 0
            safeClickListener {
                lifecycleScope.launchWhenStarted {
                    presenter.addEditAsset(
                        timeSheetEmployeeId,
                        timeSheetEmployeeAssetId,
                        selectedAsset!!.inventoryItemId,
                        getWorkTimeAsset(),
                        fragmentView.dialog_activity_add_asset_notes_edit_value.text.toString()
                    )
                    dismiss()
                }
            }
        }
        lifecycleScope.launchWhenCreated {
            val viewState = presenter.getInitialState(timeSheetEmployeeId, timeSheetEmployeeAssetId)
            render(viewState)
        }
    }

    private fun render(state: AssetViewState) {
        assetsAdapter.submit(state.assets)
        if (state.asset != null) {
            fragmentView.dialog_add_asset_titleTV.setText(R.string.dialog_activity_edit_assets_title)
            selectedAsset = AssetViewState.InventoryItemUiRow(
                state.asset.assetId,
                state.asset.assetName,
                state.asset.assetNumber
            )

            val currentName = fragmentView.dialog_add_asset_assets.text.toString()
            if (currentName != state.asset.assetName) {
                fragmentView.dialog_add_asset_assets.setText(state.asset.assetName)
                if (currentName.isEmpty()) {
                    fragmentView.dialog_add_asset_assets.requestFocus()
                }
                fragmentView.dialog_add_asset_assets.setSelection(state.asset.assetName.length)
            }

            val posHour = state.asset.totalWorkTime / 3600
            fragmentView.dialog_add_asset_hours.setSelection(posHour)
            if (state.asset.totalWorkTime % 3600 != 0) {
                val posMin = (state.asset.totalWorkTime - posHour * 3600) / 300
                fragmentView.dialog_add_asset_minutes.setSelection(posMin)
            }

            fragmentView.dialog_activity_add_asset_notes_edit_value.setText(state.asset.notes)
        } else {
            fragmentView.dialog_add_asset_assets.requestFocus()
        }

        positiveButton.isEnabled = positiveEnabled
    }

    private fun convertHourToSecond(position: Int): Int {
        return when (position) {
            0 -> 0
            1 -> 3600 // 1 hour
            2 -> 7200  // 2 hours
            3 -> 10800 // 3 hours
            4 -> 14400 // 4 hours
            5 -> 18000 // 5 hours
            6 -> 21600 // 6 hours
            7 -> 25200 // 7 hours
            8 -> 28800 // 8 hours
            9 -> 32400 // 9 hours
            10 -> 36000 // 10 hours
            11 -> 39600 // 11 hours
            12 -> 43200 // 12 hours
            13 -> 46800 // 13 hours
            14 -> 50400 // 14 hours
            15 -> 54000 // 15 hours
            16 -> 57600 // 16 hours
            17 -> 61200 // 17 hours
            18 -> 64800 // 18 hours
            19 -> 68400 // 19 hours
            20 -> 72000 // 20 hours
            21 -> 75600 // 21 hours
            22 -> 79200 // 22 hours
            23 -> 82800 // 23 hours

            else -> 0
        }
    }

    private fun convertMinuteToSecond(position: Int): Int {
        return when (position) {
            0 -> 0
            1 -> 300// 5 minutes
            2 -> 600// 10 minutes
            3 -> 900// 15 minutes
            4 -> 1200// 20 minutes
            5 -> 1500// 25 minutes
            6 -> 1800// 30 minutes
            7 -> 2100// 35 minutes
            8 -> 2400// 40 minutes
            9 -> 2700// 45 minutes
            10 -> 3000// 50 minutes
            11 -> 3300// 55 minutes

            else -> 0
        }
    }

    private fun getWorkTimeAsset(): Int {
        val hours = convertHourToSecond(fragmentView.dialog_add_asset_hours.selectedItemPosition)
        val minutes =
            convertMinuteToSecond(fragmentView.dialog_add_asset_minutes.selectedItemPosition)
        return hours + minutes
    }

    companion object {
        const val TAG = "AssetDialogFragment"
        private const val TIME_SHEET_EMPLOYEE_ASSET_ID = "time_sheet_employee_asset_id"
        private const val TIME_SHEET_EMPLOYEE_ID = "time_sheet_employee_id"

        fun newInstance(timeSheetEmployeeId: Int) = AssetDialogFragment().apply {
            arguments = Bundle().apply {
                putInt(TIME_SHEET_EMPLOYEE_ID, timeSheetEmployeeId)
            }
        }

        fun newInstance(timeSheetEmployeeId: Int, timeSheetEmployeeAssetId: Int) =
            AssetDialogFragment().apply {
                arguments = Bundle().apply {
                    putInt(TIME_SHEET_EMPLOYEE_ID, timeSheetEmployeeId)
                    putInt(TIME_SHEET_EMPLOYEE_ASSET_ID, timeSheetEmployeeAssetId)
                }
            }
    }
}