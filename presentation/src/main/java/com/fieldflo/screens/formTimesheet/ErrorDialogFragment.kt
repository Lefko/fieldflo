package com.fieldflo.screens.formTimesheet

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.fragment.app.DialogFragment

class ErrorDialogFragment : DialogFragment() {

    private val timeSheetEmployeeNames by lazy {
        arguments!!.getStringArray(TIMESHEET_EMPLOYEE_NAMES)!!
    }

    private val message by lazy {
        arguments!!.getInt(MESSAGE_INT)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val employeeNames = this.timeSheetEmployeeNames.joinToString()
        val messageString = getString(message, employeeNames)

        return AlertDialog.Builder(activity as Context)
            .setMessage(messageString)
            .setPositiveButton(android.R.string.ok) { _, _ -> }
            .show()
    }

    companion object {
        const val TAG = "ErrorDialogFragment"
        private const val MESSAGE_INT = "message_int"
        private const val TIMESHEET_EMPLOYEE_NAMES = "timesheet_employee_names"
        @JvmStatic
        fun newInstance(timeSheetEmployeeNames: Array<String>, @StringRes message: Int) =
            ErrorDialogFragment().apply {
                arguments = Bundle().apply {
                    putStringArray(TIMESHEET_EMPLOYEE_NAMES, timeSheetEmployeeNames)
                    putInt(MESSAGE_INT, message)
                }
            }
    }
}