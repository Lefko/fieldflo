package com.fieldflo.screens.formTimesheet.phases

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector

class PhasesDialogFragment : DialogFragment() {

    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }
    private val timeSheetEmployeeId by lazy { arguments!!.getInt(TIMESHEET_EMPLOYEE_ID) }
    private val phasesAdapter by lazy { PhasesAdapter(context!!) }

    private val presenter by viewModels<PhasesPresenter> { injector.phasesViewModelFactory() }

    private var selectedPhase: PhaseUiRow? = null
    private lateinit var phaseSelectedListener: PhaseSelectedListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = AlertDialog.Builder(context)
            .setTitle(R.string.activity_daily_timesheet_dialog_add_phases_title)
            .setSingleChoiceItems(phasesAdapter, -1, { _, selectedPos ->
                selectedPhase = phasesAdapter.getItem(selectedPos)
            })
            .setPositiveButton(android.R.string.ok, { _, _ ->
                if (selectedPhase == null) {
                    dismiss()
                } else {
                    val phaseId = selectedPhase!!.phaseId
                    phaseSelectedListener.onPhaseSelected(timeSheetEmployeeId, phaseId)
                }
            })
            .setNegativeButton(android.R.string.cancel, { _, _ ->
                dismiss()
            })
            .show()

        lifecycleScope.launchWhenCreated {
            render(presenter.loadPhases(projectId))
        }

        return dialog
    }

    private fun render(phases: List<PhaseUiRow>) {
        phasesAdapter.clear()
        phasesAdapter.addAll(phases)
        phasesAdapter.notifyDataSetChanged()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    private fun onAttachToContext(context: Context) {
        phaseSelectedListener = context as? PhaseSelectedListener
            ?: throw IllegalArgumentException("${context::class.java.simpleName} must implement PhaseSelectedListener")
    }

    companion object {
        const val TAG = "PhasesDialogFragment"
        private const val PROJECT_ID = "project_id"
        private const val TIMESHEET_EMPLOYEE_ID = "timesheet_employee_id"

        @JvmStatic
        fun newInstance(timeSheetEmployeeId: Int, projectId: Int) =
            PhasesDialogFragment().apply {
                arguments = Bundle().apply {
                    putInt(PROJECT_ID, projectId)
                    putInt(TIMESHEET_EMPLOYEE_ID, timeSheetEmployeeId)
                }
            }
    }

    interface PhaseSelectedListener {
        fun onPhaseSelected(timeSheetEmployeeId: Int, phaseId: Int)
    }
}
