package com.fieldflo.screens.formTimesheet.assetDialog

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.InventoryItem
import com.fieldflo.usecases.TimeSheetEmployeeAssetData
import com.fieldflo.usecases.TimeSheetUseCases
import javax.inject.Inject

class AssetPresenter @Inject constructor(
    private val timeSheetUseCases: TimeSheetUseCases
) : ViewModel() {

    suspend fun getInitialState(
        timeSheetEmployeeId: Int,
        timeSheetEmployeeAssetId: Int?
    ): AssetViewState {
        val assetData = timeSheetUseCases.getInventoryItemsForTimeSheetAsset(
            timeSheetEmployeeId,
            timeSheetEmployeeAssetId
        )
        val assets = assetData.allItems.map { it.toUiModel() }
        val employeeAsset = assetData.employeeAsset.toUiModel()
        return AssetViewState(
            assets = assets,
            asset = employeeAsset
        )
    }

    suspend fun addEditAsset(
        timeSheetEmployeeId: Int,
        timeSheetEmployeeAssetId: Int?,
        inventoryItemId: Int,
        totalWorkTimeSeconds: Int,
        notes: String
    ) {
        if (timeSheetEmployeeAssetId == null || timeSheetEmployeeAssetId == 0) {
            timeSheetUseCases.addTimeSheetEmployeeAsset(
                timeSheetEmployeeId,
                inventoryItemId,
                totalWorkTimeSeconds,
                notes
            )
        } else {
            timeSheetUseCases.editTimeSheetEmployeeAsset(
                timeSheetEmployeeId,
                timeSheetEmployeeAssetId,
                inventoryItemId,
                totalWorkTimeSeconds,
                notes
            )
        }
    }
}

private fun InventoryItem.toUiModel(): AssetViewState.InventoryItemUiRow {
    return AssetViewState.InventoryItemUiRow(
        inventoryItemId = this.inventoryItemId,
        inventoryItemName = this.inventoryItemName,
        inventoryItemNumber = this.inventoryItemNumber
    )
}

private fun TimeSheetEmployeeAssetData?.toUiModel(): AssetViewState.TimeSheetAssetUiModel? {
    return this?.let {
        AssetViewState.TimeSheetAssetUiModel(
            timeSheetEmployeeAssetId = it.timeSheetEmployeeAssetId,
            assetName = it.assetName,
            assetNumber = it.assetNumber,
            totalWorkTime = it.totalWorkTime,
            assetId = it.assetId,
            notes = it.notes
        )
    }
}

