package com.fieldflo.screens.formTimesheet.timesheetEmployee

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.Group
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.fieldflo.R
import com.fieldflo.common.*
import com.fieldflo.di.injector
import com.fieldflo.screens.drawer.DrawerManager
import com.fieldflo.screens.formTimesheet.ErrorDialogFragment
import com.fieldflo.screens.formTimesheet.TimeSheetEmployeeState
import com.fieldflo.screens.formTimesheet.addEditAdditionalPositionDialog.AdditionalPositionDialogFragment
import com.fieldflo.screens.formTimesheet.addEditDefaultPositionDialog.DefaultPositionsDialogFragment
import com.fieldflo.screens.formTimesheet.assetDialog.AssetDialogFragment
import com.fieldflo.screens.formTimesheet.confirmStartTracking.ConfirmStartTrackingDialogFragment
import com.fieldflo.screens.formTimesheet.delete.DeleteTimeSheetEmployeeDialogFragment
import com.fieldflo.screens.formTimesheet.editTime.EditTimeDialogFragment
import com.fieldflo.screens.formTimesheet.perDiemDialog.PerDiemDialogFragment
import com.fieldflo.screens.formTimesheet.phases.PhasesDialogFragment
import com.fieldflo.screens.formTimesheet.stopTrackingSingleEmployee.StopTrackingSingleEmployeeDialogFragment
import com.fieldflo.screens.formTimesheet.verify.VerifySingleEmployeeDialogFragment
import com.fieldflo.usecases.ProjectStatus
import kotlinx.android.synthetic.main.view_asset_row.view.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import java.io.File

class TimeSheetEmployeeActivity : AppCompatActivity(),
    DefaultPositionsDialogFragment.PositionSelectedListener,
    PhasesDialogFragment.PhaseSelectedListener,
    DeleteTimeSheetEmployeeDialogFragment.DeleteTimeSheetEmployeeConfirmedListener,
    ConfirmStartTrackingDialogFragment.StartTrackingConfirmedListener,
    VerifySingleEmployeeDialogFragment.VerifyDialogCallback,
    EditTimeDialogFragment.EditTimeListener {

    private lateinit var assetsContainerLL: LinearLayout
    private lateinit var positionContainerLL: LinearLayout
    private lateinit var perDiemContainerLL: LinearLayout
    private lateinit var timerState: TextView
    private lateinit var backTV: TextView
    private lateinit var trash: ImageView
    private lateinit var workerPhoto: ImageView
    private lateinit var verificationPhoto: ImageView
    private lateinit var deletingDate: TextView
    private lateinit var contentDate: TextView
    private lateinit var workerName: TextView
    private lateinit var positionValue: TextView
    private var phaseValue: TextView? = null
    private lateinit var positionEdit: ImageView
    private var phaseEdit: ImageView? = null
    private lateinit var startTimeEdit: ImageView
    private lateinit var endTimeEdit: ImageView
    private lateinit var actionBottomBtn: TextView
    private lateinit var startTime: TextView
    private lateinit var endTime: TextView
    private lateinit var workedTime: TextView
    private lateinit var breakTime: TextView
    private lateinit var injuredIcon: ImageView
    private lateinit var federalBreakIcon: ImageView
    private lateinit var timeIcon: ImageView
    private lateinit var changeLogIcon: ImageView
    private lateinit var verificationTimeValue: TextView
    private lateinit var verificationInjuredValue: TextView
    private lateinit var verificationFederalBreakValue: TextView
    private lateinit var verificationChangeLogValue: TextView
    private lateinit var verificationTitle: TextView
    private lateinit var verificationTimeStatus: TextView
    private lateinit var verificationInjuredStatus: TextView
    private lateinit var verificationFederalBreakStatus: TextView
    private lateinit var verificationChangeLogStatus: TextView
    private lateinit var verificationStatusIcon: ImageView
    private lateinit var positionDefault: TextView
    private lateinit var positionTimeDefault: TextView
    private lateinit var perDiemGroup: Group
    private lateinit var perDiemAddBtn: ImageView
    private lateinit var positionAddBtn: ImageView
    private lateinit var assetAddBtn: ImageView
    private lateinit var editDefaultPositionBtn: ImageView
    private lateinit var imageVerificationGroup: Group
    private lateinit var positionDefaultGroup: Group

    private val glideOptions =
        RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL).transform(RoundedCorners(10))

    companion object {
        private const val TIMESHEET_EMPLOYEE_ID = "time_sheet_employee_id"
        private const val USE_PHASES = "use_phases"
        private const val PROJECT_ID_KEY = "project_id"
        private const val INTERNAL_FORM_ID = "internal_form_id"

        fun start(
            sender: Activity,
            timeSheetEmployeeId: Int,
            projectId: Int,
            usePhases: Boolean,
            internalFormId: Int
        ) {
            val intent = Intent(sender, TimeSheetEmployeeActivity::class.java).apply {
                putExtra(TIMESHEET_EMPLOYEE_ID, timeSheetEmployeeId)
                putExtra(USE_PHASES, usePhases)
                putExtra(PROJECT_ID_KEY, projectId)
                putExtra(INTERNAL_FORM_ID, internalFormId)
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }

            sender.startActivity(intent)
        }
    }

    private val drawerManager by lazy {
        DrawerManager(
            { findViewById(R.id.drawer_layout) }, { this },
            { findViewById(R.id.activity_worker_details_content_toolbar) }
        )
    }

    private val timeSheetEmployeeId by lazy { intent!!.getIntExtra(TIMESHEET_EMPLOYEE_ID, 0) }
    private val usePhases by lazy { intent!!.getBooleanExtra(USE_PHASES, false) }
    private val projectId: Int by lazy { intent!!.getIntExtra(PROJECT_ID_KEY, 0) }
    private val internalFormId: Int by lazy { intent!!.getIntExtra(INTERNAL_FORM_ID, 0) }

    private val presenter by viewModels<TimeSheetEmployeePresenter> { injector.timeSheetEmployeeViewModelFactory() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(if (usePhases) R.layout.activity_worker_details_phase else R.layout.activity_worker_details_no_phase)

        lifecycleScope.launchWhenCreated {
            presenter.getProjectStatusFlow(projectId)
                .filter { it == ProjectStatus.DELETED }
                .collect { finish() }
        }

        findViews()

        initListeners()
        present()
    }

    private fun findViews() {
        timerState = findViewById(R.id.activity_worker_details_content_timer_state)
        trash = findViewById(R.id.activity_worker_details_content_trash_can_icon)
        backTV = findViewById(R.id.activity_worker_details_content_backTV)
        workerPhoto = findViewById(R.id.activity_worker_details_content_worker_photo)
        contentDate = findViewById(R.id.activity_worker_details_content_date)
        workerName = findViewById(R.id.activity_worker_details_content_worker_name)
        positionValue = findViewById(R.id.activity_worker_details_content_position_value)
        phaseValue =
            if (usePhases) findViewById(R.id.activity_worker_details_content_phase_value) else null
        positionEdit = findViewById(R.id.activity_worker_details_content_position_edit_icon)
        phaseEdit =
            if (usePhases) findViewById(R.id.activity_worker_details_content_phase_edit_icon) else null
        startTime = findViewById(R.id.activity_worker_details_content_time_details_start_value)
        endTime = findViewById(R.id.activity_worker_details_content_time_details_end_value)
        workedTime = findViewById(R.id.activity_worker_details_content_time_details_worked_value)
        breakTime = findViewById(R.id.activity_worker_details_content_time_details_break_value)
        startTimeEdit =
            findViewById(R.id.activity_worker_details_content_time_details_start_edit_icon)
        endTimeEdit = findViewById(R.id.activity_worker_details_content_time_details_end_edit_icon)
        actionBottomBtn = findViewById(R.id.activity_worker_details_content_timer_actionTV)

        injuredIcon = findViewById(R.id.activity_worker_details_content_verification_injured_icon)
        federalBreakIcon =
            findViewById(R.id.activity_worker_details_content_verification_federal_break_icon)
        timeIcon = findViewById(R.id.activity_worker_details_content_verification_time_icon)
        changeLogIcon =
            findViewById(R.id.activity_worker_details_content_verification_change_log_icon)

        verificationTitle = findViewById(R.id.activity_worker_details_content_verification_title)
        verificationStatusIcon =
            findViewById(R.id.activity_worker_details_content_verification_status_icon)

        verificationTimeStatus =
            findViewById(R.id.activity_worker_details_content_verification_time_status)
        verificationInjuredStatus =
            findViewById(R.id.activity_worker_details_content_verification_injured_status)
        verificationFederalBreakStatus =
            findViewById(R.id.activity_worker_details_content_verification_federal_break_status)
        verificationChangeLogStatus =
            findViewById(R.id.activity_worker_details_content_verification_change_log_status)

        verificationTimeValue =
            findViewById(R.id.activity_worker_details_content_verification_time_value)
        verificationInjuredValue =
            findViewById(R.id.activity_worker_details_content_verification_injured_value)
        verificationFederalBreakValue =
            findViewById(R.id.activity_worker_details_content_verification_federal_break_value)
        verificationChangeLogValue =
            findViewById(R.id.activity_worker_details_content_verification_change_log_value)

        verificationPhoto =
            findViewById(R.id.activity_worker_details_content_image_verification_photo)
        deletingDate =
            findViewById(R.id.activity_worker_details_content_image_verification_deleting_date_value)

        positionDefault =
            findViewById(R.id.activity_worker_details_content_positions_position_title_1)
        positionTimeDefault =
            findViewById(R.id.activity_worker_details_content_positions_position_time_value_1)
        perDiemAddBtn = findViewById(R.id.activity_worker_details_content_per_diem_add_icon)
        perDiemGroup = findViewById(R.id.activity_worker_details_content_per_diem_group)
        positionAddBtn = findViewById(R.id.activity_worker_details_content_positions_add_icon)
        assetAddBtn = findViewById(R.id.activity_worker_details_content_asset_tracker_add_icon)
        editDefaultPositionBtn =
            findViewById(R.id.activity_worker_details_content_positions_position_edit_icon)

        assetsContainerLL = findViewById(R.id.activity_worker_details_content_assets_container)
        positionContainerLL = findViewById(R.id.activity_worker_details_content_positions_container)
        perDiemContainerLL = findViewById(R.id.activity_worker_details_content_per_diem_container)

        imageVerificationGroup =
            findViewById(R.id.activity_worker_details_content_image_verification_group)
        positionDefaultGroup =
            findViewById(R.id.activity_worker_details_content_position_default_group)
    }

    override fun onBackPressed() {
        if (!drawerManager.onBackPress()) {
            super.onBackPressed()
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawerManager.onConfigChange(newConfig)
    }

    private fun initListeners() {
        backTV.safeClickListener { finish() }

        val listener = View.OnClickListener {
            DefaultPositionsDialogFragment.newInstance(timeSheetEmployeeId, projectId)
                .show(supportFragmentManager, DefaultPositionsDialogFragment.TAG)
        }
        positionEdit.safeClickListener(clickListener = listener)
        editDefaultPositionBtn.safeClickListener(clickListener = listener)

        phaseEdit?.let {
            it.safeClickListener {
                PhasesDialogFragment.newInstance(timeSheetEmployeeId, projectId)
                    .show(supportFragmentManager, PhasesDialogFragment.TAG)
            }
        }

        startTimeEdit.safeClickListener {
            EditTimeDialogFragment.newInstance(
                projectId,
                internalFormId,
                timeSheetEmployeeIds = intArrayOf(timeSheetEmployeeId)
            )
                .show(supportFragmentManager, EditTimeDialogFragment.TAG)
        }

        endTimeEdit.safeClickListener {
            EditTimeDialogFragment.newInstance(
                projectId,
                internalFormId,
                timeSheetEmployeeIds = intArrayOf(timeSheetEmployeeId)
            ).show(supportFragmentManager, EditTimeDialogFragment.TAG)
        }
    }

    fun present() {
        lifecycleScope.launchWhenCreated {
            findViewById<TextView>(R.id.activity_worker_details_content_project_nameTV).text =
                presenter.getProjectName(projectId)
        }

        lifecycleScope.launchWhenCreated {
            presenter.getTimeSheetEmployeeFlow(timeSheetEmployeeId).collect { render(it) }
        }

    }


    private fun render(viewState: TimeSheetEmployeeViewState) {
        if (!trash.hasOnClickListeners()) {
            trash.safeClickListener {
                DeleteTimeSheetEmployeeDialogFragment.newInstance(
                    timeSheetEmployeeId,
                    viewState.employeeFullName
                )
                    .show(supportFragmentManager, DeleteTimeSheetEmployeeDialogFragment.TAG)
            }
        }

        contentDate.text = viewState.formDate
        workerName.text = viewState.employeeFullName

        Glide.with(this)
            .load(viewState.timeSheetEmployeeImageUrl)
            .centerCrop()
            .apply(glideOptions)
            .placeholder(R.drawable.app_icon)
            .into(workerPhoto)

        // Position
        renderPosition(viewState)

        // Phases
        renderPhases(viewState)

        startTime.text = viewState.startTime
        endTime.text = viewState.endTime
        startTimeEdit.isEnabled = viewState.editTimeEnabled
        endTimeEdit.isEnabled = viewState.editTimeEnabled

        workedTime.text = viewState.totalWorkTime
        breakTime.text = viewState.breakTime

        when (viewState.employeeState) {
            TimeSheetEmployeeState.NOT_READY -> renderNotReady(viewState)
            TimeSheetEmployeeState.READY -> renderReady(viewState.employeeFullName)
            TimeSheetEmployeeState.TRACKING -> renderTracking(viewState.employeeId)
            TimeSheetEmployeeState.MUST_VERIFY -> renderMustVerify(viewState.employeeId)
            TimeSheetEmployeeState.VERIFIED -> renderVerified(viewState)
            TimeSheetEmployeeState.VERIFIED_WITH_INJURY -> renderVerifiedWithInjury(viewState)
        }

        verificationChangeLogStatus.setText(
            if (viewState.timeLogs.isNotEmpty()) {
                R.string.activity_worker_details_verification_status_for_log
            } else {
                R.string.activity_worker_details_verification_status_default_for_log
            }
        )

        verificationChangeLogValue.text = viewState.lastTimeLogChangeDate

        setDefaultPosition(viewState)
        addPositionRows(viewState.otherPositions)

        addAssetRow(viewState.assets)
        if (!assetAddBtn.hasOnClickListeners()) {
            assetAddBtn.safeClickListener {
                AssetDialogFragment.newInstance(timeSheetEmployeeId)
                    .show(supportFragmentManager, AssetDialogFragment.TAG)
            }
        }

        if (viewState.showPerDiem) {
            perDiemGroup.visible()
            if (viewState.perDiem != null) {
                perDiemAddBtn.gone()
                addPerDiemRow(viewState.perDiem)
            } else {
                perDiemContainerLL.removeAllViews()
                perDiemAddBtn.visible()
                if (!perDiemAddBtn.hasOnClickListeners()) {
                    perDiemAddBtn.safeClickListener {
                        PerDiemDialogFragment.newInstance(timeSheetEmployeeId)
                            .show(supportFragmentManager, PerDiemDialogFragment.TAG)
                    }
                }
            }
        } else {
            perDiemGroup.gone()
        }

        val pinImageFile = File(viewState.pinImageLocation)
        if (viewState.pinImageLocation.isEmpty() || !pinImageFile.exists()) {
            imageVerificationGroup.gone()
        } else {
            imageVerificationGroup.visible()
            Glide.with(this)
                .load(viewState.pinImageLocation)
                .centerCrop()
                .apply(glideOptions)
                .placeholder(R.drawable.app_icon)
                .into(verificationPhoto)
            deletingDate.text = viewState.pinImageDeletingOn
        }
    }

    private fun renderTracking(employeeId: Int) {
        actionBottomBtn.drawableTop(R.drawable.selector_ic_stop_timer)
        timerState.drawableTop(R.drawable.ic_clock_green)
        timerState.text = getText(R.string.activity_daily_timesheet_row_tracking)
        timerState.textColor(R.color.for_tracking)
        actionBottomBtn.text = getText(R.string.activity_daily_timesheet_stop)
        actionBottomBtn.colorStateList(R.color.selector_clickable_text_color)
        val listener = View.OnClickListener {
            StopTrackingSingleEmployeeDialogFragment.newInstance(
                projectId,
                internalFormId,
                timeSheetEmployeeId,
                employeeId
            ).show(supportFragmentManager, StopTrackingSingleEmployeeDialogFragment.TAG)
        }
        timerState.safeClickListener(clickListener = listener)
        actionBottomBtn.safeClickListener(clickListener = listener)
        verificationStatusIcon.visibility = View.GONE
        perDiemAddBtn.isEnabled = false
        positionAddBtn.isEnabled = false
        assetAddBtn.isEnabled = false
    }

    private fun renderVerified(viewState: TimeSheetEmployeeViewState) {
        actionBottomBtn.drawableTop(R.drawable.ic_verified)
        timerState.drawableTop(R.drawable.ic_verified)
        timerState.text = getText(R.string.activity_daily_timesheet_row_verified)
        timerState.textColor(R.color.for_verified_injured)
        actionBottomBtn.text = getText(R.string.activity_daily_timesheet_row_verified)
        actionBottomBtn.textColor(R.color.for_verified_injured)
        timerState.setOnClickListener(null)
        actionBottomBtn.setOnClickListener(null)
        verificationTitle.textColor(R.color.for_verified_injured)
        verificationTimeStatus.textColor(R.color.for_verified_injured)
        verificationInjuredStatus.textColor(R.color.for_verified_injured)
        verificationFederalBreakStatus.textColor(R.color.for_verified_injured)
        verificationStatusIcon.setImageResource(R.drawable.ic_verification_status_verified)
        verificationStatusIcon.visible()
        verificationTimeValue.text = viewState.timeVerifiedDate
        verificationInjuredValue.text = viewState.injuryVerifiedDate
        verificationFederalBreakValue.text = viewState.breakVerifiedDate
        verificationTimeStatus.text = viewState.timeVerifiedBy
        verificationInjuredStatus.text = viewState.injuryVerifiedBy
        verificationFederalBreakStatus.text = viewState.breakVerifiedBy
        timeIcon.setImageResource(R.drawable.ic_checkmark_checked)
        injuredIcon.setImageResource(R.drawable.ic_verification_status_no)
        federalBreakIcon.setImageResource(if (viewState.tookBreak) R.drawable.ic_checkmark_checked else R.drawable.ic_verification_status_no)
        changeLogIcon.setImageResource(R.drawable.ic_checkmark_checked)
        perDiemAddBtn.isEnabled = true
        positionAddBtn.isEnabled = true
        assetAddBtn.isEnabled = true
    }

    private fun renderVerifiedWithInjury(viewState: TimeSheetEmployeeViewState) {
        actionBottomBtn.drawableTop(R.drawable.ic_verified_injured)
        timerState.drawableTop(R.drawable.ic_verified_injured)
        timerState.text = getText(R.string.activity_daily_timesheet_row_verified)
        timerState.textColor(R.color.for_verified_injured)
        actionBottomBtn.text = getText(R.string.activity_daily_timesheet_row_verified)
        actionBottomBtn.textColor(R.color.for_verified_injured)
        timerState.setOnClickListener(null)
        actionBottomBtn.setOnClickListener(null)
        verificationStatusIcon.setImageResource(R.drawable.ic_verification_status_verified)
        verificationStatusIcon.visible()
        verificationTimeValue.text = viewState.timeVerifiedDate
        verificationInjuredValue.text = viewState.injuryVerifiedDate
        verificationFederalBreakValue.text = viewState.breakVerifiedDate
        verificationTimeStatus.text = viewState.timeVerifiedBy
        verificationTitle.textColor(R.color.for_verified_injured)
        verificationTimeStatus.textColor(R.color.for_verified_injured)
        verificationInjuredStatus.textColor(R.color.for_verified_injured)
        verificationFederalBreakStatus.textColor(R.color.for_verified_injured)
        verificationInjuredStatus.text = viewState.injuryVerifiedBy
        verificationFederalBreakStatus.text = viewState.breakVerifiedBy
        timeIcon.setImageResource(R.drawable.ic_checkmark_checked)
        injuredIcon.setImageResource(R.drawable.ic_verification_status_injured)
        federalBreakIcon.setImageResource(if (viewState.tookBreak) R.drawable.ic_checkmark_checked else R.drawable.ic_verification_status_no)
        changeLogIcon.setImageResource(R.drawable.ic_checkmark_checked)
        perDiemAddBtn.isEnabled = true
        positionAddBtn.isEnabled = true
        assetAddBtn.isEnabled = true
    }

    private fun renderMustVerify(employeeId: Int) {
        actionBottomBtn.drawableTop(R.drawable.ic_verification_status_verify)
        verificationStatusIcon.setImageResource(R.drawable.ic_verification_status_verify)
        verificationStatusIcon.visibility = View.VISIBLE
        timerState.drawableTop(R.drawable.ic_verify)
        timerState.text = getText(R.string.activity_daily_timesheet_row_verify)
        timerState.textColor(R.color.for_verify)
        actionBottomBtn.text = getText(R.string.activity_daily_timesheet_row_verify)
        actionBottomBtn.textColor(R.color.for_verify)
        val listener = View.OnClickListener {
            VerifySingleEmployeeDialogFragment.newInstance(
                internalFormId,
                timeSheetEmployeeId,
                employeeId
            ).show(supportFragmentManager, VerifySingleEmployeeDialogFragment.TAG)
        }
        timerState.safeClickListener(clickListener = listener)
        actionBottomBtn.safeClickListener(clickListener = listener)
        verificationTitle.textColor(R.color.for_verify)
        verificationTimeStatus.textColor(R.color.for_verify)
        verificationInjuredStatus.textColor(R.color.for_verify)
        verificationFederalBreakStatus.textColor(R.color.for_verify)
        perDiemAddBtn.isEnabled = true
        positionAddBtn.isEnabled = true
        assetAddBtn.isEnabled = true
    }

    private fun renderReady(timeSheetEmployeeName: String) {
        actionBottomBtn.drawableTop(R.drawable.selector_ic_start_timer)
        timerState.drawableTop(R.drawable.ic_clock_grey)
        timerState.text = getText(R.string.activity_daily_timesheet_row_start)
        actionBottomBtn.text = getText(R.string.activity_daily_timesheet_row_start)
        timerState.textColor(R.color.for_start)
        actionBottomBtn.colorStateList(R.color.selector_clickable_text_color)
        val listener = View.OnClickListener {
            ConfirmStartTrackingDialogFragment.newInstance(
                timeSheetEmployeeId,
                timeSheetEmployeeName
            ).show(supportFragmentManager, ConfirmStartTrackingDialogFragment.TAG)
        }
        timerState.safeClickListener(clickListener = listener)
        actionBottomBtn.safeClickListener(clickListener = listener)
        verificationStatusIcon.visibility = View.GONE
        perDiemAddBtn.isEnabled = false
        positionAddBtn.isEnabled = false
        assetAddBtn.isEnabled = false
    }

    private fun renderNotReady(viewState: TimeSheetEmployeeViewState) {
        actionBottomBtn.drawableTop(R.drawable.selector_ic_start_timer)
        timerState.drawableTop(R.drawable.ic_clock_grey_translucent)
        timerState.text = getText(R.string.activity_daily_timesheet_row_start)
        timerState.textColor(R.color.for_start_translucent)
        actionBottomBtn.text = getText(R.string.activity_daily_timesheet_row_start)
        actionBottomBtn.textColor(R.color.for_start_translucent)
        val listener = View.OnClickListener {
            val positionMissing = viewState.defaultPositionName.isEmpty()
            val phaseMissing = viewState.phaseName.isEmpty() && viewState.usePhases
            val textRes: Int = when {
                positionMissing && phaseMissing -> R.string.activity_time_sheet_position_and_phase_missing
                phaseMissing -> R.string.activity_time_sheet_phase_missing
                positionMissing -> R.string.activity_time_sheet_position_missing
                viewState.isTracking -> R.string.activity_time_sheet_tracking_somewhere
                else -> -1
            }
            if (textRes != -1) {
                toast(textRes)
            }
        }
        timerState.safeClickListener(clickListener = listener)
        actionBottomBtn.safeClickListener(clickListener = listener)
        verificationStatusIcon.visibility = View.GONE
        perDiemAddBtn.isEnabled = false
        positionAddBtn.isEnabled = false
        assetAddBtn.isEnabled = false
    }

    private fun renderPosition(viewState: TimeSheetEmployeeViewState) {
        positionEdit.setImageResource(R.drawable.selector_ic_edit_small)
        if (viewState.defaultPositionName.isEmpty()) {
            positionValue.text = getString(R.string.activity_daily_timesheet_not_selected)
            positionValue.textColor(R.color.attention)
        } else {
            positionValue.text = viewState.defaultPositionName
            positionValue.textColor(R.color.text_color)
        }

    }

    private fun renderPhases(viewState: TimeSheetEmployeeViewState) {
        if (usePhases) {
            phaseEdit?.isEnabled = viewState.editPhaseEnabled
            if (viewState.editPhaseEnabled) {
                phaseEdit?.setImageResource(R.drawable.selector_ic_edit_small)
                if (viewState.phaseName.isEmpty()) {
                    phaseValue?.setText(R.string.activity_daily_timesheet_not_selected)
                    phaseValue?.textColor(R.color.attention)
                } else {
                    phaseValue?.text = viewState.phaseName
                    phaseValue?.textColor(R.color.text_color)
                }
            } else {
                phaseValue?.text = viewState.phaseName
                phaseValue?.textColor(R.color.disabled_icon)
                phaseEdit?.setImageResource(R.drawable.ic_edit_grey_translucent)
            }
        }
    }

    override fun onPositionSelected(timeSheetEmployeeId: Int, positionId: Int) {
        presenter.setEmployeePosition(timeSheetEmployeeId, positionId)
    }

    override fun onPhaseSelected(timeSheetEmployeeId: Int, phaseId: Int) {
        presenter.setEmployeePhase(timeSheetEmployeeId, phaseId)
    }

    override fun onDeleteTimeSheetEmployeeConfirmed(timeSheetEmployeeId: Int) {
        presenter.removeEmployee(timeSheetEmployeeId)
        finish()
    }

    override fun onStartTrackingConfirmed(timeSheetEmployeeId: Int) {
        presenter.startTracking(timeSheetEmployeeId)
    }

    override fun onVerifyTracking(
        timeSheetEmployeeId: Int,
        isInjured: Boolean,
        tookFederalBreak: Boolean,
        verifiedByEmployeeId: Int
    ) {
        presenter.verifyEmployee(
            timeSheetEmployeeId,
            isInjured,
            tookFederalBreak,
            verifiedByEmployeeId
        )
    }

    override fun onEditFailed(timeSheetEmployeeInfo: List<String>) {
        ErrorDialogFragment.newInstance(
            timeSheetEmployeeInfo.toTypedArray(),
            R.string.activity_time_sheet_time_edit_error
        ).show(supportFragmentManager, ErrorDialogFragment.TAG)
    }

    private fun setDefaultPosition(viewState: TimeSheetEmployeeViewState) {
        val defaultPosition = viewState.defaultPosition
        if (defaultPosition != null) {
            positionDefaultGroup.visibility = View.VISIBLE
            positionDefault.text = defaultPosition.positionName
            positionTimeDefault.text = defaultPosition.totalWorkTimeString
            positionAddBtn.safeClickListener {
                AdditionalPositionDialogFragment.newInstance(
                    projectId,
                    timeSheetEmployeeId,
                    usePhases
                ).show(supportFragmentManager, AdditionalPositionDialogFragment.TAG)
            }
        } else {
            positionDefaultGroup.visibility = View.GONE
        }
    }

    private fun addAssetRow(list: List<TimeSheetEmployeeViewState.TimeSheetAssetEntityUiModel>) {
        assetsContainerLL.removeAllViews()
        for (item in list) {
            val newItem = layoutInflater.inflate(R.layout.view_asset_row, assetsContainerLL, false)
            val assetText = item.assetName + (if (item.assetNumber != 0) {
                " / ${item.assetNumber}"
            } else {
                ""
            })
            newItem.activity_worker_details_content_asset_tracker_value.text = assetText
            newItem.activity_worker_details_content_asset_tracker_time_value.text =
                item.totalWorkTime
            newItem.activity_worker_details_content_asset_tracker_delete_icon.safeClickListener {
                presenter.deleteAsset(item.timeSheetEmployeeAssetId)
            }

            newItem.activity_worker_details_content_asset_tracker_details_icon.safeClickListener {
                AssetDialogFragment.newInstance(
                    timeSheetEmployeeId,
                    item.timeSheetEmployeeAssetId
                ).show(supportFragmentManager, AssetDialogFragment.TAG)
            }

            assetsContainerLL.addView(newItem)
        }
    }

    private fun addPositionRows(otherPositions: List<TimeSheetEmployeeViewState.TimeSheetEmployeePositionUiModel>) {
        positionContainerLL.removeAllViews()
        for (item in otherPositions) {
            val newItem =
                layoutInflater.inflate(R.layout.view_asset_row, positionContainerLL, false)

            newItem.activity_worker_details_content_asset_tracker_value.text = item.positionName
            newItem.activity_worker_details_content_asset_tracker_time_value.text =
                item.totalWorkTimeString
            newItem.activity_worker_details_content_asset_tracker_delete_icon.safeClickListener {
                presenter.deletePosition(item.timeSheetEmployeeId, item.timeSheetEmployeePositionId)
            }

            newItem.activity_worker_details_content_asset_tracker_details_icon.safeClickListener {
                AdditionalPositionDialogFragment.newInstance(
                    projectId,
                    item.timeSheetEmployeeId,
                    item.timeSheetEmployeePositionId,
                    item.positionId,
                    usePhases
                ).show(supportFragmentManager, AdditionalPositionDialogFragment.TAG)
            }

            positionContainerLL.addView(newItem)
        }
    }

    private fun addPerDiemRow(perDiem: TimeSheetEmployeeViewState.TimeSheetEmployeePerDiemUiModel) {
        perDiemContainerLL.removeAllViews()
        val newItem = layoutInflater.inflate(R.layout.view_asset_row, perDiemContainerLL, false)

        newItem.activity_worker_details_content_asset_tracker_value.text = perDiem.perDiemType
        newItem.activity_worker_details_content_asset_tracker_time_value.text = perDiem.totalOwed
        newItem.activity_worker_details_content_asset_tracker_delete_icon.safeClickListener {
            presenter.deletePerDiemData(perDiem.timeSheetEmployeeId)
        }
        newItem.activity_worker_details_content_asset_tracker_details_icon.safeClickListener {
            PerDiemDialogFragment.newInstance(timeSheetEmployeeId)
                .show(supportFragmentManager, PerDiemDialogFragment.TAG)
        }

        perDiemContainerLL.addView(newItem)
    }
}