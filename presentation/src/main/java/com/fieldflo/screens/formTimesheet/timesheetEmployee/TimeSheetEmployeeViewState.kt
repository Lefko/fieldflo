package com.fieldflo.screens.formTimesheet.timesheetEmployee

import com.fieldflo.common.toTimeString
import com.fieldflo.screens.formTimesheet.TimeSheetEmployeeState

data class TimeSheetEmployeeViewState(
    val usePhases: Boolean,
    val phaseCode: String,
    private val fixedPhases: Boolean,
    val formDate: String,

    val timeSheetEmployeeImageUrl: String,
    val employeeId: Int,
    val employeeFullName: String,
    val phaseName: String,
    val defaultPositionName: String,
    val employeeState: TimeSheetEmployeeState,
    val isTracking: Boolean,

    val startTime: String,
    val endTime: String,
    val totalWorkTime: String,
    val breakTime: String,

    val timeVerifiedDate: String,
    val timeVerifiedBy: String,
    val injuryVerifiedDate: String,
    val injuryVerifiedBy: String,
    val tookBreak: Boolean,
    val breakVerifiedDate: String,
    val breakVerifiedBy: String,

    val defaultPosition: TimeSheetEmployeePositionUiModel?,
    val otherPositions: List<TimeSheetEmployeePositionUiModel>,
    val assets: List<TimeSheetAssetEntityUiModel>,
    val perDiem: TimeSheetEmployeePerDiemUiModel?,

    val timeLogs: List<TimeSheetEmployeeTimeLogUiModel>,

    val pinImageLocation: String,
    val pinImageDeletingOn: String
) {
    val lastTimeLogChangeDate: String
        get() = if (timeLogs.isEmpty()) {
            "-\n-"
        } else {
            timeLogs[timeLogs.size - 1].data
        }

    val showPerDiem: Boolean
        get() = employeeState.isAtLeast(TimeSheetEmployeeState.MUST_VERIFY)

    val editTimeEnabled
        get() = employeeState.isAtLeast(TimeSheetEmployeeState.READY)

    val addEditOtherPositionsEnabled
        get() = employeeState.isGreaterThan(TimeSheetEmployeeState.TRACKING)

    val editPhaseEnabled
        get() = usePhases && defaultPositionName.isNotEmpty() && !fixedPhases &&
                employeeState != TimeSheetEmployeeState.TRACKING

    data class TimeSheetEmployeePositionUiModel(
        val timeSheetEmployeePositionId: Int,
        val timeSheetEmployeeId: Int = 0,
        val positionName: String,
        val defaultPosition: Boolean = false,
        private val totalWorkTime: Int,
        val positionId: Int = 0
    ) {
        val totalWorkTimeString
            get() = totalWorkTime.toTimeString()
    }

    data class TimeSheetAssetEntityUiModel(
        val timeSheetEmployeeAssetId: Int,
        val assetName: String,
        val assetNumber: Int,
        val totalWorkTime: String
    )

    data class TimeSheetEmployeePerDiemUiModel(
        val timeSheetEmployeeId: Int = 0,
        val perDiemType: String = "",
        val totalOwed: String = ""
    )

    data class TimeSheetEmployeeTimeLogUiModel(
        val timeSheetEmployeeTimeLogId: Int = 0,
        val data: String = "",
        val type: String = ""
    )
}
