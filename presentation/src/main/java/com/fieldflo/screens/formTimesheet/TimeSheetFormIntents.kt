package com.fieldflo.screens.formTimesheet

sealed class TimeSheetFormIntents {

    data class VerifyEmployeesIntent(
        val timeSheetEmployeeIds: List<Int>,
        val pin: String,
        val isInjured: Boolean,
        val tookFederalBreak: Boolean,
        val verifiedByEmployeeId: Int
    ) : TimeSheetFormIntents()
}