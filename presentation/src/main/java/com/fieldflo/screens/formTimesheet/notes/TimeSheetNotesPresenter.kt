package com.fieldflo.screens.formTimesheet.notes

import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.TimeSheetUseCases
import javax.inject.Inject

class TimeSheetNotesPresenter @Inject constructor(
    private val timeSheetUseCases: TimeSheetUseCases
) : ViewModel() {

    suspend fun getTimeSheetNotes(internalFormId: Int) =
        timeSheetUseCases.getTimeSheetNotes(internalFormId)

    suspend fun saveTimeSheetNotes(internalFormId: Int, newNotes: String) {
        timeSheetUseCases.saveTimeSheetNotes(internalFormId, newNotes)
    }
}
