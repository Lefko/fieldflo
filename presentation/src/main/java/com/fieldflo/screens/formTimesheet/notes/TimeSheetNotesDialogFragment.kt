package com.fieldflo.screens.formTimesheet.notes

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Button
import android.widget.EditText
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.afterTextChanges
import com.fieldflo.di.injector

class TimeSheetNotesDialogFragment : DialogFragment() {

    private var originalNotes: String = ""
    private lateinit var positiveButton: Button
    private lateinit var noteInputField: EditText

    private val internalFormId by lazy { arguments!!.getInt(INTERNAL_FORM_ID) }
    private val presenter by viewModels<TimeSheetNotesPresenter> { injector.timeSheetNotesViewModelFactory() }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_time_sheet_notes, null)
        noteInputField = view.findViewById(R.id.dialog_time_sheet_notes_inputET)
        noteInputField.afterTextChanges { string, _ ->
            positiveButton.isEnabled = string != originalNotes
        }

        val d = AlertDialog.Builder(activity as Context)
            .setView(view)
            .setPositiveButton(R.string.dialog_time_sheet_notes_save_notes) { _, _ -> }
            .setNegativeButton(android.R.string.cancel) { _, _ -> dismiss() }
            .show()

        return d
    }

    override fun onStart() {
        super.onStart()
        positiveButton = (dialog as AlertDialog).getButton(Dialog.BUTTON_POSITIVE).apply {
            isEnabled = false
            setOnClickListener {
                lifecycleScope.launchWhenCreated {
                    val newNotes = noteInputField.text.toString()
                    presenter.saveTimeSheetNotes(internalFormId, newNotes)
                    dismiss()
                }
            }
        }
        lifecycleScope.launchWhenCreated {
            val notes = presenter.getTimeSheetNotes(internalFormId)
            originalNotes = notes
            noteInputField.apply {
                setText(notes)
                setSelection(text.length)
            }
        }
    }

    companion object {
        const val TAG = "TimeSheetNotesDialogFragment"
        private const val INTERNAL_FORM_ID = "internal_form_id"

        @JvmStatic
        fun newInstance(internalFormId: Int) =
            TimeSheetNotesDialogFragment().apply {
                arguments = bundleOf(INTERNAL_FORM_ID to internalFormId)
            }
    }
}