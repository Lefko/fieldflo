package com.fieldflo.screens.formTimesheet.confirmStartTracking

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.fieldflo.R

class ConfirmStartTrackingMultipleDialogFragment : DialogFragment() {

    private val timeSheetEmployeeIds by lazy { arguments!!.getIntArray(TIMESHEET_EMPLOYEE_IDS)!! }
    private val timeSheetEmployeeNames by lazy { arguments!!.getStringArray(TIMESHEET_EMPLOYEE_NAMES)!! }

    private lateinit var startTrackingConfirmedListener: StartTrackingMultipleConfirmedListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val employeeNames = this.timeSheetEmployeeNames.joinToString()

        return AlertDialog.Builder(activity as Context)
            .setMessage(
                getString(
                    R.string.confirm_start_tracking_time_sheet_employee_title,
                    employeeNames
                )
            )
            .setPositiveButton(R.string.confirm_start_tracking_time_sheet_employee_positive) { _, _ ->
                startTrackingConfirmedListener.onStartTrackingMultipleConfirmed(timeSheetEmployeeIds)
            }
            .setNegativeButton(android.R.string.cancel) { _, _ ->
                dismiss()
            }
            .show()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    private fun onAttachToContext(context: Context) {
        startTrackingConfirmedListener = context as? StartTrackingMultipleConfirmedListener
            ?: throw IllegalArgumentException("${context::class.java.simpleName} must implement StartTrackingMultipleConfirmedListener")
    }

    companion object {
        const val TAG = "ConfirmStartTrackingMultipleDialogFragment"
        private const val TIMESHEET_EMPLOYEE_IDS = "timesheet_employee_ids"
        private const val TIMESHEET_EMPLOYEE_NAMES = "timesheet_employee_names"
        @JvmStatic
        fun newInstance(timeSheetEmployeeIds: IntArray, timeSheetEmployeeNames: Array<String>) =
            ConfirmStartTrackingMultipleDialogFragment().apply {
                arguments = Bundle().apply {
                    putIntArray(TIMESHEET_EMPLOYEE_IDS, timeSheetEmployeeIds)
                    putStringArray(TIMESHEET_EMPLOYEE_NAMES, timeSheetEmployeeNames)
                }
            }
    }

    interface StartTrackingMultipleConfirmedListener {
        fun onStartTrackingMultipleConfirmed(timeSheetEmployeeIds: IntArray)
    }
}