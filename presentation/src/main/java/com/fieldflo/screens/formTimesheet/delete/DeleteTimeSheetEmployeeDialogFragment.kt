package com.fieldflo.screens.formTimesheet.delete

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.fieldflo.R

class DeleteTimeSheetEmployeeDialogFragment : DialogFragment() {

    private val timeSheetEmployeeId by lazy {
        arguments!!.getInt(TIMESHEET_EMPLOYEE_ID)
    }

    private val timeSheetEmployeeName by lazy {
        arguments!!.getString(TIMESHEET_EMPLOYEE_NAME)
    }

    private lateinit var deleteConfirmedListener: DeleteTimeSheetEmployeeConfirmedListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(activity as Context)
            .setMessage(
                getString(
                    R.string.confirm_remove_time_sheet_employee_title,
                    timeSheetEmployeeName
                )
            )
            .setPositiveButton(R.string.confirm_remove_time_sheet_employee_positive) { _, _ ->
                deleteConfirmedListener.onDeleteTimeSheetEmployeeConfirmed(timeSheetEmployeeId)
            }
            .setNegativeButton(android.R.string.cancel) { _, _ ->
                dismiss()
            }
            .show()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    private fun onAttachToContext(context: Context) {
        deleteConfirmedListener = context as? DeleteTimeSheetEmployeeConfirmedListener
            ?: throw IllegalArgumentException("${context::class.java.simpleName} must implement DeleteConfirmedListener")
    }

    companion object {
        const val TAG = "DeleteTimeSheetEmployeeDialogFragment"
        private const val TIMESHEET_EMPLOYEE_ID = "timesheet_employee_id"
        private const val TIMESHEET_EMPLOYEE_NAME = "timesheet_employee_name"

        @JvmStatic
        fun newInstance(timeSheetEmployeeId: Int, timeSheetEmployeeName: String) =
            DeleteTimeSheetEmployeeDialogFragment().apply {
                arguments = Bundle().apply {
                    putInt(TIMESHEET_EMPLOYEE_ID, timeSheetEmployeeId)
                    putString(TIMESHEET_EMPLOYEE_NAME, timeSheetEmployeeName)
                }
            }
    }

    interface DeleteTimeSheetEmployeeConfirmedListener {
        fun onDeleteTimeSheetEmployeeConfirmed(timeSheetEmployeeId: Int)
    }
}