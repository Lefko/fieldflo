package com.fieldflo.screens.formTimesheet.verifyMultiple

import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.TimeSheetUseCases
import com.fieldflo.usecases.VerifyTimeSheetPinResult
import javax.inject.Inject

class VerifyMultiplePresenter @Inject constructor(
    private val timeSheetUseCases: TimeSheetUseCases
) : ViewModel() {

    suspend fun verify(internalFormId: Int, enteredPin: String): VerificationResult {
        val pinResult = timeSheetUseCases.verifyTimeSheetPin(
            internalFormId = internalFormId,
            pin = enteredPin
        )
        return when (pinResult) {
            VerifyTimeSheetPinResult.VerifyError -> VerificationResult.VerificationError
            is VerifyTimeSheetPinResult.VerifySuccess ->
                VerificationResult.VerificationSuccess(pinResult.verifiedByEmployeeId)
        }
    }
}

sealed class VerificationResult {
    object VerificationError : VerificationResult()
    data class VerificationSuccess(val verifiedBy: Int) : VerificationResult()
}
