package com.fieldflo.screens.formTimesheet.verify

data class VerifyViewState(
    val timeSheetEmployee: TimeSheetEmployeeUiModel = TimeSheetEmployeeUiModel(
        injured = false,
        federalBreak = true,
        timeSheetEmployeeName = ""
    ),
    val needsPhotoPin: Boolean = false,
    val pinFileDestLocation: String = ""
) {
    data class TimeSheetEmployeeUiModel(
        val timeSheetEmployeeId: Int = 0,
        val timeSheetEmployeeName: String,
        val totalWorkTime: Int = 0,
        val injured: Boolean = false,
        val breakTime: String = "",
        val federalBreak: Boolean = true,
        val startTimeStamp: String = "",
        val endTimeStamp: String = "",
        val perDiemType: String = "",
        val perDiemTotalOwed: String = "",
        val assets: List<AssetUiModel> = listOf(),
        val positions: List<PositionUiModel> = listOf()
    )

    data class PositionUiModel(
        val positionName: String,
        val positionTime: String
    )

    data class AssetUiModel(
        val assetName: String,
        val assetTime: String
    )
}