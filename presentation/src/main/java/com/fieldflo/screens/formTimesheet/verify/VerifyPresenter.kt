package com.fieldflo.screens.formTimesheet.verify

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fieldflo.common.round
import com.fieldflo.common.toOnlyTimeString
import com.fieldflo.common.toTimeString
import com.fieldflo.screens.formTimesheet.TimeSheetEmployeeState
import com.fieldflo.usecases.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import javax.inject.Inject

class VerifyPresenter @Inject constructor(
    private val timeSheetUseCases: TimeSheetUseCases,
    private val settingsUseCase: SettingsUseCase
) : ViewModel() {

    private val _viewState = MutableStateFlow(VerifyViewState())
    val viewState: Flow<VerifyViewState> get() = _viewState
    val currentViewState: VerifyViewState get() = _viewState.value

    private val mutex = Mutex()

    val requiresPhotoPin get() = settingsUseCase.usesPhotoPin

    @Throws(Exception::class)
    fun loadInitialViewState(internalFormId: Int, timeSheetEmployeeId: Int) {
        viewModelScope.launch {
            val timeSheetEmployee = timeSheetUseCases.getFullTimeSheetEmployee(timeSheetEmployeeId)
            val timeSheetEmployeeUi = timeSheetEmployee!!.toUiModel()

            val pinFileDest = timeSheetUseCases
                .getTimeSheetEmployeePinOutLocation(internalFormId, timeSheetEmployeeId)

            val initialViewState =
                VerifyViewState(timeSheetEmployeeUi, requiresPhotoPin, pinFileDest)

            mutex.withLock {
                _viewState.value = initialViewState
            }
        }
    }

    fun setIsInjured(isInjured: Boolean) {
        viewModelScope.launch {
            mutex.withLock {
                val prevState = _viewState.value
                val newState =
                    prevState.copy(timeSheetEmployee = prevState.timeSheetEmployee.copy(injured = isInjured))
                _viewState.value = newState
            }
        }
    }

    fun setTookBreak(tookBreak: Boolean) {
        viewModelScope.launch {
            mutex.withLock {
                val prevState = _viewState.value
                val newState =
                    prevState.copy(timeSheetEmployee = prevState.timeSheetEmployee.copy(federalBreak = tookBreak))
                _viewState.value = newState
            }
        }
    }

    suspend fun verifyPin(
        internalFormId: Int,
        employeeId: Int,
        pin: String
    ): VerifyTimeSheetPinResult {
        return timeSheetUseCases.verifyTimeSheetPin(internalFormId, employeeId, pin)
    }
}

private fun FullTimeSheetEmployeeUiData.toUiModel(): VerifyViewState.TimeSheetEmployeeUiModel {
    return VerifyViewState.TimeSheetEmployeeUiModel(
        timeSheetEmployeeId = this.timeSheetEmployeeId,
        timeSheetEmployeeName = this.timeSheetEmployeeName,
        totalWorkTime = this.totalWorkTime,
        injured = this.employeeStatus == TimeSheetEmployeeState.VERIFIED_WITH_INJURY,
        breakTime = this.breakTime.toTimeString(),
        federalBreak = this.tookBreak,
        startTimeStamp = this.clockIn.toOnlyTimeString(),
        endTimeStamp = this.clockOut.toOnlyTimeString(),
        perDiemType = this.perDiem?.perDiemType ?: "-",
        perDiemTotalOwed = if (this.perDiem != null) {
            "$${this.perDiem.totalOwed.round(2)}"
        } else {
            "-"
        },
        assets = this.assets.map { it.toUiModel() },
        positions = this.positions.map { it.toUiModel() }
    )
}

private fun TimeSheetEmployeeAssetData.toUiModel(): VerifyViewState.AssetUiModel {
    return VerifyViewState.AssetUiModel(
        assetName = this.assetName,
        assetTime = this.totalWorkTime.toTimeString()
    )
}

private fun TimeSheetEmployeePositionData.toUiModel(): VerifyViewState.PositionUiModel {
    return VerifyViewState.PositionUiModel(
        positionName = this.positionName,
        positionTime = this.totalWorkTime.toTimeString()
    )
}
