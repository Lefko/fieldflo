package com.fieldflo.screens.formTimesheet.verifyMultiple

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.afterTextChanges
import com.fieldflo.common.hidden
import com.fieldflo.common.safeClickListener
import com.fieldflo.common.visible
import com.fieldflo.di.injector

class VerifyMultipleEmployeesDialogFragment : DialogFragment() {

    private lateinit var callback: VerifyMultipleDialogCallback

    private lateinit var timesCorrectRG: RadioGroup
    private lateinit var positionsCorrectRG: RadioGroup
    private lateinit var injuredRG: RadioGroup
    private lateinit var breakRG: RadioGroup
    private lateinit var allCorrectCB: CheckBox
    private lateinit var pinET: EditText
    private lateinit var injuredWarningTV: TextView
    private lateinit var allCorrectErrorTV: TextView
    private lateinit var noBreakErrorTV: TextView
    private lateinit var pinErrorTV: TextView
    private lateinit var positiveButton: Button

    private val positiveEnabled: Boolean
        get() = infoCorrect && pinET.text.isNotEmpty() &&
                breakRG.checkedRadioButtonId == R.id.dialog_activity_verify_working_time_federal_breaks_question_yes

    private val infoCorrect: Boolean
        get() = allCorrectCB.isChecked &&
                timesCorrectRG.checkedRadioButtonId == R.id.dialog_activity_verify_working_time_time_question_yes &&
                positionsCorrectRG.checkedRadioButtonId == R.id.dialog_activity_verify_working_time_positions_question_yes

    private val timeSheetEmployeeIds by lazy { arguments!!.getIntArray(TIME_SHEET_EMPLOYEE_IDS)!! }
    private val projectId: Int by lazy { arguments!!.getInt(PROJECT_ID_KEY) }
    private val internalFormId by lazy { arguments!!.getInt(INTERNAL_FORM_ID_KEY) }

    private val presenter by lazy {
        ViewModelProvider(this, injector.verifyMultipleViewModelFactory())
            .get(VerifyMultiplePresenter::class.java)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val viewMain =
            LayoutInflater.from(activity).inflate(R.layout.dialog_multiple_employee_verify, null)

        findViews(viewMain)

        initViews()

        return AlertDialog.Builder(activity as Context)
            .setView(viewMain)
            .setPositiveButton(R.string.dialog_single_employee_verify_verify_btn) { _, _ -> }
            .setNegativeButton(android.R.string.cancel) { _, _ -> dismiss() }
            .show()
    }

    private fun findViews(viewMain: View) {
        val root =
            viewMain.findViewById<ConstraintLayout>(R.id.dialog_activity_verify_working_day_rootCL)
        timesCorrectRG =
            root.findViewById(R.id.dialog_activity_verify_working_time_time_question_radiogroup)
        positionsCorrectRG =
            root.findViewById(R.id.dialog_activity_verify_working_time_positions_question_radiougroup)
        injuredRG =
            root.findViewById(R.id.dialog_activity_verify_working_time_injured_question_radiogroup)
        breakRG =
            root.findViewById(R.id.dialog_activity_verify_working_time_federal_breaks_question_radiogroup)
        allCorrectCB = root.findViewById(R.id.dialog_activity_verify_working_time_verify_icon)
        pinET = root.findViewById(R.id.dialog_activity_verify_working_time_enter_pin)
        injuredWarningTV =
            root.findViewById(R.id.dialog_activity_verify_working_day_message_injuredTV)
        allCorrectErrorTV =
            root.findViewById(R.id.dialog_activity_verify_working_day_above_incorrectTV)
        noBreakErrorTV = root.findViewById(R.id.dialog_activity_verify_working_day_no_breakTV)
        pinErrorTV = root.findViewById(R.id.dialog_activity_verify_working_day_incorrect_pinTV)
    }

    private fun initViews() {
        timesCorrectRG.setOnCheckedChangeListener { _, _ ->
            positiveButton.isEnabled = positiveEnabled
            if (infoCorrect) {
                allCorrectErrorTV.hidden()
            } else {
                allCorrectErrorTV.visible()
            }
        }

        breakRG.setOnCheckedChangeListener { _, checkedId ->
            positiveButton.isEnabled = positiveEnabled
            if (checkedId == R.id.dialog_activity_verify_working_time_federal_breaks_question_yes) {
                noBreakErrorTV.hidden()
            } else {
                noBreakErrorTV.visible()
            }
        }

        positionsCorrectRG.setOnCheckedChangeListener { _, _ ->
            positiveButton.isEnabled = positiveEnabled
            if (infoCorrect) {
                allCorrectErrorTV.hidden()
            } else {
                allCorrectErrorTV.visible()
            }
        }

        injuredRG.setOnCheckedChangeListener { _, _ ->
            if (injuredRG.checkedRadioButtonId == R.id.dialog_activity_verify_working_time_injured_question_yes) {
                injuredWarningTV.visible()
            } else {
                injuredWarningTV.hidden()
            }
        }

        allCorrectCB.setOnClickListener {
            positiveButton.isEnabled = positiveEnabled
            if (infoCorrect) {
                allCorrectErrorTV.hidden()
            } else {
                allCorrectErrorTV.visible()
            }
        }

        pinET.afterTextChanges { _, _ ->
            pinErrorTV.hidden()
            positiveButton.isEnabled = positiveEnabled
        }
    }

    override fun onStart() {
        super.onStart()
        positiveButton = (dialog as AlertDialog).getButton(Dialog.BUTTON_POSITIVE).apply {
            isEnabled = false
        }

        positiveButton.safeClickListener {
            lifecycleScope.launchWhenCreated {
                val verificationResult = presenter.verify(internalFormId, pinET.text.toString())
                render(verificationResult)
            }
        }
    }

    private fun render(verificationResult: VerificationResult) {

        when (verificationResult) {
            is VerificationResult.VerificationSuccess -> {
                callback.onVerifyMultiple(
                    timeSheetEmployeeIds = timeSheetEmployeeIds,
                    isInjured = injuredRG.checkedRadioButtonId == R.id.dialog_activity_verify_working_time_injured_question_yes,
                    tookFederalBreak = breakRG.checkedRadioButtonId == R.id.dialog_activity_verify_working_time_federal_breaks_question_yes,
                    verifiedByEmployeeId = verificationResult.verifiedBy
                )

                dismiss()
            }

            VerificationResult.VerificationError -> pinErrorTV.visible()
        }
    }

    private fun onAttachToContext(context: Context) {
        if (context is VerifyMultipleEmployeesDialogFragment.VerifyMultipleDialogCallback) {
            this.callback = context
        } else {
            throw IllegalArgumentException(
                "${context.javaClass.simpleName} must be an instance of VerifyMultipleDialogCallback"
            )
        }
    }

    companion object {
        const val TAG = "VerifyMultipleEmployeesDialogFragment"
        private const val TIME_SHEET_EMPLOYEE_IDS = "timesheet_employee_ids"
        private const val PROJECT_ID_KEY = "project_id_key"
        private const val INTERNAL_FORM_ID_KEY = "internal_form_id_key"

        @JvmStatic
        fun newInstance(
            projectId: Int,
            internalFormId: Int,
            timeSheetEmployeeIds: IntArray
        ): VerifyMultipleEmployeesDialogFragment {
            return VerifyMultipleEmployeesDialogFragment().apply {
                arguments = Bundle().apply {
                    putInt(PROJECT_ID_KEY, projectId)
                    putInt(INTERNAL_FORM_ID_KEY, internalFormId)
                    putIntArray(TIME_SHEET_EMPLOYEE_IDS, timeSheetEmployeeIds)
                }
            }
        }
    }

    interface VerifyMultipleDialogCallback {
        fun onVerifyMultiple(
            timeSheetEmployeeIds: IntArray,
            isInjured: Boolean,
            tookFederalBreak: Boolean,
            verifiedByEmployeeId: Int
        )
    }
}

