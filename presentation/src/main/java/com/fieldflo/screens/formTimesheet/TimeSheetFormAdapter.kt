package com.fieldflo.screens.formTimesheet

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SortedList
import androidx.recyclerview.widget.SortedListAdapterCallback
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.fieldflo.R
import com.fieldflo.common.drawableTop
import com.fieldflo.common.textColor

class TimeSheetFormAdapter(
    private val usePhases: Boolean,
    private val callback: EmployeeRowActionHandler,
) : RecyclerView.Adapter<TimeSheetFormAdapter.TimeSheetEmployeeViewHolder>() {

    @Suppress("PrivatePropertyName")
    private val CALLBACK =
        object : SortedListAdapterCallback<TimeSheetFormViewState.TimeSheetEmployeeUiModel>(this) {

            override fun areItemsTheSame(
                oldItem: TimeSheetFormViewState.TimeSheetEmployeeUiModel,
                newItem: TimeSheetFormViewState.TimeSheetEmployeeUiModel
            ) = oldItem.timeSheetEmployeeId == newItem.timeSheetEmployeeId

            override fun areContentsTheSame(
                oldItem: TimeSheetFormViewState.TimeSheetEmployeeUiModel,
                newItem: TimeSheetFormViewState.TimeSheetEmployeeUiModel
            ) = oldItem == newItem

            override fun compare(
                employee1: TimeSheetFormViewState.TimeSheetEmployeeUiModel,
                employee2: TimeSheetFormViewState.TimeSheetEmployeeUiModel
            ): Int {
                return employee2.timeSheetEmployeeId.compareTo(employee1.timeSheetEmployeeId)
            }
        }

    private val employeesSorted: SortedList<TimeSheetFormViewState.TimeSheetEmployeeUiModel> =
        SortedList(TimeSheetFormViewState.TimeSheetEmployeeUiModel::class.java, CALLBACK)

    val selectedRows: List<TimeSheetFormViewState.TimeSheetEmployeeUiModel>
        get() {
            val selected = mutableListOf<TimeSheetFormViewState.TimeSheetEmployeeUiModel>()
            for (i in 0 until itemCount) {
                val row = getItem(i)
                if (row.isRowSelected) selected.add(row)
            }

            return selected.toList()
        }

    fun submitList(newList: List<TimeSheetFormViewState.TimeSheetEmployeeUiModel>) {
        employeesSorted.replaceAll(newList)
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).timeSheetEmployeeId.toLong()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TimeSheetEmployeeViewHolder {
        val rowLayout = LayoutInflater.from(parent.context)
            .inflate(
                if (usePhases) R.layout.row_daily_timesheet_phase else R.layout.row_daily_timesheet_no_phase,
                parent,
                false
            ) as ViewGroup

        return TimeSheetEmployeeViewHolder(rowLayout, usePhases, callback) { getItem(it) }
    }

    override fun onBindViewHolder(holder: TimeSheetEmployeeViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemCount() = employeesSorted.size()

    private fun getItem(position: Int) = employeesSorted[position]

    class TimeSheetEmployeeViewHolder(
        rowLayout: ViewGroup,
        private val usePhases: Boolean,
        callback: EmployeeRowActionHandler,
        employeeProvider: (Int) -> TimeSheetFormViewState.TimeSheetEmployeeUiModel
    ) : RecyclerView.ViewHolder(rowLayout) {

        private val workerDetailsClickMask: View =
            rowLayout.findViewById(R.id.row_daily_timesheet_employee_details_click_mask)
        private val employeeNameTV: TextView =
            rowLayout.findViewById(if (usePhases) R.id.row_daily_timesheet_phase_employee_name else R.id.row_daily_timesheet_no_phase_employee_name)
        private val employeePhotoIV: ImageView =
            rowLayout.findViewById(if (usePhases) R.id.row_daily_timesheet_phase_employee_photoIV else R.id.row_daily_timesheet_no_phase_employee_photoIV)
        private val phaseClickMask: View? =
            rowLayout.findViewById(R.id.row_daily_timesheet_phase_click_mask)
        private val phaseTV: TextView? =
            if (usePhases) rowLayout.findViewById(R.id.row_daily_timesheet_phase) else null
        private val positionClickMask =
            rowLayout.findViewById<View>(R.id.row_daily_timesheet_position_click_mask)
        private val positionTV: TextView =
            rowLayout.findViewById(if (usePhases) R.id.row_daily_timesheet_phase_position else R.id.row_daily_timesheet_no_phase_position)
        private val removeIV: ImageView =
            rowLayout.findViewById(if (usePhases) R.id.row_daily_timesheet_phase_remove_icon else R.id.row_daily_timesheet_no_phase_remove_icon)
        private val statusTV: TextView =
            rowLayout.findViewById(if (usePhases) R.id.row_daily_timesheet_phase_time_status_translucentTV else R.id.row_daily_timesheet_no_phase_time_status_translucent)
        private val selectIV: ImageView =
            rowLayout.findViewById(if (usePhases) R.id.row_daily_timesheet_employee_selectIV else R.id.row_daily_timesheet_no_phase_selectIV)
        private val editPositionIV: ImageView =
            rowLayout.findViewById(if (usePhases) R.id.row_daily_timesheet_position_editIV else R.id.row_daily_timesheet_no_position_editIV)
        private val editPhaseIV: ImageView? =
            if (usePhases) rowLayout.findViewById(R.id.row_daily_timesheet_phase_editTV) else null

        init {
            workerDetailsClickMask.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val employee = employeeProvider.invoke(position)
                    callback.onEmployeeRowAction(
                        employee,
                        EmployeeRowAction.ACTION_EMPLOYEE_CLICKED(usePhases)
                    )
                }
            }

            removeIV.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val employee = employeeProvider.invoke(position)
                    callback.onEmployeeRowAction(employee, EmployeeRowAction.ACTION_DELETED)
                }
            }

            positionClickMask.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val employee = employeeProvider.invoke(position)
                    callback.onEmployeeRowAction(employee, EmployeeRowAction.POSITION_CLICKED)
                }
            }

            phaseClickMask?.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val employee = employeeProvider.invoke(position)
                    callback.onEmployeeRowAction(employee, EmployeeRowAction.PHASE_CLICKED)
                }
            }

            statusTV.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val employee = employeeProvider.invoke(position)
                    callback.onEmployeeRowAction(employee, EmployeeRowAction.STATUS_CLICKED)
                }
            }

            selectIV.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val employee = employeeProvider.invoke(position)
                    callback.onEmployeeRowAction(employee, EmployeeRowAction.ROW_SELECT_CLICKED)
                }
            }
        }

        fun bind(uiModel: TimeSheetFormViewState.TimeSheetEmployeeUiModel) {
            setSelected(uiModel)

            val glideRequestOptions: RequestOptions =
                RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL)
                    .transform(RoundedCorners(10))
            Glide.with(employeePhotoIV)
                .load(uiModel.avatarUrl)
                .centerCrop()
                .apply(glideRequestOptions)
                .placeholder(R.drawable.app_icon)
                .into(employeePhotoIV)

            employeeNameTV.text = uiModel.employeeFullName

            // Position
            positionClickMask.isEnabled = uiModel.editPositionEnabled
            if (uiModel.editPositionEnabled) {
                editPositionIV.setImageResource(R.drawable.selector_ic_edit_small)
                if (uiModel.positionName.isEmpty()) {
                    positionTV.text =
                        itemView.context.getString(R.string.activity_daily_timesheet_not_selected)
                    positionTV.textColor(R.color.attention)
                } else {
                    positionTV.text = uiModel.positionName
                    positionTV.textColor(R.color.text_color)
                }
            } else {
                positionTV.text = uiModel.positionName
                positionTV.textColor(R.color.disabled_icon)
                editPositionIV.setImageResource(R.drawable.ic_edit_grey_translucent)
            }

            // Phases
            if (usePhases) {
                phaseClickMask?.isEnabled = uiModel.editPhaseEnabled
                if (uiModel.editPhaseEnabled) {
                    editPhaseIV?.setImageResource(R.drawable.selector_ic_edit_small)
                    if (uiModel.phaseName.isEmpty()) {
                        phaseTV!!.setText(R.string.activity_daily_timesheet_not_selected)
                        phaseTV.textColor(R.color.attention)
                    } else {
                        phaseTV!!.text = uiModel.phaseName
                        phaseTV.textColor(R.color.text_color)
                    }
                } else {
                    phaseTV!!.text = uiModel.phaseName
                    phaseTV.textColor(R.color.disabled_icon)
                    editPhaseIV?.setImageResource(R.drawable.ic_edit_grey_translucent)
                }
            }

            //Status
            when (uiModel.employeeState) {
                TimeSheetEmployeeState.TRACKING -> {
                    statusTV.drawableTop(R.drawable.ic_clock_green)
                    statusTV.text =
                        itemView.context.getText(R.string.activity_daily_timesheet_row_tracking)
                    statusTV.textColor(R.color.for_tracking)
                }

                TimeSheetEmployeeState.VERIFIED -> {
                    statusTV.drawableTop(R.drawable.ic_verified)
                    statusTV.text =
                        itemView.context.getText(R.string.activity_daily_timesheet_row_verified)
                    statusTV.textColor(R.color.for_verified_injured)
                }

                TimeSheetEmployeeState.VERIFIED_WITH_INJURY -> {
                    statusTV.drawableTop(R.drawable.ic_verified_injured)
                    statusTV.text =
                        itemView.context.getText(R.string.activity_daily_timesheet_row_verified)
                    statusTV.textColor(R.color.for_verified_injured)
                }

                TimeSheetEmployeeState.MUST_VERIFY -> {
                    statusTV.drawableTop(R.drawable.ic_verify)
                    statusTV.text =
                        itemView.context.getText(R.string.activity_daily_timesheet_row_verify)
                    statusTV.textColor(R.color.for_verify)
                }

                TimeSheetEmployeeState.READY -> {
                    statusTV.drawableTop(R.drawable.ic_clock_grey)
                    statusTV.text =
                        itemView.context.getText(R.string.activity_daily_timesheet_row_start)
                    statusTV.textColor(R.color.for_start)
                }

                TimeSheetEmployeeState.NOT_READY -> {
                    statusTV.drawableTop(R.drawable.ic_clock_grey_translucent)
                    statusTV.text =
                        itemView.context.getText(R.string.activity_daily_timesheet_row_start)
                    statusTV.textColor(R.color.for_start_translucent)
                }
            }
        }

        private fun setSelected(uiModel: TimeSheetFormViewState.TimeSheetEmployeeUiModel) {
            selectIV.setImageResource(if (uiModel.isRowSelected) R.drawable.ic_select_single_medium else R.drawable.ic_select_empty)
        }
    }

    fun interface EmployeeRowActionHandler {
        fun onEmployeeRowAction(
            employee: TimeSheetFormViewState.TimeSheetEmployeeUiModel,
            action: EmployeeRowAction
        )
    }

    @Suppress("ClassName")
    sealed class EmployeeRowAction {
        data class ACTION_EMPLOYEE_CLICKED(val usePhases: Boolean) : EmployeeRowAction()
        object ACTION_DELETED : EmployeeRowAction()
        object PHASE_CLICKED : EmployeeRowAction()
        object POSITION_CLICKED : EmployeeRowAction()
        object STATUS_CLICKED : EmployeeRowAction()
        object ROW_SELECT_CLICKED : EmployeeRowAction()
    }
}

