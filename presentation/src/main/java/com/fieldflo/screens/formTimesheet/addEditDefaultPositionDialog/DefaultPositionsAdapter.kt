package com.fieldflo.screens.formTimesheet.addEditDefaultPositionDialog

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class DefaultPositionsAdapter(context: Context) : ArrayAdapter<PositionUiRow>(
    context,
    android.R.layout.simple_list_item_single_choice,
    mutableListOf()
) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val listItem = convertView ?: LayoutInflater.from(context)
            .inflate(android.R.layout.simple_list_item_single_choice, parent, false)

        (listItem as TextView).text = getItem(position)?.positionName

        return listItem
    }
}