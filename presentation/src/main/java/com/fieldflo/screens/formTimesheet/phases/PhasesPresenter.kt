package com.fieldflo.screens.formTimesheet.phases

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.Phase
import com.fieldflo.usecases.PhaseUseCases
import javax.inject.Inject

class PhasesPresenter @Inject constructor(private val phaseUseCases: PhaseUseCases) : ViewModel() {

    suspend fun loadPhases(projectId: Int): List<PhaseUiRow> =
        phaseUseCases.getPhasesForProject(projectId).map { it.toUiModel() }
}

data class PhaseUiRow(
    val phaseId: Int = 0,
    val phaseName: String = "",
    val phaseCode: String = ""
)

private fun Phase.toUiModel() = PhaseUiRow(
    phaseId = this.phaseId,
    phaseName = this.phaseName,
    phaseCode = this.phaseCode
)
