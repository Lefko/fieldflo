package com.fieldflo.screens.formTimesheet.addEditAdditionalPositionDialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.gone
import com.fieldflo.common.safeClickListener
import com.fieldflo.common.visible
import com.fieldflo.di.injector
import kotlinx.android.synthetic.main.dialog_add_position.view.*
import kotlinx.coroutines.flow.collect

class AdditionalPositionDialogFragment : DialogFragment() {
    private lateinit var fragmentView: View

    private val presenter by viewModels<AdditionalPositionPresenter> { injector.positionViewModelFactory() }

    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }
    private val timeSheetEmployeePositionId: Int? by lazy {
        arguments?.getInt(TIME_SHEET_EMPLOYEE_POSITION_ID)
    }
    private val timeSheetEmployeeId by lazy { arguments!!.getInt(TIME_SHEET_EMPLOYEE_ID) }
    private val positionId by lazy { arguments!!.getInt(POSITION_ID) }
    private val usePhases by lazy { arguments!!.getBoolean(USE_PHASES) }

    private val positionsAdapter by lazy {
        AdditionalPositionDialogSpinnerAdapter<AdditionalPositionViewState.PositionUiRow>(
            context!!
        )
    }
    private val phasesAdapter by lazy {
        AdditionalPositionDialogSpinnerAdapter<AdditionalPositionViewState.PhaseUiRow>(
            context!!
        )
    }

    private lateinit var positiveButton: Button

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        fragmentView = View.inflate(context, R.layout.dialog_add_position, null)

        val dialog = AlertDialog.Builder(activity as Context)
            .setView(fragmentView)
            .setPositiveButton(android.R.string.ok, null)
            .setNegativeButton(android.R.string.cancel) { _, _ -> dismiss() }
            .show()

        dialog.window?.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
            setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        }

        initView(fragmentView)

        return dialog
    }

    private fun initView(view: View) {
        view.dialog_add_position_positions.adapter = positionsAdapter
        view.dialog_add_position_hours.adapter = ArrayAdapter.createFromResource(
            context!!, R.array.dialog_add_position_hours_list, R.layout.row_spinner_dropdown
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        view.dialog_add_position_minutes.adapter = ArrayAdapter.createFromResource(
            context!!, R.array.dialog_add_position_minute_list, R.layout.row_spinner_dropdown
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        if (usePhases) {
            view.dialog_add_position_phases.apply {
                adapter = phasesAdapter
                visible()
            }
            view.dialog_add_position_phase_label.visible()
        } else {
            view.dialog_add_position_phases.gone()
            view.dialog_add_position_phase_label.gone()
        }

        val hourMinuteSelectionListener: AdapterView.OnItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    presenter.updateTotalTime(getWorkTimePosition())
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }

        view.dialog_add_position_hours.onItemSelectedListener = hourMinuteSelectionListener
        view.dialog_add_position_minutes.onItemSelectedListener = hourMinuteSelectionListener
    }

    private fun extractPhaseId(): Int {
        return if (usePhases) {
            (fragmentView.dialog_add_position_phases.selectedItem as AdditionalPositionViewState.PhaseUiRow).phaseId
        } else {
            0
        }
    }

    override fun onStart() {
        super.onStart()
        positiveButton = (dialog as AlertDialog).getButton(Dialog.BUTTON_POSITIVE).apply {
            isEnabled = false
            safeClickListener {
                lifecycleScope.launchWhenStarted {
                    presenter.addEditPosition(
                        timeSheetEmployeeId,
                        timeSheetEmployeePositionId,
                        (fragmentView.dialog_add_position_positions.selectedItem as AdditionalPositionViewState.PositionUiRow).positionId,
                        extractPhaseId(),
                        getWorkTimePosition()
                    )
                    dismiss()
                }

            }
        }
        present()
    }

    private fun present() {
        lifecycleScope.launchWhenStarted {
            presenter.viewState.collect { render(it) }
        }
        lifecycleScope.launchWhenStarted {
            presenter.loadInitialState(projectId, timeSheetEmployeeId, timeSheetEmployeePositionId)
        }
    }

    private fun render(state: AdditionalPositionViewState) {
        positionsAdapter.submit(state.positions)
        phasesAdapter.submit(state.phases)
        state.currentPosition?.also { currentPosition ->
            state.positions.forEachIndexed { index, position ->
                if (position.positionId == positionId) {
                    fragmentView.dialog_add_position_positions.setSelection(index)
                }
            }

            val pos = currentPosition.totalWorkTime / 3600
            fragmentView.dialog_add_position_hours.setSelection(pos)
            if (currentPosition.totalWorkTime % 3600 != 0) {
                fragmentView.dialog_add_position_minutes.setSelection(1)
            }

            state.phases.forEachIndexed { index, phase ->
                if (currentPosition.phaseId == phase.phaseId) {
                    fragmentView.dialog_add_position_phases.setSelection(index)
                }
            }

            fragmentView.dialog_add_position_titleTV.setText(R.string.dialog_edit_positions_title)
        }

        positiveButton.isEnabled = state.okEnabled
    }

    private fun convertHourToSecond(position: Int): Int {
        return when (position) {
            0 -> 0
            1 -> 3600 // 1 hour
            2 -> 7200  // 2 hours
            3 -> 10800 // 3 hours
            4 -> 14400 // 4 hours
            5 -> 18000 // 5 hours
            6 -> 21600 // 6 hours
            7 -> 25200 // 7 hours
            8 -> 28800 // 8 hours
            9 -> 32400 // 9 hours
            10 -> 36000 // 10 hours
            11 -> 39600 // 11 hours
            12 -> 43200 // 12 hours
            13 -> 46800 // 13 hours
            14 -> 50400 // 14 hours
            15 -> 54000 // 15 hours
            16 -> 57600 // 16 hours
            17 -> 61200 // 17 hours
            18 -> 64800 // 18 hours
            19 -> 68400 // 19 hours
            20 -> 72000 // 20 hours
            21 -> 75600 // 21 hours
            22 -> 79200 // 22 hours
            23 -> 82800 // 23 hours

            else -> 0
        }
    }

    private fun convertMinuteToSecond(position: Int): Int {
        return when (position) {
            0 -> 0
            1 -> 1800 // 30 minutes

            else -> 0
        }
    }

    private fun getWorkTimePosition(): Int {
        val hours = convertHourToSecond(fragmentView.dialog_add_position_hours.selectedItemPosition)
        val minutes =
            convertMinuteToSecond(fragmentView.dialog_add_position_minutes.selectedItemPosition)
        return hours + minutes
    }

    companion object {
        const val TAG = "AdditionalPositionDialogFragment"
        private const val PROJECT_ID = "project_id"
        private const val TIME_SHEET_EMPLOYEE_POSITION_ID = "time_sheet_employee_position_d"
        private const val TIME_SHEET_EMPLOYEE_ID = "time_sheet_employee_id"
        private const val POSITION_ID = "position_id"
        private const val USE_PHASES = "use_phases"


        fun newInstance(projectId: Int, timeSheetEmployeeId: Int, usePhases: Boolean) =
            AdditionalPositionDialogFragment().apply {
                arguments = Bundle().apply {
                    putInt(PROJECT_ID, projectId)
                    putInt(TIME_SHEET_EMPLOYEE_ID, timeSheetEmployeeId)
                    putBoolean(USE_PHASES, usePhases)
                }
            }

        fun newInstance(
            projectId: Int,
            timeSheetEmployeeId: Int,
            timeSheetEmployeePositionId: Int?,
            positionId: Int,
            usePhases: Boolean
        ) =
            AdditionalPositionDialogFragment().apply {
                arguments = Bundle().apply {
                    putInt(PROJECT_ID, projectId)
                    putInt(TIME_SHEET_EMPLOYEE_ID, timeSheetEmployeeId)
                    timeSheetEmployeePositionId?.let {
                        putInt(TIME_SHEET_EMPLOYEE_POSITION_ID, timeSheetEmployeePositionId)
                    }
                    putInt(POSITION_ID, positionId)
                    putBoolean(USE_PHASES, usePhases)
                }
            }
    }
}