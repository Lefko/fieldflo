package com.fieldflo.screens.formTimesheet.addEditAdditionalPositionDialog

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fieldflo.data.persistence.db.entities.Phase
import com.fieldflo.data.persistence.db.entities.Position
import com.fieldflo.data.persistence.db.entities.TimesheetEmployeePosition
import com.fieldflo.usecases.PhaseUseCases
import com.fieldflo.usecases.TimeSheetUseCases
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import javax.inject.Inject

class AdditionalPositionPresenter @Inject constructor(
    private val phaseUseCases: PhaseUseCases,
    private val timeSheetUseCases: TimeSheetUseCases
) : ViewModel() {

    private val _viewState = MutableStateFlow<AdditionalPositionViewState?>(null)
    val viewState: Flow<AdditionalPositionViewState> = _viewState.filterNotNull()

    private val mutex = Mutex()

    fun loadInitialState(
        projectId: Int,
        timeSheetEmployeeId: Int,
        timeSheetEmployeePositionId: Int?
    ) {
        viewModelScope.launch {
            val phases = phaseUseCases.getPhasesForProject(projectId).map { it.toUiModel() }
            val positions = timeSheetUseCases.getPositionsForTimeSheetAdditionalPosition(
                projectId,
                timeSheetEmployeeId
            ).map { it.toUiModel() }

            val currentPosition = timeSheetUseCases.getTimeSheetEmployeePosition(
                timeSheetEmployeeId,
                timeSheetEmployeePositionId ?: -1
            )
            val availableTimeForPosition = timeSheetUseCases.getAvailableTimeForPosition(
                timeSheetEmployeeId,
                timeSheetEmployeePositionId
            )

            mutex.withLock {
                _viewState.value = AdditionalPositionViewState(
                    positions = positions,
                    phases = phases,
                    currentPosition = currentPosition.toUiModel(),
                    totalWorkTime = currentPosition?.totalWorkTime ?: 0,
                    availableTime = availableTimeForPosition
                )
            }
        }
    }

    fun updateTotalTime(newTotalWorkTime: Int) {
        viewModelScope.launch {
            mutex.withLock {
                _viewState.value?.let { prevState ->
                    _viewState.value = prevState.copy(
                        currentPosition = prevState.currentPosition?.copy(totalWorkTime = newTotalWorkTime),
                        totalWorkTime = newTotalWorkTime
                    )
                }
            }
        }
    }

    suspend fun addEditPosition(
        timeSheetEmployeeId: Int,
        timeSheetEmployeePositionId: Int?,
        positionId: Int,
        phaseId: Int,
        totalWorkTimeSeconds: Int
    ) {
        if (timeSheetEmployeePositionId == null || timeSheetEmployeePositionId == 0) {
            timeSheetUseCases.addPosition(
                timeSheetEmployeeId,
                positionId,
                phaseId,
                totalWorkTimeSeconds
            )
        } else {
            timeSheetUseCases.editPosition(
                timeSheetEmployeeId,
                timeSheetEmployeePositionId,
                positionId,
                phaseId,
                totalWorkTimeSeconds
            )
        }
    }
}

private fun Phase.toUiModel() = AdditionalPositionViewState.PhaseUiRow(
    phaseId = this.phaseId,
    phaseName = this.phaseName,
    phaseCode = this.phaseCode
)

private fun Position.toUiModel() = AdditionalPositionViewState.PositionUiRow(
    positionId = this.positionId,
    positionName = this.positionName
)

private fun TimesheetEmployeePosition?.toUiModel(): AdditionalPositionViewState.TimeSheetEmployeePositionUiModel? {
    return this?.let {
        AdditionalPositionViewState.TimeSheetEmployeePositionUiModel(
            positionId = it.positionId,
            phaseId = it.phaseId,
            totalWorkTime = it.totalWorkTime
        )
    }
}
