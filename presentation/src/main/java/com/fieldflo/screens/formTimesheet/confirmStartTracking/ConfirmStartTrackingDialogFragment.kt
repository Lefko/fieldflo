package com.fieldflo.screens.formTimesheet.confirmStartTracking

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.fieldflo.R

class ConfirmStartTrackingDialogFragment : DialogFragment() {

    private val timeSheetEmployeeId by lazy {
        arguments!!.getInt(TIMESHEET_EMPLOYEE_ID)
    }

    private val timeSheetEmployeeName by lazy {
        arguments!!.getString(TIMESHEET_EMPLOYEE_NAME)
    }

    private lateinit var startTrackingConfirmedListener: StartTrackingConfirmedListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(activity as Context)
            .setMessage(getString(R.string.confirm_start_tracking_time_sheet_employee_title, timeSheetEmployeeName))
            .setPositiveButton(R.string.confirm_start_tracking_time_sheet_employee_positive) { _, _ ->
                startTrackingConfirmedListener.onStartTrackingConfirmed(timeSheetEmployeeId)
            }
            .setNegativeButton(android.R.string.cancel) { _, _ ->
                dismiss()
            }
            .show()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    private fun onAttachToContext(context: Context) {
        startTrackingConfirmedListener = context as? StartTrackingConfirmedListener
            ?: throw IllegalArgumentException("${context::class.java.simpleName} must implement StartTrackingConfirmedListener")
    }

    companion object {
        const val TAG = "ConfirmStartTrackingDialogFragment"
        private const val TIMESHEET_EMPLOYEE_ID = "timesheet_employee_id"
        private const val TIMESHEET_EMPLOYEE_NAME = "timesheet_employee_name"
        @JvmStatic
        fun newInstance(timeSheetEmployeeId: Int, timeSheetEmployeeName: String) =
            ConfirmStartTrackingDialogFragment().apply {
                arguments = Bundle().apply {
                    putInt(TIMESHEET_EMPLOYEE_ID, timeSheetEmployeeId)
                    putString(TIMESHEET_EMPLOYEE_NAME, timeSheetEmployeeName)
                }
            }
    }

    interface StartTrackingConfirmedListener {
        fun onStartTrackingConfirmed(timeSheetEmployeeId: Int)
    }
}