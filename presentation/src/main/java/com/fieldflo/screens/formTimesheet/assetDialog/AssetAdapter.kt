package com.fieldflo.screens.formTimesheet.assetDialog

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import com.fieldflo.R
import java.util.*

class AssetAdapter(context: Context) :
    ArrayAdapter<AssetViewState.InventoryItemUiRow>(context, R.layout.row_asset) {

    private val lock = Any()

    private var objects: MutableList<AssetViewState.InventoryItemUiRow> = mutableListOf()
    private var originalValues: MutableList<AssetViewState.InventoryItemUiRow>? = null

    private val specialItem = AssetViewState.InventoryItemUiRow(0, context.getString(R.string.asset_special_item), 0)

    private var containsSpecial = false

    fun submit(assets: List<AssetViewState.InventoryItemUiRow>) {
        handleSpecialCheck(assets)
        synchronized(lock) {
            if (originalValues != null) {
                originalValues!!.clear()
            } else {
                objects.clear()
            }

            if (originalValues != null) {
                originalValues!!.addAll(assets)
            } else {
                objects.addAll(assets)
            }
        }
        notifyDataSetChanged()
    }

    private fun handleSpecialCheck(assets: List<AssetViewState.InventoryItemUiRow>) {
        assets.forEachIndexed { index, asset ->
            if (asset.inventoryItemId == 0) {
                containsSpecial = true
                assets.toMutableList().removeAt(index)
                return
            }
        }
        containsSpecial = false
    }

    override fun getCount() = objects.size

    override fun getItem(position: Int) = objects[position]

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val rowData = getItem(position)
        val text = rowData.toString() + (if (rowData.inventoryItemNumber != 0) {
            " / ${rowData.inventoryItemNumber}"
        } else {
            ""
        })
        return (super.getView(position, convertView, parent) as TextView).apply {
            this.text = text
        }
    }

    override fun getFilter() = object : Filter() {
        override fun performFiltering(prefix: CharSequence?): Filter.FilterResults {
            val results = Filter.FilterResults()

            if (originalValues == null) {
                synchronized(lock) {
                    originalValues = mutableListOf()
                    originalValues!!.addAll(objects)
                }
            }

            if (prefix == null || prefix.isEmpty()) {
                val list: MutableList<AssetViewState.InventoryItemUiRow>
                synchronized(lock) {
                    list = mutableListOf()
                    list.addAll(originalValues!!)
                }
                results.values = list
                results.count = list.size
            } else {
                val prefixString = prefix.toString().toLowerCase(Locale.ROOT)

                val values: MutableList<AssetViewState.InventoryItemUiRow>
                synchronized(lock) {
                    values = mutableListOf()
                    values.addAll(originalValues!!)
                }

                val count = values.size
                val newValues = mutableListOf<AssetViewState.InventoryItemUiRow>()

                for (i in 0 until count) {
                    val value = values[i]
                    val valueText = value.toString().toLowerCase(Locale.ROOT)
                    val itemNumberText = value.inventoryItemNumber.toString()

                    // First match against the whole, non-splitted value
                    if (valueText.startsWith(prefixString) || itemNumberText.startsWith(prefixString)) {
                        newValues.add(value)
                    } else {
                        val words = valueText.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                        for (word in words) {
                            if (word.startsWith(prefixString)) {
                                newValues.add(value)
                                break
                            }
                        }
                    }
                }

                if (containsSpecial) {
                    newValues.add(specialItem)
                }

                results.values = newValues
                results.count = newValues.size
            }

            return results
        }

        override fun publishResults(constraint: CharSequence?, results: Filter.FilterResults) {
            @Suppress("UNCHECKED_CAST")
            objects = (results.values as List<AssetViewState.InventoryItemUiRow>).toMutableList()
            if (results.count > 0) {
                notifyDataSetChanged()
            } else {
                notifyDataSetInvalidated()
            }
        }
    }
}