package com.fieldflo.screens.formTimesheet.timesheetEmployee

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fieldflo.AppScopeHolder
import com.fieldflo.common.*
import com.fieldflo.data.persistence.db.entities.TimeSheetEmployeePerDiem
import com.fieldflo.usecases.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

class TimeSheetEmployeePresenter @Inject constructor(
    private val timeSheetUseCases: TimeSheetUseCases,
    private val timeSheetTimeClockUseCases: TimeSheetTimeClockUseCases,
    private val projectsUseCases: ProjectsUseCases,
    private val appScopeHolder: AppScopeHolder
) : ViewModel() {

    fun getProjectStatusFlow(projectId: Int): Flow<ProjectStatus> {
        return projectsUseCases.getProjectStatusFlow(projectId)
    }

    suspend fun getTimeSheetEmployeeFlow(timeSheetEmployeeId: Int): Flow<TimeSheetEmployeeViewState> {
        return timeSheetUseCases.getFullTimeSheetEmployeeFlow(timeSheetEmployeeId)
            .map { it.toUiModel() }
    }

    suspend fun getProjectName(projectId: Int): String {
        return projectsUseCases.getProjectName(projectId)
    }

    fun deletePerDiemData(timeSheetEmployeeId: Int) {
        viewModelScope.launch {
            timeSheetUseCases.deletePerDiem(timeSheetEmployeeId)
        }
    }

    fun startTracking(timeSheetEmployeeId: Int) {
        viewModelScope.launch {
            timeSheetTimeClockUseCases.startTrackingEmployees(listOf(timeSheetEmployeeId))
        }
    }

    fun verifyEmployee(
        timeSheetEmployeeId: Int,
        isInjured: Boolean,
        tookFederalBreak: Boolean,
        verifiedByEmployeeId: Int
    ) {
        viewModelScope.launch {
            timeSheetUseCases.setEmployeesVerified(
                listOf(timeSheetEmployeeId),
                isInjured,
                tookFederalBreak,
                verifiedByEmployeeId
            )
        }
    }

    fun deleteAsset(timeSheetEmployeeAssetId: Int) {
        viewModelScope.launch {
            timeSheetUseCases.deleteTimeSheetEmployeeAsset(timeSheetEmployeeAssetId)
        }
    }

    fun setEmployeePosition(timeSheetEmployeeId: Int, positionId: Int) {
        viewModelScope.launch {
            timeSheetUseCases.setEmployeePosition(timeSheetEmployeeId, positionId)
        }
    }

    fun deletePosition(timeSheetEmployeeId: Int, timeSheetEmployeePositionId: Int) {
        viewModelScope.launch {
            timeSheetUseCases.deletePosition(timeSheetEmployeeId, timeSheetEmployeePositionId)
        }
    }

    fun setEmployeePhase(timeSheetEmployeeId: Int, phaseId: Int) {
        viewModelScope.launch {
            timeSheetUseCases.setEmployeePhase(timeSheetEmployeeId, phaseId)
        }
    }

    fun removeEmployee(timeSheetEmployeeId: Int) {
        appScopeHolder.scope.launch {
            timeSheetUseCases.removeEmployeeFromTimeSheet(timeSheetEmployeeId)
        }
    }
}

private fun FullTimeSheetEmployeeUiData.toUiModel(): TimeSheetEmployeeViewState {
    val defaultPosition = this.positions.firstOrNull { it.defaultPosition }
    val otherPositions = this.positions.filter { !it.defaultPosition }
    return TimeSheetEmployeeViewState(
        formDate = this.formDate.toFullString(),
        fixedPhases = this.fixedPhases,
        phaseCode = this.phaseCode,
        usePhases = this.usePhases,
        employeeId = this.employeeId,
        timeSheetEmployeeImageUrl = this.timeSheetEmployeeImageUrl,
        isTracking = this.isTrackingOnAnyTimeSheet,
        startTime = if (this.clockIn.time > 0) this.clockIn.toOnlyTimeString() else "",
        endTime = if (this.clockOut.time > 0) this.clockOut.toOnlyTimeString() else "",
        totalWorkTime = this.totalWorkTime.toTimeString(),
        breakTime = this.breakTime.toTimeString(),
        employeeFullName = this.timeSheetEmployeeName,
        defaultPositionName = defaultPosition?.positionName ?: "",
        defaultPosition = defaultPosition?.toUiModel(this.timeSheetEmployeeId),
        otherPositions = otherPositions.map { it.toUiModel(this.timeSheetEmployeeId) },
        assets = this.assets.map { it.toUiModel() },
        perDiem = this.perDiem?.toUiModel(timeSheetEmployeeId),
        phaseName = if (this.fixedPhases) this.phaseCode else defaultPosition?.phaseName ?: "",
        employeeState = this.employeeStatus,
        timeLogs = emptyList(),
        timeVerifiedDate = "${this.timeVerifiedDate.toDateString()}\n${this.timeVerifiedDate.toOnlyTimeString()}",
        timeVerifiedBy = this.timeVerifiedByName,
        injuryVerifiedDate = "${this.injuryVerifiedDate.toDateString()}\n${this.injuryVerifiedDate.toOnlyTimeString()}",
        injuryVerifiedBy = this.injuryVerifiedByName,
        tookBreak = this.tookBreak,
        breakVerifiedDate = "${this.breakVerifiedDate.toDateString()}\n${this.breakVerifiedDate.toOnlyTimeString()}",
        breakVerifiedBy = this.breakVerifiedByName,
        pinImageDeletingOn = this.pinImageDeletingOn.toDateString(),
        pinImageLocation = this.pinImageLocation
    )
}

private fun TimeSheetEmployeePositionData.toUiModel(timeSheetEmployeeId: Int): TimeSheetEmployeeViewState.TimeSheetEmployeePositionUiModel {
    return TimeSheetEmployeeViewState.TimeSheetEmployeePositionUiModel(
        timeSheetEmployeePositionId = this.timeSheetEmployeePositionId,
        timeSheetEmployeeId = timeSheetEmployeeId,
        positionName = this.positionName,
        defaultPosition = this.defaultPosition,
        totalWorkTime = this.totalWorkTime,
        positionId = this.positionId,
    )
}

private fun TimeSheetEmployeeAssetData.toUiModel(): TimeSheetEmployeeViewState.TimeSheetAssetEntityUiModel {
    return TimeSheetEmployeeViewState.TimeSheetAssetEntityUiModel(
        timeSheetEmployeeAssetId = this.timeSheetEmployeeAssetId,
        assetName = this.assetName,
        assetNumber = this.assetNumber,
        totalWorkTime = this.totalWorkTime.toTimeString()
    )
}

private fun TimeSheetEmployeePerDiem.toUiModel(timeSheetEmployeeId: Int): TimeSheetEmployeeViewState.TimeSheetEmployeePerDiemUiModel {
    return TimeSheetEmployeeViewState.TimeSheetEmployeePerDiemUiModel(
        timeSheetEmployeeId = timeSheetEmployeeId,
        perDiemType = this.perDiemType.capitalize(),
        totalOwed = "\$${this.totalOwed.round(2)}"
    )
}
