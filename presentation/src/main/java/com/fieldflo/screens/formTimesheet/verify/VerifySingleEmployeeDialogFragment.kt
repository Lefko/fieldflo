package com.fieldflo.screens.formTimesheet.verify

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.*
import com.fieldflo.common.hiddenCamera.CameraCallbacks
import com.fieldflo.common.hiddenCamera.CameraConfig
import com.fieldflo.common.hiddenCamera.CameraPreview
import com.fieldflo.common.hiddenCamera.config.CameraFacing
import com.fieldflo.common.hiddenCamera.config.CameraImageFormat
import com.fieldflo.common.hiddenCamera.config.CameraResolution
import com.fieldflo.common.hiddenCamera.config.CameraRotation
import com.fieldflo.di.injector
import com.fieldflo.usecases.VerifyTimeSheetPinResult
import kotlinx.coroutines.flow.collect
import timber.log.Timber
import java.io.File
import java.util.concurrent.atomic.AtomicBoolean

class VerifySingleEmployeeDialogFragment : DialogFragment(), CameraCallbacks {

    private lateinit var positiveButton: Button
    private lateinit var pinET: EditText
    private lateinit var callback: VerifyDialogCallback
    private lateinit var errorTV: TextView
    private lateinit var timeNotCorrectTV: TextView
    private lateinit var cameraPreview: CameraPreview
    private lateinit var breakTV: TextView
    private lateinit var everythingCorrectRG: RadioGroup
    private lateinit var employeeName: TextView
    private lateinit var startTV: TextView
    private lateinit var endTV: TextView
    private lateinit var positionsContainerLL: LinearLayout
    private lateinit var assetsContainerLL: LinearLayout
    private lateinit var perDiemTypeTV: TextView
    private lateinit var perDiemTotalOwedTV: TextView
    private lateinit var injuredQuestionRG: RadioGroup
    private lateinit var injured: RadioButton
    private lateinit var messageInjured: TextView
    private lateinit var breakQuestionRG: RadioGroup
    private lateinit var tookBreakRB: RadioButton
    private lateinit var agreeBtn: CheckBox

    private val timeSheetEmployeeId: Int by lazy { arguments!!.getInt(TIME_SHEET_EMPLOYEE_ID_KEY) }
    private val employeeId: Int by lazy { arguments!!.getInt(EMPLOYEE_ID_KEY) }
    private val internalFormId by lazy { arguments!!.getInt(INTERNAL_FORM_ID_KEY) }

    private val cameraInitialized = AtomicBoolean(false)

    private val positiveEnabled
        get() = everythingCorrectRG.checkedRadioButtonId == R.id.dialog_activity_verify_working_day_everything_correct_yes &&
                agreeBtn.isChecked && pinET.text.isNotEmpty()

    private val showCantVerifyMessage
        get() = !agreeBtn.isChecked || everythingCorrectRG.checkedRadioButtonId != R.id.dialog_activity_verify_working_day_everything_correct_yes

    private val presenter by viewModels<VerifyPresenter> { injector.verifyViewModelFactory() }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val viewMain = View.inflate(context, R.layout.dialog_single_employee_verify, null)

        findViews(viewMain)

        initViews()

        return AlertDialog.Builder(activity as Context)
            .setView(viewMain)
            .setPositiveButton(R.string.dialog_single_employee_verify_verify_btn) { _, _ -> }
            .setNegativeButton(android.R.string.cancel) { _, _ -> dismiss() }
            .show()
    }

    private fun findViews(viewMain: View) {
        (viewMain as? ScrollView)?.scrollTo(0, 0)

        val root =
            viewMain.findViewById<ConstraintLayout>(R.id.dialog_activity_verify_working_day_rootCL)
        cameraPreview = createPreview(root)
        errorTV = viewMain.findViewById(R.id.dialog_activity_verify_working_day_incorrect_pinTV)
        timeNotCorrectTV =
            viewMain.findViewById(R.id.dialog_activity_verify_working_day_incorrect_timeTV)
        pinET = viewMain.findViewById(R.id.dialog_activity_verify_working_day_enter_pin)

        everythingCorrectRG =
            viewMain.findViewById(R.id.dialog_activity_verify_working_day_everything_correct_radiogroup)
        employeeName = viewMain.findViewById(R.id.dialog_activity_verify_working_day_employee_name)
        breakTV = viewMain.findViewById(R.id.dialog_activity_verify_working_day_break_value)
        startTV = viewMain.findViewById(R.id.dialog_activity_verify_working_day_start_value)
        endTV = viewMain.findViewById(R.id.dialog_activity_verify_working_day_end_value)
        positionsContainerLL =
            viewMain.findViewById(R.id.dialog_activity_verify_working_day_positions_container)
        assetsContainerLL =
            viewMain.findViewById(R.id.dialog_activity_verify_working_day_assets_container)
        perDiemTypeTV =
            viewMain.findViewById(R.id.dialog_activity_verify_working_day_per_diem_value)
        perDiemTotalOwedTV =
            viewMain.findViewById(R.id.dialog_activity_verify_working_day_total_owed_value)
        injuredQuestionRG =
            viewMain.findViewById(R.id.dialog_activity_verify_working_day_injured_question_radiogroup)
        injured =
            viewMain.findViewById(R.id.dialog_activity_verify_working_day_injured_question_yes)
        messageInjured =
            viewMain.findViewById(R.id.dialog_activity_verify_working_day_message_injuredTV)
        breakQuestionRG =
            viewMain.findViewById(R.id.dialog_activity_verify_working_day_federal_break_question_radiogroup)
        tookBreakRB =
            viewMain.findViewById(R.id.dialog_activity_verify_working_day_federal_break_question_yes)
        agreeBtn = viewMain.findViewById(R.id.dialog_activity_verify_working_time_verify_icon)
    }

    private fun initViews() {
        agreeBtn.setOnClickListener {
            positiveButton.isEnabled = positiveEnabled
            if (showCantVerifyMessage) {
                timeNotCorrectTV.visible()
            } else {
                timeNotCorrectTV.gone()
            }
        }

        injuredQuestionRG.setOnCheckedChangeListener { _, _ ->
            presenter.setIsInjured(injured.id == injuredQuestionRG.checkedRadioButtonId)
        }

        breakQuestionRG.setOnCheckedChangeListener { _, _ ->
            presenter.setTookBreak(tookBreakRB.id == breakQuestionRG.checkedRadioButtonId)
        }

        everythingCorrectRG.setOnCheckedChangeListener { _, _ ->
            positiveButton.isEnabled = positiveEnabled
            if (showCantVerifyMessage) {
                timeNotCorrectTV.visible()
            } else {
                timeNotCorrectTV.gone()
            }
        }

        pinET.afterTextChanges { _, _ ->
            errorTV.gone()
            positiveButton.isEnabled = positiveEnabled
        }
    }

    override fun onStop() {
        super.onStop()
        stopCamera()
    }

    override fun onStart() {
        super.onStart()
        positiveButton = (dialog as AlertDialog).getButton(Dialog.BUTTON_POSITIVE).apply {
            isEnabled = false
        }

        present()
    }

    private fun present() {
        lifecycleScope.launchWhenCreated {
            presenter.viewState.collect { render(it) }
        }

        presenter.loadInitialViewState(internalFormId, timeSheetEmployeeId)

        positiveButton.safeClickListener {
            lifecycleScope.launchWhenCreated {
                val pinResult =
                    presenter.verifyPin(internalFormId, employeeId, pinET.text.toString())
                when (pinResult) {
                    VerifyTimeSheetPinResult.VerifyError -> errorTV.visible()
                    is VerifyTimeSheetPinResult.VerifySuccess -> onPinVerified(
                        needsPhotoPin = presenter.requiresPhotoPin,
                        isInjured = injured.id == injuredQuestionRG.checkedRadioButtonId,
                        tookBreak = tookBreakRB.id == breakQuestionRG.checkedRadioButtonId,
                        verifiedByEmployeeId = pinResult.verifiedByEmployeeId
                    )
                }
            }
        }
    }

    private fun render(viewState: VerifyViewState) {
        if (viewState.needsPhotoPin && viewState.pinFileDestLocation.isNotEmpty()) {
            setupCamera(viewState.pinFileDestLocation)
        }

        renderEmployeeDetails(viewState)
    }


    private fun setupCamera(pinFileDestination: String) {
        if (!activity!!.hasPermission(Manifest.permission.CAMERA)) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), CAMERA_PERMISSION_REQUEST)
        } else {
            if (!cameraInitialized.get() && pinFileDestination.isNotEmpty()) {
                val cameraConfig = CameraConfig.Builder(activity!!)
                    .facing(CameraFacing.FRONT_FACING_CAMERA)
                    .resolution(CameraResolution.LOW_RESOLUTION)
                    .imageFormat(CameraImageFormat.FORMAT_JPEG)
                    .imageRotation(CameraRotation.ROTATION_270)
                    .imageFile(File(pinFileDestination))
                    .build()
                cameraPreview.startCameraInternal(cameraConfig)
                cameraInitialized.set(true)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CAMERA_PERMISSION_REQUEST) {
            setupCamera(presenter.currentViewState.pinFileDestLocation)
        }
    }

    private fun onPinVerified(
        needsPhotoPin: Boolean,
        isInjured: Boolean,
        tookBreak: Boolean,
        verifiedByEmployeeId: Int
    ) {
        val takePic = cameraInitialized.get() && needsPhotoPin
                && requireActivity().hasPermission(Manifest.permission.CAMERA)

        if (takePic) {
            cameraPreview.takePictureInternal()
        }

        callback.onVerifyTracking(
            timeSheetEmployeeId = timeSheetEmployeeId,
            isInjured = isInjured,
            tookFederalBreak = tookBreak,
            verifiedByEmployeeId = verifiedByEmployeeId
        )

        if (!takePic) dismiss()
    }

    private fun renderEmployeeDetails(viewState: VerifyViewState) {
        viewState.timeSheetEmployee.let {
            employeeName.text = it.timeSheetEmployeeName
            breakTV.text = it.breakTime
            startTV.text = it.startTimeStamp
            endTV.text = it.endTimeStamp
            perDiemTypeTV.text = it.perDiemType
            perDiemTotalOwedTV.text = it.perDiemTotalOwed
            if (it.injured) messageInjured.visible() else messageInjured.gone()
            renderPositions(it.positions)
            renderAssets(it.assets)
        }
    }

    private fun renderPositions(positions: List<VerifyViewState.PositionUiModel>) {
        positionsContainerLL.removeAllViews()
        context?.let {
            val inflater = LayoutInflater.from(it)
            positions.forEach { position ->
                val row = inflater.inflate(
                    R.layout.row_dialog_verify_single_title_and_value,
                    positionsContainerLL,
                    true
                )
                row.findViewById<TextView>(R.id.row_dialog_verify_single_title_and_value_title)
                    .text = position.positionName
                row.findViewById<TextView>(R.id.row_dialog_verify_single_title_and_value_value)
                    .text = position.positionTime
            }
        }
    }

    private fun renderAssets(assets: List<VerifyViewState.AssetUiModel>) {
        assetsContainerLL.removeAllViews()
        context?.let {
            val inflater = LayoutInflater.from(it)
            assets.forEach { asset ->
                val row = inflater.inflate(
                    R.layout.row_dialog_verify_single_title_and_value,
                    assetsContainerLL,
                    true
                )
                row.findViewById<TextView>(R.id.row_dialog_verify_single_title_and_value_title)
                    .text = asset.assetName
                row.findViewById<TextView>(R.id.row_dialog_verify_single_title_and_value_value)
                    .text = asset.assetTime
            }
        }
    }

    private fun onAttachToContext(context: Context) {
        this.callback = context as? VerifySingleEmployeeDialogFragment.VerifyDialogCallback
            ?: throw IllegalArgumentException(
                "${context.javaClass.simpleName} must be an instance of VerifyDialogCallback"
            )
    }

    private fun createPreview(constraintLayout: ConstraintLayout): CameraPreview {
        val context = constraintLayout.context
        val cameraPreview = CameraPreview(context, this)

        cameraPreview.id = View.generateViewId()
        cameraPreview.layoutParams = ConstraintLayout.LayoutParams(1, 1)

        constraintLayout.addView(cameraPreview)

        val set = ConstraintSet()
        set.clone(constraintLayout)
        set.connect(cameraPreview.id, ConstraintSet.START, constraintLayout.id, ConstraintSet.START)
        set.connect(
            cameraPreview.id,
            ConstraintSet.BOTTOM,
            constraintLayout.id,
            ConstraintSet.BOTTOM
        )
        set.applyTo(constraintLayout)

        return cameraPreview
    }

    private fun stopCamera() {
        cameraPreview.stopPreviewAndFreeCamera()
    }

    override fun onImageCapture(imageFile: File) {
        dismiss()
    }

    override fun onCameraError(errorCode: Int) {
        Timber.d("Error taking pic")
    }

    companion object {
        const val TAG = "VerifySingleEmployeeDialogFragment"
        private const val TIME_SHEET_EMPLOYEE_ID_KEY = "timesheet_employee_id_key"
        private const val EMPLOYEE_ID_KEY = "employee_pin_key"
        private const val INTERNAL_FORM_ID_KEY = "internal_form_id_key"

        private const val CAMERA_PERMISSION_REQUEST = 1337

        @JvmStatic
        fun newInstance(
            internalFormId: Int,
            timeSheetEmployeeId: Int,
            employeeId: Int
        ): VerifySingleEmployeeDialogFragment {
            return VerifySingleEmployeeDialogFragment().apply {
                arguments = Bundle().apply {
                    putInt(INTERNAL_FORM_ID_KEY, internalFormId)
                    putInt(TIME_SHEET_EMPLOYEE_ID_KEY, timeSheetEmployeeId)
                    putInt(EMPLOYEE_ID_KEY, employeeId)
                }
            }
        }
    }

    interface VerifyDialogCallback {
        fun onVerifyTracking(
            timeSheetEmployeeId: Int,
            isInjured: Boolean,
            tookFederalBreak: Boolean,
            verifiedByEmployeeId: Int
        )
    }
}

