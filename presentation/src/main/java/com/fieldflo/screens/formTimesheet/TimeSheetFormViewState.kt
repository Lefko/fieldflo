package com.fieldflo.screens.formTimesheet


data class TimeSheetFormViewState(
    private val editTimeAllowed: Boolean,
    val formDataId: Int = 0,
    val projectName: String = "",
    val formDate: String? = null,
    val supervisorInfo: TimeSheetSupervisor? = null,
    val timeSheetEmployees: List<TimeSheetEmployeeUiModel> = listOf()
) {
    private val selectedRows
        get() = timeSheetEmployees.filter { it.isRowSelected }

    val startEnabled
        get() = selectedRows.isNotEmpty() && selectedRows.all {
            it.employeeState == TimeSheetEmployeeState.READY
        }

    val stopEnabled
        get() = selectedRows.isNotEmpty() && selectedRows.all {
            it.employeeState == TimeSheetEmployeeState.TRACKING
        }

    val verifyEnabled
        get() = selectedRows.isNotEmpty() && selectedRows.all {
            it.employeeState == TimeSheetEmployeeState.MUST_VERIFY
        }

    val editTimeEnabled
        get() = editTimeAllowed && selectedRows.isNotEmpty() && selectedRows.all {
            it.employeeState != TimeSheetEmployeeState.NOT_READY
        }

    val addSupervisorEnabled
        get() = supervisorInfo != null

    val allSelected
        get() = timeSheetEmployees.isNotEmpty() && timeSheetEmployees.all { it.isRowSelected }

    data class TimeSheetSupervisor(
        val supervisorId: Int,
        val supervisorFullName: String
    )

    data class TimeSheetEmployeeUiModel(
        val timeSheetEmployeeId: Int = 0,
        val employeeId: Int = 0,
        val avatarUrl: String = "",
        val employeeFullName: String = "",
        val phaseName: String = "",
        val positionName: String = "",
        val usePhases: Boolean = false,
        val employeeState: TimeSheetEmployeeState = TimeSheetEmployeeState.NOT_READY,
        val isTracking: Boolean = false,
        private val fixedPhases: Boolean,
        val isRowSelected: Boolean = false
    ) {
        val editPositionEnabled
            get() = employeeState == TimeSheetEmployeeState.READY || employeeState == TimeSheetEmployeeState.NOT_READY

        val editPhaseEnabled
            get() = usePhases && positionName.isNotEmpty() && !fixedPhases &&
                    (employeeState == TimeSheetEmployeeState.READY || employeeState == TimeSheetEmployeeState.NOT_READY)

    }
}