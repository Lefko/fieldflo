package com.fieldflo.screens.formTimesheet.editTime

import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.EditTimeResult
import com.fieldflo.usecases.TimeSheetTimeClockUseCases
import com.fieldflo.usecases.TimeSheetUseCases
import com.fieldflo.usecases.VerifyTimeSheetPinResult
import java.util.*
import javax.inject.Inject

class EditTimePresenter @Inject constructor(
    private val timeSheetTimeClockUseCases: TimeSheetTimeClockUseCases,
    private val timeSheetUseCases: TimeSheetUseCases
) : ViewModel() {

    suspend fun getTimeSheetEmployeeStartStop(timeSheetEmployeeIds: List<Int>): EditTimeViewState {
        return if (timeSheetEmployeeIds.size == 1) {
            val timeData =
                timeSheetTimeClockUseCases.getTimeSheetEmployeeTimeData(timeSheetEmployeeIds[0])
            EditTimeViewState(
                _startTime = timeData.startDate,
                _endTime = timeData.endDate,
                _breakDurationSeconds = timeData.breakDurationSeconds
            )
        } else {
            EditTimeViewState(_startTime = null, _endTime = null, _breakDurationSeconds = null)
        }
    }

    suspend fun editTime(editTimeData: EditTimeData): EditTimeResult {
        val pinResult = timeSheetUseCases.verifyTimeSheetPin(
            editTimeData.internalFormId,
            pin = editTimeData.pin
        )

        return when (pinResult) {
            VerifyTimeSheetPinResult.VerifyError -> EditTimeResult.PinFailure
            is VerifyTimeSheetPinResult.VerifySuccess ->
                timeSheetTimeClockUseCases.editTime(editTimeData)
        }
    }
}

data class EditTimeData(
    val projectId: Int,
    val internalFormId: Int,
    val timeSheetEmployeeIds: List<Int>,
    val pin: String,
    val startDate: Date?,
    val endDate: Date?,
    val breakDurationSeconds: Int?,
    val dontChangeStart: Boolean,
    val dontChangeEnd: Boolean,
    val dontChangeBreak: Boolean
)
