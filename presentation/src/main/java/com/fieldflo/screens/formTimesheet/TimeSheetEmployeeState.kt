package com.fieldflo.screens.formTimesheet

import com.fieldflo.data.persistence.db.entities.Project
import com.fieldflo.data.persistence.db.entities.TimeSheetEmployee
import com.fieldflo.data.persistence.db.entities.TimesheetEmployeePosition
import com.fieldflo.data.persistence.db.entities.TimesheetEmployeeStatus

enum class TimeSheetEmployeeState {
    NOT_READY,
    READY,
    TRACKING,
    MUST_VERIFY,
    VERIFIED,
    VERIFIED_WITH_INJURY;

    fun isAtLeast(otherState: TimeSheetEmployeeState) = ordinal >= otherState.ordinal

    fun isGreaterThan(otherState: TimeSheetEmployeeState) = ordinal > otherState.ordinal

    companion object {
        fun create(
            project: Project,
            isTrackingOnTimesheet: Boolean,
            defaultPosition: TimesheetEmployeePosition?,
            timeSheetEmployee: TimeSheetEmployee
        ): TimeSheetEmployeeState {
            return if (project.usePhases && project.phaseCode.isEmpty() && (defaultPosition == null || defaultPosition.phaseId == 0)) {
                // project uses phases and phase is not set
                TimeSheetEmployeeState.NOT_READY
            } else if (defaultPosition == null || defaultPosition.positionId == 0) {
                // position not set
                TimeSheetEmployeeState.NOT_READY
            } else if (timeSheetEmployee.employeeStatus == TimesheetEmployeeStatus.VERIFIED) {
                TimeSheetEmployeeState.VERIFIED
            } else if (timeSheetEmployee.employeeStatus == TimesheetEmployeeStatus.VERIFIED_WITH_INJURY) {
                TimeSheetEmployeeState.VERIFIED_WITH_INJURY
            } else if (timeSheetEmployee.employeeStatus == TimesheetEmployeeStatus.CLOCKED_OUT) {
                TimeSheetEmployeeState.MUST_VERIFY
            } else if (timeSheetEmployee.employeeStatus == TimesheetEmployeeStatus.CLOCKED_IN) {
                TimeSheetEmployeeState.TRACKING
            } else if (isTrackingOnTimesheet) {
                // this employee is tracking on a timesheet
                TimeSheetEmployeeState.NOT_READY
            } else {
                TimeSheetEmployeeState.READY
            }
        }
    }
}