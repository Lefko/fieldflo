package com.fieldflo.screens.formTimesheet.stopTrackingSingleEmployee

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.afterTextChanges
import com.fieldflo.common.gone
import com.fieldflo.common.safeClickListener
import com.fieldflo.common.visible
import com.fieldflo.di.injector
import kotlinx.android.synthetic.main.dialog_stop_tracking.view.*

class StopTrackingSingleEmployeeDialogFragment : DialogFragment() {

    private lateinit var positiveButton: Button
    private lateinit var verifyInfoCB: CheckBox
    private lateinit var pinET: EditText
    private lateinit var spinner: Spinner
    private lateinit var errorTV: TextView

    private val timeSheetEmployeeId: Int by lazy { arguments!!.getInt(TIME_SHEET_EMPLOYEE_ID_KEY) }
    private val employeeId: Int by lazy { arguments!!.getInt(EMPLOYEE_ID_KEY) }
    private val projectId: Int by lazy { arguments!!.getInt(PROJECT_ID_KEY) }
    private val internalFormId by lazy { arguments!!.getInt(INTERNAL_FORM_ID_KEY) }

    private val positiveEnabled
        get() = pinET.text.isNotEmpty() && verifyInfoCB.isChecked

    private val presenter by viewModels<StopTrackingSingleEmployeePresenter> { injector.stopTrackingViewModelFactory() }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_stop_tracking, null)

        errorTV = view.dialog_stop_tracking_incorrect_pinTV
        verifyInfoCB = view.dialog_stop_tracking_checkBox

        pinET = view.dialog_stop_tracking_pin
        pinET.afterTextChanges { _, _ ->
            run {
                positiveButton.isEnabled = positiveEnabled
                errorTV.gone()
            }
        }
        view.context?.let { context ->
            spinner = view.dialog_stop_tracking_break_time
            spinner.adapter = ArrayAdapter.createFromResource(
                context,
                R.array.dialog_stop_tracking_break_time_list, R.layout.row_spinner_dropdown
            ).apply {
                setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            }
        }
        verifyInfoCB.setOnClickListener {
            positiveButton.isEnabled = positiveEnabled
        }
        return AlertDialog.Builder(activity as Context)
            .setView(view)
            .setPositiveButton(android.R.string.ok) { _, _ -> }
            .setNegativeButton(android.R.string.cancel) { _, _ -> dismiss() }
            .show()
    }

    private fun render(result: StopTrackingResult) {
        @Suppress("NON_EXHAUSTIVE_WHEN")
        when (result) {
            StopTrackingResult.SUCCESS -> {
                dismiss()
            }
            StopTrackingResult.PIN_FAILURE -> {
                errorTV.setText(R.string.dialog_stop_tracking_incorrect_pin)
                errorTV.visible()
            }
            StopTrackingResult.BREAK_TOO_LONG_FAILURE -> {
                errorTV.setText(R.string.dialog_stop_tracking_break_too_long)
                errorTV.visible()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        positiveButton = (dialog as AlertDialog).getButton(Dialog.BUTTON_POSITIVE).apply {
            isEnabled = false
        }
        positiveButton.safeClickListener {
            lifecycleScope.launchWhenCreated {
                val result = presenter.stopTrackingEmployee(
                    projectId,
                    internalFormId,
                    employeeId,
                    timeSheetEmployeeId,
                    convertToSecond(spinner.selectedItemPosition),
                    pinET.text.toString()
                )
                render(result)
            }
        }
    }

    private fun convertToSecond(position: Int): Int {
        return when (position) {
            0 -> 0 // dialog_stop_tracking_break_time_no
            1 -> 300 // dialog_stop_tracking_break_time_5
            2 -> 600 // dialog_stop_tracking_break_time_10
            3 -> 900 // dialog_stop_tracking_break_time_15
            4 -> 1200 // dialog_stop_tracking_break_time_20
            5 -> 1500 // dialog_stop_tracking_break_time_25
            6 -> 1800 // dialog_stop_tracking_break_time_30
            7 -> 2100 // dialog_stop_tracking_break_time_35
            8 -> 2400 // dialog_stop_tracking_break_time_40
            9 -> 2700 // dialog_stop_tracking_break_time_45
            10 -> 3000 // dialog_stop_tracking_break_time_50
            11 -> 3300 // dialog_stop_tracking_break_time_55
            12 -> 3600 // dialog_stop_tracking_break_time_60
            13 -> 3900 // dialog_stop_tracking_break_time_65
            14 -> 4200 // dialog_stop_tracking_break_time_70
            15 -> 4500 // dialog_stop_tracking_break_time_75
            16 -> 4800 // dialog_stop_tracking_break_time_80
            17 -> 5100 // dialog_stop_tracking_break_time_85
            18 -> 5400 // dialog_stop_tracking_break_time_90
            19 -> 5700 // dialog_stop_tracking_break_time_95
            20 -> 6000 // dialog_stop_tracking_break_time_100
            21 -> 6300 // dialog_stop_tracking_break_time_105
            22 -> 6600 // dialog_stop_tracking_break_time_110
            23 -> 6900 // dialog_stop_tracking_break_time_115
            24 -> 7200  // dialog_stop_tracking_break_time_120
            25 -> 9000 // dialog_stop_tracking_break_time_2h_30
            26 -> 10800 // dialog_stop_tracking_break_time_3h
            27 -> 12600 // dialog_stop_tracking_break_time_3h_30
            28 -> 14400 // dialog_stop_tracking_break_time_4h
            29 -> 16200 // dialog_stop_tracking_break_time_4h_30
            30 -> 18000 // dialog_stop_tracking_break_time_5h
            31 -> 19800 // dialog_stop_tracking_break_time_5h_30
            32 -> 21600 // dialog_stop_tracking_break_time_6h
            33 -> 23400 // dialog_stop_tracking_break_time_6h_30
            34 -> 25200 // dialog_stop_tracking_break_time_7h
            35 -> 27000 // dialog_stop_tracking_break_time_7h_30
            36 -> 28800 // dialog_stop_tracking_break_time_8h
            37 -> 30600 // dialog_stop_tracking_break_time_8h_30
            else -> 0
        }
    }

    companion object {

        const val TAG = "StopTrackingSingleEmployeeDialogFragment"
        private const val TIME_SHEET_EMPLOYEE_ID_KEY = "timesheet_employee_id_key"
        private const val EMPLOYEE_ID_KEY = "employee_pin_key"
        private const val PROJECT_ID_KEY = "project_id_key"
        private const val INTERNAL_FORM_ID_KEY = "internal_form_id_key"

        fun newInstance(
            projectId: Int,
            internalFormId: Int,
            timeSheetEmployeeId: Int,
            employeeId: Int
        ) =
            StopTrackingSingleEmployeeDialogFragment().apply {
                arguments = Bundle().apply {
                    putInt(PROJECT_ID_KEY, projectId)
                    putInt(INTERNAL_FORM_ID_KEY, internalFormId)
                    putInt(TIME_SHEET_EMPLOYEE_ID_KEY, timeSheetEmployeeId)
                    putInt(EMPLOYEE_ID_KEY, employeeId)
                }
            }
    }
}

