package com.fieldflo.screens.formTimesheet.editTime

import java.util.*

data class EditTimeViewState(
    private val _startTime: Date? = null,
    private val _endTime: Date? = null,
    private val _breakDurationSeconds: Int? = null
) {
    val startTime: Date
        get() = _startTime ?: Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY, 8)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }.time

    val endTime: Date
        get() = _endTime ?: Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY, 16)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }.time

    val breakDurationSeconds: Int
        get() = _breakDurationSeconds ?: 0
}
