package com.fieldflo.screens.formTimesheet.perDiemDialog

import com.fieldflo.common.toTimeString

data class PerDiemViewState(
    val timeSheetEmployeeId: Int,

    val perDiemType: String,
    val totalOwed: String,
    val totalEarned: String,
    val cashGivenOnSite: Double,
    val totalWorkTime: Int,
    val notes: String,

    private val hourlyAmount: Double,
    private val dailyAmount: Double
) {
    val cashGivenOnSiteString: String
        get() = if (cashGivenOnSite > 0) cashGivenOnSite.toString() else ""

    val dailyAmountString
        get() = if (dailyAmount > 0) dailyAmount.toString() else ""

    val hourlyAmountString
        get() = if (hourlyAmount > 0) hourlyAmount.toString() else ""

    val totalWorkTimeString
        get() = totalWorkTime.toTimeString()
}
