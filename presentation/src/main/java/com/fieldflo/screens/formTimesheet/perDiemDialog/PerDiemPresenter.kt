package com.fieldflo.screens.formTimesheet.perDiemDialog

import androidx.lifecycle.ViewModel
import com.fieldflo.common.formatPlaces
import com.fieldflo.common.round
import com.fieldflo.data.persistence.db.entities.TimeSheetEmployeePerDiem
import com.fieldflo.usecases.TimeSheetUseCases
import javax.inject.Inject

class PerDiemPresenter @Inject constructor(
    private val timeSheetUseCases: TimeSheetUseCases
) : ViewModel() {

    suspend fun getPerDiemViewState(
        timeSheetEmployeeId: Int
    ): PerDiemViewState {
        return timeSheetUseCases.getTimeSheetEmployeePerDiem(timeSheetEmployeeId)
            .toUiModel(timeSheetEmployeeId)
    }

    suspend fun savePerDiem(
        timeSheetEmployeeId: Int,
        isHourly: Boolean,
        amount: Double,
        cashGiven: Double,
        notes: String
    ) {
        timeSheetUseCases.setPerDiem(timeSheetEmployeeId, isHourly, amount, cashGiven, notes)
    }
}

private fun TimeSheetEmployeePerDiem?.toUiModel(
    timeSheetEmployeeId: Int
) = PerDiemViewState(
    timeSheetEmployeeId = timeSheetEmployeeId,
    perDiemType = this?.perDiemType ?: "",
    totalOwed = "\$${(this?.totalOwed ?: 0.0).formatPlaces(2)}",
    totalEarned = "\$${(this?.totalEarned ?: 0.0).formatPlaces(2)}",
    dailyAmount = if (this?.dailyAmount ?: 0.0 > 0) {
        (this?.dailyAmount ?: 0.0).round(2)
    } else {
        0.0
    },
    hourlyAmount = if (this?.hourlyAmount ?: 0.0 > 0) {
        (this?.hourlyAmount ?: 0.0).round(2)
    } else {
        0.0
    },
    cashGivenOnSite = this?.cashGivenOnSite ?: 0.0,
    totalWorkTime = this?.totalWorkTime ?: 0,
    notes = this?.notes ?: ""
)