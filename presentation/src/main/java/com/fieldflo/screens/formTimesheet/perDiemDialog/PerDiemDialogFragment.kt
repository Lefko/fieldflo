package com.fieldflo.screens.formTimesheet.perDiemDialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.afterTextChanges
import com.fieldflo.common.formatPlaces
import com.fieldflo.di.injector
import kotlinx.android.synthetic.main.dialog_activity_per_diem.view.*

class PerDiemDialogFragment : DialogFragment() {
    private lateinit var fragmentView: View

    private val presenter by viewModels<PerDiemPresenter> { injector.perDiemViewModelFactory() }
    private val timeSheetEmployeeId by lazy { arguments!!.getInt(TIME_SHEET_EMPLOYEE_ID) }

    companion object {
        const val TAG = "PerDiemDialogFragment"
        private const val TIME_SHEET_EMPLOYEE_ID = "time_sheet_employee_id"

        fun newInstance(timeSheetEmployeeId: Int) =
            PerDiemDialogFragment().apply {
                arguments = bundleOf(TIME_SHEET_EMPLOYEE_ID to timeSheetEmployeeId)
            }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        fragmentView = View.inflate(context, R.layout.dialog_activity_per_diem, null)

        val dialog = AlertDialog.Builder(activity as Context)
            .setView(fragmentView)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                lifecycleScope.launchWhenCreated {
                    presenter.savePerDiem(
                        timeSheetEmployeeId = timeSheetEmployeeId,
                        isHourly = fragmentView.dialog_activity_per_diem_hourly.isChecked,
                        amount = getAmount(),
                        cashGiven = getCashGiven(),
                        notes = fragmentView.dialog_activity_per_diem_notes_edit_value.text.toString()
                    )
                    dismiss()
                }
            }
            .setNegativeButton(android.R.string.cancel) { _, _ -> dismiss() }
            .show()

        dialog.window?.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
            setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        }

        initView(fragmentView)

        lifecycleScope.launchWhenCreated {
            render(presenter.getPerDiemViewState(timeSheetEmployeeId))
        }

        return dialog
    }

    private fun initView(view: View) {
        view.dialog_activity_per_diem_daily.setOnClickListener { renderDaily() }
        view.dialog_activity_per_diem_hourly.setOnClickListener { renderHourly() }
        view.dialog_activity_per_diem_daily_edit_value.afterTextChanges { _, _ -> renderTotal() }
        view.dialog_activity_per_diem_hourly_edit_value.afterTextChanges { _, _ -> renderTotal() }
        view.dialog_activity_per_diem_cash_given_edit_value.afterTextChanges { _, _ -> renderTotal() }
    }

    private fun render(state: PerDiemViewState) {
        val perDiemHourlyTitle =
            "${getString(R.string.dialog_activity_per_diem_per_diem_hour_title)} ${state.totalWorkTimeString}"
        fragmentView.dialog_activity_per_diem_cash_given_edit_value.setText(state.cashGivenOnSiteString)
        fragmentView.dialog_activity_per_diem_notes_edit_value.setText(state.notes)
        fragmentView.dialog_activity_per_diem_total_owed_value.text = state.totalOwed
        fragmentView.dialog_activity_per_diem_hour_title.text = perDiemHourlyTitle
        fragmentView.dialog_activity_per_diem_hour_title.tag = state.totalWorkTime
        fragmentView.dialog_activity_per_diem_hour_value.text = state.totalEarned
        when (state.perDiemType) {
            "hourly" -> renderHourly(state.hourlyAmountString)
            "daily" -> renderDaily(state.dailyAmountString)
        }
    }

    private fun renderHourly(hourlyAmountString: String = fragmentView.dialog_activity_per_diem_hourly_edit_value.text.toString()) {
        fragmentView.apply {
            this.dialog_activity_per_diem_hourly.isChecked = true
            this.dialog_activity_per_diem_hourly_edit_value.isEnabled = true
            this.dialog_activity_per_diem_hourly_edit_value.setText(hourlyAmountString)

            this.dialog_activity_per_diem_daily.isChecked = false
            this.dialog_activity_per_diem_daily_edit_value.isEnabled = false
        }

        renderTotal()
    }

    private fun renderDaily(dailyAmountString: String = fragmentView.dialog_activity_per_diem_daily_edit_value.text.toString()) {
        fragmentView.apply {
            this.dialog_activity_per_diem_hourly.isChecked = false
            this.dialog_activity_per_diem_hourly_edit_value.isEnabled = false

            this.dialog_activity_per_diem_daily.isChecked = true
            this.dialog_activity_per_diem_daily_edit_value.isEnabled = true
            this.dialog_activity_per_diem_daily_edit_value.setText(dailyAmountString)
        }
        renderTotal()
    }

    private fun renderTotal() {
        val perDiemText = "$${perDiemTotal().formatPlaces(2)}"
        fragmentView.dialog_activity_per_diem_hour_value.text = perDiemText
        val totalOwedText = "$${totalOwed().formatPlaces(2)}"
        fragmentView.dialog_activity_per_diem_total_owed_value.text = totalOwedText

    }

    private fun perDiemTotal(): Double {
        return if (fragmentView.dialog_activity_per_diem_hourly.isChecked) {
            getHourlyTotal()
        } else {
            getDailyTotal()
        }
    }

    private fun getAmount(): Double {
        return if (fragmentView.dialog_activity_per_diem_hourly.isChecked) {
            getHourlyAmount()
        } else {
            getDailyTotal()
        }
    }

    private fun totalOwed() = perDiemTotal() - getCashGiven()

    private fun getCashGiven() = try {
        fragmentView.dialog_activity_per_diem_cash_given_edit_value.text.toString().toDouble()
    } catch (e: Exception) {
        0.0
    }

    private fun getHourlyTotal(): Double {
        val workTimeSeconds = (fragmentView.dialog_activity_per_diem_hour_title.tag as? Int) ?: 0


        val hoursWorked: Double = workTimeSeconds.toDouble() / 3600.0
        val hourlyAmount = getHourlyAmount()
        return hourlyAmount * hoursWorked
    }

    private fun getHourlyAmount(): Double {
        return try {
            val text = fragmentView.dialog_activity_per_diem_hourly_edit_value.text.toString()
            text.toDouble()
        } catch (e: Exception) {
            0.0
        }
    }

    private fun getDailyTotal() = try {
        fragmentView.dialog_activity_per_diem_daily_edit_value.text.toString().toDouble()
    } catch (e: Exception) {
        0.0
    }
}