package com.fieldflo.screens.formTimesheet

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fieldflo.common.StringProvider
import com.fieldflo.common.toStringWithDay
import com.fieldflo.usecases.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import javax.inject.Inject

class TimeSheetFormPresenter @Inject constructor(
    private val stringProvider: StringProvider,
    rolesUseCases: UserRolesUseCases,
    private val projectsUseCases: ProjectsUseCases,
    private val timeSheetUseCases: TimeSheetUseCases,
    private val timeSheetTimeClockUseCases: TimeSheetTimeClockUseCases
) : ViewModel() {

    private val mutex = Mutex()

    private val _timeSheetViewState = MutableStateFlow(
        TimeSheetFormViewState(
            editTimeAllowed = rolesUseCases.hasEditTimesheetTimeRole()
        )
    )
    val timeSheetViewState: StateFlow<TimeSheetFormViewState> get() = _timeSheetViewState

    fun getProjectStatusFlow(projectId: Int): Flow<ProjectStatus> {
        return projectsUseCases.getProjectStatusFlow(projectId)
    }

    suspend fun getProjectUsesPhases(projectId: Int): Boolean {
        return projectsUseCases.isPhaseRequiredOnProject(projectId)
    }

    fun loadTimeSheetData(internalFormId: Int, projectId: Int) {
        viewModelScope.launch {
            timeSheetUseCases.getTimeSheetDataFlow(internalFormId, projectId)
                .collect {
                    mutex.withLock {
                        val currentState = _timeSheetViewState.value
                        val newState = currentState.copy(
                            formDataId = it.formDataId,
                            projectName = it.projectName,
                            formDate = if (it.formDate.time > 0) it.formDate.toStringWithDay() else null,
                            supervisorInfo = TimeSheetFormViewState.TimeSheetSupervisor(
                                supervisorId = it.supervisorId,
                                supervisorFullName = it.supervisorName,
                            )
                        )
                        _timeSheetViewState.value = newState
                    }
                }
        }

        viewModelScope.launch {
            timeSheetUseCases.getTimeSheetEmployeesFlow(internalFormId, projectId)
                .collect { employeesData ->
                    mutex.withLock {
                        val currentState = _timeSheetViewState.value
                        val newState = currentState.copy(
                            timeSheetEmployees = employeesData.map {
                                TimeSheetFormViewState.TimeSheetEmployeeUiModel(
                                    timeSheetEmployeeId = it.timeSheetEmployeeId,
                                    employeeId = it.employeeId,
                                    avatarUrl = it.employeeImageFullUrl,
                                    employeeFullName = it.employeeFullName,
                                    phaseName = it.phaseName,
                                    positionName = it.positionName,
                                    usePhases = it.usePhases,
                                    employeeState = it.employeeState,
                                    isTracking = it.isTracking,
                                    fixedPhases = it.fixedPhases
                                )
                            }
                        )
                        _timeSheetViewState.value = newState
                    }
                }
        }
    }

    fun setAllEmployeesSelected(isSelected: Boolean) {
        viewModelScope.launch {
            mutex.withLock {
                val currentState = _timeSheetViewState.value
                val newState = currentState.copy(
                    timeSheetEmployees = currentState.timeSheetEmployees.map {
                        it.copy(isRowSelected = isSelected)
                    }
                )
                _timeSheetViewState.value = newState
            }
        }
    }

    fun toggleEmployeeSelected(timeSheetEmployeeId: Int) {
        viewModelScope.launch {
            mutex.withLock {
                val currentState = _timeSheetViewState.value
                val newState = currentState.copy(
                    timeSheetEmployees = currentState.timeSheetEmployees.map {
                        if (it.timeSheetEmployeeId == timeSheetEmployeeId) {
                            it.copy(isRowSelected = !it.isRowSelected)
                        } else {
                            it
                        }
                    }
                )
                _timeSheetViewState.value = newState
            }
        }
    }

    suspend fun addSupervisorToTimeSheet(internalFormId: Int): String {
        val employeesNotAdded =
            _timeSheetViewState.value.supervisorInfo?.supervisorId?.let { supervisorId ->
                timeSheetUseCases.addEmployeesToTimeSheet(
                    listOf(supervisorId),
                    internalFormId
                )
            } ?: listOf()
        return stringProvider.getEmployeesNotAddedErrorMessage(employeesNotAdded.size)
    }

    suspend fun addEmployeesToTimeSheet(
        employeeIds: List<Int>,
        internalFormId: Int
    ): String {
        val employeesNotAdded =
            timeSheetUseCases.addEmployeesToTimeSheet(employeeIds, internalFormId)
        return stringProvider.getEmployeesNotAddedErrorMessage(employeesNotAdded.size)
    }

    fun removeEmployee(timeSheetEmployeeId: Int) {
        viewModelScope.launch {
            timeSheetUseCases.removeEmployeeFromTimeSheet(timeSheetEmployeeId)
        }
    }

    fun updateSupervisor(employeeId: Int, internalFormId: Int) {
        viewModelScope.launch {
            timeSheetUseCases.updateSupervisor(employeeId, internalFormId)
        }
    }

    fun setEmployeePosition(timeSheetEmployeeId: Int, positionId: Int) {
        viewModelScope.launch {
            timeSheetUseCases.setEmployeePosition(timeSheetEmployeeId, positionId)
        }
    }

    fun setEmployeePhase(timeSheetEmployeeId: Int, phaseId: Int) {
        viewModelScope.launch {
            timeSheetUseCases.setEmployeePhase(timeSheetEmployeeId, phaseId)
        }
    }

    fun startTracking(timeSheetEmployeeIds: List<Int>) {
        viewModelScope.launch {
            timeSheetTimeClockUseCases.startTrackingEmployees(timeSheetEmployeeIds)
        }
    }

    fun verifyEmployees(
        timeSheetEmployeeIds: List<Int>,
        injured: Boolean,
        tookBreak: Boolean,
        verifiedByEmployeeId: Int
    ) {
        viewModelScope.launch {
            timeSheetUseCases.setEmployeesVerified(
                timeSheetEmployeeIds,
                injured,
                tookBreak,
                verifiedByEmployeeId
            )
        }
    }
}
