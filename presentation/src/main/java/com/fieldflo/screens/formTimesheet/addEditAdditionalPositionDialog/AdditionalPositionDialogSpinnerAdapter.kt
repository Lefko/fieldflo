package com.fieldflo.screens.formTimesheet.addEditAdditionalPositionDialog

import android.content.Context
import android.widget.ArrayAdapter
import com.fieldflo.R

class AdditionalPositionDialogSpinnerAdapter<T>(context: Context) :
    ArrayAdapter<T>(context, R.layout.row_spinner_dropdown) {

    init {
        setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    }

    fun submit(list: List<T>) {
        clear()
        addAll(list)
        notifyDataSetChanged()
    }
}