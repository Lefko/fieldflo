package com.fieldflo.screens.settings

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.provider.Settings
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.safeClickListener
import com.fieldflo.di.injector
import com.fieldflo.screens.drawer.DrawerManager
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.view_activity_settings_content.*
import kotlinx.coroutines.flow.collect

class SettingsActivity : AppCompatActivity(R.layout.activity_settings) {

    companion object {
        fun start(sender: Activity) {
            val intent = Intent(sender, SettingsActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            sender.startActivity(intent)
        }
    }

    private val drawerManager by lazy {
        DrawerManager({ drawer_layout }, { this }, { activity_settings_content_main_toolbar })
    }

    private val presenter by viewModels<SettingsPresenter> { injector.settingsViewModelFactory() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity_settings_content_language_button.setOnClickListener { startActivity(Intent(Settings.ACTION_LOCALE_SETTINGS)) }
        present()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawerManager.onConfigChange(newConfig)
        presenter.refresh()
    }

    override fun onBackPressed() {
        val handled = drawerManager.onBackPress()
        if (!handled) {
            super.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        drawerManager.refreshSelected()
        presenter.refresh()
    }

    fun present() {
        lifecycleScope.launchWhenCreated {
            presenter.observeViewState().collect { render(it) }
        }

        setListeners()
    }

    private fun setListeners() {
        activity_settings_content_photo_pin_switch.safeClickListener {
            presenter.changeUsesPhotoPin(activity_settings_content_photo_pin_switch.isChecked)
        }

        activity_settings_content_delete_pin_photos_switch.safeClickListener {
            presenter.changeDeletePhotoPin(activity_settings_content_delete_pin_photos_switch.isChecked)
        }
    }

    private fun render(viewState: SettingsViewState) {
        activity_settings_content_language_title.text =
            getString(R.string.activity_settings_language_title, viewState.languageLocal)

        activity_settings_content_photo_pin_switch.isChecked = viewState.usesPhotoPin
        activity_settings_content_delete_pin_photos_switch.isChecked = viewState.deletesPhotoPin
    }
}