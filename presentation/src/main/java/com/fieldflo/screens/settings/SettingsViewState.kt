package com.fieldflo.screens.settings

data class SettingsViewState(
    val languageLocal: String = "",
    val usesPhotoPin: Boolean = false,
    val deletesPhotoPin: Boolean = false
)