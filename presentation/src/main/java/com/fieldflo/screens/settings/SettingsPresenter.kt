package com.fieldflo.screens.settings

import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.SettingsUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import javax.inject.Inject

class SettingsPresenter @Inject constructor(
    private val settingsUseCase: SettingsUseCase
) : ViewModel() {

    private val viewStateObservable = MutableStateFlow(createViewState())

    fun observeViewState() = viewStateObservable

    fun refresh() {
        viewStateObservable.value = createViewState()
    }

    fun changeUsesPhotoPin(usesPhotoPin: Boolean) {
        settingsUseCase.usesPhotoPin = usesPhotoPin
        refresh()
    }

    fun changeDeletePhotoPin(deletesPhotoPin: Boolean) {
        settingsUseCase.deletesPhotoPin = deletesPhotoPin
        refresh()
    }

    private fun createViewState() = SettingsViewState(
        usesPhotoPin = settingsUseCase.usesPhotoPin,
        deletesPhotoPin = settingsUseCase.deletesPhotoPin,
        languageLocal = settingsUseCase.language
    )
}
