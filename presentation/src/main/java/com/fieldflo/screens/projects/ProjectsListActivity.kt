package com.fieldflo.screens.projects

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.fieldflo.R
import com.fieldflo.common.onQueryTextChanged
import com.fieldflo.di.injector
import com.fieldflo.screens.drawer.DrawerManager
import com.fieldflo.screens.projectHome.ProjectHomeActivity
import kotlinx.android.synthetic.main.view_activity_projects_list_content.*
import kotlinx.coroutines.flow.collect

class ProjectsListActivity : AppCompatActivity(R.layout.activity_projects_list) {

    companion object {
        fun start(sender: Activity) {
            sender.startActivity(createIntent(sender))
        }

        private fun createIntent(sender: Context): Intent =
            Intent(sender, ProjectsListActivity::class.java).apply {
                flags =
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
    }

    private val drawerManager by lazy {
        DrawerManager(
            { findViewById(R.id.drawer_layout) }, { this },
            { findViewById(R.id.activity_project_lists_content_main_toolbar) }
        )
    }

    private val presenter by viewModels<ProjectsListPresenter> { injector.projectListsViewModelFactory() }

    private val adapter = ProjectsListAdapter {
        ProjectHomeActivity.start(
            this,
            it.projectId,
            it.projectNumber,
            it.projectName
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(activity_project_lists_title_toolbar)

        initRecyclerView()

        present()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_toolbar_search, menu)
        val searchItem = menu!!.findItem(R.id.search)
        setupSearch(searchItem)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onBackPressed() {
        val handled = drawerManager.onBackPress()
        if (!handled) {
            super.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        drawerManager.refreshSelected()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawerManager.onConfigChange(newConfig)
    }

    private fun setupSearch(searchItem: MenuItem) {
        val searchView = searchItem.actionView as SearchView
        searchView.onQueryTextChanged {
            presenter.filter(it.trim())
            true
        }

        searchView.setOnCloseListener {
            presenter.filter("")
            false
        }
    }

    private fun present() {
        lifecycleScope.launchWhenCreated {
            presenter.viewState().collect { render(it) }
        }
    }

    private fun initRecyclerView() {
        activity_projects_listsRV.layoutManager = LinearLayoutManager(this)
        activity_projects_listsRV.adapter = adapter
    }

    private fun render(viewState: ProjectsListViewState) {
        adapter.submitList(viewState.projects)
    }
}
