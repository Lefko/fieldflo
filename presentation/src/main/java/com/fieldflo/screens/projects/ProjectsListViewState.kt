package com.fieldflo.screens.projects

import java.util.*

data class ProjectsListViewState(
    private val canViewProjects: Boolean,
    private val projectsRaw: List<BasicProjectUiModel> = emptyList(),
    private val query: String = ""
) {
    val projects: List<BasicProjectUiModel>
        get() = if (!canViewProjects) {
            emptyList()
        } else {
            projectsRaw.filter { projectMatchesQuery(it, query) }
        }

    private fun projectMatchesQuery(project: BasicProjectUiModel, query: String): Boolean {
        return project.projectId.toString().contains(query, ignoreCase = true) ||
                project.projectName.contains(query, ignoreCase = true) ||
                project.clientFullName.contains(query, ignoreCase = true)
    }
}

data class BasicProjectUiModel(
    val projectId: Int = 0,
    val projectNumber: String = "",
    val projectName: String = "",
    val clientFullName: String = "",
    val projectCrewSize: Int = 0,
    val supervisorFullName: String = "",
    val managerFullName: String = "",
    val projectStartDate: Date = Date(),
    val projectEndDate: Date = Date(),
    val projectClosed: Boolean = false,
    val usePhases: Boolean,
    val phaseCode: String,
    var isExpanded: Boolean = false
)
