package com.fieldflo.screens.projects

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.AutoTransition
import androidx.transition.ChangeBounds
import androidx.transition.TransitionManager
import com.fieldflo.R
import com.fieldflo.common.safeClickListener
import com.fieldflo.common.toSimpleString


class ProjectsListAdapter(private val onProjectClick: (project: BasicProjectUiModel) -> Unit) :
    ListAdapter<BasicProjectUiModel, ProjectsListAdapter.BasicProjectViewHolder>(CALLBACK) {

    private var recyclerView: RecyclerView? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BasicProjectViewHolder {
        val rowLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_project, parent, false) as ViewGroup

        return BasicProjectViewHolder(rowLayout)
    }

    override fun onBindViewHolder(holder: BasicProjectViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        this.recyclerView = null
    }

    inner class BasicProjectViewHolder(private val rowLayout: ViewGroup) :
        RecyclerView.ViewHolder(rowLayout) {

        private val projectIdTV: TextView = rowLayout.findViewById(R.id.activity_project_list_code)
        private val projectNameTV: TextView =
            rowLayout.findViewById(R.id.activity_project_list_address)
        private val clientNameTV: TextView =
            rowLayout.findViewById(R.id.activity_project_list_client_name)
        private val crewSizeTV: TextView =
            rowLayout.findViewById(R.id.activity_project_list_crew_size)
        private val superVisorTV: TextView =
            rowLayout.findViewById(R.id.activity_project_list_supervisor)
        private val managerNameTV: TextView =
            rowLayout.findViewById(R.id.activity_project_list_project_manager)
        private val startDateTV: TextView =
            rowLayout.findViewById(R.id.activity_project_list_start_date)
        private val endDateTV: TextView =
            rowLayout.findViewById(R.id.activity_project_list_end_date)
        private val closedTV: TextView = rowLayout.findViewById(R.id.activity_project_list_closed)

        private val expandIV: ImageView =
            rowLayout.findViewById(R.id.activity_project_list_expand_collapse_icon)
        private val expandTV: TextView =
            rowLayout.findViewById(R.id.activity_project_list_expand_collapse)
        private val expandDetailGroup: View =
            rowLayout.findViewById(R.id.activity_project_detail_expand_group)
        private val expandCollapseClickMask: View =
            rowLayout.findViewById(R.id.activity_project_list_expand_collapse_click_mask)

        fun bind(uiModel: BasicProjectUiModel) {
            projectIdTV.text = uiModel.projectNumber
            projectNameTV.text = uiModel.projectName
            clientNameTV.text = uiModel.clientFullName
            crewSizeTV.text = uiModel.projectCrewSize.toString()
            superVisorTV.text = uiModel.supervisorFullName
            managerNameTV.text = uiModel.managerFullName
            startDateTV.text = uiModel.projectStartDate.toSimpleString()
            endDateTV.text = uiModel.projectEndDate.toSimpleString()
            closedTV.setText(if (uiModel.projectClosed) R.string.yes else R.string.no)
            expandTV.setText(if (uiModel.isExpanded) R.string.activity_project_list_collapse else R.string.activity_project_list_expand)
            expandIV.rotation = if (uiModel.isExpanded) 0F else 180F

            expandDetailGroup.visibility = if (uiModel.isExpanded) View.VISIBLE else View.GONE
            expandCollapseClickMask.setOnClickListener {
                uiModel.isExpanded = !uiModel.isExpanded
                expandIV.animate().rotation(if (uiModel.isExpanded) 0F else 180F).start()

                recyclerView?.let {
                    TransitionManager.beginDelayedTransition(
                        it,
                        if (uiModel.isExpanded) ChangeBounds() else AutoTransition()
                    )
                }
                expandDetailGroup.visibility = if (uiModel.isExpanded) View.VISIBLE else View.GONE
                expandTV.setText(if (uiModel.isExpanded) R.string.activity_project_list_collapse else R.string.activity_project_list_expand)
            }

            rowLayout.safeClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    onProjectClick(uiModel)
                }
            }
        }
    }
}

private val CALLBACK = object : DiffUtil.ItemCallback<BasicProjectUiModel>() {

    override fun areItemsTheSame(oldItem: BasicProjectUiModel, newItem: BasicProjectUiModel) =
        oldItem.projectId == newItem.projectId

    override fun areContentsTheSame(oldItem: BasicProjectUiModel, newItem: BasicProjectUiModel) =
        oldItem == newItem
}