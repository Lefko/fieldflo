package com.fieldflo.screens.projects

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fieldflo.data.persistence.db.entities.BasicProjectData
import com.fieldflo.usecases.ProjectsUseCases
import com.fieldflo.usecases.UserRolesUseCases
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import javax.inject.Inject

class ProjectsListPresenter @Inject constructor(
    userRolesUseCases: UserRolesUseCases,
    private val projectsUseCases: ProjectsUseCases
) : ViewModel() {

    private val mutex = Mutex()

    private val viewStateObservable = MutableStateFlow(
        ProjectsListViewState(
            canViewProjects = userRolesUseCases.canUserViewProjects()
        )
    )

    init {
        viewModelScope.launch(Dispatchers.IO) {
            projectsUseCases.getAllProjectsFlow()
                .map { projects ->
                    projects.map { it.toBasicProjectUiModel() }
                        .sortedBy { it.projectName.toLowerCase() }
                }.collect { projects ->
                    mutex.withLock {
                        val prevState = viewStateObservable.value
                        val newState = prevState.copy(projectsRaw = projects)
                        viewStateObservable.value = newState
                    }
                }
        }
    }

    fun viewState() = viewStateObservable

    fun filter(query: String) {
        viewModelScope.launch(Dispatchers.Default) {
            mutex.withLock {
                val prevState = viewStateObservable.value
                val newState = prevState.copy(query = query)
                viewStateObservable.value = newState
            }
        }
    }
}

private fun BasicProjectData.toBasicProjectUiModel() = BasicProjectUiModel(
    projectId = this.projectId,
    projectNumber = this.projectNumber,
    projectName = this.projectName,
    clientFullName = this.clientFullName,
    projectCrewSize = this.projectCrewSize,
    supervisorFullName = this.supervisorFullName,
    managerFullName = this.projectManagerFullName,
    projectStartDate = this.projectStartDate,
    projectEndDate = this.projectEndDate,
    usePhases = this.usePhases,
    phaseCode = this.phaseCode,
    projectClosed = this.projectClosed
)