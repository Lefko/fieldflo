package com.fieldflo.screens.formPsi.taskSteps.addTask

import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.TaskUseCases
import javax.inject.Inject

class AddTasksPresenter @Inject constructor(
    private val taskUseCases: TaskUseCases
) : ViewModel() {

    suspend fun getAllTasks(): List<AddTasksViewState.TaskUiRow> {
        return taskUseCases.getAllCompanyTasks()
            .map {
                AddTasksViewState.TaskUiRow(
                    task = it.task,
                    hazard = it.hazard,
                    control = it.control,
                    risk = it.risk
                )
            }
    }
}

data class AddTasksViewState(
    val tasks: List<TaskUiRow>
) {
    data class TaskUiRow(
        val task: String = "",
        val hazard: String = "",
        val control: String = "",
        val risk: Int = 0
    )
}