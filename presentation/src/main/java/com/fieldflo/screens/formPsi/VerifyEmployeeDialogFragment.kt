package com.fieldflo.screens.formPsi

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import com.fieldflo.R
import java.io.File

class VerifyEmployeeDialogFragment : DialogFragment() {

    private lateinit var callback: VerifyEmployeeDialogCallback

    private val employeeId: Int by lazy { arguments!!.getInt(EMPLOYEE_ID) }
    private val pinOutImageLocation: File by lazy {
        File(arguments!!.getString(EMPLOYEE_PIN_OUT_IMAGE_LOCATION)!!)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_verify_pin, null)
        val pinInputField = view.findViewById<EditText>(R.id.dialog_verify_pin_inputET)
        return AlertDialog.Builder(activity as Context)
            .setView(view)
            .setPositiveButton(android.R.string.ok, { _, _ ->
                if (pinInputField.text.isNotEmpty()) {
                    callback.onVerifyEmployeePinSubmit(
                        employeeId = employeeId,
                        enteredPin = pinInputField.text.toString(),
                        pinOutImageLocation = pinOutImageLocation
                    )

                    dismiss()
                }
            })
            .setNegativeButton(android.R.string.cancel, { _, _ ->
                dismiss()
            })
            .show()
    }

    private fun onAttachToContext(context: Context) {
        this.callback = parentFragment as? VerifyEmployeeDialogCallback
            ?: targetFragment as? VerifyEmployeeDialogCallback
                    ?: context as? VerifyEmployeeDialogCallback
                    ?: throw IllegalArgumentException(
                "${context.javaClass.simpleName} " +
                        "must be an instance of VerifyEmployeeDialogCallback"
            )
    }

    companion object {

        const val TAG = "VerifySingleEmployeeDialogFragment"
        private const val EMPLOYEE_ID = "employee_id"
        private const val EMPLOYEE_PIN_OUT_IMAGE_LOCATION = "pin_out_image_file_location"

        fun newInstance(employeeId: Int, pinOutImageLocation: File): VerifyEmployeeDialogFragment {
            return VerifyEmployeeDialogFragment().apply {
                arguments = bundleOf(
                    EMPLOYEE_ID to employeeId,
                    EMPLOYEE_PIN_OUT_IMAGE_LOCATION to pinOutImageLocation.absolutePath
                )
            }
        }
    }

    interface VerifyEmployeeDialogCallback {
        fun onVerifyEmployeePinSubmit(employeeId: Int, enteredPin: String, pinOutImageLocation: File)
    }
}