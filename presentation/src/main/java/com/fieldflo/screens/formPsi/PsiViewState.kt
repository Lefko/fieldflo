package com.fieldflo.screens.formPsi

import com.fieldflo.common.toDate
import com.fieldflo.data.persistence.db.entities.PsiEmployeeData
import com.fieldflo.data.persistence.db.entities.PsiFormData
import com.fieldflo.data.persistence.db.entities.PsiTaskData
import com.fieldflo.screens.formPsi.generalInformation.EmployeeAndVerificationData
import com.fieldflo.screens.formPsi.generalInformation.PsiGeneralInformationViewState
import com.fieldflo.screens.formPsi.other.PsiOtherViewState
import com.fieldflo.screens.formPsi.review.PsiReviewViewState
import com.fieldflo.screens.formPsi.signatures.PsiEmployeeRow
import com.fieldflo.screens.formPsi.taskSteps.TaskStepsUiEntity
import com.fieldflo.screens.formPsi.weather.PsiWeatherViewState
import java.util.*

data class PsiViewState(
    val projectName: String,
    val projectNumber: String,

    val generalInformationViewState: PsiGeneralInformationViewState,
    val reviewViewState: PsiReviewViewState,
    val weatherViewState: PsiWeatherViewState,
    val tasks: List<TaskStepsUiEntity>,
    val employees: List<PsiEmployeeRow>,
    val otherViewState: PsiOtherViewState
) {
    fun toPsiData(
        supervisorData: EmployeeAndVerificationData?,
        auditorData: EmployeeAndVerificationData?,
        internalFormId: Int,
        projectId: Int
    ): PsiFormData {
        val supervisorVerified = supervisorData?.verified ?: false
        val auditorVerified = auditorData?.verified ?: false
        val psiFormData = PsiFormData(
            internalFormId = internalFormId,
            projectId = projectId,
            companyId = 0,
            formDate = generalInformationViewState.formDate.toDate(),
            taskLocation = generalInformationViewState.taskLocation,
            musterMeetingPoint = generalInformationViewState.musterMeetingPoint,
            supervisorId = supervisorData?.employeeId ?: 0,
            supervisorVerified = supervisorVerified,
            supervisorVerifiedDate = if (supervisorVerified) {
                supervisorData?.verificationData?.verifiedDate?.toDate() ?: Date(0)
            } else {
                Date(0)
            },
            supervisorVerifiedByName = if (supervisorVerified) {
                supervisorData?.verificationData?.verifiedByName ?: ""
            } else {
                ""
            },
            supervisorVerifiedByEmployeeId = if (supervisorVerified) {
                supervisorData?.verificationData?.verifiedByEmployeeId ?: 0
            } else {
                0
            },
            auditorId = auditorData?.employeeId ?: 0,
            auditorPinVerified = auditorVerified,
            auditorVerifiedDate = if (auditorVerified) {
                auditorData?.verificationData?.verifiedDate?.toDate() ?: Date(0)
            } else {
                Date(0)
            },
            auditorVerifiedByName = if (auditorVerified) {
                auditorData?.verificationData?.verifiedByName ?: ""
            } else {
                ""
            },
            auditorVerifiedByEmployeeId = if (auditorVerified) {
                auditorData?.verificationData?.verifiedByEmployeeId ?: 0
            } else {
                0
            },
            spillPotential = reviewViewState.spillPotential,
            hazmatStorage = reviewViewState.hazmatStorage,
            weather = reviewViewState.weather,
            sdsReviewForHazmat = reviewViewState.sdsReviewForHazmat,
            ventilationRequired = reviewViewState.ventilationRequired,
            heatStress = reviewViewState.heatStress,
            lightningLevelsTooLow = reviewViewState.lightningLevelsTooLow,
            housekeeping = reviewViewState.housekeeping,
            workingTightArea = reviewViewState.workingTightArea,
            partOfBodyInLineOfFire = reviewViewState.partOfBodyInLineOfFire,
            workingAboveYourHead = reviewViewState.workingAboveYourHead,
            pinchPointsIdentified = reviewViewState.pinchPointsIdentified,
            repetitiveMotion = reviewViewState.repetitiveMotion,
            barricadesFlaggingAndSignInPlace = reviewViewState.barricadesFlaggingAndSignInPlace,
            holeCoveringsInPlace = reviewViewState.holeCoveringsInPlace,
            protectFromFallingItems = reviewViewState.protectFromFallingItems,
            poweredPlatforms = reviewViewState.poweredPlatforms,
            othersWorkingOverhead = reviewViewState.othersWorkingOverhead,
            fallArrestSystems = reviewViewState.fallArrestSystems,
            ladders = reviewViewState.ladders,
            welding = reviewViewState.welding,
            burnSources = reviewViewState.burnSources,
            compressedGasses = reviewViewState.compressedGasses,
            workingOnEnergizedEquipment = reviewViewState.workingOnEnergizedEquipment,
            electricalCordsCondition = reviewViewState.electricalCordsCondition,
            equipmentInspected = reviewViewState.equipmentInspected,
            criticalLiftMeetingRequired = reviewViewState.criticalLiftMeetingRequired,
            energyIsolation = reviewViewState.energyIsolation,
            airborneParticles = reviewViewState.airborneParticles,
            openHoles = reviewViewState.openHoles,
            mobileEquipment = reviewViewState.mobileEquipment,
            rigging = reviewViewState.rigging,
            excavation = reviewViewState.excavation,
            confinedSpace = reviewViewState.confinedSpace,
            scaffold = reviewViewState.scaffold,
            slipPotentialIdentified = reviewViewState.slipPotentialIdentified,
            requiredPermitsInPlace = reviewViewState.requiredPermitsInPlace,
            excavations = reviewViewState.excavations,
            walkways = reviewViewState.walkways,
            other = reviewViewState.other,
            clearInstructionsProvided = reviewViewState.clearInstructionsProvided,
            trainedToUseToolAndPerformTask = reviewViewState.trainedToUseToolAndPerformTask,
            distractionsInWorkArea = reviewViewState.distractionsInWorkArea,
            workingAlone = reviewViewState.workingAlone,
            liftTooHeavy = reviewViewState.liftTooHeavy,
            externalNoiseLevel = reviewViewState.externalNoiseLevel,
            physicalLimitations = reviewViewState.physicalLimitations,
            firstAidRequirements = reviewViewState.firstAidRequirements,
            goggles = reviewViewState.goggles,
            faceShield = reviewViewState.faceShield,
            gloves = reviewViewState.gloves,
            coverall = reviewViewState.coverall,
            hearingProtection = reviewViewState.hearingProtection,
            respirator = reviewViewState.respirator,
            harness = reviewViewState.harness,
            respiratorName = reviewViewState.respiratorName,
            reflectiveVest = reviewViewState.reflectiveVest,
            footwear = reviewViewState.footwear,
            safetyGlasses = reviewViewState.safetyGlasses,
            weldingHood = reviewViewState.weldingHood,
            tyvexSuit = reviewViewState.tyvexSuit,
            retractableLanyard = reviewViewState.retractableLanyard,
            hardHat = reviewViewState.hardHat,
            reviewedWeather = weatherViewState.reviewedWeather,
            reviewedRoad = weatherViewState.reviewedRoad,
            reviewedHeatIndex = weatherViewState.reviewedHeatIndex,
            taskDescription = otherViewState.taskDescription,
            hazardIdentification = otherViewState.hazardIdentification,
            hazardControls = otherViewState.hazardControls,
            allSectionsImplemented = otherViewState.allSectionsImplemented,
            workersNamesLegible = otherViewState.workersNamesLegible,
            reviewedByForeman = otherViewState.reviewedByForeman,
            musterPointIdentified = otherViewState.musterPointIdentified,
            toolsAndEquipmentInspected = otherViewState.toolsAndEquipmentInspected,
            psiAtTaskLocation = otherViewState.psiAtTaskLocation,
            comments = otherViewState.comments
        )
        psiFormData.employees = employees.map {
            PsiEmployeeData(
                psiEmployeeId = it.psiEmployeeId,
                companyId = 0,
                projectId = projectId,
                internalFormId = internalFormId,
                formDataId = 0,

                employeeId = it.employeeId,
                isPinVerified = it.verified,
                pinVerifiedByEmployeeId = it.pinEmployeeId,
                pinVerifiedByName = it.pinUserName,
                pinDate = it.pinDate.toDate()
            )
        }
        psiFormData.tasks = tasks.map {
            PsiTaskData(
                psiTaskId = it.psiTaskId,
                companyId = 0,
                projectId = projectId,
                internalFormId = internalFormId,
                formDataId = 0,
                task = it.task,
                hazard = it.hazard,
                control = it.control,
                risk = it.risk
            )
        }

        return psiFormData
    }
}