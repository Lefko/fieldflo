package com.fieldflo.screens.formPsi.taskSteps

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.fieldflo.R
import com.fieldflo.screens.formPsi.taskSteps.addTask.AddTasksDialogFragment
import kotlinx.android.synthetic.main.fragment_psi_task_steps.*


class PsiTaskStepsFragment : Fragment(R.layout.fragment_psi_task_steps),
    AddTasksDialogFragment.AddTasksCallback {

    companion object {
        fun newInstance() = PsiTaskStepsFragment()
    }

    private val tasksAdapter = PsiTaskStepsAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        fragment_pre_job_safety_task_steps_add_rowTV.setOnClickListener {
            val currentList =
                mutableListOf<TaskStepsUiEntity>().apply { addAll(tasksAdapter.getTaskStepsHazardControls()) }
            val lowestId = currentList.minByOrNull { it.psiTaskId }?.psiTaskId ?: 0
            currentList.add(0, TaskStepsUiEntity(psiTaskId = lowestId - 1))
            tasksAdapter.submitList(currentList)
        }

        fragment_pre_job_safety_task_steps_add_taskTV.setOnClickListener {
            val dialog = AddTasksDialogFragment.newInstance()
            dialog.show(childFragmentManager, AddTasksDialogFragment.TAG)
        }
    }

    fun getViewState(): List<TaskStepsUiEntity> = tasksAdapter.getTaskStepsHazardControls()

    fun setViewState(tasks: List<TaskStepsUiEntity>) {
        tasksAdapter.submitList(tasks)
    }

    override fun tasksToAdd(tasks: List<TaskStepsUiEntity>) {
        tasksAdapter.addTasks(tasks)
    }

    private fun initRecyclerView() {
        fragment_pre_job_safety_task_stepsRV.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = tasksAdapter
        }
    }
}