package com.fieldflo.screens.formPsi.other

data class PsiOtherViewState(
    val taskDescription: String,
    val hazardIdentification: String,
    val hazardControls: String,
    val allSectionsImplemented: String,
    val workersNamesLegible: String,
    val reviewedByForeman: String,
    val musterPointIdentified: String,
    val toolsAndEquipmentInspected: String,
    val psiAtTaskLocation: String,
    val comments: String
)
