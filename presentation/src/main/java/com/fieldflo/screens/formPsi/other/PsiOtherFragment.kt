package com.fieldflo.screens.formPsi.other

import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import androidx.fragment.app.Fragment
import com.fieldflo.R
import kotlinx.android.synthetic.main.fragment_psi_other.*

class PsiOtherFragment : Fragment(R.layout.fragment_psi_other) {

    companion object {
        fun newInstance() = PsiOtherFragment()
        private const val ADEQUATE = "adequate"
        private const val INADEQUATE = "inadequate"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragment_pre_job_safety_other_task_desc_adeq_rb
            .setToggleBehavior(fragment_pre_job_safety_other_task_desc_inadeq_rb)
        fragment_pre_job_safety_other_task_desc_inadeq_rb
            .setToggleBehavior(fragment_pre_job_safety_other_task_desc_adeq_rb)

        fragment_pre_job_safety_other_hazard_id_adeq_rb
            .setToggleBehavior(fragment_pre_job_safety_other_hazard_id_inadeq_rb)
        fragment_pre_job_safety_other_hazard_id_inadeq_rb
            .setToggleBehavior(fragment_pre_job_safety_other_hazard_id_adeq_rb)

        fragment_pre_job_safety_other_hazard_controls_adeq_rb
            .setToggleBehavior(fragment_pre_job_safety_other_hazard_controls_inadeq_rb)
        fragment_pre_job_safety_other_hazard_controls_inadeq_rb
            .setToggleBehavior(fragment_pre_job_safety_other_hazard_controls_adeq_rb)

        fragment_pre_job_safety_other_all_sections_impl_adeq_rb
            .setToggleBehavior(fragment_pre_job_safety_other_all_sections_impl_inadeq_rb)
        fragment_pre_job_safety_other_all_sections_impl_inadeq_rb
            .setToggleBehavior(fragment_pre_job_safety_other_all_sections_impl_adeq_rb)

        fragment_pre_job_safety_other_workers_names_adeq_rb
            .setToggleBehavior(fragment_pre_job_safety_other_workers_names_inadeq_rb)
        fragment_pre_job_safety_other_workers_names_inadeq_rb
            .setToggleBehavior(fragment_pre_job_safety_other_workers_names_adeq_rb)

        fragment_pre_job_safety_other_reviewed_adeq_rb
            .setToggleBehavior(fragment_pre_job_safety_other_reviewed_inadeq_rb)
        fragment_pre_job_safety_other_reviewed_inadeq_rb
            .setToggleBehavior(fragment_pre_job_safety_other_reviewed_adeq_rb)

        fragment_pre_job_safety_other_muster_adeq_rb
            .setToggleBehavior(fragment_pre_job_safety_other_muster_inadeq_rb)
        fragment_pre_job_safety_other_muster_inadeq_rb
            .setToggleBehavior(fragment_pre_job_safety_other_muster_adeq_rb)

        fragment_pre_job_safety_other_tools_adeq_rb
            .setToggleBehavior(fragment_pre_job_safety_other_tools_inadeq_rb)
        fragment_pre_job_safety_other_tools_inadeq_rb
            .setToggleBehavior(fragment_pre_job_safety_other_tools_adeq_rb)

        fragment_pre_job_safety_other_psi_adeq_rb
            .setToggleBehavior(fragment_pre_job_safety_other_psi_inadeq_rb)
        fragment_pre_job_safety_other_psi_inadeq_rb
            .setToggleBehavior(fragment_pre_job_safety_other_psi_adeq_rb)
    }

    private fun CheckBox.setToggleBehavior(other: CheckBox) {
        this.setOnClickListener {
            this.isChecked = true
            other.isChecked = false
        }
    }

    private fun renderAdequateInadequate(value: String, adequate: CheckBox, inadequate: CheckBox) {
        when {
            value.equals(ADEQUATE, true) -> {
                adequate.isChecked = true
                inadequate.isChecked = false
            }
            value.equals(INADEQUATE, true) -> {
                adequate.isChecked = false
                inadequate.isChecked = true
            }
            else -> {
                adequate.isChecked = false
                inadequate.isChecked = false
            }
        }
    }

    private fun isReviewed(yes: CheckBox, no: CheckBox): String {
        return when {
            yes.isChecked && !no.isChecked -> ADEQUATE
            !yes.isChecked && no.isChecked -> INADEQUATE
            else -> ""
        }
    }

    fun getViewState(): PsiOtherViewState {
        return PsiOtherViewState(
            taskDescription = isReviewed(
                fragment_pre_job_safety_other_task_desc_adeq_rb,
                fragment_pre_job_safety_other_task_desc_inadeq_rb
            ),
            hazardIdentification = isReviewed(
                fragment_pre_job_safety_other_hazard_id_adeq_rb,
                fragment_pre_job_safety_other_hazard_id_inadeq_rb
            ),
            hazardControls = isReviewed(
                fragment_pre_job_safety_other_hazard_controls_adeq_rb,
                fragment_pre_job_safety_other_hazard_controls_inadeq_rb
            ),
            allSectionsImplemented = isReviewed(
                fragment_pre_job_safety_other_all_sections_impl_adeq_rb,
                fragment_pre_job_safety_other_all_sections_impl_inadeq_rb
            ),
            workersNamesLegible = isReviewed(
                fragment_pre_job_safety_other_workers_names_adeq_rb,
                fragment_pre_job_safety_other_workers_names_inadeq_rb
            ),
            reviewedByForeman = isReviewed(
                fragment_pre_job_safety_other_reviewed_adeq_rb,
                fragment_pre_job_safety_other_reviewed_inadeq_rb
            ),
            musterPointIdentified = isReviewed(
                fragment_pre_job_safety_other_muster_adeq_rb,
                fragment_pre_job_safety_other_muster_inadeq_rb
            ),
            toolsAndEquipmentInspected = isReviewed(
                fragment_pre_job_safety_other_tools_adeq_rb,
                fragment_pre_job_safety_other_tools_inadeq_rb
            ),
            psiAtTaskLocation = isReviewed(
                fragment_pre_job_safety_other_psi_adeq_rb,
                fragment_pre_job_safety_other_psi_inadeq_rb
            ),
            comments = fragment_pre_job_safety_other_comments_input.text.toString()
        )
    }


    fun setViewState(viewState: PsiOtherViewState) {
        render(viewState)
    }

    private fun render(viewState: PsiOtherViewState) {
        //1.Task Description
        renderAdequateInadequate(
            viewState.taskDescription,
            fragment_pre_job_safety_other_task_desc_adeq_rb,
            fragment_pre_job_safety_other_task_desc_inadeq_rb
        )

        //2. Hazard Identification
        renderAdequateInadequate(
            viewState.hazardIdentification,
            fragment_pre_job_safety_other_hazard_id_adeq_rb,
            fragment_pre_job_safety_other_hazard_id_inadeq_rb
        )

        //3. Hazard Controls
        renderAdequateInadequate(
            viewState.hazardControls,
            fragment_pre_job_safety_other_hazard_controls_adeq_rb,
            fragment_pre_job_safety_other_hazard_controls_inadeq_rb
        )

        //4. All Sections Implemented
        renderAdequateInadequate(
            viewState.allSectionsImplemented,
            fragment_pre_job_safety_other_all_sections_impl_adeq_rb,
            fragment_pre_job_safety_other_all_sections_impl_inadeq_rb
        )

        //5. Workers' Names Legible
        renderAdequateInadequate(
            viewState.workersNamesLegible,
            fragment_pre_job_safety_other_workers_names_adeq_rb,
            fragment_pre_job_safety_other_workers_names_inadeq_rb
        )

        //6. Reviewed / Signed by Foreman
        renderAdequateInadequate(
            viewState.reviewedByForeman,
            fragment_pre_job_safety_other_reviewed_adeq_rb,
            fragment_pre_job_safety_other_reviewed_inadeq_rb
        )

        //7. Muster / Assembly Point Identified
        renderAdequateInadequate(
            viewState.musterPointIdentified,
            fragment_pre_job_safety_other_muster_adeq_rb,
            fragment_pre_job_safety_other_muster_inadeq_rb
        )

        //8. Tools and Equipments Inspected
        renderAdequateInadequate(
            viewState.toolsAndEquipmentInspected,
            fragment_pre_job_safety_other_tools_adeq_rb,
            fragment_pre_job_safety_other_tools_inadeq_rb
        )

        //9. PSI at Task Location
        renderAdequateInadequate(
            viewState.psiAtTaskLocation,
            fragment_pre_job_safety_other_psi_adeq_rb,
            fragment_pre_job_safety_other_psi_inadeq_rb
        )

        fragment_pre_job_safety_other_comments_input.setText(viewState.comments)
    }
}