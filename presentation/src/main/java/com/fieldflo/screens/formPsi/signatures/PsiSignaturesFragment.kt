package com.fieldflo.screens.formPsi.signatures

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.fieldflo.R
import com.fieldflo.common.toSimpleString
import com.fieldflo.screens.formPsi.VerifyEmployeeDialogFragment
import com.fieldflo.screens.formPsi.signatures.addEmployeesToSignatures.AddEmployeesToSignaturesDialogFragment
import com.fieldflo.screens.formPsi.signatures.pinOutImageDialog.PinOutImageDialog
import kotlinx.android.synthetic.main.fragment_psi_signatures.*
import java.util.*


class PsiSignaturesFragment : Fragment(R.layout.fragment_psi_signatures),
    PsiSignaturesAdapter.WorkerRowActionHandler,
    AddEmployeesToSignaturesDialogFragment.AddSignaturesCallback {

    companion object {
        private const val PROJECT_ID = "project_id"
        private const val INTERNAL_FORM_ID = "internal_form_id"

        fun newInstance(projectId: Int, internalFormId: Int) = PsiSignaturesFragment().apply {
            arguments = bundleOf(
                PROJECT_ID to projectId,
                INTERNAL_FORM_ID to internalFormId
            )
        }
    }

    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }
    private val internalFormId by lazy { arguments!!.getInt(INTERNAL_FORM_ID) }
    private val adapter = PsiSignaturesAdapter(this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()

        fragment_pre_job_safety_signatures_add_workerTV.setOnClickListener {
            AddEmployeesToSignaturesDialogFragment.newInstance(
                employeeIds = adapter.getSignatures().map { it.employeeId },
                projectId = projectId,
                internalFormId = internalFormId
            ).show(childFragmentManager, AddEmployeesToSignaturesDialogFragment.TAG)
        }
    }

    fun getViewState() = adapter.getSignatures()

    fun picSaved() = adapter.notifyDataSetChanged()

    fun setEmployeeVerified(employeeId: Int, verifiedByName: String, verifiedById: Int) {
        val allEmployees = adapter.getSignatures()
        allEmployees.find { it.employeeId == employeeId }?.let { employee ->
            val position = allEmployees.indexOf(employee)
            val employeeVerified = employee.copy(
                pinUserName = verifiedByName,
                pinEmployeeId = verifiedById,
                pinDate = Date().toSimpleString()
            )
            allEmployees.removeAt(position)
            allEmployees.add(position, employeeVerified)
            adapter.notifyItemChanged(position)
        }
    }

    fun setViewState(employees: List<PsiEmployeeRow>) {
        adapter.submitList(employees)
    }

    override fun onWorkerRowAction(
        employee: PsiEmployeeRow,
        action: PsiSignaturesAdapter.WorkerRowAction
    ) {
        when (action) {
            PsiSignaturesAdapter.WorkerRowAction.ACTION_ENTER_PIN -> {
                VerifyEmployeeDialogFragment.newInstance(
                    employee.employeeId,
                    employee.pinImageFileLocation
                ).show(childFragmentManager, VerifyEmployeeDialogFragment.TAG)
            }

            PsiSignaturesAdapter.WorkerRowAction.ACTION_VIEW_PIN_OUT_IMAGE -> {
                PinOutImageDialog.newInstance(employee.pinImageFileLocation)
                    .show(childFragmentManager, PinOutImageDialog.TAG)
            }
        }

    }

    override fun addEmployees(employees: List<PsiEmployeeRow>) {
        adapter.addEmployees(employees)
    }

    private fun initRecyclerView() {
        fragment_pre_job_safety_signatures_recycler_view.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = this@PsiSignaturesFragment.adapter
        }
    }
}
