package com.fieldflo.screens.formPsi.generalInformation

data class PsiGeneralInformationViewState(
    val projectName: String,
    val formDate: String,
    val taskLocation: String,
    val musterMeetingPoint: String
)

data class EmployeeAndVerificationData(
    val employeeName: String,
    val employeeId: Int,
    val employeePinNumber: String,
    val verificationData: VerificationData
) {
    val showVerify
        get() = employeeId > 0

    val verified
        get() = employeeId == verificationData.verifiedEmployeeId &&
                verificationData.isVerified

    val showAsterisk
        get() = showVerify && !verified
}

/**
 * Represents which employee (if any) has been verified and who verified that employee
 */
data class VerificationData(
    val isVerified: Boolean,
    val verifiedEmployeeId: Int,
    val verifiedDate: String,
    val verifiedByName: String,
    val verifiedByEmployeeId: Int
)