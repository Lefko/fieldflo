package com.fieldflo.screens.formPsi.signatures.addEmployeesToSignatures

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.ListView
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector
import com.fieldflo.screens.formPsi.signatures.PsiEmployeeRow

class AddEmployeesToSignaturesDialogFragment : DialogFragment() {

    private val presenter by viewModels<AddEmployeesToSignaturesPresenter> { injector.psiFormAddEmployeesToSignaturesViewModelFactory() }

    private val employeeIds by lazy { arguments!!.getIntegerArrayList(EMPLOYEE_IDS_IN_PSI)!! }
    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }
    private val internalFormId by lazy { arguments!!.getInt(INTERNAL_FORM_ID) }
    private val employeesAdapter by lazy { EmployeesAdapter(activity!!) }

    private lateinit var callback: AddSignaturesCallback

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = AlertDialog.Builder(activity as Context)
            .setTitle(R.string.activity_pre_job_safety_add_employees)
            .setAdapter(employeesAdapter, null)
            .setPositiveButton(android.R.string.ok, { _, _ ->
                lifecycleScope.launchWhenCreated {
                    val listView = (dialog as AlertDialog).listView

                    val newSignatures = (0 until listView.adapter.count)
                        .filter { listView.checkedItemPositions[it] }
                        .mapNotNull { employeesAdapter.getItem(it) }
                        .filter { !employeeIds.contains(it.employeeId) }
                        .map {
                            PsiEmployeeRow(
                                employeeId = it.employeeId,
                                employeeFullName = it.employeeName,
                                employeePin = it.employeePin,
                                pinEmployeeId = 0,
                                pinImageFileLocation = presenter.getPsiPhotoPinFileLocation(
                                    projectId, it.employeeId, internalFormId
                                )
                            )
                        }
                    if (newSignatures.isNotEmpty()) {
                        callback.addEmployees(newSignatures)
                    }
                    dismiss()
                }

            })
            .setNegativeButton(android.R.string.cancel, { _, _ ->
                dismiss()
            })
            .show()

        dialog.listView.itemsCanFocus = false
        dialog.listView.choiceMode = ListView.CHOICE_MODE_MULTIPLE

        present()

        return dialog
    }

    private fun present() {
        lifecycleScope.launchWhenCreated {
            render(presenter.getEmployees())
        }
    }

    private fun render(employees: List<AddEmployeesToPsiEmployee>) {
        employeesAdapter.clear()
        employeesAdapter.addAll(employees.filter { !employeeIds.contains(it.employeeId) })
        employeesAdapter.notifyDataSetChanged()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    private fun onAttachToContext(context: Context) {
        this.callback = parentFragment as? AddSignaturesCallback
            ?: targetFragment as? AddSignaturesCallback
                    ?: context as? AddSignaturesCallback
                    ?: throw IllegalArgumentException(
                "${context.javaClass.simpleName} " +
                        "must be an instance of AddSignaturesCallback"
            )
    }

    companion object {

        const val TAG = "AddEmployeesToSignaturesDialogFragment"
        private const val PROJECT_ID = "project_id"
        private const val INTERNAL_FORM_ID = "internal_form_id"
        private const val EMPLOYEE_IDS_IN_PSI = "employee_ids_in_psi_key"

        fun newInstance(employeeIds: List<Int>, projectId: Int, internalFormId: Int) =
            AddEmployeesToSignaturesDialogFragment().apply {
                arguments = bundleOf(
                    EMPLOYEE_IDS_IN_PSI to employeeIds,
                    PROJECT_ID to projectId,
                    INTERNAL_FORM_ID to internalFormId
                )
            }
    }


    interface AddSignaturesCallback {
        fun addEmployees(employees: List<PsiEmployeeRow>)
    }
}