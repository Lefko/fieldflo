package com.fieldflo.screens.formPsi.taskSteps

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.fieldflo.R
import com.fieldflo.common.afterTextChanges
import kotlinx.android.synthetic.main.row_pre_job_safety_task_step.view.*

class PsiTaskStepsAdapter : RecyclerView.Adapter<PsiTaskStepsAdapter.PreJobTaskStepsViewHolder>() {

    private val tasks = mutableListOf<TaskStepsUiEntity>()

    override fun getItemCount(): Int {
        return tasks.size
    }

    fun addTasks(tasks: List<TaskStepsUiEntity>) {
        this.tasks.addAll(0, tasks)
        notifyItemRangeInserted(0, tasks.size)
    }

    fun submitList(list: List<TaskStepsUiEntity>) {
        tasks.clear()
        tasks.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        PreJobTaskStepsViewHolder(
            parent,
            R.layout.row_pre_job_safety_task_step,
            ::removeAt,
            ::getTaskStepsHazardControls
        )

    override fun onBindViewHolder(holder: PreJobTaskStepsViewHolder, position: Int) {
        holder.bind(tasks[position])
    }

    private fun removeAt(position: Int) {
        this.tasks.removeAt(position)
        notifyItemRemoved(position)
    }

    fun getTaskStepsHazardControls(): MutableList<TaskStepsUiEntity> {
        return tasks
    }

    class PreJobTaskStepsViewHolder(
        parent: ViewGroup,
        @LayoutRes itemViewLayoutId: Int,
        removeTaskCallback: (position: Int) -> Unit,
        rowListProvider: () -> MutableList<TaskStepsUiEntity>
    ) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            itemViewLayoutId,
            parent,
            false
        )
    ) {

        init {
            itemView.row_listview_task_steps_task_steps_input.afterTextChanges { text, _ ->
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val rows = rowListProvider()
                    val changedRowEntity = rows[position].copy(task = text)
                    rows[position] = changedRowEntity
                }
            }

            itemView.row_listview_task_steps_hazard_input.afterTextChanges { text, _ ->
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val rows = rowListProvider()
                    val changedRowEntity = rows[position].copy(hazard = text)
                    rows[position] = changedRowEntity
                }
            }

            itemView.row_listview_task_steps_control_input.afterTextChanges { text, _ ->
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val rows = rowListProvider()
                    val changedRowEntity = rows[position].copy(control = text)
                    rows[position] = changedRowEntity
                }
            }

            itemView.row_listview_task_steps_delete.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    removeTaskCallback(position)
                }
            }

            itemView.row_listview_task_steps_risk_rating.apply {
                adapter = ArrayAdapter.createFromResource(
                    itemView.context,
                    R.array.task_risk_rating,
                    R.layout.row_spinner_dropdown
                ).also { adapter ->
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                }
                onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {
                        val taskPosition = bindingAdapterPosition
                        if (taskPosition != RecyclerView.NO_POSITION) {
                            val rows = rowListProvider()
                            val changedRowEntity = rows[taskPosition].copy(risk = 0)
                            rows[taskPosition] = changedRowEntity
                        }
                    }

                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        val taskPosition = bindingAdapterPosition
                        if (taskPosition != RecyclerView.NO_POSITION) {
                            val rows = rowListProvider()
                            val changedRowEntity = rows[taskPosition].copy(risk = position)
                            rows[taskPosition] = changedRowEntity
                        }
                    }
                }
            }
        }

        fun bind(row: TaskStepsUiEntity) {
            itemView.row_listview_task_steps_task_steps_input.setText(row.task)
            itemView.row_listview_task_steps_hazard_input.setText(row.hazard)
            itemView.row_listview_task_steps_control_input.setText(row.control)
            itemView.row_listview_task_steps_risk_rating.setSelection(row.risk)
        }
    }
}