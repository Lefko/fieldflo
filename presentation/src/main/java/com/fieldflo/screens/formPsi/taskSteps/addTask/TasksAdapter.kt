package com.fieldflo.screens.formPsi.taskSteps.addTask

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.fieldflo.R


class TasksAdapter(context: Context) :
    ArrayAdapter<AddTasksViewState.TaskUiRow>(
        context,
        android.R.layout.simple_list_item_multiple_choice,
        mutableListOf()
    ) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val listItem = convertView ?: LayoutInflater.from(context)
            .inflate(R.layout.row_dialog_add_task, parent, false)

        (listItem as TextView).text = getItem(position)?.task

        return listItem
    }
}