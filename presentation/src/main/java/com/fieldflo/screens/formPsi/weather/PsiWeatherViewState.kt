package com.fieldflo.screens.formPsi.weather

data class PsiWeatherViewState(
    val reviewedWeather: Boolean?,
    val reviewedRoad: Boolean?,
    val reviewedHeatIndex: Boolean?
)
