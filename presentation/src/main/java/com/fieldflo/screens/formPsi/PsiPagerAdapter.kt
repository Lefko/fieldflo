package com.fieldflo.screens.formPsi

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.fieldflo.screens.formPsi.generalInformation.PsiGeneralInformationFragment
import com.fieldflo.screens.formPsi.other.PsiOtherFragment
import com.fieldflo.screens.formPsi.review.PsiReviewFragment
import com.fieldflo.screens.formPsi.signatures.PsiSignaturesFragment
import com.fieldflo.screens.formPsi.taskSteps.PsiTaskStepsFragment
import com.fieldflo.screens.formPsi.weather.PsiWeatherFragment

@Suppress("DEPRECATION")
class PsiPagerAdapter(
    fm: FragmentManager,
    private val pageTitles: Array<String>,
    private val projectId: Int,
    private val internalFormId: Int
) : FragmentPagerAdapter(fm) {

    val generalInfo: PsiGeneralInformationFragment = PsiGeneralInformationFragment.newInstance()
    val review: PsiReviewFragment = PsiReviewFragment.newInstance()
    val taskSteps: PsiTaskStepsFragment = PsiTaskStepsFragment.newInstance()
    val weather: PsiWeatherFragment = PsiWeatherFragment.newInstance()
    val signatures: PsiSignaturesFragment =
        PsiSignaturesFragment.newInstance(projectId, internalFormId)
    val other: PsiOtherFragment = PsiOtherFragment.newInstance()

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> generalInfo
            1 -> review
            2 -> taskSteps
            3 -> weather
            4 -> signatures
            5 -> other
            else -> throw IllegalArgumentException("There is no tab for position: $position")
        }
    }

    override fun getCount() = pageTitles.size

    override fun getPageTitle(position: Int): CharSequence? {
        return pageTitles[position]
    }
}