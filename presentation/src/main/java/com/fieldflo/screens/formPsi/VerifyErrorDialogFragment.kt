package com.fieldflo.screens.formPsi

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.fieldflo.R

class VerifyErrorDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(activity as Context)
            .setMessage(R.string.activity_daily_log_form_wrong_pin_entered)
            .setNeutralButton(android.R.string.ok, { _, _ ->
                dismiss()
            }).show()
    }

    companion object {
        const val TAG = "VerifyErrorDialogFragment"

        fun newInstance() = VerifyErrorDialogFragment()
    }
}