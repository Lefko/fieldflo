package com.fieldflo.screens.formPsi.review

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.fieldflo.R
import kotlinx.android.synthetic.main.fragment_psi_review.*

class PsiReviewFragment : Fragment(R.layout.fragment_psi_review) {

    companion object {
        fun newInstance() = PsiReviewFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragment_pre_job_safety_review_ppe_respiratorSP.adapter = ArrayAdapter.createFromResource(
            context!!, R.array.fragment_pre_job_safety_respirators, R.layout.row_spinner_dropdown
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
    }

    fun getViewState() =
        PsiReviewViewState(
            //Environmental Hazards
            spillPotential = fragment_pre_job_safety_review_env_hazard_spill_potential_radiobutton.isChecked,
            hazmatStorage = fragment_pre_job_safety_review_env_hazard_hazmat_radiobutton.isChecked,
            weather = fragment_pre_job_safety_review_env_hazard_weather_radiobutton.isChecked,
            sdsReviewForHazmat = fragment_pre_job_safety_review_env_hazard_sds_reviewed_radiobutton.isChecked,
            ventilationRequired = fragment_pre_job_safety_review_env_hazard_ventilation_radiobutton.isChecked,
            heatStress = fragment_pre_job_safety_review_env_hazard_heat_stress_radiobutton.isChecked,
            lightningLevelsTooLow = fragment_pre_job_safety_review_env_hazard_lighting_levels_radiobutton.isChecked,
            housekeeping = fragment_pre_job_safety_review_env_hazard_housekeeping_radiobutton.isChecked,
            //Ergonomics Hazards / Material Handling
            workingTightArea = fragment_pre_job_safety_review_ergo_hazards_working_in_radiobutton.isChecked,
            partOfBodyInLineOfFire = fragment_pre_job_safety_review_ergo_hazards_part_of_body_radiobutton.isChecked,
            workingAboveYourHead = fragment_pre_job_safety_review_ergo_hazards_working_above_radiobutton.isChecked,
            pinchPointsIdentified = fragment_pre_job_safety_review_ergo_hazards_pinch_points_radiobutton.isChecked,
            repetitiveMotion = fragment_pre_job_safety_review_ergo_hazards_repetitive_motion_radiobutton.isChecked,

            //Work at Height Hazards
            barricadesFlaggingAndSignInPlace = fragment_pre_job_safety_review_work_at_height_hazards_barricades_radiobutton.isChecked,
            holeCoveringsInPlace = fragment_pre_job_safety_review_work_at_height_hazards_hole_coverings_radiobutton.isChecked,
            protectFromFallingItems = fragment_pre_job_safety_review_work_at_height_hazards_protect_from_falling_radiobutton.isChecked,
            poweredPlatforms = fragment_pre_job_safety_review_work_at_height_hazards_powered_platforms_radiobutton.isChecked,
            othersWorkingOverhead = fragment_pre_job_safety_review_work_at_height_hazards_other_working_radiobutton.isChecked,
            fallArrestSystems = fragment_pre_job_safety_review_work_at_height_hazards_fall_arrest_systems_radiobutton.isChecked,
            ladders = fragment_pre_job_safety_review_work_at_height_hazards_ladders_radiobutton.isChecked,
            //Activity Hazards
            welding = fragment_pre_job_safety_review_activity_hazards_welding_radiobutton.isChecked,
            burnSources = fragment_pre_job_safety_review_activity_hazards_burn_radiobutton.isChecked,
            compressedGasses = fragment_pre_job_safety_review_activity_hazards_compr_gasses_radiobutton.isChecked,
            workingOnEnergizedEquipment = fragment_pre_job_safety_review_activity_hazards_working_on_radiobutton.isChecked,
            electricalCordsCondition = fragment_pre_job_safety_review_activity_hazards_electrical_cords_radiobutton.isChecked,
            equipmentInspected = fragment_pre_job_safety_review_activity_hazards_equipment_radiobutton.isChecked,
            criticalLiftMeetingRequired = fragment_pre_job_safety_review_activity_hazards_critical_lift_radiobutton.isChecked,
            energyIsolation = fragment_pre_job_safety_review_activity_hazards_energy_isolation_radiobutton.isChecked,
            airborneParticles = fragment_pre_job_safety_review_activity_hazards_airborne_radiobutton.isChecked,
            openHoles = fragment_pre_job_safety_review_activity_hazards_open_holes_radiobutton.isChecked,
            mobileEquipment = fragment_pre_job_safety_review_activity_hazards_mobile_equip_radiobutton.isChecked,
            rigging = fragment_pre_job_safety_review_activity_hazards_rigging_radiobutton.isChecked,
            excavation = fragment_pre_job_safety_review_activity_hazards_excavation_radiobutton.isChecked,
            confinedSpace = fragment_pre_job_safety_review_activity_hazards_confined_space_radiobutton.isChecked,
            //Access / Egress Hazards
            scaffold = fragment_pre_job_safety_review_access_hazards_scaffold_radiobutton.isChecked,
            slipPotentialIdentified = fragment_pre_job_safety_review_access_hazards_slip_radiobutton.isChecked,
            requiredPermitsInPlace = fragment_pre_job_safety_review_access_hazards_required_permits_radiobutton.isChecked,
            excavations = fragment_pre_job_safety_review_access_hazards_excavations_radiobutton.isChecked,
            walkways = fragment_pre_job_safety_review_access_hazards_walkways_radiobutton.isChecked,
            other = fragment_pre_job_safety_review_other_field.text.toString(),
            //Personal Limitations / Hazards
            clearInstructionsProvided = fragment_pre_job_safety_review_personal_limitations_clear_instr_radiobutton.isChecked,
            trainedToUseToolAndPerformTask = fragment_pre_job_safety_review_personal_limitations_trained_to_use_radiobutton.isChecked,
            distractionsInWorkArea = fragment_pre_job_safety_review_personal_limitations_distractions_radiobutton.isChecked,
            workingAlone = fragment_pre_job_safety_review_personal_limitations_working_alone_radiobutton.isChecked,
            liftTooHeavy = fragment_pre_job_safety_review_personal_limitations_lift_too_heavy_radiobutton.isChecked,
            externalNoiseLevel = fragment_pre_job_safety_review_personal_limitations_external_noise_radiobutton.isChecked,
            physicalLimitations = fragment_pre_job_safety_review_personal_limitations_physical_limit_radiobutton.isChecked,
            firstAidRequirements = fragment_pre_job_safety_review_personal_limitations_first_aid_radiobutton.isChecked,
            //PPE Requirements
            goggles = fragment_pre_job_safety_review_ppe_goggles_radiobutton.isChecked,
            faceShield = fragment_pre_job_safety_review_ppe_face_shield_radiobutton.isChecked,
            gloves = fragment_pre_job_safety_review_ppe_gloves_radiobutton.isChecked,
            coverall = fragment_pre_job_safety_review_ppe_coverall_radiobutton.isChecked,
            hearingProtection = fragment_pre_job_safety_review_ppe_hearing_pro_radiobutton.isChecked,
            respirator = fragment_pre_job_safety_review_ppe_respirator_radiobutton.isChecked,
            harness = fragment_pre_job_safety_review_ppe_harness_radiobutton.isChecked,
            reflectiveVest = fragment_pre_job_safety_review_ppe_reflective_vest_radiobutton.isChecked,
            footwear = fragment_pre_job_safety_review_ppe_footwear_radiobutton.isChecked,
            safetyGlasses = fragment_pre_job_safety_review_ppe_safety_glasses_radiobutton.isChecked,
            weldingHood = fragment_pre_job_safety_review_ppe_welding_hood_radiobutton.isChecked,
            tyvexSuit = fragment_pre_job_safety_review_ppe_tyvex_suit_radiobutton.isChecked,
            retractableLanyard = fragment_pre_job_safety_review_ppe_retractable_lanyard_radiobutton.isChecked,
            hardHat = fragment_pre_job_safety_review_ppe_hardhat_radiobutton.isChecked,
            respiratorName = if (fragment_pre_job_safety_review_ppe_respiratorSP.selectedItem.toString() != context?.getString(
                    R.string.fragment_pre_job_safety_respirator_please_select
                )
            )
                fragment_pre_job_safety_review_ppe_respiratorSP.selectedItem.toString() else ""
        )

    fun setViewState(viewState: PsiReviewViewState) {
        render(viewState)
    }

    private fun render(viewState: PsiReviewViewState) {
        //Environmental Hazards
        fragment_pre_job_safety_review_env_hazard_spill_potential_radiobutton.isChecked =
            viewState.spillPotential
        fragment_pre_job_safety_review_env_hazard_hazmat_radiobutton.isChecked =
            viewState.hazmatStorage
        fragment_pre_job_safety_review_env_hazard_weather_radiobutton.isChecked =
            viewState.weather
        fragment_pre_job_safety_review_env_hazard_sds_reviewed_radiobutton.isChecked =
            viewState.sdsReviewForHazmat
        fragment_pre_job_safety_review_env_hazard_ventilation_radiobutton.isChecked =
            viewState.ventilationRequired
        fragment_pre_job_safety_review_env_hazard_heat_stress_radiobutton.isChecked =
            viewState.heatStress
        fragment_pre_job_safety_review_env_hazard_lighting_levels_radiobutton.isChecked =
            viewState.lightningLevelsTooLow
        fragment_pre_job_safety_review_env_hazard_housekeeping_radiobutton.isChecked =
            viewState.housekeeping

        //Ergonomics Hazards / Material Handling
        fragment_pre_job_safety_review_ergo_hazards_working_in_radiobutton.isChecked =
            viewState.workingTightArea
        fragment_pre_job_safety_review_ergo_hazards_part_of_body_radiobutton.isChecked =
            viewState.partOfBodyInLineOfFire
        fragment_pre_job_safety_review_ergo_hazards_working_above_radiobutton.isChecked =
            viewState.workingAboveYourHead
        fragment_pre_job_safety_review_ergo_hazards_pinch_points_radiobutton.isChecked =
            viewState.pinchPointsIdentified
        fragment_pre_job_safety_review_ergo_hazards_repetitive_motion_radiobutton.isChecked =
            viewState.repetitiveMotion

        //Work at Height Hazards
        fragment_pre_job_safety_review_work_at_height_hazards_barricades_radiobutton.isChecked =
            viewState.barricadesFlaggingAndSignInPlace
        fragment_pre_job_safety_review_work_at_height_hazards_hole_coverings_radiobutton.isChecked =
            viewState.holeCoveringsInPlace
        fragment_pre_job_safety_review_work_at_height_hazards_protect_from_falling_radiobutton.isChecked =
            viewState.protectFromFallingItems
        fragment_pre_job_safety_review_work_at_height_hazards_powered_platforms_radiobutton.isChecked =
            viewState.poweredPlatforms
        fragment_pre_job_safety_review_work_at_height_hazards_other_working_radiobutton.isChecked =
            viewState.othersWorkingOverhead
        fragment_pre_job_safety_review_work_at_height_hazards_fall_arrest_systems_radiobutton.isChecked =
            viewState.fallArrestSystems
        fragment_pre_job_safety_review_work_at_height_hazards_ladders_radiobutton.isChecked =
            viewState.ladders

        //Activity Hazards
        fragment_pre_job_safety_review_activity_hazards_welding_radiobutton.isChecked =
            viewState.welding
        fragment_pre_job_safety_review_activity_hazards_burn_radiobutton.isChecked =
            viewState.burnSources
        fragment_pre_job_safety_review_activity_hazards_compr_gasses_radiobutton.isChecked =
            viewState.compressedGasses
        fragment_pre_job_safety_review_activity_hazards_working_on_radiobutton.isChecked =
            viewState.workingOnEnergizedEquipment
        fragment_pre_job_safety_review_activity_hazards_electrical_cords_radiobutton.isChecked =
            viewState.electricalCordsCondition
        fragment_pre_job_safety_review_activity_hazards_equipment_radiobutton.isChecked =
            viewState.equipmentInspected
        fragment_pre_job_safety_review_activity_hazards_critical_lift_radiobutton.isChecked =
            viewState.criticalLiftMeetingRequired
        fragment_pre_job_safety_review_activity_hazards_energy_isolation_radiobutton.isChecked =
            viewState.energyIsolation
        fragment_pre_job_safety_review_activity_hazards_airborne_radiobutton.isChecked =
            viewState.airborneParticles
        fragment_pre_job_safety_review_activity_hazards_open_holes_radiobutton.isChecked =
            viewState.openHoles
        fragment_pre_job_safety_review_activity_hazards_mobile_equip_radiobutton.isChecked =
            viewState.mobileEquipment
        fragment_pre_job_safety_review_activity_hazards_rigging_radiobutton.isChecked =
            viewState.rigging
        fragment_pre_job_safety_review_activity_hazards_excavation_radiobutton.isChecked =
            viewState.excavations
        fragment_pre_job_safety_review_activity_hazards_confined_space_radiobutton.isChecked =
            viewState.confinedSpace

        //Access / Egress Hazards
        fragment_pre_job_safety_review_access_hazards_scaffold_radiobutton.isChecked =
            viewState.scaffold
        fragment_pre_job_safety_review_access_hazards_slip_radiobutton.isChecked =
            viewState.slipPotentialIdentified
        fragment_pre_job_safety_review_access_hazards_required_permits_radiobutton.isChecked =
            viewState.requiredPermitsInPlace
        fragment_pre_job_safety_review_access_hazards_excavations_radiobutton.isChecked =
            viewState.excavations
        fragment_pre_job_safety_review_access_hazards_walkways_radiobutton.isChecked =
            viewState.walkways
        fragment_pre_job_safety_review_other_field.setText(viewState.other)

        //Personal Limitations / Hazards
        fragment_pre_job_safety_review_personal_limitations_clear_instr_radiobutton.isChecked =
            viewState.clearInstructionsProvided
        fragment_pre_job_safety_review_personal_limitations_trained_to_use_radiobutton.isChecked =
            viewState.trainedToUseToolAndPerformTask
        fragment_pre_job_safety_review_personal_limitations_distractions_radiobutton.isChecked =
            viewState.distractionsInWorkArea
        fragment_pre_job_safety_review_personal_limitations_working_alone_radiobutton.isChecked =
            viewState.workingAlone
        fragment_pre_job_safety_review_personal_limitations_lift_too_heavy_radiobutton.isChecked =
            viewState.liftTooHeavy
        fragment_pre_job_safety_review_personal_limitations_external_noise_radiobutton.isChecked =
            viewState.externalNoiseLevel
        fragment_pre_job_safety_review_personal_limitations_physical_limit_radiobutton.isChecked =
            viewState.physicalLimitations
        fragment_pre_job_safety_review_personal_limitations_first_aid_radiobutton.isChecked =
            viewState.firstAidRequirements

        //PPE Requirements
        fragment_pre_job_safety_review_ppe_goggles_radiobutton.isChecked = viewState.goggles
        fragment_pre_job_safety_review_ppe_face_shield_radiobutton.isChecked = viewState.faceShield
        fragment_pre_job_safety_review_ppe_gloves_radiobutton.isChecked = viewState.gloves
        fragment_pre_job_safety_review_ppe_coverall_radiobutton.isChecked = viewState.coverall
        fragment_pre_job_safety_review_ppe_hearing_pro_radiobutton.isChecked =
            viewState.hearingProtection
        fragment_pre_job_safety_review_ppe_respirator_radiobutton.isChecked = viewState.respirator
        fragment_pre_job_safety_review_ppe_harness_radiobutton.isChecked = viewState.harness
        fragment_pre_job_safety_review_ppe_reflective_vest_radiobutton.isChecked =
            viewState.reflectiveVest
        fragment_pre_job_safety_review_ppe_footwear_radiobutton.isChecked = viewState.footwear
        fragment_pre_job_safety_review_ppe_safety_glasses_radiobutton.isChecked =
            viewState.safetyGlasses
        fragment_pre_job_safety_review_ppe_welding_hood_radiobutton.isChecked =
            viewState.weldingHood
        fragment_pre_job_safety_review_ppe_tyvex_suit_radiobutton.isChecked = viewState.tyvexSuit
        fragment_pre_job_safety_review_ppe_retractable_lanyard_radiobutton.isChecked =
            viewState.retractableLanyard

        fragment_pre_job_safety_review_ppe_hardhat_radiobutton.isChecked = viewState.hardHat
        val selectedRespiratorIndex =
            context!!.resources.getStringArray(R.array.fragment_pre_job_safety_respirators)
                .indexOf(viewState.respiratorName)

        if (selectedRespiratorIndex > -1) {
            fragment_pre_job_safety_review_ppe_respiratorSP.setSelection(selectedRespiratorIndex)
        }
    }
}
