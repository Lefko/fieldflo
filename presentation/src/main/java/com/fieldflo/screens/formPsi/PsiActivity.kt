package com.fieldflo.screens.formPsi

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenCreated
import com.fieldflo.R
import com.fieldflo.common.*
import com.fieldflo.common.hiddenCamera.CameraCallbacks
import com.fieldflo.common.hiddenCamera.CameraConfig
import com.fieldflo.common.hiddenCamera.CameraPreview
import com.fieldflo.common.hiddenCamera.config.CameraFacing
import com.fieldflo.common.hiddenCamera.config.CameraImageFormat
import com.fieldflo.common.hiddenCamera.config.CameraResolution
import com.fieldflo.common.hiddenCamera.config.CameraRotation
import com.fieldflo.di.injector
import com.fieldflo.screens.drawer.DrawerManager
import com.fieldflo.screens.formPsi.generalInformation.EmployeeAndVerificationData
import com.fieldflo.screens.formPsi.generalInformation.PsiGeneralInformationFragment
import com.fieldflo.screens.formPsi.other.PsiOtherFragment
import com.fieldflo.screens.formPsi.review.PsiReviewFragment
import com.fieldflo.screens.formPsi.signatures.PsiSignaturesFragment
import com.fieldflo.screens.formPsi.taskSteps.PsiTaskStepsFragment
import com.fieldflo.screens.formPsi.weather.PsiWeatherFragment
import com.fieldflo.screens.selectEmployee.EmployeeUiRow
import com.fieldflo.screens.selectEmployee.SelectEmployeeDialog
import com.fieldflo.screens.selectSupervisor.SelectSupervisorDialog
import com.fieldflo.screens.selectSupervisor.SupervisorEmployeeUiRow
import com.fieldflo.usecases.PinUseCases
import com.fieldflo.usecases.ProjectStatus
import kotlinx.android.synthetic.main.activity_pre_job_safety.*
import kotlinx.android.synthetic.main.view_activity_pre_job_safety.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File

class PsiActivity : AppCompatActivity(R.layout.activity_pre_job_safety),
    VerifyDialogFragment.VerifyDialogCallback,
    VerifyEmployeeDialogFragment.VerifyEmployeeDialogCallback,
    SelectEmployeeDialog.EmployeeSelectedCallback,
    SelectSupervisorDialog.SupervisorSelectedCallback,
    CameraCallbacks {

    companion object {
        private const val PROJECT_ID_KEY = "project_id"
        private const val INTERNAL_FORM_ID_KEY = "internal_form_id"
        private const val CAMERA_PERMISSION_REQUEST = 1337

        const val AUDITOR_EMPLOYEE_IDENTIFIER = 2

        fun start(sender: Activity, projectId: Int, internalFormId: Int) {
            val intent = Intent(sender, PsiActivity::class.java).apply {
                putExtra(INTERNAL_FORM_ID_KEY, internalFormId)
                putExtra(PROJECT_ID_KEY, projectId)
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            sender.startActivity(intent)
        }
    }

    // Camera Related
    private lateinit var cameraPreview: CameraPreview
    private var cameraInitialized = false


    private val presenter by viewModels<PsiPresenter> { injector.psiFormViewModelFactory() }
    private val internalFormId: Int by lazy { intent.getIntExtra(INTERNAL_FORM_ID_KEY, 0) }
    private val projectId: Int by lazy { intent.getIntExtra(PROJECT_ID_KEY, 0) }
    private val drawerManager by lazy {
        DrawerManager(
            { drawer_layout }, { this },
            { activity_pre_job_safety_content_toolbar }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        cameraPreview = createPreview(root_constraintLayout)

        activity_pre_job_safety_content_pages.adapter = PsiPagerAdapter(
            supportFragmentManager,
            resources.getStringArray(R.array.activity_pre_job_safety_form_page_titles),
            projectId,
            internalFormId
        )
        activity_pre_job_safety_content_pages.offscreenPageLimit = 5

        activity_pre_job_safety_content_back.safeClickListener { finish() }
        activity_pre_job_safety_content_save.safeClickListener { finish() }

        activity_pre_job_safety_content_previous.setOnClickListener {
            if (activity_pre_job_safety_content_pages.currentItem > 0) {
                activity_pre_job_safety_content_pages.currentItem--
            }
        }

        activity_pre_job_safety_content_next_sheet.setOnClickListener {
            if (activity_pre_job_safety_content_pages.currentItem < activity_pre_job_safety_content_pages.childCount) {
                activity_pre_job_safety_content_pages.currentItem++
            }
        }

        activity_per_job_safety_content_tabs.setupWithViewPager(
            activity_pre_job_safety_content_pages
        )
        supportFragmentManager.registerFragmentLifecycleCallbacks(object :
            FragmentManager.FragmentLifecycleCallbacks() {
            override fun onFragmentViewCreated(
                fm: FragmentManager,
                f: Fragment,
                v: View,
                savedInstanceState: Bundle?
            ) {
                super.onFragmentViewCreated(fm, f, v, savedInstanceState)
                present()
                supportFragmentManager.unregisterFragmentLifecycleCallbacks(this)
            }
        }, false)

        activity_pre_job_safety_content_pages.post {
            activity_pre_job_safety_content_pages.onPageChanges {
                hideKeyboard()
                renderNextPreviousEnabled(it)
            }
        }
    }

    override fun onBackPressed() {
        if (!drawerManager.onBackPress()) {
            super.onBackPressed()
        }
    }

    override fun onStart() {
        super.onStart()
        setupCamera()
    }

    override fun onStop() {
        stopCamera()
        super.onStop()
    }

    override fun onDestroy() {
        presenter.savePsiData(extractFullPsiViewState(), internalFormId, projectId)
        super.onDestroy()
    }

    private fun setupCamera() {
        if (presenter.requiresPhotoPin) {
            if (!hasPermission(Manifest.permission.CAMERA)) {
                getPermission(Manifest.permission.CAMERA, PsiActivity.CAMERA_PERMISSION_REQUEST)
            } else {
                if (!cameraInitialized) {
                    val cameraConfig = CameraConfig.Builder(this)
                        .facing(CameraFacing.FRONT_FACING_CAMERA)
                        .resolution(CameraResolution.LOW_RESOLUTION)
                        .imageFormat(CameraImageFormat.FORMAT_JPEG)
                        .imageRotation(CameraRotation.ROTATION_270)
                        .build()
                    cameraPreview.startCameraInternal(cameraConfig)
                    cameraInitialized = true
                }
            }
        }
    }

    private fun renderNextPreviousEnabled(currentPage: Int) {
        activity_pre_job_safety_content_previous.isEnabled = currentPage > 0
        activity_pre_job_safety_content_next_sheet.isEnabled =
            currentPage < activity_pre_job_safety_content_pages.childCount - 1
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        setupCamera()
    }

    override fun onImageCapture(imageFile: File) {
        Timber.d("Captured pin image: ${imageFile.absoluteFile}")
        (activity_pre_job_safety_content_pages.adapter as? PsiPagerAdapter)?.let { adapter ->
            adapter.signatures.picSaved()
        }
    }

    override fun onCameraError(errorCode: Int) {
        Timber.d("Error taking pic")
    }

    private fun createPreview(constraintLayout: ConstraintLayout): CameraPreview {
        val context = constraintLayout.context
        val cameraPreview = CameraPreview(context, this)

        cameraPreview.id = View.generateViewId()
        cameraPreview.layoutParams = ConstraintLayout.LayoutParams(1, 1)

        constraintLayout.addView(cameraPreview)

        val set = ConstraintSet()
        set.clone(constraintLayout)
        set.connect(cameraPreview.id, ConstraintSet.START, constraintLayout.id, ConstraintSet.START)
        set.connect(
            cameraPreview.id,
            ConstraintSet.BOTTOM,
            constraintLayout.id,
            ConstraintSet.BOTTOM
        )
        set.applyTo(constraintLayout)

        return cameraPreview
    }

    private fun stopCamera() {
        cameraPreview.stopPreviewAndFreeCamera()
        cameraInitialized = false
    }

    private fun extractFullPsiViewState(): PsiViewState {
        val adapter = activity_pre_job_safety_content_pages.adapter as PsiPagerAdapter
        val generalInfo = adapter.generalInfo.getGeneralInformationViewState()
        val reviewViewState = adapter.review.getViewState()
        val weatherViewState = adapter.weather.getViewState()
        val otherViewState = adapter.other.getViewState()
        val employees = adapter.signatures.getViewState()
        val tasks = adapter.taskSteps.getViewState()
        return PsiViewState(
            projectName = activity_pre_job_safety_content_project_number.text.toString(),
            projectNumber = activity_pre_job_safety_content_project_name.text.toString(),
            generalInformationViewState = generalInfo,
            reviewViewState = reviewViewState,
            weatherViewState = weatherViewState,
            tasks = tasks,
            employees = employees,
            otherViewState = otherViewState
        )
    }

    private fun present() {
        lifecycleScope.launchWhenCreated {
            presenter.getProjectStatusFlow(projectId)
                .filter { it == ProjectStatus.DELETED }
                .collect { finish() }
        }
        lifecycleScope.launchWhenCreated {
            presenter.auditorData.collect { renderAuditor(it) }
        }
        lifecycleScope.launchWhenCreated {
            presenter.supervisorData.collect { renderSupervisor(it) }
        }
        lifecycleScope.launchWhenCreated {
            val viewState = presenter.getPsiData(internalFormId, projectId)
            render(viewState)
        }
    }

    private fun render(viewState: PsiViewState) {
        activity_pre_job_safety_content_project_number.text = viewState.projectNumber
        activity_pre_job_safety_content_project_name.text = viewState.projectName
        (activity_pre_job_safety_content_pages.adapter as? PsiPagerAdapter)?.let { adapter ->
            for (i in 0 until adapter.count) {
                val fragment = adapter.getItem(i)
                when (fragment) {
                    is PsiGeneralInformationFragment -> fragment.setViewState(viewState.generalInformationViewState)

                    is PsiReviewFragment -> fragment.setViewState(viewState.reviewViewState)

                    is PsiWeatherFragment -> fragment.setViewState(viewState.weatherViewState)

                    is PsiOtherFragment -> fragment.setViewState(viewState.otherViewState)

                    is PsiTaskStepsFragment -> fragment.setViewState(viewState.tasks)

                    is PsiSignaturesFragment -> fragment.setViewState(viewState.employees)
                }
            }
        }
    }

    private fun renderAuditor(auditorData: EmployeeAndVerificationData) {
        (activity_pre_job_safety_content_pages.adapter as? PsiPagerAdapter)?.let { adapter ->
            adapter.generalInfo.renderAuditor(auditorData)
        }
    }

    private fun renderSupervisor(supervisorData: EmployeeAndVerificationData) {
        (activity_pre_job_safety_content_pages.adapter as? PsiPagerAdapter)?.let { adapter ->
            adapter.generalInfo.renderSupervisor(supervisorData)
        }
    }

    private fun showErrorDialog() {
        VerifyErrorDialogFragment.newInstance()
            .show(supportFragmentManager, VerifyErrorDialogFragment.TAG)
    }

    private fun takePic(pinOutImageLocation: File) {
        val takePic = hasPermission(Manifest.permission.CAMERA) &&
                cameraInitialized && presenter.requiresPhotoPin
        if (takePic) {
            cameraPreview.takePictureInternal(pinOutImageLocation)
        }
    }

    override fun onEmployeeSelected(
        employeeIdentifier: Int,
        employee: EmployeeUiRow
    ) {
        when (employeeIdentifier) {
            AUDITOR_EMPLOYEE_IDENTIFIER -> presenter.changeAuditor(employee)
        }
    }

    override fun onSupervisorSelected(employee: SupervisorEmployeeUiRow) {
        presenter.changeSupervisor(employee)
    }

    override fun onPinSubmit(
        employeeId: Int,
        employeePin: String,
        enteredPin: String,
        employeeType: VerifyDialogFragment.EmployeePinType
    ) {
        if (!PinUseCases.validatePin(enteredPin, employeePin)) {
            showErrorDialog()
        } else {
            when (employeeType) {
                VerifyDialogFragment.EmployeePinType.EMPLOYEE_SUPERVISOR -> presenter.verifySupervisor()
                VerifyDialogFragment.EmployeePinType.EMPLOYEE_AUDITOR -> presenter.verifyAuditor()
            }
        }
    }

    override fun onVerifyEmployeePinSubmit(
        employeeId: Int,
        enteredPin: String,
        pinOutImageLocation: File
    ) {
        lifecycleScope.launch {
            val verificationResult = presenter.verifyEmployee(employeeId, enteredPin)
            whenCreated {
                if (!verificationResult.isVerified) {
                    showErrorDialog()
                } else {
                    (activity_pre_job_safety_content_pages.adapter as? PsiPagerAdapter)?.let { adapter ->
                        adapter.signatures.setEmployeeVerified(
                            employeeId,
                            verificationResult.verifiedByName,
                            verificationResult.verifiedById
                        )
                    }
                    delay(1000)
                    takePic(pinOutImageLocation)
                }
            }
        }
    }
}
