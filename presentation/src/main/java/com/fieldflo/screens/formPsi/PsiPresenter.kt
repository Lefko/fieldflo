package com.fieldflo.screens.formPsi

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fieldflo.AppScopeHolder
import com.fieldflo.common.toDateString
import com.fieldflo.common.toSimpleString
import com.fieldflo.data.persistence.db.entities.PsiTaskData
import com.fieldflo.screens.formPsi.generalInformation.EmployeeAndVerificationData
import com.fieldflo.screens.formPsi.generalInformation.PsiGeneralInformationViewState
import com.fieldflo.screens.formPsi.generalInformation.VerificationData
import com.fieldflo.screens.formPsi.other.PsiOtherViewState
import com.fieldflo.screens.formPsi.review.PsiReviewViewState
import com.fieldflo.screens.formPsi.signatures.PsiEmployeeRow
import com.fieldflo.screens.formPsi.taskSteps.TaskStepsUiEntity
import com.fieldflo.screens.formPsi.weather.PsiWeatherViewState
import com.fieldflo.screens.selectEmployee.EmployeeUiRow
import com.fieldflo.screens.selectSupervisor.SupervisorEmployeeUiRow
import com.fieldflo.usecases.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import javax.inject.Inject

class PsiPresenter @Inject constructor(
    private val psiUseCases: PsiUseCases,
    private val projectsUseCases: ProjectsUseCases,
    private val employeeUseCases: EmployeeUseCases,
    private val pinUseCases: PinUseCases,
    private val appScopeHolder: AppScopeHolder,
    settingsUseCase: SettingsUseCase
) : ViewModel() {

    val requiresPhotoPin: Boolean = settingsUseCase.usesPhotoPin

    private val _supervisorData = MutableStateFlow<EmployeeAndVerificationData?>(null)
    val supervisorData = _supervisorData.filterNotNull()

    private val _auditorData = MutableStateFlow<EmployeeAndVerificationData?>(null)
    val auditorData = _auditorData.filterNotNull()

    suspend fun getPsiData(internalFormId: Int, projectId: Int): PsiViewState {

        val psiUiData = psiUseCases.getPsiFormData(internalFormId, projectId)

        _supervisorData.value = EmployeeAndVerificationData(
            employeeId = psiUiData.supervisor?.employeeId ?: 0,
            employeeName = psiUiData.supervisor?.employeeFullName ?: "",
            employeePinNumber = psiUiData.supervisor?.employeePinNumber ?: "",
            verificationData = createVerificationData(
                isVerified = psiUiData.psiFormData.supervisorVerified,
                verifiedEmployeeId = psiUiData.psiFormData.supervisorId,
                verifiedDate = psiUiData.psiFormData.supervisorVerifiedDate,
                verifiedByName = psiUiData.psiFormData.supervisorVerifiedByName,
                verifiedByEmployeeId = psiUiData.psiFormData.supervisorVerifiedByEmployeeId
            )
        )

        _auditorData.value = EmployeeAndVerificationData(
            employeeId = psiUiData.auditor?.employeeId ?: 0,
            employeeName = psiUiData.auditor?.employeeFullName ?: "",
            employeePinNumber = psiUiData.auditor?.employeePinNumber ?: "",
            verificationData = createVerificationData(
                isVerified = psiUiData.psiFormData.auditorPinVerified,
                verifiedEmployeeId = psiUiData.psiFormData.auditorId,
                verifiedDate = psiUiData.psiFormData.auditorVerifiedDate,
                verifiedByName = psiUiData.psiFormData.auditorVerifiedByName,
                verifiedByEmployeeId = psiUiData.psiFormData.auditorVerifiedByEmployeeId
            )
        )

        return psiUiData.toUiModel()
    }

    fun getProjectStatusFlow(projectId: Int): Flow<ProjectStatus> {
        return projectsUseCases.getProjectStatusFlow(projectId)
    }

    fun savePsiData(viewState: PsiViewState, internalFormId: Int, projectId: Int) {
        appScopeHolder.scope.launch {
            val saveData = withContext(Dispatchers.Default) {
                viewState.toPsiData(
                    _supervisorData.value,
                    _auditorData.value,
                    internalFormId,
                    projectId
                )
            }
            psiUseCases.savePsiFormData(saveData)
        }
    }

    fun changeSupervisor(employee: SupervisorEmployeeUiRow) {
        viewModelScope.launch {
            employeeUseCases.getEmployeeById(employee.employeeId)?.let { employeeEntity ->
                val currentData: EmployeeAndVerificationData? = _supervisorData.value
                val newSupervisor = EmployeeAndVerificationData(
                    employeeName = employeeEntity.employeeFullName,
                    employeeId = employeeEntity.employeeId,
                    employeePinNumber = employeeEntity.employeePinNumber,
                    verificationData = currentData?.verificationData ?: VerificationData(
                        verifiedEmployeeId = 0,
                        isVerified = false,
                        verifiedDate = "",
                        verifiedByName = "",
                        verifiedByEmployeeId = 0
                    )
                )
                _supervisorData.value = newSupervisor
            }
        }
    }

    fun changeAuditor(employee: EmployeeUiRow) {
        viewModelScope.launch {
            employeeUseCases.getEmployeeById(employee.employeeId)?.let { employeeEntity ->
                val currentData: EmployeeAndVerificationData? = _auditorData.value
                val newAuditor = EmployeeAndVerificationData(
                    employeeName = employeeEntity.employeeFullName,
                    employeeId = employeeEntity.employeeId,
                    employeePinNumber = employeeEntity.employeePinNumber,
                    verificationData = currentData?.verificationData ?: VerificationData(
                        verifiedEmployeeId = 0,
                        isVerified = false,
                        verifiedDate = "",
                        verifiedByName = "",
                        verifiedByEmployeeId = 0
                    )
                )
                _auditorData.value = newAuditor
            }
        }
    }

    fun verifySupervisor() {
        _supervisorData.value?.let { currentData ->
            val newVerificationData = VerificationData(
                isVerified = true,
                verifiedEmployeeId = currentData.employeeId,
                verifiedByEmployeeId = currentData.employeeId,
                verifiedByName = currentData.employeeName,
                verifiedDate = Date().toSimpleString()
            )
            _supervisorData.value = currentData.copy(verificationData = newVerificationData)
        }
    }

    fun verifyAuditor() {
        _auditorData.value?.let { currentData ->
            val newVerificationData = VerificationData(
                isVerified = true,
                verifiedEmployeeId = currentData.employeeId,
                verifiedByName = currentData.employeeName,
                verifiedDate = Date().toSimpleString(),
                verifiedByEmployeeId = currentData.employeeId
            )
            _auditorData.value = currentData.copy(verificationData = newVerificationData)
        }
    }

    suspend fun verifyEmployee(
        employeeId: Int,
        enteredPin: String
    ): PinUseCases.EmployeeVerificationResult {
        val supervisorId = _supervisorData.value?.employeeId
        return pinUseCases.validatePinByEmployeeAndSupervisor(employeeId, enteredPin, supervisorId)
    }

    private fun createVerificationData(
        isVerified: Boolean,
        verifiedEmployeeId: Int,
        verifiedDate: Date,
        verifiedByName: String,
        verifiedByEmployeeId: Int
    ) = VerificationData(
        isVerified = isVerified,
        verifiedEmployeeId = if (isVerified) verifiedEmployeeId else 0,
        verifiedDate = if (isVerified) verifiedDate.toSimpleString() else "",
        verifiedByName = if (isVerified) verifiedByName else "",
        verifiedByEmployeeId = if (isVerified) verifiedByEmployeeId else 0,
    )
}

private fun PsiUiData.toUiModel(): PsiViewState {
    return PsiViewState(
        projectName = this.projectName,
        projectNumber = this.projectNumber,
        generalInformationViewState = this.toGeneralInfoViewState(this.projectName),
        reviewViewState = this.toReviewViewState(),
        weatherViewState = this.toWeatherViewState(),
        otherViewState = this.toOtherViewState(),
        employees = this.employees.map { it.toPsiEmployeeUi() },
        tasks = this.tasks.map { it.toPsiTaskUi() }
    )
}

private fun PsiUiData.toGeneralInfoViewState(projectName: String): PsiGeneralInformationViewState {
    return PsiGeneralInformationViewState(
        projectName = projectName,
        formDate = this.psiFormData.formDate.toSimpleString(),
        taskLocation = this.psiFormData.taskLocation,
        musterMeetingPoint = this.psiFormData.musterMeetingPoint
    )
}

private fun PsiUiData.toReviewViewState(): PsiReviewViewState {
    return PsiReviewViewState(
        spillPotential = this.psiFormData.spillPotential,
        hazmatStorage = this.psiFormData.hazmatStorage,
        weather = this.psiFormData.weather,
        sdsReviewForHazmat = this.psiFormData.sdsReviewForHazmat,
        ventilationRequired = this.psiFormData.ventilationRequired,
        heatStress = this.psiFormData.heatStress,
        lightningLevelsTooLow = this.psiFormData.lightningLevelsTooLow,
        housekeeping = this.psiFormData.housekeeping,
        workingTightArea = this.psiFormData.workingTightArea,
        partOfBodyInLineOfFire = this.psiFormData.partOfBodyInLineOfFire,
        workingAboveYourHead = this.psiFormData.workingAboveYourHead,
        pinchPointsIdentified = this.psiFormData.pinchPointsIdentified,
        repetitiveMotion = this.psiFormData.repetitiveMotion,
        barricadesFlaggingAndSignInPlace = this.psiFormData.barricadesFlaggingAndSignInPlace,
        holeCoveringsInPlace = this.psiFormData.holeCoveringsInPlace,
        protectFromFallingItems = this.psiFormData.protectFromFallingItems,
        poweredPlatforms = this.psiFormData.poweredPlatforms,
        othersWorkingOverhead = this.psiFormData.othersWorkingOverhead,
        fallArrestSystems = this.psiFormData.fallArrestSystems,
        ladders = this.psiFormData.ladders,
        welding = this.psiFormData.welding,
        burnSources = this.psiFormData.burnSources,
        compressedGasses = this.psiFormData.compressedGasses,
        workingOnEnergizedEquipment = this.psiFormData.workingOnEnergizedEquipment,
        electricalCordsCondition = this.psiFormData.electricalCordsCondition,
        equipmentInspected = this.psiFormData.equipmentInspected,
        criticalLiftMeetingRequired = this.psiFormData.criticalLiftMeetingRequired,
        energyIsolation = this.psiFormData.energyIsolation,
        airborneParticles = this.psiFormData.airborneParticles,
        openHoles = this.psiFormData.openHoles,
        mobileEquipment = this.psiFormData.mobileEquipment,
        rigging = this.psiFormData.rigging,
        excavation = this.psiFormData.excavation,
        confinedSpace = this.psiFormData.confinedSpace,
        scaffold = this.psiFormData.scaffold,
        slipPotentialIdentified = this.psiFormData.slipPotentialIdentified,
        requiredPermitsInPlace = this.psiFormData.requiredPermitsInPlace,
        excavations = this.psiFormData.excavations,
        walkways = this.psiFormData.walkways,
        other = this.psiFormData.other,
        clearInstructionsProvided = this.psiFormData.clearInstructionsProvided,
        trainedToUseToolAndPerformTask = this.psiFormData.trainedToUseToolAndPerformTask,
        distractionsInWorkArea = this.psiFormData.distractionsInWorkArea,
        workingAlone = this.psiFormData.workingAlone,
        liftTooHeavy = this.psiFormData.liftTooHeavy,
        externalNoiseLevel = this.psiFormData.externalNoiseLevel,
        physicalLimitations = this.psiFormData.physicalLimitations,
        firstAidRequirements = this.psiFormData.firstAidRequirements,
        goggles = this.psiFormData.goggles,
        faceShield = this.psiFormData.faceShield,
        gloves = this.psiFormData.gloves,
        coverall = this.psiFormData.coverall,
        hearingProtection = this.psiFormData.hearingProtection,
        respirator = this.psiFormData.respirator,
        harness = this.psiFormData.harness,
        respiratorName = this.psiFormData.respiratorName,
        reflectiveVest = this.psiFormData.reflectiveVest,
        footwear = this.psiFormData.footwear,
        safetyGlasses = this.psiFormData.safetyGlasses,
        weldingHood = this.psiFormData.weldingHood,
        tyvexSuit = this.psiFormData.tyvexSuit,
        retractableLanyard = this.psiFormData.retractableLanyard,
        hardHat = this.psiFormData.hardHat
    )
}

private fun PsiUiData.toWeatherViewState(): PsiWeatherViewState {
    return PsiWeatherViewState(
        reviewedWeather = this.psiFormData.reviewedWeather,
        reviewedRoad = this.psiFormData.reviewedRoad,
        reviewedHeatIndex = this.psiFormData.reviewedHeatIndex
    )
}

private fun PsiUiData.toOtherViewState(): PsiOtherViewState {
    return PsiOtherViewState(
        taskDescription = psiFormData.taskDescription,
        hazardIdentification = psiFormData.hazardIdentification,
        hazardControls = psiFormData.hazardControls,
        allSectionsImplemented = psiFormData.allSectionsImplemented,
        workersNamesLegible = psiFormData.workersNamesLegible,
        reviewedByForeman = psiFormData.reviewedByForeman,
        musterPointIdentified = psiFormData.musterPointIdentified,
        toolsAndEquipmentInspected = psiFormData.toolsAndEquipmentInspected,
        psiAtTaskLocation = psiFormData.psiAtTaskLocation,
        comments = psiFormData.comments
    )
}

private fun PsiTaskData.toPsiTaskUi(): TaskStepsUiEntity {
    return TaskStepsUiEntity(
        psiTaskId = this.psiTaskId,
        task = this.task,
        hazard = this.hazard,
        control = this.control,
        risk = this.risk
    )
}

private fun PsiEmployeeUiData.toPsiEmployeeUi(): PsiEmployeeRow {
    return PsiEmployeeRow(
        psiEmployeeId = this.psiEmployeeId,
        employeeId = this.employeeId,
        employeeFullName = this.employeeFullName,
        employeePin = employeePin,
        pinUserName = this.pinUserName,
        pinDate = this.pinDate.toDateString(),
        pinImageFileLocation = pinImageFileLocation,
        pinEmployeeId = this.pinEmployeeId
    )
}
