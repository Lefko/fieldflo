package com.fieldflo.screens.formPsi.weather

import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import androidx.fragment.app.Fragment
import com.fieldflo.R
import kotlinx.android.synthetic.main.fragment_psi_weather.*

class PsiWeatherFragment : Fragment(R.layout.fragment_psi_weather) {

    companion object {
        fun newInstance() = PsiWeatherFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragment_pre_job_safety_weather_reviewed_weather_yes
            .setToggleBehavior(fragment_pre_job_safety_weather_reviewed_weather_no)
        fragment_pre_job_safety_weather_reviewed_weather_no
            .setToggleBehavior(fragment_pre_job_safety_weather_reviewed_weather_yes)

        fragment_pre_job_safety_weather_reviewed_road_yes
            .setToggleBehavior(fragment_pre_job_safety_weather_reviewed_road_no)
        fragment_pre_job_safety_weather_reviewed_road_no
            .setToggleBehavior(fragment_pre_job_safety_weather_reviewed_road_yes)

        fragment_pre_job_safety_weather_reviewed_heat_index_yes
            .setToggleBehavior(fragment_pre_job_safety_weather_reviewed_heat_index_no)
        fragment_pre_job_safety_weather_reviewed_heat_index_no
            .setToggleBehavior(fragment_pre_job_safety_weather_reviewed_heat_index_yes)
    }

    private fun CheckBox.setToggleBehavior(other: CheckBox) {
        this.setOnClickListener {
            this.isChecked = true
            other.isChecked = false
        }
    }

    private fun isReviewed(yes: CheckBox, no: CheckBox): Boolean? {
        return when {
            yes.isChecked && !no.isChecked -> true
            !yes.isChecked && no.isChecked -> false
            else -> null
        }
    }

    fun getViewState(): PsiWeatherViewState {

        return PsiWeatherViewState(
            reviewedWeather = isReviewed(
                fragment_pre_job_safety_weather_reviewed_weather_yes,
                fragment_pre_job_safety_weather_reviewed_weather_no
            ),
            reviewedRoad = isReviewed(
                fragment_pre_job_safety_weather_reviewed_road_yes,
                fragment_pre_job_safety_weather_reviewed_road_no
            ),
            reviewedHeatIndex = isReviewed(
                fragment_pre_job_safety_weather_reviewed_heat_index_yes,
                fragment_pre_job_safety_weather_reviewed_heat_index_no
            )
        )
    }

    fun setViewState(viewState: PsiWeatherViewState) {
        render(viewState)
    }

    private fun render(viewState: PsiWeatherViewState) {
        viewState.reviewedWeather?.let {
            fragment_pre_job_safety_weather_reviewed_weather_yes.isChecked = it
            fragment_pre_job_safety_weather_reviewed_weather_no.isChecked = !it
        }

        viewState.reviewedRoad?.let {
            fragment_pre_job_safety_weather_reviewed_road_yes.isChecked = it
            fragment_pre_job_safety_weather_reviewed_road_no.isChecked = !it
        }

        viewState.reviewedHeatIndex?.let {
            fragment_pre_job_safety_weather_reviewed_heat_index_yes.isChecked = it
            fragment_pre_job_safety_weather_reviewed_heat_index_no.isChecked = !it
        }

    }
}