package com.fieldflo.screens.formPsi.signatures.pinOutImageDialog

import android.app.Dialog
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import com.fieldflo.R
import java.io.File


class PinOutImageDialog : AppCompatDialogFragment() {

    private val imageLocation: File by lazy {
        File(arguments!!.getString(PIN_OUT_IMAGE_LOCATION)!!)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val imageView = LayoutInflater.from(requireContext())
            .inflate(R.layout.dialog_psi_pin_out_image, null, false) as ImageView

        val bitmap = BitmapFactory.decodeFile(imageLocation.absolutePath)
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
        val newWidth = displayMetrics.widthPixels
        val scaleFactor = newWidth.toFloat()/ bitmap.width.toFloat()
        val newHeight = (bitmap.height * scaleFactor).toInt()

        val resizedBitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true)

        imageView.setImageBitmap(resizedBitmap)

        val dialog = AlertDialog.Builder(requireContext())
            .setView(imageView)
            .show()

        dialog.window?.setBackgroundDrawable(
            ColorDrawable(ContextCompat.getColor(requireContext(), android.R.color.transparent))
        )

        return dialog
    }

    companion object {
        private const val PIN_OUT_IMAGE_LOCATION = "pin_out_image_location"
        const val TAG = "PinOutImageDialog"

        fun newInstance(imageLocation: File): PinOutImageDialog {
            return PinOutImageDialog().apply {
                arguments = bundleOf(
                    PIN_OUT_IMAGE_LOCATION to imageLocation.absolutePath
                )
            }
        }
    }
}