package com.fieldflo.screens.formPsi.signatures

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.fieldflo.R
import com.fieldflo.common.safeClickListener


class PsiSignaturesAdapter(private val rowCallback: WorkerRowActionHandler) :
    RecyclerView.Adapter<PsiSignaturesAdapter.PsiSignatureViewHolder>() {

    private val employees = mutableListOf<PsiEmployeeRow>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PsiSignatureViewHolder =
        PsiSignatureViewHolder(
            parent,
            R.layout.row_pre_job_safety_signatures,
            rowCallback,
            ::deleteAt
        ) { employees[it] }

    override fun onBindViewHolder(holder: PsiSignatureViewHolder, position: Int) {
        holder.bind(employees[position])
    }

    fun submitList(employees: List<PsiEmployeeRow>) {
        this.employees.clear()
        this.employees.addAll(employees)
        notifyDataSetChanged()
    }

    fun addEmployees(employees: List<PsiEmployeeRow>) {
        val previousSize = this.employees.size
        this.employees.addAll(employees)
        notifyItemRangeInserted(previousSize, employees.size)
    }

    fun getSignatures(): MutableList<PsiEmployeeRow> {
        return employees
    }

    override fun getItemCount(): Int {
        return employees.size
    }

    private fun deleteAt(position: Int) {
        employees.removeAt(position)
        notifyItemRemoved(position)
    }

    inner class PsiSignatureViewHolder(
        parent: ViewGroup,
        @LayoutRes itemViewLayoutId: Int,
        rowCallback: WorkerRowActionHandler,
        deleteEmployee: (position: Int) -> Unit,
        employeeProvider: (Int) -> PsiEmployeeRow
    ) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            itemViewLayoutId,
            parent,
            false
        )
    ) {

        private val deleteIV =
            itemView.findViewById<ImageView>(R.id.row_recyclerview_signatures_worker_deleteIV)
        private val enterPinTV =
            itemView.findViewById<TextView>(R.id.row_pre_job_safety_signatures_enter_pin_button)
        private val employeeName =
            itemView.findViewById<TextView>(R.id.row_pre_job_safety_signatures_employee_name)
        private val pinOutImageButton =
            itemView.findViewById<ImageView>(R.id.row_recyclerview_signatures_worker_pin_out_image_button)

        init {
            deleteIV.safeClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    deleteEmployee(position)
                }
            }

            enterPinTV.safeClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val employee = employeeProvider.invoke(position)

                    if (!employee.verified) {
                        rowCallback.onWorkerRowAction(employee, WorkerRowAction.ACTION_ENTER_PIN)
                    }
                }
            }

            pinOutImageButton.safeClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val employee = employeeProvider.invoke(position)

                    if (employee.hasPinOutImage) {
                        rowCallback.onWorkerRowAction(
                            employee,
                            WorkerRowAction.ACTION_VIEW_PIN_OUT_IMAGE
                        )
                    }
                }
            }
        }

        fun bind(employee: PsiEmployeeRow) {
            employeeName.text = employee.employeeFullName
            enterPinTV.text = if (employee.verified) {
                itemView.resources.getString(
                    R.string.activity_pre_job_safety_employee_verified,
                    employee.pinUserName,
                    employee.pinDate
                )
            } else {
                itemView.resources.getString(
                    R.string.activity_pre_job_safety_enter_pin
                )
            }
            pinOutImageButton.isVisible = employee.hasPinOutImage
        }
    }

    interface WorkerRowActionHandler {
        fun onWorkerRowAction(
            employee: PsiEmployeeRow,
            action: WorkerRowAction
        )
    }

    enum class WorkerRowAction {
        ACTION_ENTER_PIN, ACTION_VIEW_PIN_OUT_IMAGE
    }
}