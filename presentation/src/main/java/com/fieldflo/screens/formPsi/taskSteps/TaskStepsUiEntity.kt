package com.fieldflo.screens.formPsi.taskSteps

data class TaskStepsUiEntity(
    val psiTaskId: Int,
    val task: String = "",
    val hazard: String = "",
    val control: String = "",
    val risk: Int = 0
)
