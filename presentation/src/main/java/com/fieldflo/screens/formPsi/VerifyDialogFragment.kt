package com.fieldflo.screens.formPsi

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import com.fieldflo.R
import com.fieldflo.screens.formPsi.generalInformation.EmployeeAndVerificationData

class VerifyDialogFragment : DialogFragment() {

    private lateinit var callback: VerifyDialogCallback

    private val employeePin: String by lazy { arguments!!.getString(EMPLOYEE_PIN_KEY)!! }
    private val employeeId: Int by lazy { arguments!!.getInt(EMPLOYEE_ID_KEY) }

    private val employeeType: EmployeePinType by lazy {
        arguments!!.getSerializable(EMPLOYEE_TYPE) as EmployeePinType
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_verify_pin, null)
        val pinInputField = view.findViewById<EditText>(R.id.dialog_verify_pin_inputET)

        return AlertDialog.Builder(activity as Context)
            .setView(view)
            .setPositiveButton(android.R.string.ok, { _, _ ->
                if (pinInputField.text.isNotEmpty()) {
                        callback.onPinSubmit(
                            employeeId,
                            employeePin,
                            pinInputField.text.toString(),
                            employeeType
                        )

                    dismiss()
                }
            })
            .setNegativeButton(android.R.string.cancel, { _, _ ->
                dismiss()
            })
            .show()
    }

    private fun onAttachToContext(context: Context) {
        if (context is VerifyDialogCallback) {
            this.callback = context
        } else {
            throw IllegalArgumentException(
                "${context.javaClass.simpleName} " +
                        "must be an instance of VerifyDialogCallback"
            )
        }
    }

    companion object {

        const val TAG = "VerifySingleEmployeeDialogFragment"
        private const val EMPLOYEE_PIN_KEY = "employee_pin_key"
        private const val EMPLOYEE_TYPE = "employee_type"
        private const val EMPLOYEE_ID_KEY = "employee_id_key"

        fun newInstance(employee: EmployeeAndVerificationData, employeeType: EmployeePinType) =
            VerifyDialogFragment().apply {
                arguments = bundleOf(
                    EMPLOYEE_PIN_KEY to employee.employeePinNumber,
                    EMPLOYEE_TYPE to employeeType,
                    EMPLOYEE_ID_KEY to employee.employeeId
                )
            }
    }

    interface VerifyDialogCallback {
        fun onPinSubmit(employeeId: Int, employeePin: String, enteredPin: String, employeeType: EmployeePinType)
    }

    enum class EmployeePinType { EMPLOYEE_SUPERVISOR, EMPLOYEE_AUDITOR }
}