package com.fieldflo.screens.formPsi.generalInformation

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.fieldflo.R
import com.fieldflo.common.dateTimePickers.DatePickerDialog
import com.fieldflo.common.safeClickListener
import com.fieldflo.common.toDate
import com.fieldflo.common.toSimpleString
import com.fieldflo.screens.formPsi.PsiActivity.Companion.AUDITOR_EMPLOYEE_IDENTIFIER
import com.fieldflo.screens.formPsi.VerifyDialogFragment
import com.fieldflo.screens.selectEmployee.SelectEmployeeDialog
import com.fieldflo.screens.selectSupervisor.SelectSupervisorDialog
import kotlinx.android.synthetic.main.fragment_psi_general_info.*
import java.util.*

class PsiGeneralInformationFragment : Fragment(R.layout.fragment_psi_general_info) {

    companion object {
        fun newInstance() = PsiGeneralInformationFragment()
    }

    private val verifiedString by lazy { resources.getString(R.string.activity_pre_job_safety_verified) }
    private val enterPinString by lazy { resources.getString(R.string.activity_pre_job_safety_enter_pin) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragment_pre_job_safety_general_info_date_field_background.setOnClickListener(
            createShowDateClickListener(
                fragment_pre_job_safety_general_info_date_field_title
            )
        )

        fragment_pre_job_safety_general_info_supervisor_enter_pinTV.safeClickListener {
            (fragment_pre_job_safety_general_info_supervisor.tag as? EmployeeAndVerificationData)?.let {
                VerifyDialogFragment.newInstance(
                    it, VerifyDialogFragment.EmployeePinType.EMPLOYEE_SUPERVISOR
                ).show(childFragmentManager, VerifyDialogFragment.TAG)
            }
        }

        fragment_pre_job_safety_general_info_auditor_enter_pinTV.safeClickListener {
            (fragment_pre_job_safety_general_info_auditor.tag as? EmployeeAndVerificationData)?.let {
                VerifyDialogFragment.newInstance(
                    it, VerifyDialogFragment.EmployeePinType.EMPLOYEE_AUDITOR
                ).show(childFragmentManager, VerifyDialogFragment.TAG)
            }
        }

        fragment_pre_job_safety_general_info_supervisor.safeClickListener {
            SelectSupervisorDialog.newInstance()
                .show(childFragmentManager, SelectEmployeeDialog.TAG)
        }

        fragment_pre_job_safety_general_info_auditor.safeClickListener {
            SelectEmployeeDialog.newInstance(AUDITOR_EMPLOYEE_IDENTIFIER)
                .show(childFragmentManager, SelectEmployeeDialog.TAG)
        }
    }

    fun setViewState(viewState: PsiGeneralInformationViewState) {
        fragment_pre_job_safety_general_info_project_name.text = viewState.projectName
        fragment_pre_job_safety_general_info_date_field_title.text = viewState.formDate
        fragment_pre_job_safety_general_info_task_location_edit.setText(viewState.taskLocation)
        fragment_pre_job_safety_general_info_master_meeting_point_edit.setText(viewState.musterMeetingPoint)
    }

    fun getGeneralInformationViewState(): PsiGeneralInformationViewState {
        return PsiGeneralInformationViewState(
            projectName = fragment_pre_job_safety_general_info_project_name.text.toString(),
            formDate = fragment_pre_job_safety_general_info_date_field_title.text.toString(),
            taskLocation = fragment_pre_job_safety_general_info_task_location_edit.text.toString(),
            musterMeetingPoint = fragment_pre_job_safety_general_info_master_meeting_point_edit.text.toString()
        )
    }

    fun renderAuditor(auditorData: EmployeeAndVerificationData) {
        fragment_pre_job_safety_general_info_auditor.tag = auditorData
        fragment_pre_job_safety_general_info_auditor.text = auditorData.employeeName
        fragment_pre_job_safety_general_info_auditor_enter_pinTV.isVisible = auditorData.showVerify
        fragment_pre_job_safety_general_info_auditor_required_asterisk.isVisible =
            auditorData.showAsterisk
        if (auditorData.verified) {
            fragment_pre_job_safety_general_info_auditor_enter_pinTV.text =
                "${auditorData.verificationData.verifiedByName} $verifiedString\n${auditorData.verificationData.verifiedDate}".trimMargin()
            fragment_pre_job_safety_general_info_auditor_enter_pinTV.isEnabled = false
        } else {
            fragment_pre_job_safety_general_info_auditor_enter_pinTV.text = enterPinString
            fragment_pre_job_safety_general_info_auditor_enter_pinTV.isEnabled = true
        }

    }

    fun renderSupervisor(supervisorData: EmployeeAndVerificationData) {
        fragment_pre_job_safety_general_info_supervisor.tag = supervisorData
        fragment_pre_job_safety_general_info_supervisor.text = supervisorData.employeeName
        fragment_pre_job_safety_general_info_supervisor_enter_pinTV.isVisible = supervisorData.showVerify
        if (supervisorData.verified) {
            fragment_pre_job_safety_general_info_supervisor_enter_pinTV.text =
                "${supervisorData.verificationData.verifiedByName} $verifiedString\n${supervisorData.verificationData.verifiedDate}".trimMargin()
            fragment_pre_job_safety_general_info_supervisor_enter_pinTV.isEnabled = false
        } else {
            fragment_pre_job_safety_general_info_supervisor_enter_pinTV.text = enterPinString
            fragment_pre_job_safety_general_info_supervisor_enter_pinTV.isEnabled = true
        }
    }

    private fun createShowDateClickListener(dateTitleView: TextView): View.OnClickListener {
        return View.OnClickListener {
            val date = extractDateFromTextForPicker(dateTitleView)
            val picker =
                DatePickerDialog.newInstance(date, object : DatePickerDialog.DateSelectedListener {
                    override fun onDateSelected(newDate: Date) {
                        dateTitleView.text = newDate.toSimpleString()
                    }
                })
            picker.show(childFragmentManager, DatePickerDialog.TAG)
        }
    }

    private fun extractDateFromTextForPicker(dateTV: TextView): Date {
        val pleaseSelect = getString(R.string.activity_daily_form_date_title)
        val dateText = dateTV.text.toString()
        return if (dateText == "" || dateText == pleaseSelect) {
            Date()
        } else {
            dateTV.text.toString().toDate()
        }
    }
}