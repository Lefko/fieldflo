package com.fieldflo.screens.formPsi.taskSteps.addTask

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.ListView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenStarted
import com.fieldflo.R
import com.fieldflo.di.injector
import com.fieldflo.screens.formPsi.taskSteps.TaskStepsUiEntity
import kotlinx.coroutines.launch

class AddTasksDialogFragment : DialogFragment() {

    private val presenter by viewModels<AddTasksPresenter> { injector.psiAddTasksViewModelFactory() }

    private val tasksAdapter by lazy { TasksAdapter(activity!!) }

    private lateinit var callback: AddTasksCallback

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = AlertDialog.Builder(activity as Context)
            .setTitle(R.string.activity_pre_job_safety_add_tasks)
            .setAdapter(tasksAdapter, null)
            .setPositiveButton(android.R.string.ok, { _, _ ->
                val listView = (dialog as AlertDialog).listView

                val newTasks = (0 until listView.adapter.count)
                    .filter { listView.checkedItemPositions[it] }
                    .mapNotNull { tasksAdapter.getItem(it) }
                    .map {
                        TaskStepsUiEntity(
                            psiTaskId = 0,
                            task = it.task,
                            hazard = it.hazard,
                            control = it.control,
                            risk = it.risk
                        )
                    }
                if (newTasks.isNotEmpty()) {
                    callback.tasksToAdd(newTasks)
                }
                dismiss()
            })
            .setNegativeButton(android.R.string.cancel, { _, _ ->
                dismiss()
            })
            .show()

        dialog.listView.itemsCanFocus = false
        dialog.listView.choiceMode = ListView.CHOICE_MODE_MULTIPLE

        return dialog
    }

    override fun onStart() {
        super.onStart()
        present()
    }

    private fun present() {
        lifecycleScope.launch {
            whenStarted {
                render(presenter.getAllTasks())
            }
        }
    }

    private fun render(tasks: List<AddTasksViewState.TaskUiRow>) {
        tasksAdapter.clear()
        tasksAdapter.addAll(tasks)
        tasksAdapter.notifyDataSetChanged()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    private fun onAttachToContext(context: Context) {
        this.callback = parentFragment as? AddTasksCallback ?: targetFragment as? AddTasksCallback
                ?: context as? AddTasksCallback
                ?: throw IllegalArgumentException(
            "${context.javaClass.simpleName} must be an instance of AddTasksCallback"
        )
    }

    companion object {

        const val TAG = "AddTasksDialogFragment"

        fun newInstance() = AddTasksDialogFragment()
    }


    interface AddTasksCallback {
        fun tasksToAdd(tasks: List<TaskStepsUiEntity>)
    }
}