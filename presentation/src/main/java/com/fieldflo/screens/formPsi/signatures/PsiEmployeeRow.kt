package com.fieldflo.screens.formPsi.signatures

import java.io.File

data class PsiEmployeeRow(
    val psiEmployeeId: Int = 0,
    val employeeId: Int = 0,
    val employeeFullName: String = "",
    val employeePin: String,
    val pinUserName: String = "",
    val pinEmployeeId: Int,
    val pinDate: String = "",
    val pinImageFileLocation: File
) {
    val verified
        get() = pinUserName.isNotEmpty()

    val hasPinOutImage
        get() = pinImageFileLocation.exists()
}