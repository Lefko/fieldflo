package com.fieldflo.screens.formPsi.signatures.addEmployeesToSignatures

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.Employee
import com.fieldflo.usecases.EmployeeUseCases
import com.fieldflo.usecases.PsiUseCases
import javax.inject.Inject

class AddEmployeesToSignaturesPresenter @Inject constructor(
    private val employeesUseCases: EmployeeUseCases,
    private val psiUseCases: PsiUseCases
) : ViewModel() {

    suspend fun getEmployees(): List<AddEmployeesToPsiEmployee> {
        return employeesUseCases.getAllEmployees().map { it.toUiModel() }
    }

    suspend fun getPsiPhotoPinFileLocation(projectId: Int, employeeId: Int, internalFormId: Int) =
        psiUseCases.getPsiPhotoPinFileLocation(projectId, employeeId, internalFormId)
}

private fun Employee.toUiModel() = AddEmployeesToPsiEmployee(
    employeeId = this.employeeId,
    employeeName = this.employeeFullName,
    employeePin = this.employeePinNumber
)

data class AddEmployeesToPsiEmployee(
    val employeeId: Int,
    val employeeName: String,
    val employeePin: String
)
