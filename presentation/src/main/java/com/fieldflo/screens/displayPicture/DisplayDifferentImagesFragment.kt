package com.fieldflo.screens.displayPicture

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.davemorrissey.labs.subscaleview.ImageSource
import com.fieldflo.R
import kotlinx.android.synthetic.main.view_display_picture_image.*

class DisplayDifferentImagesFragment : Fragment() {
    companion object {

        const val TAG = "DisplayDifferentImagesFragment"
        private const val FILE_LOCATION_KEY = "file_location_key"

        fun newInstance(fileLocation: String, activity: FragmentActivity, containerId: Int) =
            DisplayDifferentImagesFragment().apply {
                arguments = Bundle().apply {
                    putString(FILE_LOCATION_KEY, fileLocation)
                }
                val transaction = activity.supportFragmentManager.beginTransaction()
                transaction.add(containerId, this)
                transaction.commit()
            }
    }

    private val fileLocation: String by lazy {
        arguments!!.getString(FILE_LOCATION_KEY)!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.view_display_picture_image, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity_display_picture_content_image.setImage(ImageSource.uri(fileLocation))
    }
}