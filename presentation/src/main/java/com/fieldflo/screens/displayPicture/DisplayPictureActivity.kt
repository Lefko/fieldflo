package com.fieldflo.screens.displayPicture

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector
import com.fieldflo.usecases.ProjectStatus
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter

class DisplayPictureActivity : AppCompatActivity() {
    companion object {

        fun start(sender: Activity, extension: String, fullpath: String) {
            val intent = Intent(sender, DisplayPictureActivity::class.java).apply {
                putExtra(FILE_EXTENSION, extension)
                putExtra(FILE_LOCATION, fullpath)
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            sender.startActivity(intent)
        }

        fun start(sender: Activity, extension: String, fullpath: String, projectId: Int) {
            val intent = Intent(sender, DisplayPictureActivity::class.java).apply {
                putExtra(FILE_EXTENSION, extension)
                putExtra(FILE_LOCATION, fullpath)
                putExtra(PROJECT_ID, projectId)
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            sender.startActivity(intent)
        }

        private const val FILE_EXTENSION = "file_extension"
        private const val FILE_LOCATION = "file_location"
        private const val PROJECT_ID = "project_id"
    }

    private val fileExtension by lazy {
        intent!!.extras!!.getString(FILE_EXTENSION)!!
    }

    private val fileLocation by lazy {
        intent!!.extras!!.getString(FILE_LOCATION)!!
    }

    private val projectId by lazy { intent!!.extras!!.getInt(PROJECT_ID, 0) }
    private val presenter by viewModels<DisplayPicturePresenter> { injector.displayPicturePresenterFactory() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display_picture)

        lifecycleScope.launchWhenCreated {
            presenter.getProjectStatusFlow(projectId)
                .filter { it == ProjectStatus.DELETED }
                .collect { finish() }
        }

        when (fileExtension) {
            "pdf" -> DisplayPDFFragment.newInstance(
                fileLocation,
                this,
                R.id.display_image_container
            )
            else -> DisplayDifferentImagesFragment.newInstance(
                fileLocation,
                this,
                R.id.display_image_container
            )
        }
    }
}