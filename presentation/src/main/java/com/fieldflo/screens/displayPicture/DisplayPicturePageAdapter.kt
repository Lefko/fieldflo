package com.fieldflo.screens.displayPicture

import android.graphics.Bitmap
import android.graphics.pdf.PdfRenderer
import android.os.ParcelFileDescriptor
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.davemorrissey.labs.subscaleview.ImageSource
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import com.fieldflo.R
import kotlinx.android.synthetic.main.view_display_picture_image.view.*

class DisplayPicturePageAdapter(private val inflater: LayoutInflater, pfd: ParcelFileDescriptor)
    : RecyclerView.Adapter<DisplayPicturePageAdapter.DisplayPicturePageHolder>() {

    private val renderer: PdfRenderer = PdfRenderer(pfd)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DisplayPicturePageHolder {
        return DisplayPicturePageHolder(inflater.inflate(R.layout.view_display_picture_image, parent, false))
    }

    override fun onBindViewHolder(holder: DisplayPicturePageHolder, position: Int) {
        val page = renderer.openPage(position)

        holder.setPage(page)
        page.close()
    }

    class DisplayPicturePageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val iv: SubsamplingScaleImageView = itemView.activity_display_picture_content_image
        private var bitmap: Bitmap? = null

        fun setPage(page: PdfRenderer.Page) {
            if (bitmap == null) {
                val height = 2000
                val width = height * page.width / page.height

                bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            }

            bitmap!!.eraseColor(-0x1)
            page.render(bitmap!!, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY)
            iv.resetScaleAndCenter()
            iv.setImage(ImageSource.cachedBitmap(bitmap!!))
        }
    }

    fun close() {
        renderer.close()
    }

    override fun getItemCount()= renderer.pageCount
}