package com.fieldflo.screens.displayPicture

import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.ProjectStatus
import com.fieldflo.usecases.ProjectsUseCases
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class DisplayPicturePresenter @Inject constructor(
    private val projectsUseCases: ProjectsUseCases
) : ViewModel() {
    fun getProjectStatusFlow(projectId: Int): Flow<ProjectStatus> {
        return projectsUseCases.getProjectStatusFlow(projectId)
    }
}
