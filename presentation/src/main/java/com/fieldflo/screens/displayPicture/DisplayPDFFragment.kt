package com.fieldflo.screens.displayPicture

import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.fieldflo.R
import kotlinx.android.synthetic.main.view_display_picture_image_pdf_recycler.*
import timber.log.Timber
import java.io.File

class DisplayPDFFragment : Fragment() {
    companion object {

        const val TAG = "DisplayPDFFragment"
        private const val FILE_LOCATION_KEY = "file_location_key"

        fun newInstance(fileLocation: String, activity: FragmentActivity, containerId: Int) =
            DisplayPDFFragment().apply {
                arguments = Bundle().apply {
                    putString(FILE_LOCATION_KEY, fileLocation)
                }

                val transaction = activity.supportFragmentManager.beginTransaction()
                transaction.add(containerId, this)
                transaction.commit()
            }
    }

    private val snapperCarr = PagerSnapHelper()
    private var adapter: DisplayPicturePageAdapter? = null

    private val fileLocation: String by lazy {
        arguments!!.getString(FILE_LOCATION_KEY)!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.view_display_picture_image_pdf_recycler, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val context = view.context
        view_display_picture_image_pdf_pager.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        snapperCarr.attachToRecyclerView(view_display_picture_image_pdf_pager)

        try {
            val file = File(fileLocation)
            if (file.exists()) {

                val parcel = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY)
                adapter = DisplayPicturePageAdapter(layoutInflater, parcel)
                view_display_picture_image_pdf_pager.adapter = adapter
            } else {
                Toast.makeText(context, "Image not found", Toast.LENGTH_LONG).show()
                activity?.finish()
            }
        } catch (e: java.io.IOException) {
            Timber.e(e, "open pdf")
            Toast.makeText(context, "Error loading image", Toast.LENGTH_LONG).show()
            activity?.finish()
        }
    }

    override fun onDestroyView() {
        adapter?.close()
        super.onDestroyView()
    }
}