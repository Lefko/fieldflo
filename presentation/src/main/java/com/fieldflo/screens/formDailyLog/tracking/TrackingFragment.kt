package com.fieldflo.screens.formDailyLog.tracking

import androidx.fragment.app.Fragment
import com.fieldflo.R
import kotlinx.android.synthetic.main.fragment_daily_log_form_tracking.view.*

class TrackingFragment : Fragment(R.layout.fragment_daily_log_form_tracking) {

    companion object {
        fun newInstance() = TrackingFragment()
    }

    fun setViewState(viewState: TrackingViewState) {
        render(viewState)
    }

    fun getViewState(): TrackingViewState {
        val view = requireView()
        return TrackingViewState(
            byBagCount = view.activity_daily_log_form_by_bag_count_value.text.toString(),
            projectToDateBagCount = view.activity_daily_log_form_bag_date_value.text.toString(),
            containerNumber = view.activity_daily_log_form_container_number_value.text.toString(),
            bagInContainerByDate = view.activity_daily_log_form_container_date_value.text.toString(),
            unusualConditionsOrProblemsAndActionTaken = view.activity_daily_log_form_conditions_and_problems_value.text.toString()
        )
    }

    private fun render(viewState: TrackingViewState) {
        val view = requireView()
        view.activity_daily_log_form_by_bag_count_value.setText(viewState.byBagCount)
        view.activity_daily_log_form_bag_date_value.setText(viewState.projectToDateBagCount)
        view.activity_daily_log_form_container_number_value.setText(viewState.containerNumber)
        view.activity_daily_log_form_container_date_value.setText(viewState.bagInContainerByDate)
        view.activity_daily_log_form_conditions_and_problems_value.setText(
            viewState.unusualConditionsOrProblemsAndActionTaken
        )
    }

}