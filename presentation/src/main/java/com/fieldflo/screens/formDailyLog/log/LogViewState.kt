package com.fieldflo.screens.formDailyLog.log


data class LogViewState(
    val dailyLog: String = "",
    val visitors: String = "",
    val status: String = "",
    val inspectionsAndTests: String = ""
)
