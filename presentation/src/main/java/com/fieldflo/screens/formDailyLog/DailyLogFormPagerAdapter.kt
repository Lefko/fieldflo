package com.fieldflo.screens.formDailyLog

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.fieldflo.screens.formDailyLog.generalInformation.GeneralInformationFragment
import com.fieldflo.screens.formDailyLog.log.LogFragment
import com.fieldflo.screens.formDailyLog.tracking.TrackingFragment

@Suppress("DEPRECATION")
class DailyLogFormPagerAdapter(fm: FragmentManager, private val pageTitles: Array<String>): FragmentPagerAdapter(fm){

    val generalInfo: GeneralInformationFragment = GeneralInformationFragment.newInstance()
    val log: LogFragment = LogFragment.newInstance()
    val tracking: TrackingFragment = TrackingFragment.newInstance()

    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> generalInfo
            1 -> log
            2 -> tracking
            else -> throw IllegalArgumentException("There is no tab for position: $position")
        }
    }

    override fun getCount() = pageTitles.size

    override fun getPageTitle(position: Int): CharSequence? {
        return pageTitles[position]
    }
}