package com.fieldflo.screens.formDailyLog

import com.fieldflo.screens.formDailyLog.generalInformation.GeneralInformationViewState
import com.fieldflo.screens.formDailyLog.log.LogViewState
import com.fieldflo.screens.formDailyLog.tracking.TrackingViewState

data class DailyLogFormViewState(
    val projectName: String,
    val generalInformation: GeneralInformationViewState,
    val log: LogViewState,
    val tracking: TrackingViewState
)

data class SupervisorViewData(
    val supervisorVerify: Boolean = false,
    val supervisorId: Int,
    val supervisorName: String = ""
) {
    val verifyStatus
        get() = when {
            supervisorName.isEmpty() -> VerifyStatus.NONE
            !supervisorVerify -> VerifyStatus.NOT_VERIFIED
            else -> VerifyStatus.VERIFIED
        }

    enum class VerifyStatus {
        NONE, NOT_VERIFIED, VERIFIED
    }
}