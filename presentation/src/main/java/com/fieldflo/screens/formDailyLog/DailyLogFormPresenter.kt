package com.fieldflo.screens.formDailyLog

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fieldflo.AppScopeHolder
import com.fieldflo.data.persistence.db.entities.DailyLogFormData
import com.fieldflo.screens.formDailyLog.generalInformation.GeneralInformationViewState
import com.fieldflo.screens.formDailyLog.log.LogViewState
import com.fieldflo.screens.formDailyLog.tracking.TrackingViewState
import com.fieldflo.screens.selectSupervisor.SupervisorEmployeeUiRow
import com.fieldflo.usecases.DailyLogUseCases
import com.fieldflo.usecases.PinUseCases
import com.fieldflo.usecases.ProjectStatus
import com.fieldflo.usecases.ProjectsUseCases
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class DailyLogFormPresenter @Inject constructor(
    private val dailyLogUseCases: DailyLogUseCases,
    private val projectsUseCases: ProjectsUseCases,
    private val appScopeHolder: AppScopeHolder
) : ViewModel() {

    private val _supervisorData = MutableStateFlow<SupervisorViewData?>(null)
    val supervisorData: Flow<SupervisorViewData> = _supervisorData.filterNotNull()

    suspend fun getDailyLogData(internalFormId: Int, projectId: Int): DailyLogFormViewState {
        return withContext(viewModelScope.coroutineContext) {
            val dailyLogData = dailyLogUseCases.getDailyLogData(internalFormId, projectId)
            val projectName = projectsUseCases.getProjectName(projectId)
            val projectNumber = projectsUseCases.getProjectNumber(projectId)

            val supervisorData = SupervisorViewData(
                supervisorVerify = dailyLogData.supervisorVerify,
                supervisorId = dailyLogData.supervisorId,
                supervisorName = dailyLogData.supervisorFullName
            )

            _supervisorData.value = supervisorData

            dailyLogData.toUiModel(projectName, projectNumber)
        }
    }

    fun getProjectStatusFlow(projectId: Int): Flow<ProjectStatus> {
        return projectsUseCases.getProjectStatusFlow(projectId)
    }

    fun validatePin(enteredPin: String, savedPinHash: String): Boolean {
        return PinUseCases.validatePin(enteredPin, savedPinHash)
    }

    fun save(dailyLogFormData: DailyLogUseCases.SaveDailyLogData) {
        appScopeHolder.scope.launch {
            dailyLogUseCases.saveDailyLogData(dailyLogFormData)
        }
    }

    fun supervisorVerified() {
        _supervisorData.value?.let { currentSupervisorViewData ->
            _supervisorData.value = currentSupervisorViewData.copy(supervisorVerify = true)
        }
    }

    fun supervisorChanged(employee: SupervisorEmployeeUiRow) {
        _supervisorData.value?.let { currentSupervisorViewData ->
            _supervisorData.value = currentSupervisorViewData.copy(
                supervisorVerify = false,
                supervisorId = employee.employeeId,
                supervisorName = employee.employeeName

            )
        }
    }
}

private fun DailyLogFormData.toUiModel(
    projectName: String,
    projectNumber: String
): DailyLogFormViewState {
    return DailyLogFormViewState(
        projectName = projectName,
        generalInformation = this.toGeneralInfo(projectName, projectNumber),
        log = this.toLog(),
        tracking = this.toTracking()
    )
}

private fun DailyLogFormData.toGeneralInfo(
    projectName: String,
    projectNumber: String
): GeneralInformationViewState {
    return GeneralInformationViewState(
        projectName = projectName,
        projectNumber = projectNumber,
        shiftStart = this.shiftStart,
        date = this.formDate,
        shiftEnd = this.shiftEnd,
        projectManagerName = this.projectManagerFullName
    )
}

private fun DailyLogFormData.toLog(): LogViewState {
    return LogViewState(
        dailyLog = this.dailyLog,
        visitors = this.visitorsToSite,
        status = this.statusAtQuittingTime,
        inspectionsAndTests = this.inspectionsMadeOrTestsPerformed
    )
}

private fun DailyLogFormData.toTracking(): TrackingViewState {
    return TrackingViewState(
        byBagCount = this.dailyBagCount,
        projectToDateBagCount = this.projectToDateBagCount,
        containerNumber = this.containerNo,
        bagInContainerByDate = this.bagsInContainerToDate,
        unusualConditionsOrProblemsAndActionTaken = this.unusualConditionsOrProblemsAndActionTaken,
    )
}
