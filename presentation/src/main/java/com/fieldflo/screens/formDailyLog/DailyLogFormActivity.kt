package com.fieldflo.screens.formDailyLog

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.onPageChanges
import com.fieldflo.common.safeClickListener
import com.fieldflo.di.injector
import com.fieldflo.screens.drawer.DrawerManager
import com.fieldflo.screens.formDailyLog.generalInformation.GeneralInformationFragment
import com.fieldflo.screens.formDailyLog.log.LogFragment
import com.fieldflo.screens.formDailyLog.tracking.TrackingFragment
import com.fieldflo.screens.formDailyLog.verifySupervisor.VerifyDialogFragment
import com.fieldflo.screens.selectSupervisor.SelectSupervisorDialog
import com.fieldflo.screens.selectSupervisor.SupervisorEmployeeUiRow
import com.fieldflo.screens.verifyErrorDialog.VerifyErrorDialogFragment
import com.fieldflo.usecases.DailyLogUseCases
import com.fieldflo.usecases.ProjectStatus
import kotlinx.android.synthetic.main.activity_daily_log_form.*
import kotlinx.android.synthetic.main.view_activity_daily_log_form.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.withContext

class DailyLogFormActivity : AppCompatActivity(R.layout.activity_daily_log_form),
    VerifyDialogFragment.VerifyDialogCallback,
    SelectSupervisorDialog.SupervisorSelectedCallback {

    private val drawerManager by lazy {
        DrawerManager(
            { drawer_layout }, { this },
            { activity_daily_log_form_content_toolbar }
        )
    }

    private val presenter by viewModels<DailyLogFormPresenter> { injector.dailyLogFormViewModelFactory() }
    private val internalFormId: Int by lazy { intent.getIntExtra(INTERNAL_FORM_ID_KEY, 0) }
    private val projectId: Int by lazy { intent.getIntExtra(PROJECT_ID_KEY, 0) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val adapter = DailyLogFormPagerAdapter(
            supportFragmentManager,
            resources.getStringArray(R.array.activity_daily_log_form_page_titles)
        )

        activity_daily_log_form_pages.adapter = adapter
        activity_daily_log_form_pages.offscreenPageLimit = 2

        activity_daily_log_form_content_backTV.safeClickListener { finish() }

        activity_daily_log_form_content_saveTV.safeClickListener { finish() }

        activity_daily_log_form_content_previousTV.setOnClickListener {
            if (activity_daily_log_form_pages.currentItem > 0) {
                activity_daily_log_form_pages.currentItem--
            }
        }

        activity_daily_log_form_content_nextTV.setOnClickListener {
            if (activity_daily_log_form_pages.currentItem < activity_daily_log_form_pages.childCount) {
                activity_daily_log_form_pages.currentItem++
            }
        }


        activity_daily_log_form_tabs.setupWithViewPager(activity_daily_log_form_pages)

        supportFragmentManager.registerFragmentLifecycleCallbacks(object :
            FragmentManager.FragmentLifecycleCallbacks() {
            override fun onFragmentViewCreated(
                fm: FragmentManager,
                f: Fragment,
                v: View,
                savedInstanceState: Bundle?
            ) {
                super.onFragmentViewCreated(fm, f, v, savedInstanceState)
                present()
                supportFragmentManager.unregisterFragmentLifecycleCallbacks(this)
            }
        }, false)

        activity_daily_log_form_pages.post {
            activity_daily_log_form_pages.onPageChanges { renderNextPreviousEnabled(it) }
        }
    }

    override fun onBackPressed() {
        if (!drawerManager.onBackPress()) {
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        save()
        super.onDestroy()
    }

    private fun save() {
        val data = extractDailyLogFormData()
        presenter.save(data)
    }

    private fun renderNextPreviousEnabled(currentPage: Int) {
        activity_daily_log_form_content_previousTV.isEnabled = currentPage > 0
        activity_daily_log_form_content_nextTV.isEnabled =
            currentPage < activity_daily_log_form_pages.childCount - 1
    }

    private fun extractDailyLogFormData(): DailyLogUseCases.SaveDailyLogData {
        val adapter = activity_daily_log_form_pages.adapter as DailyLogFormPagerAdapter
        val supervisorViewData = adapter.generalInfo.getSupervisorViewData()
        val generalInfoViewState = adapter.generalInfo.getViewState()
        val logFormViewState = adapter.log.getViewState()
        val trackingViewState = adapter.tracking.getViewState()

        return DailyLogUseCases.SaveDailyLogData(
            projectId = projectId,
            internalFormId = internalFormId,
            shiftStart = generalInfoViewState.shiftStart,
            shiftEnd = generalInfoViewState.shiftEnd,
            date = generalInfoViewState.date,
            supervisorVerify = supervisorViewData.supervisorVerify,
            supervisorFullName = supervisorViewData.supervisorName,
            dailyLog = logFormViewState.dailyLog,
            visitorsToSite = logFormViewState.visitors,
            statusAtQuittingTime = logFormViewState.status,
            inspectionsMadeOrTestsPerformed = logFormViewState.inspectionsAndTests,
            dailyBagCount = trackingViewState.byBagCount,
            projectToDateBagCount = trackingViewState.projectToDateBagCount,
            containerNo = trackingViewState.containerNumber,
            bagsInContainerToDate = trackingViewState.bagInContainerByDate,
            unusualConditionsOrProblemsAndActionTaken = trackingViewState.unusualConditionsOrProblemsAndActionTaken,
            supervisorId = supervisorViewData.supervisorId
        )
    }

    private fun present() {
        lifecycleScope.launchWhenCreated {
            presenter.getProjectStatusFlow(projectId)
                .filter { it == ProjectStatus.DELETED }
                .collect { finish() }
        }

        lifecycleScope.launchWhenCreated {
            presenter.supervisorData.collect {
                withContext(Dispatchers.Main) {
                    render(it)
                }
            }
        }
        lifecycleScope.launchWhenCreated {
            val dailyLogData = presenter.getDailyLogData(internalFormId, projectId)
            withContext(Dispatchers.Main) {
                render(dailyLogData)
            }
        }
    }

    private fun render(supervisorViewData: SupervisorViewData) {
        (activity_daily_log_form_pages.adapter as? DailyLogFormPagerAdapter)?.let { adapter ->
            for (i in 0 until adapter.count) {
                val fragment = adapter.getItem(i)
                if (fragment is GeneralInformationFragment) {
                    fragment.setSupervisorData(supervisorViewData)
                }
            }
        }
    }

    private fun render(viewState: DailyLogFormViewState) {
        activity_daily_log_form_content_page_title.text = viewState.projectName
        (activity_daily_log_form_pages.adapter as? DailyLogFormPagerAdapter)?.let { adapter ->
            for (i in 0 until adapter.count) {
                val fragment = adapter.getItem(i)
                when (fragment) {
                    is GeneralInformationFragment -> fragment.setViewState(viewState.generalInformation)
                    is LogFragment -> fragment.setViewState(viewState.log)
                    is TrackingFragment -> fragment.setViewState(viewState.tracking)
                }
            }
        }
    }

    private fun showErrorDialog() {
        VerifyErrorDialogFragment.newInstance()
            .show(supportFragmentManager, VerifyErrorDialogFragment.TAG)
    }

    override fun onPinSubmit(supervisorPin: String, enteredPin: String) {
        if (!presenter.validatePin(enteredPin, supervisorPin)) {
            showErrorDialog()
        } else {
            presenter.supervisorVerified()
        }
    }

    override fun onSupervisorSelected(employee: SupervisorEmployeeUiRow) {
        presenter.supervisorChanged(employee)
    }

    companion object {
        private const val PROJECT_ID_KEY = "project_id"
        private const val INTERNAL_FORM_ID_KEY = "internal_form_id"

        fun start(sender: Activity, projectId: Int, internalFormId: Int) {
            val intent = Intent(sender, DailyLogFormActivity::class.java).apply {
                putExtra(INTERNAL_FORM_ID_KEY, internalFormId)
                putExtra(PROJECT_ID_KEY, projectId)
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            sender.startActivity(intent)
        }
    }
}