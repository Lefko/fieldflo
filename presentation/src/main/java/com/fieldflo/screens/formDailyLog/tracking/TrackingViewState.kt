package com.fieldflo.screens.formDailyLog.tracking

data class TrackingViewState(
    val byBagCount: String = "",
    val projectToDateBagCount: String = "",
    val containerNumber: String = "",
    val bagInContainerByDate: String = "",
    val unusualConditionsOrProblemsAndActionTaken: String = ""
)
