package com.fieldflo.screens.formDailyLog.verifySupervisor

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.gone
import com.fieldflo.common.visible
import com.fieldflo.di.injector

class VerifyDialogFragment : DialogFragment() {

    private lateinit var callback: VerifyDialogCallback

    private val supervisorId: Int by lazy { arguments!!.getInt(SUPERVISOR_ID_KEY) }
    private val presenter by viewModels<VerifySupervisorPresenter> { injector.verifySupervisorViewModelFactory() }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view =
            LayoutInflater.from(activity).inflate(R.layout.dialog_verify_pin_with_progressbar, null)
        val progressBar = view.findViewById<ProgressBar>(R.id.dialog_verify_pin_progress)
        val pinInputField = view.findViewById<EditText>(R.id.dialog_verify_pin_inputET)
        var supervisorPin = ""

        lifecycleScope.launchWhenCreated {
            supervisorPin = presenter.getEmployeePin(supervisorId)
            progressBar.gone()
            pinInputField.visible()
        }

        return AlertDialog.Builder(activity as Context)
            .setView(view)
            .setPositiveButton(android.R.string.ok, { _, _ ->
                if (pinInputField.text.isNotEmpty()) {
                    callback.onPinSubmit(supervisorPin, pinInputField.text.toString())
                    dismiss()
                }
            })
            .setNegativeButton(android.R.string.cancel, { _, _ ->
                dismiss()
            })
            .show()
    }

    private fun onAttachToContext(context: Context) {
        if (context is VerifyDialogCallback) {
            this.callback = context
        } else {
            throw IllegalArgumentException(
                "${context.javaClass.simpleName} " +
                        "must be an instance of VerifyDialogCallback"
            )
        }
    }

    companion object {

        const val TAG = "VerifySingleEmployeeDialogFragment"
        private const val SUPERVISOR_ID_KEY = "employee_id_key"

        fun newInstance(supervisorId: Int) =
            VerifyDialogFragment().apply {
                arguments = bundleOf(SUPERVISOR_ID_KEY to supervisorId)
            }
    }

    interface VerifyDialogCallback {
        fun onPinSubmit(supervisorPin: String, enteredPin: String)
    }
}