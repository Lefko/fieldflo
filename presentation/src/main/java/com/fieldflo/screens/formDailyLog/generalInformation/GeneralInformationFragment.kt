package com.fieldflo.screens.formDailyLog.generalInformation

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.fieldflo.R
import com.fieldflo.common.dateTimePickers.DatePickerDialog
import com.fieldflo.common.gone
import com.fieldflo.common.toDate
import com.fieldflo.common.toSimpleString
import com.fieldflo.common.visible
import com.fieldflo.screens.formDailyLog.SupervisorViewData
import com.fieldflo.screens.formDailyLog.verifySupervisor.VerifyDialogFragment
import com.fieldflo.screens.selectEmployee.SelectEmployeeDialog
import com.fieldflo.screens.selectSupervisor.SelectSupervisorDialog
import kotlinx.android.synthetic.main.fragment_daily_log_form_general_information.*
import kotlinx.android.synthetic.main.view_date_picker_item.view.*
import java.util.*

class GeneralInformationFragment : Fragment(R.layout.fragment_daily_log_form_general_information) {

    companion object {
        fun newInstance() = GeneralInformationFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity_daily_log_form_general_date.date_title.text =
            getString(R.string.activity_daily_form_date)

        activity_daily_log_form_supervisor.setOnClickListener {
            SelectSupervisorDialog.newInstance()
                .show(childFragmentManager, SelectEmployeeDialog.TAG)
        }
        activity_daily_log_form_content_verifyTV.setOnClickListener {
            (activity_daily_log_form_supervisor.tag as? Int)?.let { supervisorId ->
                if (supervisorId != 0) {
                    VerifyDialogFragment.newInstance(supervisorId)
                        .show(childFragmentManager, VerifyDialogFragment.TAG)
                }
            }
        }
        activity_daily_log_form_general_date.setOnClickListener(
            createShowDateClickListener(
                activity_daily_log_form_general_date.date_field_text
            )
        )
    }

    fun setViewState(viewState: GeneralInformationViewState) {
        render(viewState)
    }

    fun setSupervisorData(supervisorViewData: SupervisorViewData) {
        when (supervisorViewData.verifyStatus) {
            SupervisorViewData.VerifyStatus.NONE -> {
                activity_daily_log_form_content_verifyTV.gone()
            }
            SupervisorViewData.VerifyStatus.NOT_VERIFIED -> {
                activity_daily_log_form_content_verifyTV.visible()
                activity_daily_log_form_content_verifyTV.isEnabled = true
                activity_daily_log_form_content_verifyTV.setText(R.string.activity_daily_timesheet_verify)
                activity_daily_log_form_content_verifyTV.background =
                    ContextCompat.getDrawable(
                        activity_daily_log_form_content_verifyTV.context,
                        R.drawable.bkgd_round_accent_button
                    )
            }
            SupervisorViewData.VerifyStatus.VERIFIED -> {
                activity_daily_log_form_content_verifyTV.visible()
                activity_daily_log_form_content_verifyTV.isEnabled = false
                activity_daily_log_form_content_verifyTV.text = getString(
                    R.string.activity_daily_log_form_verified_supervisor_text,
                    supervisorViewData.supervisorName
                )
                activity_daily_log_form_content_verifyTV.background =
                    ContextCompat.getDrawable(
                        activity_daily_log_form_content_verifyTV.context,
                        R.drawable.bkgd_round_green_button
                    )
            }
        }

        activity_daily_log_form_supervisor.apply {
            text = supervisorViewData.supervisorName
            tag = supervisorViewData.supervisorId
        }
    }

    fun getViewState() = GeneralInformationViewState(
        projectName = activity_daily_log_form_content_project_name_value.text.toString(),
        projectNumber = activity_daily_log_form_content_project_number_value.text.toString(),
        shiftStart = activity_daily_log_form_content_shift_start_value.text.toString(),
        date = activity_daily_log_form_general_date.date_field_text.text.toString().toDate(),
        shiftEnd = activity_daily_log_form_content_shift_end_value.text.toString(),
        projectManagerName = activity_daily_log_form_project_manager.text.toString()
    )

    fun getSupervisorViewData() = SupervisorViewData(
        supervisorVerify = !activity_daily_log_form_content_verifyTV.isEnabled,
        supervisorId = activity_daily_log_form_supervisor.tag as? Int ?: 0,
        supervisorName = activity_daily_log_form_supervisor.text.toString()
    )

    private fun render(viewState: GeneralInformationViewState) {
        activity_daily_log_form_content_project_name_value.setText(viewState.projectName)
        activity_daily_log_form_content_project_number_value.setText(viewState.projectNumber)
        activity_daily_log_form_content_shift_start_value.setText(viewState.shiftStart)
        activity_daily_log_form_general_date.date_field_text.text = viewState.date.toSimpleString()
        activity_daily_log_form_content_shift_end_value.setText(viewState.shiftEnd)

        activity_daily_log_form_project_manager.text = viewState.projectManagerName
    }

    private fun createShowDateClickListener(dateTitleView: TextView): View.OnClickListener {
        return View.OnClickListener {
            val date = extractDateFromTextForPicker(dateTitleView)

            val picker =
                DatePickerDialog.newInstance(date, object : DatePickerDialog.DateSelectedListener {
                    override fun onDateSelected(newDate: Date) {
                        dateTitleView.text = newDate.toSimpleString()
                    }
                })
            picker.show(childFragmentManager, DatePickerDialog.TAG)
        }
    }

    private fun extractDateFromTextForPicker(dateTV: TextView): Date {
        val pleaseSelect = getString(R.string.activity_daily_form_date_title)
        val dateText = dateTV.text.toString()
        return if (dateText == "" || dateText == pleaseSelect) {
            Date()
        } else {
            dateTV.text.toString().toDate()
        }
    }
}