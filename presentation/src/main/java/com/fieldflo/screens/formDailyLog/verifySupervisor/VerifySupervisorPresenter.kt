package com.fieldflo.screens.formDailyLog.verifySupervisor

import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.EmployeeUseCases
import javax.inject.Inject

class VerifySupervisorPresenter @Inject constructor(private val employeeUseCases: EmployeeUseCases) :
    ViewModel() {

    suspend fun getEmployeePin(employeeId: Int): String {
        return employeeUseCases.getEmployeeById(employeeId)?.employeePinNumber ?: ""
    }
}