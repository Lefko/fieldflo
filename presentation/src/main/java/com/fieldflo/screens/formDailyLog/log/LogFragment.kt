package com.fieldflo.screens.formDailyLog.log

import androidx.fragment.app.Fragment
import com.fieldflo.R
import kotlinx.android.synthetic.main.fragment_daily_log_form_log.view.*

class LogFragment : Fragment(R.layout.fragment_daily_log_form_log) {

    companion object {
        fun newInstance() = LogFragment()
    }

    fun setViewState(viewState: LogViewState) {
        render(viewState)
    }

    fun getViewState() = LogViewState(
        dailyLog = requireView().activity_daily_log_form_daily_log_value.text.toString(),
        visitors = requireView().activity_daily_log_form_visitors_value.text.toString(),
        status = requireView().activity_daily_log_form_status_value.text.toString(),
        inspectionsAndTests = requireView().activity_daily_log_form_inspections_and_tests_value.text.toString()
    )

    private fun render(viewState: LogViewState) {
        requireView().activity_daily_log_form_daily_log_value.setText(viewState.dailyLog)
        requireView().activity_daily_log_form_visitors_value.setText(viewState.visitors)
        requireView().activity_daily_log_form_status_value.setText(viewState.status)
        requireView().activity_daily_log_form_inspections_and_tests_value.setText(viewState.inspectionsAndTests)
    }
}