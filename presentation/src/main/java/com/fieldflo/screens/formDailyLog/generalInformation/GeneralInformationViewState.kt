package com.fieldflo.screens.formDailyLog.generalInformation

import java.util.*

data class GeneralInformationViewState(
    val projectName: String = "",
    val projectNumber: String = "",

    val shiftStart: String = "",
    val date: Date = Date(0),
    val shiftEnd: String = "",

    val projectManagerName: String = ""
)
