package com.fieldflo.screens.employeesCertificate.projectCertsList

import com.fieldflo.usecases.EmployeeCertData

data class EmployeesCertificateViewState(
    val fullCertificateList: List<EmployeeCertData> = emptyList(),
    val filteredEmployeeCertificates: List<EmployeeCertificateUiRow> = emptyList(),
    val isNetworkConnected: Boolean = false,
    val hasPrintCertRole: Boolean = false
) {

    val isPrintEnabled =
        isNetworkConnected && hasPrintCertRole && filteredEmployeeCertificates.any { it.isRowSelected }

    data class EmployeeCertificateUiRow(
        val certificateId: Int = 0,
        val employeeImageFullUrl: String = "",
        val employeeFullName: String = "",
        val certificateName: String = "",
        val formattedExpirationDate: String = "",
        val isRowSelected: Boolean = false,
        val isExpired: Boolean,
        private val isNetworkConnected: Boolean = false,
        private val hasPrintCertRole: Boolean = false
    ) {
        val isPrintEnabled
            get() = isNetworkConnected && hasPrintCertRole
    }
}