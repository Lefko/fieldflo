package com.fieldflo.screens.employeesCertificate.projectCertsList

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.fieldflo.R
import com.fieldflo.common.safeClickListener
import com.fieldflo.screens.employeesCertificate.projectCertsList.EmployeesCertificatesAdapter.EmployeesCertificatesViewHolder

class EmployeesCertificatesAdapter(private val certClick: (EmployeeCertClick) -> Unit) :
    ListAdapter<EmployeesCertificateViewState.EmployeeCertificateUiRow, EmployeesCertificatesViewHolder>(
        CALLBACK
    ) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): EmployeesCertificatesViewHolder {

        val holder = EmployeesCertificatesViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_employers_certificate, parent, false) as ViewGroup
        )

        holder.printBtnIV.safeClickListener {
            val position = holder.bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val cert = getItem(position)
                certClick(EmployeeCertClick.PRINT(cert))
            }
        }

        holder.selectBtnIV.safeClickListener {
            val position = holder.bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val cert = getItem(position)
                certClick(EmployeeCertClick.TOGGLE_SELECT(cert))
            }
        }

        return holder
    }

    override fun onBindViewHolder(holder: EmployeesCertificatesViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class EmployeesCertificatesViewHolder(rowLayout: ViewGroup) :
        RecyclerView.ViewHolder(rowLayout) {
        private val employeePhotoIV: ImageView =
            rowLayout.findViewById(R.id.row_employers_certificate_employee_photo_icon)
        private val employeeNameTV: TextView =
            rowLayout.findViewById(R.id.row_employers_certificate_employee_name)
        private val expDateTV: TextView =
            rowLayout.findViewById(R.id.row_employers_certificate_exp_date)
        private val categoryNameTV: TextView =
            rowLayout.findViewById(R.id.row_employers_certificate_category)
        val selectBtnIV: ImageView =
            rowLayout.findViewById(R.id.row_employers_certificate_employee_select)
        val printBtnIV: ImageView =
            rowLayout.findViewById(R.id.row_employers_certificate_cert_print_btn)

        private val glideRequestOptions: RequestOptions = RequestOptions
            .diskCacheStrategyOf(DiskCacheStrategy.ALL)
            .centerCrop()
            .placeholder(R.drawable.app_icon)
            .transform(RoundedCorners(5))

        private val glideRequest = Glide.with(employeePhotoIV)

        fun bind(uiModel: EmployeesCertificateViewState.EmployeeCertificateUiRow) {
            glideRequest
                .load(uiModel.employeeImageFullUrl)
                .apply(glideRequestOptions)
                .into(employeePhotoIV)
            employeeNameTV.text = uiModel.employeeFullName
            expDateTV.apply {
                text = uiModel.formattedExpirationDate
                setTextColor(
                    ContextCompat.getColor(
                        context, if (uiModel.isExpired) {
                            R.color.attention
                        } else {
                            R.color.text_color
                        }
                    )
                )
            }

            categoryNameTV.text = uiModel.certificateName
            setSelected(uiModel.isRowSelected)

            printBtnIV.isEnabled = uiModel.isPrintEnabled
        }

        private fun setSelected(isSelected: Boolean) {
            if (isSelected) {
                selectBtnIV.setImageResource(R.drawable.ic_select_single_medium)
            } else {
                selectBtnIV.setImageResource(R.drawable.ic_select_empty)
            }
        }
    }

    sealed class EmployeeCertClick {
        data class PRINT(val cert: EmployeesCertificateViewState.EmployeeCertificateUiRow) :
            EmployeeCertClick()

        data class TOGGLE_SELECT(val cert: EmployeesCertificateViewState.EmployeeCertificateUiRow) :
            EmployeeCertClick()
    }
}

private val CALLBACK =
    object : DiffUtil.ItemCallback<EmployeesCertificateViewState.EmployeeCertificateUiRow>() {

        override fun areItemsTheSame(
            oldItem: EmployeesCertificateViewState.EmployeeCertificateUiRow,
            newItem: EmployeesCertificateViewState.EmployeeCertificateUiRow
        ) =
            oldItem.certificateId == newItem.certificateId

        override fun areContentsTheSame(
            oldItem: EmployeesCertificateViewState.EmployeeCertificateUiRow,
            newItem: EmployeesCertificateViewState.EmployeeCertificateUiRow
        ) =
            oldItem == newItem
    }