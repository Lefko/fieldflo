package com.fieldflo.screens.employeesCertificate.printedProjectCerts

import androidx.lifecycle.ViewModel
import com.fieldflo.common.toSimpleString
import com.fieldflo.usecases.EmployeeCertificatesUseCases
import com.fieldflo.usecases.ProjectStatus
import com.fieldflo.usecases.ProjectsUseCases
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.io.File
import java.util.*
import javax.inject.Inject

class PrintedCertificatePresenter @Inject constructor(
    private val projectsUseCases: ProjectsUseCases,
    private val employeeCertificatesUseCases: EmployeeCertificatesUseCases
) : ViewModel() {

    fun getProjectStatusFlow(projectId: Int): Flow<ProjectStatus> {
        return projectsUseCases.getProjectStatusFlow(projectId)
    }

    fun getProjectCertsFlow(projectId: Int): Flow<List<PrintedProjectCertificateUiRow>> {
        return employeeCertificatesUseCases.getProjectCertsDir(projectId)
            .map { files ->
                files.map { file ->
                    file.toUiModel()
                }
            }
    }

    fun changeFileName(fileLocation: String, newName: String) {
        employeeCertificatesUseCases.changeFileName(fileLocation, newName)
    }

    fun deleteCert(fileName: String, projectId: Int) {
        employeeCertificatesUseCases.deleteProjectCertificateFile(fileName, projectId)
    }
}

private fun File.toUiModel() = PrintedProjectCertificateUiRow(
    fileLocation = this.absolutePath,
    fileName = this.name,
    fileExtension = this.extension,
    fileCreatedDate = Date(this.lastModified()).toSimpleString(),
    fileSize = this.length()
)

data class PrintedProjectCertificateUiRow(
    val fileLocation: String = "",
    val fileName: String = "",
    val fileExtension: String = "",
    val fileSize: Long = 0,
    val fileCreatedDate: String = ""
)