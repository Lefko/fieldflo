package com.fieldflo.screens.employeesCertificate.printedProjectCerts

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.fieldflo.R
import com.fieldflo.di.injector
import com.fieldflo.screens.displayPicture.DisplayPictureActivity
import com.fieldflo.screens.drawer.DrawerManager
import com.fieldflo.usecases.ProjectStatus
import kotlinx.android.synthetic.main.view_activity_printed_certificate_content.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter

class PrintedCertificateActivity : AppCompatActivity(R.layout.activity_printed_certificate),
    EditDialogFragment.EditDialogCallback, VerifyDeleteCertDialogFragment.DeleteCertDialogCallback {

    companion object {
        fun start(sender: Activity, projectId: Int, projectName: String) {
            val intent = Intent(sender, PrintedCertificateActivity::class.java).apply {
                putExtra(PROJECT_ID, projectId)
                putExtra(PROJECT_NAME, projectName)
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }

            sender.startActivity(intent)
        }

        private const val PROJECT_NAME = "project_name"
        private const val PROJECT_ID = "project_id"
    }

    private val presenter by viewModels<PrintedCertificatePresenter> { injector.printedCertificateViewModelFactory() }
    private val projectId by lazy { intent!!.extras!!.getInt(PROJECT_ID) }
    private val projectName by lazy { intent!!.extras!!.getString(PROJECT_NAME) }
    private val drawerManager by lazy {
        DrawerManager(
            { findViewById(R.id.drawer_layout) }, { this },
            { findViewById(R.id.activity_printed_certificate_content_toolbar) }
        )
    }
    private val adapter = PrintedCertificateAdapter {
        when (it) {
            is PrintedCertificateAdapter.EmployeeCertClick.EDIT -> onEditCertClick(it.cert)
            is PrintedCertificateAdapter.EmployeeCertClick.DELETE -> onDeleteCertClick(it.cert)
            is PrintedCertificateAdapter.EmployeeCertClick.CLICK -> onCertClick(it.cert)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity_printed_certificate_content_project_nameTV.text = projectName
        setSupportActionBar(activity_certificates_list_content_search_closed_toolbar_new)

        activity_printed_certificate_content_backTV.setOnClickListener {
            onBackPressed()
        }

        initRecyclerView()

        present()

        lifecycleScope.launchWhenCreated {
            presenter.getProjectStatusFlow(projectId)
                .filter { it == ProjectStatus.DELETED }
                .collect { finish() }
        }
    }

    override fun onBackPressed() {
        val handled = drawerManager.onBackPress()
        if (!handled) {
            super.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        drawerManager.refreshSelected()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawerManager.onConfigChange(newConfig)
    }

    private fun onEditCertClick(cert: PrintedProjectCertificateUiRow) {
        EditDialogFragment.newInstance(
            cert.fileName,
            cert.fileLocation,
            cert.fileExtension,
            adapter.getCertificateNames()
        ).show(supportFragmentManager, EditDialogFragment.TAG)
    }

    private fun onDeleteCertClick(cert: PrintedProjectCertificateUiRow) {
        VerifyDeleteCertDialogFragment.newInstance(cert.fileName)
            .show(supportFragmentManager, VerifyDeleteCertDialogFragment.TAG)
    }

    private fun onCertClick(cert: PrintedProjectCertificateUiRow) {
        DisplayPictureActivity.start(this, cert.fileExtension, cert.fileLocation, projectId)
    }

    private fun initRecyclerView() {
        activity_printed_certificate_listRV.layoutManager = LinearLayoutManager(this)
        activity_printed_certificate_listRV.adapter = adapter
    }

    override fun setNewName(newName: String, fileLocation: String) {
        presenter.changeFileName(fileLocation, newName)
    }

    override fun onDeleteCertificate(fileName: String) {
        presenter.deleteCert(fileName, projectId)
    }

    private fun present() {
        lifecycleScope.launchWhenCreated {
            presenter.getProjectCertsFlow(projectId).collect {
                adapter.submitList(it)
            }
        }
    }
}
