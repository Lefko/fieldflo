package com.fieldflo.screens.employeesCertificate.projectCertsList

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.fieldflo.R
import com.fieldflo.common.onQueryTextChanged
import com.fieldflo.common.safeClickListener
import com.fieldflo.di.injector
import com.fieldflo.screens.drawer.DrawerManager
import com.fieldflo.screens.employeesCertificate.printedProjectCerts.PrintedCertificateActivity
import com.fieldflo.usecases.ProjectStatus
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.view_activity_employers_certificate_content.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter

class EmployeesCertificateActivity : AppCompatActivity(R.layout.activity_employers_certificate) {

    companion object {
        fun start(sender: Activity, projectId: Int, projectName: String) {
            val intent = Intent(sender, EmployeesCertificateActivity::class.java).apply {
                putExtra(PROJECT_ID, projectId)
                putExtra(PROJECT_NAME, projectName)
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }

            sender.startActivity(intent)
        }

        private const val PROJECT_NAME = "project_name"
        private const val PROJECT_ID = "project_id"
    }

    private val adapter = EmployeesCertificatesAdapter { certClick ->
        when (certClick) {
            is EmployeesCertificatesAdapter.EmployeeCertClick.PRINT ->
                printCert(certClick.cert)
            is EmployeesCertificatesAdapter.EmployeeCertClick.TOGGLE_SELECT ->
                presenter.toggleRow(certClick.cert)
        }
    }
    private val presenter by viewModels<EmployeesCertificatePresenter> { injector.employersCertificateViewModelFactory() }
    private val projectId by lazy { intent!!.extras!!.getInt(PROJECT_ID) }
    private val projectName by lazy { intent!!.extras!!.getString(PROJECT_NAME)!! }

    private val drawerManager by lazy {
        DrawerManager(
            { findViewById(R.id.drawer_layout) }, { this },
            { findViewById(R.id.activity_employers_certificate_content_toolbar) }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity_employers_certificate_content_project_nameTV.text = projectName
        setSupportActionBar(activity_certificates_list_content_search_closed_toolbar_new)

        activity_employers_certificate_content_backTV.setOnClickListener {
            onBackPressed()
        }
        activity_employers_certificate_content_printedTV.safeClickListener {
            PrintedCertificateActivity.start(this, projectId, projectName)
        }

        activity_employers_certificate_content_select_allTV.safeClickListener {
            presenter.selectAllRows()
        }

        activity_employers_certificate_content_printTV.safeClickListener {
            lifecycleScope.launchWhenCreated {
                val printSuccess = presenter.printSelected(projectId)
                handlePrintResult(printSuccess)
            }
        }

        initRecyclerView()

        lifecycleScope.launchWhenCreated {
            presenter.getProjectStatusFlow(projectId)
                .filter { it == ProjectStatus.DELETED }
                .collect { finish() }
        }

        present()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_toolbar_search, menu)
        val searchItem = menu!!.findItem(R.id.search)
        setupSearch(searchItem)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onBackPressed() {
        val handled = drawerManager.onBackPress()
        if (!handled) {
            super.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        drawerManager.refreshSelected()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawerManager.onConfigChange(newConfig)
    }

    private fun initRecyclerView() {
        activity_employers_certificate_listRV.layoutManager = LinearLayoutManager(this)
        activity_employers_certificate_listRV.adapter = adapter
    }

    private fun present() {
        presenter.loadCerts(projectId)
        lifecycleScope.launchWhenCreated {
            presenter.viewStateFlow.collect { render(it) }
        }
    }

    private fun printCert(cert: EmployeesCertificateViewState.EmployeeCertificateUiRow) {
        lifecycleScope.launchWhenCreated {
            val printSuccess = presenter.printCert(cert.certificateId, projectId)
            handlePrintResult(printSuccess)
        }
    }

    private fun render(viewState: EmployeesCertificateViewState) {
        activity_employers_certificate_content_printTV.isEnabled = viewState.isPrintEnabled
        adapter.submitList(viewState.filteredEmployeeCertificates)
    }

    private fun setupSearch(searchItem: MenuItem) {
        val searchView = searchItem.actionView as SearchView
        searchView.onQueryTextChanged {
            presenter.submitQuery(it.trim())
            true
        }

        searchView.setOnCloseListener {
            presenter.submitQuery("")
            false
        }
    }

    private fun handlePrintResult(printSuccess: Boolean) {
        if (printSuccess) {
            Snackbar.make(
                activity_employers_certificate_content_rootCL,
                R.string.activity_employers_certificate_print_complete,
                Snackbar.LENGTH_LONG
            ).setAction(R.string.activity_employers_certificate_view) {
                PrintedCertificateActivity.start(
                    this@EmployeesCertificateActivity,
                    projectId,
                    projectName
                )
            }.show()
        } else {
            Snackbar.make(
                activity_employers_certificate_content_rootCL,
                R.string.activity_employers_certificate_print_error,
                Snackbar.LENGTH_LONG
            ).show()
        }
    }
}
