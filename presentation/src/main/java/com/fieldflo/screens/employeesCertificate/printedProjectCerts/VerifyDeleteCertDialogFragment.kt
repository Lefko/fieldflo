package com.fieldflo.screens.employeesCertificate.printedProjectCerts

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.fieldflo.R

class VerifyDeleteCertDialogFragment : DialogFragment() {

    private lateinit var callback: DeleteCertDialogCallback

    private val fileName: String by lazy { arguments!!.getString(FILENAME_KEY)!! }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val name = fileName.substring(0, fileName.lastIndexOf("."))
        return AlertDialog.Builder(activity as Context)
            .setMessage(getString(R.string.activity_printed_certificate_title_delete_certificate, name))
            .setPositiveButton(R.string.activity_printed_certificate_delete_btn, { _, _ ->
                callback.onDeleteCertificate(fileName)
            })
            .setNegativeButton(android.R.string.cancel, { _, _ ->
                dismiss()
            })
            .show()
    }

    private fun onAttachToContext(context: Context) {
        if (context is DeleteCertDialogCallback) {
            this.callback = context
        } else {
            throw IllegalArgumentException(
                "${context.javaClass.simpleName} " +
                        "must be an instance of VerifyDeleteCertDialogFragment"
            )
        }
    }

    companion object {

        const val TAG = "VerifyDeleteCertDialogFragment"
        private const val FILENAME_KEY = "filename_key"

        fun newInstance(fileName: String) =
            VerifyDeleteCertDialogFragment().apply {
                arguments = Bundle().apply {
                    putString(FILENAME_KEY, fileName)
                }
            }
    }

    interface DeleteCertDialogCallback {
        fun onDeleteCertificate(fileName: String)
    }
}