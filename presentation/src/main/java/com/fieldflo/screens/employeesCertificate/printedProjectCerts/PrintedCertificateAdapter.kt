package com.fieldflo.screens.employeesCertificate.printedProjectCerts

import android.text.format.Formatter
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fieldflo.R
import com.fieldflo.common.safeClickListener

class PrintedCertificateAdapter(private val clickListener: (EmployeeCertClick) -> Unit) :
    ListAdapter<PrintedProjectCertificateUiRow, PrintedCertificateAdapter.PrintedCertificatesViewHolder>(
        CALLBACK
    ) {

    sealed class EmployeeCertClick {
        data class EDIT(val cert: PrintedProjectCertificateUiRow) : EmployeeCertClick()
        data class DELETE(val cert: PrintedProjectCertificateUiRow) : EmployeeCertClick()
        data class CLICK(val cert: PrintedProjectCertificateUiRow) : EmployeeCertClick()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PrintedCertificatesViewHolder {
        val rowLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_certificate_printed, parent, false) as ViewGroup

        val holder = PrintedCertificatesViewHolder(rowLayout)

        holder.editBtnIV.safeClickListener {
            val position = holder.bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val cert = getItem(position)
                clickListener(EmployeeCertClick.EDIT(cert))
            }
        }

        holder.removeBtnIV.safeClickListener {
            val position = holder.bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val cert = getItem(position)
                clickListener(EmployeeCertClick.DELETE(cert))
            }
        }

        rowLayout.safeClickListener {
            val position = holder.bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val cert = getItem(position)
                clickListener(EmployeeCertClick.CLICK(cert))
            }
        }

        return holder
    }

    fun getCertificateNames(): List<String> {
        val names = mutableListOf<String>()
        for (i in 0 until itemCount) {
            names.add(getItem(i).fileName)
        }
        return names
    }

    override fun onBindViewHolder(holder: PrintedCertificatesViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class PrintedCertificatesViewHolder(rowLayout: ViewGroup) : RecyclerView.ViewHolder(rowLayout) {
        private val certificateNameTV: TextView =
            rowLayout.findViewById(R.id.row_certificate_printed_label)
        private val typeTV: TextView = rowLayout.findViewById(R.id.row_certificate_printed_type)
        private val sizeTV: TextView = rowLayout.findViewById(R.id.row_certificate_printed_size)
        private val createdDateTV: TextView =
            rowLayout.findViewById(R.id.row_certificate_printed_created)

        val editBtnIV: ImageView = rowLayout.findViewById(R.id.row_certificate_printed_edit_icon)
        val removeBtnIV: ImageView =
            rowLayout.findViewById(R.id.row_certificate_printed_trash_can_icon)

        fun bind(uiModel: PrintedProjectCertificateUiRow) {
            typeTV.text = uiModel.fileExtension
            createdDateTV.text = uiModel.fileCreatedDate
            sizeTV.text = Formatter.formatShortFileSize(sizeTV.context, uiModel.fileSize)
            certificateNameTV.text =
                uiModel.fileName.substring(0, uiModel.fileName.lastIndexOf("."))
        }
    }
}

private val CALLBACK = object : DiffUtil.ItemCallback<PrintedProjectCertificateUiRow>() {

    override fun areItemsTheSame(
        oldItem: PrintedProjectCertificateUiRow,
        newItem: PrintedProjectCertificateUiRow
    ) =
        oldItem.fileName == newItem.fileName

    override fun areContentsTheSame(
        oldItem: PrintedProjectCertificateUiRow,
        newItem: PrintedProjectCertificateUiRow
    ) =
        oldItem == newItem
}