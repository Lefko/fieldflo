package com.fieldflo.screens.employeesCertificate.printedProjectCerts

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.fieldflo.R
import com.fieldflo.common.afterTextChanges
import kotlinx.android.synthetic.main.dialog_edit_certificate_name.view.*
import java.util.*
import java.util.regex.Pattern

class EditDialogFragment : DialogFragment() {

    private lateinit var callback: EditDialogCallback
    private lateinit var positiveBtn: Button
    private lateinit var dialog: AlertDialog
    private val fileLocation: String by lazy {
        arguments!!.getString(FILE_LOCATION_KEY)!!
    }
    private val extension: String by lazy {
        arguments!!.getString(EXTENTION_KEY)!!
    }
    private val names: ArrayList<String> by lazy {
        arguments!!.getStringArrayList(NAMES_LIST_KEY)!!
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    override fun onStart() {
        super.onStart()
        positiveBtn = dialog.getButton(Dialog.BUTTON_POSITIVE)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_edit_certificate_name, null)
        var oldName = arguments!!.getString(OLD_NAME_KEY)
        oldName = oldName!!.substring(0, oldName.lastIndexOf("."))

        var newName = ""
        val pattern = Pattern.compile("^[a-zA-Z0-9 ]*$")

        view.dialog_edit_new_name.afterTextChanges{ text, _ ->
            if (pattern.matcher(text.trim()).matches()){
                newName = text.trim()
                positiveBtn.isEnabled = true
            }else{
                positiveBtn.isEnabled = false
            }
        }

        view.dialog_edit_old_name.text = oldName
        dialog = AlertDialog.Builder(activity as Context)
            .setView(view)
            .setPositiveButton(android.R.string.ok, null)
            .setNegativeButton(android.R.string.cancel, { _, _ ->
                dismiss()
            })
            .create()
        dialog.setOnShowListener {
            val button = (dialog).getButton(AlertDialog.BUTTON_POSITIVE)
            button.setOnClickListener {
                when {
                    newName.isEmpty() -> Toast.makeText(context, R.string.activity_printed_certificate_new_name_hint, Toast.LENGTH_LONG).show()
                    "$newName.$extension" == oldName -> Toast.makeText(context, R.string.activity_printed_certificate_another_name, Toast.LENGTH_LONG).show()
                    names.contains("$newName.$extension") -> Toast.makeText(context, R.string.activity_printed_certificate_name_exist, Toast.LENGTH_LONG).show()
                    else -> {
                        val name = "$newName.$extension".replace(" ", "_")
                        callback.setNewName(name, fileLocation)
                        dismiss()
                    }
                }
            }
        }
        dialog.show()
        return dialog
    }

    private fun onAttachToContext(context: Context) {
        if (context is EditDialogCallback) {
            this.callback = context
        } else {
            throw IllegalArgumentException(
                "${context.javaClass.simpleName} " +
                        "must be an instance of EditDialogFragment"
            )
        }
    }

    companion object {

        const val TAG = "EditDialogFragment"
        private const val OLD_NAME_KEY = "old_name_key"
        private const val FILE_LOCATION_KEY = "names_list_key"
        private const val NAMES_LIST_KEY = "file_location_key"
        private const val EXTENTION_KEY = "extension_key"

        fun newInstance(oldName: String, fileLocation: String, extension: String, names: List<String>) =
            EditDialogFragment().apply {
                arguments = Bundle().apply {
                    putString(OLD_NAME_KEY, oldName)
                    putString(FILE_LOCATION_KEY, fileLocation)
                    putString(EXTENTION_KEY, extension)
                    putStringArrayList(NAMES_LIST_KEY, names as ArrayList<String>?)
                }
            }
    }

    interface EditDialogCallback {
        fun setNewName(newName: String, fileLocation: String)
    }
}