package com.fieldflo.screens.employeesCertificate.projectCertsList

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fieldflo.common.networkConnection.IConnectionData
import com.fieldflo.common.toSimpleString
import com.fieldflo.usecases.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class EmployeesCertificatePresenter @Inject constructor(
    private val projectsUseCases: ProjectsUseCases,
    private val employeesCertificatesUseCase: EmployeeCertificatesUseCases,
    private val networkConnection: IConnectionData,
    rolesUseCases: UserRolesUseCases
) : ViewModel() {

    private val hasPrintCertRole = rolesUseCases.hasCreateEditCertRole()

    private val _viewStateObservable = MutableStateFlow(
        EmployeesCertificateViewState(
            isNetworkConnected = networkConnection.isWifiConnected,
            hasPrintCertRole = hasPrintCertRole
        )
    )
    val viewStateFlow: Flow<EmployeesCertificateViewState> = _viewStateObservable

    private val mutex = Mutex()

    private val scope = viewModelScope + CoroutineExceptionHandler { _, e ->
        Timber.d(e, "EmployeesCertificatePresenter error")
    }

    init {
        scope.launch {
            networkConnection.getWifiConnected().collect { wifiConnected ->
                mutex.withLock {
                    val viewState = _viewStateObservable.value
                    val newState = viewState.copy(isNetworkConnected = wifiConnected)
                    _viewStateObservable.value = newState
                }
            }
        }
    }

    fun getProjectStatusFlow(projectId: Int): Flow<ProjectStatus> {
        return projectsUseCases.getProjectStatusFlow(projectId)
    }

    fun loadCerts(projectId: Int) {
        scope.launch(Dispatchers.IO) {
            val certs = employeesCertificatesUseCase.getCertsForProject(projectId)
            mutex.withLock {
                val viewState = _viewStateObservable.value
                val newState = viewState.copy(
                    fullCertificateList = certs,
                    filteredEmployeeCertificates = certs.map {
                        it.toUiModel(
                            viewState.isNetworkConnected,
                            hasPrintCertRole
                        )
                    }
                )
                _viewStateObservable.value = newState
            }
        }
    }

    fun submitQuery(query: String) {
        scope.launch(Dispatchers.IO) {
            mutex.withLock {
                val viewState = _viewStateObservable.value
                val certs = viewState.fullCertificateList
                val hasWifi = viewState.isNetworkConnected
                val newState = viewState.copy(
                    filteredEmployeeCertificates = certs.filter { it.matchesQuery(query) }
                        .map { it.toUiModel(hasWifi, hasPrintCertRole) }
                )
                _viewStateObservable.value = newState
            }
        }
    }

    fun selectAllRows() {
        scope.launch(Dispatchers.Default) {
            mutex.withLock {
                val prevState = _viewStateObservable.value
                val newState =
                    prevState.copy(filteredEmployeeCertificates = prevState.filteredEmployeeCertificates.map {
                        it.copy(isRowSelected = true)
                    })

                withContext(Dispatchers.Main) {
                    _viewStateObservable.value = newState
                }
            }
        }
    }

    fun toggleRow(cert: EmployeesCertificateViewState.EmployeeCertificateUiRow) {
        scope.launch(Dispatchers.Default) {
            mutex.withLock {
                val prevState = _viewStateObservable.value
                val newEmployees = prevState.filteredEmployeeCertificates.map {
                    if (it.certificateId == cert.certificateId) {
                        it.copy(isRowSelected = !it.isRowSelected)
                    } else {
                        it
                    }
                }
                val newState = prevState.copy(filteredEmployeeCertificates = newEmployees)
                _viewStateObservable.value = newState
            }
        }
    }

    suspend fun printSelected(projectId: Int): Boolean {
        val currentEmployeeCerts =
            mutex.withLock { _viewStateObservable.value.filteredEmployeeCertificates }
        val selectedCertIds =
            currentEmployeeCerts.filter { it.isRowSelected }.map { it.certificateId }
        deselectAll()
        return employeesCertificatesUseCase.printCertificates(selectedCertIds, projectId)
    }

    suspend fun printCert(certificateId: Int, projectId: Int): Boolean {
        return employeesCertificatesUseCase.printCertificates(listOf(certificateId), projectId)
    }

    private fun deselectAll() {
        scope.launch(Dispatchers.Default) {
            mutex.withLock {
                val prevState = _viewStateObservable.value
                val newState = withContext(Dispatchers.Default) {
                    prevState.copy(filteredEmployeeCertificates = prevState.filteredEmployeeCertificates.map {
                        it.copy(isRowSelected = false)
                    })
                }
                withContext(Dispatchers.Main) {
                    _viewStateObservable.value = newState
                }
            }
        }
    }
}

private fun EmployeeCertData.matchesQuery(query: String): Boolean {
    return this.employeeFullName.contains(query, ignoreCase = true) ||
            this.certificateName.contains(query, ignoreCase = true) ||
            this.employeeId.toString().contains(query, ignoreCase = true)
}

private fun EmployeeCertData.toUiModel(isNetworkConnected: Boolean, hasPrintCertRole: Boolean) =
    EmployeesCertificateViewState.EmployeeCertificateUiRow(
        certificateId = this.certificateId,
        employeeImageFullUrl = this.employeeImageUrl,
        employeeFullName = this.employeeFullName,
        certificateName = this.certificateName,
        formattedExpirationDate = this.certificateExpirationDate.toSimpleString(),
        isExpired = this.certificateExpirationDate.before(Date()),
        isNetworkConnected = isNetworkConnected,
        hasPrintCertRole = hasPrintCertRole
    )