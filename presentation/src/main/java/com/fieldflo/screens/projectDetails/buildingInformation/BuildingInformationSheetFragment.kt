package com.fieldflo.screens.projectDetails.buildingInformation

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector
import com.fieldflo.screens.projectDetails.BaseSheetFragment
import kotlinx.android.synthetic.main.fragment_base_project_details_sheet.*


class BuildingInformationSheetFragment : BaseSheetFragment() {

    private val presenter by viewModels<BuildingInformationViewModel> { injector.buildingInformationViewModelFactory() }
    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity_project_details_content_certificate_info_titleTV.text =
            getText(R.string.activity_project_building_information_label)

        val buildingSize = addView(R.string.activity_project_building_information_building_size)
        val ageInYears = addView(R.string.activity_project_building_information_age_in_years)
        val numberOfFloors = addView(R.string.activity_project_building_information_number_floors)
        val presentUse = addView(R.string.activity_project_building_information_present_use)
        val priorUse = addView(R.string.activity_project_building_information_prior_use)
        val numberOfUnits =
            addView(R.string.activity_project_building_information_number_dwelling_units)
        val workLocation = addView(R.string.activity_project_building_information_work_location)
        val workDescription =
            addView(R.string.activity_project_building_information_work_description)

        lifecycleScope.launchWhenCreated {
            val buildingInformation = presenter.getBuildingInformation(projectId)
            buildingSize.text = buildingInformation.buildingSize
            ageInYears.text = buildingInformation.buildingAgeOfYears.toString()
            numberOfFloors.text = buildingInformation.buildingNumberOfFloors.toString()
            presentUse.text = buildingInformation.buildingPresentUse
            priorUse.text = buildingInformation.buildingPriorUse
            numberOfUnits.text = buildingInformation.buildingNumberOfDwellingUnits.toString()
            workLocation.text = buildingInformation.buildingWorkLocation
            workDescription.text = buildingInformation.buildingWorkDescription
        }
    }

    companion object {
        const val TAG = "building_information"
        private const val PROJECT_ID = "projectId"

        @JvmStatic
        fun newInstance(projectId: Int) =
            BuildingInformationSheetFragment().apply {
                arguments = bundleOf(PROJECT_ID to projectId)
            }
    }
}
