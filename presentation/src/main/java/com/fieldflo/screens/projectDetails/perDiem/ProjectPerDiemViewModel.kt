package com.fieldflo.screens.projectDetails.perDiem

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.ProjectPerDiemData
import com.fieldflo.usecases.ProjectsUseCases
import javax.inject.Inject

class ProjectPerDiemViewModel @Inject constructor(
    private val projectsUseCases: ProjectsUseCases
) : ViewModel() {

    suspend fun getProjectPerDiem(projectId: Int): PerDiemSheetUiModel {
        return projectsUseCases.getProjectPerDiem(projectId).toUiModel()
    }
}

private fun ProjectPerDiemData.toUiModel() = PerDiemSheetUiModel(
    requiresPerDiem = this.requiresPerDiem,
    perDiemType = this.perDiemType,
    perDiemHourlyAmount = this.perDiemHourlyAmount,
    perDiemDailyAmount = this.perDiemDailyAmount,
    perDiemNotes = this.perDiemNotes,
    perDiemAutoApplyToTimesheet = this.perDiemAutoApplyToTimesheet
)

data class PerDiemSheetUiModel(
    val requiresPerDiem: Boolean = true,
    val perDiemType: String = "",
    val perDiemHourlyAmount: Double = 0.0,
    val perDiemDailyAmount: Double = 0.0,
    val perDiemNotes: String = "",
    val perDiemAutoApplyToTimesheet: Boolean = false
)
