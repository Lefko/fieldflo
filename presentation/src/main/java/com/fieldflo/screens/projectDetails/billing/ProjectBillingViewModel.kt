package com.fieldflo.screens.projectDetails.billing

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.ProjectBillingInfo
import com.fieldflo.usecases.ProjectsUseCases
import javax.inject.Inject

class ProjectBillingViewModel @Inject constructor(
    private val projectsUseCases: ProjectsUseCases
) : ViewModel() {

    suspend fun getProjectBilling(projectId: Int): BillingSheetUiModel =
        projectsUseCases.getProjectBillingInfo(projectId).toUiModel()

}

private fun ProjectBillingInfo.toUiModel() = BillingSheetUiModel(
    billingCompanyName = this.billingCompanyName,
    billingPurchaseOrWorkOrderNumber = this.billingPurchaseOrWorkOrderNumber,
    billingTermsOfContact = this.billingTermsOfContact,
    billingClientContact = this.billingClientContact,
    billingAddress = this.billingAddress,
    billingCity = this.billingCity,
    billingZipCode = this.billingZipCode,
    billingPhoneNumber = this.billingPhoneNumber,
    billingFaxNumber = this.billingFaxNumber,
    billingStateName = this.billingState
)

data class BillingSheetUiModel(
    val billingCompanyName: String = "",
    val billingPurchaseOrWorkOrderNumber: String = "",
    val billingTermsOfContact: String = "",
    val billingClientContact: String = "",
    val billingAddress: String = "",
    val billingCity: String = "",
    val billingStateName: String = "",
    val billingZipCode: String = "",
    val billingPhoneNumber: String = "",
    val billingFaxNumber: String = ""
)
