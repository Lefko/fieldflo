package com.fieldflo.screens.projectDetails.buildingInformation

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.ProjectBuildingInformationData
import com.fieldflo.usecases.ProjectsUseCases
import javax.inject.Inject

class BuildingInformationViewModel @Inject constructor(
    private val projectsUseCases: ProjectsUseCases
) : ViewModel() {

    suspend fun getBuildingInformation(projectId: Int): BuildingInformationSheetUiModel {
        return projectsUseCases.getProjectBuildingInto(projectId).toUiModel()
    }
}

fun ProjectBuildingInformationData.toUiModel() = BuildingInformationSheetUiModel(
    buildingSize = this.buildingSize,
    buildingAgeOfYears = this.buildingAgeOfYears,
    buildingNumberOfFloors = this.buildingNumberOfFloors,
    buildingPresentUse = this.buildingPresentUse,
    buildingPriorUse = this.buildingPriorUse,
    buildingNumberOfDwellingUnits = this.buildingNumberOfDwellingUnits,
    buildingWorkLocation = this.buildingWorkLocation,
    buildingWorkDescription = this.buildingWorkDescription
)

data class BuildingInformationSheetUiModel(
    val buildingSize: String = "",
    val buildingAgeOfYears: Int = 0,
    val buildingNumberOfFloors: Int = 0,
    val buildingPresentUse: String = "",
    val buildingPriorUse: String = "",
    val buildingNumberOfDwellingUnits: Int = 0,
    val buildingWorkLocation: String = "",
    val buildingWorkDescription: String = ""
)
