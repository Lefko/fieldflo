package com.fieldflo.screens.projectDetails.payroll

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.toGeneralString
import com.fieldflo.di.injector
import com.fieldflo.screens.projectDetails.BaseSheetFragment
import kotlinx.android.synthetic.main.fragment_base_project_details_sheet.*

class PayrollSheetFragment : BaseSheetFragment() {

    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }
    private val presenter by viewModels<PayrollViewModel> { injector.payrollViewModelFactory() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity_project_details_content_certificate_info_titleTV.text =
            getString(R.string.activity_project_payroll_information_label)

        val requiresReport = addView(R.string.activity_project_payroll_information_weekly_certified)
        val isUnion = addView(R.string.activity_project_payroll_information_union_job)
        val isWageJob = addView(R.string.activity_project_payroll_information_wage)

        lifecycleScope.launchWhenCreated {
            val payroll = presenter.getProjectPayrollData(projectId)
            requiresReport.text = payroll.payrollWeeklyCertifiedPayroll.toGeneralString()
            isUnion.text = payroll.payrollUnionJob.toGeneralString()
            isWageJob.text = payroll.payrollPrevailingWage.toGeneralString()
        }
    }

    companion object {
        const val TAG = "payroll"
        private const val PROJECT_ID = "project_id"

        @JvmStatic
        fun newInstance(projectId: Int) =
            PayrollSheetFragment().apply {
                arguments = bundleOf(PROJECT_ID to projectId)
            }
    }
}
