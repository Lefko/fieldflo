package com.fieldflo.screens.projectDetails.emergencyInfo

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.ProjectEmergencyInfo
import com.fieldflo.usecases.ProjectsUseCases
import javax.inject.Inject

class EmergencyInfoViewModel @Inject constructor(
    private val projectsUseCases: ProjectsUseCases
) : ViewModel() {

    suspend fun getProjectEmergencyInfo(projectId: Int): EmergencyInfoSheetUiModel {
        return projectsUseCases.getProjectEmergencyInfo(projectId).toUiModel()
    }
}

private fun ProjectEmergencyInfo.toUiModel() = EmergencyInfoSheetUiModel(
    fireDepartmentName = fireDepartmentName,
    fireDepartmentAddress = fireDepartmentAddress,
    fireDepartmentCity = fireDepartmentCity,
    fireDepartmentState = fireDepartmentState,
    fireDepartmentZipCode = fireDepartmentZipCode,
    fireDepartmentPhone = fireDepartmentPhone,
    policeDepartmentName = policeDepartmentName,
    policeDepartmentAddress = policeDepartmentAddress,
    policeDepartmentCity = policeDepartmentCity,
    policeDepartmentState = policeDepartmentState,
    policeDepartmentZipCode = policeDepartmentZipCode,
    policeDepartmentPhone = policeDepartmentPhone,
    medicalDepartmentName = medicalDepartmentName,
    medicalDepartmentAddress = medicalDepartmentAddress,
    medicalDepartmentCity = medicalDepartmentCity,
    medicalDepartmentState = medicalDepartmentState,
    medicalDepartmentZipCode = medicalDepartmentZipCode,
    medicalDepartmentPhone = medicalDepartmentPhone
)

data class EmergencyInfoSheetUiModel(
    val fireDepartmentName: String,
    val fireDepartmentAddress: String,
    val fireDepartmentCity: String,
    val fireDepartmentState: String,
    val fireDepartmentZipCode: String,
    val fireDepartmentPhone: String,
    val policeDepartmentName: String,
    val policeDepartmentAddress: String,
    val policeDepartmentCity: String,
    val policeDepartmentState: String,
    val policeDepartmentZipCode: String,
    val policeDepartmentPhone: String,
    val medicalDepartmentName: String,
    val medicalDepartmentAddress: String,
    val medicalDepartmentCity: String,
    val medicalDepartmentState: String,
    val medicalDepartmentZipCode: String,
    val medicalDepartmentPhone: String
)
