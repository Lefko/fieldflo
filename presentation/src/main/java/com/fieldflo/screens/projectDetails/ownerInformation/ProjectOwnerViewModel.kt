package com.fieldflo.screens.projectDetails.ownerInformation

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.ProjectOwnerData
import com.fieldflo.usecases.ProjectsUseCases
import javax.inject.Inject

class ProjectOwnerViewModel @Inject constructor(
    private val projectsUseCases: ProjectsUseCases
) : ViewModel() {

    suspend fun getProjectOwnerData(projectId: Int): OwnerInformationSheetUiModel {
        return projectsUseCases.getProjectOwnerData(projectId).toUiModel()
    }
}

private fun ProjectOwnerData.toUiModel() = OwnerInformationSheetUiModel(
    ownerCompanyName = this.ownerCompanyName,
    ownerAddress = this.ownerAddress,
    ownerCity = this.ownerCity,
    ownerZipCode = this.ownerZipCode,
    ownerNameOfContact = this.ownerNameOfContact,
    ownerPhoneNumber = this.ownerPhoneNumber,
    ownerFaxNumber = this.ownerFaxNumber,
    ownerCellPhone = this.ownerCellPhone,
    ownerStateName = this.ownerState
)

data class OwnerInformationSheetUiModel(
    val ownerCompanyName: String = "",
    val ownerAddress: String = "",
    val ownerCity: String = "",
    val ownerStateName: String = "",
    val ownerZipCode: String = "",
    val ownerNameOfContact: String = "",
    val ownerPhoneNumber: String = "",
    val ownerFaxNumber: String = "",
    val ownerCellPhone: String = ""
)
