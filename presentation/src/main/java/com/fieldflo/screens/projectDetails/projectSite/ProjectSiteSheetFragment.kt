package com.fieldflo.screens.projectDetails.projectSite

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector
import com.fieldflo.screens.projectDetails.BaseSheetFragment
import kotlinx.android.synthetic.main.fragment_base_project_details_sheet.*

class ProjectSiteSheetFragment : BaseSheetFragment() {

    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }
    private val presenter by viewModels<ProjectSiteViewModel> { injector.projectSiteViewModelFactory() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity_project_details_content_certificate_info_titleTV.text =
            getText(R.string.activity_project_site_label)

        val address = addView(R.string.activity_project_site_site_address)
        val crossStreet = addView(R.string.activity_project_site_cross_street)
        val city = addView(R.string.activity_project_site_city)
        val state = addView(R.string.activity_project_site_state)
        val zipCode = addView(R.string.activity_project_site_zip_code)
        val country = addView(R.string.activity_project_site_country_parish)
        val siteContact = addView(R.string.activity_project_site_name_site_contact)
        val phone = addView(R.string.activity_project_site_contract_type)
        val estimatorName = addView(R.string.activity_project_site_site_phone)
        val estimatorPhone = addView(R.string.activity_project_site_estimator)
        val salesPerson = addView(R.string.activity_project_site_estimator_contact_phone)
        val divisionManager = addView(R.string.activity_project_site_salesperson)

        lifecycleScope.launchWhenCreated {
            val site = presenter.getProjectSite(projectId)
            address.text = site.projectSiteAddress
            crossStreet.text = site.projectCrossStreet
            city.text = site.projectCity
            state.text = site.projectStateName
            zipCode.text = site.projectZipCode
            country.text = site.projectCounty
            siteContact.text = site.projectNameOfSiteContact
            phone.text = site.contractType
            estimatorName.text = site.projectSitePhone
            estimatorPhone.text = site.projectEstimator
            salesPerson.text = site.estimatorContactPhone
            divisionManager.text = site.projectSalesPerson
        }
    }

    companion object {
        const val TAG = "project_site"
        private const val PROJECT_ID = "project_id"

        @JvmStatic
        fun newInstance(projectId: Int) =
            ProjectSiteSheetFragment().apply {
                arguments = bundleOf(PROJECT_ID to projectId)
            }
    }
}
