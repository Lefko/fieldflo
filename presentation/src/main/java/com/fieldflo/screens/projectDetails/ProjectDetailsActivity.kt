package com.fieldflo.screens.projectDetails

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.safeClickListener
import com.fieldflo.di.injector
import com.fieldflo.screens.drawer.DrawerManager
import com.fieldflo.screens.projectDetails.airMonitoring.AirMonitoringSpecialistSheetFragment
import com.fieldflo.screens.projectDetails.billing.BillingSheetFragment
import com.fieldflo.screens.projectDetails.buildingInformation.BuildingInformationSheetFragment
import com.fieldflo.screens.projectDetails.consultant.ConsultantSheetFragment
import com.fieldflo.screens.projectDetails.designer.ProjectDesignerSheetFragment
import com.fieldflo.screens.projectDetails.emergencyInfo.EmergencyInfoSheetFragment
import com.fieldflo.screens.projectDetails.generalInfo.GeneralInfoFragment
import com.fieldflo.screens.projectDetails.insuranceCertificate.InsuranceCertificateSheetFragment
import com.fieldflo.screens.projectDetails.lienInfo.LienInformationSheetFragment
import com.fieldflo.screens.projectDetails.other.OtherSheetFragment
import com.fieldflo.screens.projectDetails.ownerInformation.OwnerInformationSheetFragment
import com.fieldflo.screens.projectDetails.payroll.PayrollSheetFragment
import com.fieldflo.screens.projectDetails.perDiem.PerDiemSheetFragment
import com.fieldflo.screens.projectDetails.permits.PermitsFragment
import com.fieldflo.screens.projectDetails.procedures.ProceduresSheetFragment
import com.fieldflo.screens.projectDetails.projectDetails.ProjectDetailsSheetFragment
import com.fieldflo.screens.projectDetails.projectSite.ProjectSiteSheetFragment
import com.fieldflo.usecases.ProjectStatus
import kotlinx.android.synthetic.main.view_activity_project_details_content.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter

class ProjectDetailsActivity : AppCompatActivity(R.layout.activity_project_details) {

    companion object {
        fun start(sender: Activity, projectId: Int) {
            val intent = Intent(sender, ProjectDetailsActivity::class.java).apply {
                putExtra(PROJECT_ID, projectId)
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }

            sender.startActivity(intent)
        }

        private const val PROJECT_ID = "project_id"
    }

    private val drawerManager by lazy {
        DrawerManager(
            { findViewById(R.id.drawer_layout) }, { this },
            { findViewById(R.id.activity_project_details_content_toolbar) }
        )
    }

    private val presenter by viewModels<ProjectDetailsPresenter> { injector.projectDetailsViewModelFactory() }

    private val projectId by lazy { intent!!.extras!!.getInt(PROJECT_ID) }

    private val popup: PopupMenu by lazy {
        PopupMenu(this, activity_project_details_content_expand_iconIV).apply {
            inflate(R.menu.menu_project_details)
            setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.general_info -> showSheet(
                        GeneralInfoFragment.newInstance(projectId),
                        GeneralInfoFragment.TAG
                    )
                    R.id.project_details -> showSheet(
                        ProjectDetailsSheetFragment.newInstance(
                            projectId
                        ), ProjectDetailsSheetFragment.TAG
                    )
                    R.id.project_site -> showSheet(
                        ProjectSiteSheetFragment.newInstance(projectId),
                        ProjectSiteSheetFragment.TAG
                    )
                    R.id.permits -> showSheet(
                        PermitsFragment.newInstance(projectId),
                        PermitsFragment.TAG
                    )
                    R.id.per_diem -> showSheet(
                        PerDiemSheetFragment.newInstance(projectId),
                        PerDiemSheetFragment.TAG
                    )
                    R.id.building_information -> showSheet(
                        BuildingInformationSheetFragment.newInstance(
                            projectId
                        ), BuildingInformationSheetFragment.TAG
                    )
                    R.id.owner_information -> showSheet(
                        OwnerInformationSheetFragment.newInstance(
                            projectId
                        ), OwnerInformationSheetFragment.TAG
                    )
                    R.id.procedures -> showSheet(
                        ProceduresSheetFragment.newInstance(projectId),
                        ProceduresSheetFragment.TAG
                    )
                    R.id.billing_information -> showSheet(
                        BillingSheetFragment.newInstance(projectId),
                        BillingSheetFragment.TAG
                    )
                    R.id.lien_information -> showSheet(
                        LienInformationSheetFragment.newInstance(
                            projectId
                        ), LienInformationSheetFragment.TAG
                    )
                    R.id.project_designer -> showSheet(
                        ProjectDesignerSheetFragment.newInstance(
                            projectId
                        ), ProjectDesignerSheetFragment.TAG
                    )
                    R.id.consultant -> showSheet(
                        ConsultantSheetFragment.newInstance(projectId),
                        ConsultantSheetFragment.TAG
                    )
                    R.id.airmonitoring_specialist -> showSheet(
                        AirMonitoringSpecialistSheetFragment.newInstance(
                            projectId
                        ), AirMonitoringSpecialistSheetFragment.TAG
                    )
                    R.id.insurance_information -> showSheet(
                        InsuranceCertificateSheetFragment.newInstance(
                            projectId
                        ), InsuranceCertificateSheetFragment.TAG
                    )
                    R.id.payroll -> showSheet(
                        PayrollSheetFragment.newInstance(projectId),
                        PayrollSheetFragment.TAG
                    )
                    R.id.emergency_info -> showSheet(
                        EmergencyInfoSheetFragment.newInstance(
                            projectId
                        ), EmergencyInfoSheetFragment.TAG
                    )
                    R.id.other -> showSheet(
                        OtherSheetFragment.newInstance(projectId),
                        OtherSheetFragment.TAG
                    )
                }
                true
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycleScope.launchWhenCreated {
            presenter.getProjectStatusFlow(projectId)
                .filter { it == ProjectStatus.DELETED }
                .collect { finish() }

            activity_project_details_content_project_nameTV.text =
                presenter.getProjectName(projectId)
        }

        activity_project_details_content_backTV.safeClickListener {
            finish()
        }

        activity_project_details_content_expand_iconIV.safeClickListener {
            popup.show()
        }

        activity_project_details_content_previous_sheetTV.safeClickListener {
            goPrev()
        }

        activity_project_details_content_next_sheetTV.safeClickListener {
            goNext()
        }

        showSheet(GeneralInfoFragment.newInstance(projectId), GeneralInfoFragment.TAG)
    }

    override fun onBackPressed() {
        if (!drawerManager.onBackPress()) {
            finish()
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawerManager.onConfigChange(newConfig)
    }

    private fun showSheet(frag: Fragment, tag: String) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.activity_project_details_content_main_viewSV, frag, tag)
            .addToBackStack(tag)
            .commit()

        when (frag) {
            is GeneralInfoFragment -> {
                activity_project_details_content_previous_sheetTV.isEnabled = false
                activity_project_details_content_next_sheetTV.isEnabled = true
            }
            is OtherSheetFragment -> {
                activity_project_details_content_previous_sheetTV.isEnabled = true
                activity_project_details_content_next_sheetTV.isEnabled = false
            }
            else -> {
                activity_project_details_content_previous_sheetTV.isEnabled = true
                activity_project_details_content_next_sheetTV.isEnabled = true
            }
        }
    }

    private fun goPrev() {
        when (supportFragmentManager.findFragmentById(R.id.activity_project_details_content_main_viewSV)) {
            is ProjectDetailsSheetFragment -> showSheet(
                GeneralInfoFragment.newInstance(projectId),
                GeneralInfoFragment.TAG
            )
            is ProjectSiteSheetFragment -> showSheet(
                ProjectDetailsSheetFragment.newInstance(
                    projectId
                ), ProjectDetailsSheetFragment.TAG
            )
            is PermitsFragment -> showSheet(
                ProjectSiteSheetFragment.newInstance(projectId),
                ProjectSiteSheetFragment.TAG
            )
            is PerDiemSheetFragment -> showSheet(
                PermitsFragment.newInstance(projectId),
                PermitsFragment.TAG
            )
            is BuildingInformationSheetFragment -> showSheet(
                PerDiemSheetFragment.newInstance(
                    projectId
                ), PerDiemSheetFragment.TAG
            )
            is OwnerInformationSheetFragment -> showSheet(
                BuildingInformationSheetFragment.newInstance(
                    projectId
                ), BuildingInformationSheetFragment.TAG
            )
            is ProceduresSheetFragment -> showSheet(
                OwnerInformationSheetFragment.newInstance(
                    projectId
                ), OwnerInformationSheetFragment.TAG
            )
            is BillingSheetFragment -> showSheet(
                ProceduresSheetFragment.newInstance(projectId),
                ProceduresSheetFragment.TAG
            )
            is LienInformationSheetFragment -> showSheet(
                BillingSheetFragment.newInstance(projectId),
                BillingSheetFragment.TAG
            )
            is ProjectDesignerSheetFragment -> showSheet(
                LienInformationSheetFragment.newInstance(
                    projectId
                ), LienInformationSheetFragment.TAG
            )
            is ConsultantSheetFragment -> showSheet(
                ProjectDesignerSheetFragment.newInstance(
                    projectId
                ), ProjectDesignerSheetFragment.TAG
            )
            is AirMonitoringSpecialistSheetFragment -> showSheet(
                ConsultantSheetFragment.newInstance(
                    projectId
                ), ConsultantSheetFragment.TAG
            )
            is InsuranceCertificateSheetFragment -> showSheet(
                AirMonitoringSpecialistSheetFragment.newInstance(
                    projectId
                ), AirMonitoringSpecialistSheetFragment.TAG
            )
            is PayrollSheetFragment -> showSheet(
                InsuranceCertificateSheetFragment.newInstance(
                    projectId
                ), InsuranceCertificateSheetFragment.TAG
            )
            is EmergencyInfoSheetFragment -> showSheet(
                PayrollSheetFragment.newInstance(projectId),
                PayrollSheetFragment.TAG
            )
            is OtherSheetFragment -> showSheet(
                EmergencyInfoSheetFragment.newInstance(projectId),
                EmergencyInfoSheetFragment.TAG
            )
        }
    }

    private fun goNext() {
        when (supportFragmentManager.findFragmentById(R.id.activity_project_details_content_main_viewSV)) {
            is GeneralInfoFragment -> showSheet(
                ProjectDetailsSheetFragment.newInstance(projectId),
                ProjectDetailsSheetFragment.TAG
            )
            is ProjectDetailsSheetFragment -> showSheet(
                ProjectSiteSheetFragment.newInstance(
                    projectId
                ), ProjectSiteSheetFragment.TAG
            )
            is ProjectSiteSheetFragment -> showSheet(
                PermitsFragment.newInstance(projectId),
                PermitsFragment.TAG
            )
            is PermitsFragment -> showSheet(
                PerDiemSheetFragment.newInstance(projectId),
                PerDiemSheetFragment.TAG
            )
            is PerDiemSheetFragment -> showSheet(
                BuildingInformationSheetFragment.newInstance(
                    projectId
                ), BuildingInformationSheetFragment.TAG
            )
            is BuildingInformationSheetFragment -> showSheet(
                OwnerInformationSheetFragment.newInstance(
                    projectId
                ), OwnerInformationSheetFragment.TAG
            )
            is OwnerInformationSheetFragment -> showSheet(
                ProceduresSheetFragment.newInstance(
                    projectId
                ), ProceduresSheetFragment.TAG
            )
            is ProceduresSheetFragment -> showSheet(
                BillingSheetFragment.newInstance(projectId),
                BillingSheetFragment.TAG
            )
            is BillingSheetFragment -> showSheet(
                LienInformationSheetFragment.newInstance(projectId),
                LienInformationSheetFragment.TAG
            )
            is LienInformationSheetFragment -> showSheet(
                ProjectDesignerSheetFragment.newInstance(
                    projectId
                ), ProjectDesignerSheetFragment.TAG
            )
            is ProjectDesignerSheetFragment -> showSheet(
                ConsultantSheetFragment.newInstance(
                    projectId
                ), ConsultantSheetFragment.TAG
            )
            is ConsultantSheetFragment -> showSheet(
                AirMonitoringSpecialistSheetFragment.newInstance(
                    projectId
                ), AirMonitoringSpecialistSheetFragment.TAG
            )
            is AirMonitoringSpecialistSheetFragment -> showSheet(
                InsuranceCertificateSheetFragment.newInstance(
                    projectId
                ), InsuranceCertificateSheetFragment.TAG
            )
            is InsuranceCertificateSheetFragment -> showSheet(
                PayrollSheetFragment.newInstance(
                    projectId
                ), PayrollSheetFragment.TAG
            )
            is PayrollSheetFragment -> showSheet(
                EmergencyInfoSheetFragment.newInstance(projectId),
                EmergencyInfoSheetFragment.TAG
            )
            is EmergencyInfoSheetFragment -> showSheet(
                OtherSheetFragment.newInstance(projectId),
                OtherSheetFragment.TAG
            )
        }
    }
}
