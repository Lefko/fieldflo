package com.fieldflo.screens.projectDetails.airMonitoring

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.ProjectAirMonitoringData
import com.fieldflo.usecases.ProjectsUseCases
import javax.inject.Inject

class AirMonitoringSpecialistViewModel @Inject constructor(
    private val projectsUseCases: ProjectsUseCases
) : ViewModel() {
    suspend fun getAirMonitoringData(projectId: Int): AirmonitoringSpecialistSheetUiModel {
        return projectsUseCases.getProjectAirMonitoringData(projectId).toUiModel()
    }
}

private fun ProjectAirMonitoringData.toUiModel() = AirmonitoringSpecialistSheetUiModel(
    airMonitoringFirmName = this.airMonitoringFirmName,
    airMonitoringContact = this.airMonitoringContact,
    airMonitoringAddress = this.airMonitoringAddress,
    airMonitoringCity = this.airMonitoringCity,
    airMonitoringZipCode = this.airMonitoringZipCode,
    airMonitoringPhoneNumber = this.airMonitoringPhoneNumber,
    airMonitoringFaxNumber = this.airMonitoringFaxNumber,
    airMonitoringCell = this.airMonitoringCell,
    airMonitoringCertNumber = this.airMonitoringCertNumber,
    airMonitoringStateName = this.airMonitoringState
)

data class AirmonitoringSpecialistSheetUiModel(
    val airMonitoringFirmName: String,
    val airMonitoringContact: String,
    val airMonitoringAddress: String,
    val airMonitoringCity: String,
    val airMonitoringStateName: String,
    val airMonitoringZipCode: String,
    val airMonitoringPhoneNumber: String,
    val airMonitoringFaxNumber: String,
    val airMonitoringCell: String,
    val airMonitoringCertNumber: String
)
