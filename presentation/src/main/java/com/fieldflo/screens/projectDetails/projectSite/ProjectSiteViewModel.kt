package com.fieldflo.screens.projectDetails.projectSite

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.ProjectSiteData
import com.fieldflo.usecases.ProjectsUseCases
import javax.inject.Inject

class ProjectSiteViewModel @Inject constructor(
    private val projectsUseCases: ProjectsUseCases
) : ViewModel() {

    suspend fun getProjectSite(projectId: Int): ProjectSiteSheetUiModel {
        return projectsUseCases.getProjectSite(projectId).toUiModel()
    }
}

private fun ProjectSiteData.toUiModel() = ProjectSiteSheetUiModel(
    projectSiteAddress = this.projectSiteAddress,
    projectCrossStreet = this.projectCrossStreet,
    projectCity = this.projectCity,
    projectZipCode = this.projectZipCode,
    projectCounty = this.projectCounty,
    projectNameOfSiteContact = this.projectNameOfSiteContact,
    contractType = this.contractType,
    projectSitePhone = this.projectSitePhone,
    projectEstimator = this.estimatorFullName,
    divisionManager = this.divisionManagerFullName,
    estimatorContactPhone = this.estimatorContactPhone,
    projectSalesPerson = this.salePersonFullName,
    projectStateName = this.projectState
)

data class ProjectSiteSheetUiModel(
    val projectSiteAddress: String = "",
    val projectCrossStreet: String = "",
    val projectCity: String = "",
    val projectStateName: String = "",
    val projectZipCode: String = "",
    val projectCounty: String = "",
    val projectNameOfSiteContact: String = "",
    val contractType: String = "",
    val projectSitePhone: String = "",
    val projectEstimator: String = "",
    val divisionManager: String = "",
    val estimatorContactPhone: String = "",
    val projectSalesPerson: String = ""
)
