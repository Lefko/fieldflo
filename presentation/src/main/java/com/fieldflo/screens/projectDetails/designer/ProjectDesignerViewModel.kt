package com.fieldflo.screens.projectDetails.designer

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.ProjectDesignerData
import com.fieldflo.usecases.ProjectsUseCases
import javax.inject.Inject

class ProjectDesignerViewModel @Inject constructor(
    private val projectUseCases: ProjectsUseCases
) : ViewModel() {
    suspend fun getProjectDesignerData(projectId: Int): ProjectDesignerSheetUiModel =
        projectUseCases.getProjectDesignerData(projectId).toUiModel()
}

private fun ProjectDesignerData.toUiModel() = ProjectDesignerSheetUiModel(
    designerFirmName = this.designerFirmName,
    designerContact = this.designerContact,
    designerAddress = this.designerAddress,
    designerCity = this.designerCity,
    designerZipCode = this.designerZipCode,
    designerPhoneNumber = this.designerPhoneNumber,
    designerFaxNumber = this.designerFaxNumber,
    designerCell = this.designerCell,
    designerCertNumber = this.designerCertNumber,
    designerStateName = this.designerState
)

data class ProjectDesignerSheetUiModel(
    val designerFirmName: String = "",
    val designerContact: String = "",
    val designerAddress: String = "",
    val designerCity: String = "",
    val designerStateName: String = "",
    val designerZipCode: String = "",
    val designerPhoneNumber: String = "",
    val designerFaxNumber: String = "",
    val designerCell: String = "",
    val designerCertNumber: String = ""
)
