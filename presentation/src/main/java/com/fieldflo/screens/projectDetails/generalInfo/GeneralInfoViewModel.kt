package com.fieldflo.screens.projectDetails.generalInfo

import androidx.lifecycle.ViewModel
import com.fieldflo.common.toDateString
import com.fieldflo.data.persistence.db.entities.ProjectGeneralInfoData
import com.fieldflo.usecases.ProjectsUseCases
import javax.inject.Inject

class GeneralInfoViewModel @Inject constructor(
    private val projectsUseCases: ProjectsUseCases
) : ViewModel() {

    suspend fun getGeneralInfo(projectId: Int): GeneralInfoViewState {
        return projectsUseCases.getProjectGeneralInfo(projectId).toUiModel()
    }
}

data class GeneralInfoViewState(
    val projectNumber: String,
    val projectName: String,
    val projectManagerFullName: String,
    val supervisorFullName: String,
    val startDate: String,
    val endDate: String
)

private fun ProjectGeneralInfoData.toUiModel() = GeneralInfoViewState(
    projectNumber = this.projectNumber,
    projectName = this.projectName,
    projectManagerFullName = this.projectManagerFullName,
    supervisorFullName = this.supervisorFullName,
    startDate = this.projectStartDate.toDateString(),
    endDate = this.projectEndDate.toDateString()
)
