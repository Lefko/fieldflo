package com.fieldflo.screens.projectDetails.designer

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector
import com.fieldflo.screens.projectDetails.BaseSheetFragment
import kotlinx.android.synthetic.main.fragment_base_project_details_sheet.*

class ProjectDesignerSheetFragment : BaseSheetFragment() {

    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }
    private val presenter by viewModels<ProjectDesignerViewModel> { injector.projectDesignerViewModelFactory() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity_project_details_content_certificate_info_titleTV.text =
            getString(R.string.activity_project_designer_label)

        val designerFirmName = addView(R.string.activity_project_designer_firm_name)
        val contact = addView(R.string.activity_project_designer_contact)
        val address = addView(R.string.activity_project_designer_address)
        val city = addView(R.string.activity_project_designer_city)
        val state = addView(R.string.activity_project_designer_state)
        val zip = addView(R.string.activity_project_designer_zip_code)
        val phone = addView(R.string.activity_project_designer_phone_number)
        val cell = addView(R.string.activity_project_designer_cell)
        val fax = addView(R.string.activity_project_designer_fax_number)
        val cert = addView(R.string.activity_project_designer_cert_number)

        lifecycleScope.launchWhenCreated {
            val projectDesigner = presenter.getProjectDesignerData(projectId)
            designerFirmName.text = projectDesigner.designerFirmName
            contact.text = projectDesigner.designerContact
            address.text = projectDesigner.designerAddress
            city.text = projectDesigner.designerCity
            state.text = projectDesigner.designerStateName
            zip.text = projectDesigner.designerZipCode
            phone.text = projectDesigner.designerPhoneNumber
            cell.text = projectDesigner.designerCell
            fax.text = projectDesigner.designerFaxNumber
            cert.text = projectDesigner.designerCertNumber
        }
    }

    companion object {
        const val TAG = "project_designer"
        private const val PROJECT_ID = "project_id"

        @JvmStatic
        fun newInstance(projectId: Int) =
            ProjectDesignerSheetFragment().apply {
                arguments = bundleOf(PROJECT_ID to projectId)
            }
    }
}
