package com.fieldflo.screens.projectDetails.payroll

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.ProjectPayrollData
import com.fieldflo.usecases.ProjectsUseCases
import javax.inject.Inject

class PayrollViewModel @Inject constructor(
    private val projectsUseCases: ProjectsUseCases
) : ViewModel() {
    suspend fun getProjectPayrollData(projectId: Int): PayrollSheetUiModel =
        projectsUseCases.getProjectPayrollData(projectId).toUiModel()
}

private fun ProjectPayrollData.toUiModel() = PayrollSheetUiModel(
    payrollWeeklyCertifiedPayroll = this.payrollWeeklyCertifiedPayroll,
    payrollUnionJob = this.payrollUnionJob,
    payrollPrevailingWage = this.payrollPrevailingWage
)

data class PayrollSheetUiModel(
    val payrollWeeklyCertifiedPayroll: Boolean = false,
    val payrollUnionJob: Boolean = false,
    val payrollPrevailingWage: Boolean = false
)
