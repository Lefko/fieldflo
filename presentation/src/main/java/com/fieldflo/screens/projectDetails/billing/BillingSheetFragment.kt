package com.fieldflo.screens.projectDetails.billing

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector
import com.fieldflo.screens.projectDetails.BaseSheetFragment
import kotlinx.android.synthetic.main.fragment_base_project_details_sheet.*

class BillingSheetFragment : BaseSheetFragment() {

    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }
    private val presenter by viewModels<ProjectBillingViewModel> { injector.projectBillingViewModelFactory() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity_project_details_content_certificate_info_titleTV.text =
            getString(R.string.activity_project_billing_label)

        val billingCompanyName = addView(R.string.activity_project_billing_company_name)
        val billingPurchaseOrWorkOrderNumber =
            addView(R.string.activity_project_billing_purchase_work)
        val billingTermsOfContact = addView(R.string.activity_project_billing_terms_contact)
        val billingClientContact = addView(R.string.activity_project_billing_client_contact)
        val billingAddress = addView(R.string.activity_project_billing_address)
        val billingCity = addView(R.string.activity_project_billing_city)
        val billingZipCode = addView(R.string.activity_project_billing_state)
        val billingPhoneNumber = addView(R.string.activity_project_billing_zip_code)
        val billingFaxNumber = addView(R.string.activity_project_billing_phone_number)
        val billingStateName = addView(R.string.activity_project_billing_fax_number)

        lifecycleScope.launchWhenCreated {
            val billing = presenter.getProjectBilling(projectId)
            billingCompanyName.text = billing.billingCompanyName
            billingPurchaseOrWorkOrderNumber.text = billing.billingPurchaseOrWorkOrderNumber
            billingTermsOfContact.text = billing.billingTermsOfContact
            billingClientContact.text = billing.billingClientContact
            billingAddress.text = billing.billingAddress
            billingCity.text = billing.billingCity
            billingZipCode.text = billing.billingStateName
            billingPhoneNumber.text = billing.billingZipCode
            billingFaxNumber.text = billing.billingPhoneNumber
            billingStateName.text = billing.billingFaxNumber
        }
    }

    companion object {
        const val TAG = "billing"
        private const val PROJECT_ID = "project_id"

        @JvmStatic
        fun newInstance(projectId: Int) = BillingSheetFragment().apply {
            arguments = bundleOf(PROJECT_ID to projectId)
        }
    }
}
