package com.fieldflo.screens.projectDetails.ownerInformation

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector
import com.fieldflo.screens.projectDetails.BaseSheetFragment
import kotlinx.android.synthetic.main.fragment_base_project_details_sheet.*

class OwnerInformationSheetFragment : BaseSheetFragment() {

    private val projectId: Int by lazy { arguments!!.getInt(PROJECT_ID) }
    private val presenter by viewModels<ProjectOwnerViewModel> { injector.projectOwnerViewModelFactory() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity_project_details_content_certificate_info_titleTV.text =
            getText(R.string.activity_project_owner_information_label)

        val companyId = addView(R.string.activity_project_owner_information_company_name)
        val address = addView(R.string.activity_project_owner_information_address)
        val city = addView(R.string.activity_project_owner_information_city)
        val state = addView(R.string.activity_project_owner_information_state)
        val zip = addView(R.string.activity_project_owner_information_zip_code)
        val name = addView(R.string.activity_project_owner_information_number_contact)
        val phone = addView(R.string.activity_project_owner_information_phone_number)
        val fax = addView(R.string.activity_project_owner_information_fax_number)
        val cell = addView(R.string.activity_project_owner_information_cell_number)

        lifecycleScope.launchWhenCreated {
            val ownerInformation = presenter.getProjectOwnerData(projectId)
            companyId.text = ownerInformation.ownerCompanyName
            address.text = ownerInformation.ownerAddress
            city.text = ownerInformation.ownerCity
            state.text = ownerInformation.ownerStateName
            zip.text = ownerInformation.ownerZipCode
            name.text = ownerInformation.ownerNameOfContact
            phone.text = ownerInformation.ownerPhoneNumber
            fax.text = ownerInformation.ownerFaxNumber
            cell.text = ownerInformation.ownerCellPhone
        }
    }

    companion object {
        const val TAG = "owner_information"
        private const val PROJECT_ID = "project_id"

        @JvmStatic
        fun newInstance(projectId: Int) =
            OwnerInformationSheetFragment().apply {
                arguments = bundleOf(PROJECT_ID to projectId)
            }
    }
}
