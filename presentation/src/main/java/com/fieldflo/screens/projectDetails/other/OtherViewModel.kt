package com.fieldflo.screens.projectDetails.other

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.ProjectOtherData
import com.fieldflo.usecases.ProjectsUseCases
import javax.inject.Inject

class OtherViewModel @Inject constructor(
    private val projectsUseCases: ProjectsUseCases
) : ViewModel() {
    suspend fun getProjectOtherData(projectId: Int) =
        projectsUseCases.getProjectOtherData(projectId).toUiModel()
}

private fun ProjectOtherData.toUiModel() = OtherSheetUiModel(
    customField1 = this.customField1,
    customField2 = this.customField2,
    customField3 = this.customField3,
    customField4 = this.customField4,
    customField5 = this.customField5,
    customField6 = this.customField6
)

data class OtherSheetUiModel(
    val customField1: String = "",
    val customField2: String = "",
    val customField3: String = "",
    val customField4: String = "",
    val customField5: String = "",
    val customField6: String = ""
)
