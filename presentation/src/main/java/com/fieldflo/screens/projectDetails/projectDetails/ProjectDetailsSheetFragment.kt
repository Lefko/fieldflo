package com.fieldflo.screens.projectDetails.projectDetails

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector
import com.fieldflo.screens.projectDetails.BaseSheetFragment
import kotlinx.android.synthetic.main.fragment_base_project_details_sheet.*

class ProjectDetailsSheetFragment : BaseSheetFragment() {

    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }

    private val presenter by viewModels<ProjectDetailsViewModel> { injector.projectDetailsDetailsViewModelFactory() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity_project_details_content_certificate_info_titleTV.text =
            getString(R.string.activity_project_details_project_details_label)
        val branchNumber = addView(R.string.activity_project_details_project_branch_number_label)
        val bidNumber = addView(R.string.activity_project_details_project_bid_num_label)
        val projectType = addView(R.string.activity_project_details_project_type_label)
        val lossType = addView(R.string.activity_project_details_loss_type)
        val nightShift = addView(R.string.activity_project_details_night_shift)
        val shiftLength = addView(R.string.activity_project_details_shift_length)
        val crewSize = addView(R.string.activity_project_details_project_crew_size)
        val supervisorPhone = addView(R.string.activity_project_details_supervisor_contact_phone)
        val administratorFullName = addView(R.string.activity_project_details_administrator)
        val clientName = addView(R.string.activity_project_details_client)
        val scopeOfWork = addView(R.string.activity_project_details_scope_of_work_label)
        lifecycleScope.launchWhenCreated {
            val details = presenter.getProjectDetails(projectId)
            branchNumber.text = details.branchNumber
            bidNumber.text = details.projectBidNumber
            projectType.text = details.projectType
            lossType.text = details.projectLossType
            nightShift.text = details.hasNightShift
            shiftLength.text = details.shiftLength
            crewSize.text = details.projectCrewSize
            supervisorPhone.text = details.supervisorPhone
            administratorFullName.text = details.administratorFullName
            clientName.text = details.clientFullName
            scopeOfWork.text = details.scopeOfWork
        }
    }

    companion object {
        const val TAG = "project_details"
        private const val PROJECT_ID = "project_id"

        @JvmStatic
        fun newInstance(projectId: Int) =
            ProjectDetailsSheetFragment().apply {
                arguments = bundleOf(PROJECT_ID to projectId)
            }
    }
}
