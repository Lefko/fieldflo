package com.fieldflo.screens.projectDetails.permits

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.drawableStart
import com.fieldflo.common.textColor
import com.fieldflo.di.injector
import com.fieldflo.screens.displayPicture.DisplayPictureActivity
import com.fieldflo.screens.projectDetails.BaseSheetFragment
import kotlinx.android.synthetic.main.fragment_base_project_details_sheet.*
import kotlinx.android.synthetic.main.row_project_details_text.view.*

class PermitsFragment : BaseSheetFragment() {

    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }
    private val presenter by viewModels<ProjectPermitsViewModel> { injector.projectPermitsViewModelFactory() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity_project_details_content_certificate_info_titleTV.text =
            getText(R.string.activity_project_details_project_permits_label)

        lifecycleScope.launchWhenCreated {
            presenter.getProjectPermits(projectId).forEach { permit ->
                val v = addView(permit.name, permit.expireDate)
                v.setOnClickListener {
                    DisplayPictureActivity.start(
                        activity!!,
                        permit.permitFileExtension,
                        permit.permitFileLocation,
                        projectId
                    )
                }

                if (permit.expiresWithinThirty) {
                    v.row_project_details_contentTV.textColor(R.color.attention)
                    v.row_project_details_contentTV.drawableStart(R.drawable.ic_expires)
                }
            }
        }
    }

    companion object {
        const val TAG = "project_permits"
        private const val PROJECT_ID = "project_id"

        @JvmStatic
        fun newInstance(projectId: Int) = PermitsFragment().apply {
            arguments = bundleOf(PROJECT_ID to projectId)
        }
    }
}