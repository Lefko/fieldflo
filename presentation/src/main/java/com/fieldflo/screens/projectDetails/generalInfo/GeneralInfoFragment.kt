package com.fieldflo.screens.projectDetails.generalInfo

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector
import kotlinx.android.synthetic.main.fragment_base_project_details_sheet.*
import kotlinx.android.synthetic.main.fragment_project_details_general_info.view.*

class GeneralInfoFragment : Fragment(R.layout.fragment_project_details_general_info) {

    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }

    private val presenter by viewModels<GeneralInfoViewModel> {
        injector.projectDetailsGeneralInfoViewModelFactory()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity_project_details_content_certificate_info_titleTV.setText(R.string.activity_project_details_general_info)
        lifecycleScope.launchWhenCreated {
            val generalInfoData = presenter.getGeneralInfo(projectId)
            render(view, generalInfoData)
        }
    }

    private fun render(view: View, viewState: GeneralInfoViewState) {
        view.row_project_details_general_info_project_number_contentTV.text =
            viewState.projectNumber
        view.row_project_details_general_info_project_name_contentTV.text = viewState.projectName
        view.row_project_details_general_info_project_manager_contentTV.text =
            viewState.projectManagerFullName
        view.row_project_details_general_info_project_supervisor_contentTV.text =
            viewState.supervisorFullName
        view.row_project_details_general_info_project_start_date_contentTV.text =
            viewState.startDate
        view.row_project_details_general_info_project_end_date_contentTV.text = viewState.endDate
    }

    companion object {
        const val TAG = "general_info"
        private const val PROJECT_ID = "project_id"

        @JvmStatic
        fun newInstance(projectId: Int) =
            GeneralInfoFragment().apply {
                arguments = bundleOf(PROJECT_ID to projectId)
            }
    }
}