package com.fieldflo.screens.projectDetails.procedures

import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.ProjectsUseCases
import javax.inject.Inject

class ProceduresViewModel @Inject constructor(
    private val projectsUseCases: ProjectsUseCases
) : ViewModel() {

    suspend fun getProjectProcedures(projectId: Int): String {
        return projectsUseCases.getProjectProcedures(projectId)
    }
}
