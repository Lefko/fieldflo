package com.fieldflo.screens.projectDetails.insuranceCertificate

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.ProjectInsuranceCertificateData
import com.fieldflo.usecases.ProjectsUseCases
import javax.inject.Inject

class InsuranceCertificateViewModel @Inject constructor(
    private val projectsUseCases: ProjectsUseCases
) : ViewModel() {

    suspend fun getProjectInsuranceCertificateData(projectId: Int) =
        projectsUseCases.getProjectInsuranceCertificateData(projectId).toUiModel()
}

private fun ProjectInsuranceCertificateData.toUiModel() = InsuranceCertificateSheetUiModel(
    insuranceAdditionalInsured = this.insuranceAdditionalInsured,
    insuranceAddress = this.insuranceAddress,
    insuranceCity = this.insuranceCity,
    insuranceZipCode = this.insuranceZipCode,
    insurancePrimaryWordingRequest = this.insurancePrimaryWordingRequest,
    insuranceClientProjectNumber = this.insuranceClientProjectNumber,
    insuranceOtherInstructions = this.insuranceOtherInstructions,
    insuranceOtherInstructions2 = this.insuranceOtherInstructions2,
    insuranceOtherInstructions3 = this.insuranceOtherInstructions3,
    insuranceOtherInstructions4 = this.insuranceOtherInstructions4,
    insuranceRegMailOrigToCertHolder = this.insuranceRegMailOrigToCertHolder,
    insuranceFedexOrigToCertHolder = this.insuranceFedexOrigToCertHolder,
    insuranceFaxToClient = this.insuranceFaxToClient,
    insuranceFaxCopy = this.insuranceFaxCopy,
    insuranceStateName = this.insuranceState
)

data class InsuranceCertificateSheetUiModel(
    val insuranceAdditionalInsured: String = "",
    val insuranceAddress: String = "",
    val insuranceCity: String = "",
    val insuranceStateName: String = "",
    val insuranceZipCode: String = "",
    val insurancePrimaryWordingRequest: String = "",
    val insuranceClientProjectNumber: String = "",
    val insuranceOtherInstructions: String = "",
    val insuranceOtherInstructions2: String = "",
    val insuranceOtherInstructions3: String = "",
    val insuranceOtherInstructions4: String = "",
    val insuranceRegMailOrigToCertHolder: String = "",
    val insuranceFedexOrigToCertHolder: String = "",
    val insuranceFaxToClient: String = "",
    val insuranceFaxCopy: String = ""

)
