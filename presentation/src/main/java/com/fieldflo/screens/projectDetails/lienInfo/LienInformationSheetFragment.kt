package com.fieldflo.screens.projectDetails.lienInfo

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector
import com.fieldflo.screens.projectDetails.BaseSheetFragment
import kotlinx.android.synthetic.main.fragment_base_project_details_sheet.*

class LienInformationSheetFragment : BaseSheetFragment() {

    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }
    private val presenter by viewModels<LienInformationViewModel> { injector.lienInformationViewModelFactory() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity_project_details_content_certificate_info_titleTV.text =
            getString(R.string.activity_project_lien_information_label)

        val name = addView(R.string.activity_project_lien_information_lender)
        val address = addView(R.string.activity_project_lien_information_lender_address)
        val city = addView(R.string.activity_project_lien_information_city)
        val state = addView(R.string.activity_project_lien_information_state)
        val zip = addView(R.string.activity_project_lien_information_zip_code)
        val value = addView(R.string.activity_project_lien_information_lender_value)
        val month = addView(R.string.activity_project_lien_information_month)
        val day = addView(R.string.activity_project_lien_information_day)
        val year = addView(R.string.activity_project_lien_information_year)

        lifecycleScope.launchWhenCreated {
            val lienInformation = presenter.getProjectLienInformation(projectId)
            name.text = lienInformation.lienLender
            address.text = lienInformation.lienLenderAddress
            city.text = lienInformation.lienCity
            state.text = lienInformation.lienStateName
            zip.text = lienInformation.lienZipCode
            value.text = lienInformation.lienLenderValue.toString()
            month.text = lienInformation.lienMonth
            day.text = lienInformation.lienDay
            year.text = lienInformation.lienYear
        }
    }

    companion object {
        const val TAG = "lien_information"
        private const val PROJECT_ID = "project_id"

        @JvmStatic
        fun newInstance(projectId: Int) =
            LienInformationSheetFragment().apply {
                arguments = bundleOf(PROJECT_ID to projectId)
            }
    }
}
