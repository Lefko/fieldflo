package com.fieldflo.screens.projectDetails.procedures

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector
import com.fieldflo.screens.projectDetails.BaseSheetFragment
import kotlinx.android.synthetic.main.fragment_base_project_details_sheet.*

class ProceduresSheetFragment : BaseSheetFragment() {

    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }
    private val presenter by viewModels<ProceduresViewModel> { injector.proceduresViewModelFactory() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity_project_details_content_certificate_info_titleTV.text =
            getString(R.string.activity_project_procedures_label)
        val procedures = addView(R.string.activity_project_procedures_label)
        lifecycleScope.launchWhenCreated {
            procedures.text = presenter.getProjectProcedures(projectId)
        }
    }

    companion object {
        const val TAG = "procedures"
        private const val PROJECT_ID = "project_id"

        @JvmStatic
        fun newInstance(projectId: Int) = ProceduresSheetFragment().apply {
            arguments = bundleOf(PROJECT_ID to projectId)
        }
    }
}
