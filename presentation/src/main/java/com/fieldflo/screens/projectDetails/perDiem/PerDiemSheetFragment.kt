package com.fieldflo.screens.projectDetails.perDiem

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.toGeneralString
import com.fieldflo.di.injector
import com.fieldflo.screens.projectDetails.BaseSheetFragment
import kotlinx.android.synthetic.main.fragment_base_project_details_sheet.*

class PerDiemSheetFragment : BaseSheetFragment() {

    private val presenter by viewModels<ProjectPerDiemViewModel> { injector.projectPerDiemViewModelFactory() }
    private val projectId: Int by lazy { arguments!!.getInt(PROJECT_ID) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity_project_details_content_certificate_info_titleTV.text =
            getString(R.string.activity_project_per_diem_label)

        val perDiemLabel = addView(R.string.activity_project_per_diem_label)
        val perDiemRequired = addView(R.string.activity_project_per_diem_require)
        val dailyHourly = addView(R.string.activity_project_per_diem_daily_hourly)
        val amount = addView(R.string.activity_project_daily_per_diem_amount)
        val notes = addView(R.string.activity_project_per_diem_notes)
        val autoApply = addView(R.string.activity_project_per_diem_auto_apply)
        lifecycleScope.launchWhenCreated {
            val perDiem = presenter.getProjectPerDiem(projectId)
            perDiemLabel.text = perDiem.requiresPerDiem.toGeneralString()
            perDiemRequired.text = perDiem.perDiemType
            dailyHourly.text = perDiem.perDiemHourlyAmount.toString()
            amount.text = perDiem.perDiemDailyAmount.toString()
            notes.text = perDiem.perDiemNotes
            autoApply.text = perDiem.perDiemAutoApplyToTimesheet.toGeneralString()
        }

    }

    companion object {
        const val TAG = "per_diem"
        private const val PROJECT_ID = "project_id"

        @JvmStatic
        fun newInstance(projectId: Int) =
            PerDiemSheetFragment().apply {
                arguments = bundleOf(PROJECT_ID to projectId)
            }
    }
}
