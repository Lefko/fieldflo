package com.fieldflo.screens.projectDetails.other

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector
import com.fieldflo.screens.projectDetails.BaseSheetFragment
import kotlinx.android.synthetic.main.fragment_base_project_details_sheet.*

class OtherSheetFragment : BaseSheetFragment() {

    private val presenter by viewModels<OtherViewModel> { injector.otherViewModelFactory() }
    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity_project_details_content_certificate_info_titleTV.text =
            getString(R.string.activity_project_other_label)

        val custom1 = addView(R.string.activity_project_other_custom_field_1)
        val custom2 = addView(R.string.activity_project_other_custom_field_2)
        val custom3 = addView(R.string.activity_project_other_custom_field_3)
        val custom4 = addView(R.string.activity_project_other_custom_field_4)
        val custom5 = addView(R.string.activity_project_other_custom_field_5)
        val custom6 = addView(R.string.activity_project_other_custom_field_6)

        lifecycleScope.launchWhenCreated {
            val others = presenter.getProjectOtherData(projectId)
            custom1.text = others.customField1
            custom2.text = others.customField2
            custom3.text = others.customField3
            custom4.text = others.customField4
            custom5.text = others.customField5
            custom6.text = others.customField6
        }
    }

    companion object {
        const val TAG = "other"
        private const val PROJECT_ID = "project_id"

        @JvmStatic
        fun newInstance(projectId: Int) =
            OtherSheetFragment().apply {
                arguments = bundleOf(PROJECT_ID to projectId)
            }
    }
}
