package com.fieldflo.screens.projectDetails.airMonitoring

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector
import com.fieldflo.screens.projectDetails.BaseSheetFragment
import kotlinx.android.synthetic.main.fragment_base_project_details_sheet.*

class AirMonitoringSpecialistSheetFragment : BaseSheetFragment() {

    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }
    private val presenter by viewModels<AirMonitoringSpecialistViewModel> { injector.airMonitoringSpecialistViewModelFactory() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity_project_details_content_certificate_info_titleTV.text =
            getText(R.string.activity_project_airmonitoring_label)

        val firmName = addView(R.string.activity_project_designer_firm_name)
        val contactName = addView(R.string.activity_project_designer_contact)
        val address = addView(R.string.activity_project_designer_address)
        val city = addView(R.string.activity_project_designer_city)
        val state = addView(R.string.activity_project_designer_state)
        val zip = addView(R.string.activity_project_designer_zip_code)
        val phone = addView(R.string.activity_project_designer_phone_number)
        val cell = addView(R.string.activity_project_designer_cell)
        val fax = addView(R.string.activity_project_designer_fax_number)
        val cert = addView(R.string.activity_project_designer_cert_number)

        lifecycleScope.launchWhenCreated {
            val airmonitoringSpecialist = presenter.getAirMonitoringData(projectId)
            firmName.text = airmonitoringSpecialist.airMonitoringFirmName
            contactName.text = airmonitoringSpecialist.airMonitoringContact
            address.text = airmonitoringSpecialist.airMonitoringAddress
            city.text = airmonitoringSpecialist.airMonitoringCity
            state.text = airmonitoringSpecialist.airMonitoringStateName
            zip.text = airmonitoringSpecialist.airMonitoringZipCode
            phone.text = airmonitoringSpecialist.airMonitoringPhoneNumber
            cell.text = airmonitoringSpecialist.airMonitoringCell
            fax.text = airmonitoringSpecialist.airMonitoringFaxNumber
            cert.text = airmonitoringSpecialist.airMonitoringCertNumber
        }
    }

    companion object {
        const val TAG = "airmonitoring"
        private const val PROJECT_ID = "project_id"

        @JvmStatic
        fun newInstance(projectId: Int) =
            AirMonitoringSpecialistSheetFragment().apply {
                arguments = bundleOf(PROJECT_ID to projectId)
            }
    }
}
