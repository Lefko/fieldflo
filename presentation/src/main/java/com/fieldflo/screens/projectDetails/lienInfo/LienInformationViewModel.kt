package com.fieldflo.screens.projectDetails.lienInfo

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.ProjectLienInfo
import com.fieldflo.usecases.ProjectsUseCases
import javax.inject.Inject

class LienInformationViewModel @Inject constructor(
    private val projectsUseCases: ProjectsUseCases
) : ViewModel() {

    suspend fun getProjectLienInformation(projectId: Int): LienInformationSheetUiModel {
        return projectsUseCases.getProjectLienInformation(projectId).toUiModel()
    }
}

private fun ProjectLienInfo.toUiModel() = LienInformationSheetUiModel(
    lienLender = this.lienLender,
    lienLenderAddress = this.lienLenderAddress,
    lienCity = this.lienCity,
    lienZipCode = this.lienZipCode,
    lienLenderValue = this.lienLenderValue,
    lienMonth = this.lienMonth,
    lienDay = this.lienDay,
    lienYear = this.lienYear,
    lienStateName = this.lienState
)

data class LienInformationSheetUiModel(
    val lienLender: String = "",
    val lienLenderAddress: String = "",
    val lienCity: String = "",
    val lienStateName: String = "",
    val lienZipCode: String = "",
    val lienLenderValue: Double = 0.0,
    val lienMonth: String = "",
    val lienDay: String = "",
    val lienYear: String = ""
)
