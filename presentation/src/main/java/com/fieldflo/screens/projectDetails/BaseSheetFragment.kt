package com.fieldflo.screens.projectDetails

import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.fieldflo.R
import kotlinx.android.synthetic.main.fragment_base_project_details_sheet.*

open class BaseSheetFragment : Fragment(R.layout.fragment_base_project_details_sheet) {

    fun addView(@StringRes labelId: Int, value: String?): View {
        val rowView = LayoutInflater.from(requireContext()).inflate(
            R.layout.row_project_details_text,
            fragment_project_details_content_mainLL,
            false
        )

        rowView.let {
            val labelTV =
                it.findViewById<TextView>(R.id.row_project_details_content_project_labelTV)
            labelTV.setText(labelId)

            val contentTV = it.findViewById<TextView>(R.id.row_project_details_contentTV)
            contentTV.text = value

            fragment_project_details_content_mainLL.addView(it)
        }

        return rowView
    }

    fun addView(@StringRes labelId: Int): TextView {
        val rowView = LayoutInflater.from(requireContext()).inflate(
            R.layout.row_project_details_text,
            fragment_project_details_content_mainLL,
            false
        )


        val labelTV =
            rowView.findViewById<TextView>(R.id.row_project_details_content_project_labelTV)
        labelTV.setText(labelId)

        val contentTV = rowView.findViewById<TextView>(R.id.row_project_details_contentTV)
        contentTV.text = null

        fragment_project_details_content_mainLL.addView(rowView)

        return contentTV
    }

    fun addView(label: String, value: String?): View {
        val rowView = LayoutInflater.from(requireContext()).inflate(
            R.layout.row_project_details_text,
            fragment_project_details_content_mainLL,
            false
        )

        rowView.let {
            val labelTV =
                it.findViewById<TextView>(R.id.row_project_details_content_project_labelTV)
            labelTV.text = label

            val contentTV = it.findViewById<TextView>(R.id.row_project_details_contentTV)
            contentTV.text = value

            fragment_project_details_content_mainLL.addView(it)
        }

        return rowView
    }
}
