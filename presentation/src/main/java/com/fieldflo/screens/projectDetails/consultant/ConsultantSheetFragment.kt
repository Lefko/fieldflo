package com.fieldflo.screens.projectDetails.consultant

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector
import com.fieldflo.screens.projectDetails.BaseSheetFragment
import kotlinx.android.synthetic.main.fragment_base_project_details_sheet.*

class ConsultantSheetFragment : BaseSheetFragment() {

    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }
    private val presenter by viewModels<ConsultantViewModel> { injector.consultantViewModelFactory() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity_project_details_content_certificate_info_titleTV.text =
            getString(R.string.activity_project_consultant_label)

        val firmName = addView(R.string.activity_project_designer_firm_name)
        val contactName = addView(R.string.activity_project_designer_contact)
        val address = addView(R.string.activity_project_designer_address)
        val city = addView(R.string.activity_project_designer_city)
        val state = addView(R.string.activity_project_designer_state)
        val zip = addView(R.string.activity_project_designer_zip_code)
        val phone = addView(R.string.activity_project_designer_phone_number)
        val fax = addView(R.string.activity_project_designer_fax_number)

        lifecycleScope.launchWhenCreated {
            val consultant = presenter.getConsultantData(projectId)
            firmName.text = consultant.consultantFirmName
            contactName.text = consultant.consultantContact
            address.text = consultant.consultantAddress
            city.text = consultant.consultantCity
            state.text = consultant.consultantStateName
            zip.text = consultant.consultantZipCode
            phone.text = consultant.consultantPhoneNumber
            fax.text = consultant.consultantFaxNumber
        }
    }

    companion object {
        const val TAG = "consultant"
        private const val PROJECT_ID = "project_id"

        @JvmStatic
        fun newInstance(projectId: Int) =
            ConsultantSheetFragment().apply {
                arguments = bundleOf(PROJECT_ID to projectId)
            }
    }
}
