package com.fieldflo.screens.projectDetails.insuranceCertificate

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector
import com.fieldflo.screens.projectDetails.BaseSheetFragment
import kotlinx.android.synthetic.main.fragment_base_project_details_sheet.*

class InsuranceCertificateSheetFragment : BaseSheetFragment() {

    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }
    private val presenter by viewModels<InsuranceCertificateViewModel> { injector.insuranceCertificateViewModelFactory() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity_project_details_content_certificate_info_titleTV.text =
            getString(R.string.activity_project_insurance_cert_label)

        val additionalInsured = addView(R.string.activity_project_insurance_cert_additional_insured)
        val addressException = addView(R.string.activity_project_insurance_cert_address)
        val city = addView(R.string.activity_project_insurance_cert_city)
        val state = addView(R.string.activity_project_insurance_cert_state)
        val zip = addView(R.string.activity_project_insurance_cert_zip_code)
        val wording = addView(R.string.activity_project_insurance_cert_primary_wording)
        val clientProject = addView(R.string.activity_project_insurance_cert_project_number)
        val special1 = addView(R.string.activity_project_insurance_cert_other_special)
        val special2 = addView(R.string.activity_project_insurance_cert_other_special)
        val special3 = addView(R.string.activity_project_insurance_cert_other_special)
        val special4 = addView(R.string.activity_project_insurance_cert_other_special)
        val regMail = addView(R.string.activity_project_insurance_cert_reg_mail_holder)
        val fedex = addView(R.string.activity_project_insurance_cert_fedex_holder)
        val faxClient = addView(R.string.activity_project_insurance_cert_fax_client)
        val faxInsured = addView(R.string.activity_project_insurance_cert_mail_insured)

        lifecycleScope.launchWhenCreated {
            val insuranceCertificate = presenter.getProjectInsuranceCertificateData(projectId)
            additionalInsured.text = insuranceCertificate.insuranceAdditionalInsured
            addressException.text = insuranceCertificate.insuranceAddress
            city.text = insuranceCertificate.insuranceCity
            state.text = insuranceCertificate.insuranceStateName
            zip.text = insuranceCertificate.insuranceZipCode
            wording.text = insuranceCertificate.insurancePrimaryWordingRequest
            clientProject.text = insuranceCertificate.insuranceClientProjectNumber
            special1.text = insuranceCertificate.insuranceOtherInstructions
            special2.text = insuranceCertificate.insuranceOtherInstructions2
            special3.text = insuranceCertificate.insuranceOtherInstructions3
            special4.text = insuranceCertificate.insuranceOtherInstructions4
            regMail.text = insuranceCertificate.insuranceRegMailOrigToCertHolder
            fedex.text = insuranceCertificate.insuranceFedexOrigToCertHolder
            faxClient.text = insuranceCertificate.insuranceFaxToClient
            faxInsured.text = insuranceCertificate.insuranceRegMailOrigToCertHolder
        }
    }

    companion object {
        const val TAG = "insurance"
        private const val PROJECT_ID = "project_id"

        @JvmStatic
        fun newInstance(projectId: Int) =
            InsuranceCertificateSheetFragment().apply {
                arguments = bundleOf(PROJECT_ID to projectId)
            }
    }
}
