package com.fieldflo.screens.projectDetails.consultant

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.ProjectConsultantData
import com.fieldflo.usecases.ProjectsUseCases
import javax.inject.Inject

class ConsultantViewModel @Inject constructor(
    private val projectsUseCases: ProjectsUseCases
) : ViewModel() {

    suspend fun getConsultantData(projectId: Int): ConsultantSheetUiModel =
        projectsUseCases.getProjectConsultantData(projectId).toUiModel()
}

private fun ProjectConsultantData.toUiModel() = ConsultantSheetUiModel(
    consultantFirmName = this.consultantFirmName,
    consultantContact = this.consultantContact,
    consultantAddress = this.consultantAddress,
    consultantCity = this.consultantCity,
    consultantZipCode = this.consultantZipCode,
    consultantPhoneNumber = this.consultantPhoneNumber,
    consultantFaxNumber = this.consultantFaxNumber,
    consultantStateName = this.consultantState
)

data class ConsultantSheetUiModel(
    val consultantFirmName: String = "",
    val consultantContact: String = "",
    val consultantAddress: String = "",
    val consultantCity: String = "",
    val consultantStateName: String = "",
    val consultantZipCode: String = "",
    val consultantPhoneNumber: String = "",
    val consultantFaxNumber: String = ""
)
