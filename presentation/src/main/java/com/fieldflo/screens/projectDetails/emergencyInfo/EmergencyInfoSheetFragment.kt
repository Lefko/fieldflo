package com.fieldflo.screens.projectDetails.emergencyInfo

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector
import com.fieldflo.screens.projectDetails.BaseSheetFragment
import kotlinx.android.synthetic.main.fragment_base_project_details_sheet.*

class EmergencyInfoSheetFragment : BaseSheetFragment() {

    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }
    private val presenter by viewModels<EmergencyInfoViewModel> { injector.emergencyInfoViewModelFactory() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity_project_details_content_certificate_info_titleTV.text =
            getString(R.string.activity_project_emergency_info_label)

        val fireName = addView(R.string.activity_project_emergency_info_fire_department)
        val fireAddress = addView(R.string.activity_project_emergency_info_address)
        val fireCity = addView(R.string.activity_project_emergency_info_city)
        val fireState = addView(R.string.activity_project_emergency_info_state)
        val fireZip = addView(R.string.activity_project_emergency_info_zip_code)
        val firePhone = addView(R.string.activity_project_emergency_info_phone_number)
        val policeName = addView(R.string.activity_project_emergency_info_police_department)
        val policeAddress = addView(R.string.activity_project_emergency_info_address)
        val policeCity = addView(R.string.activity_project_emergency_info_city)
        val policeState = addView(R.string.activity_project_emergency_info_state)
        val policeZip = addView(R.string.activity_project_emergency_info_zip_code)
        val policePhone = addView(R.string.activity_project_emergency_info_phone_number)
        val medName = addView(R.string.activity_project_emergency_info_medical_hotpital)
        val medAddress = addView(R.string.activity_project_emergency_info_address)
        val medCity = addView(R.string.activity_project_emergency_info_city)
        val medState = addView(R.string.activity_project_emergency_info_state)
        val medZip = addView(R.string.activity_project_emergency_info_zip_code)
        val medPhone = addView(R.string.activity_project_emergency_info_phone_number)

        lifecycleScope.launchWhenCreated {
            val emergencyInfo = presenter.getProjectEmergencyInfo(projectId)
            fireName.text = emergencyInfo.fireDepartmentName
            fireAddress.text = emergencyInfo.fireDepartmentAddress
            fireCity.text = emergencyInfo.fireDepartmentCity
            fireState.text = emergencyInfo.fireDepartmentState
            fireZip.text = emergencyInfo.fireDepartmentZipCode
            firePhone.text = emergencyInfo.fireDepartmentPhone
            policeName.text = emergencyInfo.policeDepartmentName
            policeAddress.text = emergencyInfo.policeDepartmentAddress
            policeCity.text = emergencyInfo.policeDepartmentCity
            policeState.text = emergencyInfo.policeDepartmentState
            policeZip.text = emergencyInfo.policeDepartmentZipCode
            policePhone.text = emergencyInfo.policeDepartmentPhone
            medName.text = emergencyInfo.medicalDepartmentName
            medAddress.text = emergencyInfo.medicalDepartmentAddress
            medCity.text = emergencyInfo.medicalDepartmentCity
            medState.text = emergencyInfo.medicalDepartmentState
            medZip.text = emergencyInfo.medicalDepartmentZipCode
            medPhone.text = emergencyInfo.medicalDepartmentPhone
        }
    }

    companion object {
        const val TAG = "emergency_info"
        private const val PROJECT_ID = "project_id"

        @JvmStatic
        fun newInstance(projectId: Int) =
            EmergencyInfoSheetFragment().apply {
                arguments = bundleOf(PROJECT_ID to projectId)
            }
    }
}
