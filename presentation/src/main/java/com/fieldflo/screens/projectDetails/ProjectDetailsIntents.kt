package com.fieldflo.screens.projectDetails

sealed class ProjectDetailsIntents {
    data class GetProjectDetailsIntent(val projectId: Int): ProjectDetailsIntents()
    class PreviousSheetIntent: ProjectDetailsIntents()
    class NextSheetIntent: ProjectDetailsIntents()
    data class SelectSheetIntent(val selectedSheet: Int): ProjectDetailsIntents()
}
