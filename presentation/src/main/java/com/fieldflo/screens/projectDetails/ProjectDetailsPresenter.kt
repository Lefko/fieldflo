package com.fieldflo.screens.projectDetails

import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.ProjectStatus
import com.fieldflo.usecases.ProjectsUseCases
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ProjectDetailsPresenter @Inject constructor(
    private val projectsUseCases: ProjectsUseCases
) : ViewModel() {

    suspend fun getProjectName(projectId: Int) = projectsUseCases.getProjectName(projectId)

    fun getProjectStatusFlow(projectId: Int): Flow<ProjectStatus> {
        return projectsUseCases.getProjectStatusFlow(projectId)
    }
}