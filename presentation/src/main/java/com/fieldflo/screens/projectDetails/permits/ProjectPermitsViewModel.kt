package com.fieldflo.screens.projectDetails.permits

import androidx.lifecycle.ViewModel
import com.fieldflo.common.toSimpleString
import com.fieldflo.usecases.ProjectPermitData
import com.fieldflo.usecases.ProjectsUseCases
import javax.inject.Inject

class ProjectPermitsViewModel @Inject constructor(
    private val projectsUseCases: ProjectsUseCases
) : ViewModel() {

    suspend fun getProjectPermits(projectId: Int): List<PermitUiModel> {
        return projectsUseCases.getProjectPermits(projectId).map { it.toUiModel() }
    }
}

private fun ProjectPermitData.toUiModel() = PermitUiModel(
    name = this.permitType,
    expireDate = this.expireDate.toSimpleString(),
    expiresWithinThirty = this.expiresWithinThirty,
    permitFileLocation = this.permitFileLocation,
    permitFileExtension = this.permitFileExtension
)

data class PermitUiModel(
    val name: String = "",
    val expireDate: String = "",
    val expiresWithinThirty: Boolean = false,
    val permitFileLocation: String = "",
    val permitFileExtension: String = ""
)
