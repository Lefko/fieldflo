package com.fieldflo.screens.projectDetails.projectDetails

import androidx.lifecycle.ViewModel
import com.fieldflo.common.StringProvider
import com.fieldflo.common.toGeneralString
import com.fieldflo.data.persistence.db.entities.ProjectDetailsData
import com.fieldflo.usecases.ProjectsUseCases
import javax.inject.Inject

class ProjectDetailsViewModel @Inject constructor(
    private val projectsUseCases: ProjectsUseCases,
    private val stringProvider: StringProvider
) : ViewModel() {

    suspend fun getProjectDetails(projectId: Int): ProjectDetailsViewState {
        return projectsUseCases.getProjectDetails(projectId).toUiModel(stringProvider)
    }
}

private fun ProjectDetailsData.toUiModel(stringProvider: StringProvider) = ProjectDetailsViewState(
    branchNumber = this.branchNumber,
    projectBidNumber = this.projectBidNumber,
    projectType = this.projectTypeName,
    projectLossType = this.projectLossTypeName,
    hasNightShift = this.hasNightShift.toGeneralString(),
    shiftLength = stringProvider.getShiftLengthMessage(this.shiftLength),
    projectCrewSize = this.projectCrewSize.toString(),
    supervisorPhone = this.supervisorPhone,
    administratorFullName = this.projectAdministratorFullName,
    clientFullName = this.clientFullName,
    scopeOfWork = this.scopeOfWork
)

data class ProjectDetailsViewState(
    val branchNumber: String = "",
    val projectBidNumber: String = "",
    val projectType: String = "",
    val projectLossType: String = "",
    val hasNightShift: String = "",
    val shiftLength: String = "",
    val projectCrewSize: String = "",
    val supervisorPhone: String = "",
    val administratorFullName: String = "",
    val clientFullName: String = "",
    val scopeOfWork: String = ""
)
