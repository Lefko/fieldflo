package com.fieldflo.screens.formDailyFieldReport

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.fieldflo.R

class RequestPermissionRationalDialog : DialogFragment() {

    private lateinit var callback: RequestPermissionRationalCallback

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(activity as Context)
            .setMessage(R.string.activity_field_report_get_permission)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                callback.startPermissionRequest()
            }
            .show()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    private fun onAttachToContext(context: Context) {
        this.callback = context as? RequestPermissionRationalCallback
            ?: throw IllegalArgumentException(
                "${context.javaClass.simpleName} must be an instance of RequestPermissionRationalCallback"
            )
    }

    companion object {
        const val TAG = "RequestPermissionRationalDialog"
    }
}

interface RequestPermissionRationalCallback {
    fun startPermissionRequest()
}
