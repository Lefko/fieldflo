package com.fieldflo.screens.formDailyFieldReport

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import com.fieldflo.R

class ImagePickerOptionsDialog: AppCompatDialogFragment() {

    private lateinit var listener: PickerOptionListener
    private val imageNumber by lazy {
        arguments!!.getInt(IMAGE_NUMBER)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    private fun onAttachToContext(context: Context) {
        if (context is PickerOptionListener) {
            listener = context
        } else {
            throw IllegalArgumentException("${context.javaClass.simpleName} must implement PickerOptionListener")
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(context!!)
            .setItems(R.array.activity_daily_form_picker_options_array) { _, which ->
                when(which) {
                    0 -> listener.onTakeCameraSelected(imageNumber)
                    1 -> listener.onChooseGallerySelected(imageNumber)
                }
            }
            .show()
    }

    companion object {
        const val TAG = "ImagePickerOptionsDialog"
        private const val IMAGE_NUMBER = "image_number_key"

        fun newInstance(imageNumber: Int) = ImagePickerOptionsDialog().apply {
            arguments = Bundle().apply {
                putInt(IMAGE_NUMBER, imageNumber)
            }
        }
    }

    interface PickerOptionListener {
        fun onTakeCameraSelected(imageNumber: Int)

        fun onChooseGallerySelected(imageNumber: Int)
    }
}