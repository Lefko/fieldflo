package com.fieldflo.screens.formDailyFieldReport

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.Spinner
import android.widget.TextView
import androidx.activity.viewModels
import androidx.annotation.IntRange
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.fieldflo.R
import com.fieldflo.common.*
import com.fieldflo.common.dateTimePickers.DatePickerDialog
import com.fieldflo.di.injector
import com.fieldflo.screens.drawer.DrawerManager
import com.fieldflo.screens.imagePicker.ImagePickerActivity
import com.fieldflo.screens.imagePicker.ImagePickerActivity.Companion.RESULT_IMAGE_PATH
import com.fieldflo.screens.selectEmployee.EmployeeUiRow
import com.fieldflo.screens.selectEmployee.SelectEmployeeDialog
import com.fieldflo.screens.selectSupervisor.SelectSupervisorDialog
import com.fieldflo.screens.selectSupervisor.SupervisorEmployeeUiRow
import com.fieldflo.usecases.DailyFieldReportUseCases
import com.fieldflo.usecases.ProjectStatus
import kotlinx.android.synthetic.main.activity_daily_form.*
import kotlinx.android.synthetic.main.view_activity_daily_field_report_form.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import java.io.File
import java.util.*

class FormDailyFieldReportActivity : AppCompatActivity(R.layout.activity_daily_form),
    ImagePickerOptionsDialog.PickerOptionListener,
    SelectEmployeeDialog.EmployeeSelectedCallback,
    SelectSupervisorDialog.SupervisorSelectedCallback,
    RequestPermissionRationalCallback {

    companion object {

        private const val INTERNAL_FORM_ID_KEY = "internal_form_id"
        private const val PROJECT_ID_KEY = "project_id"
        private const val FORM_TYPE_NAME_KEY = "form_type_name"
        const val CHANGE_FIELD_REPORT_PROJECT_MANAGER_IDENTIFIER = 2
        const val REQUEST_IMAGE = 1337

        private const val PERMISSIONS_REQUEST_CODE = 1339
        private val requiredPermissions = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )

        fun start(sender: Activity, formTypeName: String, internalFormId: Int, projectId: Int) {
            val intent = Intent(sender, FormDailyFieldReportActivity::class.java).apply {
                putExtra(FORM_TYPE_NAME_KEY, formTypeName)
                putExtra(INTERNAL_FORM_ID_KEY, internalFormId)
                putExtra(PROJECT_ID_KEY, projectId)
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            sender.startActivity(intent)
        }
    }

    private val internalFormId: Int by lazy { intent.extras!!.getInt(INTERNAL_FORM_ID_KEY, 0) }
    private val projectId: Int by lazy { intent.extras!!.getInt(PROJECT_ID_KEY, 0) }
    private val formTypeName: String by lazy { intent.extras!!.getString(FORM_TYPE_NAME_KEY, "") }
    private val presenter by viewModels<FormDailyFieldReportPresenter> { injector.dailyFormViewModelFactory() }
    private val drawerManager by lazy {
        DrawerManager({ drawer_layout }, { this },
            { activity_daily_form_content_toolbar })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initSpinners()

        initSupervisorClickListener()

        initProjectManagerClickListener()

        addDateClickListeners()

        initReportableIncident()

        initStopWorkPerformed()

        initSelectPicClickListener()

        initDeletePicClickListener()

        lifecycleScope.launchWhenCreated {
            presenter.getProjectStatusFlow(projectId)
                .filter { it == ProjectStatus.DELETED }
                .collect { finish() }
        }

        bind()

        activity_daily_form_content_backTV.safeClickListener { finish() }
        activity_daily_form_save_buttonTV.safeClickListener { finish() }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawerManager.onConfigChange(newConfig)
    }

    override fun onBackPressed() {
        if (!drawerManager.onBackPress()) {
            super.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        drawerManager.refreshSelected()
    }

    override fun onDestroy() {
        presenter.saveForm(projectId, internalFormId, createDailyReportForm())
        super.onDestroy()
    }

    override fun onEmployeeSelected(
        employeeIdentifier: Int,
        employee: EmployeeUiRow
    ) {
        when (employeeIdentifier) {
            CHANGE_FIELD_REPORT_PROJECT_MANAGER_IDENTIFIER ->
                presenter.projectManagerChanged(employee)
        }
    }

    override fun onSupervisorSelected(employee: SupervisorEmployeeUiRow) {
        presenter.supervisorChanged(employee)
    }

    private fun initSpinners() {
        activity_daily_form_scope_received_spinner.adapter = ArrayAdapter.createFromResource(
            this,
            R.array.activity_daily_form_yes_no_items,
            R.layout.row_spinner_dropdown
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }

        activity_daily_form_structure_spinner.adapter = ArrayAdapter.createFromResource(
            this,
            R.array.activity_daily_form_structure_items,
            R.layout.row_spinner_dropdown
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }

        activity_daily_form_reportable_incident_spinner.adapter = ArrayAdapter.createFromResource(
            this,
            R.array.activity_daily_form_reportable_incident_items,
            R.layout.row_spinner_dropdown
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }

        activity_daily_form_acm_detected_spinner.adapter = ArrayAdapter.createFromResource(
            this,
            R.array.activity_daily_form_yes_no_items,
            R.layout.row_spinner_dropdown
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }

        activity_daily_form_lead_detected_spinner.adapter = ArrayAdapter.createFromResource(
            this,
            R.array.activity_daily_form_yes_no_items,
            R.layout.row_spinner_dropdown
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }

        activity_daily_form_mold_detected_spinner.adapter = ArrayAdapter.createFromResource(
            this,
            R.array.activity_daily_form_yes_no_items,
            R.layout.row_spinner_dropdown
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }

        activity_daily_form_work_area_locations1.adapter = ArrayAdapter.createFromResource(
            this,
            R.array.activity_daily_form_work_area_locations_items,
            R.layout.row_spinner_dropdown
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }

        activity_daily_form_work_area_locations2.adapter = ArrayAdapter.createFromResource(
            this,
            R.array.activity_daily_form_work_area_locations_items,
            R.layout.row_spinner_dropdown
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }

        activity_daily_form_work_area_locations3.adapter = ArrayAdapter.createFromResource(
            this,
            R.array.activity_daily_form_work_area_locations_items,
            R.layout.row_spinner_dropdown
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }

        activity_daily_form_cleaning_verification1.adapter = ArrayAdapter.createFromResource(
            this,
            R.array.activity_daily_form_cleaning_verification_items,
            R.layout.row_spinner_dropdown
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }

        activity_daily_form_cleaning_verification2.adapter = ArrayAdapter.createFromResource(
            this,
            R.array.activity_daily_form_cleaning_verification_items,
            R.layout.row_spinner_dropdown
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }

        activity_daily_form_cleaning_verification3.adapter = ArrayAdapter.createFromResource(
            this,
            R.array.activity_daily_form_cleaning_verification_items,
            R.layout.row_spinner_dropdown
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
    }

    private fun initSupervisorClickListener() {
        activity_daily_form_supervisor.setOnClickListener {
            SelectSupervisorDialog.newInstance()
                .show(supportFragmentManager, SelectEmployeeDialog.TAG)
        }
    }

    private fun initProjectManagerClickListener() {
        activity_daily_form_project_managerTV.setOnClickListener {
            SelectEmployeeDialog.newInstance(CHANGE_FIELD_REPORT_PROJECT_MANAGER_IDENTIFIER)
                .show(supportFragmentManager, SelectEmployeeDialog.TAG)
        }
    }

    private fun addDateClickListeners() {
        activity_daily_form_date_containerCL.setOnClickListener(
            createShowDateClickListener(activity_daily_form_date_titleTV)
        )
        activity_daily_form_content_scope_date_containerCL.setOnClickListener(
            createShowDateClickListener(activity_daily_form_content_scope_date_titleTV)
        )
        activity_daily_form_content_acm_tested_date_containerCL.setOnClickListener(
            createShowDateClickListener(activity_daily_form_content_acm_tested_date_titleTV)
        )
        activity_daily_form_content_acm_survey_date_containerCL.setOnClickListener(
            createShowDateClickListener(activity_daily_form_content_acm_survey_date_titleTV)
        )
        activity_daily_form_content_lead_tested_date_containerCL.setOnClickListener(
            createShowDateClickListener(activity_daily_form_content_lead_tested_date_titleTV)
        )
        activity_daily_form_content_estimated_completion_date_containerCL.setOnClickListener(
            createShowDateClickListener(
                activity_daily_form_content_estimated_completion_date_titleTV
            )
        )
        activity_daily_form_content_mold_tested_date_containerCL.setOnClickListener(
            createShowDateClickListener(activity_daily_form_content_mold_tested_date_titleTV)
        )
    }

    private fun createShowDateClickListener(dateTitleView: TextView): View.OnClickListener {
        return View.OnClickListener {
            val date = extractDateFromTextForPicker(dateTitleView)

            val picker =
                DatePickerDialog.newInstance(date, object : DatePickerDialog.DateSelectedListener {
                    override fun onDateSelected(newDate: Date) {
                        dateTitleView.text = newDate.toSimpleString()
                    }
                })
            picker.show(supportFragmentManager, DatePickerDialog.TAG)
        }
    }

    private fun extractDateFromTextForPicker(dateTV: TextView): Date {
        val pleaseSelect = getString(R.string.activity_daily_form_date_title)
        val dateText = dateTV.text.toString()
        return if (dateText == "" || dateText == pleaseSelect) {
            Date()
        } else {
            dateTV.text.toString().toDate()
        }
    }

    private fun initReportableIncident() {
        val reportableIncidentListener = View.OnClickListener {
            val checkedId = activity_daily_form_reportable_incident_radiogroup.checkedRadioButtonId
            val hasIncident = checkedId != R.id.activity_daily_form_reportable_incident_noRB
            activity_daily_form_reportable_incident_spinner.isEnabled = hasIncident
            activity_daily_form_reportable_incident_value.isEnabled = hasIncident
        }

        activity_daily_form_reportable_incident_noRB.setOnClickListener(reportableIncidentListener)
        activity_daily_form_reportable_incident_yesRB.setOnClickListener(reportableIncidentListener)
        activity_daily_form_reportable_incident_near_missRB.setOnClickListener(
            reportableIncidentListener
        )
    }

    private fun initStopWorkPerformed() {
        val stopWorkPerformedListener = View.OnClickListener {
            activity_daily_form_stop_work_value.isEnabled =
                activity_daily_form_stop_work_radiogroup.checkedRadioButtonId != R.id.activity_daily_form_stop_work_item_noRB
        }

        activity_daily_form_stop_work_item_noRB.setOnClickListener(stopWorkPerformedListener)
        activity_daily_form_stop_work_item_yesRB.setOnClickListener(stopWorkPerformedListener)
    }

    private fun initSelectPicClickListener() {
        activity_daily_form_upload_image1_label.setOnClickListener(getPicClickListener(1))
        activity_daily_form_upload_image2_label.setOnClickListener(getPicClickListener(2))
        activity_daily_form_upload_image3_label.setOnClickListener(getPicClickListener(3))
        activity_daily_form_upload_image4_label.setOnClickListener(getPicClickListener(4))
        activity_daily_form_upload_image5_label.setOnClickListener(getPicClickListener(5))
        activity_daily_form_upload_image6_label.setOnClickListener(getPicClickListener(6))
    }

    private fun getPicClickListener(imageNumber: Int): View.OnClickListener {
        return View.OnClickListener {
            if (!hasAllPermissions(requiredPermissions)) {
                RequestPermissionRationalDialog()
                    .show(supportFragmentManager, RequestPermissionRationalDialog.TAG)
            } else {
                showImagePickerOptions(imageNumber)
            }
        }
    }

    override fun startPermissionRequest() {
        getPermissions(requiredPermissions, PERMISSIONS_REQUEST_CODE)
    }

    private fun showImagePickerOptions(imageNumber: Int) {
        ImagePickerOptionsDialog.newInstance(imageNumber)
            .show(supportFragmentManager, ImagePickerOptionsDialog.TAG)
    }

    override fun onTakeCameraSelected(imageNumber: Int) {
        ImagePickerActivity.startTakePic(this, imageNumber.toString())
    }

    override fun onChooseGallerySelected(imageNumber: Int) {
        ImagePickerActivity.startSelectFromGallery(this, imageNumber.toString())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE && resultCode == Activity.RESULT_OK) {
            val uri: Uri = data?.getParcelableExtra(RESULT_IMAGE_PATH)!!
            val imageNumber = data.getStringExtra(ImagePickerActivity.INTENT_FILE_NAME_NO_EXTENSION)
                ?.toIntOrNull()

            if (imageNumber != null) {
                val path = uri.path!!
                if (File(path).exists()) {
                    presenter.saveImage(projectId, internalFormId, path, imageNumber)
                }
                renderImages(presenter.getImageData(internalFormId, projectId))
            }
        }
    }

    private fun initDeletePicClickListener() {
        activity_daily_form_image1_trash.setOnClickListener(createDeletePicListener(1))
        activity_daily_form_image2_trash.setOnClickListener(createDeletePicListener(2))
        activity_daily_form_image3_trash.setOnClickListener(createDeletePicListener(3))
        activity_daily_form_image4_trash.setOnClickListener(createDeletePicListener(4))
        activity_daily_form_image5_trash.setOnClickListener(createDeletePicListener(5))
        activity_daily_form_image6_trash.setOnClickListener(createDeletePicListener(6))
    }

    private fun createDeletePicListener(imageNumber: Int): View.OnClickListener {
        return View.OnClickListener {
            presenter.deleteImage(projectId, internalFormId, imageNumber)
            renderImages(presenter.getImageData(internalFormId, projectId))
        }
    }

    private fun bind() {
        renderImages(presenter.getImageData(internalFormId, projectId))

        lifecycleScope.launchWhenCreated {
            presenter.formViewState.collect { render(it) }
        }

        lifecycleScope.launchWhenCreated {
            presenter.loadDailyFieldReportViewState(internalFormId, projectId)
        }
    }

    @SuppressLint("DefaultLocale")
    private fun render(viewState: DailyFormViewState) {
        activity_daily_form_content_project_name.text = viewState.projectName
        activity_daily_form_content_page_title.text = getTitle(viewState.formDate)
        activity_daily_form_supervisor.text = viewState.supervisorFullName
        activity_daily_form_supervisor.tag = viewState.supervisorId
        activity_daily_form_project_managerTV.text = viewState.projectManagerFullName
        activity_daily_form_project_managerTV.tag = viewState.projectManagerId
        activity_daily_form_date_titleTV.text = viewState.formDate.toSimpleString()
        setSimpleSpinnerValue(activity_daily_form_scope_received_spinner, viewState.scopeReceived)
        activity_daily_form_content_scope_date_titleTV.text = viewState.scopeDate.toSimpleString()
        setSimpleSpinnerValue(activity_daily_form_structure_spinner, viewState.scopeStructure)
        activity_daily_form_work_completed_value.setText(viewState.workCompletedToday)
        activity_daily_form_what_went_right_value.setText(viewState.whatWentRight)
        activity_daily_form_what_to_improve_value.setText(viewState.whatCanBeImproved)
        activity_daily_form_reportable_incident_radiogroup.check(
            when (viewState.reportableIncident.toLowerCase()) {
                getString(R.string.activity_daily_form_reportable_incident_no).toLowerCase() -> {
                    R.id.activity_daily_form_reportable_incident_noRB
                }
                getString(R.string.activity_daily_form_reportable_incident_yes).toLowerCase() -> {
                    R.id.activity_daily_form_reportable_incident_yesRB
                }
                getString(R.string.activity_daily_form_reportable_incident_near_miss).toLowerCase() -> {
                    R.id.activity_daily_form_reportable_incident_near_missRB
                }
                else -> R.id.activity_daily_form_reportable_incident_noRB
            }
        )

        val incidentEnabled =
            activity_daily_form_reportable_incident_radiogroup.checkedRadioButtonId != R.id.activity_daily_form_reportable_incident_noRB
        activity_daily_form_reportable_incident_spinner.isEnabled = incidentEnabled
        activity_daily_form_reportable_incident_value.isEnabled = incidentEnabled

        setSimpleSpinnerValue(activity_daily_form_reportable_incident_spinner, viewState.incident)
        activity_daily_form_reportable_incident_value.setText(viewState.otherIncident)
        activity_daily_form_stop_work_radiogroup.check(
            when (viewState.stopWorkPerformed.toLowerCase()) {
                getString(R.string.activity_daily_form_stop_work_item_yes).toLowerCase() -> {
                    R.id.activity_daily_form_stop_work_item_yesRB
                }
                else -> {
                    R.id.activity_daily_form_stop_work_item_noRB
                }
            }
        )

        val stopWorkEnabled =
            viewState.stopWorkPerformed.toLowerCase() == getString(R.string.activity_daily_form_stop_work_item_yes).toLowerCase()
        activity_daily_form_stop_work_value.isEnabled = stopWorkEnabled

        activity_daily_form_stop_work_value.setText(viewState.swpNote)

        activity_daily_form_tomorrow_goals_value.setText(viewState.goalsForTomorrow)

        activity_daily_form_week_goals_value.setText(viewState.goalsForTheWeek)

        activity_daily_form_action_required_value.setText(viewState.actionRequired)

        activity_daily_form_project_notes_value.setText(viewState.projectNotesOrSpecialConsiderations)

        activity_daily_form_meeting_log_value.setText(viewState.meetingLog)

        activity_daily_form_visitors_value.setText(viewState.visitors)

        activity_daily_form_workflow_value.setText(viewState.workflow)

        activity_daily_form_equipment_value.setText(viewState.bulkEquipmentInUseThisDay)

        setSimpleSpinnerValue(activity_daily_form_acm_detected_spinner, viewState.acmDetected)

        activity_daily_form_content_acm_tested_date_titleTV.text =
            viewState.acmBuildDate.toSimpleString()

        activity_daily_form_content_acm_survey_date_titleTV.text =
            viewState.acmSurveyDate.toSimpleString()

        setSimpleSpinnerValue(activity_daily_form_lead_detected_spinner, viewState.leadDetected)

        activity_daily_form_content_lead_tested_date_titleTV.text =
            viewState.leadTestDate.toSimpleString()

        setSimpleSpinnerValue(activity_daily_form_mold_detected_spinner, viewState.moldDetected)

        activity_daily_form_content_mold_tested_date_titleTV.text =
            viewState.moldDateOdProtocol.toSimpleString()

        activity_daily_form_mold_notes_value.setText(viewState.notesRelatedToEnvironmentalTesting)

        activity_daily_form_content_estimated_completion_date_titleTV.text =
            viewState.estimatedCompletionDate.toSimpleString()

        activity_daily_form_days_remaining_input_field.setText(viewState.daysRemainingOnProject)

        activity_daily_form_activities_done_install_criticalsRB.isChecked =
            viewState.activitiesDoneTodayInstallCriticals
        activity_daily_form_activities_done_item_negative_pressureRB.isChecked =
            viewState.activitiesDoneTodayEstablishNegativePressure
        activity_daily_form_activities_done_construct_deconRB.isChecked =
            viewState.activitiesDoneTodayConstructDecon
        activity_daily_form_activities_pre_cleanRB.isChecked =
            viewState.activitiesDoneTodayPreClean
        activity_daily_form_activities_done_cover_flexedRB.isChecked =
            viewState.activitiesDoneTodayCoverFixedObjects
        activity_daily_form_activities_done_construct_containmentRB.isChecked =
            viewState.activitiesDoneTodayConstructContainment
        activity_daily_form_activities_removalRB.isChecked =
            viewState.activitiesDoneTodayRemoval
        activity_daily_form_activities_done_bag_outRB.isChecked =
            viewState.activitiesDoneTodayBagOut
        activity_daily_form_activities_done_final_clean_deconRB.isChecked =
            viewState.activitiesDoneTodayFinalCleanOrDecontamination
        activity_daily_form_activities_done_clearanceRB.isChecked =
            viewState.activitiesDoneTodayClearance
        activity_daily_form_activities_done_tear_downRB.isChecked =
            viewState.activitiesDoneTodayTearDown
        activity_daily_form_activities_done_demobRB.isChecked =
            viewState.activitiesDoneTodayDemob

        activity_daily_form_cleaning_verification_exteriorRB.isChecked =
            viewState.cleaningVerificationExterior
        activity_daily_form_cleaning_verification_interiorRB.isChecked =
            viewState.cleaningVerificationInterior

        setSimpleSpinnerValue(
            activity_daily_form_work_area_locations1,
            viewState.workAreaRoomLocations1
        )
        setSimpleSpinnerValue(
            activity_daily_form_work_area_locations2,
            viewState.workAreaRoomLocations2
        )
        setSimpleSpinnerValue(
            activity_daily_form_work_area_locations3,
            viewState.workAreaRoomLocations3
        )

        activity_daily_form_cleaning_cloths_utilized1.setText(viewState.numberOfCleaningClothsUtilized1.toString())
        activity_daily_form_cleaning_cloths_utilized2.setText(viewState.numberOfCleaningClothsUtilized2.toString())
        activity_daily_form_cleaning_cloths_utilized3.setText(viewState.numberOfCleaningClothsUtilized3.toString())

        setSimpleSpinnerValue(
            activity_daily_form_cleaning_verification1,
            viewState.whatCleaningVerificationDidYouPassOn1
        )
        setSimpleSpinnerValue(
            activity_daily_form_cleaning_verification2,
            viewState.whatCleaningVerificationDidYouPassOn2
        )
        setSimpleSpinnerValue(
            activity_daily_form_cleaning_verification3,
            viewState.whatCleaningVerificationDidYouPassOn3
        )
    }

    private fun getTitle(formDate: Date?): String {
        val sb = StringBuilder()

        sb.append(formTypeName)

        if (formDate != null) {
            sb.append(" - ")
            sb.append(formDate.toSimpleString())
        }

        return sb.toString()
    }

    private fun setSimpleSpinnerValue(spinner: Spinner, selectedValue: String?) {
        @Suppress("UNCHECKED_CAST")
        val adapter = spinner.adapter as ArrayAdapter<String>

        if (selectedValue.isNullOrEmpty()) {
            spinner.setSelection(0)
        } else {
            val position = adapter.getPosition(selectedValue)
            spinner.setSelection(position)
        }
    }

    private fun getSimpleSpinnerValue(spinner: Spinner): String {
        val na = getString(R.string.activity_daily_form_not_assigned)
        val spinnerHint = getString(R.string.activity_daily_form_spinner_hint)

        val selected = (spinner.selectedItem as? String) ?: ""

        return if (selected == na || selected == spinnerHint) {
            ""
        } else {
            selected
        }
    }

    private fun renderImages(imagesViewData: List<ImageViewData>) {
        imagesViewData.forEachIndexed { index, imageViewData ->
            renderImage(
                imageViewData = imageViewData,
                imageView = getImageViewForImage(index + 1),
                uploadLabel = getUploadLabelForImage(index + 1),
                imageLabel = getImageLabelForImage(index + 1),
                container = getContainerForImage(index + 1)
            )
        }
    }

    private fun renderImage(
        imageViewData: ImageViewData,
        imageView: ImageView,
        uploadLabel: View,
        imageLabel: View,
        container: View
    ) {
        if (imageViewData.hasImage) {
            Glide.with(imageView)
                .load(if (imageViewData.imageFile.exists()) imageViewData.imageFile else imageViewData.hasImage)
                .centerInside()
                .into(imageView)
            uploadLabel.gone()
            imageLabel.visible()
            container.visible()
        } else {
            Glide.with(imageView).clear(imageView)
            uploadLabel.visible()
            imageLabel.gone()
            container.gone()
        }
    }

    private fun getImageViewForImage(@IntRange(from = 1, to = 6) index: Int): ImageView {
        return when (index) {
            1 -> activity_daily_form_image1
            2 -> activity_daily_form_image2
            3 -> activity_daily_form_image3
            4 -> activity_daily_form_image4
            5 -> activity_daily_form_image5
            6 -> activity_daily_form_image6
            else -> throw IllegalArgumentException("index must be from 1-6. Got $index")
        }
    }

    private fun getUploadLabelForImage(@IntRange(from = 1, to = 6) index: Int): View {
        return when (index) {
            1 -> activity_daily_form_upload_image1_label
            2 -> activity_daily_form_upload_image2_label
            3 -> activity_daily_form_upload_image3_label
            4 -> activity_daily_form_upload_image4_label
            5 -> activity_daily_form_upload_image5_label
            6 -> activity_daily_form_upload_image6_label
            else -> throw IllegalArgumentException("index must be from 1-6. Got $index")
        }
    }

    private fun getImageLabelForImage(@IntRange(from = 1, to = 6) index: Int): View {
        return when (index) {
            1 -> activity_daily_form_image1_label
            2 -> activity_daily_form_image2_label
            3 -> activity_daily_form_image3_label
            4 -> activity_daily_form_image4_label
            5 -> activity_daily_form_image5_label
            6 -> activity_daily_form_image6_label
            else -> throw IllegalArgumentException("index must be from 1-6. Got $index")
        }
    }

    private fun getContainerForImage(@IntRange(from = 1, to = 6) index: Int): View {
        return when (index) {
            1 -> activity_daily_form_image1_container
            2 -> activity_daily_form_image2_container
            3 -> activity_daily_form_image3_container
            4 -> activity_daily_form_image4_container
            5 -> activity_daily_form_image5_container
            6 -> activity_daily_form_image6_container
            else -> throw IllegalArgumentException("index must be from 1-6. Got $index")
        }
    }

    private fun createDailyReportForm(): DailyFieldReportUseCases.DailyFieldReportSaveData {
        return DailyFieldReportUseCases.DailyFieldReportSaveData(
            internalFormId = internalFormId,
            supervisorId = activity_daily_form_supervisor.tag as? Int ?: 0,
            supervisorFullName = activity_daily_form_supervisor.text.toString(),
            projectManagerId = activity_daily_form_project_managerTV.tag as? Int ?: 0,
            projectManagerFullName = activity_daily_form_project_managerTV.text.toString(),
            formDate = activity_daily_form_date_titleTV.text.toString().toDate(),
            scopeReceived = getSimpleSpinnerValue(activity_daily_form_scope_received_spinner),
            scopeDate = activity_daily_form_content_scope_date_titleTV.text.toString().toDate(),
            scopeStructure = getSimpleSpinnerValue(activity_daily_form_structure_spinner),
            workCompletedToday = activity_daily_form_work_completed_value.text.toString(),
            goalsForTomorrow = activity_daily_form_tomorrow_goals_value.text.toString(),
            goalsForTheWeek = activity_daily_form_week_goals_value.text.toString(),
            actionRequired = activity_daily_form_action_required_value.text.toString(),
            projectNotesOrSpecialConsiderations = activity_daily_form_project_notes_value.text.toString(),
            meetingLog = activity_daily_form_meeting_log_value.text.toString(),
            visitors = activity_daily_form_visitors_value.text.toString(),
            workflow = activity_daily_form_workflow_value.text.toString(),
            bulkEquipmentInUseThisDay = activity_daily_form_equipment_value.text.toString(),
            acmDetected = getSimpleSpinnerValue(activity_daily_form_acm_detected_spinner),
            acmBuildDate = activity_daily_form_content_acm_tested_date_titleTV.text.toString()
                .toDate(),
            acmSurveyDate = activity_daily_form_content_acm_survey_date_titleTV.text.toString()
                .toDate(),
            leadDetected = getSimpleSpinnerValue(activity_daily_form_lead_detected_spinner),
            leadTestDate = activity_daily_form_content_lead_tested_date_titleTV.text.toString()
                .toDate(),
            moldDetected = getSimpleSpinnerValue(activity_daily_form_mold_detected_spinner),
            moldTestDate = activity_daily_form_content_mold_tested_date_titleTV.text.toString()
                .toDate(),
            notesRelatedToEnvironmentalTesting = activity_daily_form_mold_notes_value.text.toString(),
            estimatedCompletionDate = activity_daily_form_content_estimated_completion_date_titleTV.text.toString()
                .toDate(),
            daysRemainingOnProject = activity_daily_form_days_remaining_input_field.text.toString(),
            activitiesDoneTodayInstallCriticals = activity_daily_form_activities_done_install_criticalsRB.isChecked,
            activitiesDoneTodayRemoval = activity_daily_form_activities_removalRB.isChecked,
            activitiesDoneTodayEstablishNegativePressure = activity_daily_form_activities_done_item_negative_pressureRB.isChecked,
            activitiesDoneTodayBagOut = activity_daily_form_activities_done_bag_outRB.isChecked,
            activitiesDoneTodayConstructDecon = activity_daily_form_activities_done_construct_deconRB.isChecked,
            activitiesDoneTodayFinalCleanOrDecontamination = activity_daily_form_activities_done_final_clean_deconRB.isChecked,
            activitiesDoneTodayPreClean = activity_daily_form_activities_pre_cleanRB.isChecked,
            activitiesDoneTodayClearance = activity_daily_form_activities_done_clearanceRB.isChecked,
            activitiesDoneTodayCoverFixedObjects = activity_daily_form_activities_done_cover_flexedRB.isChecked,
            activitiesDoneTodayTearDown = activity_daily_form_activities_done_tear_downRB.isChecked,
            activitiesDoneTodayConstructContainment = activity_daily_form_activities_done_construct_containmentRB.isChecked,
            activitiesDoneTodayDemob = activity_daily_form_activities_done_demobRB.isChecked,
            cleaningVerificationExterior = activity_daily_form_cleaning_verification_exteriorRB.isChecked,
            cleaningVerificationInterior = activity_daily_form_cleaning_verification_interiorRB.isChecked,
            workAreaRoomLocations1 = getSimpleSpinnerValue(activity_daily_form_work_area_locations1),
            workAreaRoomLocations2 = getSimpleSpinnerValue(activity_daily_form_work_area_locations2),
            workAreaRoomLocations3 = getSimpleSpinnerValue(activity_daily_form_work_area_locations3),
            numberOfCleaningClothsUtilized1 = activity_daily_form_cleaning_cloths_utilized1.text.toString()
                .safeToInt(),
            numberOfCleaningClothsUtilized2 = activity_daily_form_cleaning_cloths_utilized2.text.toString()
                .safeToInt(),
            numberOfCleaningClothsUtilized3 = activity_daily_form_cleaning_cloths_utilized3.text.toString()
                .safeToInt(),
            whatCleaningVerificationDidYouPassOn1 = getSimpleSpinnerValue(
                activity_daily_form_cleaning_verification1
            ),
            whatCleaningVerificationDidYouPassOn2 = getSimpleSpinnerValue(
                activity_daily_form_cleaning_verification2
            ),
            whatCleaningVerificationDidYouPassOn3 = getSimpleSpinnerValue(
                activity_daily_form_cleaning_verification3
            ),
            whatWentRight = activity_daily_form_what_went_right_value.text.toString(),
            whatCanBeImproved = activity_daily_form_what_to_improve_value.text.toString(),
            reportableIncident = getReportableIncidentValue(),
            incident = if (activity_daily_form_reportable_incident_radiogroup.checkedRadioButtonId !=
                R.id.activity_daily_form_reportable_incident_noRB
            ) {
                getSimpleSpinnerValue(activity_daily_form_reportable_incident_spinner)
            } else {
                ""
            },
            otherIncident = if (activity_daily_form_reportable_incident_radiogroup.checkedRadioButtonId !=
                R.id.activity_daily_form_reportable_incident_noRB
            ) {
                activity_daily_form_reportable_incident_value.text.toString()
            } else {
                ""
            },
            stopWorkPerformed = getStopWorkPerformedValue(),
            swpNote = if (activity_daily_form_stop_work_radiogroup.checkedRadioButtonId != R.id.activity_daily_form_stop_work_item_noRB) {
                activity_daily_form_stop_work_value.text.toString()
            } else {
                ""
            }
        )
    }

    private fun getReportableIncidentValue(): String {
        return when (activity_daily_form_reportable_incident_radiogroup.checkedRadioButtonId) {
            R.id.activity_daily_form_reportable_incident_noRB -> {
                getString(R.string.activity_daily_form_reportable_incident_no)
            }
            R.id.activity_daily_form_reportable_incident_yesRB -> {
                getString(R.string.activity_daily_form_reportable_incident_yes)
            }
            R.id.activity_daily_form_reportable_incident_near_missRB -> {
                getString(R.string.activity_daily_form_reportable_incident_near_miss)
            }
            else -> getString(R.string.activity_daily_form_reportable_incident_no)
        }
    }

    private fun getStopWorkPerformedValue(): String {
        return when (activity_daily_form_stop_work_radiogroup.checkedRadioButtonId) {
            R.id.activity_daily_form_stop_work_item_yesRB -> {
                getString(R.string.activity_daily_form_stop_work_item_yes)
            }
            R.id.activity_daily_form_stop_work_item_noRB -> {
                getString(R.string.activity_daily_form_stop_work_item_no)
            }
            else -> getString(R.string.activity_daily_form_stop_work_item_no)
        }
    }
}