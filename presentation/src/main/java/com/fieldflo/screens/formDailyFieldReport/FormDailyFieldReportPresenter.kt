package com.fieldflo.screens.formDailyFieldReport

import androidx.lifecycle.ViewModel
import com.fieldflo.AppScopeHolder
import com.fieldflo.data.persistence.db.entities.DailyFieldReportFormData
import com.fieldflo.screens.selectEmployee.EmployeeUiRow
import com.fieldflo.screens.selectSupervisor.SupervisorEmployeeUiRow
import com.fieldflo.usecases.DailyFieldReportUseCases
import com.fieldflo.usecases.ProjectStatus
import com.fieldflo.usecases.ProjectsUseCases
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch
import javax.inject.Inject

class FormDailyFieldReportPresenter @Inject constructor(
    private val dailyFieldReportUseCases: DailyFieldReportUseCases,
    private val projectsUseCases: ProjectsUseCases,
    private val appScopeHolder: AppScopeHolder
) : ViewModel() {

    private val _formViewState = MutableStateFlow<DailyFormViewState?>(null)
    val formViewState: Flow<DailyFormViewState> = _formViewState.filterNotNull()

    suspend fun loadDailyFieldReportViewState(internalFormId: Int, projectId: Int) {
        val formData = dailyFieldReportUseCases.getFieldReportData(internalFormId, projectId)
        val projectName = projectsUseCases.getProjectName(projectId)
        val viewState = formData.toUiModel(projectName)
        _formViewState.value = viewState
    }

    fun getImageData(internalFormId: Int, projectId: Int): List<ImageViewData> {
        return dailyFieldReportUseCases.dailyFieldReportPics(internalFormId, projectId)
            .map { ImageViewData(it.absolutePath) }
    }

    fun getProjectStatusFlow(projectId: Int): Flow<ProjectStatus> {
        return projectsUseCases.getProjectStatusFlow(projectId)
    }

    fun supervisorChanged(employee: SupervisorEmployeeUiRow) {
        _formViewState.value?.let { previousState ->
            val newState = previousState.copy(
                supervisorFullName = employee.employeeName,
                supervisorId = employee.employeeId
            )
            _formViewState.value = newState
        }
    }

    fun projectManagerChanged(employee: EmployeeUiRow) {
        _formViewState.value?.let { previousState ->
            val newState = previousState.copy(
                projectManagerFullName = employee.employeeName,
                projectManagerId = employee.employeeId
            )
            _formViewState.value = newState
        }
    }

    fun saveImage(projectId: Int, internalFormId: Int, sourcePath: String, picNumber: Int) {
        dailyFieldReportUseCases.saveDailyFieldReportPic(
            projectId,
            internalFormId,
            sourcePath,
            picNumber
        )
    }

    fun deleteImage(projectId: Int, internalFormId: Int, imageNumber: Int) {
        dailyFieldReportUseCases.deleteSavedDailyFieldReportPic(
            projectId,
            internalFormId,
            imageNumber
        )
    }

    fun saveForm(
        projectId: Int,
        internalFormId: Int,
        dailyFieldReportFormData: DailyFieldReportUseCases.DailyFieldReportSaveData
    ) {
        appScopeHolder.scope.launch {
            dailyFieldReportUseCases.saveDailyForm(
                projectId,
                internalFormId,
                dailyFieldReportFormData
            )
        }
    }
}

private fun DailyFieldReportFormData.toUiModel(projectName: String): DailyFormViewState {
    return DailyFormViewState(
        projectName = projectName,
        supervisorFullName = this.supervisorFullName,
        supervisorId = this.supervisorId,
        projectManagerFullName = this.projectManagerFullName,
        projectManagerId = this.projectManagerId,
        formDate = this.formDate,
        scopeReceived = this.scopeReceived,
        scopeDate = this.scopeDate,
        scopeStructure = this.scopeStructure,
        workCompletedToday = this.workCompletedToday,
        whatWentRight = this.whatWentRight,
        whatCanBeImproved = this.whatCanBeImproved,
        reportableIncident = this.reportableIncident,
        incident = this.incident,
        otherIncident = this.otherIncident,
        stopWorkPerformed = this.stopWorkPerformed,
        swpNote = this.swpNote,
        goalsForTomorrow = this.goalsForTomorrow,
        goalsForTheWeek = this.goalsForTheWeek,
        actionRequired = this.actionRequired,
        projectNotesOrSpecialConsiderations = this.projectNotesOrSpecialConsiderations,
        meetingLog = this.meetingLog,
        visitors = this.visitors,
        workflow = this.workflow,
        bulkEquipmentInUseThisDay = this.bulkEquipmentInUseThisDay,
        acmDetected = this.acmDetected,
        acmBuildDate = this.acmBuildDate,
        acmSurveyDate = this.acmSurveyDate,
        leadDetected = this.leadDetected,
        leadTestDate = this.leadTestDate,
        moldDetected = this.moldDetected,
        moldDateOdProtocol = this.moldTestDate,
        notesRelatedToEnvironmentalTesting = this.notesRelatedToEnvironmentalTesting,
        estimatedCompletionDate = this.estimatedCompletionDate,
        daysRemainingOnProject = this.daysRemainingOnProject.toString(),
        activitiesDoneTodayInstallCriticals = this.activitiesDoneTodayInstallCriticals,
        activitiesDoneTodayEstablishNegativePressure = this.activitiesDoneTodayEstablishNegativePressure,
        activitiesDoneTodayConstructDecon = this.activitiesDoneTodayConstructDecon,
        activitiesDoneTodayPreClean = this.activitiesDoneTodayPreClean,
        activitiesDoneTodayCoverFixedObjects = this.activitiesDoneTodayCoverFixedObjects,
        activitiesDoneTodayConstructContainment = this.activitiesDoneTodayConstructContainment,
        activitiesDoneTodayRemoval = this.activitiesDoneTodayRemoval,
        activitiesDoneTodayBagOut = this.activitiesDoneTodayBagOut,
        activitiesDoneTodayFinalCleanOrDecontamination = this.activitiesDoneTodayFinalCleanOrDecontamination,
        activitiesDoneTodayClearance = this.activitiesDoneTodayClearance,
        activitiesDoneTodayTearDown = this.activitiesDoneTodayTearDown,
        activitiesDoneTodayDemob = this.activitiesDoneTodayDemob,
        cleaningVerificationExterior = this.cleaningVerificationExterior,
        cleaningVerificationInterior = this.cleaningVerificationInterior,
        workAreaRoomLocations1 = this.workAreaRoomLocations1,
        workAreaRoomLocations2 = this.workAreaRoomLocations2,
        workAreaRoomLocations3 = this.workAreaRoomLocations3,
        numberOfCleaningClothsUtilized1 = this.numberOfCleaningClothsUtilized1,
        numberOfCleaningClothsUtilized2 = this.numberOfCleaningClothsUtilized2,
        numberOfCleaningClothsUtilized3 = this.numberOfCleaningClothsUtilized3,
        whatCleaningVerificationDidYouPassOn1 = this.whatCleaningVerificationDidYouPassOn1,
        whatCleaningVerificationDidYouPassOn2 = this.whatCleaningVerificationDidYouPassOn2,
        whatCleaningVerificationDidYouPassOn3 = this.whatCleaningVerificationDidYouPassOn3
    )
}