package com.fieldflo.screens.formDailyFieldReport

import java.io.File
import java.util.*

data class DailyFormViewState(
    val projectName: String = "",

    val projectManagerFullName: String,
    val projectManagerId: Int,
    val supervisorFullName: String,
    val supervisorId: Int,
    val formDate: Date,
    val scopeReceived: String,
    val scopeDate: Date,
    val scopeStructure: String,
    val workCompletedToday: String,
    val whatWentRight: String,
    val whatCanBeImproved: String,
    val reportableIncident: String,
    val incident: String,
    val otherIncident: String,
    val stopWorkPerformed: String,
    val swpNote: String,
    val goalsForTomorrow: String,
    val goalsForTheWeek: String,
    val actionRequired: String,
    val projectNotesOrSpecialConsiderations: String,
    val meetingLog: String,
    val visitors: String,
    val workflow: String,
    val bulkEquipmentInUseThisDay: String,
    val acmDetected: String,
    val acmBuildDate: Date,
    val acmSurveyDate: Date,
    val leadDetected: String,
    val leadTestDate: Date,
    val moldDetected: String,
    val moldDateOdProtocol: Date,
    val notesRelatedToEnvironmentalTesting: String,
    val estimatedCompletionDate: Date,
    val daysRemainingOnProject: String,
    val activitiesDoneTodayInstallCriticals: Boolean,
    val activitiesDoneTodayRemoval: Boolean,
    val activitiesDoneTodayEstablishNegativePressure: Boolean,
    val activitiesDoneTodayBagOut: Boolean,
    val activitiesDoneTodayConstructDecon: Boolean,
    val activitiesDoneTodayFinalCleanOrDecontamination: Boolean,
    val activitiesDoneTodayPreClean: Boolean,
    val activitiesDoneTodayClearance: Boolean,
    val activitiesDoneTodayCoverFixedObjects: Boolean,
    val activitiesDoneTodayTearDown: Boolean,
    val activitiesDoneTodayConstructContainment: Boolean,
    val activitiesDoneTodayDemob: Boolean,
    val cleaningVerificationExterior: Boolean,
    val cleaningVerificationInterior: Boolean,
    val workAreaRoomLocations1: String,
    val workAreaRoomLocations2: String,
    val workAreaRoomLocations3: String,
    val numberOfCleaningClothsUtilized1: Int,
    val numberOfCleaningClothsUtilized2: Int,
    val numberOfCleaningClothsUtilized3: Int,
    val whatCleaningVerificationDidYouPassOn1: String,
    val whatCleaningVerificationDidYouPassOn2: String,
    val whatCleaningVerificationDidYouPassOn3: String
)

data class ImageViewData(
    private val imageFileLocation: String = ""
) {
    val imageFile
        get() = File(imageFileLocation)

    val hasImage
        get() = imageFile.exists()
}