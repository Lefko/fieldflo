package com.fieldflo.screens.projectHome.activeForms

import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fieldflo.R
import com.fieldflo.common.*

class ActiveFormsAdapter(private val formActionsCallback: (FormActionType, ActiveFormUiModel) -> Any) :
    ListAdapter<ActiveFormUiModel, ActiveFormsAdapter.ActiveFormViewHolder>(
        CALLBACK
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActiveFormViewHolder {
        val rowLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_form_active, parent, false) as ViewGroup

        val holder = ActiveFormViewHolder(
            rowLayout,
            { getItem(it) },
            formActionsCallback
        )
        rowLayout.setOnClickListener {
            val position = holder.bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val uiModel = getItem(position)
                formActionsCallback(FormActionType.CLICK, uiModel)
            }
        }
        return holder
    }

    override fun onBindViewHolder(holder: ActiveFormViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ActiveFormViewHolder(
        rowLayout: ViewGroup,
        uiModelProvider: (Int) -> ActiveFormUiModel,
        formActionsCallback: (FormActionType, ActiveFormUiModel) -> Any
    ) : RecyclerView.ViewHolder(rowLayout) {
        private val formTitleTV: TextView = rowLayout.findViewById(R.id.row_form_label)
        private val formDateTV: TextView = rowLayout.findViewById(R.id.row_form_date)
        private val formDotIV: ImageView = rowLayout.findViewById(R.id.row_form_dot)
        private val divider: View = rowLayout.findViewById(R.id.row_form_divider_1)
        private val mainViewCL: ConstraintLayout = rowLayout.findViewById(R.id.ic_form_main)
        private val optionsViewCL: ConstraintLayout =
            rowLayout.findViewById(R.id.ic_form_main_options)
        private val optionsPreviousTV: TextView =
            rowLayout.findViewById(R.id.ic_form_options_previous)
        private val optionsCloseTV: TextView = rowLayout.findViewById(R.id.ic_form_options_close)
        private val optionsDeleteTV: TextView = rowLayout.findViewById(R.id.ic_form_options_delete)
        private val hideMenuSet: ConstraintSet = ConstraintSet()
        private val showMenuSet: ConstraintSet = ConstraintSet()

        init {
            optionsPreviousTV.setSafeOnClickListener {
                if (bindingAdapterPosition != RecyclerView.NO_POSITION) {
                    val uiModel = uiModelProvider(bindingAdapterPosition)

                    formActionsCallback(FormActionType.SAME_AS_PREVIOUS, uiModel)

                    optionsPreviousTV.post {
                        uiModel.menuOpen = false
                        TransitionManager.beginDelayedTransition(mainViewCL)
                        hideMenuSet.applyTo(mainViewCL)
                    }
                }
            }

            optionsCloseTV.setSafeOnClickListener {
                if (bindingAdapterPosition != RecyclerView.NO_POSITION) {
                    val uiModel = uiModelProvider(bindingAdapterPosition)
                    formActionsCallback(FormActionType.CLOSE, uiModel)
                }
            }

            optionsDeleteTV.setSafeOnClickListener {
                if (bindingAdapterPosition != RecyclerView.NO_POSITION) {
                    val uiModel = uiModelProvider(bindingAdapterPosition)
                    formActionsCallback(FormActionType.DELETE, uiModel)
                }
            }

            formDotIV.setSafeOnClickListener {
                if (bindingAdapterPosition != RecyclerView.NO_POSITION) {
                    val uiModel = uiModelProvider(bindingAdapterPosition)
                    if (uiModel.hasPreviousDayOption) {
                        optionsPreviousTV.visible()
                    } else {
                        optionsPreviousTV.gone()
                    }

                    if (uiModel.menuOpen) {
                        TransitionManager.beginDelayedTransition(mainViewCL)
                        hideMenuSet.applyTo(mainViewCL)
                    } else {
                        mainViewCL.post {
                            TransitionManager.beginDelayedTransition(mainViewCL)
                            showMenuSet.applyTo(mainViewCL)
                        }
                    }
                    uiModel.menuOpen = !uiModel.menuOpen
                }
            }

            hideMenuSet.apply {
                clone(mainViewCL)

                clear(formDateTV.id, ConstraintSet.BOTTOM)
                connect(
                    formDateTV.id,
                    ConstraintSet.TOP,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.TOP
                )

                clear(formTitleTV.id, ConstraintSet.TOP)
                connect(formTitleTV.id, ConstraintSet.TOP, formDateTV.id, ConstraintSet.BOTTOM)

                clear(optionsViewCL.id, ConstraintSet.START)
                clear(optionsViewCL.id, ConstraintSet.END)
                connect(
                    optionsViewCL.id,
                    ConstraintSet.END,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.START
                )
            }

            showMenuSet.apply {
                clone(mainViewCL)

                clear(formDateTV.id, ConstraintSet.TOP)
                connect(
                    formDateTV.id,
                    ConstraintSet.BOTTOM,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.TOP
                )

                clear(formTitleTV.id, ConstraintSet.TOP)
                connect(
                    formTitleTV.id,
                    ConstraintSet.TOP,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.BOTTOM
                )

                clear(optionsViewCL.id, ConstraintSet.START)
                clear(optionsViewCL.id, ConstraintSet.END)
                connect(
                    optionsViewCL.id,
                    ConstraintSet.START,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.START
                )
                connect(optionsViewCL.id, ConstraintSet.END, divider.id, ConstraintSet.START)
            }
        }

        fun bind(uiModel: ActiveFormUiModel) {
            formTitleTV.text = uiModel.formName
            formDateTV.text = uiModel.formDate.toSimpleString()
            if (uiModel.isClosable) {
                formTitleTV.textColor(R.color.text_color)
                mainViewCL.background = null
                optionsCloseTV.visible()
                optionsCloseTV.isEnabled = true
            } else {
                formTitleTV.textColor(R.color.selected_project_item_stroke)
                mainViewCL.background =
                    ContextCompat.getDrawable(mainViewCL.context, R.drawable.closeable_frame)
                if (uiModel.hasPreviousDayOption) {
                    optionsCloseTV.gone()
                } else {
                    optionsCloseTV.visibility = View.INVISIBLE
                    optionsCloseTV.isEnabled = false
                }
            }

            if (uiModel.menuOpen) {
                showMenuSet.applyTo(mainViewCL)
            } else {
                hideMenuSet.applyTo(mainViewCL)
            }

            if (uiModel.hasPreviousDayOption) {
                optionsPreviousTV.visible()
            } else {
                if (uiModel.isClosable) {
                    optionsPreviousTV.gone()
                } else {
                    optionsPreviousTV.visibility = View.INVISIBLE
                    optionsPreviousTV.isEnabled = false
                }
            }
        }
    }

    enum class FormActionType {
        CLOSE, DELETE, SAME_AS_PREVIOUS, CLICK
    }
}


private val CALLBACK = object : DiffUtil.ItemCallback<ActiveFormUiModel>() {

    override fun areItemsTheSame(oldItem: ActiveFormUiModel, newItem: ActiveFormUiModel) =
        oldItem.internalFormId == newItem.internalFormId

    override fun areContentsTheSame(oldItem: ActiveFormUiModel, newItem: ActiveFormUiModel) =
        oldItem == newItem
}