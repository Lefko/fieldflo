package com.fieldflo.screens.projectHome.activeForms;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.fieldflo.common.DynamicFragmentPagerAdapter;

public class ActiveFormsFragmentIdentifier extends DynamicFragmentPagerAdapter.FragmentIdentifier {

    public ActiveFormsFragmentIdentifier(@NonNull String fragmentTag, @Nullable Bundle args) {
        super(fragmentTag, args);
    }

    @SuppressWarnings("WeakerAccess")
    ActiveFormsFragmentIdentifier(Parcel parcel) {
        super(parcel);
    }

    @Override
    protected Fragment createFragment() {
        return new ActiveFormsFragment();
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    public static final Parcelable.Creator<ActiveFormsFragmentIdentifier> CREATOR = new Parcelable.Creator<ActiveFormsFragmentIdentifier>() {

        @Override
        public ActiveFormsFragmentIdentifier createFromParcel(Parcel source) {
            return new ActiveFormsFragmentIdentifier(source);
        }

        @Override
        public ActiveFormsFragmentIdentifier[] newArray(int size) {
            return new ActiveFormsFragmentIdentifier[size];
        }
    };
}
