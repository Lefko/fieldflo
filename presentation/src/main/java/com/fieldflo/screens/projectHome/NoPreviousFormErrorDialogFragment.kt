package com.fieldflo.screens.projectHome

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.fieldflo.R

class NoPreviousFormErrorDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val context = activity as Context

        val errorMessage = context.getString(R.string.project_home_active_forms_no_previous_error)

        return AlertDialog.Builder(context)
            .setMessage(errorMessage)
            .setPositiveButton(android.R.string.ok) { _, _ -> }
            .show()
    }

    companion object {
        private const val TAG = "NoPreviousFormErrorDialogFragment"
        private const val FORM_NAME_KEY = "form_name_key"
        @JvmStatic
        fun newInstance(formName: String) = NoPreviousFormErrorDialogFragment().apply {
            arguments = bundleOf(FORM_NAME_KEY to formName)
        }

        @JvmStatic
        fun show(formName: String, fm: FragmentManager) {
            val frag =
                newInstance(
                    formName
                )
            frag.show(fm,
                TAG
            )
        }
    }
}