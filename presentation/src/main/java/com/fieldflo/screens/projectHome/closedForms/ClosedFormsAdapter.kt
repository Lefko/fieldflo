package com.fieldflo.screens.projectHome.closedForms

import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fieldflo.R
import com.fieldflo.common.*

class ClosedFormsAdapter(private val reopenFormCallback: (ClosedFormUiModel) -> Unit) :
    ListAdapter<ClosedFormUiModel, ClosedFormsAdapter.ClosedFormViewHolder>(CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClosedFormViewHolder {
        val rowLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_form_closed, parent, false)

        val holder = ClosedFormViewHolder(rowLayout) { getItem(it) }

        holder.reopenTV.safeClickListener {
            val position = holder.bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val closedForm = getItem(position)
                reopenFormCallback(closedForm)
            }
        }

        return holder
    }

    override fun onBindViewHolder(holder: ClosedFormViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ClosedFormViewHolder(
        rowLayout: View,
        uiModelProvider: (position: Int) -> ClosedFormUiModel
    ) : RecyclerView.ViewHolder(rowLayout) {
        private val formTitleTV: TextView = rowLayout.findViewById(R.id.row_form_label)
        private val formDateTV: TextView = rowLayout.findViewById(R.id.row_form_date)
        private val formDotIV: ImageView = rowLayout.findViewById(R.id.row_form_dot)
        private val menuDivider: View = rowLayout.findViewById(R.id.row_form_divider_1)
        private val syncStatusTV: TextView = rowLayout.findViewById(R.id.row_form_sync_statusTV)
        private val syncErrorMessageTV: TextView =
            rowLayout.findViewById(R.id.row_form_sync_error_messageTV)
        private val mainViewCL: ConstraintLayout = rowLayout.findViewById(R.id.ic_form_main)
        val reopenTV = rowLayout.findViewById<TextView>(R.id.ic_form_options_reopen)

        private val hideMenuSet: ConstraintSet = ConstraintSet()
        private val showMenuSet: ConstraintSet = ConstraintSet()

        init {
            showMenuSet.apply {
                clone(mainViewCL)

                clear(formDateTV.id, ConstraintSet.TOP)
                connect(formDateTV.id, ConstraintSet.BOTTOM, formTitleTV.id, ConstraintSet.TOP)

                clear(formTitleTV.id, ConstraintSet.TOP)
                connect(
                    formTitleTV.id,
                    ConstraintSet.BOTTOM,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.TOP
                )

                clear(syncStatusTV.id, ConstraintSet.TOP)
                connect(
                    syncStatusTV.id,
                    ConstraintSet.TOP,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.BOTTOM
                )

                clear(reopenTV.id, ConstraintSet.END)
                connect(
                    reopenTV.id,
                    ConstraintSet.START,
                    R.id.ic_form_options_reopen_start_guide,
                    ConstraintSet.END
                )
            }

            hideMenuSet.apply {
                clone(mainViewCL)
            }

            formDotIV.setSafeOnClickListener {
                if (bindingAdapterPosition != RecyclerView.NO_POSITION) {
                    val uiModel = uiModelProvider(bindingAdapterPosition)
                    if (uiModel.menuOpen) {
                        TransitionManager.beginDelayedTransition(mainViewCL)
                        hideMenuSet.applyTo(mainViewCL)
                    } else {
                        mainViewCL.post {
                            TransitionManager.beginDelayedTransition(mainViewCL)
                            showMenuSet.applyTo(mainViewCL)
                        }
                    }
                    uiModel.menuOpen = !uiModel.menuOpen
                }
            }
        }

        fun bind(uiModel: ClosedFormUiModel) {

            formTitleTV.text = uiModel.formName
            formDateTV.text = uiModel.closeDate.toSimpleString()

            if (uiModel.menuOpen) {
                showMenuSet.applyTo(mainViewCL)
            } else {
                hideMenuSet.applyTo(mainViewCL)
            }

            when {
                uiModel.isSynced -> {
                    syncStatusTV.setText(R.string.sync_completed)
                    syncStatusTV.textColor(R.color.status_circle_green)
                    syncErrorMessageTV.gone()
                    formDotIV.gone()
                    menuDivider.gone()
                    reopenTV.gone()
                }
                uiModel.hasSyncError -> {
                    syncStatusTV.setText(R.string.sync_error)
                    syncStatusTV.textColor(R.color.attention)
                    formDotIV.visible()
                    menuDivider.visible()
                    syncErrorMessageTV.visible()
                    reopenTV.visible()
                }
                else -> {
                    syncStatusTV.setText(R.string.pending_upload)
                    syncStatusTV.textColor(R.color.attention)
                    syncErrorMessageTV.gone()
                    formDotIV.gone()
                    menuDivider.gone()
                    reopenTV.gone()
                }
            }
        }
    }
}

private val CALLBACK = object : DiffUtil.ItemCallback<ClosedFormUiModel>() {

    override fun areItemsTheSame(oldItem: ClosedFormUiModel, newItem: ClosedFormUiModel) =
        oldItem.internalFormId == newItem.internalFormId

    override fun areContentsTheSame(oldItem: ClosedFormUiModel, newItem: ClosedFormUiModel) =
        oldItem == newItem
}