package com.fieldflo.screens.projectHome.deletedForms;

import android.os.Bundle;
import android.os.Parcel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.fieldflo.common.DynamicFragmentPagerAdapter;

public class DeletedFormsFragmentIdentifier extends DynamicFragmentPagerAdapter.FragmentIdentifier {

    public DeletedFormsFragmentIdentifier(@NonNull String fragmentTag, @Nullable Bundle args) {
        super(fragmentTag, args);
    }

    @SuppressWarnings("WeakerAccess")
    DeletedFormsFragmentIdentifier(Parcel parcel) {
        super(parcel);
    }

    @Override
    protected Fragment createFragment() {
        return new DeletedFormsFragment();
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    public static final Creator<DeletedFormsFragmentIdentifier> CREATOR = new Creator<DeletedFormsFragmentIdentifier>() {

        @Override
        public DeletedFormsFragmentIdentifier createFromParcel(Parcel source) {
            return new DeletedFormsFragmentIdentifier(source);
        }

        @Override
        public DeletedFormsFragmentIdentifier[] newArray(int size) {
            return new DeletedFormsFragmentIdentifier[size];
        }
    };
}
