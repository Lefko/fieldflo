package com.fieldflo.screens.projectHome.closedForms

import java.util.*

data class ClosedFormsViewState(
    val projectforms: List<ClosedFormUiModel> = listOf()
)

data class ClosedFormUiModel(
    val internalFormId: Int = 0,
    val projectId: Int = 0,
    val formName: String = "",
    val closeDate: Date = Date(),
    val isSynced: Boolean = false,
    val hasSyncError: Boolean = false,
    var menuOpen: Boolean = false
)