package com.fieldflo.screens.projectHome.deletedForms

sealed class DeletedFormsIntents {
    data class GetDeletedFormsIntent(val projectId: Int): DeletedFormsIntents()
}