package com.fieldflo.screens.projectHome.deletedForms

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fieldflo.R
import com.fieldflo.common.toSimpleString

class DeletedFormsAdapter : ListAdapter<DeletedFormUiModel, DeletedFormsAdapter.ClosedFormViewHolder>(
    CALLBACK
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClosedFormViewHolder {
        return ClosedFormViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_form_no_menu, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ClosedFormViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ClosedFormViewHolder(rowLayout: View) : RecyclerView.ViewHolder(rowLayout) {
        private val formTitleTV: TextView = rowLayout.findViewById(R.id.row_form_label)
        private val formDateTV: TextView = rowLayout.findViewById(R.id.row_form_date)

        fun bind(uiModel: DeletedFormUiModel) {
            formTitleTV.text = uiModel.formName
            formDateTV.text = uiModel.deletedDate.toSimpleString()
        }
    }
}

private val CALLBACK = object : DiffUtil.ItemCallback<DeletedFormUiModel>() {

    override fun areItemsTheSame(oldItem: DeletedFormUiModel, newItem: DeletedFormUiModel) =
        oldItem.internalFormId == newItem.internalFormId

    override fun areContentsTheSame(oldItem: DeletedFormUiModel, newItem: DeletedFormUiModel) =
        oldItem == newItem
}