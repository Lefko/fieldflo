package com.fieldflo.screens.projectHome

import androidx.fragment.app.FragmentManager
import com.fieldflo.common.DynamicFragmentPagerAdapter
import com.fieldflo.screens.projectHome.activeForms.ActiveFormsFragment
import com.fieldflo.screens.projectHome.closedForms.ClosedFormsFragment
import com.fieldflo.screens.projectHome.deletedForms.DeletedFormsFragment

class ProjectFormsPagerAdapter(
    projectId: Int,
    fragmentManager: FragmentManager,
    private val fragmentsTitles: Array<String>
) : DynamicFragmentPagerAdapter(fragmentManager) {

    init {
        addFragment(ActiveFormsFragment.getIdentifier(projectId))
        addFragment(ClosedFormsFragment.getIdentifier(projectId))
        addFragment(DeletedFormsFragment.getIdentifier(projectId))
    }

    override fun getPageTitle(position: Int): CharSequence {
        return fragmentsTitles[position]
    }
}