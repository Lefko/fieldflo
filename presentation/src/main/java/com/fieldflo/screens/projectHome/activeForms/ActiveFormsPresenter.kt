package com.fieldflo.screens.projectHome.activeForms

import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.ActiveForm
import com.fieldflo.usecases.ProjectFormsUseCase
import com.fieldflo.usecases.SupportedForm
import com.fieldflo.usecases.UserRolesUseCases
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

class ActiveFormsPresenter @Inject constructor(
    private val userRolesUseCases: UserRolesUseCases,
    private val projectFormsUseCase: ProjectFormsUseCase
) : ViewModel() {

    private val viewState = MutableStateFlow(ActiveFormsViewState())

    fun getViewState(): Flow<ActiveFormsViewState> = viewState

    suspend fun loadActiveForms(projectId: Int) {
        if (userRolesUseCases.hasViewEditFormsRole()) {
            projectFormsUseCase.getActiveFormsFlow(projectId)
                .collect {
                    val previousState = viewState.value
                    val newState = previousState.copy(activeForms = it.map { it.toUiModel() })
                    viewState.value = newState
                }
        }
    }
}

private fun ActiveForm.toUiModel() = ActiveFormUiModel(
    internalFormId = this.activeFormData.internalFormId,
    projectId = this.activeFormData.projectId,
    formTypeId = this.activeFormData.formTypeId,
    formName = this.activeFormData.formName,
    formDate = this.activeFormData.formDate,
    hasPreviousDayOption = this.activeFormData.formTypeId == SupportedForm.TIMESHEET_FORM_ID.formTypeId ||
            this.activeFormData.formTypeId == SupportedForm.PSI_FORM_ID.formTypeId ||
            this.activeFormData.formTypeId == SupportedForm.DAILY_DEMO_LOG_ID.formTypeId,
    isClosable = this.isClosable,
    formHasPrevious = this.formHasPreviousData,
    sameAsPreviousComplete = false
)

data class ActiveFormsViewState(
    val activeForms: List<ActiveFormUiModel> = emptyList()
)