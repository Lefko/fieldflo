package com.fieldflo.screens.projectHome.addFrom

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.AvailableForm
import com.fieldflo.usecases.ProjectFormsUseCase
import com.fieldflo.usecases.UserRolesUseCases
import javax.inject.Inject

class AddFormPresenter @Inject constructor(
    private val projectFormsUseCase: ProjectFormsUseCase,
    private val rolesUseCases: UserRolesUseCases
) : ViewModel() {

    suspend fun getAvailableForms(projectId: Int) = if (rolesUseCases.hasViewEditFormsRole()) {
        projectFormsUseCase.getAvailableForms(projectId).map { it.toUiModel() }
    } else {
        emptyList()
    }

    suspend fun addFormsToProject(projectId: Int, formTypesToAdd: List<Int>) {
        projectFormsUseCase.addFormsToProject(projectId, formTypesToAdd)
    }
}

data class AvailableFormUiRow(
    val projectId: Int = 0,
    val formTypeId: Int = 0,
    val formName: String = ""
)

private fun AvailableForm.toUiModel() = AvailableFormUiRow(
    projectId = this.projectId,
    formTypeId = this.formTypeId,
    formName = this.formName
)
