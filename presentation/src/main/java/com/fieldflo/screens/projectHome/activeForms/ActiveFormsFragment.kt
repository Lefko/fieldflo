package com.fieldflo.screens.projectHome.activeForms

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.fieldflo.R
import com.fieldflo.di.injector
import kotlinx.android.synthetic.main.fragment_forms.*
import kotlinx.coroutines.flow.collect

class ActiveFormsFragment : Fragment(R.layout.fragment_forms) {

    private val projectId by lazy { arguments?.getInt(PROJECT_ID)!! }

    private lateinit var callback: ActiveFormActionsCallback

    private val presenter by viewModels<ActiveFormsPresenter> {
        injector.activeFormsViewModelFactory()
    }

    private val adapter: ActiveFormsAdapter = ActiveFormsAdapter { actionType, activeForm ->
        callback.onActiveFormAction(actionType, activeForm)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    private fun onAttachToContext(context: Context) {
        callback = context as ActiveFormActionsCallback
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
    }

    override fun onResume() {
        super.onResume()
        present()
    }

    private fun initRecyclerView() {
        fragment_forms_listRV.layoutManager = LinearLayoutManager(activity)
        fragment_forms_listRV.adapter = adapter
    }

    private fun present() {
        lifecycleScope.launchWhenResumed {
            presenter.getViewState().collect { submitList(it.activeForms) }
        }
        lifecycleScope.launchWhenResumed {
            presenter.loadActiveForms(projectId)
        }
    }

    private fun submitList(projectforms: List<ActiveFormUiModel>) {
        adapter.submitList(projectforms)
    }

    interface ActiveFormActionsCallback {
        fun onActiveFormAction(
            actionType: ActiveFormsAdapter.FormActionType,
            activeForm: ActiveFormUiModel
        )
    }

    companion object {
        const val TAG = "ActiveFormsFragment"
        private const val PROJECT_ID = "project_id"

        fun getIdentifier(projectId: Int) =
            ActiveFormsFragmentIdentifier(TAG, bundleOf(PROJECT_ID to projectId))
    }
}