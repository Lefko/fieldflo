package com.fieldflo.screens.projectHome

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenResumed
import com.fieldflo.R
import com.fieldflo.common.animateVisibility
import com.fieldflo.common.circularReveal
import com.fieldflo.common.safeClickListener
import com.fieldflo.di.injector
import com.fieldflo.screens.drawer.DrawerManager
import com.fieldflo.screens.employeesCertificate.projectCertsList.EmployeesCertificateActivity
import com.fieldflo.screens.formDailyDemoLog.DailyDemoLogActivity
import com.fieldflo.screens.formDailyFieldReport.FormDailyFieldReportActivity
import com.fieldflo.screens.formDailyLog.DailyLogFormActivity
import com.fieldflo.screens.formPsi.PsiActivity
import com.fieldflo.screens.formTimesheet.TimeSheetFormActivity
import com.fieldflo.screens.projectDetails.ProjectDetailsActivity
import com.fieldflo.screens.projectHome.activeForms.ActiveFormUiModel
import com.fieldflo.screens.projectHome.activeForms.ActiveFormsAdapter
import com.fieldflo.screens.projectHome.activeForms.ActiveFormsFragment
import com.fieldflo.screens.projectHome.addFrom.AddFormToProjectDialogFragment
import com.fieldflo.usecases.ProjectStatus
import com.fieldflo.usecases.SupportedForm
import kotlinx.android.synthetic.main.view_activity_project_content.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch

class ProjectHomeActivity : AppCompatActivity(R.layout.activity_project),
    ActiveFormsFragment.ActiveFormActionsCallback,
    ConfirmPreviousDialogFragment.ConfirmPreviousCallback {

    private val drawerManager by lazy {
        DrawerManager(
            { findViewById(R.id.drawer_layout) }, { this },
            { findViewById(R.id.activity_project_content_toolbar) }
        )
    }

    private val projectId by lazy { intent!!.extras!!.getInt(PROJECT_ID) }

    private val projectName: String by lazy { intent!!.extras!!.getString(PROJECT_NAME)!! }

    private val projectNumber by lazy { intent!!.extras!!.getString(PROJECT_NUMBER) }

    private val pagerAdapter by lazy {
        ProjectFormsPagerAdapter(
            projectId = projectId,
            fragmentManager = supportFragmentManager,
            fragmentsTitles = resources.getStringArray(R.array.project_forms_fragments)
        )
    }

    private val presenter by viewModels<ProjectHomePresenter> { injector.projectHomeViewModelFactory() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity_project_content_decimal_numberTV.text = projectNumber
        activity_project_content_project_titleTV.text = projectName

        activity_project_content_pager.apply {
            adapter = pagerAdapter
            offscreenPageLimit = 4
        }
        activity_project_content_tabs.setupWithViewPager(activity_project_content_pager)

        lifecycleScope.launchWhenCreated {
            presenter.getProjectStatusFlow(projectId)
                .filter { it == ProjectStatus.DELETED }
                .collect { finish() }
        }

        initListeners()

        bind()
    }

    override fun onBackPressed() {
        var handled = drawerManager.onBackPress()

        if (!handled) {
            if (activity_project_content_folder_content_holder.visibility == View.VISIBLE) {
                handled = true
                hideDropDown()
            }
        }

        if (!handled) {
            super.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        drawerManager.refreshSelected()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawerManager.onConfigChange(newConfig)
    }

    override fun onActiveFormAction(
        actionType: ActiveFormsAdapter.FormActionType,
        activeForm: ActiveFormUiModel
    ) {
        when (actionType) {
            ActiveFormsAdapter.FormActionType.CLOSE -> presenter.closeForm(activeForm.internalFormId)

            ActiveFormsAdapter.FormActionType.DELETE -> presenter.deleteForm(activeForm.internalFormId)

            ActiveFormsAdapter.FormActionType.SAME_AS_PREVIOUS -> {
                if (activeForm.formHasPrevious) {
                    ConfirmPreviousDialogFragment.show(
                        internalFormId = activeForm.internalFormId,
                        formName = activeForm.formName,
                        formTypeId = activeForm.formTypeId,
                        fm = supportFragmentManager
                    )
                } else {
                    NoPreviousFormErrorDialogFragment.show(
                        activeForm.formName,
                        supportFragmentManager
                    )
                }
            }

            ActiveFormsAdapter.FormActionType.CLICK -> gotoForm(
                internalFormId = activeForm.internalFormId,
                formName = activeForm.formName,
                formTypeId = activeForm.formTypeId
            )
        }
    }

    override fun onPreviousConfirmed(
        internalFormId: Int,
        formName: String,
        formTypeId: Int
    ) {
        lifecycleScope.launch {
            val sameAsPreviousComplete = presenter.sameAsPrevious(
                projectId = projectId,
                internalFormId = internalFormId,
                formTypeId = formTypeId,
                perDiemNote = getString(R.string.per_diem_copied_from_previous)
            )
            whenResumed {
                if (sameAsPreviousComplete) {
                    gotoForm(
                        formTypeId = formTypeId,
                        formName = formName,
                        internalFormId = internalFormId
                    )
                }
            }
        }
    }

    private fun gotoForm(formTypeId: Int, formName: String, internalFormId: Int) {

        when (formTypeId) {
            SupportedForm.FIELD_REPORT_FORM_ID.formTypeId -> FormDailyFieldReportActivity.start(
                sender = this,
                formTypeName = formName,
                internalFormId = internalFormId,
                projectId = projectId
            )

            SupportedForm.DAILY_LOG_FORM_ID.formTypeId -> DailyLogFormActivity.start(
                this,
                projectId,
                internalFormId
            )

            SupportedForm.TIMESHEET_FORM_ID.formTypeId -> TimeSheetFormActivity.start(
                this,
                projectId,
                internalFormId
            )

            SupportedForm.PSI_FORM_ID.formTypeId -> PsiActivity.start(
                this,
                projectId,
                internalFormId
            )

            SupportedForm.DAILY_DEMO_LOG_ID.formTypeId -> DailyDemoLogActivity.start(
                this,
                projectId,
                internalFormId
            )
        }
    }

    private fun initListeners() {
        activity_project_content_project_folder_iconIV.safeClickListener {
            if (activity_project_content_folder_content_holder.isVisible) {
                hideDropDown()
            } else {
                showDropDown()
            }
        }

        activity_project_content_project_detailsTV.safeClickListener {
            ProjectDetailsActivity.start(this, projectId)
            hideDropDown()
        }

        activity_project_content_backTV.safeClickListener {
            finish()
        }

        activity_project_content_add_formTV.safeClickListener {
            AddFormToProjectDialogFragment.newInstance(projectId)
                .show(supportFragmentManager, AddFormToProjectDialogFragment.TAG)
        }

        activity_project_content_dropdown_dimmer.safeClickListener {
            hideDropDown()
        }

        activity_project_content_employer_certsTV.safeClickListener {
            EmployeesCertificateActivity.start(this, projectId, projectName)
            hideDropDown()
        }

        activity_project_content_start_dayTV.safeClickListener { presenter.startDay(projectId) }

        activity_project_content_end_dayTV.safeClickListener { presenter.endDay(projectId) }
    }

    private fun bind() {
        activity_project_content_add_formTV.isEnabled = presenter.hasAddEditFormsRole
        activity_project_content_employer_certsTV.isVisible = presenter.hasCertificatesRole
    }

    private fun hideDropDown() {
        activity_project_content_project_folder_iconIV.setImageResource(R.drawable.ic_project_options_large)
        activity_project_content_folder_content_holder.circularReveal(false)
        activity_project_content_dropdown_dimmer.animateVisibility(500, 0f)
    }

    private fun showDropDown() {
        activity_project_content_project_folder_iconIV.setImageResource(R.drawable.ic_project_options_open)
        activity_project_content_folder_content_holder.circularReveal(true)
        activity_project_content_dropdown_dimmer.animateVisibility(500, 0.5f)
    }

    companion object {
        fun start(
            sender: Activity,
            projectId: Int,
            projectNumber: String,
            projectName: String
        ) {
            val intent = Intent(sender, ProjectHomeActivity::class.java).apply {
                putExtra(PROJECT_ID, projectId)
                putExtra(PROJECT_NUMBER, projectNumber)
                putExtra(PROJECT_NAME, projectName)
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }

            sender.startActivity(intent)
        }

        private const val PROJECT_ID = "project_id"
        private const val PROJECT_NAME = "project_name"
        private const val PROJECT_NUMBER = "project_number"
    }
}
