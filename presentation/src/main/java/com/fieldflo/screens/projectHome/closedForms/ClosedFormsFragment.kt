package com.fieldflo.screens.projectHome.closedForms

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.fieldflo.R
import com.fieldflo.di.injector
import kotlinx.android.synthetic.main.fragment_forms.*
import kotlinx.coroutines.flow.collect

class ClosedFormsFragment : Fragment(R.layout.fragment_forms) {

    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }

    private val presenter by viewModels<ClosedFormsPresenter> { injector.closedFormViewModelFactory() }

    private val adapter = ClosedFormsAdapter { presenter.reopenForm(it.internalFormId) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        present()
    }

    private fun present() {
        lifecycleScope.launchWhenCreated {
            presenter.getViewState().collect { render(it) }
        }
        presenter.loadClosedForms(projectId)
    }

    private fun initRecyclerView() {
        fragment_forms_listRV.layoutManager = LinearLayoutManager(activity)
        fragment_forms_listRV.adapter = adapter
    }

    private fun render(viewState: ClosedFormsViewState) {
        adapter.submitList(viewState.projectforms)
        fragment_forms_listRV.scrollToPosition(0)
    }

    companion object {
        private const val TAG = "ClosedFormsFragment"
        private const val PROJECT_ID = "project_id"

        fun getIdentifier(projectId: Int) =
            ClosedFormsFragmentIdentifier(TAG, bundleOf(PROJECT_ID to projectId))
    }
}
