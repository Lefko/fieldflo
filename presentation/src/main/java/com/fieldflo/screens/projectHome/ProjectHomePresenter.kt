package com.fieldflo.screens.projectHome

import androidx.lifecycle.ViewModel
import com.fieldflo.AppScopeHolder
import com.fieldflo.usecases.ProjectFormsUseCase
import com.fieldflo.usecases.ProjectStatus
import com.fieldflo.usecases.ProjectsUseCases
import com.fieldflo.usecases.UserRolesUseCases
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

class ProjectHomePresenter @Inject constructor(
    private val projectsUseCases: ProjectsUseCases,
    private val projectFormsUseCase: ProjectFormsUseCase,
    private val rolesUseCases: UserRolesUseCases,
    private val appScopeHolder: AppScopeHolder
) : ViewModel() {

    val hasCertificatesRole: Boolean
        get() = rolesUseCases.hasCertificateRole()

    val hasAddEditFormsRole: Boolean
        get() = rolesUseCases.hasViewEditFormsRole()

    fun getProjectStatusFlow(projectId: Int): Flow<ProjectStatus> {
        return projectsUseCases.getProjectStatusFlow(projectId)
    }

    fun endDay(projectId: Int) {
        appScopeHolder.scope.launch { projectFormsUseCase.endDay(projectId) }
    }

    fun closeForm(internalFormId: Int) {
        appScopeHolder.scope.launch { projectFormsUseCase.closeForm(internalFormId) }
    }

    fun startDay(projectId: Int) {
        appScopeHolder.scope.launch { projectFormsUseCase.startDay(projectId) }
    }

    fun deleteForm(internalFormId: Int) {
        appScopeHolder.scope.launch { projectFormsUseCase.deleteForm(internalFormId) }
    }

    suspend fun sameAsPrevious(
        projectId: Int,
        formTypeId: Int,
        internalFormId: Int,
        perDiemNote: String
    ): Boolean {
        return projectFormsUseCase.sameAsPrevious(
            projectId = projectId,
            formTypeId = formTypeId,
            internalFormId = internalFormId,
            perDiemNote = perDiemNote
        )
    }
}