package com.fieldflo.screens.projectHome.deletedForms

import java.util.*

data class DeletedFormsViewState(
    val projectforms: List<DeletedFormUiModel> = listOf()
)

data class DeletedFormUiModel(
    val internalFormId: Int = 0,
    val projectId: Int = 0,
    val formName: String = "",
    val deletedDate: Date = Date()
)
