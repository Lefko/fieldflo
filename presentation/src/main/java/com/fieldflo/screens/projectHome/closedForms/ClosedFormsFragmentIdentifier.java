package com.fieldflo.screens.projectHome.closedForms;

import android.os.Bundle;
import android.os.Parcel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.fieldflo.common.DynamicFragmentPagerAdapter;

public class ClosedFormsFragmentIdentifier extends DynamicFragmentPagerAdapter.FragmentIdentifier {

    public ClosedFormsFragmentIdentifier(@NonNull String fragmentTag, @Nullable Bundle args) {
        super(fragmentTag, args);
    }

    @SuppressWarnings("WeakerAccess")
    ClosedFormsFragmentIdentifier(Parcel parcel) {
        super(parcel);
    }

    @Override
    protected Fragment createFragment() {
        return new ClosedFormsFragment();
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    public static final Creator<ClosedFormsFragmentIdentifier> CREATOR = new Creator<ClosedFormsFragmentIdentifier>() {

        @Override
        public ClosedFormsFragmentIdentifier createFromParcel(Parcel source) {
            return new ClosedFormsFragmentIdentifier(source);
        }

        @Override
        public ClosedFormsFragmentIdentifier[] newArray(int size) {
            return new ClosedFormsFragmentIdentifier[size];
        }
    };
}
