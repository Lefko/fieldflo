package com.fieldflo.screens.projectHome.closedForms

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fieldflo.data.persistence.db.entities.ClosedFormData
import com.fieldflo.usecases.ProjectFormsUseCase
import com.fieldflo.usecases.UserRolesUseCases
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class ClosedFormsPresenter @Inject constructor(
    private val userRolesUseCases: UserRolesUseCases,
    private val projectFormsUseCase: ProjectFormsUseCase
) : ViewModel() {

    private val viewState = MutableStateFlow(ClosedFormsViewState())

    fun getViewState(): Flow<ClosedFormsViewState> = viewState.onEach { Timber.d("$it") }

    fun loadClosedForms(projectId: Int) {
        if (userRolesUseCases.hasViewEditFormsRole()) {
            viewModelScope.launch {
                projectFormsUseCase.getClosedFormsFlow(projectId)
                    .collect { closedForms ->
                        val previousState = viewState.value
                        val newState =
                            previousState.copy(projectforms = closedForms.map { it.toUiModel() })
                        viewState.value = newState
                    }
            }
        }
    }

    fun reopenForm(internalFormId: Int) {
        viewModelScope.launch {
            projectFormsUseCase.reopenForm(internalFormId)
        }
    }
}

private fun ClosedFormData.toUiModel() = ClosedFormUiModel(
    internalFormId = this.internalFormId,
    projectId = this.projectId,
    formName = this.formName,
    closeDate = this.closeDate,
    isSynced = this.isCloseSynced,
    hasSyncError = this.hasSyncError
)
