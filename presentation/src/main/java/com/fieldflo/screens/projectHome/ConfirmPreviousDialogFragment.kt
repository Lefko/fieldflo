package com.fieldflo.screens.projectHome

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.fieldflo.R

class ConfirmPreviousDialogFragment : DialogFragment() {

    private lateinit var callback: ConfirmPreviousCallback

    private val internalFormId: Int by lazy { arguments?.getInt(INTERNAL_FORM_ID_KEY) ?: 0 }
    private val formTypeId: Int by lazy { arguments?.getInt(FORM_TYPE_ID_KEY) ?: 0 }
    private val formName: String by lazy { arguments?.getString(FORM_NAME_KEY)!! }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    private fun onAttachToContext(context: Context) {
        var callbackSet = false
        if (parentFragment != null) {
            if (parentFragment is ConfirmPreviousCallback) {
                callback = parentFragment as ConfirmPreviousCallback
                callbackSet = true
            }
        } else {
            if (context is ConfirmPreviousCallback) {
                callback = context
                callbackSet = true
            }
        }

        if (!callbackSet) return
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val context = activity as Context

        val errorMessage = context.getString(R.string.project_home_active_forms_previous_confirm)

        return AlertDialog.Builder(context)
            .setMessage(errorMessage)
            .setPositiveButton(android.R.string.ok) { _, _ -> callback.onPreviousConfirmed(
                internalFormId = internalFormId,
                formName = formName,
                formTypeId = formTypeId
            )}
            .setNegativeButton(android.R.string.cancel) { _, _ -> }
            .show()
    }

    companion object {
        private const val TAG = "ConfirmPreviousDialogFragment"

        private const val INTERNAL_FORM_ID_KEY = "internal_form_id_key"
        private const val FORM_TYPE_ID_KEY = "form_type_id_key"
        private const val FORM_NAME_KEY = "form_name_key"
        @JvmStatic
        private fun newInstance(internalFormId: Int, formName: String, formTypeId: Int) = ConfirmPreviousDialogFragment().apply {
            arguments = bundleOf(
                INTERNAL_FORM_ID_KEY to internalFormId,
                FORM_NAME_KEY to formName,
                FORM_TYPE_ID_KEY to formTypeId
            )
        }

        @JvmStatic
        fun show(internalFormId: Int, formName: String, formTypeId: Int, fm: FragmentManager) {
            val frag =
                newInstance(
                    internalFormId,
                    formName,
                    formTypeId
                )
            frag.show(fm, TAG)
        }
    }

    interface ConfirmPreviousCallback {
        fun onPreviousConfirmed(internalFormId: Int, formName: String, formTypeId: Int)
    }
}