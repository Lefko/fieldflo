package com.fieldflo.screens.projectHome.activeForms

import java.util.*

data class ActiveFormUiModel(
    val internalFormId: Int = 0,
    val formTypeId: Int = 0,
    val projectId: Int = 0,
    val formName: String = "",
    val formDate: Date = Date(),
    val hasPreviousDayOption: Boolean = false,
    val formHasPrevious: Boolean,
    var menuOpen: Boolean = false,
    var isClosable: Boolean = false,

    val sameAsPreviousComplete: Boolean = false
)
