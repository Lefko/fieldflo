package com.fieldflo.screens.projectHome.deletedForms

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.fieldflo.R
import com.fieldflo.di.injector
import kotlinx.android.synthetic.main.fragment_forms.*
import kotlinx.coroutines.flow.collect

class DeletedFormsFragment : Fragment(R.layout.fragment_forms) {

    private val projectId by lazy { arguments!!.getInt(PROJECT_ID) }

    private val presenter by viewModels<DeletedFormsPresenter> { injector.deletedFormViewModelFactory() }

    private val adapter = DeletedFormsAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        present()
    }

    private fun present() {
        lifecycleScope.launchWhenCreated {
            presenter.getViewState().collect { render(it) }
        }
        presenter.loadDeletedForms(projectId)
    }

    private fun initRecyclerView() {
        fragment_forms_listRV.layoutManager = LinearLayoutManager(activity)
        fragment_forms_listRV.adapter = adapter
    }

    private fun render(viewState: DeletedFormsViewState) {
        adapter.submitList(viewState.projectforms)
    }

    companion object {
        private const val TAG = "DeletedFormsFragment"
        private const val PROJECT_ID = "project_id"

        fun getIdentifier(projectId: Int) =
            DeletedFormsFragmentIdentifier(TAG, bundleOf(PROJECT_ID to projectId))
    }
}
