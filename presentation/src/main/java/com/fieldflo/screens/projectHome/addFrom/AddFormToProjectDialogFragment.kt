package com.fieldflo.screens.projectHome.addFrom

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.ListView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.common.safeClickListener
import com.fieldflo.di.injector

class AddFormToProjectDialogFragment : DialogFragment() {

    private val projectId: Int by lazy { arguments!!.getInt(PROJECT_ID) }
    private val presenter by viewModels<AddFormPresenter> { injector.addFormViewModelFactory() }

    private val availableFormsAdapter by lazy { AvailableFormsAdapter(activity!!) }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        present()

        val dialog = AlertDialog.Builder(activity as Context)
            .setAdapter(availableFormsAdapter, null)
            .setPositiveButton(android.R.string.ok, null)
            .setNegativeButton(android.R.string.cancel, { _, _ ->
                dismiss()
            })
            .show()

        dialog.listView.itemsCanFocus = false
        dialog.listView.choiceMode = ListView.CHOICE_MODE_MULTIPLE

        return dialog
    }

    override fun onStart() {
        super.onStart()
        val alertDialog = dialog as AlertDialog
        val positiveButton = alertDialog.getButton(Dialog.BUTTON_POSITIVE)
        positiveButton.safeClickListener {
            val listView = (dialog as AlertDialog).listView

            val selectedForms = (0 until listView.adapter.count)
                .filter { listView.checkedItemPositions[it] }
                .mapNotNull { availableFormsAdapter.getItem(it)?.formTypeId }

            if (selectedForms.isEmpty()) {
                dismiss()
            } else {
                lifecycleScope.launchWhenStarted {
                    presenter.addFormsToProject(projectId, selectedForms)
                    dismiss()
                }
            }
        }
    }

    private fun present() {
        lifecycleScope.launchWhenCreated {
            val availableForms = presenter.getAvailableForms(projectId)
            render(availableForms)
        }
    }

    private fun render(availableForms: List<AvailableFormUiRow>) {
        availableFormsAdapter.clear()
        availableFormsAdapter.addAll(availableForms)
        availableFormsAdapter.notifyDataSetChanged()
    }

    companion object {
        const val TAG = "AddFormToProjectDialogFragment"
        private const val PROJECT_ID = "project_id"

        @JvmStatic
        fun newInstance(projectId: Int) =
            AddFormToProjectDialogFragment().apply {
                arguments = Bundle().apply {
                    putInt(PROJECT_ID, projectId)
                }
            }
    }
}