package com.fieldflo.screens.projectHome.deletedForms

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fieldflo.data.persistence.db.entities.DeletedFormData
import com.fieldflo.usecases.ProjectFormsUseCase
import com.fieldflo.usecases.UserRolesUseCases
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class DeletedFormsPresenter @Inject constructor(
    private val userRolesUseCases: UserRolesUseCases,
    private val projectFormsUseCase: ProjectFormsUseCase
) : ViewModel() {

    private val viewState = MutableStateFlow(DeletedFormsViewState())

    fun getViewState(): Flow<DeletedFormsViewState> = viewState.onEach { Timber.d("$it") }

    fun loadDeletedForms(projectId: Int) {
        if (userRolesUseCases.hasViewEditFormsRole()) {
            viewModelScope.launch {
                projectFormsUseCase.getDeletedFormsFlow(projectId)
                    .collect { deletedForms ->
                        val previousState = viewState.value
                        val newState =
                            previousState.copy(projectforms = deletedForms.map { it.toUiModel() })
                        viewState.value = newState
                    }
            }
        }
    }
}

private fun DeletedFormData.toUiModel() = DeletedFormUiModel(
    internalFormId = this.internalFormId,
    projectId = this.projectId,
    formName = this.formName,
    deletedDate = this.deleteDate
)