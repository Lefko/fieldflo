package com.fieldflo.screens.addCompany

data class AddCompanyViewState(
    val companies: List<BasicCompanyUiModel> = listOf(),
    val companyAddSuccess: Boolean = false
)

data class BasicCompanyUiModel(val securityKey: String, val token: String, val companyName: String)