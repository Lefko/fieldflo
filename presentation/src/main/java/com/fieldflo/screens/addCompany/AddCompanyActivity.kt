package com.fieldflo.screens.addCompany

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.fieldflo.R
import com.fieldflo.di.injector
import com.fieldflo.screens.manualAddCompany.ManualAddCompanyActivity
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.activity_add_company.*
import kotlinx.coroutines.flow.collect
import org.json.JSONObject

class AddCompanyActivity : AppCompatActivity() {

    companion object {
        @JvmStatic
        fun start(sender: Activity) {
            sender.startActivity(
                Intent(sender, AddCompanyActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
                }
            )
        }
    }

    private val presenter by viewModels<AddCompanyPresenter> { injector.addCompanyViewModelFactory() }

    private val adapter = AddCompanyAdapter { securityKey, token ->
        presenter.removeCompany(securityKey, token)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_company)

        initRecyclerView()

        initObservables()

        present()
    }

    private fun initRecyclerView() {
        activity_add_company_company_listRV.layoutManager = LinearLayoutManager(this)
        activity_add_company_company_listRV.adapter = adapter
    }

    private fun initObservables() {
        activity_add_company_qr_code_iconCV.setOnClickListener {
            IntentIntegrator(this)
                .setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)
                .setOrientationLocked(false)
                .setBeepEnabled(true)
                .setBarcodeImageEnabled(false)
                .initiateScan()
        }

        activity_add_company_add_manual_iconIV.setOnClickListener {
            ManualAddCompanyActivity.start(this)
        }

        activity_add_company_login.setOnClickListener { finish() }
    }

    private fun present() {
        lifecycleScope.launchWhenCreated {
            presenter.viewState().collect { render(it) }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents != null) {
                val qrCodeJson = JSONObject(result.contents)
                val name: String = qrCodeJson.get("Name") as String
                val baseUrl: String = qrCodeJson.get("URL") as String
                val apiToken: String = qrCodeJson.get("API Token") as String
                val secToken: String = qrCodeJson.get("Securty Key") as String

                presenter.addCompany(name, baseUrl, apiToken, secToken)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun render(viewState: AddCompanyViewState) {
        if (viewState.companyAddSuccess) finish()
        adapter.submitList(viewState.companies)
    }
}
