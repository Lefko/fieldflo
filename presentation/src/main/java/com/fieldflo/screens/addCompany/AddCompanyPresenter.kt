package com.fieldflo.screens.addCompany

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fieldflo.data.persistence.db.entities.CompanyData
import com.fieldflo.usecases.AddCompanyParams
import com.fieldflo.usecases.CompanyUseCases
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class AddCompanyPresenter @Inject constructor(
    private val companiesUseCases: CompanyUseCases
) :
    ViewModel() {

    private val viewStateObservable = MutableStateFlow(AddCompanyViewState())

    init {
        viewModelScope.launch {
            companiesUseCases.getCompaniesListFlow()
                .collect { companies ->
                    val previousState = viewStateObservable.value
                    viewStateObservable.value = previousState.copy(
                        companies = companies.map { it.toUiModel() }
                    )
                }
        }
    }

    private fun CompanyData.toUiModel(): BasicCompanyUiModel =
        BasicCompanyUiModel(
            this.securityKey,
            this.token,
            this.companyName
        )

    fun viewState(): StateFlow<AddCompanyViewState> = viewStateObservable

    fun addCompany(companyName: String, baseUrl: String, apiToken: String, secToken: String) {
        val addCompanyAction = AddCompanyParams(companyName, baseUrl, apiToken, secToken)
        viewModelScope.launch(Dispatchers.IO) {
            companiesUseCases.addCompany(addCompanyAction)
            viewStateObservable.value = viewStateObservable.value.copy(companyAddSuccess = true)
        }
    }

    fun removeCompany(securityKey: String, token: String) {
        viewModelScope.launch(Dispatchers.IO) {
            companiesUseCases.deleteCompany(securityKey, token)
        }
    }
}
