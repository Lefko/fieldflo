package com.fieldflo.screens.addCompany

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fieldflo.R
import com.fieldflo.common.safeClickListener

class AddCompanyAdapter(private val onDeleteClick: (securityKey: String, token: String) -> Unit) :
    ListAdapter<BasicCompanyUiModel, BasicCompanyViewHolder>(CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BasicCompanyViewHolder {
        val rowLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_add_company_existing_company, parent, false) as ViewGroup
        val holder = BasicCompanyViewHolder(rowLayout)

        holder.deleteTV.safeClickListener {
            val position = holder.bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val company = getItem(position)
                onDeleteClick(company.securityKey, company.token)
            }
        }

        return holder
    }

    override fun onBindViewHolder(holder: BasicCompanyViewHolder, position: Int) {
        val uiModel = getItem(position)
        holder.bind(uiModel)
    }
}

class BasicCompanyViewHolder(rowLayout: ViewGroup) : RecyclerView.ViewHolder(rowLayout) {

    private val companyNameTV: TextView =
        rowLayout.findViewById(R.id.row_add_company_existing_company_company_nameTV)
    val deleteTV: TextView = rowLayout.findViewById(R.id.row_add_company_existing_company_removeTV)

    fun bind(uiModel: BasicCompanyUiModel) {
        companyNameTV.text = uiModel.companyName
    }
}

private val CALLBACK = object : DiffUtil.ItemCallback<BasicCompanyUiModel>() {

    override fun areItemsTheSame(oldItem: BasicCompanyUiModel, newItem: BasicCompanyUiModel) =
        oldItem.securityKey == newItem.securityKey && oldItem.token == newItem.token

    override fun areContentsTheSame(oldItem: BasicCompanyUiModel, newItem: BasicCompanyUiModel) =
        oldItem.companyName == newItem.companyName
}