package com.fieldflo.screens.formDailyDemoLog.trucking.addTruck

import android.app.Dialog
import android.os.Bundle
import android.widget.ListView
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector

class AddTruckingDialogFragment : DialogFragment() {
    private val viewModel by viewModels<AddTruckingViewModel> { injector.addTruckingViewModelFactory() }

    private val haulersAdapter by lazy { TruckingAdapter(requireContext()) }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = AlertDialog.Builder(requireContext())
            .setTitle(R.string.add_hauler_dialog_fragment_title)
            .setAdapter(haulersAdapter, null)
            .setPositiveButton(android.R.string.ok, { _, _ ->
                val listView = (dialog as AlertDialog).listView
                val selectedSubcontractors = (0 until listView.adapter.count)
                    .filter { listView.checkedItemPositions[it] }
                    .mapNotNull { haulersAdapter.getItem(it) }
                setFragmentResult(
                    HAULER_SELECTED_RESULT_KEY,
                    bundleOf(HAULER_KEY to selectedSubcontractors[0])
                )
            })
            .setNegativeButton(android.R.string.cancel, { _, _ ->
                dismiss()
            })
            .show()

        dialog.listView.itemsCanFocus = false
        dialog.listView.choiceMode = ListView.CHOICE_MODE_SINGLE

        lifecycleScope.launchWhenCreated {
            val employees = viewModel.getAllTrucking()
            haulersAdapter.clear()
            haulersAdapter.addAll(employees)
            haulersAdapter.notifyDataSetChanged()
        }

        return dialog
    }

    companion object {
        const val TAG = "AddTruckingDialogFragment"
        const val HAULER_SELECTED_RESULT_KEY = "subcontractor selected result key"
        const val HAULER_KEY = "subcontractor_key"

        fun newInstance() = AddTruckingDialogFragment()
    }
}