package com.fieldflo.screens.formDailyDemoLog.miscComments

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import com.fieldflo.R

class ImagePickerOptionsDialog : AppCompatDialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(context!!)
            .setItems(R.array.activity_daily_form_picker_options_array) { _, which ->
                val pickerOption = when (which) {
                    0 -> PickerOption.TAKE_PIC
                    1 -> PickerOption.GALLERY
                    else -> PickerOption.CANCELLED
                }

                setFragmentResult(
                    IMAGE_PICKER_RESULT_KEY,
                    bundleOf(IMAGE_PICKER_OPTION to pickerOption)
                )
            }
            .show()
    }

    companion object {
        const val TAG = "ImagePickerOptionsDialog"
        const val IMAGE_PICKER_RESULT_KEY = "image picker result key"
        const val IMAGE_PICKER_OPTION = "image picker option"

        fun newInstance() = ImagePickerOptionsDialog()
    }

    enum class PickerOption {
        TAKE_PIC, GALLERY, CANCELLED
    }
}