package com.fieldflo.screens.formDailyDemoLog.materials.addMaterial

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.afterTextChanges
import com.fieldflo.common.safeClickListener
import com.fieldflo.di.injector
import kotlinx.android.synthetic.main.dialog_add_material.view.*
import kotlin.properties.Delegates

class AddMaterialDialogFragment : DialogFragment() {

    private val viewModel by viewModels<AddMaterialViewModel> { injector.addMaterialViewModelFactory() }

    private lateinit var fragmentView: View
    private lateinit var positiveButton: Button
    private var selectedMaterial: MaterialUiModel? by Delegates.observable(null) { _, _, _ ->
        positiveButton.isEnabled = positiveEnabled
    }

    private val positiveEnabled get() = selectedMaterial != null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        fragmentView = View.inflate(context, R.layout.dialog_add_material, null)
        return AlertDialog.Builder(requireContext())
            .setView(fragmentView)
            .setPositiveButton(android.R.string.ok, null)
            .setNegativeButton(android.R.string.cancel) { _, _ -> dismiss() }
            .show()
    }

    override fun onStart() {
        super.onStart()
        positiveButton = (dialog as AlertDialog).getButton(Dialog.BUTTON_POSITIVE).apply {
            isEnabled = false
            safeClickListener {
                setFragmentResult(
                    MATERIAL_SELECTED_RESULT_KEY,
                    bundleOf(MATERIAL_KEY to selectedMaterial)
                )
                dismiss()
            }
        }

        lifecycleScope.launchWhenCreated {
            val materials = viewModel.getAllMaterials()
            initView(materials)
        }
    }

    private fun initView(materials: List<MaterialUiModel>) {
        fragmentView.dialog_add_material_search.apply {
            threshold = 1
            setAdapter(MaterialAdapter(requireContext(), materials))
            setOnItemClickListener { parent, _, position, _ ->
                selectedMaterial = parent.getItemAtPosition(position) as? MaterialUiModel
            }
            afterTextChanges { _, _ ->
                selectedMaterial = null
            }
        }
        fragmentView.dialog_add_material_search.isVisible = true
        fragmentView.dialog_add_material_progressbar.isVisible = false
    }

    companion object {
        const val TAG = "AddMaterialDialogFragment"
        const val MATERIAL_SELECTED_RESULT_KEY = "material selected result key"
        const val MATERIAL_KEY = "material_key"

        fun newInstance() = AddMaterialDialogFragment()
    }
}