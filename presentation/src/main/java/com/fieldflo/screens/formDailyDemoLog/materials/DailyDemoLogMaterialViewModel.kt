package com.fieldflo.screens.formDailyDemoLog.materials

import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.DailyDemoLogMaterialUiData
import com.fieldflo.usecases.DailyDemoLogUseCases
import javax.inject.Inject

class DailyDemoLogMaterialViewModel @Inject constructor(
    private val dailyDemoLogUseCases: DailyDemoLogUseCases
) : ViewModel() {

    suspend fun getMaterials(
        internalFormId: Int
    ): List<DailyDemoLogMaterialUiData> {
        return dailyDemoLogUseCases.getDailyDemoMaterials(internalFormId)
    }
}