package com.fieldflo.screens.formDailyDemoLog.employees

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.Employee
import com.fieldflo.usecases.DailyDemoLogEmployeeUiData
import com.fieldflo.usecases.DailyDemoLogUseCases
import com.fieldflo.usecases.EmployeeUseCases
import javax.inject.Inject

class DailyDemoLogEmployeesViewModel @Inject constructor(
    private val dailyDemoLogUseCases: DailyDemoLogUseCases,
    private val employeeUseCases: EmployeeUseCases
) : ViewModel() {

    suspend fun getEmployees(
        internalFormId: Int,
        projectId: Int
    ): List<DailyDemoLogEmployeeUiData> {
        return dailyDemoLogUseCases.getDailyDemoLogEmployees(internalFormId, projectId)
    }

    suspend fun getEmployee(employeeId: Int): Employee? =
        employeeUseCases.getEmployeeById(employeeId)
}
