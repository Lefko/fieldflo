package com.fieldflo.screens.formDailyDemoLog.subContractors

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fieldflo.R
import com.fieldflo.common.afterTextChanges
import com.fieldflo.common.safeClickListener
import com.fieldflo.usecases.DailyDemoLogSubcontractorUiData

class SubcontractorsAdapter : RecyclerView.Adapter<DailyDemoLogSubcontractorViewHolder>() {

    private val subcontractors = mutableListOf<DailyDemoLogSubcontractorUiData>()

    fun setSubcontractors(subcontractors: List<DailyDemoLogSubcontractorUiData>) {
        this.subcontractors.clear()
        this.subcontractors.addAll(subcontractors)
        notifyDataSetChanged()
    }

    fun addSubcontractor(subcontractor: DailyDemoLogSubcontractorUiData) {
        val previousSize = subcontractors.size
        subcontractors.add(subcontractor)
        notifyItemRangeInserted(previousSize, subcontractors.size)
    }

    fun getSubcontractors(): List<DailyDemoLogSubcontractorUiData> = subcontractors.toList()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DailyDemoLogSubcontractorViewHolder {
        return DailyDemoLogSubcontractorViewHolder(parent, ::deleteAt, { subcontractors })
    }

    override fun onBindViewHolder(holder: DailyDemoLogSubcontractorViewHolder, position: Int) {
        holder.bind(subcontractors[position])
    }

    override fun getItemCount(): Int = subcontractors.size

    private fun deleteAt(position: Int) {
        subcontractors.removeAt(position)
        notifyItemRemoved(position)
    }
}

class DailyDemoLogSubcontractorViewHolder(
    parent: ViewGroup,
    deleteEmployee: (position: Int) -> Unit,
    rowListProvider: () -> MutableList<DailyDemoLogSubcontractorUiData>
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(
        R.layout.row_daily_demo_log_subcontractor,
        parent,
        false
    )
) {

    private val deleteIV =
        itemView.findViewById<ImageView>(R.id.subcontractor_item_row_remove_subcontractor)
    private val subcontractorName =
        itemView.findViewById<TextView>(R.id.subcontractor_item_row_subcontractor_value)
    private val phaseName = itemView.findViewById<EditText>(R.id.subcontractor_item_row_phase_value)
    private val hours = itemView.findViewById<EditText>(R.id.subcontractor_item_row_hours_value)

    init {
        deleteIV.safeClickListener {
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                deleteEmployee(position)
            }
        }

        phaseName.afterTextChanges { text, _ ->
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val subcontractors = rowListProvider()
                val changedSubcontractor = subcontractors[position].copy(phase = text)
                subcontractors[position] = changedSubcontractor
            }
        }

        hours.afterTextChanges { text, _ ->
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val subcontractors = rowListProvider()
                val changedSubcontractor = subcontractors[position].copy(hours = text)
                subcontractors[position] = changedSubcontractor
            }
        }
    }

    fun bind(subcontractor: DailyDemoLogSubcontractorUiData) {
        subcontractorName.text = subcontractor.subcontractorName
        phaseName.setText(subcontractor.phase)
        hours.setText(subcontractor.hours)
    }
}
