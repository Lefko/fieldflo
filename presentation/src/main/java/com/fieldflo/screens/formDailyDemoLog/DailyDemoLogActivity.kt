package com.fieldflo.screens.formDailyDemoLog

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.hideKeyboard
import com.fieldflo.common.onPageChanges
import com.fieldflo.common.safeClickListener
import com.fieldflo.common.toSimpleString
import com.fieldflo.di.injector
import com.fieldflo.screens.drawer.DrawerManager
import com.fieldflo.screens.formDailyDemoLog.employees.DailyDemoLogEmployeesFragment
import com.fieldflo.screens.formDailyDemoLog.equipment.EquipmentFragment
import com.fieldflo.screens.formDailyDemoLog.generalInformation.DailyDemoGeneralInfoSaveData
import com.fieldflo.screens.formDailyDemoLog.generalInformation.DailyDemoGeneralInformationFragment
import com.fieldflo.screens.formDailyDemoLog.materials.MaterialsFragment
import com.fieldflo.screens.formDailyDemoLog.miscComments.MiscCommentsFragment
import com.fieldflo.screens.formDailyDemoLog.miscItems.MiscItemsFragment
import com.fieldflo.screens.formDailyDemoLog.productionNarrative.ProductionNarrativeFragment
import com.fieldflo.screens.formDailyDemoLog.subContractors.SubContractorsFragment
import com.fieldflo.screens.formDailyDemoLog.trucking.TruckingFragment
import com.fieldflo.usecases.*
import kotlinx.android.synthetic.main.activity_daily_demo_log.*
import kotlinx.android.synthetic.main.view_activity_demo_daily_log_form.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter

class DailyDemoLogActivity : AppCompatActivity(R.layout.activity_daily_demo_log) {

    private val drawerManager by lazy {
        DrawerManager(
            { demo_log_drawer_layout }, { this },
            { activity_daily_demo_log_form_content_toolbar }
        )
    }

    private val viewModel by viewModels<DailyDemoLogFormViewModel> { injector.dailyDemoLogFormViewModelFactory() }
    private val internalFormId: Int by lazy { intent.getIntExtra(INTERNAL_FORM_ID_KEY, 0) }
    private val projectId: Int by lazy { intent.getIntExtra(PROJECT_ID_KEY, 0) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initializeViewpager()

        initializeTexts()

        observePageChanges()

        initializeClickListeners()

        lifecycleScope.launchWhenCreated {
            viewModel.getProjectStatusFlow(projectId)
                .filter { it == ProjectStatus.DELETED }
                .collect { finish() }
        }
    }

    private fun initializeClickListeners() {
        activity_daily_demo_log_form_content_backTV.safeClickListener { finish() }

        activity_daily_demo_log_form_content_saveTV.safeClickListener { finish() }

        activity_daily_demo_log_form_content_previousTV.safeClickListener {
            if (activity_daily_demo_log_form_pages.currentItem > 0) {
                activity_daily_demo_log_form_pages.currentItem--
            }
        }

        activity_daily_demo_log_form_content_nextTV.safeClickListener {
            if (activity_daily_demo_log_form_pages.currentItem < activity_daily_demo_log_form_pages.childCount) {
                activity_daily_demo_log_form_pages.currentItem++
            }
        }
    }

    private fun initializeViewpager() {
        lifecycleScope.launchWhenCreated {
            val tabsToDisplay = viewModel.getTabs(projectId)
            val adapter = DailyDemoLogFormPagerAdapter(
                supportFragmentManager,
                internalFormId,
                projectId,
                this@DailyDemoLogActivity,
                tabsToDisplay
            )

            activity_daily_demo_log_form_pages.adapter = adapter
            activity_daily_demo_log_form_pages.offscreenPageLimit = tabsToDisplay.size
        }

        activity_daily_demo_log_form_tabs.setupWithViewPager(activity_daily_demo_log_form_pages)
    }

    private fun observePageChanges() {
        activity_daily_demo_log_form_pages.post {
            activity_daily_demo_log_form_pages.onPageChanges {
                hideKeyboard()
                renderNextPreviousEnabled(it)
            }
        }
    }

    private fun initializeTexts() {
        lifecycleScope.launchWhenCreated {
            val activityUiData = viewModel.getDailyDemoLogActivityUiData(internalFormId, projectId)
            val projectNumberName =
                "${activityUiData.projectNumber} - ${activityUiData.projectName}"
            activity_daily_demo_log_form_content_project_number_and_name.text = projectNumberName
            val formTypeAndDateString = getString(
                R.string.activity_daily_demo_log_type_date,
                activityUiData.formDate.toSimpleString()
            )
            activity_daily_demo_log_form_content_page_title.text = formTypeAndDateString
        }
    }

    private fun renderNextPreviousEnabled(currentPage: Int) {
        activity_daily_demo_log_form_content_previousTV.isEnabled = currentPage > 0
        activity_daily_demo_log_form_content_nextTV.isEnabled =
            currentPage < activity_daily_demo_log_form_pages.childCount - 1
    }

    override fun onBackPressed() {
        if (!drawerManager.onBackPress()) {
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        val saveData = extractData()
        viewModel.save(saveData)
        super.onDestroy()
    }

    private fun extractData(): DailyDemoLogSaveData {
        var generalInfoSaveData: DailyDemoGeneralInfoSaveData? = null
        var productionNarrative: DailyDemoLogProductionNarrativeUiData? = null
        var employees: List<DailyDemoLogEmployeeUiData>? = null
        var subContractors: List<DailyDemoLogSubcontractorUiData>? = null
        var equipment: List<DailyDemoLogEquipmentUiData>? = null
        var materials: List<DailyDemoLogMaterialUiData>? = null
        var trucking: List<DailyDemoLogTruckingUiData>? = null
        var miscItems: DailyDemoLogMiscItems? = null
        var comments: String? = null

        supportFragmentManager.fragments.forEach {
            when (it) {
                is DailyDemoGeneralInformationFragment -> generalInfoSaveData = it.extractState()
                is ProductionNarrativeFragment -> productionNarrative = it.extractState()
                is DailyDemoLogEmployeesFragment -> employees = it.extractState()
                is SubContractorsFragment -> subContractors = it.extractState()
                is EquipmentFragment -> equipment = it.extractState()
                is MaterialsFragment -> materials = it.extractState()
                is TruckingFragment -> trucking = it.extractState()
                is MiscItemsFragment -> miscItems = it.extractState()
                is MiscCommentsFragment -> comments = it.extractState()
            }
        }
        return DailyDemoLogSaveData(
            projectId,
            internalFormId,
            generalInfoSaveData,
            productionNarrative,
            employees,
            subContractors,
            equipment,
            materials,
            trucking,
            miscItems,
            comments
        )
    }

    companion object {
        private const val PROJECT_ID_KEY = "project_id"
        private const val INTERNAL_FORM_ID_KEY = "internal_form_id"

        fun start(sender: Activity, projectId: Int, internalFormId: Int) {
            val intent = Intent(sender, DailyDemoLogActivity::class.java).apply {
                putExtra(INTERNAL_FORM_ID_KEY, internalFormId)
                putExtra(PROJECT_ID_KEY, projectId)
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            sender.startActivity(intent)
        }
    }
}