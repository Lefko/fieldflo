package com.fieldflo.screens.formDailyDemoLog.materials

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fieldflo.R
import com.fieldflo.common.afterTextChanges
import com.fieldflo.common.safeClickListener
import com.fieldflo.usecases.DailyDemoLogMaterialUiData

class MaterialsAdapter : RecyclerView.Adapter<DailyDemoLogMaterialViewHolder>() {

    private val material = mutableListOf<DailyDemoLogMaterialUiData>()

    fun setMaterial(equipment: List<DailyDemoLogMaterialUiData>) {
        this.material.clear()
        this.material.addAll(equipment)
        notifyDataSetChanged()
    }

    fun addMaterial(newEquipment: DailyDemoLogMaterialUiData) {
        val previousSize = material.size
        material.add(newEquipment)
        notifyItemRangeInserted(previousSize, material.size)
    }

    fun getEquipment(): List<DailyDemoLogMaterialUiData> = material.toList()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DailyDemoLogMaterialViewHolder {
        return DailyDemoLogMaterialViewHolder(parent, ::deleteAt, { material })
    }

    override fun onBindViewHolder(holder: DailyDemoLogMaterialViewHolder, position: Int) {
        holder.bind(material[position])
    }

    override fun getItemCount(): Int = material.size

    private fun deleteAt(position: Int) {
        material.removeAt(position)
        notifyItemRemoved(position)
    }
}

class DailyDemoLogMaterialViewHolder(
    parent: ViewGroup,
    deleteMaterial: (position: Int) -> Unit,
    rowListProvider: () -> MutableList<DailyDemoLogMaterialUiData>
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(
        R.layout.row_daily_demo_log_material,
        parent,
        false
    )
) {

    private val deleteIV =
        itemView.findViewById<ImageView>(R.id.row_daily_demo_log_material_remove_material)

    private val materialName =
        itemView.findViewById<TextView>(R.id.row_daily_demo_log_material_name_value)

    private val materialQuantity =
        itemView.findViewById<EditText>(R.id.row_daily_demo_log_material_quantity_value)

    private val materialAmount =
        itemView.findViewById<EditText>(R.id.row_daily_demo_log_material_amount_value)

    private val materialTotal =
        itemView.findViewById<EditText>(R.id.row_daily_demo_log_material_total_value)

    init {
        deleteIV.safeClickListener {
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                deleteMaterial(position)
            }
        }

        materialQuantity.afterTextChanges { text, _ ->
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val material = rowListProvider()
                val changedEquipment = material[position].copy(quantity = text)
                material[position] = changedEquipment
            }
        }

        materialAmount.afterTextChanges { text, _ ->
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val material = rowListProvider()
                val changedEquipment = material[position].copy(amount = text)
                material[position] = changedEquipment
            }
        }

        materialTotal.afterTextChanges { text, _ ->
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val material = rowListProvider()
                val changedEquipment = material[position].copy(total = text)
                material[position] = changedEquipment
            }
        }
    }


    fun bind(material: DailyDemoLogMaterialUiData) {
        materialName.text = material.materialName
        materialQuantity.setText(material.quantity)
        materialAmount.setText(material.amount)
        materialTotal.setText(material.total)
    }
}