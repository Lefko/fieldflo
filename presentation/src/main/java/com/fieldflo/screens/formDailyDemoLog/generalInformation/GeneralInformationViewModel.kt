package com.fieldflo.screens.formDailyDemoLog.generalInformation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fieldflo.common.toDay
import com.fieldflo.common.toSimpleString
import com.fieldflo.usecases.DailyDemoLogGeneralInfoUiData
import com.fieldflo.usecases.DailyDemoLogUseCases
import com.fieldflo.usecases.EmployeeUseCases
import com.fieldflo.usecases.PinUseCases
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import java.io.File
import java.util.*
import javax.inject.Inject

class GeneralInformationViewModel @Inject constructor(
    private val employeeUseCases: EmployeeUseCases,
    private val dailyDemoLogUseCases: DailyDemoLogUseCases
) : ViewModel() {

    private val _supervisorData =
        MutableStateFlow(DemoDailyLogEmployeeAndVerificationData.createEmpty())
    val supervisorData: StateFlow<DemoDailyLogEmployeeAndVerificationData> = _supervisorData

    suspend fun getViewState(internalFormId: Int, projectId: Int): GeneralInformationViewState {
        val generalInfoData = dailyDemoLogUseCases.getGeneralInfoData(internalFormId, projectId)
        _supervisorData.value = DemoDailyLogEmployeeAndVerificationData(
            employeeName = generalInfoData.supervisor?.employeeFullName ?: "",
            employeeId = generalInfoData.supervisor?.employeeId ?: 0,
            employeePinNumber = generalInfoData.supervisor?.employeePinNumber ?: "",
            verificationData = createDemoDailyLogVerificationData(
                isVerified = generalInfoData.supervisorVerified,
                verifiedEmployeeId = generalInfoData.supervisor?.employeeId ?: 0,
                verifiedDate = generalInfoData.supervisorVerifiedDate,
                verifiedByEmployeeId = generalInfoData.supervisorVerifiedByEmployeeId,
                pinOutLocation = generalInfoData.pinOutLocation
            )
        )
        return generalInfoData.toViewState()
    }

    fun supervisorChanged(supervisorId: Int) {
        viewModelScope.launch {
            val newSupervisor = employeeUseCases.getEmployeeById(supervisorId)
            val currentSupervisorData = _supervisorData.value
            val newSupervisorData = currentSupervisorData.copy(
                employeeName = newSupervisor?.employeeFullName.orEmpty(),
                employeeId = newSupervisor?.employeeId ?: 0,
                employeePinNumber = newSupervisor?.employeePinNumber.orEmpty()
            )
            _supervisorData.value = newSupervisorData
        }
    }

    fun supervisorVerified(pinOutLocation: File) {
        viewModelScope.launch {
            val currentSupervisorData = _supervisorData.value
            val newSupervisorData = currentSupervisorData.copy(
                verificationData = DemoDailyLogVerificationData(
                    isVerified = true,
                    verifiedEmployeeId = currentSupervisorData.employeeId,
                    verifiedDate = Date().toSimpleString(),
                    verifiedByName = currentSupervisorData.employeeName,
                    verifiedByEmployeeId = currentSupervisorData.employeeId,
                    pinOutLocation = pinOutLocation
                )
            )
            _supervisorData.value = newSupervisorData
        }
    }

    fun validatePin(enteredPin: String, savedPinHash: String): Boolean {
        return PinUseCases.validatePin(enteredPin, savedPinHash)
    }

    fun getPinOutLocation(internalFormId: Int, projectId: Int): File {
        return dailyDemoLogUseCases.getDailyDemoLogSupervisorPinOutLocation(
            internalFormId,
            projectId
        )
    }

    private suspend fun createDemoDailyLogVerificationData(
        isVerified: Boolean,
        verifiedEmployeeId: Int,
        verifiedDate: Date,
        verifiedByEmployeeId: Int,
        pinOutLocation: File
    ): DemoDailyLogVerificationData {

        val supervisorVerifiedByName =
            employeeUseCases.getEmployeeById(verifiedByEmployeeId)?.employeeFullName ?: ""

        return DemoDailyLogVerificationData(
            isVerified = isVerified,
            verifiedEmployeeId = if (isVerified) verifiedEmployeeId else 0,
            verifiedDate = if (isVerified) verifiedDate.toSimpleString() else "",
            verifiedByName = if (isVerified) supervisorVerifiedByName else "",
            verifiedByEmployeeId = if (isVerified) verifiedByEmployeeId else 0,
            pinOutLocation = if (isVerified) pinOutLocation else File("")
        )
    }
}

data class GeneralInformationViewState(
    val projectName: String = "",
    val projectNumber: String = "",
    val date: String,
    val day: String = "",
    val safetyTopic: String = "",
)

private fun DailyDemoLogGeneralInfoUiData.toViewState(): GeneralInformationViewState {
    return GeneralInformationViewState(
        projectName = this.projectName,
        projectNumber = this.projectNumber,
        date = this.formDate.toSimpleString(),
        day = this.formDate.toDay(),
        safetyTopic = this.safetyTopic
    )
}

data class DemoDailyLogEmployeeAndVerificationData(
    val employeeName: String,
    val employeeId: Int,
    val employeePinNumber: String,
    val verificationData: DemoDailyLogVerificationData
) {
    val showVerify
        get() = employeeId > 0

    val verified
        get() = employeeId == verificationData.verifiedEmployeeId &&
                verificationData.isVerified

    companion object {
        fun createEmpty(): DemoDailyLogEmployeeAndVerificationData {
            return DemoDailyLogEmployeeAndVerificationData(
                employeeName = "",
                employeeId = 0,
                employeePinNumber = "",
                verificationData = DemoDailyLogVerificationData(
                    isVerified = false,
                    verifiedByEmployeeId = 0,
                    verifiedByName = "",
                    verifiedEmployeeId = 0,
                    verifiedDate = "",
                    pinOutLocation = File("")
                )
            )
        }
    }
}

data class DemoDailyLogVerificationData(
    val isVerified: Boolean,
    val verifiedEmployeeId: Int,
    val verifiedDate: String,
    val verifiedByName: String,
    val verifiedByEmployeeId: Int,
    val pinOutLocation: File
)

data class DailyDemoGeneralInfoSaveData(
    val supervisorId: Int,
    val supervisorVerified: Boolean,
    val supervisorVerifiedDate: Date?,
    val supervisorVerifiedByEmployeeId: Int?,
    val pinOutLocation: String,
    val safetyTopic: String,
    val formDate: Date
)
