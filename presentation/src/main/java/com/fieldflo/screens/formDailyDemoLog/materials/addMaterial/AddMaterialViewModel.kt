package com.fieldflo.screens.formDailyDemoLog.materials.addMaterial

import android.os.Parcelable
import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.DailyDemoLogUseCases
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

class AddMaterialViewModel @Inject constructor(
    private val dailyDemoLogUseCases: DailyDemoLogUseCases
) : ViewModel() {

    suspend fun getAllMaterials(): List<MaterialUiModel> {
        return dailyDemoLogUseCases.getAllEquipment().map {
            MaterialUiModel(
                materialId = it.inventoryItemId,
                materialName = it.inventoryItemName,
                materialNumber = it.inventoryItemNumber
            )
        }
    }
}

@Parcelize
data class MaterialUiModel(
    val materialId: Int,
    val materialName: String,
    val materialNumber: Int
) : Parcelable {
    override fun toString(): String = "$materialName / $materialNumber"
}