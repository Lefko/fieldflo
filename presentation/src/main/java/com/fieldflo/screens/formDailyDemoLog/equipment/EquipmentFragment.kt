package com.fieldflo.screens.formDailyDemoLog.equipment

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.fieldflo.R
import com.fieldflo.common.safeClickListener
import com.fieldflo.di.injector
import com.fieldflo.screens.formDailyDemoLog.equipment.addCustom.AddCustomEquipmentDialog
import com.fieldflo.screens.formDailyDemoLog.equipment.addEquipment.AddEquipmentDialogFragment
import com.fieldflo.screens.formDailyDemoLog.equipment.addEquipment.EquipmentUiModel
import com.fieldflo.usecases.DailyDemoLogEquipmentUiData
import kotlinx.android.synthetic.main.fragment_daily_demo_log_form_equipment.*

class EquipmentFragment : Fragment(R.layout.fragment_daily_demo_log_form_equipment) {

    private val internalFormId by lazy { arguments!!.getInt(INTERNAL_FORM_ID_KEY) }
    private val viewModel by viewModels<DailyDemoLogEquipmentViewModel> {
        injector.dailyDemoLogEquipmentViewModelFactory()
    }
    private val equipmentAdapter = EquipmentAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initClickListeners()
        loadEquipment()
    }

    fun extractState(): List<DailyDemoLogEquipmentUiData> = equipmentAdapter.getEquipment()

    private fun initRecyclerView() {
        activity_daily_demo_log_form_equipment.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = equipmentAdapter
        }
    }

    private fun initClickListeners() {
        activity_daily_demo_log_form_content_add_equipment.safeClickListener {
            childFragmentManager.setFragmentResultListener(
                AddEquipmentDialogFragment.EQUIPMENT_SELECTED_RESULT_KEY,
                this
            ) { _, bundle ->
                childFragmentManager.clearFragmentResultListener(AddEquipmentDialogFragment.EQUIPMENT_SELECTED_RESULT_KEY)
                val equipment =
                    bundle.getParcelable(AddEquipmentDialogFragment.EQUIPMENT_KEY) as? EquipmentUiModel
                handleAddEquipment(equipment)
            }
            AddEquipmentDialogFragment.newInstance()
                .show(childFragmentManager, AddEquipmentDialogFragment.TAG)
        }

        activity_daily_demo_log_form_content_add_equipment_custom.safeClickListener {
            childFragmentManager.setFragmentResultListener(
                AddCustomEquipmentDialog.EQUIPMENT_ENTERED_RESULT_KEY,
                this
            ) { _, bundle ->
                childFragmentManager.clearFragmentResultListener(AddCustomEquipmentDialog.EQUIPMENT_ENTERED_RESULT_KEY)
                val customEquipmentName =
                    bundle.getString(AddCustomEquipmentDialog.CUSTOM_EQUIPMENT_ENTERED_KEY)
                        .orEmpty()
                handleAddCustomEquipment(customEquipmentName)
            }
            AddCustomEquipmentDialog.newInstance()
                .show(childFragmentManager, AddCustomEquipmentDialog.TAG)
        }
    }

    private fun loadEquipment() {
        lifecycleScope.launchWhenCreated {
            val equipment = viewModel.getEquipment(internalFormId)
            equipmentAdapter.setEquipment(equipment)
        }
    }

    private fun handleAddEquipment(equipment: EquipmentUiModel?) {
        equipment?.let {
            equipmentAdapter.addEquipment(DailyDemoLogEquipmentUiData.create(it))
        }
    }

    private fun handleAddCustomEquipment(customEquipmentName: String) {
        if (customEquipmentName.isNotEmpty()) {
            equipmentAdapter.addEquipment(
                DailyDemoLogEquipmentUiData.createCustom(customEquipmentName)
            )
        }
    }

    companion object {
        private const val INTERNAL_FORM_ID_KEY = "internal_form_id_key"
        fun newInstance(internalFormId: Int) = EquipmentFragment().apply {
            arguments = bundleOf(INTERNAL_FORM_ID_KEY to internalFormId)
        }
    }
}