package com.fieldflo.screens.formDailyDemoLog.equipment

import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.DailyDemoLogEquipmentUiData
import com.fieldflo.usecases.DailyDemoLogUseCases
import javax.inject.Inject

class DailyDemoLogEquipmentViewModel @Inject constructor(
    private val dailyDemoLogUseCases: DailyDemoLogUseCases
) : ViewModel() {

    suspend fun getEquipment(
        internalFormId: Int
    ): List<DailyDemoLogEquipmentUiData> {
        return dailyDemoLogUseCases.getDailyDemoLogEquipment(internalFormId)
    }
}
