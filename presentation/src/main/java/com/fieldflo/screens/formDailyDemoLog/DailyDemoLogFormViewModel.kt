package com.fieldflo.screens.formDailyDemoLog

import androidx.lifecycle.ViewModel
import com.fieldflo.AppScopeHolder
import com.fieldflo.screens.formDailyDemoLog.generalInformation.DailyDemoGeneralInfoSaveData
import com.fieldflo.usecases.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

class DailyDemoLogFormViewModel @Inject constructor(
    private val projectsUseCases: ProjectsUseCases,
    private val dailyDemoLogUseCases: DailyDemoLogUseCases,
    private val appScopeHolder: AppScopeHolder
) : ViewModel() {

    suspend fun getDailyDemoLogActivityUiData(
        internalFormId: Int,
        projectId: Int
    ): DailyDemoLogActivityUiData {
        return dailyDemoLogUseCases.getDailyDemoLogActivityUiData(internalFormId, projectId)
    }

    suspend fun getTabs(projectId: Int): List<DailyDemoLogScreen> {
        return dailyDemoLogUseCases.getDailyDemoLogTabs(projectId)
    }

    fun getProjectStatusFlow(projectId: Int): Flow<ProjectStatus> {
        return projectsUseCases.getProjectStatusFlow(projectId)
    }

    fun save(saveData: DailyDemoLogSaveData) {
        appScopeHolder.scope.launch {
            dailyDemoLogUseCases.saveDailyDemoLogFormData(saveData)
        }
    }
}

data class DailyDemoLogSaveData(
    val projectId: Int,
    val internalFormId: Int,
    val generalInfoSaveData: DailyDemoGeneralInfoSaveData?,
    val productionNarrativeUiData: DailyDemoLogProductionNarrativeUiData?,
    val employees: List<DailyDemoLogEmployeeUiData>?,
    val subContractors: List<DailyDemoLogSubcontractorUiData>? = null,
    val equipment: List<DailyDemoLogEquipmentUiData>? = null,
    val materials: List<DailyDemoLogMaterialUiData>? = null,
    val trucking: List<DailyDemoLogTruckingUiData>? = null,
    val miscItems: DailyDemoLogMiscItems? = null,
    val comments: String? = null
)

