package com.fieldflo.screens.formDailyDemoLog.trucking

import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.DailyDemoLogTruckingUiData
import com.fieldflo.usecases.DailyDemoLogUseCases
import javax.inject.Inject

class DailyDemoLogTruckingViewModel @Inject constructor(
    private val dailyDemoLogUseCases: DailyDemoLogUseCases
) : ViewModel() {

    suspend fun getTrucking(
        internalFormId: Int
    ): List<DailyDemoLogTruckingUiData> {
        return dailyDemoLogUseCases.getDailyDemoTrucking(internalFormId)
    }
}