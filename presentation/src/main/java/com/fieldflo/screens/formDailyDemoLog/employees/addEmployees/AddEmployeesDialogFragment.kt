package com.fieldflo.screens.formDailyDemoLog.employees.addEmployees

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.ListView
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector
import com.fieldflo.usecases.DailyDemoLogEmployeeUiData

class AddEmployeesDialogFragment : DialogFragment() {

    private val presenter by viewModels<AddEmployeesPresenter> { injector.dailyDemoLogAddEmployeesPresenterFactory() }

    private val employeeIds by lazy { arguments!!.getIntegerArrayList(EMPLOYEE_IDS_IN_DAILY_DEMO)!! }
    private val employeesAdapter by lazy { EmployeesAdapter(activity!!) }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = AlertDialog.Builder(activity as Context)
            .setTitle(R.string.activity_pre_job_safety_add_employees)
            .setAdapter(employeesAdapter, null)
            .setPositiveButton(android.R.string.ok, { _, _ ->
                lifecycleScope.launchWhenCreated {
                    val listView = (dialog as AlertDialog).listView

                    val newEmployees = (0 until listView.adapter.count)
                        .filter { listView.checkedItemPositions[it] }
                        .mapNotNull { employeesAdapter.getItem(it) }

                    val newEmployeesList = ArrayList(newEmployees)

                    setFragmentResult(
                        EMPLOYEES_SELECTED_RESULT_KEY,
                        Bundle().apply {
                            this.putParcelableArrayList(EMPLOYEES_SELECTED_KEY, newEmployeesList)
                        }
                    )
                    dismiss()
                }

            })
            .setNegativeButton(android.R.string.cancel, { _, _ ->
                dismiss()
            })
            .show()

        dialog.listView.itemsCanFocus = false
        dialog.listView.choiceMode = ListView.CHOICE_MODE_MULTIPLE

        present()

        return dialog
    }

    private fun present() {
        lifecycleScope.launchWhenCreated {
            render(presenter.getEmployees(employeeIds))
        }
    }

    private fun render(employees: List<DailyDemoLogEmployeeUiData>) {
        employeesAdapter.clear()
        employeesAdapter.addAll(employees.filter { !employeeIds.contains(it.employeeId) })
        employeesAdapter.notifyDataSetChanged()
    }

    companion object {

        const val TAG = "AddEmployeesDialogFragment"
        const val EMPLOYEES_SELECTED_RESULT_KEY = "employees selected result key"
        const val EMPLOYEES_SELECTED_KEY = "employees selected key"
        private const val EMPLOYEE_IDS_IN_DAILY_DEMO = "employee_ids_in_daily_demo_key"

        fun newInstance(employeeIds: List<Int>) =
            AddEmployeesDialogFragment().apply {
                arguments = bundleOf(EMPLOYEE_IDS_IN_DAILY_DEMO to employeeIds)
            }
    }
}