package com.fieldflo.screens.formDailyDemoLog.employees

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.fieldflo.R
import com.fieldflo.common.safeClickListener
import com.fieldflo.di.injector
import com.fieldflo.screens.formDailyDemoLog.employees.addEmployees.AddEmployeesDialogFragment
import com.fieldflo.usecases.DailyDemoLogEmployeeUiData
import kotlinx.android.synthetic.main.fragment_daily_demo_log_form_employees.*

class DailyDemoLogEmployeesFragment : Fragment(R.layout.fragment_daily_demo_log_form_employees) {

    private val internalFormId by lazy { arguments!!.getInt(INTERNAL_FORM_ID_KEY) }
    private val projectId by lazy { arguments!!.getInt(PROJECT_ID_KEY) }
    private val viewModel by viewModels<DailyDemoLogEmployeesViewModel> {
        injector.dailyDemoLogEmployeesViewModelFactory()
    }
    private val dailyDemoLogEmployeesAdapter = DailyDemoLogEmployeesAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initClickListeners()
        loadEmployees()
    }

    fun extractState(): List<DailyDemoLogEmployeeUiData> =
        dailyDemoLogEmployeesAdapter.getEmployees()

    private fun initRecyclerView() {
        activity_daily_demo_log_form_employees.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = dailyDemoLogEmployeesAdapter
        }
    }

    private fun initClickListeners() {
        activity_daily_demo_log_form_content_add_employee.safeClickListener {
            childFragmentManager.setFragmentResultListener(
                AddEmployeesDialogFragment.EMPLOYEES_SELECTED_RESULT_KEY,
                this
            ) { _, bundle ->
                childFragmentManager.clearFragmentResultListener(AddEmployeesDialogFragment.EMPLOYEES_SELECTED_RESULT_KEY)
                val employeesToAdd: List<DailyDemoLogEmployeeUiData> =
                    bundle.getParcelableArrayList(AddEmployeesDialogFragment.EMPLOYEES_SELECTED_KEY)
                        ?: emptyList()
                handleNewEmployees(employeesToAdd)
            }
            val currentEmployees = dailyDemoLogEmployeesAdapter.getEmployees().map { it.employeeId }
            AddEmployeesDialogFragment.newInstance(currentEmployees)
                .show(childFragmentManager, AddEmployeesDialogFragment.TAG)
        }
    }

    private fun handleNewEmployees(employeesToAdd: List<DailyDemoLogEmployeeUiData>) {
        dailyDemoLogEmployeesAdapter.addEmployees(employeesToAdd)
    }

    private fun loadEmployees() {
        lifecycleScope.launchWhenCreated {
            val employees = viewModel.getEmployees(internalFormId, projectId)
            dailyDemoLogEmployeesAdapter.setEmployees(employees)
        }
    }

    companion object {
        private const val PROJECT_ID_KEY = "project_id_key"
        private const val INTERNAL_FORM_ID_KEY = "internal_form_id_key"
        fun newInstance(projectId: Int, internalFormId: Int) =
            DailyDemoLogEmployeesFragment().apply {
                arguments = bundleOf(
                    PROJECT_ID_KEY to projectId,
                    INTERNAL_FORM_ID_KEY to internalFormId
                )
            }
    }
}
