package com.fieldflo.screens.formDailyDemoLog.miscItems

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.fieldflo.R
import com.fieldflo.common.viewLifecycleScope
import com.fieldflo.di.injector
import com.fieldflo.usecases.DailyDemoLogMiscItems
import kotlinx.android.synthetic.main.fragment_daily_demo_log_form_misc_items.*

class MiscItemsFragment :
    Fragment(R.layout.fragment_daily_demo_log_form_misc_items) {

    private val projectId: Int by lazy { arguments!!.getInt(PROJECT_ID_KEY) }
    private val internalFormId: Int by lazy { arguments!!.getInt(INTERNAL_FORM_ID_KEY) }

    private val viewModel by viewModels<MiscItemsViewModel> {
        injector.dailyDemoLogMiscItemsViewModelFactory()
    }

    private val reportableIncidentAdapter: ArrayAdapter<CharSequence> by lazy {
        ArrayAdapter.createFromResource(
            context!!, R.array.fragment_miscellaneous_items, R.layout.row_spinner_dropdown
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    fun extractState(): DailyDemoLogMiscItems {
        val hasReportableIncident = hasReportableIncident()
        val hasStopWorkPerformed = hasStopWorkPerformed()

        return DailyDemoLogMiscItems(
            reportableIncident = hasReportableIncident,
            incident = if (hasReportableIncident) getReportableIncidentValue() else "",
            incidentDescription = fragment_miscellaneous_items_reportable_incident_description.text.toString(),
            stopWorkPerformed = hasStopWorkPerformed,
            stopWorkReason = fragment_miscellaneous_items_stop_work_description_value.text.toString(),
            stopWorkBeyondControlDescription = fragment_miscellaneous_items_prevent_work_description_value.text.toString(),
            stopWorkOutOfScopeDescription = fragment_miscellaneous_items_required_work_description_value.text.toString(),
            stopWorkBackChargeDescription = fragment_miscellaneous_items_perform_work_description_value.text.toString(),
            stopWorkVisitorsDescription = fragment_miscellaneous_items_inspections_work_description_value.text.toString(),
            stopWorkIssuesDescription = fragment_miscellaneous_items_issues_work_description_value.text.toString()
        )
    }

    private fun initViews() {
        fragment_miscellaneous_items_reportable_incident_spinner.adapter = reportableIncidentAdapter

        viewLifecycleScope.launchWhenCreated {
            val dailyDemoLogMiscItems =
                viewModel.getDailyDemoLogMiscItems(internalFormId, projectId)

            renderViewData(dailyDemoLogMiscItems)
            setClickListeners()
        }
    }

    private fun renderViewData(dailyDemoLogMiscItems: DailyDemoLogMiscItems) {
        initReportableIncidentDropdown(dailyDemoLogMiscItems.incident)

        handleReportableIncidentUI(dailyDemoLogMiscItems.reportableIncident)
        fragment_miscellaneous_items_reportable_incident_radiogroup.check(
            if (dailyDemoLogMiscItems.reportableIncident) R.id.fragment_miscellaneous_items_reportable_incident_yes
            else R.id.fragment_miscellaneous_items_reportable_incident_no
        )

        handleWorkStoppedUI(dailyDemoLogMiscItems.stopWorkPerformed)
        fragment_miscellaneous_items_stop_work_radiogroup.check(
            if (dailyDemoLogMiscItems.stopWorkPerformed) R.id.fragment_miscellaneous_items_stop_work_yes
            else R.id.fragment_miscellaneous_items_stop_work_no
        )

        fragment_miscellaneous_items_reportable_incident_description
            .setText(dailyDemoLogMiscItems.incidentDescription)

        fragment_miscellaneous_items_stop_work_description_value
            .setText(dailyDemoLogMiscItems.stopWorkReason)

        fragment_miscellaneous_items_prevent_work_description_value
            .setText(dailyDemoLogMiscItems.stopWorkBeyondControlDescription)

        fragment_miscellaneous_items_required_work_description_value
            .setText(dailyDemoLogMiscItems.stopWorkOutOfScopeDescription)

        fragment_miscellaneous_items_perform_work_description_value
            .setText(dailyDemoLogMiscItems.stopWorkBackChargeDescription)

        fragment_miscellaneous_items_inspections_work_description_value
            .setText(dailyDemoLogMiscItems.stopWorkVisitorsDescription)

        fragment_miscellaneous_items_issues_work_description_value
            .setText(dailyDemoLogMiscItems.stopWorkIssuesDescription)

    }

    private fun initReportableIncidentDropdown(incident: String) {
        fragment_miscellaneous_items_reportable_incident_spinner.setSelection(
            if (incident.isEmpty()) 0
            else reportableIncidentAdapter.getPosition(incident)
        )
    }

    private fun setClickListeners() {
        fragment_miscellaneous_items_reportable_incident_radiogroup.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.fragment_miscellaneous_items_reportable_incident_yes -> {
                    handleReportableIncidentUI(true)
                }

                R.id.fragment_miscellaneous_items_reportable_incident_no -> {
                    handleReportableIncidentUI(false)
                }
            }
        }

        fragment_miscellaneous_items_stop_work_radiogroup.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.fragment_miscellaneous_items_stop_work_yes -> {
                    handleWorkStoppedUI(true)
                }

                R.id.fragment_miscellaneous_items_stop_work_no -> {
                    handleWorkStoppedUI(false)
                }
            }
        }
    }

    private fun handleWorkStoppedUI(isEnabled: Boolean) {
        fragment_miscellaneous_items_stop_work_description_value.isEnabled = isEnabled
        fragment_miscellaneous_items_prevent_work_description_value.isEnabled = isEnabled
        fragment_miscellaneous_items_required_work_description_value.isEnabled = isEnabled
        fragment_miscellaneous_items_perform_work_description_value.isEnabled = isEnabled
        fragment_miscellaneous_items_inspections_work_description_value.isEnabled = isEnabled
        fragment_miscellaneous_items_issues_work_description_value.isEnabled = isEnabled
    }

    private fun handleReportableIncidentUI(isEnabled: Boolean) {
        fragment_miscellaneous_items_reportable_incident_description.isEnabled = isEnabled
        fragment_miscellaneous_items_reportable_incident_spinner.isEnabled = isEnabled
    }

    private fun getReportableIncidentValue(): String {
        val spinnerHint = getString(R.string.fragment_miscellaneous_items_please_select)

        val selected =
            (fragment_miscellaneous_items_reportable_incident_spinner.selectedItem as? String) ?: ""

        return if (selected == spinnerHint) {
            ""
        } else {
            selected
        }
    }

    private fun hasReportableIncident(): Boolean {
        return fragment_miscellaneous_items_reportable_incident_radiogroup.checkedRadioButtonId == R.id.fragment_miscellaneous_items_reportable_incident_yes
    }

    private fun hasStopWorkPerformed(): Boolean {
        return fragment_miscellaneous_items_stop_work_radiogroup.checkedRadioButtonId == R.id.fragment_miscellaneous_items_stop_work_yes
    }

    companion object {
        private const val PROJECT_ID_KEY = "project_id_key"
        private const val INTERNAL_FORM_ID_KEY = "internal_form_id_key"
        fun newInstance(projectId: Int, internalFormId: Int) = MiscItemsFragment().apply {
            arguments = bundleOf(
                PROJECT_ID_KEY to projectId,
                INTERNAL_FORM_ID_KEY to internalFormId
            )
        }
    }
}
