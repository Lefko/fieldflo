package com.fieldflo.screens.formDailyDemoLog.subContractors

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.fieldflo.R
import com.fieldflo.common.safeClickListener
import com.fieldflo.di.injector
import com.fieldflo.screens.formDailyDemoLog.subContractors.addCustom.AddCustomSubcontractorDialog
import com.fieldflo.screens.formDailyDemoLog.subContractors.addSubcontractor.AddSubcontractorDialogFragment
import com.fieldflo.screens.formDailyDemoLog.subContractors.addSubcontractor.SubcontractorUiModel
import com.fieldflo.usecases.DailyDemoLogSubcontractorUiData
import kotlinx.android.synthetic.main.fragment_daily_demo_log_form_subcontractors.*

class SubContractorsFragment : Fragment(R.layout.fragment_daily_demo_log_form_subcontractors) {

    private val internalFormId by lazy { arguments!!.getInt(INTERNAL_FORM_ID_KEY) }
    private val viewModel by viewModels<DailyDemoLogSubContractorsViewModel> {
        injector.dailyDailyDemoLogSubContractorsViewModelFactory()
    }
    private val subcontractorsAdapter = SubcontractorsAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initClickListeners()
        loadSubcontractors()
    }

    fun extractState(): List<DailyDemoLogSubcontractorUiData> =
        subcontractorsAdapter.getSubcontractors()

    private fun initRecyclerView() {
        activity_daily_demo_log_form_subcontractors.apply {
            this.layoutManager = LinearLayoutManager(requireContext())
            this.adapter = subcontractorsAdapter
        }
    }

    private fun initClickListeners() {
        subcontractor_item_row_subcontractor_add_custom.safeClickListener {
            childFragmentManager.setFragmentResultListener(
                AddCustomSubcontractorDialog.SUBCONTRACTOR_ENTERED_RESULT_KEY,
                this
            ) { _, bundle ->
                childFragmentManager.clearFragmentResult(AddCustomSubcontractorDialog.SUBCONTRACTOR_ENTERED_RESULT_KEY)
                val customSubcontractorName =
                    bundle.getString(AddCustomSubcontractorDialog.CUSTOM_SUBCONTRACTOR_ENTERED_KEY)
                        .orEmpty()
                handleAddCustomSubcontractor(customSubcontractorName)
            }
            AddCustomSubcontractorDialog.newInstance()
                .show(childFragmentManager, AddCustomSubcontractorDialog.TAG)
        }

        activity_daily_demo_log_form_content_add_subcontractor.safeClickListener {
            childFragmentManager.setFragmentResultListener(
                AddSubcontractorDialogFragment.SUBCONTRACTOR_SELECTED_RESULT_KEY,
                this
            ) { _, bundle ->
                childFragmentManager.clearFragmentResult(AddSubcontractorDialogFragment.SUBCONTRACTOR_SELECTED_RESULT_KEY)
                val subcontractor = bundle.getParcelable<SubcontractorUiModel>(
                    AddSubcontractorDialogFragment.SUBCONTRACTOR_KEY
                )
                handleAddSubcontractor(subcontractor)
            }
            AddSubcontractorDialogFragment.newInstance()
                .show(childFragmentManager, AddSubcontractorDialogFragment.TAG)
        }
    }

    private fun loadSubcontractors() {
        lifecycleScope.launchWhenCreated {
            val subcontractors = viewModel.getSubContractors(internalFormId)
            subcontractorsAdapter.setSubcontractors(subcontractors)
        }
    }

    private fun handleAddCustomSubcontractor(customSubcontractorName: String) {
        if (customSubcontractorName.isNotEmpty()) {
            subcontractorsAdapter.addSubcontractor(
                DailyDemoLogSubcontractorUiData.createCustom(customSubcontractorName)
            )
        }
    }

    private fun handleAddSubcontractor(subcontractor: SubcontractorUiModel?) {
        subcontractor?.let {
            subcontractorsAdapter.addSubcontractor(DailyDemoLogSubcontractorUiData.create(it))
        }
    }

    companion object {
        private const val INTERNAL_FORM_ID_KEY = "internal_form_id_key"
        fun newInstance(internalFormId: Int) = SubContractorsFragment().apply {
            arguments = bundleOf(INTERNAL_FORM_ID_KEY to internalFormId)
        }
    }
}
