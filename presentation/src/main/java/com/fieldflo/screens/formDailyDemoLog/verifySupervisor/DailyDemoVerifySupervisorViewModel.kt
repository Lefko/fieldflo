package com.fieldflo.screens.formDailyDemoLog.verifySupervisor

import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.EmployeeUseCases
import javax.inject.Inject

class DailyDemoVerifySupervisorViewModel @Inject constructor(private val employeeUseCases: EmployeeUseCases) :
    ViewModel() {

    suspend fun getEmployeePin(employeeId: Int): String {
        return employeeUseCases.getEmployeeById(employeeId)?.employeePinNumber ?: ""
    }
}