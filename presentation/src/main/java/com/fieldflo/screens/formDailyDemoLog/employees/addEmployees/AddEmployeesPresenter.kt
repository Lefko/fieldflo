package com.fieldflo.screens.formDailyDemoLog.employees.addEmployees

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.Employee
import com.fieldflo.usecases.DailyDemoLogEmployeeUiData
import com.fieldflo.usecases.EmployeeUseCases
import javax.inject.Inject

class AddEmployeesPresenter @Inject constructor(
    private val employeesUseCases: EmployeeUseCases
) : ViewModel() {

    suspend fun getEmployees(employeeIds: List<Int>): List<DailyDemoLogEmployeeUiData> {
        return employeesUseCases.getAllEmployees()
            .filter { !employeeIds.contains(it.employeeId) }
            .map { it.toUiModel() }
    }
}

private fun Employee.toUiModel() = DailyDemoLogEmployeeUiData(
    dailyDemoLogEmployeeId = 0,
    employeeId = this.employeeId,
    employeeFullName = this.employeeFullName,
    descriptionOfWork = "",
    hours = ""
)