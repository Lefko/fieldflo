package com.fieldflo.screens.formDailyDemoLog.productionNarrative

import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.DailyDemoLogProductionNarrativeUiData
import com.fieldflo.usecases.DailyDemoLogUseCases
import javax.inject.Inject

class ProductionNarrativeViewModel @Inject constructor(
    private val dailyDemoLogUseCases: DailyDemoLogUseCases
) : ViewModel() {

    suspend fun getViewState(
        internalFormId: Int,
        projectId: Int
    ): DailyDemoLogProductionNarrativeUiData {
        return dailyDemoLogUseCases.getProductionNarrativeUiData(internalFormId, projectId)
    }
}
