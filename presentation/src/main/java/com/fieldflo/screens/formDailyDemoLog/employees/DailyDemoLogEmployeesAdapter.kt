package com.fieldflo.screens.formDailyDemoLog.employees

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fieldflo.R
import com.fieldflo.common.afterTextChanges
import com.fieldflo.common.safeClickListener
import com.fieldflo.usecases.DailyDemoLogEmployeeUiData

class DailyDemoLogEmployeesAdapter : RecyclerView.Adapter<DailyDemoLogEmployeeViewHolder>() {

    private val employees = mutableListOf<DailyDemoLogEmployeeUiData>()

    fun setEmployees(employees: List<DailyDemoLogEmployeeUiData>) {
        this.employees.clear()
        this.employees.addAll(employees)
        notifyDataSetChanged()
    }

    fun addEmployees(employeesToAdd: List<DailyDemoLogEmployeeUiData>) {
        val previousSize = this.employees.size
        this.employees.addAll(employeesToAdd)
        notifyItemRangeInserted(previousSize, employees.size)
    }

    fun getEmployees(): List<DailyDemoLogEmployeeUiData> = employees.toList()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DailyDemoLogEmployeeViewHolder {
        return DailyDemoLogEmployeeViewHolder(parent, ::deleteAt, { employees })
    }

    override fun onBindViewHolder(holder: DailyDemoLogEmployeeViewHolder, position: Int) {
        holder.bind(employees[position])
    }

    override fun getItemCount(): Int = employees.size

    private fun deleteAt(position: Int) {
        employees.removeAt(position)
        notifyItemRemoved(position)
    }
}

class DailyDemoLogEmployeeViewHolder(
    parent: ViewGroup,
    deleteEmployee: (position: Int) -> Unit,
    rowListProvider: () -> MutableList<DailyDemoLogEmployeeUiData>
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(
        R.layout.row_daily_demo_log_employee,
        parent,
        false
    )
) {

    private val deleteIV = itemView.findViewById<ImageView>(R.id.employee_item_row_remove_employee)
    private val employeeName =
        itemView.findViewById<TextView>(R.id.employee_item_row_employee_value)
    private val descriptionOfWork =
        itemView.findViewById<EditText>(R.id.employee_item_row_description_value)
    private val hoursOfWork = itemView.findViewById<EditText>(R.id.employee_item_row_hours_value)

    init {
        deleteIV.safeClickListener {
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                deleteEmployee(position)
            }
        }
        descriptionOfWork.afterTextChanges { text, _ ->
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val employees = rowListProvider()
                val changedEmployee = employees[position].copy(descriptionOfWork = text)
                employees[position] = changedEmployee
            }
        }

        hoursOfWork.afterTextChanges { text, _ ->
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val employees = rowListProvider()
                val changedEmployee = employees[position].copy(hours = text)
                employees[position] = changedEmployee
            }
        }
    }

    fun bind(employee: DailyDemoLogEmployeeUiData) {
        employeeName.text = employee.employeeFullName
        descriptionOfWork.setText(employee.descriptionOfWork)
        hoursOfWork.setText(employee.hours)
    }
}