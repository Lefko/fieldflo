package com.fieldflo.screens.formDailyDemoLog.subContractors.addCustom

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import com.fieldflo.R

class AddCustomSubcontractorDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_custom_subcontractor, null)
        val inputField = view.findViewById<EditText>(R.id.dialog_custom_subcontractor_inputET)

        return AlertDialog.Builder(activity as Context)
            .setView(view)
            .setPositiveButton(android.R.string.ok, { _, _ ->
                setFragmentResult(
                    SUBCONTRACTOR_ENTERED_RESULT_KEY,
                    bundleOf(CUSTOM_SUBCONTRACTOR_ENTERED_KEY to inputField.text.toString())
                )
                dismiss()
            })
            .setNegativeButton(android.R.string.cancel, { _, _ ->
                dismiss()
            })
            .show()
    }

    companion object {

        const val TAG = "AddCustomSubcontractorDialog"
        const val SUBCONTRACTOR_ENTERED_RESULT_KEY = "subcontractor entered result key"
        const val CUSTOM_SUBCONTRACTOR_ENTERED_KEY = "subcontractor entered key"

        fun newInstance() = AddCustomSubcontractorDialog()
    }
}