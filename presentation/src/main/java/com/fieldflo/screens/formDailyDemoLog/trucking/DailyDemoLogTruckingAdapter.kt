package com.fieldflo.screens.formDailyDemoLog.trucking

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fieldflo.R
import com.fieldflo.common.afterTextChanges
import com.fieldflo.common.safeClickListener
import com.fieldflo.usecases.DailyDemoLogTruckingUiData

class DailyDemoLogTruckingAdapter : RecyclerView.Adapter<DailyDemoLogTruckingViewHolder>() {

    private val haulers = mutableListOf<DailyDemoLogTruckingUiData>()

    fun setHaulers(equipment: List<DailyDemoLogTruckingUiData>) {
        this.haulers.clear()
        this.haulers.addAll(equipment)
        notifyDataSetChanged()
    }

    fun addHauler(newEquipment: DailyDemoLogTruckingUiData) {
        val previousSize = haulers.size
        haulers.add(newEquipment)
        notifyItemRangeInserted(previousSize, haulers.size)
    }

    fun getHaulers(): List<DailyDemoLogTruckingUiData> = haulers.toList()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DailyDemoLogTruckingViewHolder {
        return DailyDemoLogTruckingViewHolder(parent, ::deleteAt, { haulers })
    }

    override fun onBindViewHolder(holder: DailyDemoLogTruckingViewHolder, position: Int) {
        holder.bind(haulers[position])
    }

    override fun getItemCount(): Int = haulers.size

    private fun deleteAt(position: Int) {
        haulers.removeAt(position)
        notifyItemRemoved(position)
    }
}

class DailyDemoLogTruckingViewHolder(
    parent: ViewGroup,
    deleteMaterial: (position: Int) -> Unit,
    rowListProvider: () -> MutableList<DailyDemoLogTruckingUiData>
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(
        R.layout.row_daily_demo_log_trucking,
        parent,
        false
    )
) {

    private val deleteIV =
        itemView.findViewById<ImageView>(R.id.row_daily_demo_log_remove_trucking)

    private val companyName =
        itemView.findViewById<TextView>(R.id.row_daily_demo_log_trucking_name_value)

    private val materialName =
        itemView.findViewById<EditText>(R.id.row_daily_demo_log_trucking_material_value)

    private val loads =
        itemView.findViewById<EditText>(R.id.row_daily_demo_log_trucking_loads_value)

    private val truckNumber =
        itemView.findViewById<EditText>(R.id.row_daily_demo_log_trucking_truck_number_value)

    private val hours =
        itemView.findViewById<EditText>(R.id.row_daily_demo_log_trucking_hours_value)

    private val hourlyRate =
        itemView.findViewById<EditText>(R.id.row_daily_demo_log_trucking_hourly_rate_value)

    private val total =
        itemView.findViewById<EditText>(R.id.row_daily_demo_log_trucking_total_value)

    init {
        deleteIV.safeClickListener {
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                deleteMaterial(position)
            }
        }

        materialName.afterTextChanges { text, _ ->
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val haulers = rowListProvider()
                val changedName = haulers[position].copy(material = text)
                haulers[position] = changedName
            }
        }

        loads.afterTextChanges { text, _ ->
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val haulers = rowListProvider()
                val changedLoads = haulers[position].copy(loads = text)
                haulers[position] = changedLoads
            }
        }

        truckNumber.afterTextChanges { text, _ ->
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val haulers = rowListProvider()
                val changedTrackNumber = haulers[position].copy(truckNumber = text)
                haulers[position] = changedTrackNumber
            }
        }

        hours.afterTextChanges { text, _ ->
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val haulers = rowListProvider()
                val changedHours = haulers[position].copy(hours = text)
                haulers[position] = changedHours
                calculateDisplayAndSaveTotal(position, haulers)
            }
        }

        hourlyRate.afterTextChanges { text, _ ->
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val haulers = rowListProvider()
                val changedHourlyRate = haulers[position].copy(hourlyRate = text)
                haulers[position] = changedHourlyRate
                calculateDisplayAndSaveTotal(position, haulers)
            }
        }
    }

    private fun calculateDisplayAndSaveTotal(
        position: Int,
        haulers: MutableList<DailyDemoLogTruckingUiData>
    ) {
        val hoursValue = hours.text.toString().toFloatOrNull()
        val hourlyRateValue = hourlyRate.text.toString().toFloatOrNull()

        if (hoursValue != null && hourlyRateValue != null) {
            val total = (hoursValue * hourlyRateValue).toString()
            this.total.setText(total)
            val changedTotal = haulers[position].copy(total = total)
            haulers[position] = changedTotal
        }
    }

    fun bind(hauler: DailyDemoLogTruckingUiData) {
        companyName.text = hauler.truckingName
        materialName.setText(hauler.material)
        loads.setText(hauler.loads)
        truckNumber.setText(hauler.truckNumber)
        hours.setText(hauler.hours)
        hourlyRate.setText(hauler.hourlyRate)
        total.setText(hauler.total)
    }
}
