package com.fieldflo.screens.formDailyDemoLog.trucking.addTruck

import android.os.Parcelable
import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.HaulersUseCases
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

class AddTruckingViewModel @Inject constructor(
    private val haulersUseCases: HaulersUseCases
) : ViewModel() {

    suspend fun getAllTrucking(): List<HaulerUiModel> {
        return haulersUseCases.getAllHaulers().map {
            HaulerUiModel(truckingId = it.haulerId, name = it.name)
        }
    }
}

@Parcelize
data class HaulerUiModel(
    val truckingId: Int,
    val name: String
): Parcelable