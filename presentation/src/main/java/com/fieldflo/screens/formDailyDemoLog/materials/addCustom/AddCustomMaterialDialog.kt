package com.fieldflo.screens.formDailyDemoLog.materials.addCustom

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import com.fieldflo.R

class AddCustomMaterialDialog : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_custom_material, null)
        val inputField = view.findViewById<EditText>(R.id.dialog_custom_material_inputET)

        return AlertDialog.Builder(activity as Context)
            .setView(view)
            .setPositiveButton(android.R.string.ok, { _, _ ->
                setFragmentResult(
                    MATERIAL_ENTERED_RESULT_KEY,
                    bundleOf(CUSTOM_MATERIAL_ENTERED_KEY to inputField.text.toString())
                )
                dismiss()
            })
            .setNegativeButton(android.R.string.cancel, { _, _ ->
                dismiss()
            })
            .show()
    }

    companion object {

        const val TAG = "AddCustomMaterialDialog"
        const val MATERIAL_ENTERED_RESULT_KEY = "material entered result key"
        const val CUSTOM_MATERIAL_ENTERED_KEY = "material entered key"

        fun newInstance() = AddCustomMaterialDialog()
    }
}