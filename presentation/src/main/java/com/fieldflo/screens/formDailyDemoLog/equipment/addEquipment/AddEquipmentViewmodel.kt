package com.fieldflo.screens.formDailyDemoLog.equipment.addEquipment

import android.os.Parcelable
import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.DailyDemoLogUseCases
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

class AddEquipmentViewmodel @Inject constructor(
    private val dailyDemoLogUseCases: DailyDemoLogUseCases
) : ViewModel() {
    suspend fun getAllSubcontractors(): List<EquipmentUiModel> {
        return dailyDemoLogUseCases.getAllEquipment().map {
            EquipmentUiModel(it.inventoryItemId, it.inventoryItemName, it.inventoryItemNumber)
        }
    }
}

@Parcelize
data class EquipmentUiModel(
    val equipmentId: Int,
    val equipmentName: String,
    val equipmentNumber: Int
) : Parcelable {
    override fun toString(): String = "$equipmentName / $equipmentNumber"
}