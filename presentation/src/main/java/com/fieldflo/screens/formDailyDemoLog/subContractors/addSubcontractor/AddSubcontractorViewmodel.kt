package com.fieldflo.screens.formDailyDemoLog.subContractors.addSubcontractor

import android.os.Parcelable
import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.SubcontractorUseCases
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

class AddSubcontractorViewmodel @Inject constructor(
    private val subcontractorsUseCase: SubcontractorUseCases
) : ViewModel() {
    suspend fun getAllSubcontractors(): List<SubcontractorUiModel> {
        return subcontractorsUseCase.getAllSubcontractors().map {
            SubcontractorUiModel(it.subContractorId, it.name)
        }
    }
}

@Parcelize
data class SubcontractorUiModel(
    val subcontractorId: Int = 0,
    val name: String
) : Parcelable