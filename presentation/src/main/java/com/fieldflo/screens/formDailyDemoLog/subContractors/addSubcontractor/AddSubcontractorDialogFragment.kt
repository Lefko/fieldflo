package com.fieldflo.screens.formDailyDemoLog.subContractors.addSubcontractor

import android.app.Dialog
import android.os.Bundle
import android.widget.ListView
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector

class AddSubcontractorDialogFragment : DialogFragment() {

    private val viewModel by viewModels<AddSubcontractorViewmodel> {
        injector.addSubcontractorViewmodelFactory()
    }
    private val subcontractorAdapter by lazy { SubcontractorAdapter(requireContext()) }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = AlertDialog.Builder(requireContext())
            .setTitle(R.string.add_subcontractor_dialog_fragment_title)
            .setAdapter(subcontractorAdapter, null)
            .setPositiveButton(android.R.string.ok, { _, _ ->
                val listView = (dialog as AlertDialog).listView
                val selectedSubcontractors = (0 until listView.adapter.count)
                    .filter { listView.checkedItemPositions[it] }
                    .mapNotNull { subcontractorAdapter.getItem(it) }
                setFragmentResult(
                    SUBCONTRACTOR_SELECTED_RESULT_KEY,
                    bundleOf(SUBCONTRACTOR_KEY to selectedSubcontractors[0])
                )
            })
            .setNegativeButton(android.R.string.cancel, { _, _ ->
                dismiss()
            })
            .show()

        dialog.listView.itemsCanFocus = false
        dialog.listView.choiceMode = ListView.CHOICE_MODE_SINGLE

        lifecycleScope.launchWhenCreated {
            val employees = viewModel.getAllSubcontractors()
            subcontractorAdapter.clear()
            subcontractorAdapter.addAll(employees)
            subcontractorAdapter.notifyDataSetChanged()
        }

        return dialog
    }

    companion object {
        const val TAG = "AddSubcontractorDialogFragment"
        const val SUBCONTRACTOR_SELECTED_RESULT_KEY = "subcontractor selected result key"
        const val SUBCONTRACTOR_KEY = "subcontractor_key"

        fun newInstance() = AddSubcontractorDialogFragment()
    }
}