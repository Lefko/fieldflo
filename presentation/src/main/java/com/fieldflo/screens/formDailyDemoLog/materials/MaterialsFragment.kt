package com.fieldflo.screens.formDailyDemoLog.materials

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.fieldflo.R
import com.fieldflo.common.safeClickListener
import com.fieldflo.di.injector
import com.fieldflo.screens.formDailyDemoLog.materials.addCustom.AddCustomMaterialDialog
import com.fieldflo.screens.formDailyDemoLog.materials.addMaterial.AddMaterialDialogFragment
import com.fieldflo.screens.formDailyDemoLog.materials.addMaterial.MaterialUiModel
import com.fieldflo.usecases.DailyDemoLogMaterialUiData
import kotlinx.android.synthetic.main.fragment_daily_demo_log_form_materials.*

class MaterialsFragment : Fragment(R.layout.fragment_daily_demo_log_form_materials) {

    private val internalFormId by lazy { arguments!!.getInt(INTERNAL_FORM_ID_KEY) }
    private val viewModel by viewModels<DailyDemoLogMaterialViewModel> {
        injector.dailyDemoLogMaterialViewModelFactory()
    }

    private val materialAdapter = MaterialsAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initClickListeners()
        loadMaterial()
    }

    fun extractState(): List<DailyDemoLogMaterialUiData> = materialAdapter.getEquipment()

    private fun loadMaterial() {
        lifecycleScope.launchWhenCreated {
            val material = viewModel.getMaterials(internalFormId)
            materialAdapter.setMaterial(material)
        }
    }

    private fun initClickListeners() {
        activity_daily_demo_log_form_content_add_material.safeClickListener {
            childFragmentManager.setFragmentResultListener(
                AddMaterialDialogFragment.MATERIAL_SELECTED_RESULT_KEY,
                this
            ) { _, bundle ->
                childFragmentManager.clearFragmentResultListener(AddMaterialDialogFragment.MATERIAL_SELECTED_RESULT_KEY)
                val material =
                    bundle.getParcelable(AddMaterialDialogFragment.MATERIAL_KEY) as? MaterialUiModel
                handleAddMaterial(material)
            }
            AddMaterialDialogFragment.newInstance()
                .show(childFragmentManager, AddMaterialDialogFragment.TAG)
        }

        activity_daily_demo_log_form_content_add_material_custom.safeClickListener {
            childFragmentManager.setFragmentResultListener(
                AddCustomMaterialDialog.MATERIAL_ENTERED_RESULT_KEY,
                this
            ) { _, bundle ->
                childFragmentManager.clearFragmentResultListener(AddCustomMaterialDialog.MATERIAL_ENTERED_RESULT_KEY)
                val customEquipmentName =
                    bundle.getString(AddCustomMaterialDialog.CUSTOM_MATERIAL_ENTERED_KEY)
                        .orEmpty()
                handleAddCustomMaterial(customEquipmentName)
            }
            AddCustomMaterialDialog.newInstance()
                .show(childFragmentManager, AddCustomMaterialDialog.TAG)
        }
    }

    private fun handleAddCustomMaterial(customMaterialName: String) {
        if (customMaterialName.isNotEmpty()) {
            materialAdapter.addMaterial(
                DailyDemoLogMaterialUiData.createCustom(customMaterialName)
            )
        }
    }

    private fun handleAddMaterial(material: MaterialUiModel?) {
        material?.let {
            materialAdapter.addMaterial(DailyDemoLogMaterialUiData.create(it))
        }
    }

    private fun initRecyclerView() {
        activity_daily_demo_log_form_materials.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = materialAdapter
        }
    }


    companion object {
        const val TAG = "materials"

        private const val INTERNAL_FORM_ID_KEY = "internal_form_id_key"
        fun newInstance(internalFormId: Int) = MaterialsFragment().apply {
            arguments = bundleOf(INTERNAL_FORM_ID_KEY to internalFormId)
        }
    }
}