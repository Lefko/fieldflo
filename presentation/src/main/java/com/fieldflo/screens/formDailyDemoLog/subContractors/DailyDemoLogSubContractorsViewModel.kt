package com.fieldflo.screens.formDailyDemoLog.subContractors

import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.DailyDemoLogSubcontractorUiData
import com.fieldflo.usecases.DailyDemoLogUseCases
import javax.inject.Inject

class DailyDemoLogSubContractorsViewModel @Inject constructor(
    private val dailyDemoLogUseCases: DailyDemoLogUseCases
) : ViewModel() {

    suspend fun getSubContractors(
        internalFormId: Int
    ): List<DailyDemoLogSubcontractorUiData> {
        return dailyDemoLogUseCases.getDailyDemoLogSubcontractors(internalFormId)
    }
}
