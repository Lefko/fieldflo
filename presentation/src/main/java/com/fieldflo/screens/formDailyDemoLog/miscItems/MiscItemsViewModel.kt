package com.fieldflo.screens.formDailyDemoLog.miscItems

import androidx.lifecycle.ViewModel
import com.fieldflo.usecases.DailyDemoLogMiscItems
import com.fieldflo.usecases.DailyDemoLogUseCases
import javax.inject.Inject

class MiscItemsViewModel @Inject constructor(
    private val dailyDemoLogUseCases: DailyDemoLogUseCases
) : ViewModel() {

    suspend fun getDailyDemoLogMiscItems(
        internalFormId: Int,
        projectId: Int
    ): DailyDemoLogMiscItems {
        return dailyDemoLogUseCases.getDailyDemoLogMiscItems(internalFormId, projectId)
    }

}