package com.fieldflo.screens.formDailyDemoLog.trucking

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.fieldflo.R
import com.fieldflo.common.safeClickListener
import com.fieldflo.di.injector
import com.fieldflo.screens.formDailyDemoLog.trucking.addCustom.AddCustomTruckingDialog
import com.fieldflo.screens.formDailyDemoLog.trucking.addTruck.AddTruckingDialogFragment
import com.fieldflo.screens.formDailyDemoLog.trucking.addTruck.HaulerUiModel
import com.fieldflo.usecases.DailyDemoLogTruckingUiData
import kotlinx.android.synthetic.main.fragment_daily_demo_log_form_trucking.*

class TruckingFragment : Fragment(R.layout.fragment_daily_demo_log_form_trucking) {

    private val internalFormId by lazy { arguments!!.getInt(INTERNAL_FORM_ID_KEY) }

    private val viewModel by viewModels<DailyDemoLogTruckingViewModel> {
        injector.dailyDemoLogTruckingViewModelFactory()
    }

    private val truckingAdapter = DailyDemoLogTruckingAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initClickListeners()
        loadTrucking()
    }

    fun extractState(): List<DailyDemoLogTruckingUiData> = truckingAdapter.getHaulers()

    private fun initClickListeners() {
        activity_daily_demo_log_form_content_add_trucking.safeClickListener {
            childFragmentManager.setFragmentResultListener(
                AddTruckingDialogFragment.HAULER_SELECTED_RESULT_KEY,
                this
            ) { _, bundle ->
                childFragmentManager.clearFragmentResultListener(AddTruckingDialogFragment.HAULER_SELECTED_RESULT_KEY)
                val hauler =
                    bundle.getParcelable(AddTruckingDialogFragment.HAULER_KEY) as? HaulerUiModel
                handleAddHauler(hauler)
            }
            AddTruckingDialogFragment.newInstance()
                .show(childFragmentManager, AddTruckingDialogFragment.TAG)
        }

        activity_daily_demo_log_form_content_add_trucking_custom.safeClickListener {
            childFragmentManager.setFragmentResultListener(
                AddCustomTruckingDialog.TRUCKING_ENTERED_RESULT_KEY,
                this
            ) { _, bundle ->
                childFragmentManager.clearFragmentResultListener(AddCustomTruckingDialog.TRUCKING_ENTERED_RESULT_KEY)
                val customHauler =
                    bundle.getString(AddCustomTruckingDialog.CUSTOM_TRUCKING_ENTERED_KEY)
                        .orEmpty()
                handleAddCustomHauler(customHauler)
            }
            AddCustomTruckingDialog.newInstance()
                .show(childFragmentManager, AddCustomTruckingDialog.TAG)
        }
    }

    private fun handleAddCustomHauler(customHaulerName: String) {
        if (customHaulerName.isNotEmpty()) {
            truckingAdapter.addHauler(
                DailyDemoLogTruckingUiData.createCustom(customHaulerName)
            )
        }
    }

    private fun handleAddHauler(hauler: HaulerUiModel?) {
        hauler?.let {
            truckingAdapter.addHauler(DailyDemoLogTruckingUiData.create(it))
        }
    }

    private fun initRecyclerView() {
        activity_daily_demo_log_form_trucking.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = truckingAdapter
        }
    }

    private fun loadTrucking() {
        lifecycleScope.launchWhenCreated {
            val trucking = viewModel.getTrucking(internalFormId)
            truckingAdapter.setHaulers(trucking)
        }
    }

    companion object {
        const val TAG = "trucking"
        private const val INTERNAL_FORM_ID_KEY = "internal_form_id_key"

        fun newInstance(internalFormId: Int) = TruckingFragment().apply {
            arguments = bundleOf(INTERNAL_FORM_ID_KEY to internalFormId)
        }
    }
}