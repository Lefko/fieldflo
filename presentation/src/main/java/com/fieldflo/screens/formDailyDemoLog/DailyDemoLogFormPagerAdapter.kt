package com.fieldflo.screens.formDailyDemoLog

import android.content.Context
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.fieldflo.R
import com.fieldflo.screens.formDailyDemoLog.employees.DailyDemoLogEmployeesFragment
import com.fieldflo.screens.formDailyDemoLog.equipment.EquipmentFragment
import com.fieldflo.screens.formDailyDemoLog.generalInformation.DailyDemoGeneralInformationFragment
import com.fieldflo.screens.formDailyDemoLog.materials.MaterialsFragment
import com.fieldflo.screens.formDailyDemoLog.miscComments.MiscCommentsFragment
import com.fieldflo.screens.formDailyDemoLog.miscItems.MiscItemsFragment
import com.fieldflo.screens.formDailyDemoLog.productionNarrative.ProductionNarrativeFragment
import com.fieldflo.screens.formDailyDemoLog.subContractors.SubContractorsFragment
import com.fieldflo.screens.formDailyDemoLog.trucking.TruckingFragment

@Suppress("DEPRECATION")
class DailyDemoLogFormPagerAdapter(
    fm: FragmentManager,
    private val internalFormId: Int,
    private val projectId: Int,
    private val context: Context,
    private val dailyDemoLogScreens: List<DailyDemoLogScreen>
) : FragmentPagerAdapter(fm) {

    override fun getCount(): Int {
        return dailyDemoLogScreens.size
    }

    override fun getItem(position: Int): Fragment {
        return when (dailyDemoLogScreens[position]) {
            DailyDemoLogScreen.GeneralInformation -> DailyDemoGeneralInformationFragment.newInstance(
                projectId = projectId,
                internalFormId = internalFormId
            )
            DailyDemoLogScreen.ProductionNarrative -> ProductionNarrativeFragment.newInstance(
                projectId = projectId,
                internalFormId = internalFormId
            )
            DailyDemoLogScreen.Employees -> DailyDemoLogEmployeesFragment.newInstance(
                projectId = projectId,
                internalFormId = internalFormId
            )
            DailyDemoLogScreen.SubContractors -> SubContractorsFragment.newInstance(internalFormId = internalFormId)
            DailyDemoLogScreen.Equipment -> EquipmentFragment.newInstance(internalFormId = internalFormId)
            DailyDemoLogScreen.Materials -> MaterialsFragment.newInstance(internalFormId = internalFormId)
            DailyDemoLogScreen.Trucking -> TruckingFragment.newInstance(internalFormId = internalFormId)
            DailyDemoLogScreen.MiscItems -> MiscItemsFragment.newInstance(
                projectId = projectId,
                internalFormId = internalFormId
            )
            DailyDemoLogScreen.MiscComments -> MiscCommentsFragment.newInstance(
                projectId = projectId,
                internalFormId = internalFormId
            )
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.getString(dailyDemoLogScreens[position].titleRef)
    }
}

enum class DailyDemoLogScreen(@StringRes val titleRef: Int) {
    GeneralInformation(R.string.activity_demo_daily_log_general_info),
    ProductionNarrative(R.string.activity_demo_daily_log_production_narrative),
    Employees(R.string.activity_demo_daily_log_employees),
    SubContractors(R.string.activity_demo_daily_log_sub_contractors),
    Equipment(R.string.activity_demo_daily_log_equipment),
    Materials(R.string.activity_demo_daily_log_materials),
    Trucking(R.string.activity_demo_daily_log_trucking),
    MiscItems(R.string.activity_demo_daily_log_misc_items),
    MiscComments(R.string.activity_demo_daily_log_misc_comments)
}