package com.fieldflo.screens.formDailyDemoLog.productionNarrative

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.safeClickListener
import com.fieldflo.di.injector
import com.fieldflo.screens.selectSupervisor.SelectSupervisorDialog
import com.fieldflo.usecases.DailyDemoLogProductionNarrativeUiData
import kotlinx.android.synthetic.main.fragment_daily_demo_log_form_production_narrative.*

class ProductionNarrativeFragment :
    Fragment(R.layout.fragment_daily_demo_log_form_production_narrative) {

    private val viewModel by viewModels<ProductionNarrativeViewModel> {
        injector.dailyDemoLogProductionNarrativeViewModelFactory()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
    }

    private fun initViews() {
        val projectId: Int = arguments?.getInt(PROJECT_ID_KEY)
            ?: throw IllegalArgumentException("ProductionNarrativeFragment needs projectId to work")

        val internalFormId: Int = arguments?.getInt(INTERNAL_FORM_ID_KEY)
            ?: throw IllegalArgumentException("ProductionNarrativeFragment needs internalFormId to work")

        viewLifecycleScope.launchWhenCreated {
            val viewState = viewModel.getViewState(internalFormId, projectId)
            setViewState(viewState)
        }

        setClickListeners()
    }

    private fun setViewState(viewState: DailyDemoLogProductionNarrativeUiData) {
        activity_daily_demo_log_form_production_narrative_value.setText(viewState.productionNarrative)
        updateSafetyMeetingConductedBy(
            viewState.safetyMeetingConductedById,
            viewState.safetyMeetingConductedBy
        )
        activity_daily_demo_log_form_weather_am_value.setText(viewState.weatherAM)
        activity_daily_demo_log_form_weather_pm_value.setText(viewState.weatherPM)
    }

    private fun setClickListeners() {
        activity_daily_demo_log_form_safety_meeting_conducted_by_value.safeClickListener {
            childFragmentManager.setFragmentResultListener(
                SelectSupervisorDialog.SUPERVISOR_SELECTED_RESULT_KEY,
                this
            ) { _, bundle ->
                childFragmentManager.clearFragmentResultListener(SelectSupervisorDialog.SUPERVISOR_SELECTED_RESULT_KEY)
                val employeeId = bundle.getInt(SelectSupervisorDialog.SELECTED_EMPLOYEE_ID_KEY)
                val employeeName =
                    bundle.getString(SelectSupervisorDialog.SELECTED_EMPLOYEE_NAME_KEY).orEmpty()
                updateSafetyMeetingConductedBy(employeeId, employeeName)
            }
            SelectSupervisorDialog.newInstance()
                .show(childFragmentManager, SelectSupervisorDialog.TAG)
        }
    }

    private fun updateSafetyMeetingConductedBy(employeeId: Int, employeeName: String) {
        activity_daily_demo_log_form_safety_meeting_conducted_by_value.text = employeeName
        activity_daily_demo_log_form_safety_meeting_conducted_by_value.tag = employeeId
    }

    fun extractState(): DailyDemoLogProductionNarrativeUiData {
        return DailyDemoLogProductionNarrativeUiData(
            productionNarrative = activity_daily_demo_log_form_production_narrative_value.text.toString(),
            safetyMeetingConductedById = activity_daily_demo_log_form_safety_meeting_conducted_by_value.tag as? Int
                ?: 0,
            safetyMeetingConductedBy = activity_daily_demo_log_form_safety_meeting_conducted_by_value.text.toString(),
            weatherAM = activity_daily_demo_log_form_weather_am_value.text.toString(),
            weatherPM = activity_daily_demo_log_form_weather_pm_value.text.toString()
        )
    }

    companion object {
        private const val PROJECT_ID_KEY = "project_id_key"
        private const val INTERNAL_FORM_ID_KEY = "internal_form_id_key"
        fun newInstance(projectId: Int, internalFormId: Int) = ProductionNarrativeFragment().apply {
            arguments = bundleOf(
                PROJECT_ID_KEY to projectId,
                INTERNAL_FORM_ID_KEY to internalFormId
            )
        }
    }
}

private val Fragment.viewLifecycleScope: LifecycleCoroutineScope
    get() = viewLifecycleOwner.lifecycleScope