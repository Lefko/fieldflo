package com.fieldflo.screens.formDailyDemoLog.generalInformation

import android.Manifest
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.fieldflo.R
import com.fieldflo.common.*
import com.fieldflo.common.dateTimePickers.DatePickerDialog
import com.fieldflo.common.hiddenCamera.CameraCallbacks
import com.fieldflo.common.hiddenCamera.CameraConfig
import com.fieldflo.common.hiddenCamera.CameraPreview
import com.fieldflo.common.hiddenCamera.config.CameraFacing
import com.fieldflo.common.hiddenCamera.config.CameraImageFormat
import com.fieldflo.common.hiddenCamera.config.CameraResolution
import com.fieldflo.common.hiddenCamera.config.CameraRotation
import com.fieldflo.di.injector
import com.fieldflo.screens.formDailyDemoLog.verifySupervisor.DailyDemoVerifySupervisorDialogFragment
import com.fieldflo.screens.selectEmployee.SelectEmployeeDialog
import com.fieldflo.screens.verifyErrorDialog.VerifyErrorDialogFragment
import kotlinx.android.synthetic.main.fragment_daily_demo_log_form_general_information.*
import kotlinx.android.synthetic.main.view_date_picker_item.view.*
import kotlinx.coroutines.flow.collect
import timber.log.Timber
import java.io.File
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean

class DailyDemoGeneralInformationFragment :
    Fragment(R.layout.fragment_daily_demo_log_form_general_information), CameraCallbacks {

    private val projectId: Int by lazy { arguments!!.getInt(PROJECT_ID_KEY) }
    private val internalFormId: Int by lazy { arguments!!.getInt(INTERNAL_FORM_ID_KEY) }

    private val verifiedString by lazy { resources.getString(R.string.activity_pre_job_safety_verified) }
    private val enterPinString by lazy { resources.getString(R.string.activity_pre_job_safety_enter_pin) }

    private val viewModel by viewModels<GeneralInformationViewModel> {
        injector.dailyDemoLogGeneralInformationViewModelFactory()
    }

    // Camera Related
    private lateinit var cameraPreview: CameraPreview
    private val cameraInitialized = AtomicBoolean(false)

    private val getPermission =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            setupCamera()
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    override fun onStart() {
        super.onStart()
        setupCamera()
    }

    override fun onStop() {
        stopCamera()
        super.onStop()
    }

    fun extractState(): DailyDemoGeneralInfoSaveData {
        val supervisor = viewModel.supervisorData.value
        val verificationData = supervisor.verificationData
        val isVerified =
            verificationData.isVerified && supervisor.employeeId == verificationData.verifiedEmployeeId
        return DailyDemoGeneralInfoSaveData(
            supervisorId = supervisor.employeeId,
            supervisorVerified = isVerified,
            supervisorVerifiedDate = if (isVerified) verificationData.verifiedDate.toDate() else null,
            supervisorVerifiedByEmployeeId = if (isVerified) verificationData.verifiedByEmployeeId else null,
            safetyTopic = activity_daily_demo_log_form_safety_topic_value.text.toString(),
            formDate = extractDateFromTextForPicker(activity_daily_demo_log_form_general_date.date_field_text),
            pinOutLocation = verificationData.pinOutLocation.absolutePath
        )
    }

    private fun setupCamera() {
        if (!cameraPreview.context.hasPermission(Manifest.permission.CAMERA)) {
            getPermission.launch(Manifest.permission.CAMERA)
        } else {
            if (!cameraInitialized.get()) {
                val cameraConfig = CameraConfig.Builder(cameraPreview.context)
                    .facing(CameraFacing.FRONT_FACING_CAMERA)
                    .resolution(CameraResolution.LOW_RESOLUTION)
                    .imageFormat(CameraImageFormat.FORMAT_JPEG)
                    .imageRotation(CameraRotation.ROTATION_270)
                    .build()
                cameraPreview.startCameraInternal(cameraConfig)
                cameraInitialized.set(true)
            }
        }
    }

    private fun stopCamera() {
        cameraPreview.stopPreviewAndFreeCamera()
        cameraInitialized.set(false)
    }

    override fun onImageCapture(imageFile: File) {
        Timber.d("Captured pin image: ${imageFile.absoluteFile}")
    }

    override fun onCameraError(errorCode: Int) {
        Timber.d("Error taking pic")
    }

    private fun initViews() {
        viewLifecycleScope.launchWhenCreated {
            val viewState = viewModel.getViewState(internalFormId, projectId)
            setViewState(viewState)
            initClickListeners()
        }

        viewLifecycleScope.launchWhenCreated {
            viewModel.supervisorData.collect { renderSupervisor(it) }
        }

        cameraPreview = createPreview(activity_daily_demo_log_form_general_info_container)
    }

    private fun createPreview(container: LinearLayout): CameraPreview {
        val context = container.context
        val cameraPreview = CameraPreview(context, this)
        cameraPreview.id = View.generateViewId()
        cameraPreview.layoutParams = LinearLayout.LayoutParams(1, 1)
        container.addView(cameraPreview, 0)

        return cameraPreview
    }

    private fun renderSupervisor(supervisorData: DemoDailyLogEmployeeAndVerificationData) {
        activity_daily_demo_log_form_supervisor_value.text = supervisorData.employeeName
        fragment_daily_demo_log_general_info_supervisor_enter_pinTV.isVisible =
            supervisorData.showVerify
        if (supervisorData.verified) {
            fragment_daily_demo_log_general_info_supervisor_enter_pinTV.text =
                "${supervisorData.verificationData.verifiedByName} $verifiedString\n${supervisorData.verificationData.verifiedDate}".trimMargin()
            fragment_daily_demo_log_general_info_supervisor_enter_pinTV.isEnabled = false
        } else {
            fragment_daily_demo_log_general_info_supervisor_enter_pinTV.text = enterPinString
            fragment_daily_demo_log_general_info_supervisor_enter_pinTV.isEnabled = true
        }
    }

    private fun initClickListeners() {
        activity_daily_demo_log_form_supervisor_value.safeClickListener {
            childFragmentManager.setFragmentResultListener(
                SelectEmployeeDialog.EMPLOYEE_SELECTED_RESULT_KEY,
                this
            ) { _, bundle ->
                childFragmentManager.clearFragmentResultListener(SelectEmployeeDialog.EMPLOYEE_SELECTED_RESULT_KEY)
                val supervisorEmployeeId =
                    bundle.getInt(SelectEmployeeDialog.SELECTED_EMPLOYEE_ID_KEY)
                viewModel.supervisorChanged(supervisorEmployeeId)
            }
            SelectEmployeeDialog.newInstance(DAILY_DEMO_LOG_SUPERVISOR_IDENTIFIER)
                .show(childFragmentManager, SelectEmployeeDialog.TAG)
        }

        activity_daily_demo_log_form_general_date.setOnClickListener(
            createShowDateClickListener(
                activity_daily_demo_log_form_general_date.date_field_text
            )
        )

        fragment_daily_demo_log_general_info_supervisor_enter_pinTV.safeClickListener {
            childFragmentManager.setFragmentResultListener(
                DailyDemoVerifySupervisorDialogFragment.SUPERVISOR_VERIFIED_RESULT_KEY,
                this
            ) { _, bundle ->
                childFragmentManager.clearFragmentResultListener(
                    DailyDemoVerifySupervisorDialogFragment.SUPERVISOR_VERIFIED_RESULT_KEY
                )
                val supervisorPinData = DailyDemoVerifySupervisorDialogFragment.extractData(bundle)
                handleSupervisorPinInfo(supervisorPinData)
            }
            DailyDemoVerifySupervisorDialogFragment.newInstance(viewModel.supervisorData.value.employeeId)
                .show(childFragmentManager, DailyDemoVerifySupervisorDialogFragment.TAG)
        }
    }

    private fun createShowDateClickListener(dateTitleView: TextView): View.OnClickListener {
        return View.OnClickListener {
            val date = extractDateFromTextForPicker(dateTitleView)

            DatePickerDialog.newInstance(date, object : DatePickerDialog.DateSelectedListener {
                override fun onDateSelected(newDate: Date) {
                    dateTitleView.text = newDate.toSimpleString()
                    activity_daily_demo_log_form_day_value.setText(newDate.toDay())
                }
            }).show(childFragmentManager, DatePickerDialog.TAG)
        }
    }

    private fun extractDateFromTextForPicker(dateTV: TextView): Date {
        val pleaseSelect = getString(R.string.activity_daily_form_date_title)
        val dateText = dateTV.text.toString()
        return if (dateText == "" || dateText == pleaseSelect) {
            Date()
        } else {
            dateTV.text.toString().toDate()
        }
    }

    private fun handleSupervisorPinInfo(supervisorPinInfo: DailyDemoVerifySupervisorDialogFragment.SupervisorPinInfo) {
        if (supervisorPinInfo.enteredPin.isEmpty() || supervisorPinInfo.enteredPin.isEmpty()) {
            showErrorDialog()
        } else if (!viewModel.validatePin(
                supervisorPinInfo.enteredPin,
                supervisorPinInfo.supervisorPin
            )
        ) {
            showErrorDialog()
        } else {
            val pinOutLocation: File = viewModel.getPinOutLocation(internalFormId, projectId)
            cameraPreview.takePictureInternal(pinOutLocation)
            viewModel.supervisorVerified(pinOutLocation)
        }
    }

    private fun showErrorDialog() {
        VerifyErrorDialogFragment.newInstance()
            .show(childFragmentManager, VerifyErrorDialogFragment.TAG)
    }

    private fun setViewState(generalInformationViewState: GeneralInformationViewState) {
        activity_daily_demo_log_form_content_project_name_value
            .setText(generalInformationViewState.projectName)

        activity_daily_demo_log_form_content_project_number_value
            .setText(generalInformationViewState.projectNumber)

        activity_daily_demo_log_form_general_date.date_field_text.text =
            generalInformationViewState.date

        activity_daily_demo_log_form_safety_topic_value.setText(generalInformationViewState.safetyTopic)

        activity_daily_demo_log_form_day_value.setText(generalInformationViewState.day)
    }

    companion object {
        private const val PROJECT_ID_KEY = "project_id_key"
        private const val INTERNAL_FORM_ID_KEY = "internal_form_id_key"

        fun newInstance(projectId: Int, internalFormId: Int) =
            DailyDemoGeneralInformationFragment().apply {
                arguments = bundleOf(
                    PROJECT_ID_KEY to projectId,
                    INTERNAL_FORM_ID_KEY to internalFormId
                )
            }
    }
}

private const val DAILY_DEMO_LOG_SUPERVISOR_IDENTIFIER = 4597
