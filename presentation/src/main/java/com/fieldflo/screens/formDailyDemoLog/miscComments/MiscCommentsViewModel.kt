package com.fieldflo.screens.formDailyDemoLog.miscComments

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.fieldflo.usecases.DailyDemoLogUseCases
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import java.io.File
import javax.inject.Inject

class MiscCommentsViewModel @Inject constructor(
    private val dailyDemoLogUseCases: DailyDemoLogUseCases
) : ViewModel() {

    suspend fun getDailyDemoLogMiscComments(
        internalFormId: Int,
        projectId: Int
    ): String {
        return dailyDemoLogUseCases.getDailyDemoLogMiscComments(internalFormId, projectId)
    }

    fun getDailyDemoLogImages(internalFormId: Int, projectId: Int): LiveData<List<File>> {
        return dailyDemoLogUseCases.getDailyDemoLogImagesDirFlow(
            projectId = projectId, internalFormId = internalFormId
        ).filter { it.isDirectory }
            .map { it.listFiles()?.toList().orEmpty().filter { it.isFile && it.exists() } }
            .asLiveData(viewModelScope.coroutineContext + Dispatchers.IO)
    }

    fun saveImage(projectId: Int, internalFormId: Int, sourcePath: String) {
        dailyDemoLogUseCases.saveDailyDailyDemoLogPic(
            projectId, internalFormId, sourcePath
        )
    }
}
