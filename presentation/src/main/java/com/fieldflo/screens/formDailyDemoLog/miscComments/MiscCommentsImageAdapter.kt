package com.fieldflo.screens.formDailyDemoLog.miscComments

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fieldflo.R
import com.fieldflo.common.safeClickListener
import java.io.File

class MiscCommentsImageAdapter : ListAdapter<File, MiscCommentsImageViewHolder>(CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MiscCommentsImageViewHolder {
        return MiscCommentsImageViewHolder(parent, ::getItem)
    }

    override fun onBindViewHolder(holder: MiscCommentsImageViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

class MiscCommentsImageViewHolder(parent: ViewGroup, fileProvider: (Int) -> File) :
    RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.row_daily_demo_log_comments_image,
            parent,
            false
        )
    ) {

    private val imageContainer =
        itemView.findViewById<ImageView>(R.id.row_daily_demo_log_comments_image)
    private val trash =
        itemView.findViewById<ImageView>(R.id.row_daily_demo_log_comments_image_trash)

    init {
        trash.safeClickListener {
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                fileProvider(position).delete()
            }
        }
    }

    fun bind(file: File) {
        Glide.with(imageContainer)
            .load(file)
            .centerInside()
            .into(imageContainer)
    }
}

private val CALLBACK = object : DiffUtil.ItemCallback<File>() {
    override fun areItemsTheSame(oldItem: File, newItem: File): Boolean {
        return oldItem.exists() == newItem.exists() &&
                oldItem.absolutePath == newItem.absolutePath
    }

    override fun areContentsTheSame(oldItem: File, newItem: File): Boolean {
        return oldItem.exists() == newItem.exists() &&
                oldItem.absolutePath == newItem.absolutePath
    }
}