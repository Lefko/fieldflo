package com.fieldflo.screens.formDailyDemoLog.equipment.addEquipment

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.afterTextChanges
import com.fieldflo.common.safeClickListener
import com.fieldflo.di.injector
import kotlinx.android.synthetic.main.dialog_add_equipment.view.*
import kotlin.properties.Delegates

class AddEquipmentDialogFragment : DialogFragment() {

    private val viewModel by viewModels<AddEquipmentViewmodel> { injector.addEquipmentViewmodelFactory() }

    private lateinit var fragmentView: View
    private lateinit var positiveButton: Button
    private var selectedEquipment: EquipmentUiModel? by Delegates.observable(null) { _, _, _ ->
        positiveButton.isEnabled = positiveEnabled
    }

    private val positiveEnabled get() = selectedEquipment != null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        fragmentView = View.inflate(context, R.layout.dialog_add_equipment, null)
        return AlertDialog.Builder(requireContext())
            .setView(fragmentView)
            .setPositiveButton(android.R.string.ok, null)
            .setNegativeButton(android.R.string.cancel) { _, _ -> dismiss() }
            .show()
    }

    override fun onStart() {
        super.onStart()
        positiveButton = (dialog as AlertDialog).getButton(Dialog.BUTTON_POSITIVE).apply {
            isEnabled = false
            safeClickListener {
                setFragmentResult(
                    EQUIPMENT_SELECTED_RESULT_KEY,
                    bundleOf(EQUIPMENT_KEY to selectedEquipment)
                )
                dismiss()
            }
        }

        lifecycleScope.launchWhenCreated {
            val subcontractors = viewModel.getAllSubcontractors()
            initView(subcontractors)
        }
    }

    private fun initView(subcontractors: List<EquipmentUiModel>) {
        fragmentView.dialog_add_equipment_search.apply {
            threshold = 1
            setAdapter(EquipmentAdapter(requireContext(), subcontractors))
            setOnItemClickListener { parent, _, position, _ ->
                selectedEquipment = parent.getItemAtPosition(position) as? EquipmentUiModel
            }
            afterTextChanges { _, _ ->
                selectedEquipment = null
            }
        }
        fragmentView.dialog_add_equipment_search.isVisible = true
        fragmentView.dialog_add_equipment_progressbar.isVisible = false
    }

    companion object {
        const val TAG = "AddEquipmentDialogFragment"
        const val EQUIPMENT_SELECTED_RESULT_KEY = "equipment selected result key"
        const val EQUIPMENT_KEY = "equipment_key"

        fun newInstance() = AddEquipmentDialogFragment()
    }
}