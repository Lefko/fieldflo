package com.fieldflo.screens.formDailyDemoLog.verifySupervisor

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.common.gone
import com.fieldflo.common.visible
import com.fieldflo.di.injector

class DailyDemoVerifySupervisorDialogFragment : DialogFragment() {

    private val supervisorId: Int by lazy { arguments!!.getInt(SUPERVISOR_ID_KEY) }
    private val presenter by viewModels<DailyDemoVerifySupervisorViewModel> {
        injector.dailyDemoVerifySupervisorViewModelFactory()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view =
            LayoutInflater.from(activity).inflate(R.layout.dialog_verify_pin_with_progressbar, null)
        val progressBar = view.findViewById<ProgressBar>(R.id.dialog_verify_pin_progress)
        val pinInputField = view.findViewById<EditText>(R.id.dialog_verify_pin_inputET)
        var supervisorPin = ""

        lifecycleScope.launchWhenCreated {
            supervisorPin = presenter.getEmployeePin(supervisorId)
            progressBar.gone()
            pinInputField.visible()
        }

        return AlertDialog.Builder(activity as Context)
            .setView(view)
            .setPositiveButton(android.R.string.ok, { _, _ ->
                if (pinInputField.text.isNotEmpty()) {
                    setFragmentResult(
                        SUPERVISOR_VERIFIED_RESULT_KEY,
                        bundleOf(
                            SUPERVISOR_PIN_KEY to supervisorPin,
                            ENTERED_PIN_KEY to pinInputField.text.toString()
                        )
                    )
                    dismiss()
                }
            })
            .setNegativeButton(android.R.string.cancel, { _, _ ->
                dismiss()
            })
            .show()
    }

    companion object {

        const val TAG = "DailyDemoVerifySupervisorDialogFragment"
        const val SUPERVISOR_VERIFIED_RESULT_KEY = "supervisor verified result"
        private const val SUPERVISOR_ID_KEY = "employee_id_key"

        private const val SUPERVISOR_PIN_KEY = "supervisor pin"
        private const val ENTERED_PIN_KEY = "supervisor pin"

        fun newInstance(supervisorId: Int) =
            DailyDemoVerifySupervisorDialogFragment().apply {
                arguments = bundleOf(SUPERVISOR_ID_KEY to supervisorId)
            }

        fun extractData(result: Bundle): SupervisorPinInfo {
            return SupervisorPinInfo(
                supervisorPin = result.getString(SUPERVISOR_PIN_KEY).orEmpty(),
                enteredPin = result.getString(ENTERED_PIN_KEY).orEmpty()
            )
        }
    }

    data class SupervisorPinInfo(
        val supervisorPin: String,
        val enteredPin: String
    )
}
