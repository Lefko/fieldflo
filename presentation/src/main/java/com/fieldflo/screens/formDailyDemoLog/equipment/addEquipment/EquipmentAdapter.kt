package com.fieldflo.screens.formDailyDemoLog.equipment.addEquipment

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import com.fieldflo.R
import java.util.*

class EquipmentAdapter(context: Context, private val originalValues: List<EquipmentUiModel>) :
    ArrayAdapter<EquipmentUiModel>(context, R.layout.row_asset) {

    private val lock = Any()

    private var objects: MutableList<EquipmentUiModel> = mutableListOf()

    override fun getCount() = objects.size

    override fun getItem(position: Int) = objects[position]

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val rowData = getItem(position)
        val text = rowData.equipmentName + (if (rowData.equipmentNumber != 0) {
            " / ${rowData.equipmentNumber}"
        } else {
            ""
        })
        return (super.getView(position, convertView, parent) as TextView).apply {
            this.text = text
        }
    }

    override fun getFilter() = object : Filter() {
        override fun performFiltering(queryText: CharSequence?): Filter.FilterResults {
            val results = Filter.FilterResults()

            if (queryText.isNullOrEmpty()) {
                val list: MutableList<EquipmentUiModel>
                synchronized(lock) {
                    list = mutableListOf()
                    list.addAll(originalValues)
                }
                results.values = list
                results.count = list.size
            } else {
                val queryTextLowered = queryText.toString().toLowerCase(Locale.ROOT)

                val values: MutableList<EquipmentUiModel>
                synchronized(lock) {
                    values = mutableListOf()
                    values.addAll(originalValues)
                }

                val count = values.size
                val newValues = mutableListOf<EquipmentUiModel>()

                for (i in 0 until count) {
                    val value = values[i]
                    val valueText = value.equipmentName.toLowerCase(Locale.ROOT)
                    val itemNumberText = value.equipmentNumber.toString()

                    // First match against the whole, non-splitted value
                    if (valueText.startsWith(queryTextLowered) || itemNumberText.startsWith(
                            queryTextLowered
                        )
                    ) {
                        newValues.add(value)
                    } else {
                        val words = valueText.split(" ".toRegex()).dropLastWhile { it.isEmpty() }
                            .toTypedArray()
                        for (word in words) {
                            if (word.startsWith(queryTextLowered)) {
                                newValues.add(value)
                                break
                            }
                        }
                    }
                }

                results.values = newValues
                results.count = newValues.size
            }

            return results
        }

        override fun publishResults(constraint: CharSequence?, results: Filter.FilterResults) {
            @Suppress("UNCHECKED_CAST")
            objects = (results.values as List<EquipmentUiModel>).toMutableList()
            if (results.count > 0) {
                notifyDataSetChanged()
            } else {
                notifyDataSetInvalidated()
            }
        }
    }
}