package com.fieldflo.screens.formDailyDemoLog.equipment

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fieldflo.R
import com.fieldflo.common.afterTextChanges
import com.fieldflo.common.safeClickListener
import com.fieldflo.usecases.DailyDemoLogEquipmentUiData

class EquipmentAdapter : RecyclerView.Adapter<DailyDemoLogEquipmentViewHolder>() {

    private val equipment = mutableListOf<DailyDemoLogEquipmentUiData>()

    fun setEquipment(equipment: List<DailyDemoLogEquipmentUiData>) {
        this.equipment.clear()
        this.equipment.addAll(equipment)
        notifyDataSetChanged()
    }

    fun addEquipment(newEquipment: DailyDemoLogEquipmentUiData) {
        val previousSize = equipment.size
        equipment.add(newEquipment)
        notifyItemRangeInserted(previousSize, equipment.size)
    }

    fun getEquipment(): List<DailyDemoLogEquipmentUiData> = equipment.toList()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DailyDemoLogEquipmentViewHolder {
        return DailyDemoLogEquipmentViewHolder(parent, ::deleteAt, { equipment })
    }

    override fun onBindViewHolder(holder: DailyDemoLogEquipmentViewHolder, position: Int) {
        holder.bind(equipment[position])
    }

    override fun getItemCount(): Int = equipment.size

    private fun deleteAt(position: Int) {
        equipment.removeAt(position)
        notifyItemRemoved(position)
    }
}

class DailyDemoLogEquipmentViewHolder(
    parent: ViewGroup,
    deleteEquipment: (position: Int) -> Unit,
    rowListProvider: () -> MutableList<DailyDemoLogEquipmentUiData>
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(
        R.layout.row_daily_demo_log_equipment,
        parent,
        false
    )
) {

    private val deleteIV =
        itemView.findViewById<ImageView>(R.id.equipment_item_row_remove_equipment)
    private val equipmentName =
        itemView.findViewById<TextView>(R.id.equipment_item_row_equipment_value)
    private val equipmentNumber =
        itemView.findViewById<EditText>(R.id.equipment_item_row_phase_value)
    private val descriptionOfWork =
        itemView.findViewById<EditText>(R.id.equipment_item_row_description_of_work_value)
    private val hours = itemView.findViewById<EditText>(R.id.equipment_item_row_hours_value)

    init {
        deleteIV.safeClickListener {
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                deleteEquipment(position)
            }
        }

        equipmentNumber.afterTextChanges { text, _ ->
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val equipment = rowListProvider()
                val changedEquipment = equipment[position].copy(equipmentNumber = text)
                equipment[position] = changedEquipment
            }
        }

        descriptionOfWork.afterTextChanges { text, _ ->
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val equipment = rowListProvider()
                val changedEquipment = equipment[position].copy(descriptionOfWork = text)
                equipment[position] = changedEquipment
            }
        }

        hours.afterTextChanges { text, _ ->
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val equipment = rowListProvider()
                val changedEquipment = equipment[position].copy(hours = text)
                equipment[position] = changedEquipment
            }
        }
    }

    fun bind(equipment: DailyDemoLogEquipmentUiData) {
        equipmentName.text = equipment.equipmentName
        equipmentNumber.setText(equipment.equipmentNumber)
        descriptionOfWork.setText(equipment.descriptionOfWork)
        hours.setText(equipment.hours)
    }
}
