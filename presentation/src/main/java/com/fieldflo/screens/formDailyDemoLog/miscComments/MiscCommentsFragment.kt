package com.fieldflo.screens.formDailyDemoLog.miscComments

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.fieldflo.R
import com.fieldflo.common.hasAllPermissions
import com.fieldflo.common.safeClickListener
import com.fieldflo.common.viewLifecycleScope
import com.fieldflo.di.injector
import com.fieldflo.screens.formDailyFieldReport.FormDailyFieldReportActivity
import com.fieldflo.screens.imagePicker.ImagePickerActivity
import kotlinx.android.synthetic.main.fragment_daily_demo_log_form_misc_comments.*
import java.util.*

class MiscCommentsFragment : Fragment(R.layout.fragment_daily_demo_log_form_misc_comments) {

    private val projectId: Int by lazy { arguments!!.getInt(PROJECT_ID_KEY) }
    private val internalFormId: Int by lazy { arguments!!.getInt(INTERNAL_FORM_ID_KEY) }

    private val viewModel by viewModels<MiscCommentsViewModel> { injector.dailyDemoLogMiscCommentsViewModelFactory() }

    private val imageAdapter = MiscCommentsImageAdapter()
    private val getPermission =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
            showPickerDialog()
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    fun extractState(): String = fragment_daily_demo_log_form_miscellaneous_comments.text.toString()

    private fun initViews() {
        initList()
        loadData()
        initAddPhoto()
    }

    private fun initList() {
        fragment_daily_demo_log_form_miscellaneous_images_list.apply {
            this.layoutManager = LinearLayoutManager(requireContext())
            this.adapter = imageAdapter
        }
    }

    private fun loadData() {
        viewLifecycleScope.launchWhenCreated {
            val comments = viewModel.getDailyDemoLogMiscComments(internalFormId, projectId)
            fragment_daily_demo_log_form_miscellaneous_comments.setText(comments)
        }

        viewModel.getDailyDemoLogImages(internalFormId, projectId).observe(this) {
            imageAdapter.submitList(it)
        }
    }

    private fun initAddPhoto() {
        fragment_daily_demo_log_form_miscellaneous_add_image.safeClickListener {
            if (!requireContext().hasAllPermissions(requiredPermissions)) {
                getPermission.launch(requiredPermissions)
            } else {
                showPickerDialog()
            }
        }
    }

    private fun showPickerDialog() {
        childFragmentManager.setFragmentResultListener(
            ImagePickerOptionsDialog.IMAGE_PICKER_RESULT_KEY,
            this
        ) { _, bundle ->
            childFragmentManager.clearFragmentResultListener(ImagePickerOptionsDialog.IMAGE_PICKER_RESULT_KEY)
            val pickerOption =
                bundle.getSerializable(ImagePickerOptionsDialog.IMAGE_PICKER_OPTION) as? ImagePickerOptionsDialog.PickerOption

            handlePickerOption(pickerOption)
        }
        ImagePickerOptionsDialog.newInstance()
            .show(childFragmentManager, ImagePickerOptionsDialog.TAG)
    }

    private fun handlePickerOption(pickerOption: ImagePickerOptionsDialog.PickerOption?) {
        when (pickerOption) {
            ImagePickerOptionsDialog.PickerOption.TAKE_PIC -> takePic()
            ImagePickerOptionsDialog.PickerOption.GALLERY -> selectFromGallery()
            ImagePickerOptionsDialog.PickerOption.CANCELLED,
            null -> {  /* do nothing */
            }
        }
    }

    private fun takePic() {
        ImagePickerActivity.startTakePic(requireContext(), this, UUID.randomUUID().toString())
    }

    private fun selectFromGallery() {
        ImagePickerActivity.startSelectFromGallery(
            requireContext(),
            this,
            UUID.randomUUID().toString()
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == FormDailyFieldReportActivity.REQUEST_IMAGE && resultCode == Activity.RESULT_OK) {
            val uri: Uri = data?.getParcelableExtra(ImagePickerActivity.RESULT_IMAGE_PATH)!!
            val sourcePath = uri.path!!
            viewModel.saveImage(projectId, internalFormId, sourcePath)
        }
    }

    companion object {
        private val requiredPermissions = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )

        private const val PROJECT_ID_KEY = "project_id_key"
        private const val INTERNAL_FORM_ID_KEY = "internal_form_id_key"

        fun newInstance(projectId: Int, internalFormId: Int) = MiscCommentsFragment().apply {
            arguments = bundleOf(
                PROJECT_ID_KEY to projectId,
                INTERNAL_FORM_ID_KEY to internalFormId
            )
        }
    }
}