package com.fieldflo.screens.selectEmployee

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class EmployeesAdapter(context: Context) :
    ArrayAdapter<EmployeeUiRow>(
        context,
        android.R.layout.simple_list_item_multiple_choice,
        mutableListOf()
    ) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val listItem = convertView ?: LayoutInflater.from(context)
            .inflate(android.R.layout.simple_list_item_multiple_choice, parent, false)

        (listItem as TextView).text = getItem(position)?.employeeName

        return listItem
    }
}
//class EmployeesAdapter : BaseAdapter(), Filterable {
//
//    private val allEmployees = mutableListOf<EmployeeUiRow>()
//    private val filteredEmployees = mutableListOf<EmployeeUiRow>()
//
//    override fun getCount(): Int {
//        return filteredEmployees.size
//    }
//
//    override fun getItem(position: Int): Any {
//        return filteredEmployees[position]
//    }
//
//    override fun getItemId(position: Int): Long {
//        return position.toLong()
//    }
//
//    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
//        val listItem = convertView ?: LayoutInflater.from(parent.context)
//            .inflate(android.R.layout.simple_list_item_multiple_choice, parent, false)
//
//        (listItem as TextView).text = filteredEmployees[position].employeeName
//
//        return listItem
//    }
//
//    fun setData(employees: List<EmployeeUiRow>) {
//        allEmployees.clear()
//        filteredEmployees.clear()
//        allEmployees.addAll(employees)
//        filteredEmployees.addAll(employees)
//        notifyDataSetChanged()
//    }
//
//    override fun getFilter(): Filter {
//        return object : Filter() {
//            override fun performFiltering(constraint: CharSequence): FilterResults {
//                val results = FilterResults()
//
//                if (constraint.isEmpty()) {
//                    results.apply {
//                        values = allEmployees
//                        count = allEmployees.size
//                    }
//                } else {
//
//                    // make a copy of all employees to help with thread safety
//                    val originalData = allEmployees.subList(0, allEmployees.size - 1).toList()
//                    val filteredResults = originalData
//                        .filter { it.employeeName.contains(constraint, true) }
//
//                    results.apply {
//                        values = filteredResults
//                        count = filteredResults.size
//                    }
//                }
//
//                return results
//            }
//
//            override fun publishResults(constraint: CharSequence, results: FilterResults) {
//                filteredEmployees.clear()
//                @Suppress("UNCHECKED_CAST")
//                filteredEmployees.addAll(results.values as List<EmployeeUiRow>)
//                notifyDataSetChanged()
//            }
//        }
//    }
//}