package com.fieldflo.screens.selectEmployee

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.ListView
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector

class SelectEmployeeDialog : DialogFragment() {

    private var callback: EmployeeSelectedCallback? = null

    private val employeeIdentifier: Int by lazy { arguments!!.getInt(EMPLOYEE_IDENTIFIER_KEY) }
    private val presenter by viewModels<SelectEmployeePresenter> { injector.selectEmployeeViewModelFactory() }
    private val employeesAdapter by lazy { EmployeesAdapter(requireActivity()) }

    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val dialog = AlertDialog.Builder(requireContext())
            .setTitle(R.string.dialog_select_employee_title)
            .setAdapter(employeesAdapter, null)
            .setPositiveButton(android.R.string.ok, { _, _ ->
                val listView = (dialog as AlertDialog).listView

                val selectedEmployees = (0 until listView.adapter.count)
                    .filter { listView.checkedItemPositions[it] }
                    .mapNotNull { employeesAdapter.getItem(it) }

                if (selectedEmployees.isEmpty()) {
                    dismiss()
                } else {
                    setFragmentResult(
                        EMPLOYEE_SELECTED_RESULT_KEY,
                        bundleOf(SELECTED_EMPLOYEE_ID_KEY to selectedEmployees[0].employeeId)
                    )
                    callback?.onEmployeeSelected(employeeIdentifier, selectedEmployees[0])
                }
            })
            .setNegativeButton(android.R.string.cancel, { _, _ ->
                dismiss()
            })
            .show()

        dialog.listView.itemsCanFocus = false
        dialog.listView.choiceMode = ListView.CHOICE_MODE_SINGLE

        lifecycleScope.launchWhenCreated {
            val employees = presenter.getAllEmployees()
            employeesAdapter.clear()
            employeesAdapter.addAll(employees)
            employeesAdapter.notifyDataSetChanged()
        }

        return dialog
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    private fun onAttachToContext(context: Context) {
        callback = context as? EmployeeSelectedCallback
    }

    interface EmployeeSelectedCallback {
        fun onEmployeeSelected(employeeIdentifier: Int, employee: EmployeeUiRow)
    }

    companion object {

        const val TAG = "SelectEmployeeDialog"
        const val EMPLOYEE_SELECTED_RESULT_KEY = "employee selected result key"
        const val SELECTED_EMPLOYEE_ID_KEY = "selected employeeId key"
        private const val EMPLOYEE_IDENTIFIER_KEY = "employee_identifier_key"

        fun newInstance(employeeIdentifier: Int) = SelectEmployeeDialog().apply {
            arguments = bundleOf(EMPLOYEE_IDENTIFIER_KEY to employeeIdentifier)
        }
    }
}