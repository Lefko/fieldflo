package com.fieldflo.screens.selectEmployee

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.Employee
import com.fieldflo.usecases.EmployeeUseCases
import javax.inject.Inject

class SelectEmployeePresenter @Inject constructor(
    private val employeeUseCases: EmployeeUseCases
) : ViewModel() {

    suspend fun getAllEmployees(): List<EmployeeUiRow> =
        employeeUseCases.getAllEmployees().map { it.toUiModel() }
}

private fun Employee.toUiModel() = EmployeeUiRow(
    employeeId = this.employeeId,
    employeeName = this.employeeFullName
)

data class EmployeeUiRow(
    val employeeName: String,
    val employeeId: Int
) {
    override fun toString() = employeeName
}
