package com.fieldflo.screens.drawer

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fieldflo.R
import com.fieldflo.common.safeClickListener

class DrawerCompanyAdapter(private val onCompanyClick: (company: BasicCompanyUiModel) -> Unit) :
    ListAdapter<BasicCompanyUiModel, DrawerCompanyViewHolder>(CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrawerCompanyViewHolder {
        val rowLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_drawer_company, parent, false) as ViewGroup
        return DrawerCompanyViewHolder(rowLayout, onCompanyClick)
    }

    override fun onBindViewHolder(holder: DrawerCompanyViewHolder, position: Int) {
        val uiModel = getItem(position)
        holder.bind(uiModel)
    }
}

class DrawerCompanyViewHolder(
    private val rowLayout: ViewGroup,
    private val onCompanyClick: (company: BasicCompanyUiModel) -> Unit
) : RecyclerView.ViewHolder(rowLayout) {

    private val companyNameTV =
        rowLayout.findViewById<TextView>(R.id.row_drawer_company_company_nameTV)

    fun bind(uiModel: BasicCompanyUiModel) {
        companyNameTV.text = uiModel.companyName
        rowLayout.safeClickListener {
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                onCompanyClick(uiModel)
            }
        }
    }
}

private val CALLBACK = object : DiffUtil.ItemCallback<BasicCompanyUiModel>() {

    override fun areItemsTheSame(oldItem: BasicCompanyUiModel, newItem: BasicCompanyUiModel) =
        oldItem.securityKey == newItem.securityKey && oldItem.token == newItem.token

    override fun areContentsTheSame(oldItem: BasicCompanyUiModel, newItem: BasicCompanyUiModel) =
        oldItem.companyName == newItem.companyName
}