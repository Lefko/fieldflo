package com.fieldflo.screens.drawer

data class DrawerViewState(
    val companies: List<BasicCompanyUiModel> = listOf(),
    val internetConnected: Boolean = false,
    val currentCompanyName: String = "",
    val hasCertificatesRole: Boolean = false,
    val canLogout: Boolean = false
)

data class BasicCompanyUiModel(val securityKey: String, val token: String, val companyName: String)
