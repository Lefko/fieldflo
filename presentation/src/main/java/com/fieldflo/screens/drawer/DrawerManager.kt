package com.fieldflo.screens.drawer

import android.content.res.Configuration
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.core.view.isVisible
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.AutoTransition
import androidx.transition.ChangeBounds
import androidx.transition.TransitionManager
import com.fieldflo.R
import com.fieldflo.common.hideKeyboard
import com.fieldflo.di.injector
import com.fieldflo.screens.projects.ProjectsListActivity
import com.fieldflo.screens.settings.SettingsActivity
import com.fieldflo.screens.splashScreen.SplashScreen
import com.google.android.material.navigation.NavigationView
import kotlinx.coroutines.flow.collect

class DrawerManager(
    private val drawerProvider: () -> DrawerLayout,
    private val activityProvider: () -> FragmentActivity,
    toolbarProvider: () -> Toolbar
) : NavigationView.OnNavigationItemSelectedListener {

    private val toggle: ActionBarDrawerToggle
    private val drawerCompanyAdapter = DrawerCompanyAdapter { logout() }

    private var headerCompaniesRV: RecyclerView? = null
    private var currentCompanyNameTV: TextView? = null
    private var statusIconIV: ImageView? = null
    private var statusTextTV: TextView? = null
    private var logoutButton: TextView? = null

    init {
        val drawerLayout = drawerProvider.invoke()
        toggle = ActionBarDrawerToggle(
            activityProvider.invoke(),
            drawerLayout,
            toolbarProvider.invoke(),
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        initViews()
    }

    private lateinit var presenter: DrawerPresenter

    fun onConfigChange(newConfig: Configuration) {
        toggle.onConfigurationChanged(newConfig)
    }

    fun onBackPress() = closeDrawer()

    fun refreshSelected() {
        val navView = drawerProvider.invoke().findViewById<NavigationView>(R.id.nav_view)
        setSelectedItem(navView)
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {

            R.id.nav_drawer_projects -> {
                activityProvider.invoke().hideKeyboard()
                closeDrawer()
                if (activityProvider.invoke().javaClass.simpleName != ProjectsListActivity::class.java.simpleName) {
                    ProjectsListActivity.start(activityProvider.invoke())
                    closeDrawer()
                }

                return true
            }

            R.id.nav_drawer_settings -> {
                activityProvider.invoke().hideKeyboard()
                closeDrawer()
                if (activityProvider.invoke().javaClass.simpleName != SettingsActivity::class.java.simpleName) {
                    SettingsActivity.start(activityProvider.invoke())
                    closeDrawer()
                }
                return true
            }
        }

        return false
    }

    private fun closeDrawer(): Boolean {
        val drawer = drawerProvider.invoke()
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
            return true
        }
        return false
    }

    private fun initViews() {
        val navView = drawerProvider.invoke().findViewById<NavigationView>(R.id.nav_view)
        navView.setNavigationItemSelectedListener(this)
        initHeaderViews(navView)
        initMenu(navView)

        val activity: FragmentActivity = activityProvider.invoke()
        presenter =
            ViewModelProvider(activity, activity.injector.drawerViewModelFactory())
                .get(DrawerPresenter::class.java)

        activity.lifecycleScope.launchWhenCreated {
            presenter.observeViewState().collect { render(it) }
        }
    }

    private fun initHeaderViews(navigationView: NavigationView) {
        for (i in 0 until navigationView.headerCount) {
            val headerView: ConstraintLayout = navigationView.getHeaderView(i) as ConstraintLayout
            val headerCurrentCompany = headerView.findViewById<View>(R.id.drawer_header_expand_mask)
            val expandIV = headerView.findViewById<ImageView>(R.id.drawer_header_expand_iconIV)
            headerCompaniesRV = headerView.findViewById(R.id.drawer_header_all_companiesRV)
            currentCompanyNameTV = headerView.findViewById(R.id.drawer_header_company_nameTV)
            headerCurrentCompany.setOnClickListener {
                headerCompaniesRV?.let {
                    val isExpanded = it.isVisible
                    it.isVisible = !it.isVisible
                    if (isExpanded) {
                        expandIV.animate().rotation(0f).start()
                        TransitionManager.beginDelayedTransition(headerView, AutoTransition())
                    } else {
                        expandIV.animate().rotation(180f).start()
                        TransitionManager.beginDelayedTransition(headerView, ChangeBounds())
                    }
                }
            }
            headerCompaniesRV?.apply {
                layoutManager = LinearLayoutManager(activityProvider.invoke())
                adapter = drawerCompanyAdapter
            }
            if (headerCompaniesRV != null) {
                break
            }
        }
    }

    private fun initMenu(navigationView: NavigationView) {
        val statusView = navigationView.menu.findItem(R.id.nav_drawer_status).actionView
        statusIconIV = statusView.findViewById(R.id.nav_drawer_status_iconIV)
        statusTextTV = statusView.findViewById(R.id.nav_drawer_status_textTV)
        logoutButton = navigationView.findViewById(R.id.nav_view_logout_button)
        logoutButton?.setOnClickListener { logout() }

        setSelectedItem(navigationView)
    }

    private fun setSelectedItem(navigationView: NavigationView) {
        val currentClassName = activityProvider.invoke().javaClass.simpleName

        navigationView.menu.findItem(R.id.nav_drawer_projects).isChecked =
            currentClassName == ProjectsListActivity::class.java.simpleName

        navigationView.menu.findItem(R.id.nav_drawer_settings).isChecked =
            currentClassName == SettingsActivity::class.java.simpleName
    }

    private fun render(viewState: DrawerViewState) {
        drawerCompanyAdapter.submitList(viewState.companies)
        currentCompanyNameTV?.text = viewState.currentCompanyName
        refreshSelected()
        statusIconIV?.setImageResource(
            if (viewState.internetConnected) {
                R.drawable.view_status_circle_green
            } else {
                R.drawable.view_status_circle_red
            }
        )
        statusTextTV?.setText(
            if (viewState.internetConnected) {
                R.string.drawer_online
            } else {
                R.string.drawer_offline
            }
        )

        statusTextTV?.setTextColor(
            ContextCompat.getColor(
                activityProvider.invoke(),
                if (viewState.internetConnected) {
                    R.color.status_circle_green
                } else {
                    R.color.attention
                }
            )
        )
        logoutButton?.isEnabled = viewState.canLogout
    }

    private fun logout() {
        presenter.logout()
        SplashScreen.start(activityProvider.invoke())
        activityProvider.invoke().finish()
    }
}