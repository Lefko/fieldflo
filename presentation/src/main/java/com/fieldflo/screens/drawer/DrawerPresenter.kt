package com.fieldflo.screens.drawer

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fieldflo.common.networkConnection.IConnectionData
import com.fieldflo.data.persistence.db.entities.CompanyData
import com.fieldflo.usecases.AuthUseCases
import com.fieldflo.usecases.CompanyUseCases
import com.fieldflo.usecases.UserRolesUseCases
import io.shipbook.shipbooksdk.ShipBook
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

class DrawerPresenter @Inject constructor(
    userRolesUseCases: UserRolesUseCases,
    private val authUseCases: AuthUseCases,
    private val companyUseCases: CompanyUseCases,
    private val networkConnection: IConnectionData
) : ViewModel() {

    private val viewStateObservable = MutableStateFlow(
        DrawerViewState(
            currentCompanyName = companyUseCases.getCurrentCompanyName(),
            hasCertificatesRole = userRolesUseCases.hasCertificateRole(),
            internetConnected = networkConnection.isInternetConnected
        )
    )

    init {
        viewModelScope.launch {
            networkConnection.getInternetConnected()
                .collect {
                    val prevState = viewStateObservable.value
                    val newState = prevState.copy(internetConnected = it)
                    viewStateObservable.value = newState
                }
        }

        viewModelScope.launch(Dispatchers.IO) {
            companyUseCases.getCompaniesListFlow()
                .map {
                    it.filter {
                        it.companyName != companyUseCases.getCurrentCompanyName()
                    }
                }
                .collect { companies ->
                    val prevState = viewStateObservable.value
                    val newState = prevState.copy(companies = companies.map { it.toUiModel() })
                    viewStateObservable.value = newState
                }
        }

        viewModelScope.launch(Dispatchers.IO) {
            authUseCases.canLogout()
                .collect {
                    val prevState = viewStateObservable.value
                    val newState = prevState.copy(canLogout = it)
                    viewStateObservable.value = newState
                }
        }
    }

    fun observeViewState(): Flow<DrawerViewState> = viewStateObservable

    fun logout() {
        ShipBook.logout()
        authUseCases.logout()
    }
}

private fun CompanyData.toUiModel() = BasicCompanyUiModel(
    securityKey = this.securityKey,
    token = this.token,
    companyName = this.companyName
)
