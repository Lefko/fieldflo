package com.fieldflo.screens.imagePicker

import android.app.Activity
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.OpenableColumns
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.fieldflo.R
import com.fieldflo.screens.formDailyFieldReport.FormDailyFieldReportActivity.Companion.REQUEST_IMAGE
import com.yalantis.ucrop.UCrop
import timber.log.Timber
import java.io.File




class ImagePickerActivity : AppCompatActivity() {

    companion object {

        private const val INTENT_IMAGE_PICKER_OPTION = "image_picker_option"
        const val INTENT_FILE_NAME_NO_EXTENSION = "fileName"

        private const val REQUEST_IMAGE_CAPTURE = 0
        private const val REQUEST_GALLERY_IMAGE = 1

        const val RESULT_IMAGE_PATH = "path"

        fun startTakePic(sender: Activity, fileName: String) {
            val intent = Intent(sender, ImagePickerActivity::class.java).apply {
                putExtra(INTENT_IMAGE_PICKER_OPTION, REQUEST_IMAGE_CAPTURE)
                putExtra(INTENT_FILE_NAME_NO_EXTENSION, fileName)
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            sender.startActivityForResult(intent, REQUEST_IMAGE)
        }

        fun startSelectFromGallery(sender: Activity, fileName: String) {
            val intent = Intent(sender, ImagePickerActivity::class.java).apply {
                putExtra(INTENT_IMAGE_PICKER_OPTION, REQUEST_GALLERY_IMAGE)
                putExtra(INTENT_FILE_NAME_NO_EXTENSION, fileName)
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            sender.startActivityForResult(intent, REQUEST_IMAGE)
        }

        fun startTakePic(context: Context, sender: Fragment, fileName: String) {
            val intent = Intent(context, ImagePickerActivity::class.java).apply {
                putExtra(INTENT_IMAGE_PICKER_OPTION, REQUEST_IMAGE_CAPTURE)
                putExtra(INTENT_FILE_NAME_NO_EXTENSION, fileName)
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            sender.startActivityForResult(intent, REQUEST_IMAGE)
        }

        fun startSelectFromGallery(context: Context, sender: Fragment, fileName: String) {
            val intent = Intent(context, ImagePickerActivity::class.java).apply {
                putExtra(INTENT_IMAGE_PICKER_OPTION, REQUEST_GALLERY_IMAGE)
                putExtra(INTENT_FILE_NAME_NO_EXTENSION, fileName)
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            sender.startActivityForResult(intent, REQUEST_IMAGE)
        }

    }

    private val fileName: String by lazy { intent.getStringExtra(INTENT_FILE_NAME_NO_EXTENSION)!! }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_picker)

        val requestCode = intent.getIntExtra(INTENT_IMAGE_PICKER_OPTION, -1)
        when (requestCode) {
            REQUEST_IMAGE_CAPTURE -> takeCameraImage()
            REQUEST_GALLERY_IMAGE -> chooseImageFromGallery()
            else -> setResultCancelled()
        }
    }

    private fun takeCameraImage() {
        val fileName = "$fileName.jpg"
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, getCacheImagePath(fileName))
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    private fun getCacheImagePath(fileName: String): Uri {
        val path = File(externalCacheDir, "camera")
        if (!path.exists()) path.mkdirs()
        val image = File(path, fileName)
        return FileProvider.getUriForFile(this, "$packageName.provider", image)
    }

    private fun chooseImageFromGallery() {
        val pickPhoto = Intent(
            Intent.ACTION_PICK,
            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(pickPhoto, REQUEST_GALLERY_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_IMAGE_CAPTURE -> if (resultCode == Activity.RESULT_OK) {
                cropImage(getCacheImagePath("$fileName.jpg"))
            } else {
                setResultCancelled()
            }
            REQUEST_GALLERY_IMAGE -> if (resultCode == Activity.RESULT_OK) {
                val imageUri = data?.data
                if (imageUri != null) {
                    cropImage(imageUri)
                } else {
                    setResultCancelled()
                }
            } else {
                setResultCancelled()
            }
            UCrop.REQUEST_CROP -> if (resultCode == Activity.RESULT_OK) {
                handleUCropResult(data)
            } else {
                setResultCancelled()
            }
            UCrop.RESULT_ERROR -> {
                val cropError = UCrop.getError(data!!)
                Timber.e(cropError, "Crop error")
                setResultCancelled()
            }
            else -> setResultCancelled()
        }
    }

    private fun cropImage(sourceUri: Uri) {
        val destinationUri = Uri.fromFile(File(cacheDir, queryName(contentResolver, sourceUri)))
        val options = UCrop.Options().apply {
            setToolbarColor(ContextCompat.getColor(this@ImagePickerActivity, R.color.colorPrimary))
            setStatusBarColor(
                ContextCompat.getColor(
                    this@ImagePickerActivity,
                    R.color.colorPrimary
                )
            )
            setActiveControlsWidgetColor(
                ContextCompat.getColor(
                    this@ImagePickerActivity,
                    R.color.colorPrimary
                )
            )
            setShowCropFrame(true)
            setFreeStyleCropEnabled(true)
        }

        UCrop.of(sourceUri, destinationUri)
            .withOptions(options)
            .start(this)
    }

    private fun queryName(resolver: ContentResolver, uri: Uri): String {
        val returnCursor = resolver.query(uri, null, null, null, null)!!
        val nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        returnCursor.moveToFirst()
        val name = returnCursor.getString(nameIndex)
        returnCursor.close()
        return name
    }

    private fun handleUCropResult(data: Intent?) {
        if (data == null) {
            setResultCancelled()
            return
        }
        val resultUri = UCrop.getOutput(data)
        if (resultUri != null) {
            setResultOk(resultUri)
        } else {
            setResultCancelled()
        }
    }

    private fun setResultCancelled() {
        val intent = Intent()
        setResult(Activity.RESULT_CANCELED, intent)
        finish()
    }

    private fun setResultOk(imagePath: Uri) {
        val intent = Intent().apply {
            putExtra(RESULT_IMAGE_PATH, imagePath)
            putExtra(INTENT_FILE_NAME_NO_EXTENSION, fileName)
        }
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}
