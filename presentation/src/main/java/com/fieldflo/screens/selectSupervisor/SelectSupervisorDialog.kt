package com.fieldflo.screens.selectSupervisor

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.ListView
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.fieldflo.R
import com.fieldflo.di.injector

class SelectSupervisorDialog : DialogFragment() {

    private var callback: SupervisorSelectedCallback? = null

    private val presenter by viewModels<SelectSupervisorPresenter> { injector.selectSupervisorViewModelFactory() }
    private val employeesAdapter by lazy { EmployeesAdapter(requireActivity()) }

    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val dialog = AlertDialog.Builder(requireContext())
            .setTitle(R.string.dialog_select_employee_title)
            .setAdapter(employeesAdapter, null)
            .setPositiveButton(android.R.string.ok, { _, _ ->
                val listView = (dialog as AlertDialog).listView

                val selectedEmployees = (0 until listView.adapter.count)
                    .filter { listView.checkedItemPositions[it] }
                    .mapNotNull { employeesAdapter.getItem(it) }

                if (selectedEmployees.isEmpty()) {
                    dismiss()
                } else {
                    callback?.onSupervisorSelected(selectedEmployees[0])
                    setFragmentResult(
                        SUPERVISOR_SELECTED_RESULT_KEY,
                        bundleOf(
                            SELECTED_EMPLOYEE_ID_KEY to selectedEmployees[0].employeeId,
                            SELECTED_EMPLOYEE_NAME_KEY to selectedEmployees[0].employeeName
                        )
                    )
                }
            })
            .setNegativeButton(android.R.string.cancel, { _, _ ->
                dismiss()
            })
            .show()

        dialog.listView.itemsCanFocus = false
        dialog.listView.choiceMode = ListView.CHOICE_MODE_SINGLE

        lifecycleScope.launchWhenCreated {
            val employees = presenter.getAllEmployees()
            employeesAdapter.clear()
            employeesAdapter.addAll(employees)
            employeesAdapter.notifyDataSetChanged()
        }

        return dialog
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onAttachToContext(context)
    }

    @Suppress("DEPRECATION")
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        onAttachToContext(activity)
    }

    private fun onAttachToContext(context: Context) {
        callback = context as? SupervisorSelectedCallback
    }

    interface SupervisorSelectedCallback {
        fun onSupervisorSelected(employee: SupervisorEmployeeUiRow)
    }

    companion object {

        const val TAG = "SelectEmployeeDialog"
        const val SUPERVISOR_SELECTED_RESULT_KEY = "supervisor selected result"
        const val SELECTED_EMPLOYEE_ID_KEY = "selected employeeId key"
        const val SELECTED_EMPLOYEE_NAME_KEY = "selected employee name key"

        fun newInstance() = SelectSupervisorDialog()
    }
}