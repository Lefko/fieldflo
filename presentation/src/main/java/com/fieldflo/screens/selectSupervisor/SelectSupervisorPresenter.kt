package com.fieldflo.screens.selectSupervisor

import androidx.lifecycle.ViewModel
import com.fieldflo.data.persistence.db.entities.Employee
import com.fieldflo.usecases.EmployeeUseCases
import javax.inject.Inject

class SelectSupervisorPresenter @Inject constructor(
    private val employeeUseCases: EmployeeUseCases
) : ViewModel() {

    suspend fun getAllEmployees(): List<SupervisorEmployeeUiRow> =
        employeeUseCases.getAllEmployees()
            .filter { it.admin }
            .map { it.toUiModel() }
}

private fun Employee.toUiModel() = SupervisorEmployeeUiRow(
    employeeId = this.employeeId,
    employeeName = this.employeeFullName
)

data class SupervisorEmployeeUiRow(
    val employeeName: String,
    val employeeId: Int
) {
    override fun toString() = employeeName
}
