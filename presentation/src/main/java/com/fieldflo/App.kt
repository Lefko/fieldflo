package com.fieldflo

import android.app.Application
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.work.Configuration
import androidx.work.WorkManager
import com.fieldflo.di.ApplicationComponent
import com.fieldflo.di.DaggerApplicationComponent
import com.fieldflo.di.DaggerComponentProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class App : Application(), DaggerComponentProvider, LifecycleObserver {

    private val applicationScopeHolder by lazy { AppScopeHolder(CoroutineScope(SupervisorJob())) }
    private val applicationScope get() = applicationScopeHolder.scope

    override val component: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .applicationContext(applicationContext)
            .appScopeHolder(applicationScopeHolder)
            .build()
    }

    private val networkConnection by lazy {
        component.dataConnection()
    }

    override fun onCreate() {
        super.onCreate()

        initWorkManager()

        ProcessLifecycleOwner.get().lifecycle.addObserver(this)

        applicationScope.launch {
            networkConnection.getInternetConnected().collect {
                if (it) {
                    initiateServerSync()
                }
            }
        }

        init()
    }

    override fun onTerminate() {
        applicationScope.cancel()
        super.onTerminate()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onBackground() {
        initiateServerSync()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onForeground() {
        initiateServerSync()
    }

    private fun initiateServerSync() {
        applicationScope.launch {
            component.serverUpdatesUseCase().performServerSync()
        }
    }

    private fun initWorkManager() {
        WorkManager.initialize(this, Configuration.Builder().run {
            setWorkerFactory(component.fieldFloWorkerFactory())
            build()
        })
    }
}

class AppScopeHolder(val scope: CoroutineScope)