package com.fieldflo.data.persistence.db.daos

import androidx.room.*
import com.fieldflo.data.persistence.db.entities.DailyLogFormData

@Dao
abstract class DailyLogDao2 {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addDailyLog(dailyLog: DailyLogFormData): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun updateDailyLog(dailyLog: DailyLogFormData): Int

    @Query("SELECT * FROM DailyLogs WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun getDailyLogByInternalFormId(
        internalFormId: Int,
        companyId: Int
    ): DailyLogFormData?

    @Query("SELECT supervisorVerify FROM DailyLogs WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun isDailyLogClosable(internalFormId: Int, companyId: Int): Boolean?

    @Query("DELETE FROM DailyLogs WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun deleteDailyLog(internalFormId: Int, companyId: Int): Int

    @Query("DELETE FROM DailyLogs WHERE companyId = :companyId")
    abstract suspend fun deleteAllDailyLogs(companyId: Int): Int
}
