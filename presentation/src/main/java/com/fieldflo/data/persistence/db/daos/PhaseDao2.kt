package com.fieldflo.data.persistence.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.fieldflo.data.persistence.db.entities.Phase

@Dao
abstract class PhaseDao2 {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addPhases(phases: List<Phase>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addPhase(phase: Phase)

    @Query("SELECT * FROM Phases WHERE phaseId = :phaseId AND companyId = :companyId")
    abstract suspend fun getPhaseById(phaseId: Int, companyId: Int): Phase?

    @Query("SELECT * FROM Phases WHERE phaseActive = 1 AND phaseDeleted = 0 AND companyId = :companyId")
    abstract suspend fun getPhasesAllPhases(companyId: Int): List<Phase>

    @Query("SELECT * FROM Phases WHERE phaseActive = 1 AND phaseDeleted = 0 AND (projectId = :projectId OR projectId = 0) AND companyId = :companyId")
    abstract suspend fun getPhasesForProject(projectId: Int, companyId: Int): List<Phase>

    @Query("DELETE FROM PHASES WHERE phaseId = :phaseId AND companyId = :companyId")
    abstract suspend fun deletePhase(phaseId: Int, companyId: Int)

    @Query("DELETE FROM PHASES WHERE companyId = :companyId")
    abstract suspend fun deleteAllPhases(companyId: Int)

    @Query("UPDATE PHASES SET phaseDeleted = 1 WHERE phaseId = :phaseId AND companyId = :companyId")
    abstract suspend fun setPhaseDeleted(phaseId: Int, companyId: Int)
}
