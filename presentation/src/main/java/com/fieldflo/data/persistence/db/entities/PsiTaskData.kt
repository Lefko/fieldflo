package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PsiTaskData(

    @PrimaryKey(autoGenerate = true)
    val psiTaskId: Int = 0,

    val companyId: Int = 0,
    val projectId: Int = 0,
    val internalFormId: Int = 0,
    val formDataId: Int = 0,

    val task: String = "",
    val hazard: String = "",
    val control: String = "",
    val risk: Int = 0
)
