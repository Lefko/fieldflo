package com.fieldflo.data.persistence.db.entities

data class ProjectEmergencyInfo(
    val fireDepartmentName: String,
    val fireDepartmentAddress: String,
    val fireDepartmentCity: String,
    val fireDepartmentState: String,
    val fireDepartmentZipCode: String,
    val fireDepartmentPhone: String,
    val policeDepartmentName: String,
    val policeDepartmentAddress: String,
    val policeDepartmentCity: String,
    val policeDepartmentState: String,
    val policeDepartmentZipCode: String,
    val policeDepartmentPhone: String,
    val medicalDepartmentName: String,
    val medicalDepartmentAddress: String,
    val medicalDepartmentCity: String,
    val medicalDepartmentState: String,
    val medicalDepartmentZipCode: String,
    val medicalDepartmentPhone: String
)