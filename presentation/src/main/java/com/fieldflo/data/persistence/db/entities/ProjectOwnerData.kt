package com.fieldflo.data.persistence.db.entities

data class ProjectOwnerData(
    val ownerCompanyName: String,
    val ownerAddress: String,
    val ownerCity: String,
    val ownerZipCode: String,
    val ownerNameOfContact: String,
    val ownerPhoneNumber: String,
    val ownerFaxNumber: String,
    val ownerCellPhone: String,
    val ownerState: String
)