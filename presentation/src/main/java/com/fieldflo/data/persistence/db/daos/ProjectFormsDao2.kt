package com.fieldflo.data.persistence.db.daos

import androidx.room.*
import com.fieldflo.data.persistence.db.entities.ActiveFormData
import com.fieldflo.data.persistence.db.entities.ClosedFormData
import com.fieldflo.data.persistence.db.entities.DeletedFormData
import com.fieldflo.data.persistence.db.entities.ProjectForm
import kotlinx.coroutines.flow.Flow
import java.util.*

@Dao
abstract class ProjectFormsDao2 {

    @Query(
        "SELECT (COUNT(internalFormId) > 0) FROM ProjectForms WHERE formClosed = 1 AND isCloseSynced = 0 AND companyId = :companyId"
    )
    abstract fun hasUnsyncedFormsFlow(companyId: Int): Flow<Boolean>

    @Query(
        "SELECT (COUNT(internalFormId) > 0) FROM ProjectForms WHERE formClosed = 1 AND isCloseSynced = 0 AND companyId = :companyId"
    )
    abstract suspend fun hasUnsyncedForms(companyId: Int): Boolean

    @Query("SELECT isCloseSynced FROM ProjectForms WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun isFormSynced(internalFormId: Int, companyId: Int): Boolean?

    @Query("SELECT * FROM ProjectForms WHERE formClosed = 1 AND isCloseSynced = 0 AND companyId = :companyId")
    abstract suspend fun getUnsyncedClosedForms(companyId: Int): List<ProjectForm>

    @Query("UPDATE ProjectForms SET formActive = 1, formClosed = 0, closeDate = 0, hasSyncError = 0, isCloseSynced = 0 WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun reopenForm(internalFormId: Int, companyId: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addProjectForms(projectForms: List<ProjectForm>)

    @Query("SELECT * FROM ProjectForms WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun getProjectForms(projectId: Int, companyId: Int): List<ProjectForm>

    @Query("SELECT internalFormId, projectId, formTypeId, formName, formActive, formDate FROM ProjectForms WHERE projectId = :projectId AND formActive = 1 AND companyId = :companyId")
    abstract suspend fun getActiveProjectForms(projectId: Int, companyId: Int): List<ActiveFormData>

    @Query("SELECT internalFormId, projectId, formTypeId, formName, formActive, formDate FROM ProjectForms WHERE projectId = :projectId AND formActive = 1 AND companyId = :companyId")
    abstract fun getActiveProjectFormsFlow(
        projectId: Int,
        companyId: Int
    ): Flow<List<ActiveFormData>>

    @Query(
        "SELECT internalFormId FROM ProjectForms WHERE projectId = :projectId AND formTypeId = :formTypeId AND internalFormId < :internalFormId AND formClosed AND companyId = :companyId ORDER BY internalFormId DESC LIMIT 1"
    )
    abstract suspend fun getPreviousFormInternalFormId(
        projectId: Int,
        formTypeId: Int,
        internalFormId: Int,
        companyId: Int
    ): Int?

    @Query("SELECT * FROM ProjectForms WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun getProjectFormById(internalFormId: Int, companyId: Int): ProjectForm?

    @Update
    abstract suspend fun updateProjectForm(projectForm: ProjectForm)

    @Query(
        "SELECT internalFormId, projectId, formName, closeDate, isCloseSynced, hasSyncError FROM ProjectForms WHERE projectId = :projectId AND formClosed = 1 AND companyId = :companyId ORDER BY closeDate DESC"
    )
    abstract fun getClosedProjectFormsFlow(
        projectId: Int,
        companyId: Int
    ): Flow<List<ClosedFormData>>

    @Query(
        "UPDATE ProjectForms SET formClosed = 1, closeDate = :now, formActive = 0, formDeleted = 0 WHERE internalFormId = :internalFormId AND companyId = :companyId"
    )
    abstract suspend fun closeProjectForm(internalFormId: Int, companyId: Int, now: Date = Date())

    @Query("UPDATE ProjectForms SET isCloseSynced = 1 WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun setCloseSynced(internalFormId: Int, companyId: Int)

    @Query(
        "SELECT internalFormId, projectId, formName, deleteDate FROM ProjectForms WHERE projectId = :projectId AND formDeleted = 1 AND companyId = :companyId  ORDER BY deleteDate DESC"
    )
    abstract fun getDeletedProjectFormsFlow(
        projectId: Int,
        companyId: Int
    ): Flow<List<DeletedFormData>>

    @Query(
        "UPDATE ProjectForms SET formDeleted = 1, deleteDate = :now, formClosed = 0, formActive = 0 WHERE internalFormId = :internalFormId AND companyId = :companyId"
    )
    abstract suspend fun deleteProjectForm(
        internalFormId: Int,
        companyId: Int,
        now: Date = Date()
    ): Int

    @Query("DELETE FROM ProjectForms WHERE companyId = :companyId")
    abstract suspend fun deleteProjectForms(companyId: Int)
}
