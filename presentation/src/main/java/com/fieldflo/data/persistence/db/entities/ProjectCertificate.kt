package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity
import java.util.*

@Entity(
    tableName = "ProjectCertificates",
    primaryKeys = ["certificateId", "companyId", "projectId"]
)
data class ProjectCertificate(
    val certificateId: Int = 0,
    val projectId: Int = 0,
    val companyId: Int = 0,

    val employeeId: Int = 0,
    val certificateName: String = "",
    val certificateExpirationDate: Date = Date()
)