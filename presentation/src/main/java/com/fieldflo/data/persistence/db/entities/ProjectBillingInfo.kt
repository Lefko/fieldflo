package com.fieldflo.data.persistence.db.entities

data class ProjectBillingInfo(
    val billingCompanyName: String,
    val billingPurchaseOrWorkOrderNumber: String,
    val billingTermsOfContact: String,
    val billingClientContact: String,
    val billingAddress: String,
    val billingCity: String,
    val billingState: String,
    val billingZipCode: String,
    val billingPhoneNumber: String,
    val billingFaxNumber: String
)