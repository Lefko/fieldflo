package com.fieldflo.data.persistence.db.entities

data class ProjectPayrollData(
    val payrollWeeklyCertifiedPayroll: Boolean,
    val payrollUnionJob: Boolean,
    val payrollPrevailingWage: Boolean
)
