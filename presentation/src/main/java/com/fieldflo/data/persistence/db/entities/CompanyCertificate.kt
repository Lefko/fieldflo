package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity
import java.util.*

@Entity(tableName = "CompanyCertificates", primaryKeys = ["certificateId", "companyId"])
data class CompanyCertificate(
    val certificateId: Int = 0,
    val companyId: Int = 0,
    val employeeId: Int = 0,
    val certificateName: String = "",
    val certificateExpirationDate: Date = Date(),
    val certificateFileUrl: String = "",
    val certificateFile: String = "",
    val isDeleted: Boolean = false
) {
    fun toProjectCert(projectId: Int): ProjectCertificate {
        return ProjectCertificate(
            certificateId = certificateId,
            projectId = projectId,
            companyId = companyId,
            employeeId = employeeId,
            certificateName = certificateName,
            certificateExpirationDate = certificateExpirationDate
        )
    }
}