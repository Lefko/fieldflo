package com.fieldflo.data.persistence.db.entities

data class ProjectPerDiemData(
    val requiresPerDiem: Boolean,
    val perDiemType: String,
    val perDiemHourlyAmount: Double,
    val perDiemDailyAmount: Double,
    val perDiemNotes: String,
    val perDiemAutoApplyToTimesheet: Boolean
)