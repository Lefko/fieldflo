package com.fieldflo.data.persistence.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.fieldflo.data.persistence.db.entities.SubContractor

@Dao
abstract class SubContractorsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addAllSubContractors(subContractors: List<SubContractor>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addSubcontractor(subContractor: SubContractor)

    @Query("SELECT * FROM SubContractors WHERE companyId = :companyId")
    abstract suspend fun getAllSubContractors(companyId: Int): List<SubContractor>

    @Query("SELECT * FROM SubContractors WHERE subContractorId = :subcontractorId AND companyId = :companyId")
    abstract suspend fun getSubContractorsById(subcontractorId: Int, companyId: Int): SubContractor?

    @Query("DELETE FROM SubContractors WHERE companyId = :companyId")
    abstract suspend fun deleteAllSubContractors(companyId: Int): Int

    @Query("DELETE FROM SubContractors WHERE subcontractorId = :subcontractorId AND companyId = :companyId")
    abstract suspend fun deleteSubcontractor(subcontractorId: Int, companyId: Int)

    @Query("UPDATE SubContractors SET subcontractorDeleted = 1 WHERE subcontractorId = :subcontractorId AND companyId = :companyId")
    abstract suspend fun setSubcontractorDeleted(subcontractorId: Int, companyId: Int)
}