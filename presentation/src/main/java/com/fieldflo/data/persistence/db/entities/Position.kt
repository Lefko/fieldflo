package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity

@Entity(tableName = "Positions", primaryKeys = ["positionId", "companyId"])
data class Position(
    val positionId: Int = 0,
    val companyId: Int = 0,
    val positionName: String = "",
    val projectId: Int = 0,
    val positionCode: String = "",
    val positionActive: Boolean = false,
    val positionDeleted: Boolean = false
)
