package com.fieldflo.data.persistence.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.fieldflo.data.persistence.db.daos.*
import com.fieldflo.data.persistence.db.entities.*

@TypeConverters(RoomTypeConverters::class)
@Database(
    entities = [
        CompanyData::class,

        Employee::class, Position::class, Phase::class, InventoryItem::class, Task::class,

        SubContractor::class, Hauler::class, CompanyCertificate::class,

        Project::class, AvailableForm::class, ProjectForm::class, ProjectPermit::class, ProjectCertificate::class,

        DailyFieldReportFormData::class,
        DailyLogFormData::class, DailyDemoLogFormData::class,
        PsiFormData::class, PsiEmployeeData::class, PsiTaskData::class,
        TimeSheetFormData::class, TimeSheetEmployee::class, TimeSheetEmployeeAsset::class,
        TimesheetEmployeePosition::class, TimeSheetEmployeeTimeLog::class,
        DailyDemoLogEmployee::class, DailyDemoLogSubContractor::class, DailyDemoLogEquipment::class,
        DailyDemoLogMaterials::class, DailyDemoLogTrucking::class

    ], version = 2, exportSchema = true
)
abstract class FieldFloDatabase : RoomDatabase() {
    abstract fun getCompanyDao(): CompanyDao

    abstract fun getEmployeeDao2(): EmployeeDao2

    abstract fun getPositionDao2(): PositionDao2

    abstract fun getPhaseDao2(): PhaseDao2

    abstract fun getInventoryItemDao2(): InventoryItemDao2

    abstract fun getCompanyCertificateDao2(): CompanyCertificateDao2

    abstract fun getTaskDao2(): TaskDao2

    abstract fun getSubContractors(): SubContractorsDao

    abstract fun getHaulers(): HaulersDao

    abstract fun getProjectDao2(): ProjectDao2

    abstract fun getProjectPermitDao2(): ProjectPermitDao2

    abstract fun getAvailableFormDao2(): AvailableFormDao2

    abstract fun getProjectCertificatesDao2(): ProjectCertificatesDao2

    abstract fun getProjectFormsDao2(): ProjectFormsDao2

    abstract fun getPsiDao2(): PsiDao2

    abstract fun getTimeSheetDao(): TimesheetDao
    abstract fun getTimeSheetEmployeeDao(): TimeSheetEmployeeDao
    abstract fun getTimeSheetEmployeePositionDao(): TimeSheetEmployeePositionDao
    abstract fun getTimeSheetEmployeeAssetDao(): TimeSheetEmployeeAssetDao

    abstract fun getDailyFieldReportDao2(): DailyFieldReportDao2

    abstract fun getDailyLogDao2(): DailyLogDao2
    abstract fun getDailyDemoLogDao(): DailyDemoLogDao
}