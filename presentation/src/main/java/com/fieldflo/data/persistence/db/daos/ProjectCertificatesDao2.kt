package com.fieldflo.data.persistence.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.fieldflo.data.persistence.db.entities.ProjectCertificate

@Dao
abstract class ProjectCertificatesDao2 {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addAllProjectCertificates(certificates: List<ProjectCertificate>)

    @Query("SELECT * FROM ProjectCertificates WHERE projectId = :projectId AND companyId = :companyId")
    abstract fun getProjectCertificatesByProjectId(
        projectId: Int,
        companyId: Int
    ): List<ProjectCertificate>

    @Query("DELETE FROM ProjectCertificates WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun deleteProjectCertificateFromProject(projectId: Int, companyId: Int)

    @Query("DELETE FROM ProjectCertificates WHERE companyId = :companyId")
    abstract suspend fun deleteProjectCertificates(companyId: Int)
}
