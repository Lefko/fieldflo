package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity

@Entity(tableName = "Phases", primaryKeys = ["companyId", "phaseId"])
data class Phase(
    val phaseName: String = "",
    val phaseId: Int = 0,
    val phaseCode: String = "",
    val projectId: Int = 0,
    val phaseActive: Boolean = true,
    val phaseDeleted: Boolean = false,
    val companyId: Int = 0
)