package com.fieldflo.data.persistence.db.entities

data class ProjectInsuranceCertificateData(
    val insuranceAdditionalInsured: String,
    val insuranceAddress: String,
    val insuranceCity: String,
    val insuranceState: String,
    val insuranceZipCode: String,
    val insurancePrimaryWordingRequest: String,
    val insuranceClientProjectNumber: String,
    val insuranceOtherInstructions: String,
    val insuranceOtherInstructions2: String,
    val insuranceOtherInstructions3: String,
    val insuranceOtherInstructions4: String,
    val insuranceRegMailOrigToCertHolder: String,
    val insuranceFedexOrigToCertHolder: String,
    val insuranceFaxToClient: String,
    val insuranceFaxCopy: String
)
