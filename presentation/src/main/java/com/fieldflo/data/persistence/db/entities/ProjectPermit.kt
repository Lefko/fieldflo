package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity
import java.util.*

@Entity(tableName = "ProjectPermits", primaryKeys = ["companyId", "projectId", "permitId"])
data class ProjectPermit(
    val companyId: Int = 0,
    val projectId: Int = 0,
    val permitId: Int = 0,
    val permitType: String = "",
    val fee: Double = 0.0,
    val number: String = "",
    val expireDate: Date = Date(0),
    val notes: String = "",
    val permitImageUrl: String,
    val documentFile: String
)
