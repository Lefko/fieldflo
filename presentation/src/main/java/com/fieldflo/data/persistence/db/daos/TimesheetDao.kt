package com.fieldflo.data.persistence.db.daos

import androidx.room.*
import com.fieldflo.data.persistence.db.entities.TimeSheetEmployeeAsset
import com.fieldflo.data.persistence.db.entities.TimeSheetFormData
import kotlinx.coroutines.flow.Flow

@Dao
abstract class TimesheetDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addTimeSheetFormData(timeSheetFormData: TimeSheetFormData): Long

    @Query("SELECT COUNT(*) > 0 FROM TimeSheetFormData WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun hasTimeSheetFormData(internalFormId: Int, companyId: Int): Boolean

    @Query("SELECT * FROM TimeSheetFormData WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun getTimeSheetFormDataByInternalId(
        internalFormId: Int,
        companyId: Int
    ): TimeSheetFormData?

    @Query("SELECT * FROM TimeSheetFormData WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract fun getTimesheetDataFlow(internalFormId: Int, companyId: Int): Flow<TimeSheetFormData>

    @Query("SELECT * FROM TimeSheetFormData WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract fun getTimeSheetData(internalFormId: Int, companyId: Int): TimeSheetFormData

    @Query(
        "UPDATE TimeSheetFormData SET supervisorId = :supervisorId WHERE internalFormId = :internalFormId AND companyId = :companyId"
    )
    abstract suspend fun updateTimeSheetSupervisor(
        supervisorId: Int,
        internalFormId: Int,
        companyId: Int
    )

    @Query("SELECT supervisorId FROM TimeSheetFormData WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun getTimeSheetSupervisorId(internalFormId: Int, companyId: Int): Int

    @Query("SELECT notes FROM TimeSheetFormData WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun getTimeSheetNotes(internalFormId: Int, companyId: Int): String

    @Query("UPDATE TimeSheetFormData SET notes = :newNotes WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun updateTimeSheetNotes(internalFormId: Int, newNotes: String, companyId: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addTimeSheetEmployeeAsset(timeSheetEmployeeAsset: TimeSheetEmployeeAsset)

    @Transaction
    open suspend fun deleteAllTimeSheetFormData(internalFormId: Int, companyId: Int) {
        _deleteTimeSheetEmployeeAssetsData(internalFormId, companyId)
        _deleteTimeSheetEmployeeTimeLogsData(internalFormId, companyId)
        _deleteTimeSheetEmployeePositionsData(internalFormId, companyId)
        _deleteTimeSheetEmployeesData(internalFormId, companyId)
        _deleteTimeSheetFormData(internalFormId, companyId)
    }

    @Transaction
    open suspend fun deleteAllTimeSheetFormData(companyId: Int) {
        _deleteTimeSheetEmployeeAssetsData(companyId)
        _deleteTimeSheetEmployeeTimeLogsData(companyId)
        _deleteTimeSheetEmployeePositionsData(companyId)
        _deleteTimeSheetEmployeesData(companyId)
        _deleteTimeSheetFormData(companyId)
    }

    @Query("DELETE FROM TimeSheetEmployeeAssets WHERE internalFormId = :internalFormId AND companyId = :companyId")
    protected abstract suspend fun _deleteTimeSheetEmployeeAssetsData(
        internalFormId: Int,
        companyId: Int
    )

    @Query("DELETE FROM TimeSheetEmployeeAssets WHERE companyId = :companyId")
    protected abstract suspend fun _deleteTimeSheetEmployeeAssetsData(companyId: Int)

    @Query("DELETE FROM TimeSheetEmployeeTimeLogs WHERE internalFormId = :internalFormId AND companyId = :companyId")
    protected abstract suspend fun _deleteTimeSheetEmployeeTimeLogsData(
        internalFormId: Int,
        companyId: Int
    )

    @Query("DELETE FROM TimeSheetEmployeeTimeLogs WHERE companyId = :companyId")
    protected abstract suspend fun _deleteTimeSheetEmployeeTimeLogsData(companyId: Int)

    @Query("DELETE FROM TimesheetEmployeePositions WHERE internalFormId = :internalFormId AND companyId = :companyId")
    protected abstract suspend fun _deleteTimeSheetEmployeePositionsData(
        internalFormId: Int,
        companyId: Int
    )

    @Query("DELETE FROM TimesheetEmployeePositions WHERE companyId = :companyId")
    protected abstract suspend fun _deleteTimeSheetEmployeePositionsData(companyId: Int)

    @Query("DELETE FROM TimeSheetEmployees WHERE internalFormId = :internalFormId AND companyId = :companyId")
    protected abstract suspend fun _deleteTimeSheetEmployeesData(
        internalFormId: Int,
        companyId: Int
    )

    @Query("DELETE FROM TimeSheetEmployees WHERE companyId = :companyId")
    protected abstract suspend fun _deleteTimeSheetEmployeesData(companyId: Int)

    @Query("DELETE FROM TimeSheetFormData WHERE internalFormId = :internalFormId AND companyId = :companyId")
    protected abstract suspend fun _deleteTimeSheetFormData(internalFormId: Int, companyId: Int)

    @Query("DELETE FROM TimeSheetFormData WHERE companyId = :companyId")
    protected abstract suspend fun _deleteTimeSheetFormData(companyId: Int)
}
