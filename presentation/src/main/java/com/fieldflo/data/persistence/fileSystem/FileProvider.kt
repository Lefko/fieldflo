package com.fieldflo.data.persistence.fileSystem

import android.content.Context
import androidx.annotation.IntRange
import java.io.File
import java.util.*

/**
 * This class creates the files that the app uses
 */
class FileProvider(private val context: Context) : IFileProvider {

    companion object {
        private const val COMPANY_CERTS_DIRECTORY_NAME = "companyCerts"
        private const val PROJECT_CERTS_DIRECTORY_NAME = "projectCerts"
        private const val PROJECT_PERMITS_DIRECTORY_NAME = "projectPermits"
        private const val DAILY_FIELD_REPORT_DIRECTORY_NAME = "dailyFieldReport"
        private const val DAILY_DEMO_LOG_DIRECTORY_NAME = "dailyDemoLog"
        private const val SAVED_DAILY_FIELD_REPORT_DIRECTORY_NAME = "saved"
        private const val SAVED_DAILY_DEMO_LOG_DIRECTORY_NAME = "saved"
        private const val PIN_IMAGES_DIRECTORY_NAME = "pinImages"

        private const val DAILY_DEMO_LOG_SUPERVISOR_PIN_IMAGE_FILE_NAME = "supervisorPin.jpg"
    }

    override fun getCompanyDirs(companyId: Int): Array<File> {
        return arrayOf(
            getMainCompanyDir(companyId),
            getPinImagesCompanyDir(companyId)
        )
    }

    override fun getProjectPermitFile(companyId: Int, projectId: Int, fileName: String): File {
        return File(
            getProjectPermitsDir(companyId, projectId),
            projectId.toString() + "_" + fileName
        )
    }

    override fun getProjectCertsFile(companyId: Int, projectId: Int, fileName: String): File {
        return File(getProjectCertDir(companyId, projectId), fileName)
    }

    override fun getPinImagePhotoPinForFormFile(
        companyId: Int,
        projectId: Int,
        id: Int,
        formDate: Date,
        internalFormId: Int
    ): File {
        val dir = getPinImagePhotoPinForFormDir(companyId, formDate, projectId, internalFormId)
        if (!dir.exists()) dir.mkdirs()
        return File(dir, "$id.jpg")
    }

    override fun getDailyDemoSupervisorPinFile(
        companyId: Int,
        projectId: Int,
        formDate: Date,
        internalFormId: Int
    ): File {
        val dir = getPinImagePhotoPinForFormDir(companyId, formDate, projectId, internalFormId)
        if (!dir.exists()) dir.mkdirs()
        return File(dir, DAILY_DEMO_LOG_SUPERVISOR_PIN_IMAGE_FILE_NAME)
    }

    override fun getPinImagePhotoPinForFormDir(
        companyId: Int,
        formDate: Date,
        projectId: Int,
        internalFormId: Int
    ): File {
        val pinDirForDate = getPinImageDirProject(companyId, projectId, formDate)
        val dir = File(pinDirForDate, internalFormId.toString())

        if (!dir.exists()) dir.mkdirs()

        return dir
    }

    override fun getPinImagesCompanyDir(companyId: Int): File {
        val pinImagesDir = File(context.filesDir, PIN_IMAGES_DIRECTORY_NAME)
        val companyPinDir = File(pinImagesDir, companyId.toString())
        companyPinDir.mkdirs()
        return companyPinDir
    }

    override fun getProjectPermitsDir(companyId: Int, projectId: Int): File {
        val projectDir = getProjectFilesDir(companyId, projectId)
        val projectCertsDir = File(projectDir, PROJECT_PERMITS_DIRECTORY_NAME)
        if (!projectCertsDir.exists()) projectCertsDir.mkdirs()

        return projectCertsDir
    }

    override fun getDailyFieldReportDir(companyId: Int, projectId: Int): File {
        val projectDir = getProjectFilesDir(companyId, projectId)
        val dailyFieldReportDir = File(projectDir, DAILY_FIELD_REPORT_DIRECTORY_NAME)
        if (!dailyFieldReportDir.exists()) dailyFieldReportDir.mkdirs()

        return dailyFieldReportDir
    }

    override fun getProjectCertDir(companyId: Int, projectId: Int): File {
        val projectDir = getProjectFilesDir(companyId, projectId)
        val projectCertsDir = File(projectDir, PROJECT_CERTS_DIRECTORY_NAME)
        if (!projectCertsDir.exists()) projectCertsDir.mkdirs()

        return projectCertsDir
    }

    override fun getProjectFilesDir(companyId: Int, projectId: Int): File {
        val companyDir = getMainCompanyDir(companyId)
        val projectDir = File(companyDir, projectId.toString())

        if (!projectDir.exists()) projectDir.mkdirs()

        return projectDir
    }

    override fun getMainCompanyDir(companyId: Int): File {
        return File(context.filesDir, companyId.toString())
    }

    override fun getPinImageDirForDate(companyId: Int, date: Date): File {
        val companyPinDir = getPinImagesCompanyDir(companyId)

        val calendar = Calendar.getInstance()
        calendar.time = date
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)

        val dateTimeStamp = (calendar.time).time

        return File(companyPinDir, dateTimeStamp.toString())
    }

    override fun getPinImageDirProject(companyId: Int, projectId: Int, timeSheetDate: Date): File {
        return File(getPinImageDirForDate(companyId, timeSheetDate), projectId.toString())
    }

    override fun getDailyFieldReportSavedDir(
        companyId: Int,
        projectId: Int,
        internalFormId: Int
    ): File {
        val dailyFieldReportDir = getDailyFieldReportDir(companyId, projectId)
        val specificFieldReportDir = File(dailyFieldReportDir, internalFormId.toString())
        val savedFieldReportDir =
            File(specificFieldReportDir, SAVED_DAILY_FIELD_REPORT_DIRECTORY_NAME)
        if (!savedFieldReportDir.exists()) savedFieldReportDir.mkdirs()

        return savedFieldReportDir
    }

    override fun getDailyFieldReportPic(
        companyId: Int,
        projectId: Int,
        internalFormId: Int,
        @IntRange(from = 1, to = 6) picNumber: Int
    ): File {
        val dir = getDailyFieldReportSavedDir(companyId, projectId, internalFormId)
        return File(dir, IFileProvider.determineImageFile(picNumber))
    }

    override fun getDailyDemoFormPicFile(
        companyId: Int,
        projectId: Int,
        internalFormId: Int,
        fileName: String
    ): File {
        val dir = getDailyDemoLogSavedDir(companyId, projectId, internalFormId)
        return File(dir, "$fileName.jpg")
    }

    override fun getDailyDemoFormSavedImagesDir(
        companyId: Int,
        projectId: Int,
        internalFormId: Int
    ): File {
        return getDailyDemoLogSavedDir(companyId, projectId, internalFormId)
    }

    private fun getDailyDemoLogSavedDir(
        companyId: Int,
        projectId: Int,
        internalFormId: Int
    ): File {
        val dailyDemoLogDir = getDailyDemoLogDir(companyId, projectId)
        val specificDailyDemoLogDir = File(dailyDemoLogDir, internalFormId.toString())
        val savedDailyDemoLogDir =
            File(specificDailyDemoLogDir, SAVED_DAILY_DEMO_LOG_DIRECTORY_NAME)
        if (!savedDailyDemoLogDir.exists()) savedDailyDemoLogDir.mkdirs()

        return savedDailyDemoLogDir
    }

    private fun getDailyDemoLogDir(companyId: Int, projectId: Int): File {
        val projectDir = getProjectFilesDir(companyId, projectId)
        val dailyDemoDir = File(projectDir, DAILY_DEMO_LOG_DIRECTORY_NAME)
        if (!dailyDemoDir.exists()) dailyDemoDir.mkdirs()

        return dailyDemoDir
    }
}