package com.fieldflo.data.persistence.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.fieldflo.data.persistence.db.entities.Employee

@Dao
abstract class EmployeeDao2 {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addEmployee(employee: Employee)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addAllEmployees(employees: List<Employee>)

    @Query("SELECT * FROM Employees WHERE employeeId = :employeeId AND companyId = :companyId")
    abstract suspend fun getEmployeeById(employeeId: Int, companyId: Int): Employee?

    @Query("SELECT * FROM Employees WHERE companyId = :companyId AND employeeDeleted = 0 AND employeeActive = 1 ORDER BY employeeFirstName, employeeLastName")
    abstract suspend fun getAllEmployees(companyId: Int): List<Employee>

    @Query("DELETE FROM Employees WHERE companyId = :companyId")
    abstract suspend fun deleteAllEmployees(companyId: Int)

    @Query("DELETE FROM Employees WHERE employeeId = :employeeId AND companyId = :companyId")
    abstract suspend fun deleteEmployee(employeeId: Int, companyId: Int)

    @Query("UPDATE Employees SET employeeDeleted = 1 WHERE employeeId = :employeeId AND companyId = :companyId")
    abstract suspend fun setEmployeeDeleted(employeeId: Int, companyId: Int)
}
