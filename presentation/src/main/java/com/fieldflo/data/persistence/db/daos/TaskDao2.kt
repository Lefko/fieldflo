package com.fieldflo.data.persistence.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.fieldflo.data.persistence.db.entities.Task

@Dao
abstract class TaskDao2 {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addAllTasks(tasks: List<Task>)

    @Query("SELECT * FROM Tasks WHERE companyId = :companyId")
    abstract suspend fun getAllTasks(companyId: Int): List<Task>

    @Query("DELETE FROM Tasks WHERE companyId = :companyId")
    abstract suspend fun deleteAllTasks(companyId: Int): Int
}
