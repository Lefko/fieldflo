package com.fieldflo.data.persistence.db.entities

data class ProjectDesignerData(
    val designerFirmName: String,
    val designerContact: String,
    val designerAddress: String,
    val designerCity: String,
    val designerState: String,
    val designerZipCode: String,
    val designerPhoneNumber: String,
    val designerFaxNumber: String,
    val designerCell: String,
    val designerCertNumber: String
)