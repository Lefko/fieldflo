package com.fieldflo.data.persistence.db.daos

import androidx.room.*
import com.fieldflo.data.persistence.db.entities.DailyFieldReportFormData

@Dao
abstract class DailyFieldReportDao2 {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addDailyFieldReportData(dailyFieldReport: DailyFieldReportFormData): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun updateDailyFieldReportDataSuspended(dailyFieldReport: DailyFieldReportFormData): Int

    @Query("DELETE FROM DailyFieldReportFormData WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun deleteDailyFieldReport(internalFormId: Int, companyId: Int): Int

    @Query("SELECT (workCompletedToday IS NOT NULL AND workCompletedToday <> '') FROM DailyFieldReportFormData WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun isDailyFieldReportClosable(internalFormId: Int, companyId: Int): Boolean?

    @Query("SELECT * FROM DailyFieldReportFormData WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun getDailyFieldReportDataByInternalFormId(
        internalFormId: Int,
        companyId: Int
    ): DailyFieldReportFormData?

    @Query("DELETE FROM DailyFieldReportFormData WHERE companyId = :companyId")
    abstract suspend fun deleteDailyFieldReport(companyId: Int): Int
}
