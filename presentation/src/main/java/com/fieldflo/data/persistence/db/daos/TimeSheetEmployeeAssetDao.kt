package com.fieldflo.data.persistence.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.fieldflo.data.persistence.db.entities.TimeSheetEmployeeAsset

@Dao
abstract class TimeSheetEmployeeAssetDao {

    @Insert
    abstract suspend fun addAsset(newAsset: TimeSheetEmployeeAsset)

    @Update
    abstract suspend fun editAsset(editedAsset: TimeSheetEmployeeAsset)

    @Query("DELETE FROM TimeSheetEmployeeAssets WHERE timeSheetEmployeeAssetId = :timeSheetEmployeeAssetId AND companyId = :companyId")
    abstract fun deleteTimeSheetEmployeeAsset(timeSheetEmployeeAssetId: Int, companyId: Int)
}
