package com.fieldflo.data.persistence.db.entities

import java.util.*

data class ProjectHealthData(
    val projectId: Int,
    val projectName: String,
    val projectNumber: String,
    val clientFullName: String,
    val projectClosed: Boolean,
    val projectSiteAddress: String,
    val projectManagerFullName: String,
    val projectCrewSize: Int,
    val projectStartDate: Date,
    val projectEndDate: Date,
    val projectNameOfSiteContact: String,
    val projectSitePhone: String,
    val projectCity: String,
    val projectState: String,
    val projectZipCode: String,
    val warehousePending: Int
)