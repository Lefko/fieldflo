package com.fieldflo.data.persistence.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.fieldflo.data.persistence.db.entities.CompanyCertificate

@Dao
abstract class CompanyCertificateDao2 {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addAllCompanyCertificates(certificates: List<CompanyCertificate>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addCompanyCertificate(certificate: CompanyCertificate)

    @Query("DELETE FROM CompanyCertificates WHERE certificateId = :certId AND companyId = :companyId")
    abstract suspend fun deleteCompanyCert(certId: Int, companyId: Int)

    @Query("DELETE FROM CompanyCertificates WHERE companyId = :companyId")
    abstract suspend fun deleteCompanyCerts(companyId: Int)

    @Query("SELECT * FROM CompanyCertificates WHERE employeeId IN (:employeeIds) AND isDeleted = 0 AND companyId = :companyId")
    abstract suspend fun getCompanyCertificatesByEmployeeIds(
        employeeIds: List<Int>,
        companyId: Int
    ): List<CompanyCertificate>
}