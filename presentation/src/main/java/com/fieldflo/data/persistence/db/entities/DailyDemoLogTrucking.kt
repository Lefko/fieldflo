package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DailyDemoLogTrucking(
    @PrimaryKey(autoGenerate = true)
    val dailyDemoLogTruckingId: Int = 0,
    val formDataId: Int = 0,
    val internalFormId: Int = 0,
    val projectId: Int = 0,
    val companyId: Int = 0,
    val truckingID: Int = 0,
    val truckingCustom: String = "",
    val material: String = "",
    val loads: String = "",
    val truckno: String = "",
    val hours: String = "",
    val hourlyRate: String = "",
    val total: String = ""
)
