package com.fieldflo.data.persistence.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.fieldflo.data.persistence.db.entities.ProjectPermit

@Dao
interface ProjectPermitDao2 {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addProjectPermitList(projectPermitList: List<ProjectPermit>)

    @Query("DELETE FROM ProjectPermits WHERE projectId = :projectId AND companyId = :companyId")
    fun deleteProjectPermits(projectId: Int, companyId: Int)

    @Query("DELETE FROM ProjectPermits WHERE companyId = :companyId")
    fun deleteProjectPermits(companyId: Int)

    @Query("SELECT * FROM ProjectPermits WHERE projectId = :projectId AND companyId = :companyId")
    suspend fun getProjectPermits(projectId: Int, companyId: Int): List<ProjectPermit>
}
