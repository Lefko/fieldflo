package com.fieldflo.data.persistence.db.daos

import androidx.room.*
import com.fieldflo.data.persistence.db.entities.TimeSheetEmployee
import com.fieldflo.data.persistence.db.relationWrappers.TimeSheetEmployeeWrapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import java.util.*

@Dao
abstract class TimeSheetEmployeeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addTimeSheetEmployee(timeSheetEmployee: TimeSheetEmployee): Long

    suspend fun getTimeSheetEmployeeById(
        timeSheetEmployeeId: Int,
        companyId: Int
    ): TimeSheetEmployee? {
        return _getTimeSheetEmployeeByTimeSheetEmployeeId(timeSheetEmployeeId, companyId)?.unwrap()
    }

    fun getTimeSheetEmployeeByIdFlow(
        timeSheetEmployeeId: Int,
        companyId: Int
    ): Flow<TimeSheetEmployee> {
        return _getTimeSheetEmployeeByTimeSheetEmployeeIdFlow(
            timeSheetEmployeeId,
            companyId
        ).mapNotNull { it?.unwrap() }
    }

    @Query("SELECT * FROM TimeSheetEmployees WHERE timeSheetEmployeeId IN (:timeSheetEmployeeIds) AND companyId = :companyId")
    abstract suspend fun getTimeSheetEmployees(
        timeSheetEmployeeIds: List<Int>,
        companyId: Int
    ): List<TimeSheetEmployee>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addTimeSheetEmployees(timeSheetEmployees: List<TimeSheetEmployee>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun updateTimeSheetEmployees(timeSheetEmployees: List<TimeSheetEmployee>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun updateTimeSheetEmployee(timeSheetEmployee: TimeSheetEmployee)

    @Query(
        "SELECT (Count(timeSheetEmployeeId) > 0) FROM TimeSheetEmployees WHERE employeeId = :employeeId AND isDeleted = 0 AND employeeStatus = 'CLOCKED_IN' AND companyId = :companyId"
    )
    abstract suspend fun isEmployeeClockedIn(employeeId: Int, companyId: Int): Boolean

    @Query("SELECT Count(timeSheetEmployeeId) > 0 FROM TimeSheetEmployees WHERE projectId = :projectId AND employeeId = :employeeId AND companyId = :companyId AND isDeleted = 0")
    abstract suspend fun isEmployeeOnProjectTimeSheet(
        projectId: Int,
        employeeId: Int,
        companyId: Int
    ): Boolean

    @Query("UPDATE TimeSheetEmployees SET isDeleted = 1 WHERE timeSheetEmployeeId = :timesheetEmployeeId AND companyId = :companyId")
    abstract suspend fun removeEmployeeFromTimeSheet(timesheetEmployeeId: Int, companyId: Int)

    fun getTimeSheetEmployeesByInternalFormIdFlow(
        internalFormId: Int,
        companyId: Int
    ): Flow<List<TimeSheetEmployee>> {
        return _getTimeSheetEmployeesByInternalFormIdFlow(internalFormId, companyId)
            .map { it.map { it.unwrap() } }
    }

    suspend fun getTimeSheetEmployeesByInternalFormId(
        internalFormId: Int,
        companyId: Int
    ): List<TimeSheetEmployee> {
        return _getTimeSheetEmployeesByInternalFormId(internalFormId, companyId).map { it.unwrap() }
    }

    suspend fun getTimeSheetEmployeesByProjectId(
        projectId: Int,
        companyId: Int
    ): List<TimeSheetEmployee> {
        return _getTimeSheetEmployeesByProjectId(projectId, companyId).map { it.unwrap() }
    }

    @Query("UPDATE TimeSheetEmployees SET employeeStatus = 'CLOCKED_IN', clockIn = :clockInDate WHERE timeSheetEmployeeId IN (:timesheetEmployeeIds) AND companyId = :companyId")
    abstract suspend fun startTrackingEmployees(
        timesheetEmployeeIds: List<Int>,
        companyId: Int,
        clockInDate: Date
    )

    @Query("UPDATE TimeSheetEmployees SET employeeStatus = 'VERIFIED', federalBreak = :tookBreak, federalBreakVerifyDate = :verificationDate, federalBreakVerifyEmployeeId = :verifiedById, injuredVerifiedById = :verifiedById, injuredVerifiedByDate = :verificationDate, timeVerifiedById = :verifiedById, timeVerifiedByDate = :verificationDate WHERE timeSheetEmployeeId IN (:timesheetEmployeeIds) AND companyId = :companyId")
    abstract suspend fun verifyEmployees(
        timesheetEmployeeIds: List<Int>,
        verifiedById: Int,
        tookBreak: Boolean,
        companyId: Int,
        verificationDate: Date = Date()
    )

    @Query("UPDATE TimeSheetEmployees SET employeeStatus = 'VERIFIED_WITH_INJURY', federalBreak = :tookBreak, federalBreakVerifyDate = :verificationDate, federalBreakVerifyEmployeeId = :verifiedById, injuredVerifiedById = :verifiedById, injuredVerifiedByDate = :verificationDate, timeVerifiedById = :verifiedById, timeVerifiedByDate = :verificationDate WHERE timeSheetEmployeeId IN (:timesheetEmployeeIds) AND companyId = :companyId")
    abstract suspend fun verifyInjuredEmployees(
        timesheetEmployeeIds: List<Int>,
        verifiedById: Int,
        tookBreak: Boolean,
        companyId: Int,
        verificationDate: Date = Date()
    )

    @Query("UPDATE TimeSheetEmployees SET per_diem_perDiemType = :perDiemType, per_diem_dailyAmount = :dailyAmount, per_diem_hourlyAmount = :hourlyAmount, per_diem_cashGivenOnSite = :cashGivenOnSite, per_diem_totalWorkTime = :totalWorkTime, per_diem_notes = :notes, per_diem_totalEarned = :totalEarned, per_diem_totalOwed = :totalOwed WHERE timeSheetEmployeeId = :timeSheetEmployeeId AND companyId = :companyId")
    abstract suspend fun updatePerDiemData(
        perDiemType: String,
        dailyAmount: Double,
        hourlyAmount: Double,
        cashGivenOnSite: Double,
        totalWorkTime: Int,
        notes: String,
        totalEarned: Double,
        totalOwed: Double,
        timeSheetEmployeeId: Int,
        companyId: Int
    )

    @Transaction
    @Query("SELECT * FROM TimeSheetEmployees WHERE timeSheetEmployeeId = :timeSheetEmployeeId AND companyId = :companyId AND isDeleted = 0")
    protected abstract suspend fun _getTimeSheetEmployeeByTimeSheetEmployeeId(
        timeSheetEmployeeId: Int,
        companyId: Int
    ): TimeSheetEmployeeWrapper?

    @Transaction
    @Query("SELECT * FROM TimeSheetEmployees WHERE timeSheetEmployeeId = :timeSheetEmployeeId AND companyId = :companyId AND isDeleted = 0")
    protected abstract fun _getTimeSheetEmployeeByTimeSheetEmployeeIdFlow(
        timeSheetEmployeeId: Int,
        companyId: Int
    ): Flow<TimeSheetEmployeeWrapper?>

    @Transaction
    @Query("SELECT * FROM TimeSheetEmployees WHERE internalFormId = :internalFormId AND companyId = :companyId AND isDeleted = 0")
    protected abstract suspend fun _getTimeSheetEmployeesByInternalFormId(
        internalFormId: Int,
        companyId: Int
    ): List<TimeSheetEmployeeWrapper>

    @Transaction
    @Query("SELECT * FROM TimeSheetEmployees WHERE projectId = :projectId AND companyId = :companyId AND isDeleted = 0")
    protected abstract suspend fun _getTimeSheetEmployeesByProjectId(
        projectId: Int,
        companyId: Int
    ): List<TimeSheetEmployeeWrapper>

    @Transaction
    @Query("SELECT * FROM TimeSheetEmployees WHERE internalFormId = :internalFormId AND companyId = :companyId AND isDeleted = 0")
    protected abstract fun _getTimeSheetEmployeesByInternalFormIdFlow(
        internalFormId: Int,
        companyId: Int
    ): Flow<List<TimeSheetEmployeeWrapper>>
}