package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "ProjectForms", indices = [Index("companyId"), Index("projectId")])
data class ProjectForm(
    @PrimaryKey(autoGenerate = true)
    var internalFormId: Int = 0,
    val companyId: Int = 0,
    val projectId: Int = 0,
    val formTypeId: Int = 0,
    val formName: String = "",
    val formDate: Date = Date(),
    val formActive: Boolean = true,
    val formClosed: Boolean = false,
    val closeDate: Date = Date(0),
    val formDeleted: Boolean = false,
    val deleteDate: Date = Date(0),
    val hasSyncError: Boolean = false,
    val isCloseSynced: Boolean = false
)
