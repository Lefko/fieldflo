package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import java.util.*

@Entity(
    tableName = "TimeSheetEmployeeTimeLogs",
    foreignKeys = [ForeignKey(
        entity = TimeSheetEmployee::class,
        parentColumns = ["timeSheetEmployeeId"],
        childColumns = ["timeSheetEmployeeId"],
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    )],
    indices = [Index("timeSheetEmployeeId")]
)
data class TimeSheetEmployeeTimeLog(

    @PrimaryKey(autoGenerate = true)
    val timeSheetEmployeeTimeLogId: Int = 0,

    var companyId: Int = 0,
    var projectId: Int = 0,
    var internalFormId: Int = 0,
    var timeSheetFormDataId: Int = 0,
    var timeSheetEmployeeId: Int = 0,

    val project2formId: Int = 0,
    val employeeAutoId: Int = 0,
    val employeeId: Int = 0,
    val employeeName: String = "",
    val data: String = "",
    val date: Date = Date(),
    val type: String = ""
)