package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "DailyLogs")
data class DailyLogFormData(

    @PrimaryKey(autoGenerate = true)
    val formDataId: Int = 0,
    val companyId: Int = 0,
    val projectId: Int = 0,
    val internalFormId: Int = 0,
    val formDate: Date = Date(),
    val shiftStart: String = "",
    val shiftEnd: String = "",
    val supervisorId: Int = 0,
    val supervisorFullName: String = "",
    val supervisorVerify: Boolean = false,
    val supervisorVerifyEmployeeId: Int = 0,
    val supervisorVerifyDate: Date = Date(0),
    val dailyLog: String = "",
    val visitorsToSite: String = "",
    val statusAtQuittingTime: String = "",
    val inspectionsMadeOrTestsPerformed: String = "",
    val dailyBagCount: String = "",
    val projectToDateBagCount: String = "",
    val containerNo: String = "",
    val bagsInContainerToDate: String = "",
    val unusualConditionsOrProblemsAndActionTaken: String = "",
    val projectManagerFullName: String
)
