package com.fieldflo.data.persistence.db.entities

import java.util.*

data class BasicProjectData(
    val projectId: Int = 0,
    val projectNumber: String = "",
    val projectName: String = "",
    val clientFullName: String = "",
    val projectCrewSize: Int = 0,
    val supervisorFullName: String = "",
    val projectManagerFullName: String = "",
    val projectStartDate: Date = Date(),
    val projectEndDate: Date = Date(),
    val projectClosed: Boolean = false,
    val usePhases: Boolean,
    val phaseCode: String,
    val phasesType: String
)
