package com.fieldflo.data.persistence.db.entities

data class ProjectLienInfo(
    val lienLender: String,
    val lienLenderAddress: String,
    val lienCity: String,
    val lienState: String,
    val lienZipCode: String,
    val lienLenderValue: Double,
    val lienMonth: String,
    val lienDay: String,
    val lienYear: String
)