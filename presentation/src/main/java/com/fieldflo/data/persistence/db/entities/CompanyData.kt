package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity
import androidx.room.Index

@Entity(tableName = "BasicCompanies", primaryKeys = ["token", "securityKey"], indices = [Index("companyId")])
data class CompanyData(
    val companyId: Int = 0,
    val companyName: String,
    val url: String,
    val token: String,
    val securityKey: String,
    val initialLoadComplete: Boolean = false
)
