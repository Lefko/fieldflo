package com.fieldflo.data.persistence.fileSystem

import androidx.annotation.IntRange
import java.io.File
import java.util.*

interface IFileProvider {
    fun getCompanyDirs(companyId: Int): Array<File>
    fun getProjectPermitFile(companyId: Int, projectId: Int, fileName: String): File
    fun getProjectCertsFile(companyId: Int, projectId: Int, fileName: String): File
    fun getPinImagePhotoPinForFormFile(
        companyId: Int,
        projectId: Int,
        id: Int,
        formDate: Date,
        internalFormId: Int
    ): File

    fun getDailyDemoSupervisorPinFile(
        companyId: Int,
        projectId: Int,
        formDate: Date,
        internalFormId: Int
    ): File

    fun getPinImagePhotoPinForFormDir(
        companyId: Int,
        formDate: Date,
        projectId: Int,
        internalFormId: Int
    ): File

    fun getPinImagesCompanyDir(companyId: Int): File
    fun getProjectPermitsDir(companyId: Int, projectId: Int): File
    fun getDailyFieldReportDir(companyId: Int, projectId: Int): File
    fun getProjectCertDir(companyId: Int, projectId: Int): File
    fun getProjectFilesDir(companyId: Int, projectId: Int): File
    fun getMainCompanyDir(companyId: Int): File
    fun getPinImageDirForDate(companyId: Int, date: Date): File
    fun getPinImageDirProject(companyId: Int, projectId: Int, timeSheetDate: Date): File
    fun getDailyFieldReportSavedDir(companyId: Int, projectId: Int, internalFormId: Int): File
    fun getDailyFieldReportPic(
        companyId: Int,
        projectId: Int,
        internalFormId: Int,
        @IntRange(from = 1, to = 6) picNumber: Int
    ): File


    fun getDailyDemoFormPicFile(
        companyId: Int,
        projectId: Int,
        internalFormId: Int,
        fileName: String
    ): File

    fun getDailyDemoFormSavedImagesDir(
        companyId: Int,
        projectId: Int,
        internalFormId: Int
    ): File

    companion object {
        private const val IMAGE1 = "image1.jpg"
        private const val IMAGE2 = "image2.jpg"
        private const val IMAGE3 = "image3.jpg"
        private const val IMAGE4 = "image4.jpg"
        private const val IMAGE5 = "image5.jpg"
        private const val IMAGE6 = "image6.jpg"

        fun determineImageNumber(imageFile: File): Int {
            return when (imageFile.name) {
                IMAGE1 -> 1
                IMAGE2 -> 2
                IMAGE3 -> 3
                IMAGE4 -> 4
                IMAGE5 -> 5
                IMAGE6 -> 6
                else -> throw IllegalArgumentException("Cant extract image number from filename: ${imageFile.name}")
            }
        }

        fun determineImageFile(picNumber: Int): String {
            return when (picNumber) {
                1 -> IMAGE1
                2 -> IMAGE2
                3 -> IMAGE3
                4 -> IMAGE4
                5 -> IMAGE5
                6 -> IMAGE6
                else -> throw IllegalArgumentException("picNumber must be between 1 and 6")
            }
        }
    }
}