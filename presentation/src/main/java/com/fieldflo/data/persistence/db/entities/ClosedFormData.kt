package com.fieldflo.data.persistence.db.entities

import java.util.*

data class ClosedFormData(
    val internalFormId: Int = 0,
    val projectId: Int = 0,
    val formName: String = "",
    val closeDate: Date = Date(),
    val isCloseSynced: Boolean = false,
    val hasSyncError: Boolean = false
)
