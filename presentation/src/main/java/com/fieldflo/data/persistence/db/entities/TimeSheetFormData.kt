package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "TimeSheetFormData")
data class TimeSheetFormData @Ignore constructor(

    @PrimaryKey(autoGenerate = true)
    val formDataId: Int = 0,

    var companyId: Int = 0,
    var internalFormId: Int = 0,
    var projectId: Int = 0,
    val formDate: Date = Date(),
    val supervisorId: Int = 0,
    val notes: String = "",

    @Ignore
    var employees: List<TimeSheetEmployee> = listOf()
) {
    constructor(
        formDataId: Int = 0,
        companyId: Int = 0,
        internalFormId: Int = 0,
        projectId: Int = 0,
        formDate: Date = Date(),
        supervisorId: Int = 0,
        notes: String = ""
    ) : this(
        formDataId,
        companyId,
        internalFormId,
        projectId,
        formDate,
        supervisorId,
        notes,
        listOf()
    )
}
