package com.fieldflo.data.persistence.db.entities

import java.util.*

data class TimeSheetEmployeeTimeStamp(
    val employeeId: Int = 0,
    val employeeName: String = "",
    val timestamp: Date = Date(0),
    val type: String = "",
    val originalEmployeeId: Int = 0,
    val insertUserId: Int = 0,
    val insertUserName: String = ""
)
