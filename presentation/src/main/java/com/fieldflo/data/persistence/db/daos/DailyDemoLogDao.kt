package com.fieldflo.data.persistence.db.daos

import androidx.room.*
import com.fieldflo.data.persistence.db.entities.*

@Dao
abstract class DailyDemoLogDao {

    @Query("SELECT (productionNarrative IS NOT NULL AND productionNarrative <> '' AND (supervisorId = 0 OR supervisorVerified = 1)) FROM DailyDemoLogs WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun isDailyDemoLogClosable(internalFormId: Int, companyId: Int): Boolean?

    @Query("SELECT * FROM DailyDemoLogs WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract fun getDailyDemoLogByInternalFormId(
        internalFormId: Int,
        companyId: Int
    ): DailyDemoLogFormData?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addDailyDemoLog(dailyLog: DailyDemoLogFormData): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun updateDailyDemoLog(dailyLog: DailyDemoLogFormData): Int

    @Query("DELETE FROM DailyDemoLogs WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun deleteDailyDemoLog(internalFormId: Int, companyId: Int): Int

    @Query("DELETE FROM DailyDemoLogs WHERE companyId = :companyId")
    abstract suspend fun deleteAllDailyDemoLogs(companyId: Int): Int

    suspend fun saveDemoLogEmployees(
        internalFormId: Int,
        companyId: Int,
        employees: List<DailyDemoLogEmployee>
    ) {
        _deleteAllDailyDemoLogEmployees(internalFormId, companyId)
        _insertDailyDemoLogEmployees(employees)
    }

    suspend fun saveDemoLogSubContractors(
        internalFormId: Int,
        companyId: Int,
        subContractors: List<DailyDemoLogSubContractor>
    ) {
        _deleteAllDailyDemoLogSubContractors(internalFormId, companyId)
        _insertDailyDemoLogSubContractors(subContractors)
    }

    suspend fun saveDemoLogeEquipment(
        internalFormId: Int,
        companyId: Int,
        equipment: List<DailyDemoLogEquipment>
    ) {
        _deleteAllDailyDemoLogEquipment(internalFormId, companyId)
        _insertDailyDemoLogEquipment(equipment)
    }

    suspend fun saveDemoLogeMaterials(
        internalFormId: Int,
        companyId: Int,
        materials: List<DailyDemoLogMaterials>
    ) {
        _deleteAllDailyDemoLogMaterials(internalFormId, companyId)
        _insertDailyDemoLogMaterials(materials)
    }

    suspend fun saveDemoLogeTrucking(
        internalFormId: Int,
        companyId: Int,
        trucking: List<DailyDemoLogTrucking>
    ) {
        _deleteAllDailyDemoLogTrucking(internalFormId, companyId)
        _insertDailyDemoLogTrucking(trucking)
    }

    @Query("DELETE FROM DailyDemoLogTrucking WHERE internalFormId = :internalFormId AND companyId = :companyId")
    protected abstract suspend fun _deleteAllDailyDemoLogTrucking(
        internalFormId: Int,
        companyId: Int
    )

    @Insert
    protected abstract suspend fun _insertDailyDemoLogTrucking(trucking: List<DailyDemoLogTrucking>)

    @Insert
    protected abstract suspend fun _insertDailyDemoLogMaterials(materials: List<DailyDemoLogMaterials>)

    @Query("DELETE FROM DailyDemoLogMaterials WHERE internalFormId = :internalFormId AND companyId = :companyId")
    protected abstract suspend fun _deleteAllDailyDemoLogMaterials(
        internalFormId: Int,
        companyId: Int
    )

    @Insert
    protected abstract suspend fun _insertDailyDemoLogEquipment(equipment: List<DailyDemoLogEquipment>)

    @Query("DELETE FROM DailyDemoLogEquipment WHERE internalFormId = :internalFormId AND companyId = :companyId")
    protected abstract suspend fun _deleteAllDailyDemoLogEquipment(
        internalFormId: Int,
        companyId: Int
    )

    @Query("DELETE FROM DailyDemoLogSubContractor WHERE internalFormId = :internalFormId AND companyId = :companyId")
    protected abstract suspend fun _deleteAllDailyDemoLogSubContractors(
        internalFormId: Int,
        companyId: Int
    )

    @Insert
    protected abstract suspend fun _insertDailyDemoLogSubContractors(subContractors: List<DailyDemoLogSubContractor>)

    @Query("DELETE FROM DailyDemoLogEmployee WHERE internalFormId = :internalFormId AND companyId = :companyId")
    protected abstract suspend fun _deleteAllDailyDemoLogEmployees(
        internalFormId: Int,
        companyId: Int
    )

    @Insert
    protected abstract suspend fun _insertDailyDemoLogEmployees(employees: List<DailyDemoLogEmployee>)

    @Query("SELECT * FROM DailyDemoLogEmployee WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun getDailyDemoLogEmployees(
        internalFormId: Int,
        companyId: Int
    ): List<DailyDemoLogEmployee>

    @Query("SELECT * FROM DailyDemoLogSubContractor WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun getDailyDemoLogSubContractors(
        internalFormId: Int,
        companyId: Int
    ): List<DailyDemoLogSubContractor>

    @Query("SELECT * FROM DailyDemoLogEquipment WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun getDailyDemoLogEquipment(
        internalFormId: Int,
        companyId: Int
    ): List<DailyDemoLogEquipment>

    @Query("SELECT * FROM DailyDemoLogMaterials WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun getDailyDemoLogMaterials(
        internalFormId: Int,
        companyId: Int
    ): List<DailyDemoLogMaterials>

    @Query("SELECT * FROM DailyDemoLogTrucking WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun getDailyDemoLogTrucking(
        internalFormId: Int,
        companyId: Int
    ): List<DailyDemoLogTrucking>

    open suspend fun deleteAllDailyDemoFormData(internalFormId: Int, companyId: Int) {
        _deleteAllDailyDemoLogEmployees(internalFormId, companyId)
        _deleteAllDailyDemoLogSubContractors(internalFormId, companyId)
        _deleteAllDailyDemoLogEquipment(internalFormId, companyId)
        _deleteAllDailyDemoLogMaterials(internalFormId, companyId)
        _deleteAllDailyDemoLogTrucking(internalFormId, companyId)
        _deleteAllDailyDemoLogFormData(internalFormId, companyId)
    }

    @Query("DELETE FROM DailyDemoLogs WHERE internalFormId = :internalFormId AND companyId = :companyId")
    protected abstract suspend fun _deleteAllDailyDemoLogFormData(
        internalFormId: Int,
        companyId: Int
    )
}