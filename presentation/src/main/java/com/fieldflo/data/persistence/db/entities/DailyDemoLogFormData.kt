package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "DailyDemoLogs")
data class DailyDemoLogFormData @Ignore constructor(

    @PrimaryKey(autoGenerate = true)
    val formDataId: Int = 0,
    val internalFormId: Int = 0,
    val projectId: Int = 0,
    val companyId: Int = 0,
    val insertDate: Date = Date(),
    val supervisorId: Int = 0,
    val supervisorVerified: Boolean = false,
    val supervisorVerifiedDate: Date = Date(0),
    val supervisorVerifiedByEmployeeId: Int = 0,
    val pinOutLocation: String = "",
    val safetyTopic: String = "",
    val productionNarrative: String = "",
    val safetyMeetingConductedByEmployeeID: Int = 0,
    val weatherAM: String = "",
    val weatherPM: String = "",
    val reportableIncident: Boolean = false,
    val incident: String = "",
    val incidentDescription: String = "",
    val stopWorkPerformed: Boolean = false,
    val stopWorkReason: String = "",
    val stopWorkBeyondControlDescription: String = "",
    val stopWorkOutOfScopeDescription: String = "",
    val stopWorkBackChargeDescription: String = "",
    val stopWorkVisitorsDescription: String = "",
    val stopWorkIssuesDescription: String = "",
    val comments: String = "",

    @Ignore
    var dailyDemoLogEmployees: List<DailyDemoLogEmployee> = emptyList(),

    @Ignore
    var dailyDemoLogSubContractors: List<DailyDemoLogSubContractor> = emptyList(),

    @Ignore
    var dailyDemoLogEquipment: List<DailyDemoLogEquipment> = emptyList(),

    @Ignore
    var dailyDemoLogMaterials: List<DailyDemoLogMaterials> = emptyList(),

    @Ignore
    var dailyDemoLogTrucking: List<DailyDemoLogTrucking> = emptyList()
) {
    constructor(
        formDataId: Int = 0,
        internalFormId: Int = 0,
        projectId: Int = 0,
        companyId: Int = 0,
        insertDate: Date = Date(),
        supervisorId: Int = 0,
        supervisorVerified: Boolean = false,
        supervisorVerifiedDate: Date = Date(),
        supervisorVerifiedByEmployeeId: Int = 0,
        pinOutLocation: String = "",
        safetyTopic: String = "",
        productionNarrative: String = "",
        safetyMeetingConductedByEmployeeID: Int = 0,
        weatherAM: String = "",
        weatherPM: String = "",
        reportableIncident: Boolean = false,
        incident: String = "",
        incidentDescription: String = "",
        stopWorkPerformed: Boolean = false,
        stopWorkReason: String = "",
        stopWorkBeyondControlDescription: String = "",
        stopWorkOutOfScopeDescription: String = "",
        stopWorkBackChargeDescription: String = "",
        stopWorkVisitorsDescription: String = "",
        stopWorkIssuesDescription: String = "",
        comments: String = "",
    ) : this(
        formDataId = formDataId,
        internalFormId = internalFormId,
        projectId = projectId,
        companyId = companyId,
        insertDate = insertDate,
        supervisorId = supervisorId,
        supervisorVerified = supervisorVerified,
        supervisorVerifiedDate = supervisorVerifiedDate,
        supervisorVerifiedByEmployeeId = supervisorVerifiedByEmployeeId,
        pinOutLocation = pinOutLocation,
        safetyTopic = safetyTopic,
        productionNarrative = productionNarrative,
        safetyMeetingConductedByEmployeeID = safetyMeetingConductedByEmployeeID,
        weatherAM = weatherAM,
        weatherPM = weatherPM,
        reportableIncident = reportableIncident,
        incident = incident,
        incidentDescription = incidentDescription,
        stopWorkPerformed = stopWorkPerformed,
        stopWorkReason = stopWorkReason,
        stopWorkBeyondControlDescription = stopWorkBeyondControlDescription,
        stopWorkOutOfScopeDescription = stopWorkOutOfScopeDescription,
        stopWorkBackChargeDescription = stopWorkBackChargeDescription,
        stopWorkVisitorsDescription = stopWorkVisitorsDescription,
        stopWorkIssuesDescription = stopWorkIssuesDescription,
        comments = comments,
        dailyDemoLogEmployees = emptyList(),
        dailyDemoLogSubContractors = emptyList(),
        dailyDemoLogEquipment = emptyList(),
        dailyDemoLogMaterials = emptyList(),
        dailyDemoLogTrucking = emptyList()
    )
}
