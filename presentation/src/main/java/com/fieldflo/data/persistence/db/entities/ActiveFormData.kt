package com.fieldflo.data.persistence.db.entities

import java.util.*

data class ActiveFormData(
    val internalFormId: Int = 0,
    val projectId: Int = 0,
    val formTypeId: Int = 0,
    val formName: String = "",
    val formActive: Boolean = true,
    val formDate: Date = Date()
)
