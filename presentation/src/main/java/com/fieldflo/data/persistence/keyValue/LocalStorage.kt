package com.fieldflo.data.persistence.keyValue

import com.orhanobut.hawk.Hawk
import java.util.*

class LocalStorage : ILocalStorage {

    companion object {
        private const val DEVICE_ID_KEY = "device_id"
        private const val API_TOKEN_KEY = "api_token"
        private const val SECURITY_TOKEN_KEY = "security_token"
        private const val USER_TOKEN_KEY = "user_token"
        private const val FCM_TOKEN = "fcm_token"
        private const val BASE_URL_KEY = "base_url"
        private const val CURRENT_COMPANY_NAME = "company_name"
        private const val CURRENT_COMPANY_ID = "company_id"
        private const val USER_EMAIL_KEY = "user_email"
        private const val USER_PASSWORD_KEY = "user_password"
        private const val CURRENT_EMPLOYEE_ID = "current_employee_id"
        private const val DOWNLOAD_ALL_COMPANY_CERTS = "download_all_company_certs"
        private const val USES_PHOTO_PIN_KEY = "uses_photo_pin"
        private const val DELETES_PHOTO_PIN_KEY = "deletes_photo_pin"
        private const val EMPLOYEE_ID_KEY = "employee_id"
        private const val EMPLOYEE_FIRST_NAME_KEY = "employee_first_name"
        private const val EMPLOYEE_LAST_NAME_KEY = "employee_last_name"
        private const val EMPLOYEE_PIN_NUMBER_KEY = "employee_pin_number"
        private const val EMPLOYEE_POSITION_ID = "employee_position_id"
        private const val EMPLOYEE_USER_CAN_VIEW_PROJECTS_KEY = "employee_role_view_projects"
        private const val EMPLOYEE_USER_CAN_ADD_EDIT_OPEN_FORMS_KEY = "employee_role_add_edit_open_forms"
        private const val EMPLOYEE_USER_CAN_VIEW_CERTS = "employee_role_view_certs"
        private const val EMPLOYEE_USER_CAN_EDIT_CERTS = "employee_role_edit_certs"
        private const val EMPLOYEE_USER_CAN_EDIT_TIME_SHEET_TIME = "employee_role_edit_time_sheet_time"
        private const val EMPLOYEE_USER_CAN_VALIDATE_TIME_SHEET_WITH_PIN =
            "employee_role_validate_time_sheet_with_pin"
        private const val V2_MIGRATION_COMPLETE = "v2_migration_complete"
    }

    override fun getDeviceId(): String {
        val savedDeviceId = Hawk.get(DEVICE_ID_KEY, "")
        if (savedDeviceId.isEmpty()) {
            val newId = UUID.randomUUID().toString()
            Hawk.put(DEVICE_ID_KEY, newId)
            return newId
        }

        return savedDeviceId
    }

    override fun clear() {
        Hawk.delete(DEVICE_ID_KEY)
        Hawk.delete(API_TOKEN_KEY)
        Hawk.delete(SECURITY_TOKEN_KEY)
        Hawk.delete(USER_TOKEN_KEY)
        Hawk.delete(FCM_TOKEN)
        Hawk.delete(BASE_URL_KEY)
        Hawk.delete(CURRENT_COMPANY_NAME)
        Hawk.delete(CURRENT_COMPANY_ID)
        Hawk.delete(USER_EMAIL_KEY)
        Hawk.delete(USER_PASSWORD_KEY)
        Hawk.delete(CURRENT_EMPLOYEE_ID)
        Hawk.delete(DOWNLOAD_ALL_COMPANY_CERTS)
        Hawk.delete(USES_PHOTO_PIN_KEY)
        Hawk.delete(DELETES_PHOTO_PIN_KEY)
        Hawk.delete(EMPLOYEE_ID_KEY)
        Hawk.delete(EMPLOYEE_FIRST_NAME_KEY)
        Hawk.delete(EMPLOYEE_LAST_NAME_KEY)
        Hawk.delete(EMPLOYEE_PIN_NUMBER_KEY)
        Hawk.delete(EMPLOYEE_POSITION_ID)
        Hawk.delete(EMPLOYEE_USER_CAN_VIEW_PROJECTS_KEY)
        Hawk.delete(EMPLOYEE_USER_CAN_ADD_EDIT_OPEN_FORMS_KEY)
        Hawk.delete(EMPLOYEE_USER_CAN_VIEW_CERTS)
        Hawk.delete(EMPLOYEE_USER_CAN_EDIT_CERTS)
        Hawk.delete(EMPLOYEE_USER_CAN_EDIT_TIME_SHEET_TIME)
        Hawk.delete(EMPLOYEE_USER_CAN_VALIDATE_TIME_SHEET_WITH_PIN)
    }

    override fun setApiToken(apiToken: String) {
        Hawk.put(API_TOKEN_KEY, apiToken)
    }

    override fun getApiToken(): String {
        return Hawk.get(API_TOKEN_KEY, "")
    }

    override fun setSecurityKey(secToken: String) {
        Hawk.put(SECURITY_TOKEN_KEY, secToken)
    }

    override fun getSecurityKey(): String {
        return Hawk.get(SECURITY_TOKEN_KEY, "")
    }

    override fun setUserToken(userToken: String) {
        Hawk.put(USER_TOKEN_KEY, userToken)
    }

    override fun getUserToken(): String {
        return Hawk.get(USER_TOKEN_KEY, "")
    }

    override fun setFcmToken(fcmToken: String) {
        Hawk.put(FCM_TOKEN, fcmToken)
    }

    override fun getFcmToken(): String {
        return Hawk.get(FCM_TOKEN, "")
    }

    override fun setBaseUrl(baseUrl: String) {
        Hawk.put(BASE_URL_KEY, baseUrl)
    }

    override fun getBaseUrl(): String {
        return Hawk.get(BASE_URL_KEY, "")
    }

    override fun setCurrentCompanyName(companyName: String) {
        Hawk.put(CURRENT_COMPANY_NAME, companyName)
    }

    override fun getCurrentCompanyName(): String {
        return Hawk.get(CURRENT_COMPANY_NAME, "")
    }

    override fun setCurrentCompanyId(companyId: Int) {
        Hawk.put(CURRENT_COMPANY_ID, companyId)
    }

    override fun getCurrentCompanyId(): Int {
        return Hawk.get(CURRENT_COMPANY_ID, 0)
    }

    override fun setUserEmail(email: String) {
        Hawk.put(USER_EMAIL_KEY, email)
    }

    override fun getUserEmail(): String {
        return Hawk.get(USER_EMAIL_KEY, "")
    }

    override fun setUserPassword(password: String) {
        Hawk.put(USER_PASSWORD_KEY, password)
    }

    override fun getUserPassword(): String {
        return Hawk.get(USER_PASSWORD_KEY, "")
    }

    override fun setLoggedInEmployeeId(employeeId: Int) {
        Hawk.put(CURRENT_EMPLOYEE_ID, employeeId)
    }

    override fun getLoggedInEmployeeId(): Int {
        return Hawk.get(CURRENT_EMPLOYEE_ID)
    }

    override fun isDownloadAllCompanyCerts(): Boolean {
        return Hawk.get(DOWNLOAD_ALL_COMPANY_CERTS, false)
    }

    override fun setDownloadAllCompanyCerts(downloadAll: Boolean) {
        Hawk.put(DOWNLOAD_ALL_COMPANY_CERTS, downloadAll)
    }

    override fun setUsePhotoPin(photoPin: Boolean) {
        Hawk.put(USES_PHOTO_PIN_KEY, photoPin)
    }

    override fun setDeletePhotoPin(deletePin: Boolean) {
        Hawk.put(DELETES_PHOTO_PIN_KEY, deletePin)
    }

    override fun deletesPhotoPin(): Boolean {
        return Hawk.get(DELETES_PHOTO_PIN_KEY, true)
    }

    override fun usesPhotoPin(): Boolean {
        return Hawk.get(USES_PHOTO_PIN_KEY, true)
    }

    override fun setEmployeeId(employeeId: Int) {
        Hawk.put(EMPLOYEE_ID_KEY, employeeId)
    }

    override fun getEmployeeId(): Int {
        return Hawk.get(EMPLOYEE_ID_KEY, 0)
    }

    override fun setEmployeeFirstName(employeeFirstName: String) {
        Hawk.put(EMPLOYEE_FIRST_NAME_KEY, employeeFirstName)
    }

    override fun getEmployeeFirstName(): String {
        return Hawk.get(EMPLOYEE_FIRST_NAME_KEY, "")
    }

    override fun setEmployeeLastName(employeeLastName: String) {
        Hawk.put(EMPLOYEE_LAST_NAME_KEY, employeeLastName)
    }

    override fun getEmployeeLastName(): String {
        return Hawk.get(EMPLOYEE_LAST_NAME_KEY, "")
    }

    override fun setEmployeePinNumber(employeePinNumber: String) {
        Hawk.put(EMPLOYEE_PIN_NUMBER_KEY, employeePinNumber)
    }

    override fun getEmployeePinNumber(): String {
        return Hawk.get(EMPLOYEE_PIN_NUMBER_KEY, "")
    }

    override fun setEmployeePositionId(employeePositionId: Int) {
        Hawk.put(EMPLOYEE_POSITION_ID, employeePositionId)
    }

    override fun getEmployeePositionId(): Int {
        return Hawk.get(EMPLOYEE_POSITION_ID)
    }

    override fun setUserCanViewProjects(hasRole: Boolean) {
        Hawk.put(EMPLOYEE_USER_CAN_VIEW_PROJECTS_KEY, hasRole)
    }

    override fun canUserViewProjects(): Boolean {
        return Hawk.get(EMPLOYEE_USER_CAN_VIEW_PROJECTS_KEY, false)
    }

    override fun setUserCanAddEditOpenForms(hasRole: Boolean) {
        Hawk.put(EMPLOYEE_USER_CAN_ADD_EDIT_OPEN_FORMS_KEY, hasRole)
    }

    override fun canUserCanAddEditOpenForms(): Boolean {
        return Hawk.get(EMPLOYEE_USER_CAN_ADD_EDIT_OPEN_FORMS_KEY, false)
    }

    override fun setUserCanViewCerts(hasRole: Boolean) {
        Hawk.put(EMPLOYEE_USER_CAN_VIEW_CERTS, hasRole)
    }

    override fun canUserCanViewCerts(): Boolean {
        return Hawk.get(EMPLOYEE_USER_CAN_VIEW_CERTS, false)
    }

    override fun setUserCanEditCerts(hasRole: Boolean) {
        Hawk.put(EMPLOYEE_USER_CAN_EDIT_CERTS, hasRole)
    }

    override fun canUserCanEditCerts(): Boolean {
        return Hawk.get(EMPLOYEE_USER_CAN_EDIT_CERTS, false)
    }

    override fun setUserCanEditTimeSheetTime(hasRole: Boolean) {
        Hawk.put(EMPLOYEE_USER_CAN_EDIT_TIME_SHEET_TIME, hasRole)
    }

    override fun canUserCanEditTimeSheetTime(): Boolean {
        return Hawk.get(EMPLOYEE_USER_CAN_EDIT_TIME_SHEET_TIME, false)
    }

    override fun setUserCanValidateTimeSheetWithPin(hasRole: Boolean) {
        Hawk.put(EMPLOYEE_USER_CAN_VALIDATE_TIME_SHEET_WITH_PIN, hasRole)
    }

    override fun canUserCanValidateTimeSheetWithPin(): Boolean {
        return Hawk.get(EMPLOYEE_USER_CAN_VALIDATE_TIME_SHEET_WITH_PIN, false)
    }

    override fun isV2MigrationComplete(): Boolean {
        return Hawk.get(V2_MIGRATION_COMPLETE, false)
    }

    override fun setV2MigrationComplete(complete: Boolean) {
        Hawk.put(V2_MIGRATION_COMPLETE, complete)
    }
}
