package com.fieldflo.data.persistence.db.daos

import androidx.room.*
import com.fieldflo.data.persistence.db.entities.CompanyData
import kotlinx.coroutines.flow.Flow

@Dao
interface CompanyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addCompany(companyData: CompanyData)

    @Query("DELETE FROM BasicCompanies WHERE securityKey = :securityKey AND token = :token")
    suspend fun deleteCompany(securityKey: String, token: String)

    @Query("SELECT * FROM BasicCompanies WHERE securityKey = :securityKey AND token = :token")
    suspend fun getBasicCompanyBySecurityKeyAndToken(
        securityKey: String,
        token: String
    ): CompanyData?

    @Query("SELECT initialLoadComplete FROM BasicCompanies WHERE securityKey = :securityKey AND token = :token")
    suspend fun didInitialCompanyLoad(securityKey: String, token: String): Boolean?

    @Query("UPDATE BasicCompanies SET initialLoadComplete = 1  WHERE companyId = :companyId")
    suspend fun setInitialCompanyLoadComplete(companyId: Int)

    @Query("UPDATE BasicCompanies SET companyId = :companyId  WHERE securityKey = :securityKey AND token = :token")
    suspend fun setCompanyId(companyId: Int, securityKey: String, token: String)

    @Query("SELECT * FROM BasicCompanies ORDER BY companyName ASC")
    fun getBasicCompanies(): Flow<List<CompanyData>>

    @Query("SELECT * FROM BasicCompanies ORDER BY companyName ASC")
    suspend fun getAllCompanies(): List<CompanyData>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateAllCompanies(companies: List<CompanyData>)
}