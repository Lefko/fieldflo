package com.fieldflo.data.persistence.db.entities

data class ProjectAirMonitoringData(
    val airMonitoringFirmName: String,
    val airMonitoringContact: String,
    val airMonitoringAddress: String,
    val airMonitoringCity: String,
    val airMonitoringState: String,
    val airMonitoringZipCode: String,
    val airMonitoringPhoneNumber: String,
    val airMonitoringFaxNumber: String,
    val airMonitoringCell: String,
    val airMonitoringCertNumber: String
)