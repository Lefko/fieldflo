package com.fieldflo.data.persistence.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.fieldflo.data.persistence.db.entities.AvailableForm

@Dao
abstract class AvailableFormDao2 {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addAvailableForms(availableForms: List<AvailableForm>)

    @Query("SELECT * FROM AvailableForms WHERE companyId = :companyId AND (projectId = :projectId OR projectId = 0)")
    abstract suspend fun getAvailableForms(projectId: Int, companyId: Int): List<AvailableForm>

    @Query("DELETE FROM AvailableForms WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun deleteAvailableFormsOnProject(projectId: Int, companyId: Int)

    @Query("DELETE FROM AvailableForms WHERE companyId = :companyId")
    abstract suspend fun deleteAvailableFormsOnProject(companyId: Int)
}