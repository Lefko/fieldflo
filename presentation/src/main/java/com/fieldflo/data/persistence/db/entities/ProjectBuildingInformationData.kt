package com.fieldflo.data.persistence.db.entities

data class ProjectBuildingInformationData(
    val buildingSize: String,
    val buildingAgeOfYears: Int,
    val buildingNumberOfFloors: Int,
    val buildingPresentUse: String,
    val buildingPriorUse: String,
    val buildingNumberOfDwellingUnits: Int,
    val buildingWorkLocation: String,
    val buildingWorkDescription: String
)
