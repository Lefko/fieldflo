package com.fieldflo.data.persistence.db.daos

import androidx.room.*
import com.fieldflo.data.persistence.db.entities.*
import kotlinx.coroutines.flow.Flow

@Dao
abstract class ProjectDao2 {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addAllProjects(projects: List<Project>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addProject(project: Project)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun updateProject(project: Project)

    @Query("SELECT projectDeleted FROM Projects WHERE projectId = :projectId AND companyId = :companyId")
    abstract fun observeProjectDeleted(projectId: Int, companyId: Int): Flow<Boolean>

    @Query("SELECT usePhases FROM PROJECTS WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun isPhaseRequiredOnProject(projectId: Int, companyId: Int): Boolean

    @Query("SELECT projectId, projectNumber, projectName, clientFullName, supervisorFullName, projectCrewSize, projectManagerFullName, projectStartDate, projectEndDate, projectClosed, usePhases, phasesType, phaseCode FROM Projects WHERE companyId = :companyId ORDER BY projectId ASC")
    abstract suspend fun getAllProjects(companyId: Int): List<BasicProjectData>

    @Query("SELECT projectId, projectNumber, projectName, clientFullName, supervisorFullName, projectCrewSize, projectManagerFullName, projectStartDate, projectEndDate, projectClosed, usePhases, phasesType, phaseCode FROM Projects WHERE companyId = :companyId AND projectClosed = 0 AND projectActive = 1 AND projectDeleted = 0 ORDER BY projectId ASC")
    abstract fun getAllActiveProjectsFlow(companyId: Int): Flow<List<BasicProjectData>>

    @Query("SELECT * FROM Projects WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun getProjectById(projectId: Int, companyId: Int): Project

    @Query("SELECT perDiemAutoApplyToTimesheet FROM Projects WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun isProjectAutoApplyPerDiem(projectId: Int, companyId: Int): Boolean

    @Query("SELECT projectName FROM PROJECTS WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun getProjectName(projectId: Int, companyId: Int): String

    @Query("SELECT projectNumber FROM PROJECTS WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun getProjectNumber(projectId: Int, companyId: Int): String

    @Query("SELECT projectNumber, projectName, projectManagerFullName, supervisorFullName, projectStartDate, projectEndDate FROM Projects WHERE companyId = :companyId AND projectId = :projectId")
    abstract suspend fun getProjectGeneralInfo(
        projectId: Int,
        companyId: Int
    ): ProjectGeneralInfoData

    @Query("SELECT branchNumber, projectBidNumber, projectTypeName, projectLossTypeName, hasNightShift, shiftLength, projectCrewSize, supervisorPhone, projectAdministratorFullName, clientFullName, scopeOfWork FROM Projects WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun getProjectDetails(projectId: Int, companyId: Int): ProjectDetailsData

    @Query("SELECT projectSiteAddress, projectCrossStreet, projectCity, projectState, projectZipCode, projectCounty, projectNameOfSiteContact, contractType, projectSitePhone, estimatorFullName, divisionManagerFullName, estimatorContactPhone, salePersonFullName FROM Projects WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun getProjectSite(projectId: Int, companyId: Int): ProjectSiteData

    @Query("SELECT requiresPerDiem, perDiemType, perDiemHourlyAmount, perDiemDailyAmount, perDiemNotes, perDiemAutoApplyToTimesheet FROM Projects WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun getProjectPerDiem(projectId: Int, companyId: Int): ProjectPerDiemData

    @Query("SELECT buildingSize, buildingAgeOfYears, buildingNumberOfFloors, buildingPresentUse, buildingPriorUse, buildingNumberOfDwellingUnits, buildingWorkLocation, buildingWorkDescription FROM Projects WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun getProjectBuildingInfo(
        projectId: Int,
        companyId: Int
    ): ProjectBuildingInformationData

    @Query("SELECT ownerCompanyName, ownerAddress, ownerCity, ownerZipCode, ownerNameOfContact, ownerPhoneNumber, ownerFaxNumber, ownerCellPhone, ownerState FROM Projects WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun getProjectOwnerInfo(projectId: Int, companyId: Int): ProjectOwnerData

    @Query("SELECT procedures FROM Projects WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun getProjectProcedures(projectId: Int, companyId: Int): String

    @Query("SELECT billingCompanyName, billingPurchaseOrWorkOrderNumber, billingTermsOfContact, billingClientContact, billingAddress, billingCity, billingState, billingZipCode, billingPhoneNumber, billingFaxNumber FROM Projects WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun getProjectBillingInfo(projectId: Int, companyId: Int): ProjectBillingInfo

    @Query("SELECT airMonitoringFirmName, airMonitoringContact, airMonitoringAddress, airMonitoringCity, airMonitoringZipCode, airMonitoringPhoneNumber, airMonitoringFaxNumber, airMonitoringCell, airMonitoringCertNumber, airMonitoringState FROM Projects WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun getAirMonitoringData(
        projectId: Int,
        companyId: Int
    ): ProjectAirMonitoringData

    @Query("SELECT consultantFirmName, consultantContact, consultantAddress, consultantCity, consultantState, consultantZipCode, consultantPhoneNumber, consultantFaxNumber FROM Projects WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun getProjectConsultantData(
        projectId: Int,
        companyId: Int
    ): ProjectConsultantData

    @Query("SELECT lienLender, lienLenderAddress, lienCity, lienState, lienZipCode, lienLenderValue, lienMonth, lienDay, lienYear FROM Projects WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun getProjectLienInformation(projectId: Int, companyId: Int): ProjectLienInfo

    @Query("SELECT fireDepartmentName, fireDepartmentAddress, fireDepartmentCity, fireDepartmentState, fireDepartmentState, fireDepartmentZipCode, fireDepartmentPhone, policeDepartmentName, policeDepartmentAddress, policeDepartmentCity, policeDepartmentState, policeDepartmentZipCode, policeDepartmentPhone, medicalDepartmentName, medicalDepartmentAddress, medicalDepartmentCity, medicalDepartmentState, medicalDepartmentZipCode, medicalDepartmentPhone FROM Projects WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun getProjectEmergencyInfo(
        projectId: Int,
        companyId: Int
    ): ProjectEmergencyInfo

    @Query("SELECT insuranceAdditionalInsured, insuranceAddress, insuranceCity, insuranceState, insuranceZipCode, insurancePrimaryWordingRequest, insuranceClientProjectNumber, insuranceOtherInstructions, insuranceOtherInstructions2, insuranceOtherInstructions3, insuranceOtherInstructions4, insuranceRegMailOrigToCertHolder, insuranceFedexOrigToCertHolder, insuranceFaxToClient, insuranceFaxCopy FROM Projects WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun getProjectInsuranceCertificateData(
        projectId: Int,
        companyId: Int
    ): ProjectInsuranceCertificateData

    @Query("SELECT customField1, customField2, customField3, customField4, customField5, customField6 FROM Projects WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun getProjectOtherData(projectId: Int, companyId: Int): ProjectOtherData

    @Query("SELECT payrollWeeklyCertifiedPayroll, payrollUnionJob, payrollPrevailingWage FROM Projects WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun getProjectPayrollData(projectId: Int, companyId: Int): ProjectPayrollData

    @Query("SELECT designerFirmName, designerContact, designerAddress, designerCity, designerState, designerZipCode, designerPhoneNumber, designerFaxNumber, designerCell, designerCertNumber FROM Projects WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun getProjectDesignerData(projectId: Int, companyId: Int): ProjectDesignerData

    @Query("UPDATE Projects SET projectDeleted = 1 WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun setProjectDeleted(projectId: Int, companyId: Int)

    @Query("DELETE FROM Projects WHERE projectId = :projectId AND companyId = :companyId")
    abstract suspend fun deleteProject(projectId: Int, companyId: Int)

    @Query("DELETE FROM Projects WHERE companyId = :companyId")
    abstract suspend fun deleteProjects(companyId: Int)
}
