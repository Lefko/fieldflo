package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity

@Entity(tableName = "Haulers", primaryKeys = ["haulerId", "companyId"])
data class Hauler(
    val haulerId: Int = 0,
    val companyId: Int = 0,
    val name: String,
    val haulerDeleted: Boolean = false
)
