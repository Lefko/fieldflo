package com.fieldflo.data.persistence.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.fieldflo.data.persistence.db.entities.Hauler

@Dao
abstract class HaulersDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addAllHaulers(hauler: List<Hauler>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addHauler(hauler: Hauler)

    @Query("SELECT * FROM Haulers WHERE haulerDeleted = 0 AND companyId = :companyId")
    abstract suspend fun getAllHaulers(companyId: Int): List<Hauler>

    @Query("SELECT * FROM Haulers WHERE haulerId = :haulerId AND companyId = :companyId")
    abstract suspend fun getHaulerById(haulerId: Int, companyId: Int): Hauler?

    @Query("DELETE FROM Haulers WHERE haulerId = :haulerId AND companyId = :companyId")
    abstract suspend fun deleteHauler(haulerId: Int, companyId: Int)

    @Query("UPDATE Haulers SET haulerDeleted = 1 WHERE haulerId = :haulerId AND companyId = :companyId")
    abstract suspend fun setHaulerDeleted(haulerId: Int, companyId: Int)

    @Query("DELETE FROM Haulers WHERE companyId = :companyId")
    abstract suspend fun deleteAllHaulers(companyId: Int): Int
}