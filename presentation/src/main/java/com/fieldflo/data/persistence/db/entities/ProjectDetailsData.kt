package com.fieldflo.data.persistence.db.entities

data class ProjectDetailsData(
    val branchNumber: String,
    val projectBidNumber: String,
    val projectTypeName: String,
    val projectLossTypeName: String,
    val hasNightShift: Boolean,
    val shiftLength: Int,
    val projectCrewSize: Int,
    val supervisorPhone: String,
    val projectAdministratorFullName: String,
    val clientFullName: String,
    val scopeOfWork: String
)