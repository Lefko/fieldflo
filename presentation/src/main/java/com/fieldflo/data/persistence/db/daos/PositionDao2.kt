package com.fieldflo.data.persistence.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.fieldflo.data.persistence.db.entities.Position

@Dao
abstract class PositionDao2 {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addPositions(positions: List<Position>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addPosition(position: Position)

    @Query("SELECT * FROM Positions WHERE positionId = :positionId AND companyId = :companyId")
    abstract suspend fun getPosition(positionId: Int, companyId: Int): Position?

    @Query("UPDATE Positions SET positionDeleted = 1 WHERE positionId = :positionId AND companyId = :companyId")
    abstract suspend fun setPositionDeleted(positionId: Int, companyId: Int)

    @Query("DELETE FROM Positions WHERE positionId = :positionId AND companyId = :companyId")
    abstract suspend fun deletePosition(positionId: Int, companyId: Int)

    @Query("DELETE FROM Positions WHERE companyId = :companyId")
    abstract suspend fun deleteAllPositions(companyId: Int)

    @Query("SELECT * FROM Positions WHERE positionActive = 1 AND positionDeleted = 0 AND (projectId = :projectId OR projectId = 0) AND companyId = :companyId")
    abstract suspend fun loadPositionsByProjectId(projectId: Int, companyId: Int): List<Position>

    @Query("SELECT * FROM Positions WHERE companyId = :companyId")
    abstract suspend fun getAllPositions(companyId: Int): List<Position>
}
