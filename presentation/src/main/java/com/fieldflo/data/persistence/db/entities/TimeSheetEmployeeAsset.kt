package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "TimeSheetEmployeeAssets",
    foreignKeys = [ForeignKey(
        entity = TimeSheetEmployee::class,
        parentColumns = ["timeSheetEmployeeId"],
        childColumns = ["timeSheetEmployeeId"],
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    )],
    indices = [Index("timeSheetEmployeeId")]
)
data class TimeSheetEmployeeAsset(

    @PrimaryKey(autoGenerate = true)
    val timeSheetEmployeeAssetId: Int = 0,

    var companyId: Int = 0,
    var projectId: Int = 0,
    var internalFormId: Int = 0,
    var timeSheetFormDataId: Int = 0,
    var timeSheetEmployeeId: Int = 0,

    val employeeId: Int = 0,
    val itemId: Int = 0,
    val totalWorkTime: Int = 0,
    val notes: String = "",
    val totalWorkTimeFormatted: String = ""
)