package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DailyDemoLogMaterials(
    @PrimaryKey(autoGenerate = true)
    val dailyDemoLogMaterialsId: Int = 0,
    val formDataId: Int = 0,
    val internalFormId: Int = 0,
    val projectId: Int = 0,
    val companyId: Int = 0,
    val materialID: Int = 0,
    val materialCustom: String = "",
    val quantity: String = "",
    val amount: String = "",
    val total: String = ""
)
