package com.fieldflo.data.persistence.fileSystem

import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit

class FileManager(fileProvider: IFileProvider) : IFileProvider by fileProvider {

    fun deleteExpiredPhotoPins(companyId: Int) {
        val pinDir = getPinImagesCompanyDir(companyId)
        val todayMillis = Date().time
        val thirtyDaysMillis = TimeUnit.MILLISECONDS.convert(30, TimeUnit.DAYS)
        val pinDateDirs = try {
            pinDir.listFiles()!!.toList()
        } catch (e: Exception) {
            emptyList<File>()
        }
        for (file in pinDateDirs) {
            if (file.exists() && file.isDirectory) {
                try {
                    val dirDateMillis = file.name.toLong()
                    if (todayMillis - dirDateMillis > thirtyDaysMillis) {
                        file.deleteRecursively()
                    }
                } catch (ignored: NumberFormatException) {
                }
            }
        }
    }

    fun deleteProjectFiles(companyId: Int, projectId: Int) {
        getProjectFilesDir(companyId, projectId).deleteRecursively()
        deletePinImagesForProject(companyId, projectId)
    }

    fun getPsiPhotoPinFileLocation(
        companyId: Int,
        projectId: Int,
        employeeId: Int,
        insertDate: Date,
        internalFormId: Int
    ): File {
        return getPinImagePhotoPinForFormFile(
            companyId = companyId,
            projectId = projectId,
            id = employeeId,
            formDate = insertDate,
            internalFormId = internalFormId
        )
    }

    fun getTimesheetPhotoPinFileLocation(
        companyId: Int,
        projectId: Int,
        timeSheetEmployeeId: Int,
        timeSheetDate: Date,
        internalFormId: Int
    ): File {
        return getPinImagePhotoPinForFormFile(
            companyId = companyId,
            projectId = projectId,
            id = timeSheetEmployeeId,
            formDate = timeSheetDate,
            internalFormId = internalFormId
        )
    }

    fun getPinOutDirForForm(companyId: Int, formDate: Date, projectId: Int, internalFormId: Int) =
        getPinImagePhotoPinForFormDir(companyId, formDate, projectId, internalFormId)

    fun changeFileName(fileLocation: String, newName: String): Boolean {
        val from = File(fileLocation)
        val to = File(from.parent, newName)
        return from.renameTo(to)
    }

    fun deleteCompanyFiles(companyId: Int) {
        getCompanyDirs(companyId).forEach { it.deleteRecursively() }
    }

    private fun deletePinImagesForProject(companyId: Int, projectId: Int) {
        val pinImagesCompanyDir = getPinImagesCompanyDir(companyId)
        pinImagesCompanyDir.listFiles()?.let { pinImagesDirFile ->
            pinImagesDirFile.filter { it.isDirectory }
                .forEach { dateDir ->
                    dateDir.listFiles()?.let { dateDirFile ->
                        dateDirFile.filter { it.isDirectory && it.name == projectId.toString() }
                            .forEach { it.deleteRecursively() }
                    }
                }
        }

    }
}
