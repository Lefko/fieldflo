package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DailyDemoLogEquipment(
    @PrimaryKey(autoGenerate = true)
    val dailyDemoLogEquipmentId: Int = 0,
    val formDataId: Int = 0,
    val internalFormId: Int = 0,
    val projectId: Int = 0,
    val companyId: Int = 0,
    val equipmentID: Int = 0,
    val equipmentCustom: String = "",
    val equipmentNo: String = "",
    val descriptionOfWork: String = "",
    val hours: String = ""
)
