package com.fieldflo.data.persistence.db.entities

data class TimeSheetEmployeePerDiem(
    val perDiemType: String = "",
    val dailyAmount: Double = 0.0,
    val hourlyAmount: Double = 0.0,
    val cashGivenOnSite: Double = 0.0,
    val totalWorkTime: Int = 0,
    val notes: String = "",
    val totalEarned: Double = 0.0,
    val totalOwed: Double = 0.0
)