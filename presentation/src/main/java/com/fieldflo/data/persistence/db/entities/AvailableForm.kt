package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity

@Entity(tableName = "AvailableForms", primaryKeys = ["companyId", "projectId", "formTypeId"])
data class AvailableForm(
    val companyId: Int = 0,
    val projectId: Int = 0,
    val formTypeId: Int = 0,
    val mandatory: Boolean = false,
    val formName: String = "",
    val sort: Int = 0,
    val tabs: List<String>
)
