package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "TimesheetEmployeePositions",
    foreignKeys = [ForeignKey(
        entity = TimeSheetEmployee::class,
        parentColumns = ["timeSheetEmployeeId"],
        childColumns = ["timeSheetEmployeeId"],
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    )],
    indices = [Index("timeSheetEmployeeId")]
)
data class TimesheetEmployeePosition(

    @PrimaryKey(autoGenerate = true)
    val timeSheetEmployeePositionId: Int = 0,

    var companyId: Int = 0,
    var projectId: Int = 0,
    var internalFormId: Int = 0,
    var timeSheetFormDataId: Int = 0,
    var timeSheetEmployeeId: Int = 0,

    val positionId: Int = 0,
    val defaultPosition: Boolean = false,
    val totalWorkTime: Int = 0,
    val isDeleted: Boolean = false,
    val phaseId: Int = 0
)