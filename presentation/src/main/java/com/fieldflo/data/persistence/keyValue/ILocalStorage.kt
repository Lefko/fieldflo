package com.fieldflo.data.persistence.keyValue

interface ILocalStorage {
    fun clear()

    fun getDeviceId(): String

    fun setApiToken(apiToken: String)
    fun getApiToken(): String

    fun setSecurityKey(secToken: String)
    fun getSecurityKey(): String

    fun setUserToken(userToken: String)
    fun getUserToken(): String

    fun setFcmToken(fcmToken: String)
    fun getFcmToken(): String

    fun setBaseUrl(baseUrl: String)
    fun getBaseUrl(): String

    fun setCurrentCompanyName(companyName: String)
    fun getCurrentCompanyName(): String

    fun setCurrentCompanyId(companyId: Int)
    fun getCurrentCompanyId(): Int

    fun setUserEmail(email: String)
    fun getUserEmail(): String

    fun setUserPassword(password: String)
    fun getUserPassword(): String

    fun setLoggedInEmployeeId(employeeId: Int)
    fun getLoggedInEmployeeId(): Int

    fun isDownloadAllCompanyCerts(): Boolean
    fun setDownloadAllCompanyCerts(downloadAll: Boolean)

    fun setUsePhotoPin(photoPin: Boolean)
    fun usesPhotoPin(): Boolean

    fun setDeletePhotoPin(deletePin: Boolean)
    fun deletesPhotoPin(): Boolean

    fun setEmployeeId(employeeId: Int)
    fun getEmployeeId(): Int

    fun setEmployeeFirstName(employeeFirstName: String)
    fun getEmployeeFirstName(): String

    fun setEmployeeLastName(employeeLastName: String)
    fun getEmployeeLastName(): String

    fun setEmployeePinNumber(employeePinNumber: String)
    fun getEmployeePinNumber(): String

    fun setEmployeePositionId(employeePositionId: Int)
    fun getEmployeePositionId(): Int

    fun setUserCanViewProjects(hasRole: Boolean)
    fun canUserViewProjects(): Boolean

    fun setUserCanAddEditOpenForms(hasRole: Boolean)
    fun canUserCanAddEditOpenForms(): Boolean

    fun setUserCanViewCerts(hasRole: Boolean)
    fun canUserCanViewCerts(): Boolean

    fun setUserCanEditCerts(hasRole: Boolean)
    fun canUserCanEditCerts(): Boolean

    fun setUserCanEditTimeSheetTime(hasRole: Boolean)
    fun canUserCanEditTimeSheetTime(): Boolean

    fun setUserCanValidateTimeSheetWithPin(hasRole: Boolean)
    fun canUserCanValidateTimeSheetWithPin(): Boolean

    fun isV2MigrationComplete(): Boolean
    fun setV2MigrationComplete(complete: Boolean)
}