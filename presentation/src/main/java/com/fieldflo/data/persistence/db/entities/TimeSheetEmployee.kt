package com.fieldflo.data.persistence.db.entities

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "TimeSheetEmployees")
data class TimeSheetEmployee(

    @PrimaryKey(autoGenerate = true)
    val timeSheetEmployeeId: Int = 0,
    var timeSheetFormDataId: Int = 0,
    var companyId: Int = 0,
    var internalFormId: Int = 0,
    var projectId: Int = 0,
    val employeeId: Int = 0,
    val employeeStatus: TimesheetEmployeeStatus,
    val clockIn: Date,
    val clockOut: Date,
    val totalWorkTime: Int = 0,
    val timeVerifiedById: Int = 0,
    val timeVerifiedByDate: Date = Date(0),
    val injuredVerifiedById: Int = 0,
    val injuredVerifiedByDate: Date = Date(0),
    val breakTime: Int = 0,
    val federalBreak: Boolean = false,
    val federalBreakVerifyEmployeeId: Int = 0,
    val federalBreakVerifyDate: Date = Date(0),
    val isDeleted: Boolean = false,

    @Embedded(prefix = "per_diem_")
    val perDiem: TimeSheetEmployeePerDiem? = null,

    @Ignore
    val timeLogs: List<TimeSheetEmployeeTimeLog> = listOf(),

    @Ignore
    val assets: List<TimeSheetEmployeeAsset> = listOf(),

    @Ignore
    val positions: List<TimesheetEmployeePosition> = listOf()
) {
    constructor(
        timeSheetEmployeeId: Int = 0,
        timeSheetFormDataId: Int = 0,
        companyId: Int = 0,
        internalFormId: Int = 0,
        projectId: Int = 0,
        employeeId: Int = 0,
        employeeStatus: TimesheetEmployeeStatus,
        clockIn: Date,
        clockOut: Date,
        totalWorkTime: Int = 0,
        timeVerifiedById: Int = 0,
        timeVerifiedByDate: Date = Date(0),
        injuredVerifiedById: Int = 0,
        injuredVerifiedByDate: Date = Date(0),
        breakTime: Int = 0,
        federalBreak: Boolean = false,
        federalBreakVerifyEmployeeId: Int = 0,
        federalBreakVerifyDate: Date = Date(0),
        isDeleted: Boolean = false,
        perDiem: TimeSheetEmployeePerDiem? = null
    ) : this(
        timeSheetEmployeeId,
        timeSheetFormDataId,
        companyId,
        internalFormId,
        projectId,
        employeeId,
        employeeStatus,
        clockIn,
        clockOut,
        totalWorkTime,
        timeVerifiedById,
        timeVerifiedByDate,
        injuredVerifiedById,
        injuredVerifiedByDate,
        breakTime,
        federalBreak,
        federalBreakVerifyEmployeeId,
        federalBreakVerifyDate,
        isDeleted,
        perDiem,
        emptyList(),
        emptyList(),
        emptyList()
    )
}

enum class TimesheetEmployeeStatus {
    NOT_TRACKING, CLOCKED_IN, CLOCKED_OUT, VERIFIED, VERIFIED_WITH_INJURY
}