package com.fieldflo.data.persistence.db.daos

import androidx.room.*
import com.fieldflo.data.persistence.db.entities.PsiEmployeeData
import com.fieldflo.data.persistence.db.entities.PsiFormData
import com.fieldflo.data.persistence.db.entities.PsiTaskData
import com.fieldflo.data.persistence.db.relationWrappers.PsiFormDataWrapper

@Dao
abstract class PsiDao2 {

    @Query("SELECT * FROM PsiFormData WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun getPsiFormByInternalFormId(
        internalFormId: Int,
        companyId: Int
    ): PsiFormData?

    @Query("SELECT * FROM PsiTaskData WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun getPsiTasks(internalFormId: Int, companyId: Int): List<PsiTaskData>

    @Query("SELECT * FROM PsiEmployeeData WHERE internalFormId = :internalFormId AND companyId = :companyId")
    abstract suspend fun getPsiEmployees(internalFormId: Int, companyId: Int): List<PsiEmployeeData>

    suspend fun getFullPsiFormByInternalFormId(internalFormId: Int, companyId: Int): PsiFormData? {
        return _getPsiFormByInternalFormId(internalFormId, companyId)?.unwrap()
    }

    @Transaction
    open suspend fun deleteAllPsiFormData(internalFormId: Int, companyId: Int) {
        _deleteAllPsiEmployees(internalFormId, companyId)
        _deleteAllPsiTasks(internalFormId, companyId)
        _deleteAllPsiFormData(internalFormId, companyId)
    }

    @Transaction
    open suspend fun deleteAllPsiFormData(companyId: Int) {
        _deleteAllPsiEmployees(companyId)
        _deleteAllPsiTasks(companyId)
        _deleteAllPsiFormData(companyId)
    }

    @Insert
    abstract suspend fun savePsiFormData(psiFormData: PsiFormData): Long

    @Update
    abstract suspend fun updateFormData(psiFormData: PsiFormData)

    suspend fun savePsiTasks(
        internalFormId: Int,
        companyId: Int,
        tasks: List<PsiTaskData>
    ) {
        _deleteAllPsiTasks(internalFormId, companyId)
        _insertPsiTasks(tasks)
    }

    suspend fun savePsiEmployees(
        internalFormId: Int,
        companyId: Int,
        employees: List<PsiEmployeeData>
    ) {
        _deleteAllPsiEmployees(internalFormId, companyId)
        _insertPsiEmployees(employees)
    }

    @Transaction
    @Query("SELECT * FROM PsiFormData WHERE internalFormId = :internalFormId AND companyId = :companyId")
    protected abstract suspend fun _getPsiFormByInternalFormId(
        internalFormId: Int,
        companyId: Int
    ): PsiFormDataWrapper?

    @Query("DELETE FROM PsiEmployeeData WHERE internalFormId = :internalFormId AND companyId = :companyId")
    protected abstract suspend fun _deleteAllPsiEmployees(internalFormId: Int, companyId: Int)

    @Query("DELETE FROM PsiEmployeeData WHERE companyId = :companyId")
    protected abstract suspend fun _deleteAllPsiEmployees(companyId: Int)

    @Query("DELETE From PsiTaskData WHERE internalFormId = :internalFormId AND companyId = :companyId")
    protected abstract suspend fun _deleteAllPsiTasks(internalFormId: Int, companyId: Int)

    @Query("DELETE From PsiTaskData WHERE companyId = :companyId")
    protected abstract suspend fun _deleteAllPsiTasks(companyId: Int)

    @Query("DELETE FROM PsiFormData WHERE internalFormId = :internalFormId AND companyId = :companyId")
    protected abstract suspend fun _deleteAllPsiFormData(internalFormId: Int, companyId: Int)

    @Query("DELETE FROM PsiFormData WHERE companyId = :companyId")
    protected abstract suspend fun _deleteAllPsiFormData(companyId: Int)

    @Insert
    protected abstract suspend fun _insertPsiTasks(tasks: List<PsiTaskData>)

    @Insert
    protected abstract suspend fun _insertPsiEmployees(tasks: List<PsiEmployeeData>)
}
