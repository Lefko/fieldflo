package com.fieldflo.data.persistence.db.daos

import androidx.room.*
import com.fieldflo.data.persistence.db.entities.TimesheetEmployeePosition

@Dao
abstract class TimeSheetEmployeePositionDao {

    @Query("SELECT * FROM TimesheetEmployeePositions WHERE timeSheetEmployeeId = :timeSheetEmployeeId AND companyId = :companyId")
    abstract fun getPositionsForTimeSheetEmployee(
        timeSheetEmployeeId: Int,
        companyId: Int
    ): List<TimesheetEmployeePosition>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract fun updateTimeSheetEmployeePosition(timeSheetEmployeePosition: TimesheetEmployeePosition)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun setTimeSheetEmployeeDefaultPosition(timeSheetEmployeePosition: TimesheetEmployeePosition): Long

    @Query("UPDATE TimesheetEmployeePositions SET phaseId = :phaseId WHERE timeSheetEmployeePositionId = :timeSheetEmployeePositionId AND companyId = :companyId")
    abstract fun setPhaseToTimeSheetPosition(
        phaseId: Int,
        timeSheetEmployeePositionId: Int,
        companyId: Int
    )

    @Transaction
    open suspend fun replaceEmployeePositionData(
        timeSheetEmployeeId: Int,
        positions: List<TimesheetEmployeePosition>,
        companyId: Int
    ) {
        deletePositionDataForEmployee(timeSheetEmployeeId, companyId)
        addAllPositions(positions)
    }

    @Query("DELETE FROM TimesheetEmployeePositions WHERE timeSheetEmployeeId = :timeSheetEmployeeId AND companyId = :companyId")
    abstract suspend fun deletePositionDataForEmployee(timeSheetEmployeeId: Int, companyId: Int)

    @Insert
    abstract suspend fun addAllPositions(positions: List<TimesheetEmployeePosition>)
}
