package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity

@Entity(tableName = "InventoryItems", primaryKeys = ["inventoryItemId", "companyId"])
data class InventoryItem(
    val companyId: Int,
    val inventoryItemId: Int = 0,
    val inventoryItemName: String = "",
    val inventoryItemNumber: Int = 0,
    val assetClass: String,
    val isDeleted: Boolean = false
)
