package com.fieldflo.data.persistence.db.relationWrappers

import androidx.room.Embedded
import androidx.room.Relation
import com.fieldflo.data.persistence.db.entities.*
import java.util.*

class TimeSheetEmployeeWrapper {

    @Embedded
    var timeSheetEmployee: TimeSheetEmployee = TimeSheetEmployee(
        clockIn = Date(0),
        clockOut = Date(0),
        employeeStatus = TimesheetEmployeeStatus.NOT_TRACKING
    )

    @Relation(parentColumn = "timeSheetEmployeeId", entityColumn = "timeSheetEmployeeId")
    var positions: List<TimesheetEmployeePosition> = listOf()

    @Relation(parentColumn = "timeSheetEmployeeId", entityColumn = "timeSheetEmployeeId")
    var timeLogs: List<TimeSheetEmployeeTimeLog> = listOf()

    @Relation(parentColumn = "timeSheetEmployeeId", entityColumn = "timeSheetEmployeeId")
    var assets: List<TimeSheetEmployeeAsset> = listOf()

    fun unwrap(): TimeSheetEmployee {
        return timeSheetEmployee.copy(
            positions = positions.filter { !it.isDeleted },
            timeLogs = timeLogs,
            assets = assets
        )
    }
}