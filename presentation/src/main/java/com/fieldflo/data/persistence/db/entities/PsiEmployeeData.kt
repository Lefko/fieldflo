package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class PsiEmployeeData(

    @PrimaryKey(autoGenerate = true)
    val psiEmployeeId: Int = 0,
    val companyId: Int = 0,
    val projectId: Int = 0,
    val internalFormId: Int = 0,
    val formDataId: Int = 0,

    val employeeId: Int = 0,
    val isPinVerified: Boolean = false,
    val pinVerifiedByEmployeeId: Int,
    val pinVerifiedByName: String = "",
    val pinDate: Date = Date(0)
)
