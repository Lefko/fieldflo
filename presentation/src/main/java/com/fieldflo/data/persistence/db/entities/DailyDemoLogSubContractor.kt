package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DailyDemoLogSubContractor(
    @PrimaryKey(autoGenerate = true)
    val dailyDemoLogSubContractorId: Int = 0,
    val formDataId: Int = 0,
    val internalFormId: Int = 0,
    val projectId: Int = 0,
    val companyId: Int = 0,
    val subcontractorID: Int = 0,
    val subcontractorCustom: String = "",
    val phase: String = "",
    val hours: String = ""
)
