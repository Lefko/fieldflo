package com.fieldflo.data.persistence.db.entities

data class ProjectOtherData(
    val customField1: String,
    val customField2: String,
    val customField3: String,
    val customField4: String,
    val customField5: String,
    val customField6: String
)
