package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity

@Entity(tableName = "Employees", primaryKeys = ["employeeId", "companyId"])
data class Employee(
    val employeeId: Int = 0,
    val companyId: Int = 0,
    val positionId: Int = 0,

    val employeeFirstName: String = "",
    val employeeMiddleName: String = "",
    val employeeLastName: String = "",
    val employeeImageFullUrl: String = "",
    val employeePhoneNumber: String = "",
    val employeePinNumber: String = "",
    val admin: Boolean,
    val employeeActive: Boolean = true,
    val employeeDeleted: Boolean = false
) {

    val employeeFullName: String
        get() {
            val sb = StringBuilder()
            if (employeeFirstName.isNotEmpty()) {
                sb.append(employeeFirstName)
                sb.append(" ")
            }

            if (employeeMiddleName.isNotEmpty()) {
                sb.append(employeeMiddleName)
                sb.append(" ")
            }

        if (employeeLastName.isNotEmpty()) {
            sb.append(employeeLastName)
        }
            return sb.toString().trimEnd()
    }
}