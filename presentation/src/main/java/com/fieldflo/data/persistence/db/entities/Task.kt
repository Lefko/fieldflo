package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity

@Entity(tableName = "Tasks", primaryKeys = ["taskId", "companyId"])
data class Task(
    val taskId: Int = 0,
    val companyId: Int = 0,
    val task: String = "",
    val hazard: String = "",
    val control: String = "",
    val risk: Int = 0,
    val deleted: Boolean = false
)
