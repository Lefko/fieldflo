package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity

@Entity(tableName = "SubContractors", primaryKeys = ["subContractorId", "companyId"])
data class SubContractor(
    val subContractorId: Int = 0,
    val companyId: Int = 0,
    val name: String,
    val subcontractorDeleted: Boolean = false
)