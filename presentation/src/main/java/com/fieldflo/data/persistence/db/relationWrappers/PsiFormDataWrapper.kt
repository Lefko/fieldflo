package com.fieldflo.data.persistence.db.relationWrappers

import androidx.room.Embedded
import androidx.room.Relation
import com.fieldflo.data.persistence.db.entities.PsiEmployeeData
import com.fieldflo.data.persistence.db.entities.PsiFormData
import com.fieldflo.data.persistence.db.entities.PsiTaskData
import timber.log.Timber

data class PsiFormDataWrapper(

    @Embedded
    var preJobSafetyFormData: PsiFormData,

    @Relation(parentColumn = "formDataId", entityColumn = "formDataId")
    var tasks: List<PsiTaskData>,

    @Relation(parentColumn = "formDataId", entityColumn = "formDataId")
    var employees: List<PsiEmployeeData>
) {

    fun unwrap(): PsiFormData {
        Timber.d("PSI tasks: $tasks\nemployees: $employees")
        return preJobSafetyFormData.copy(
            tasks = this.tasks,
            employees = this.employees
        )
    }
}