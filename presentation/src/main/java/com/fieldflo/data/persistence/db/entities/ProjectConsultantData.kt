package com.fieldflo.data.persistence.db.entities

data class ProjectConsultantData(
    val consultantFirmName: String,
    val consultantContact: String,
    val consultantAddress: String,
    val consultantCity: String,
    val consultantState: String,
    val consultantZipCode: String,
    val consultantPhoneNumber: String,
    val consultantFaxNumber: String
)
