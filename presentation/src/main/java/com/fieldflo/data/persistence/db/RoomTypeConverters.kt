package com.fieldflo.data.persistence.db

import android.util.JsonReader
import android.util.JsonWriter
import androidx.room.TypeConverter
import com.fieldflo.data.persistence.db.entities.TimesheetEmployeeStatus
import timber.log.Timber
import java.io.IOException
import java.io.StringReader
import java.io.StringWriter
import java.util.*


class RoomTypeConverters {

    @TypeConverter
    fun fromDate(date: Date?): Long? {
        return date?.time ?: return null

    }

    @TypeConverter
    fun toDate(millisSinceEpoch: Long?): Date? {
        return if (millisSinceEpoch == null) {
            return null
        } else {
            Date(millisSinceEpoch)
        }
    }

    @TypeConverter
    fun fromTimesheetEmployeeStatus(status: TimesheetEmployeeStatus): String {
        return status.name
    }

    @TypeConverter
    fun toTimesheetEmployeeStatus(name: String): TimesheetEmployeeStatus {
        return TimesheetEmployeeStatus.valueOf(name)
    }

    @TypeConverter
    fun fromStringList(strings: List<String>?): String? {
        if (strings == null) {
            return null
        }

        val result = StringWriter()
        val json = JsonWriter(result)

        try {
            json.beginArray()

            for (s in strings) {
                json.value(s)
            }

            json.endArray()
            json.close()
        } catch (e: IOException) {
            Timber.e(e,"Exception creating JSON")
        }

        return result.toString()
    }

    @TypeConverter
    fun toStringList(strings: String?): List<String>? {
        if (strings == null) {
            return null
        }

        val reader = StringReader(strings)
        val json = JsonReader(reader)
        val result = mutableListOf<String>()

        try {
            json.beginArray()

            while (json.hasNext()) {
                result.add(json.nextString())
            }

            json.endArray()
        } catch (e: IOException) {
            Timber.e(e,"Exception parsing JSON")
        }

        return result.toList()
    }

    @TypeConverter
    fun fromIntList(ints: List<Int>?): String? {
        if (ints == null) {
            return null
        }

        val result = StringWriter()
        val json = JsonWriter(result)

        try {
            json.beginArray()

            for (int in ints) {
                json.value(int)
            }

            json.endArray()
            json.close()
        } catch (e: IOException) {
            Timber.e(e,"Exception creating JSON")
        }

        return result.toString()
    }

    @TypeConverter
    fun toIntList(ints: String?): List<Int>? {
        if (ints == null) {
            return null
        }

        val reader = StringReader(ints)
        val json = JsonReader(reader)
        val result = mutableListOf<Int>()

        try {
            json.beginArray()

            while (json.hasNext()) {
                result.add(json.nextInt())
            }

            json.endArray()
        } catch (e: IOException) {
            Timber.e(e,"Exception parsing JSON")
        }

        return result.toList()
    }
}