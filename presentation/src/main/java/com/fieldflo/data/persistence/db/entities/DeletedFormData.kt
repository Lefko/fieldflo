package com.fieldflo.data.persistence.db.entities

import java.util.*

data class DeletedFormData (
    val internalFormId: Int = 0,
    val projectId: Int = 0,
    val formName: String = "",
    val deleteDate: Date = Date()
)
