package com.fieldflo.data.persistence.db.entities

data class ProjectSiteData(
    val projectSiteAddress: String,
    val projectCrossStreet: String,
    val projectCity: String,
    val projectState: String,
    val projectZipCode: String,
    val projectCounty: String,
    val projectNameOfSiteContact: String,
    val contractType: String,
    val projectSitePhone: String,
    val estimatorFullName: String,
    val divisionManagerFullName: String,
    val estimatorContactPhone: String,
    val salePersonFullName: String
)