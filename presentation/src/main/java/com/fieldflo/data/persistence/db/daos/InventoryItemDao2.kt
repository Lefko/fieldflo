package com.fieldflo.data.persistence.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.fieldflo.data.persistence.db.entities.InventoryItem

@Dao
abstract class InventoryItemDao2 {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addInventoryItems(inventoryItems: List<InventoryItem>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addInventoryItem(inventoryItem: InventoryItem)

    @Query("SELECT * FROM InventoryItems WHERE (assetClass = 'Equipment' OR assetClass = 'equipment') AND isDeleted = 0 AND companyId = :companyId")
    abstract suspend fun getEquipmentInventoryItems(companyId: Int): List<InventoryItem>

    @Query("SELECT * FROM InventoryItems WHERE inventoryItemId = :inventoryItemId AND companyId = :companyId")
    abstract suspend fun getInventoryItem(inventoryItemId: Int, companyId: Int): InventoryItem?

    @Query("DELETE FROM InventoryItems WHERE inventoryItemId = :inventoryItemId AND companyId = :companyId")
    abstract suspend fun deleteInventoryItem(inventoryItemId: Int, companyId: Int)

    @Query("DELETE FROM InventoryItems WHERE companyId = :companyId")
    abstract suspend fun deleteAllInventoryItems(companyId: Int)
}
