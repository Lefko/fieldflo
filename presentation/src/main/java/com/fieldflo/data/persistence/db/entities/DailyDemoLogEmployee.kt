package com.fieldflo.data.persistence.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DailyDemoLogEmployee(
    @PrimaryKey(autoGenerate = true)
    val dailyDemoLogEmployeeId: Int = 0,
    val formDataId: Int = 0,
    val internalFormId: Int = 0,
    val projectId: Int = 0,
    val companyId: Int = 0,
    val employeeId: Int = 0,
    val descriptionOfWork: String = "",
    val hours: String = ""
)
