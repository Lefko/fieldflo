package com.fieldflo.data.persistence.db.entities

import java.util.*

data class ProjectGeneralInfoData(
    val projectNumber: String,
    val projectName: String,
    val projectManagerFullName: String,
    val supervisorFullName: String,
    val projectStartDate: Date,
    val projectEndDate: Date
)