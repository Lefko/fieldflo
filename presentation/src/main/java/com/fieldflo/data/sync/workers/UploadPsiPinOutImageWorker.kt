package com.fieldflo.data.sync.workers

import android.content.Context
import android.text.format.Formatter
import androidx.work.*
import com.fieldflo.common.Analytics
import com.fieldflo.data.api.endpoints.ImageUploadApi
import com.fieldflo.di.WorkerKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import timber.log.Timber
import java.io.File
import javax.inject.Inject

class UploadPsiPinOutImageWorker @Inject constructor(
    appContext: Context,
    workerParams: WorkerParameters,
    private val imageUploadApi: ImageUploadApi,
    private val analytics: Analytics
) : Worker(appContext, workerParams) {


    companion object {

        private const val PSI_PIN_OUT_IMAGE_EVENT = "psi_pin_out_image"
        private const val TAG = "UploadPsiPinOutImageWorker"

        private const val PIN_OUT_IMAGE_LOCATION = "pin_out_image_location_key"
        private const val PROJECT_2_FORM_ID_KEY = "project2form_id_key"
        private const val FIELDFLO_PSI_EMPLOYEE_ID = "employee_id_key"

        fun createWorkerParams(
            pinOutImageLocation: String,
            project2formId: Int,
            fieldFloPsiEmployeeId: Int
        ): Data {
            if (pinOutImageLocation.isEmpty()) {
                throw IllegalArgumentException("Got invalid pinOutImageLocation: $pinOutImageLocation")
            }

            return workDataOf(
                PIN_OUT_IMAGE_LOCATION to pinOutImageLocation,
                FIELDFLO_PSI_EMPLOYEE_ID to fieldFloPsiEmployeeId,
                PROJECT_2_FORM_ID_KEY to project2formId
            )
        }
    }

    override fun doWork(): Result {
        try {
            Timber.tag(TAG).d("uploading psi pin out image")
            val pinOutImageLocation = inputData.getString(PIN_OUT_IMAGE_LOCATION)
            val fieldFloPsiEmployeeId = inputData.getInt(FIELDFLO_PSI_EMPLOYEE_ID, 0)
            val project2formId = inputData.getInt(PROJECT_2_FORM_ID_KEY, 0)
            Timber.tag("PsiPin")
                .d("pinOutImageLocation: $pinOutImageLocation, fieldFloPsiEmployeeId: $fieldFloPsiEmployeeId, project2formId: $project2formId")

            if (pinOutImageLocation.isNullOrEmpty()) {
                Timber.tag(TAG).d("No pin out image location")
                Timber.tag("PsiPin").d("No pin out image location")
                return Result.success()
            }

            val pinOutImage = File(pinOutImageLocation)
            if (!pinOutImage.exists()) {
                Timber.tag(TAG).d("Pin out image doesnt exist")
                Timber.tag("PsiPin").d("Pin out image doesnt exist")
                return Result.success()
            }

            if (fieldFloPsiEmployeeId == 0 || project2formId == 0) {
                Timber.tag(TAG)
                    .d("Cant upload pin out image without fieldFloPsiEmployeeId AND project2formId")
                Timber.tag("PsiPin")
                    .d("Cant upload pin out image without fieldFloPsiEmployeeId AND project2formId")
                return Result.success()
            }

            val project2formIdPart =
                "$project2formId".toRequestBody("multipart/form-data".toMediaTypeOrNull())
            val autoIdsPart =
                "$fieldFloPsiEmployeeId".toRequestBody("multipart/form-data".toMediaTypeOrNull())
            val imagePart = MultipartBody.Part.createFormData(
                "image",
                pinOutImage.name,
                pinOutImage.asRequestBody("multipart/form-data".toMediaTypeOrNull())
            )

            Timber.tag("PsiPin")
                .d("Uploading to project2formId: $project2formId for fieldFloPsiEmployeeId: $fieldFloPsiEmployeeId")
            Timber.tag(TAG)
                .d("Uploading to project2formId: $project2formId for fieldFloPsiEmployeeId: $fieldFloPsiEmployeeId")

            val imageSize = Formatter.formatFileSize(applicationContext, pinOutImage.length())
            Timber.tag("PsiPin").d("Pin out image size = $imageSize")
            Timber.tag(TAG).d("Pin out image size = $imageSize")
            analytics.logEvent(
                PSI_PIN_OUT_IMAGE_EVENT, listOf(
                    "File" to pinOutImage.absolutePath,
                    "ImageSize" to imageSize
                )
            )

            val response = imageUploadApi.uploadPsiPinOutImage(
                project2formId = project2formIdPart,
                employeeId = autoIdsPart,
                image = imagePart
            ).execute()

            return if (response.isSuccessful) {
                Timber.tag("PsiPin").d("Success!!!")
                Timber.tag(TAG).d("Success!!!")
                pinOutImage.delete()
                Result.success()
            } else {
                Timber.tag("PsiPin").d("Got error when uploading pin out image: ${response.code()}")
                Timber.tag(TAG).d("Got error when uploading pin out image: ${response.code()}")
                Result.retry()
            }
        } catch (e: Exception) {
            Timber.tag("PsiPin")
                .d(e, "Something unexpected happened. Message = ${e.message} Cause = ${e.cause}")
            Timber.tag(TAG)
                .d(e, "Something unexpected happened. Message = ${e.message} Cause = ${e.cause}")
            return Result.retry()
        }
    }

    @Module
    abstract class Builder {
        @Binds
        @IntoMap
        @WorkerKey(UploadPsiPinOutImageWorker::class)
        abstract fun bindUploadPsiPinOutImageWorker(worker: UploadPsiPinOutImageWorker): ListenableWorker
    }
}