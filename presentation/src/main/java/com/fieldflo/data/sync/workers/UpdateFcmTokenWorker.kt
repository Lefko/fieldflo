package com.fieldflo.data.sync.workers

import android.content.Context
import androidx.work.*
import com.fieldflo.data.api.endpoints.AppSyncApi
import com.fieldflo.data.api.endpoints.UpdateFirebaseTokenBody
import com.fieldflo.di.WorkerKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Inject

class UpdateFcmTokenWorker @Inject constructor(
    appContext: Context,
    workerParams: WorkerParameters,
    private val appSyncApi: AppSyncApi
) : CoroutineWorker(appContext, workerParams) {

    companion object {

        private const val FCM_TOKEN_KEY = "fcm_token_key"
        private const val DEVICE_ID_KEY = "device_id_key"

        fun createWorkerParams(fcmToken: String, deviceId: String): Data {
            return workDataOf(
                FCM_TOKEN_KEY to fcmToken,
                DEVICE_ID_KEY to deviceId
            )
        }
    }

    override suspend fun doWork(): Result {
        val fcmToken = inputData.getString(FCM_TOKEN_KEY)
        val deviceId = inputData.getString(DEVICE_ID_KEY)

        if (fcmToken.isNullOrEmpty() || deviceId.isNullOrEmpty()) {
            return Result.success()
        }

        return try {
            appSyncApi.updateFirebaseToken(UpdateFirebaseTokenBody(deviceId, fcmToken))
            Result.success()
        } catch (e: Exception) {
            Result.retry()
        }
    }

    @Module
    abstract class Builder {
        @Binds
        @IntoMap
        @WorkerKey(UpdateFcmTokenWorker::class)
        abstract fun bindUpdateFcmTokenWorker(worker: UpdateFcmTokenWorker): ListenableWorker
    }
}