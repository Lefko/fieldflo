package com.fieldflo.data.sync

import android.content.Context
import androidx.work.*
import com.fieldflo.data.sync.workers.*
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class WorkScheduler @Inject constructor(private val context: Context) {

    private val workConstraints =
        Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()

    fun hasPendingWork(): Boolean {
        val tags = listOf(
            UpdateFcmTokenWorker::class.java.canonicalName,
            UploadDailyFieldReportImageWorker::class.java.canonicalName,
            UploadTimesheetPinOutImageWorker::class.java.canonicalName,
            UploadPsiPinOutImageWorker::class.java.canonicalName
        )
        val states = listOf(
            WorkInfo.State.ENQUEUED,
            WorkInfo.State.RUNNING,
            WorkInfo.State.BLOCKED
        )
        val query = WorkQuery.Builder.fromTags(tags).addStates(states).build()
        return try {
            WorkManager.getInstance(context).getWorkInfos(query)
                .get(5, TimeUnit.SECONDS)
                .isNotEmpty()
        } catch (e: Exception) {
            true
        }
    }

    fun updateFcmToken(fcmToken: String, deviceId: String) {
        WorkManager.getInstance(context)
            .enqueue(
                createOneTimeWork(UpdateFcmTokenWorker::class.java)
                    .apply {
                        setInputData(UpdateFcmTokenWorker.createWorkerParams(fcmToken, deviceId))
                    }
                    .build()
            )
    }

    fun uploadPsiEmployeePinOutImage(
        imageLocation: String, fieldFloPsiEmployeeId: Int, project2formId: Int
    ) {
        Timber.tag("ServerUpdates").d("Scheduling psi pin out image upload")
        WorkManager.getInstance(context)
            .enqueue(
                createOneTimeWork(UploadPsiPinOutImageWorker::class.java)
                    .apply {
                        setInputData(
                            UploadPsiPinOutImageWorker.createWorkerParams(
                                pinOutImageLocation = imageLocation,
                                fieldFloPsiEmployeeId = fieldFloPsiEmployeeId,
                                project2formId = project2formId
                            )
                        )
                    }
                    .build()
            )
    }

    fun uploadDailyDemoLogSupervisorPinOutImage(imageLocation: String, project2formId: Int) {
        Timber.tag("ServerUpdates")
            .d("Scheduling daily demo log supervisor pin out image upload")
        WorkManager.getInstance(context)
            .enqueue(
                createOneTimeWork(UploadDailyDemoSupervisorPinOutImageWorker::class.java)
                    .apply {
                        setInputData(
                            UploadDailyDemoSupervisorPinOutImageWorker.createWorkerParams(
                                pinOutImageLocation = imageLocation,
                                project2formId = project2formId
                            )
                        )
                    }
                    .build()
            )
    }

    fun uploadDailyDemoLogImage(imageLocation: String, project2formId: Int) {
        Timber.tag("ServerUpdates")
            .d("Scheduling daily demo log image upload")
        WorkManager.getInstance(context)
            .enqueue(
                createOneTimeWork(UploadDailyDemoImageWorker::class.java)
                    .apply {
                        setInputData(
                            UploadDailyDemoImageWorker.createWorkerParams(
                                pinOutImageLocation = imageLocation,
                                project2formId = project2formId
                            )
                        )
                    }
                    .build()
            )
    }

    fun uploadTimesheetEmployeePinOutImage(
        imageLocation: String, project2formId: Int, fieldFloTimesheetEmployeeId: Int
    ) {
        WorkManager.getInstance(context)
            .enqueue(
                createOneTimeWork(UploadTimesheetPinOutImageWorker::class.java)
                    .apply {
                        setInputData(
                            UploadTimesheetPinOutImageWorker.createWorkerParams(
                                pinOutImageLocation = imageLocation,
                                project2formId = project2formId,
                                fieldFloTimesheetEmployeeId = fieldFloTimesheetEmployeeId
                            )
                        )
                    }
                    .build()
            )
    }

    fun uploadSavedDailyFieldReportImage(
        project2formId: Int,
        imageNumber: Int,
        imageLocation: String
    ) {
        WorkManager.getInstance(context)
            .enqueue(
                createOneTimeWork(UploadDailyFieldReportImageWorker::class.java)
                    .apply {
                        setInputData(
                            UploadDailyFieldReportImageWorker.createWorkerParams(
                                project2formId = project2formId,
                                imageNumber = imageNumber,
                                imageLocation = imageLocation
                            )
                        )
                    }
                    .build()
            )
    }

    fun startServerSyncWorker() {
        WorkManager.getInstance(context)
            .enqueueUniquePeriodicWork(
                AppToServerUpdatesWorker::class.java.name,
                ExistingPeriodicWorkPolicy.KEEP,
                PeriodicWorkRequest.Builder(
                    AppToServerUpdatesWorker::class.java,
                    15,
                    TimeUnit.MINUTES
                )
                    .setConstraints(workConstraints)
                    .build()
            )
    }

    private fun createOneTimeWork(workerClass: Class<out ListenableWorker>): OneTimeWorkRequest.Builder {
        return OneTimeWorkRequest.Builder(workerClass)
            .setConstraints(workConstraints)
            .setBackoffCriteria(BackoffPolicy.LINEAR, 1, TimeUnit.SECONDS)
    }
}