package com.fieldflo.data.sync.workers

import android.content.Context
import android.text.format.Formatter
import androidx.work.*
import com.fieldflo.common.Analytics
import com.fieldflo.data.api.endpoints.ImageUploadApi
import com.fieldflo.di.WorkerKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import timber.log.Timber
import java.io.File
import javax.inject.Inject

class UploadDailyDemoImageWorker @Inject constructor(
    appContext: Context,
    workerParams: WorkerParameters,
    private val imageUploadApi: ImageUploadApi,
    private val analytics: Analytics
) : Worker(appContext, workerParams) {

    companion object {

        private const val DAILY_DEMO_LOG_IMAGE_EVENT =
            "daily_demo_log_image"
        private const val TAG = "UploadDailyDemoImageWorker"

        private const val PIN_OUT_IMAGE_LOCATION = "pin_out_image_location_key"
        private const val PROJECT_2_FORM_ID_KEY = "project2form_id_key"

        fun createWorkerParams(
            pinOutImageLocation: String,
            project2formId: Int
        ): Data {
            if (pinOutImageLocation.isEmpty()) {
                throw IllegalArgumentException("Got invalid pinOutImageLocation: $pinOutImageLocation")
            }

            return workDataOf(
                PIN_OUT_IMAGE_LOCATION to pinOutImageLocation,
                PROJECT_2_FORM_ID_KEY to project2formId
            )
        }
    }

    override fun doWork(): Result {
        try {
            Timber.tag(TAG).d("uploading daily demo log supervisor pin out image")
            val pinOutImageLocation = inputData.getString(PIN_OUT_IMAGE_LOCATION)
            val project2formId = inputData.getInt(PROJECT_2_FORM_ID_KEY, 0)
            Timber.tag(TAG)
                .d("pinOutImageLocation: $pinOutImageLocation, project2formId: $project2formId")

            if (pinOutImageLocation.isNullOrEmpty()) {
                Timber.tag(TAG).d("No pin out image location")
                return Result.success()
            }

            val pinOutImage = File(pinOutImageLocation)
            if (!pinOutImage.exists()) {
                Timber.tag(TAG).d("Pin out image doesnt exist")
                return Result.success()
            }

            val project2formIdPart =
                "$project2formId".toRequestBody("multipart/form-data".toMediaTypeOrNull())
            val imagePart: Array<MultipartBody.Part> = arrayOf(
                MultipartBody.Part.createFormData(
                    "image[]",
                    pinOutImage.name,
                    pinOutImage.asRequestBody("multipart/form-data".toMediaTypeOrNull())
                )
            )

            Timber.tag(TAG).d("Uploading to project2formId: $project2formId")

            val imageSize = Formatter.formatFileSize(applicationContext, pinOutImage.length())
            Timber.tag(TAG).d("Pin out image size = $imageSize")
            analytics.logEvent(
                DAILY_DEMO_LOG_IMAGE_EVENT, listOf(
                    "File" to pinOutImage.absolutePath,
                    "ImageSize" to imageSize
                )
            )

            val response = imageUploadApi.uploadDailyDemoLogImage(
                project2formId = project2formIdPart,
                image = imagePart
            ).execute()

            return if (response.isSuccessful) {
                pinOutImage.delete()
                Result.success()
            } else {
                Timber.tag(TAG).d("Got error when uploading pin out image: ${response.code()}")
                Result.retry()
            }
        } catch (e: Exception) {
            Timber.tag(TAG)
                .d(e, "Something unexpected happened. Message = ${e.message} Cause = ${e.cause}")
            return Result.retry()
        }
    }

    @Module
    abstract class Builder {
        @Binds
        @IntoMap
        @WorkerKey(UploadDailyDemoImageWorker::class)
        abstract fun bindUploadUploadDailyDemoImageWorker(worker: UploadDailyDemoImageWorker): ListenableWorker
    }
}