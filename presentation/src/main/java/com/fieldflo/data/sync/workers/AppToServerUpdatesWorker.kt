package com.fieldflo.data.sync.workers

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.ListenableWorker
import androidx.work.WorkerParameters
import com.fieldflo.di.WorkerKey
import com.fieldflo.usecases.ServerUpdatesUseCases
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Inject

class AppToServerUpdatesWorker @Inject constructor(
    appContext: Context,
    workerParams: WorkerParameters,
    private val serverUpdatesUseCases: ServerUpdatesUseCases
) : CoroutineWorker(appContext, workerParams) {

    override suspend fun doWork(): Result {
        serverUpdatesUseCases.performServerSync()
        return Result.success()
    }

    @Module
    abstract class Builder {
        @Binds
        @IntoMap
        @WorkerKey(AppToServerUpdatesWorker::class)
        abstract fun bindAppToServerUpdatesWorker(worker: AppToServerUpdatesWorker): ListenableWorker
    }
}