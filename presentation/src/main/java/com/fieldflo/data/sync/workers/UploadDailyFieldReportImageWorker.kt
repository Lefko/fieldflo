package com.fieldflo.data.sync.workers

import android.content.Context
import android.text.format.Formatter
import androidx.work.ListenableWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.fieldflo.common.Analytics
import com.fieldflo.data.api.endpoints.ImageUploadApi
import com.fieldflo.di.WorkerKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import timber.log.Timber
import java.io.File
import javax.inject.Inject

class UploadDailyFieldReportImageWorker @Inject constructor(
    appContext: Context,
    workerParams: WorkerParameters,
    private val imageUploadApi: ImageUploadApi,
    private val analytics: Analytics
) : Worker(appContext, workerParams) {

    companion object {
        private const val TAG = "UploadDailyFieldReportImageWorker"
        private const val IMAGE_NUMBER_KEY = "image_number_key"
        private const val PROJECT_2_FORM_ID_KEY = "project2form_id_key"

        private const val IMAGE_LOCATION_KEY = "image_location_key"

        fun createWorkerParams(project2formId: Int, imageNumber: Int, imageLocation: String) =
            workDataOf(
                PROJECT_2_FORM_ID_KEY to project2formId,
                IMAGE_NUMBER_KEY to imageNumber,
                IMAGE_LOCATION_KEY to imageLocation
            )
    }

    override fun doWork(): Result {
        val imageLocation = inputData.getString(IMAGE_LOCATION_KEY)
        if (imageLocation.isNullOrEmpty()) {
            return Result.success()
        }

        val image = File(imageLocation)
        if (image.isDirectory || !image.exists()) {
            return Result.success()
        }

        val imageNumber = inputData.getInt(IMAGE_NUMBER_KEY, 0)
        val project2formId = inputData.getInt(PROJECT_2_FORM_ID_KEY, 0)

        if (project2formId == 0 || imageNumber < 1 || imageNumber > 6) {
            return Result.success()
        }

        val imageSize = Formatter.formatFileSize(applicationContext, image.length())
        Timber.tag(TAG).d("Daily field report image size = $imageSize")
        analytics.logEvent(
            "dfr_image", listOf(
                "File" to image.absolutePath,
                "ImageSize" to imageSize
            )
        )

        val project2formIdPart =
            "$project2formId".toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val imageParts: Array<MultipartBody.Part> = arrayOf(
            MultipartBody.Part.createFormData(
                "image[]",
                image.name,
                image.asRequestBody("image/*".toMediaTypeOrNull())
            )
        )

        val response = imageUploadApi.uploadDailyFieldReportImage(
            project2formId = project2formIdPart,
            images = imageParts
        ).execute()

        return if (response.isSuccessful) {
            image.delete()
            Result.success()
        } else {
            Result.retry()
        }
    }

    @Module
    abstract class Builder {
        @Binds
        @IntoMap
        @WorkerKey(UploadDailyFieldReportImageWorker::class)
        abstract fun bindUploadDailyFieldReportImageWorker(worker: UploadDailyFieldReportImageWorker): ListenableWorker
    }
}