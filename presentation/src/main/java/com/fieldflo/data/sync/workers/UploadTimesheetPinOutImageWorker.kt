package com.fieldflo.data.sync.workers

import android.content.Context
import android.text.format.Formatter
import androidx.work.*
import com.fieldflo.common.Analytics
import com.fieldflo.data.api.endpoints.ImageUploadApi
import com.fieldflo.di.WorkerKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import timber.log.Timber
import java.io.File
import javax.inject.Inject

class UploadTimesheetPinOutImageWorker @Inject constructor(
    appContext: Context,
    workerParams: WorkerParameters,
    private val imageUploadApi: ImageUploadApi,
    private val analytics: Analytics
) : Worker(appContext, workerParams) {

    companion object {

        private const val TAG = "UploadTimesheetPinOutImageWorker"
        private const val PIN_OUT_IMAGE_LOCATION = "pin_out_image_location_key"
        private const val PROJECT_2_FORM_ID_KEY = "project2form_id_key"
        private const val FIELDFLO_TIMESHEET_EMPLOYEE_ID = "employee_id_key"

        fun createWorkerParams(
            pinOutImageLocation: String,
            project2formId: Int,
            fieldFloTimesheetEmployeeId: Int
        ): Data {
            if (pinOutImageLocation.isEmpty()) {
                throw IllegalArgumentException("Got invalid pinOutImageLocation: $pinOutImageLocation")
            }

            return workDataOf(
                PIN_OUT_IMAGE_LOCATION to pinOutImageLocation,
                PROJECT_2_FORM_ID_KEY to project2formId,
                FIELDFLO_TIMESHEET_EMPLOYEE_ID to fieldFloTimesheetEmployeeId
            )
        }
    }

    override fun doWork(): Result {
        Timber.d("uploading timesheet pin out image")
        val pinOutImageLocation = inputData.getString(PIN_OUT_IMAGE_LOCATION)
        val project2formId = inputData.getInt(PROJECT_2_FORM_ID_KEY, 0)
        val fieldFloTimesheetEmployeeId = inputData.getInt(FIELDFLO_TIMESHEET_EMPLOYEE_ID, 0)

        if (pinOutImageLocation.isNullOrEmpty()) {
            return Result.success()
        }

        val pinOutImage = File(pinOutImageLocation)
        if (!pinOutImage.exists()) {
            return Result.success()
        }

        if (project2formId == 0 || fieldFloTimesheetEmployeeId == 0) {
            return Result.success()
        }

        val imageSize = Formatter.formatFileSize(applicationContext, pinOutImage.length())
        Timber.tag(TAG).d("Pin out image size = $imageSize")
        analytics.logEvent("ts_pin_out_image", listOf(
            "File" to pinOutImage.absolutePath,
            "ImageSize" to imageSize
        ))

        val project2formIdPart =
            "$project2formId".toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val autoIdsPart =
            "$fieldFloTimesheetEmployeeId".toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val imagePart = MultipartBody.Part.createFormData(
            "image",
            pinOutImage.name,
            pinOutImage.asRequestBody("multipart/form-data".toMediaTypeOrNull())
        )

        val response = imageUploadApi.uploadTimeSheetPinOutImage(
            project2formId = project2formIdPart,
            employeeAutoIds = autoIdsPart,
            image = imagePart
        ).execute()

        return if (response.isSuccessful) {
            pinOutImage.delete()
            Result.success()
        } else {
            Result.retry()
        }
    }

    @Module
    abstract class Builder {
        @Binds
        @IntoMap
        @WorkerKey(UploadTimesheetPinOutImageWorker::class)
        abstract fun bindInitialSaveTimeSheetDataWorker(worker: UploadTimesheetPinOutImageWorker): ListenableWorker
    }
}