package com.fieldflo.data.repos.interfaces

import com.fieldflo.data.persistence.db.entities.Position

interface IPositionsRepo {
    @Throws(Exception::class)
    suspend fun loadPositions(companyId: Int)
    suspend fun replaceAllPositions(companyId: Int)
    suspend fun loadNewPosition(positionId: Int, companyId: Int)
    suspend fun deletePosition(positionId: Int, companyId: Int)
    suspend fun getPositionById(positionId: Int, companyId: Int): Position?
    suspend fun getPositionsForProject(projectId: Int, companyId: Int): List<Position>
    suspend fun deletePositions(companyId: Int)
}
