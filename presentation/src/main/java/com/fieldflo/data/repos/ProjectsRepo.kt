package com.fieldflo.data.repos

import com.fieldflo.data.api.endpoints.CompanyDataApi
import com.fieldflo.data.api.endpoints.GetByIdListBody
import com.fieldflo.data.api.entities.ProjectRestObject
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.entities.*
import com.fieldflo.data.repos.interfaces.IProjectsRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class ProjectsRepo(db: FieldFloDatabase, private val companyDataApi: CompanyDataApi) :
    IProjectsRepo {

    private val projectDao = db.getProjectDao2()
    private val projectPermitDao = db.getProjectPermitDao2()

    override suspend fun loadProject(projectId: Int, companyId: Int): List<ProjectRestObject> {
        return withContext(Dispatchers.IO) {
            val savedProject = projectDao.getProjectById(projectId, companyId)
            val projectsRest =
                companyDataApi.getProjectsByIds(GetByIdListBody(intArrayOf(projectId)))
            val newProjects = projectsRest.map { it.toProject(companyId) }
            if (newProjects.size == 1) {
                @Suppress("SENSELESS_COMPARISON")
                if (savedProject == null) {
                    projectDao.addProject(newProjects[0])
                } else {
                    projectDao.updateProject(newProjects[0])
                }
            }

            projectsRest
        }
    }

    override suspend fun replaceProjectPermits(
        permits: List<ProjectPermit>,
        projectId: Int,
        companyId: Int
    ) {
        withContext(Dispatchers.IO) {
            projectPermitDao.deleteProjectPermits(projectId, companyId)
            projectPermitDao.addProjectPermitList(permits)
        }
    }

    override fun projectDeleted(projectId: Int, companyId: Int): Flow<Boolean> {
        Timber.tag("ProjectsRepo").d("Is project open")
        return projectDao.observeProjectDeleted(projectId, companyId)
            .flowOn(Dispatchers.IO)
            .onEach { Timber.tag("ProjectsRepo").d("ProjectId: $projectId is deleted: $it") }
    }

    override suspend fun getAllProjects(companyId: Int): List<BasicProjectData> {
        Timber.tag("ProjectsRepo").d("Get all projects")
        return withContext(Dispatchers.IO) {
            projectDao.getAllProjects(companyId)
        }
    }

    override fun getAllActiveProjectsFlow(companyId: Int): Flow<List<BasicProjectData>> {
        Timber.tag("ProjectsRepo").d("Get all projects flow")
        return projectDao.getAllActiveProjectsFlow(companyId)
    }

    override suspend fun isProjectAutoApplyPerDiem(projectId: Int, companyId: Int): Boolean {
        Timber.tag("ProjectsRepo").d("Is project auto apply per diem for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectDao.isProjectAutoApplyPerDiem(projectId, companyId)
        }
    }

    override suspend fun getProjectName(projectId: Int, companyId: Int): String {
        Timber.tag("ProjectsRepo").d("Get project name for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectDao.getProjectName(projectId, companyId)
        }
    }

    override suspend fun getProjectNumber(projectId: Int, companyId: Int): String {
        Timber.tag("ProjectsRepo").d("Get project number for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectDao.getProjectNumber(projectId, companyId)
        }
    }

    override suspend fun getProjectById(projectId: Int, companyId: Int): Project {
        Timber.tag("ProjectsRepo").d("Get project by id $projectId")
        return withContext(Dispatchers.IO) {
            projectDao.getProjectById(projectId, companyId)
        }
    }

    override suspend fun getProjectDetailsGeneralInfo(
        projectId: Int,
        companyId: Int
    ): ProjectGeneralInfoData {
        Timber.tag("ProjectsRepo").d("Get project general info for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectDao.getProjectGeneralInfo(projectId, companyId)
        }
    }

    override suspend fun getProjectDetails(projectId: Int, companyId: Int): ProjectDetailsData {
        Timber.tag("ProjectsRepo").d("Get project details for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectDao.getProjectDetails(projectId, companyId)
        }
    }

    override suspend fun getProjectSite(projectId: Int, companyId: Int): ProjectSiteData {
        Timber.tag("ProjectsRepo").d("Get project site for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectDao.getProjectSite(projectId, companyId)
        }
    }

    override suspend fun getPermits(projectId: Int, companyId: Int): List<ProjectPermit> {
        Timber.tag("ProjectsRepo").d("Get project permits for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectPermitDao.getProjectPermits(projectId, companyId)
        }
    }

    override suspend fun getProjectPerDiemData(projectId: Int, companyId: Int): ProjectPerDiemData {
        Timber.tag("ProjectsRepo").d("Get project per diem for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectDao.getProjectPerDiem(projectId, companyId)
        }
    }

    override suspend fun getProjectBuildingInfo(
        projectId: Int,
        companyId: Int
    ): ProjectBuildingInformationData {
        Timber.tag("ProjectsRepo").d("Get project building info for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectDao.getProjectBuildingInfo(projectId, companyId)
        }
    }

    override suspend fun getProjectOwnerData(projectId: Int, companyId: Int): ProjectOwnerData {
        Timber.tag("ProjectsRepo").d("Get project owner info for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectDao.getProjectOwnerInfo(projectId, companyId)
        }
    }

    override suspend fun getProjectProcedures(projectId: Int, companyId: Int): String {
        Timber.tag("ProjectsRepo").d("Get project procedures for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectDao.getProjectProcedures(projectId, companyId)
        }
    }

    override suspend fun getProjectBillingInfo(projectId: Int, companyId: Int): ProjectBillingInfo {
        Timber.tag("ProjectsRepo").d("Get project billing info for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectDao.getProjectBillingInfo(projectId, companyId)
        }
    }

    override suspend fun getProjectAirMonitoringData(
        projectId: Int,
        companyId: Int
    ): ProjectAirMonitoringData {
        Timber.tag("ProjectsRepo").d("Get project air monitoring data for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectDao.getAirMonitoringData(projectId, companyId)
        }
    }

    override suspend fun getProjectConsultantData(
        projectId: Int,
        companyId: Int
    ): ProjectConsultantData {
        Timber.tag("ProjectsRepo").d("Get project air monitoring data for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectDao.getProjectConsultantData(projectId, companyId)
        }
    }

    override suspend fun getProjectLienInformation(
        projectId: Int,
        companyId: Int
    ): ProjectLienInfo {
        Timber.tag("ProjectsRepo").d("Get project air monitoring data for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectDao.getProjectLienInformation(projectId, companyId)
        }
    }

    override suspend fun getProjectEmergencyInfo(
        projectId: Int,
        companyId: Int
    ): ProjectEmergencyInfo {
        Timber.tag("ProjectsRepo").d("Get project emergencyInfo for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectDao.getProjectEmergencyInfo(projectId, companyId)
        }
    }

    override suspend fun getProjectInsuranceCertificateData(
        projectId: Int,
        companyId: Int
    ): ProjectInsuranceCertificateData {
        Timber.tag("ProjectsRepo")
            .d("Get Project Insurance Certificate Data for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectDao.getProjectInsuranceCertificateData(projectId, companyId)
        }
    }

    override suspend fun getProjectOtherData(projectId: Int, companyId: Int): ProjectOtherData {
        Timber.tag("ProjectsRepo").d("Get Project Other Data for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectDao.getProjectOtherData(projectId, companyId)
        }
    }

    override suspend fun getProjectPayrollData(projectId: Int, companyId: Int): ProjectPayrollData {
        Timber.tag("ProjectsRepo").d("Get Project payroll Data for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectDao.getProjectPayrollData(projectId, companyId)
        }
    }

    override suspend fun getProjectDesignerData(
        projectId: Int,
        companyId: Int
    ): ProjectDesignerData {
        Timber.tag("ProjectsRepo").d("Get Project Designer Data for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectDao.getProjectDesignerData(projectId, companyId)
        }
    }

    override suspend fun isPhaseRequiredOnProject(projectId: Int, companyId: Int): Boolean {
        return withContext(Dispatchers.IO) {
            projectDao.isPhaseRequiredOnProject(projectId, companyId)
        }
    }

    override suspend fun deleteProject(projectId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            launch {
                projectDao.setProjectDeleted(
                    projectId,
                    companyId
                ) // this triggers project status change to project related screens giving them a trigger to close
                delay(100)
                projectDao.deleteProject(projectId, companyId)
            }

            launch {
                projectPermitDao.deleteProjectPermits(projectId, companyId)
            }
        }
    }

    override suspend fun deleteProjects(companyId: Int) {
        withContext(Dispatchers.IO) {
            projectDao.deleteProjects(companyId)
            projectPermitDao.deleteProjectPermits(companyId)
        }
    }
}
