package com.fieldflo.data.repos.interfaces

import com.fieldflo.data.persistence.db.entities.ActiveFormData
import com.fieldflo.data.persistence.db.entities.ClosedFormData
import com.fieldflo.data.persistence.db.entities.DeletedFormData
import com.fieldflo.data.persistence.db.entities.ProjectForm
import kotlinx.coroutines.flow.Flow

interface IProjectFormsRepo {
    fun hasUnclosedFormsFlow(companyId: Int): Flow<Boolean>
    suspend fun hasUnclosedForms(companyId: Int): Boolean
    suspend fun isFormSynced(internalFormId: Int, companyId: Int): Boolean
    suspend fun getUnsyncedClosedForms(companyId: Int): List<ProjectForm>
    suspend fun setHasSyncError(projectForm: ProjectForm)
    suspend fun reopenForm(internalFormId: Int, companyId: Int)
    suspend fun logSyncError(formDataForLogging: String, formTypeId: Int, formName: String)
    suspend fun getProjectForms(projectId: Int, companyId: Int): List<ProjectForm>

    suspend fun getProjectForm(internalFormId: Int, companyId: Int): ProjectForm?
    suspend fun triggerFormUpdate(internalFormId: Int, companyId: Int)

    suspend fun addFormsToProject(projectForms: List<ProjectForm>, companyId: Int)
    suspend fun getActiveProjectForms(projectId: Int, companyId: Int): List<ActiveFormData>
    fun getActiveProjectFormsFlow(projectId: Int, companyId: Int): Flow<List<ActiveFormData>>

    fun getClosedProjectForms(projectId: Int, companyId: Int): Flow<List<ClosedFormData>>
    suspend fun closeProjectForm(internalFormId: Int, companyId: Int)
    suspend fun setProjectClosedSynced(internalFormId: Int, companyId: Int)

    fun getDeletedProjectFormsFlow(projectId: Int, companyId: Int): Flow<List<DeletedFormData>>
    suspend fun deleteProjectForm(internalFormId: Int, companyId: Int)

    suspend fun getPreviousFormId(
        projectId: Int,
        formTypeId: Int,
        internalFormId: Int,
        companyId: Int
    ): Int?

    suspend fun formHasPrevious(
        projectId: Int,
        formTypeId: Int,
        internalFormId: Int,
        companyId: Int
    ): Boolean

    suspend fun deleteProjectForms(companyId: Int)
}
