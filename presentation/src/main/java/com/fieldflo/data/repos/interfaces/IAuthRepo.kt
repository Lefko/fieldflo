package com.fieldflo.data.repos.interfaces

import com.fieldflo.data.persistence.db.entities.CompanyData

interface IAuthRepo {
    suspend fun login(
        company: CompanyData,
        email: String,
        password: String,
        fcmToken: String,
        deviceId: String
    ): LoginResponse

    fun logout()
}

sealed class LoginResponse {
    data class Success(val companyId: Int) : LoginResponse()
    object InvalidPw : LoginResponse()
    object GeneralError : LoginResponse()
}
