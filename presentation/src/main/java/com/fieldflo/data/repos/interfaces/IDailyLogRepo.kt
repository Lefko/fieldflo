package com.fieldflo.data.repos.interfaces

import com.fieldflo.data.persistence.db.entities.DailyLogFormData
import java.util.*

interface IDailyLogRepo {
    suspend fun isFormClosable(internalFormId: Int, companyId: Int): Boolean
    suspend fun deleteForm(internalFormId: Int, companyId: Int)
    suspend fun syncFormClose(internalFormId: Int, closeDate: Date, companyId: Int)
    suspend fun getFormDataForLogging(internalFormId: Int, closeDate: Date, companyId: Int): String
    suspend fun getDailyLog(internalFormId: Int, companyId: Int): DailyLogFormData?
    suspend fun saveDailyLogForm(dailyLogSaveData: DailyLogFormData)
    suspend fun deleteDailyLogData(companyId: Int)
}
