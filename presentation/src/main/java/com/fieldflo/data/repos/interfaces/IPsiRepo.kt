package com.fieldflo.data.repos.interfaces

import com.fieldflo.data.persistence.db.entities.PsiEmployeeData
import com.fieldflo.data.persistence.db.entities.PsiFormData
import com.fieldflo.data.persistence.db.entities.PsiTaskData
import com.fieldflo.usecases.PsiEmployeeUiData
import java.util.*

interface IPsiRepo {
    suspend fun getPsiFormData(internalFormId: Int, companyId: Int): PsiFormData?
    suspend fun getPsiTasks(internalFormId: Int, companyId: Int): List<PsiTaskData>
    suspend fun getPsiEmployees(internalFormId: Int, companyId: Int): List<PsiEmployeeData>
    suspend fun savePsiFormData(psiFormData: PsiFormData)
    suspend fun sameAsPrevious(
        employees: List<PsiEmployeeUiData>,
        previousFormInternalFormId: Int,
        internalFormId: Int,
        companyId: Int
    )

    suspend fun isFormClosable(internalFormId: Int, companyId: Int): Boolean
    suspend fun deleteForm(internalFormId: Int, companyId: Int)
    suspend fun syncFormClose(internalFormId: Int, closeDate: Date, companyId: Int)
    suspend fun getFormDataForLogging(internalFormId: Int, closeDate: Date, companyId: Int): String
    suspend fun deleteForms(companyId: Int)
}
