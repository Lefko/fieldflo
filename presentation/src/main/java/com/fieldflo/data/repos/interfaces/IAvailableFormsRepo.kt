package com.fieldflo.data.repos.interfaces

import com.fieldflo.data.persistence.db.entities.AvailableForm

interface IAvailableFormsRepo {
    suspend fun replaceAvailableForms(projectId: Int, companyId: Int)
    suspend fun getAvailableForms(projectId: Int, companyId: Int): List<AvailableForm>
    suspend fun replaceAvailableForms(
        newAvailableForms: List<AvailableForm>,
        projectId: Int,
        companyId: Int
    )

    suspend fun deleteAvailableFormsOnProject(projectId: Int, companyId: Int)
    suspend fun deleteAvailableForms(companyId: Int)
}
