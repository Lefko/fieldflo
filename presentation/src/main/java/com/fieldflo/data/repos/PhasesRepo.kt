package com.fieldflo.data.repos

import com.fieldflo.data.api.endpoints.CompanyDataApi
import com.fieldflo.data.api.endpoints.GetByIdBody
import com.fieldflo.data.api.entities.PhaseRestObject
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.entities.Phase
import com.fieldflo.data.repos.interfaces.IPhasesRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PhasesRepo(
    db: FieldFloDatabase,
    private val companyDataApi: CompanyDataApi
): IPhasesRepo {

    private val phasesDao = db.getPhaseDao2()

    @Throws(Exception::class)
    override suspend fun loadPhases(companyId: Int) {
        withContext(Dispatchers.IO) {
            val phasesRest = companyDataApi.getAllPhases()
            val phases = phasesRest.map { it.toPhase(companyId) }
            phasesDao.addPhases(phases)
        }
    }

    @Throws(Exception::class)
    override suspend fun replaceAllPhases(companyId: Int) {
        withContext(Dispatchers.IO) {
            val newPhases = companyDataApi.getAllPhases().map { it.toPhase(companyId) }
            val currentPhases = phasesDao.getPhasesAllPhases(companyId)
            val phasesToSetDeleted =
                (currentPhases - newPhases).map { it.copy(phaseDeleted = true) }
            phasesDao.deleteAllPhases(companyId)
            phasesDao.addPhases(newPhases + phasesToSetDeleted)
        }
    }

    @Throws(Exception::class)
    override suspend fun loadNewPhase(phaseId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            val phasesRest = companyDataApi.getPhase(GetByIdBody(phaseId))
            if (phasesRest.size == 1) {
                val newPhase = phasesRest[0].toPhase(companyId)
                phasesDao.deletePhase(phaseId, companyId)
                phasesDao.addPhase(newPhase)
            }
        }
    }

    override suspend fun getPhaseById(phaseId: Int, companyId: Int): Phase? {
        return withContext(Dispatchers.IO) {
            phasesDao.getPhaseById(phaseId, companyId)
        }
    }

    override suspend fun getPhasesForProject(projectId: Int, companyId: Int): List<Phase> {
        return withContext(Dispatchers.IO) {
            phasesDao.getPhasesForProject(projectId, companyId)
        }
    }

    override suspend fun setPhaseDeleted(phaseId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            phasesDao.setPhaseDeleted(phaseId, companyId)
        }
    }

    override suspend fun deletePhases(companyId: Int) {
        withContext(Dispatchers.IO) {
            phasesDao.deleteAllPhases(companyId)
        }
    }
}

private fun PhaseRestObject.toPhase(companyId: Int) = Phase(
    phaseName = this.phaseName!!,
    phaseId = this.phaseId!!,
    phaseCode = this.phaseCode!!,
    projectId = this.projectId!!,
    phaseActive = this.phaseActive!!,
    phaseDeleted = this.phaseDeleted!!,
    companyId = companyId
)
