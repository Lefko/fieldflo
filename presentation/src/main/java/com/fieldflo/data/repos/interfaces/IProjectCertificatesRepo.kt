package com.fieldflo.data.repos.interfaces

import com.fieldflo.data.persistence.db.entities.ProjectCertificate
import kotlinx.coroutines.flow.Flow
import java.io.File

interface IProjectCertificatesRepo {
    suspend fun loadProjectCertificates(companyId: Int)
    suspend fun loadProjectCertificates(projectId: Int, companyId: Int)
    suspend fun saveProjectCerts(projectCerts: List<ProjectCertificate>)
    suspend fun deleteProjectCertificateFromProject(projectId: Int, companyId: Int)
    suspend fun deleteProjectCertificates(companyId: Int)

    suspend fun getEmployeesCertificates(projectId: Int, companyId: Int): List<ProjectCertificate>
    suspend fun printProjectCertificate(
        certificateIds: List<Int>,
        projectId: Int,
        companyId: Int
    ): Boolean

    fun getProjectCertsDir(projectId: Int, companyId: Int): Flow<List<File>>
    fun changeFileName(fileLocation: String, newName: String): Boolean
    fun deleteProjectCertificateFile(fileName: String, projectId: Int, companyId: Int): Boolean
    suspend fun deleteEmployeesCertificates(companyId: Int)
}