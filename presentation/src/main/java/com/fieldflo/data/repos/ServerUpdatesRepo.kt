package com.fieldflo.data.repos

import com.fieldflo.common.interval
import com.fieldflo.data.api.endpoints.AppSyncApi
import com.fieldflo.data.api.endpoints.CheckForModificationsBody
import com.fieldflo.data.api.endpoints.SetUpdatedBody
import com.fieldflo.data.api.entities.ModificationRestObject
import com.fieldflo.data.repos.interfaces.IServerUpdatesRepo
import com.fieldflo.data.sync.WorkScheduler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import timber.log.Timber

class ServerUpdatesRepo(
    private val workScheduler: WorkScheduler,
    private val appSyncApi: AppSyncApi
) : IServerUpdatesRepo {

    override fun hasPendingWorkFlow(): Flow<Boolean> {
        return interval(5_000, false)
            .map { workScheduler.hasPendingWork() }
    }

    override fun onNewFcmToken(fcmToken: String, deviceId: String) {
        workScheduler.updateFcmToken(fcmToken, deviceId)
    }

    override suspend fun getModifications(deviceId: String): List<ModificationRestObject> {
        return withContext(Dispatchers.IO) {
            try {
                appSyncApi.checkForModifications(CheckForModificationsBody(deviceId))
            } catch (e: Exception) {
                Timber.e(e)
                throw e
            }
        }
    }

    override suspend fun setUpdated(deviceId: String) {
        withContext(Dispatchers.IO) {
            appSyncApi.setUpdated(SetUpdatedBody(deviceId))
        }
    }

    override fun startServerSyncWorker() = workScheduler.startServerSyncWorker()
}
