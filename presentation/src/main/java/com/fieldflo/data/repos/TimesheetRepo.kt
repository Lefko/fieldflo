package com.fieldflo.data.repos

import com.fieldflo.common.roundToSecond
import com.fieldflo.data.api.endpoints.ManageFormsApi
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.entities.*
import com.fieldflo.data.persistence.fileSystem.FileManager
import com.fieldflo.data.repos.interfaces.ITimesheetRepo
import com.fieldflo.data.sync.WorkScheduler
import com.squareup.moshi.Moshi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.*

class TimesheetRepo(
    db: FieldFloDatabase,
    private val fileManager: FileManager,
    private val manageForms: ManageFormsApi,
    private val workScheduler: WorkScheduler,
    private val moshi: Moshi
) : ITimesheetRepo {

    private val timeSheetDao = db.getTimeSheetDao()
    private val timeSheetEmployeeDao = db.getTimeSheetEmployeeDao()
    private val timeSheetEmployeePositionDao = db.getTimeSheetEmployeePositionDao()
    private val timeSheetEmployeeAssetDao = db.getTimeSheetEmployeeAssetDao()

    override suspend fun sameAsPrevious(
        previousFormInternalFormId: Int,
        internalFormId: Int,
        perDiemAutoApply: Boolean,
        perDiemNote: String,
        companyId: Int
    ) {
        Timber.tag(TAG)
            .d("same as previous for internalFormId: $internalFormId (previousFormInternalFormId: $previousFormInternalFormId)")
        withContext(Dispatchers.IO) {
            timeSheetDao.getTimeSheetFormDataByInternalId(previousFormInternalFormId, companyId)
                ?.let { prevFormData ->
                    val previousEmployees =
                        timeSheetEmployeeDao.getTimeSheetEmployeesByInternalFormId(
                            previousFormInternalFormId,
                            companyId
                        )

                    deleteForm(internalFormId, companyId)

                    val newTimeSheetData = TimeSheetFormData(
                        companyId = companyId,
                        internalFormId = internalFormId,
                        projectId = prevFormData.projectId,
                        formDate = Date(),
                        supervisorId = prevFormData.supervisorId
                    )

                    val formDataId = timeSheetDao.addTimeSheetFormData(newTimeSheetData).toInt()
                    previousEmployees.forEach { prevEmployee ->
                        val prevEmployeePerDiem = prevEmployee.perDiem
                        val newPerDiem = if (prevEmployeePerDiem != null && perDiemAutoApply) {
                            prevEmployeePerDiem.copy(
                                totalWorkTime = 0,
                                notes = perDiemNote
                            )
                        } else {
                            null
                        }

                        val newEmployee = TimeSheetEmployee(
                            timeSheetFormDataId = formDataId,
                            companyId = companyId,
                            internalFormId = internalFormId,
                            projectId = newTimeSheetData.projectId,
                            employeeId = prevEmployee.employeeId,
                            clockIn = Date(0),
                            clockOut = Date(0),
                            employeeStatus = TimesheetEmployeeStatus.NOT_TRACKING,
                            perDiem = newPerDiem
                        )
                        val newTimeSheetEmployeeId =
                            timeSheetEmployeeDao.addTimeSheetEmployee(newEmployee).toInt()

                        prevEmployee.positions.firstOrNull { it.defaultPosition }
                            ?.let { prevDefaultPosition ->
                                val newPosition = TimesheetEmployeePosition(
                                    internalFormId = internalFormId,
                                    timeSheetFormDataId = formDataId,
                                    companyId = companyId,
                                    projectId = newEmployee.projectId,
                                    timeSheetEmployeeId = newTimeSheetEmployeeId,
                                    positionId = prevDefaultPosition.positionId,
                                    phaseId = prevDefaultPosition.phaseId,
                                    defaultPosition = true
                                )
                                timeSheetEmployeePositionDao.setTimeSheetEmployeeDefaultPosition(
                                    newPosition
                                )

                            }

                        prevEmployee.assets.forEach { prevAsset ->
                            val asset = TimeSheetEmployeeAsset(
                                companyId = companyId,
                                projectId = newEmployee.projectId,
                                internalFormId = newEmployee.internalFormId,
                                timeSheetFormDataId = formDataId,
                                timeSheetEmployeeId = newTimeSheetEmployeeId,
                                employeeId = newEmployee.employeeId,
                                itemId = prevAsset.itemId,
                                totalWorkTime = prevAsset.totalWorkTime
                            )
                            timeSheetDao.addTimeSheetEmployeeAsset(asset)
                        }
                    }
                }
        }
    }

    override suspend fun isFormClosable(internalFormId: Int, companyId: Int): Boolean {
        Timber.tag(TAG).d("Is form closable internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            val employees =
                timeSheetEmployeeDao.getTimeSheetEmployeesByInternalFormId(
                    internalFormId,
                    companyId
                )
            employees.isNotEmpty() && employees.all {
                it.employeeStatus.ordinal >= TimesheetEmployeeStatus.VERIFIED.ordinal
            }
        }
    }

    override suspend fun deleteForm(internalFormId: Int, companyId: Int) {
        Timber.tag(TAG).d("Delete form internalFormId: $internalFormId")
        withContext(Dispatchers.IO) {
            timeSheetDao.deleteAllTimeSheetFormData(internalFormId, companyId)
        }
    }

    override suspend fun syncFormClose(
        internalFormId: Int,
        closeDate: Date,
        companyId: Int
    ) {
        Timber.tag(TAG).d("Sync timesheet internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            val formData =
                timeSheetDao.getTimeSheetFormDataByInternalId(internalFormId, companyId)!!
            val requestEmployees =
                timeSheetEmployeeDao.getTimeSheetEmployeesByInternalFormId(
                    internalFormId,
                    companyId
                )
            formData.employees = requestEmployees
            val requestBody =
                ManageFormsApi.SaveTimeSheetRequestBody.fromTimesheetFormData(
                    formData,
                    closeDate
                )

            Timber.d("Save timesheet request body: $requestBody")
            val response = manageForms.saveTimeSheet(requestBody)
            formData.employees.forEach {
                uploadPinOutImage(
                    internalFormId,
                    formData.formDate,
                    it,
                    response.project2formId,
                    response.employeesMapping,
                    companyId
                )
            }
        }
    }

    override suspend fun getFormDataForLogging(
        internalFormId: Int,
        closeDate: Date,
        companyId: Int
    ): String {
        return withContext(Dispatchers.IO) {
            val formData =
                timeSheetDao.getTimeSheetFormDataByInternalId(internalFormId, companyId)!!
            val requestEmployees =
                timeSheetEmployeeDao.getTimeSheetEmployeesByInternalFormId(
                    internalFormId,
                    companyId
                )
            formData.employees = requestEmployees
            val requestBody =
                ManageFormsApi.SaveTimeSheetRequestBody.fromTimesheetFormData(
                    formData,
                    closeDate
                )
            moshi.adapter(ManageFormsApi.SaveTimeSheetRequestBody::class.java).toJson(requestBody)
        }
    }

    override suspend fun getTimeSheetEmployeesByInternalFormId(
        internalFormId: Int,
        companyId: Int
    ): List<TimeSheetEmployee> {
        Timber.tag(TAG).d("Get timesheet employee by internal form id: $internalFormId")
        return withContext(Dispatchers.IO) {
            timeSheetEmployeeDao.getTimeSheetEmployeesByInternalFormId(internalFormId, companyId)
        }
    }

    override suspend fun getTimeSheetEmployeesByProjectId(
        projectId: Int,
        companyId: Int
    ): List<TimeSheetEmployee> {
        Timber.tag(TAG).d("Get timesheet employee by project id: $projectId")
        return withContext(Dispatchers.IO) {
            timeSheetEmployeeDao.getTimeSheetEmployeesByProjectId(projectId, companyId)
        }
    }

    override suspend fun getTimeSheetDataFlow(
        projectId: Int,
        internalFormId: Int,
        companyId: Int
    ): Flow<TimeSheetFormData> {
        Timber.tag(TAG).d("Get timesheet data by internal form id: $internalFormId")
        withContext(Dispatchers.IO) {
            val hasData = timeSheetDao.hasTimeSheetFormData(internalFormId, companyId)
            if (!hasData) {
                val timesheetData = TimeSheetFormData(
                    companyId = companyId,
                    internalFormId = internalFormId,
                    projectId = projectId,
                    formDate = Date()
                )
                timeSheetDao.addTimeSheetFormData(timesheetData)
            }
        }
        return timeSheetDao.getTimesheetDataFlow(internalFormId, companyId)
    }

    override suspend fun getTimeSheetData(internalFormId: Int, companyId: Int): TimeSheetFormData {
        Timber.tag(TAG).d("Get timesheet data by internal form id: $internalFormId")
        return withContext(Dispatchers.IO) {
            timeSheetDao.getTimeSheetData(internalFormId, companyId)
        }
    }

    override fun getTimesheetEmployeesFlow(
        internalFormId: Int,
        companyId: Int
    ): Flow<List<TimeSheetEmployee>> {
        Timber.tag(TAG).d("Get timesheet employees by internal form id: $internalFormId")
        return timeSheetEmployeeDao.getTimeSheetEmployeesByInternalFormIdFlow(
            internalFormId,
            companyId
        )
    }

    override suspend fun isEmployeeClockedInOnAnyTimesheet(
        employeeId: Int,
        companyId: Int
    ): Boolean {
        Timber.tag(TAG).d("Is employee clocked in. employeeId: $employeeId")
        return withContext(Dispatchers.IO) {
            timeSheetEmployeeDao.isEmployeeClockedIn(employeeId, companyId)
        }
    }

    override suspend fun isEmployeeOnProjectTimeSheet(
        projectId: Int,
        employeeId: Int,
        companyId: Int
    ): Boolean {
        return withContext(Dispatchers.IO) {
            val isEmployeeOnProjectTimeSheet =
                timeSheetEmployeeDao.isEmployeeOnProjectTimeSheet(projectId, employeeId, companyId)
            Timber.d("isEmployeeOnProjectTimeSheet: $isEmployeeOnProjectTimeSheet")
            isEmployeeOnProjectTimeSheet
        }
    }

    override suspend fun addEmployeesToTimeSheet(
        employees: List<Employee>,
        internalFormId: Int,
        companyId: Int
    ): List<TimeSheetEmployee> {
        Timber.tag(TAG)
            .d("Add employees to timesheet. Employees: ${employees.joinToString { it.employeeFullName }}")
        return withContext(Dispatchers.IO) {
            val formData = timeSheetDao.getTimeSheetFormDataByInternalId(internalFormId, companyId)
            if (formData != null) {
                val existingTimeSheetEmployees =
                    timeSheetEmployeeDao.getTimeSheetEmployeesByInternalFormId(
                        internalFormId,
                        companyId
                    )
                val timeSheetEmployeesToAdd = employees.map {
                    TimeSheetEmployee(
                        timeSheetFormDataId = formData.formDataId,
                        companyId = companyId,
                        internalFormId = internalFormId,
                        projectId = formData.projectId,
                        employeeId = it.employeeId,
                        employeeStatus = TimesheetEmployeeStatus.NOT_TRACKING,
                        clockIn = Date(0),
                        clockOut = Date(0),
                    )
                }
                timeSheetEmployeeDao.addTimeSheetEmployees(timeSheetEmployeesToAdd)

                val updateTimeSheetEmployees =
                    timeSheetEmployeeDao.getTimeSheetEmployeesByInternalFormId(
                        internalFormId,
                        companyId
                    )

                updateTimeSheetEmployees - existingTimeSheetEmployees
            } else {
                emptyList()
            }
        }
    }

    override suspend fun removeEmployeeFromTimesheet(timesheetEmployeeId: Int, companyId: Int) {
        Timber.tag(TAG)
            .d("Remove employee from timeSheet. TimeSheetEmployeeId: $timesheetEmployeeId")
        withContext(Dispatchers.IO) {
            timeSheetEmployeeDao.removeEmployeeFromTimeSheet(timesheetEmployeeId, companyId)
        }
    }

    override suspend fun updateSupervisor(employeeId: Int, internalFormId: Int, companyId: Int) {
        Timber.tag(TAG).d("Update supervisor on timesheet internalFormId: $internalFormId")
        withContext(Dispatchers.IO) {
            timeSheetDao.updateTimeSheetSupervisor(employeeId, internalFormId, companyId)
        }
    }

    override suspend fun setEmployeeDefaultPosition(
        timeSheetEmployeeId: Int,
        position: Position,
        companyId: Int
    ) {
        Timber.tag(TAG).d("Set timeSheet employee default position")
        withContext(Dispatchers.IO) {
            val defaultPosition =
                timeSheetEmployeePositionDao.getPositionsForTimeSheetEmployee(
                    timeSheetEmployeeId,
                    companyId
                )
                    .find { it.defaultPosition }
            if (defaultPosition != null && defaultPosition.positionId != position.positionId) {
                val allPositions = timeSheetEmployeePositionDao.getPositionsForTimeSheetEmployee(
                    timeSheetEmployeeId, companyId
                )
                if (allPositions.none { it.positionId == position.positionId }) {
                    val updatedPosition = defaultPosition.copy(positionId = position.positionId)
                    timeSheetEmployeePositionDao.updateTimeSheetEmployeePosition(updatedPosition)
                }
            } else {
                timeSheetEmployeeDao.getTimeSheetEmployeeById(timeSheetEmployeeId, companyId)
                    ?.let { timeSheetEmployee ->
                        val timeSheetEmployeePosition = TimesheetEmployeePosition(
                            internalFormId = timeSheetEmployee.internalFormId,
                            timeSheetFormDataId = timeSheetEmployee.timeSheetFormDataId,
                            companyId = companyId,
                            projectId = position.projectId,
                            timeSheetEmployeeId = timeSheetEmployeeId,
                            positionId = position.positionId,
                            defaultPosition = true
                        )
                        timeSheetEmployeePositionDao.setTimeSheetEmployeeDefaultPosition(
                            timeSheetEmployeePosition
                        )
                    }
            }
            Unit
        }
    }

    override suspend fun setEmployeePhase(phaseId: Int, timeSheetEmployeeId: Int, companyId: Int) {
        Timber.tag(TAG).d("Set timeSheet employee phase")
        withContext(Dispatchers.IO) {
            val positions = timeSheetEmployeePositionDao.getPositionsForTimeSheetEmployee(
                timeSheetEmployeeId,
                companyId
            )
            positions.find { it.defaultPosition }
                ?.let {
                    timeSheetEmployeePositionDao.setPhaseToTimeSheetPosition(
                        phaseId,
                        it.timeSheetEmployeePositionId,
                        companyId
                    )
                }
        }
    }

    override suspend fun getEmployeesById(
        timeSheetEmployeeIds: List<Int>,
        companyId: Int
    ): List<TimeSheetEmployee> {
        Timber.tag(TAG)
            .d("Get timeSheet employees by time sheet employee ids: ${timeSheetEmployeeIds.joinToString()}")
        return withContext(Dispatchers.IO) {
            timeSheetEmployeeDao.getTimeSheetEmployees(timeSheetEmployeeIds, companyId)
        }
    }

    override suspend fun startTrackingEmployees(
        timesheetEmployeeIds: List<Int>,
        companyId: Int
    ) {
        Timber.tag(TAG)
            .d("Start tracking timeSheet employees with time sheet employee ids: ${timesheetEmployeeIds.joinToString()}")
        withContext(Dispatchers.IO) {
            val now = Date().roundToSecond()
            timeSheetEmployeeDao.startTrackingEmployees(timesheetEmployeeIds, companyId, now)
        }
    }

    override suspend fun deleteTimeSheetEmployeeAsset(
        timeSheetEmployeeAssetId: Int,
        companyId: Int
    ) {
        timeSheetEmployeeAssetDao.deleteTimeSheetEmployeeAsset(timeSheetEmployeeAssetId, companyId)
    }

    override suspend fun verifyEmployees(
        timesheetEmployeeIds: List<Int>,
        verifiedByEmployeeId: Int,
        injured: Boolean,
        tookBreak: Boolean,
        companyId: Int
    ) {
        Timber.tag(TAG)
            .d("Verify timeSheet employees with time sheet employee ids: ${timesheetEmployeeIds.joinToString()}")
        withContext(Dispatchers.IO) {
            if (injured) {
                timeSheetEmployeeDao.verifyInjuredEmployees(
                    timesheetEmployeeIds,
                    verifiedByEmployeeId,
                    tookBreak,
                    companyId
                )
            } else {
                timeSheetEmployeeDao.verifyEmployees(
                    timesheetEmployeeIds,
                    verifiedByEmployeeId,
                    tookBreak,
                    companyId
                )
            }
        }
    }

    override suspend fun getTimeSheetSupervisorId(internalFormId: Int, companyId: Int): Int {
        Timber.tag(TAG)
            .d("Get time sheet supervisor on time sheet with internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            timeSheetDao.getTimeSheetSupervisorId(internalFormId, companyId)
        }
    }

    override suspend fun getTimeSheetNotes(internalFormId: Int, companyId: Int): String {
        Timber.tag(TAG).d("Get time sheet notes internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            timeSheetDao.getTimeSheetNotes(internalFormId, companyId)
        }
    }

    override suspend fun saveTimeSheetNotes(internalFormId: Int, newNotes: String, companyId: Int) {
        Timber.tag(TAG)
            .d("Save new time sheet notes (\"$newNotes\") internalFormId: $internalFormId")
        withContext(Dispatchers.IO) {
            timeSheetDao.updateTimeSheetNotes(internalFormId, newNotes, companyId)
        }
    }

    override suspend fun getEmployeeById(
        timeSheetEmployeeId: Int,
        companyId: Int
    ): TimeSheetEmployee? {
        Timber.tag(TAG).d("Get time sheet employee by timeSheetEmployeeId: $timeSheetEmployeeId")
        return withContext(Dispatchers.IO) {
            timeSheetEmployeeDao.getTimeSheetEmployeeById(timeSheetEmployeeId, companyId)
        }
    }

    override fun getEmployeeByIdFlow(
        timeSheetEmployeeId: Int,
        companyId: Int
    ): Flow<TimeSheetEmployee> {
        Timber.tag(TAG)
            .d("Get time sheet employee flow by timeSheetEmployeeId: $timeSheetEmployeeId")
        return timeSheetEmployeeDao.getTimeSheetEmployeeByIdFlow(timeSheetEmployeeId, companyId)
    }

    override suspend fun getEmployeePositions(
        timeSheetEmployeeId: Int,
        companyId: Int
    ): List<TimesheetEmployeePosition> {
        Timber.tag(TAG)
            .d("Get time sheet employee positions by timeSheetEmployeeId: $timeSheetEmployeeId")
        return withContext(Dispatchers.IO) {
            timeSheetEmployeePositionDao.getPositionsForTimeSheetEmployee(
                timeSheetEmployeeId,
                companyId
            )
        }
    }

    override suspend fun updateEmployees(employees: List<TimeSheetEmployee>) {
        Timber.tag(TAG).d("Update timeSheet employees")
        withContext(Dispatchers.IO) {
            employees.forEach {
                timeSheetEmployeePositionDao.replaceEmployeePositionData(
                    it.timeSheetEmployeeId,
                    it.positions,
                    it.companyId
                )
            }
            timeSheetEmployeeDao.updateTimeSheetEmployees(employees)
        }
    }

    override suspend fun setPerDiem(
        timeSheetEmployeeId: Int,
        perDiem: TimeSheetEmployeePerDiem,
        companyId: Int
    ) {
        Timber.tag(TAG).d("Set time sheet employee per diem")
        withContext(Dispatchers.IO) {
            timeSheetEmployeeDao.updatePerDiemData(
                perDiem.perDiemType,
                perDiem.dailyAmount,
                perDiem.hourlyAmount,
                perDiem.cashGivenOnSite,
                perDiem.totalWorkTime,
                perDiem.notes,
                perDiem.totalEarned,
                perDiem.totalOwed,
                timeSheetEmployeeId,
                companyId
            )
        }
    }

    override suspend fun deletePerDiem(timeSheetEmployeeId: Int, companyId: Int) {
        Timber.tag(TAG).d("Delete time sheet employee per diem")
        withContext(Dispatchers.IO) {
            val timeSheetEmployee =
                timeSheetEmployeeDao.getTimeSheetEmployeeById(timeSheetEmployeeId, companyId)
                    ?: return@withContext
            val updatedTimeSheetEmployee = timeSheetEmployee.copy(perDiem = null)
            timeSheetEmployeeDao.updateTimeSheetEmployee(updatedTimeSheetEmployee)
        }
    }

    override suspend fun updatePositions(
        timeSheetEmployeeId: Int,
        positions: List<TimesheetEmployeePosition>,
        companyId: Int
    ) {
        Timber.tag(TAG).d("Update time sheet employee positions")
        withContext(Dispatchers.IO) {
            timeSheetEmployeePositionDao.replaceEmployeePositionData(
                timeSheetEmployeeId,
                positions,
                companyId
            )
        }
    }

    override suspend fun addAsset(newAsset: TimeSheetEmployeeAsset) {
        Timber.tag(TAG).d("Add new time sheet employee asset: $newAsset")
        withContext(Dispatchers.IO) {
            timeSheetEmployeeAssetDao.addAsset(newAsset)
        }
    }

    override suspend fun editAsset(editedAsset: TimeSheetEmployeeAsset) {
        Timber.tag(TAG).d("Add new time sheet employee asset: $editedAsset")
        withContext(Dispatchers.IO) {
            timeSheetEmployeeAssetDao.editAsset(editedAsset)
        }
    }

    override suspend fun deleteTimeSheetData(companyId: Int) {
        withContext(Dispatchers.IO) {
            timeSheetDao.deleteAllTimeSheetFormData(companyId)
        }
    }

    private fun uploadPinOutImage(
        internalFormId: Int,
        timeSheetFormDate: Date,
        employee: TimeSheetEmployee,
        project2formId: Int,
        employeesMapping: List<ManageFormsApi.EmployeeMapping>,
        companyId: Int
    ) {
        Timber.d("Upload time sheet employee pin")
        val fieldFloTimesheetEmployeeId = employeesMapping.firstOrNull {
            it.externalEmployeeId == employee.timeSheetEmployeeId.toString()
        }?.fieldFloId ?: 0

        val imageFile = fileManager.getTimesheetPhotoPinFileLocation(
            companyId = companyId,
            projectId = employee.projectId,
            timeSheetEmployeeId = employee.timeSheetEmployeeId,
            timeSheetDate = timeSheetFormDate,
            internalFormId = internalFormId
        )

        if (imageFile.exists()) {
            workScheduler.uploadTimesheetEmployeePinOutImage(
                imageLocation = imageFile.absolutePath,
                project2formId = project2formId,
                fieldFloTimesheetEmployeeId = fieldFloTimesheetEmployeeId
            )
        }
    }
}

private const val TAG = "TimesheetRepo"