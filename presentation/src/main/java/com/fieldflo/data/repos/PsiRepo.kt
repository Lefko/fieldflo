package com.fieldflo.data.repos

import com.fieldflo.data.api.endpoints.ManageFormsApi
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.entities.PsiEmployeeData
import com.fieldflo.data.persistence.db.entities.PsiFormData
import com.fieldflo.data.persistence.db.entities.PsiTaskData
import com.fieldflo.data.persistence.fileSystem.FileManager
import com.fieldflo.data.repos.interfaces.IPsiRepo
import com.fieldflo.data.sync.WorkScheduler
import com.fieldflo.usecases.PsiEmployeeUiData
import com.squareup.moshi.Moshi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.*

class PsiRepo(
    db: FieldFloDatabase,
    private val manageForms: ManageFormsApi,
    private val fileManager: FileManager,
    private val workScheduler: WorkScheduler,
    private val moshi: Moshi
) : IPsiRepo {

    private val psiDao = db.getPsiDao2()

    override suspend fun getPsiFormData(internalFormId: Int, companyId: Int): PsiFormData? {
        Timber.tag("PsiRepo").d("Get psi form data internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            psiDao.getPsiFormByInternalFormId(internalFormId, companyId)
        }
    }

    override suspend fun getPsiTasks(internalFormId: Int, companyId: Int): List<PsiTaskData> {
        Timber.tag("PsiRepo").d("Get psi tasks internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            psiDao.getPsiTasks(internalFormId, companyId)
        }
    }

    override suspend fun getPsiEmployees(
        internalFormId: Int,
        companyId: Int
    ): List<PsiEmployeeData> {
        Timber.tag("PsiRepo").d("Get psi employees internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            psiDao.getPsiEmployees(internalFormId, companyId)
        }
    }

    override suspend fun savePsiFormData(psiFormData: PsiFormData) {
        Timber.tag("PsiRepo").d("Save psi form data internalFormId: ${psiFormData.internalFormId}")
        withContext(Dispatchers.IO) {
            val savedData =
                psiDao.getPsiFormByInternalFormId(psiFormData.internalFormId, psiFormData.companyId)
            if (savedData == null) {
                // this is a first save
                val formDataId = psiDao.savePsiFormData(psiFormData).toInt()
                val employees = psiFormData.employees.map { it.copy(formDataId = formDataId) }
                val tasks = psiFormData.tasks.map { it.copy(formDataId = formDataId) }
                psiDao.savePsiEmployees(
                    psiFormData.internalFormId,
                    psiFormData.companyId,
                    employees
                )
                psiDao.savePsiTasks(psiFormData.internalFormId, psiFormData.companyId, tasks)
            } else {
                psiDao.updateFormData(psiFormData.copy(formDataId = savedData.formDataId))
                psiDao.savePsiTasks(
                    psiFormData.internalFormId,
                    psiFormData.companyId,
                    psiFormData.tasks.map { it.copy(formDataId = savedData.formDataId) }
                )
                psiDao.savePsiEmployees(
                    psiFormData.internalFormId,
                    psiFormData.companyId,
                    psiFormData.employees.map { it.copy(formDataId = savedData.formDataId) }
                )
            }
        }
    }

    override suspend fun sameAsPrevious(
        employees: List<PsiEmployeeUiData>,
        previousFormInternalFormId: Int,
        internalFormId: Int,
        companyId: Int
    ) {
        Timber.tag("PsiRepo")
            .d("same as previous for internalFormId: $internalFormId (previousFormInternalFormId: $previousFormInternalFormId)")
        withContext(Dispatchers.IO) {
            val previousPsiData =
                psiDao.getFullPsiFormByInternalFormId(previousFormInternalFormId, companyId)

            deleteForm(internalFormId, companyId)

            previousPsiData?.let { previousPsiForm ->
                val prevTasks = previousPsiForm.tasks
                val newPsi = previousPsiForm.copy(
                    formDataId = 0,
                    internalFormId = internalFormId,
                    formDate = Date(),
                    supervisorVerified = false,
                    supervisorVerifiedDate = Date(0),
                    supervisorVerifiedByName = "",
                    auditorPinVerified = false,
                    auditorVerifiedDate = Date(0),
                    auditorVerifiedByName = "",
                    employees = emptyList(),
                    reviewedWeather = null,
                    reviewedRoad = null,
                    reviewedHeatIndex = null,
                    tasks = emptyList(),
                )
                val formDataId = psiDao.savePsiFormData(newPsi).toInt()
                val newTasks = prevTasks.map {
                    it.copy(
                        psiTaskId = 0,
                        internalFormId = internalFormId,
                        projectId = previousPsiForm.projectId,
                        formDataId = formDataId
                    )
                }
                psiDao.savePsiTasks(internalFormId, companyId, newTasks)

                val newEmployees = employees.map {
                    PsiEmployeeData(
                        psiEmployeeId = 0,
                        companyId = companyId,
                        projectId = previousPsiForm.projectId,
                        internalFormId = internalFormId,
                        formDataId = formDataId,

                        employeeId = it.employeeId,
                        isPinVerified = it.pinEmployeeId != 0,
                        pinVerifiedByEmployeeId = it.pinEmployeeId,
                        pinVerifiedByName = it.pinUserName,
                        pinDate = it.pinDate
                    )
                }
                psiDao.savePsiEmployees(internalFormId, companyId, newEmployees)
            }
        }
    }

    override suspend fun isFormClosable(internalFormId: Int, companyId: Int): Boolean {
        Timber.tag("PsiRepo").d("Is form closable internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            psiDao.getFullPsiFormByInternalFormId(internalFormId, companyId)?.let { psiFormData ->
                psiFormData.taskLocation.isNotEmpty() &&
                        psiFormData.musterMeetingPoint.isNotEmpty() &&
                        psiFormData.reviewedWeather ?: false &&
                        psiFormData.reviewedRoad ?: false &&
                        psiFormData.reviewedHeatIndex ?: false &&
                        psiFormData.supervisorVerified &&
                        (psiFormData.auditorId == 0 || psiFormData.auditorPinVerified) &&
                        psiFormData.tasks.isNotEmpty() &&
                        psiFormData.employees.all { it.isPinVerified } &&
                        psiFormData.tasks.all { it.task.isNotEmpty() && it.hazard.isNotEmpty() && it.control.isNotEmpty() }
            } ?: false
        }
    }

    override suspend fun deleteForm(internalFormId: Int, companyId: Int) {
        Timber.tag("PsiRepo").d("Delete psi form internalFormId: $internalFormId")
        withContext(Dispatchers.IO) {
            psiDao.deleteAllPsiFormData(internalFormId, companyId)
        }
    }

    override suspend fun syncFormClose(
        internalFormId: Int,
        closeDate: Date,
        companyId: Int
    ) {
        Timber.tag("PsiRepo").d("Sync psi form internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            val psiFormData = psiDao.getFullPsiFormByInternalFormId(internalFormId, companyId)!!
            val request =
                ManageFormsApi.SavePsiRequestBody.fromPsiFormData(psiFormData, closeDate)
            Timber.d("Save PSI request body: $request")
            val response = manageForms.savePsi(request)
            psiFormData.employees.forEach {
                uploadPinOutImage(
                    internalFormId,
                    psiFormData.insertDate,
                    it,
                    response.project2formId,
                    response.employeesMapping,
                    companyId
                )
            }
        }
    }

    override suspend fun getFormDataForLogging(
        internalFormId: Int,
        closeDate: Date,
        companyId: Int
    ): String {
        return withContext(Dispatchers.IO) {
            val psiFormData = psiDao.getFullPsiFormByInternalFormId(internalFormId, companyId)!!
            val request =
                ManageFormsApi.SavePsiRequestBody.fromPsiFormData(psiFormData, closeDate)
            moshi.adapter(ManageFormsApi.SavePsiRequestBody::class.java).toJson(request)
        }
    }

    override suspend fun deleteForms(companyId: Int) {
        withContext(Dispatchers.IO) {
            psiDao.deleteAllPsiFormData(companyId)
        }
    }

    private fun uploadPinOutImage(
        internalFormId: Int,
        insertDate: Date,
        employee: PsiEmployeeData,
        project2formId: Int,
        employeesMapping: List<ManageFormsApi.EmployeeMapping>,
        companyId: Int
    ) {
        Timber.tag("PsiPin")
            .d("uploadPinOutImage(internalFormId = $internalFormId, insertDate = $insertDate, employee = $employee, project2formId = $project2formId, employeesMapping = $employeesMapping, companyId = $companyId")
        val imageFile = fileManager.getPsiPhotoPinFileLocation(
            companyId = companyId,
            projectId = employee.projectId,
            employeeId = employee.employeeId,
            insertDate = insertDate,
            internalFormId = internalFormId
        )
        Timber.tag("PsiPin")
            .d("Psi pin out file ${imageFile.absolutePath} exists = ${imageFile.exists()}")

        val fieldFloPsiEmployeeId = employeesMapping.firstOrNull {
            it.externalEmployeeId == employee.psiEmployeeId.toString()
        }?.fieldFloId ?: 0

        Timber.tag("PsiPin").d("Upload PSI employee pin for $fieldFloPsiEmployeeId")
        if (imageFile.exists() && fieldFloPsiEmployeeId > 0) {
            workScheduler.uploadPsiEmployeePinOutImage(
                imageLocation = imageFile.absolutePath,
                fieldFloPsiEmployeeId = fieldFloPsiEmployeeId,
                project2formId = project2formId
            )
        }
    }
}