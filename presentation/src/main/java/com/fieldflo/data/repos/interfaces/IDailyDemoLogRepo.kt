package com.fieldflo.data.repos.interfaces

import com.fieldflo.data.persistence.db.entities.*
import java.util.*

interface IDailyDemoLogRepo {
    suspend fun isFormClosable(internalFormId: Int, companyId: Int): Boolean
    suspend fun getDailyDemoLog(internalFormId: Int, companyId: Int): DailyDemoLogFormData?
    suspend fun saveDailyDemoLogFormData(dailyDemoLogFormData: DailyDemoLogFormData)
    suspend fun getDailyDemoLogEmployees(
        internalFormId: Int,
        companyId: Int
    ): List<DailyDemoLogEmployee>

    suspend fun getDailyDemoLogSubContractors(
        internalFormId: Int,
        companyId: Int
    ): List<DailyDemoLogSubContractor>

    suspend fun getDailyDemoLogEquipment(
        internalFormId: Int,
        companyId: Int
    ): List<DailyDemoLogEquipment>

    suspend fun getDailyDemoLogMaterials(
        internalFormId: Int,
        companyId: Int
    ): List<DailyDemoLogMaterials>

    suspend fun getDailyDemoLogTrucking(
        internalFormId: Int,
        companyId: Int
    ): List<DailyDemoLogTrucking>

    suspend fun sameAsPrevious(previousFormInternalFormId: Int, internalFormId: Int, companyId: Int)
    suspend fun deleteForm(internalFormId: Int, companyId: Int)
    suspend fun syncFormClose(internalFormId: Int, closeDate: Date, companyId: Int)
    suspend fun getFormDataForLogging(internalFormId: Int, closeDate: Date, companyId: Int): String
}