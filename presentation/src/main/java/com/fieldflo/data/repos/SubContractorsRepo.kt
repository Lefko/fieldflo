package com.fieldflo.data.repos

import com.fieldflo.data.api.endpoints.CompanyDataApi
import com.fieldflo.data.api.endpoints.GetByIdBody
import com.fieldflo.data.api.entities.SubContractorRestObject
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.entities.SubContractor
import com.fieldflo.data.repos.interfaces.ISubContractorsRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class SubContractorsRepo(
    db: FieldFloDatabase,
    private val companyDataApi: CompanyDataApi
) : ISubContractorsRepo {

    private val subContractorsDao = db.getSubContractors()

    override suspend fun loadSubContractors(companyId: Int) {
        withContext(Dispatchers.IO) {
            val subContractorsRest = companyDataApi.getAllSubContractors()
            val subContractors = subContractorsRest.map { it.toSubContractor(companyId) }
            subContractorsDao.addAllSubContractors(subContractors)
        }
    }

    override suspend fun replaceAllSubContractors(companyId: Int) {
        withContext(Dispatchers.IO) {
            val subContractorsRest = companyDataApi.getAllSubContractors()
            val subContractors = subContractorsRest.map { it.toSubContractor(companyId) }
            subContractorsDao.deleteAllSubContractors(companyId)
            subContractorsDao.addAllSubContractors(subContractors)
        }
    }

    override suspend fun loadNewSubcontractor(subcontractorId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            val subcontractorRest = companyDataApi.getSubContractor(GetByIdBody(subcontractorId))
            if (subcontractorRest.size == 1) {
                val newSubContractor = subcontractorRest[0].toSubContractor(companyId)
                subContractorsDao.deleteSubcontractor(subcontractorId, companyId)
                subContractorsDao.addSubcontractor(newSubContractor)
            }
        }
    }

    override suspend fun getAllSubContractors(companyId: Int): List<SubContractor> {
        Timber.tag("SubContractorsRepo").d("Get all subContractors")
        return withContext(Dispatchers.IO) {
            subContractorsDao.getAllSubContractors(companyId)
        }
    }

    override suspend fun getSubcontractorById(
        subcontractorId: Int,
        companyId: Int
    ): SubContractor? {
        Timber.tag("SubContractorsRepo").d("Get subContractor by id")
        return withContext(Dispatchers.IO) {
            subContractorsDao.getSubContractorsById(subcontractorId, companyId)
        }
    }

    override suspend fun setSubcontractorDeleted(subcontractorId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            subContractorsDao.setSubcontractorDeleted(subcontractorId, companyId)
        }
    }

    override suspend fun deleteSubContractors(companyId: Int) {
        withContext(Dispatchers.IO) {
            subContractorsDao.deleteAllSubContractors(companyId)
        }
    }
}

private fun SubContractorRestObject.toSubContractor(companyId: Int) = SubContractor(
    subContractorId = subContractorId!!,
    companyId = companyId,
    name = name!!
)