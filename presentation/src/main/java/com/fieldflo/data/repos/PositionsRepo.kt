package com.fieldflo.data.repos

import com.fieldflo.data.api.endpoints.CompanyDataApi
import com.fieldflo.data.api.endpoints.GetByIdBody
import com.fieldflo.data.api.entities.PositionRestObject
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.entities.Position
import com.fieldflo.data.repos.interfaces.IPositionsRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class PositionsRepo(
    db: FieldFloDatabase,
    private val companyDataApi: CompanyDataApi
) : IPositionsRepo {

    private val positionsDao = db.getPositionDao2()

    @Throws(Exception::class)
    override suspend fun loadPositions(companyId: Int) {
        withContext(Dispatchers.IO) {
            val positionsRest = companyDataApi.getAllPositions()
            val positions = positionsRest.map { it.toPosition(companyId) }
            positionsDao.addPositions(positions)
        }
    }

    @Throws(Exception::class)
    override suspend fun replaceAllPositions(companyId: Int) {
        withContext(Dispatchers.IO) {
            val newPositions = companyDataApi.getAllPositions().map { it.toPosition(companyId) }
            val currentPositions = positionsDao.getAllPositions(companyId)
            val positionsToDelete =
                (currentPositions - newPositions).map { it.copy(positionDeleted = true) }
            positionsDao.deleteAllPositions(companyId)
            positionsDao.addPositions(newPositions + positionsToDelete)
        }
    }

    @Throws(Exception::class)
    override suspend fun loadNewPosition(positionId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            val newPositionsRest = companyDataApi.getPosition(GetByIdBody(positionId))
            if (newPositionsRest.size == 1) {
                val newPosition = newPositionsRest[0].toPosition(companyId)
                positionsDao.deletePosition(positionId, companyId)
                positionsDao.addPosition(newPosition)
            }
        }
    }

    @Throws(Exception::class)
    override suspend fun deletePosition(positionId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            positionsDao.setPositionDeleted(positionId, companyId)
        }
    }

    override suspend fun getPositionById(positionId: Int, companyId: Int): Position? {
        Timber.tag(TAG).d("Load Position by position id")
        return withContext(Dispatchers.IO) {
            positionsDao.getPosition(positionId, companyId)
        }
    }

    override suspend fun getPositionsForProject(projectId: Int, companyId: Int): List<Position> {
        Timber.tag(TAG).d("Get positions for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            positionsDao.loadPositionsByProjectId(projectId, companyId)
        }
    }

    override suspend fun deletePositions(companyId: Int) {
        withContext(Dispatchers.IO) {
            positionsDao.deleteAllPositions(companyId)
        }
    }
}

private const val TAG = "PositionsRepo"

private fun PositionRestObject.toPosition(companyId: Int) =
    Position(
        companyId = companyId,
        positionId = this.positionId!!,
        projectId = this.projectId!!,
        positionName = this.positionName!!,
        positionCode = this.positionCode!!,
        positionActive = this.positionActive!!,
        positionDeleted = this.positionDeleted!!
    )
