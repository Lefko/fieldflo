package com.fieldflo.data.repos

import com.fieldflo.data.api.endpoints.CompanyDataApi
import com.fieldflo.data.api.endpoints.GetByIdListBody
import com.fieldflo.data.api.entities.CompanyCertificateRestObject
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.entities.CompanyCertificate
import com.fieldflo.data.repos.interfaces.ICompanyCertificatesRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CompanyCertificatesRepo(
    db: FieldFloDatabase,
    private val companyDataApi: CompanyDataApi
) : ICompanyCertificatesRepo {

    private val certsDao = db.getCompanyCertificateDao2()

    override suspend fun loadCompanyCertificates(companyId: Int) {
        withContext(Dispatchers.IO) {
            val certs = companyDataApi.getAllCertificates().map { it.toCompanyCert(companyId) }
            certsDao.addAllCompanyCertificates(certs)
        }
    }

    override suspend fun replaceAllCerts(companyId: Int) {
        withContext(Dispatchers.IO) {
            val certs = companyDataApi.getAllCertificates().map { it.toCompanyCert(companyId) }
            certsDao.deleteCompanyCerts(companyId)
            certsDao.addAllCompanyCertificates(certs)
        }
    }


    override suspend fun loadNewCert(certId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            val certs = companyDataApi.getCertificates(GetByIdListBody(intArrayOf(certId)))
                .map { it.toCompanyCert(companyId) }
            if (certs.size == 1) {
                val newCert = certs[0]
                certsDao.deleteCompanyCert(certId, companyId)
                certsDao.addCompanyCertificate(newCert)
            }
        }
    }

    override suspend fun deleteCompanyCert(certId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            certsDao.deleteCompanyCert(certId, companyId)
        }
    }

    override suspend fun deleteCompanyCertificates(companyId: Int) {
        withContext(Dispatchers.IO) {
            certsDao.deleteCompanyCerts(companyId)
        }
    }

    override suspend fun getCompanyCertificatesByEmployeeIds(
        employeeIds: List<Int>,
        companyId: Int
    ): List<CompanyCertificate> {
        return withContext(Dispatchers.IO) {
            certsDao.getCompanyCertificatesByEmployeeIds(employeeIds, companyId)
        }
    }
}

private fun CompanyCertificateRestObject.toCompanyCert(companyId: Int) = CompanyCertificate(
    companyId = companyId,
    certificateId = this.certificateId!!,
    employeeId = this.employeeId!!,
    certificateName = this.certificateName!!,
    certificateExpirationDate = this.certificateExpirationDate!!,
    certificateFileUrl = this.certificateFileUrl!!
)
