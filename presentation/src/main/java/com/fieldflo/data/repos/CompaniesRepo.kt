package com.fieldflo.data.repos

import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.daos.CompanyDao
import com.fieldflo.data.persistence.db.entities.CompanyData
import com.fieldflo.data.repos.interfaces.ICompaniesRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext

class CompaniesRepo(db: FieldFloDatabase) : ICompaniesRepo {

    private val companiesDao: CompanyDao = db.getCompanyDao()

    override suspend fun addCompany(
        companyName: String,
        baseUrl: String,
        token: String,
        securityKey: String
    ) {
        withContext(Dispatchers.IO) {
            companiesDao.addCompany(
                CompanyData(
                    companyName = companyName,
                    url = baseUrl,
                    token = token,
                    securityKey = securityKey
                )
            )
        }
    }

    override suspend fun deleteCompany(securityKey: String, token: String) {
        withContext(Dispatchers.IO) {
            companiesDao.deleteCompany(securityKey, token)
        }
    }

    override suspend fun didInitialCompanyLoad(securityKey: String, token: String): Boolean {
        return withContext(Dispatchers.IO) {
            val loadComplete = companiesDao.didInitialCompanyLoad(securityKey, token)
            loadComplete == true
        }
    }

    override suspend fun setInitialCompanyLoadComplete(companyId: Int) {
        withContext(Dispatchers.IO) {
            companiesDao.setInitialCompanyLoadComplete(companyId)
        }
    }

    override fun getCompaniesFlow(): Flow<List<CompanyData>> {
        return companiesDao.getBasicCompanies()
    }

    override suspend fun getCompanyBySecurityKeyAndToken(
        securityKey: String,
        token: String
    ): CompanyData? {
        return withContext(Dispatchers.IO) {
            companiesDao.getBasicCompanyBySecurityKeyAndToken(securityKey, token)
        }
    }

    override suspend fun getAllCompanies(): List<CompanyData> {
        return withContext(Dispatchers.IO) {
            companiesDao.getAllCompanies()
        }
    }

    override suspend fun updateAllCompanies(companies: List<CompanyData>) {
        withContext(Dispatchers.IO) {
            companiesDao.updateAllCompanies(companies)
        }
    }
}
