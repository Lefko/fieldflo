package com.fieldflo.data.repos.interfaces

import com.fieldflo.data.persistence.db.entities.InventoryItem

interface IInventoryItemsRepo {
    @Throws(Exception::class)
    suspend fun loadInventoryItems(companyId: Int)
    suspend fun loadNewInventoryItem(inventoryItemId: Int, companyId: Int)
    suspend fun replaceAllInventoryItems(companyId: Int)
    suspend fun getEquipmentInventoryItems(companyId: Int): List<InventoryItem>
    suspend fun getInventoryItem(inventoryItemId: Int, companyId: Int): InventoryItem?
    suspend fun deleteInventoryItem(inventoryItemId: Int, companyId: Int)
    suspend fun deleteInventoryItems(companyId: Int)
}
