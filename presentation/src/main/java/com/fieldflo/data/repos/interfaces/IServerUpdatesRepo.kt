package com.fieldflo.data.repos.interfaces

import com.fieldflo.data.api.entities.ModificationRestObject
import kotlinx.coroutines.flow.Flow

interface IServerUpdatesRepo {
    fun onNewFcmToken(fcmToken: String, deviceId: String)
    fun hasPendingWorkFlow(): Flow<Boolean>
    suspend fun getModifications(deviceId: String): List<ModificationRestObject>
    suspend fun setUpdated(deviceId: String)
    fun startServerSyncWorker()
}
