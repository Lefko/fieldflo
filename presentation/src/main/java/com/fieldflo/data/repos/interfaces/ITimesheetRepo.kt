package com.fieldflo.data.repos.interfaces

import com.fieldflo.data.persistence.db.entities.*
import kotlinx.coroutines.flow.Flow
import java.util.*

interface ITimesheetRepo {
    suspend fun sameAsPrevious(
        previousFormInternalFormId: Int,
        internalFormId: Int,
        perDiemAutoApply: Boolean,
        perDiemNote: String,
        companyId: Int
    )

    suspend fun isFormClosable(internalFormId: Int, companyId: Int): Boolean
    suspend fun deleteForm(internalFormId: Int, companyId: Int)
    suspend fun syncFormClose(internalFormId: Int, closeDate: Date, companyId: Int)
    suspend fun getFormDataForLogging(internalFormId: Int, closeDate: Date, companyId: Int): String
    suspend fun getTimeSheetEmployeesByInternalFormId(
        internalFormId: Int,
        companyId: Int
    ): List<TimeSheetEmployee>

    suspend fun getTimeSheetEmployeesByProjectId(
        projectId: Int,
        companyId: Int
    ): List<TimeSheetEmployee>

    suspend fun getTimeSheetDataFlow(
        projectId: Int,
        internalFormId: Int,
        companyId: Int
    ): Flow<TimeSheetFormData>

    suspend fun getTimeSheetData(internalFormId: Int, companyId: Int): TimeSheetFormData

    suspend fun getTimeSheetSupervisorId(internalFormId: Int, companyId: Int): Int
    suspend fun getTimeSheetNotes(internalFormId: Int, companyId: Int): String
    suspend fun saveTimeSheetNotes(internalFormId: Int, newNotes: String, companyId: Int)

    fun getTimesheetEmployeesFlow(
        internalFormId: Int,
        companyId: Int
    ): Flow<List<TimeSheetEmployee>>

    suspend fun isEmployeeClockedInOnAnyTimesheet(employeeId: Int, companyId: Int): Boolean
    suspend fun isEmployeeOnProjectTimeSheet(
        projectId: Int,
        employeeId: Int,
        companyId: Int
    ): Boolean

    /**
     * Adds employees to the timesheet.
     *
     * @return List of created TimeSheetEmployees
     */
    suspend fun addEmployeesToTimeSheet(
        employees: List<Employee>,
        internalFormId: Int,
        companyId: Int
    ): List<TimeSheetEmployee>

    suspend fun removeEmployeeFromTimesheet(timesheetEmployeeId: Int, companyId: Int)
    suspend fun updateSupervisor(employeeId: Int, internalFormId: Int, companyId: Int)
    suspend fun setEmployeeDefaultPosition(
        timeSheetEmployeeId: Int,
        position: Position,
        companyId: Int
    )

    suspend fun setEmployeePhase(
        phaseId: Int,
        timeSheetEmployeeId: Int,
        companyId: Int
    )

    suspend fun getEmployeeById(timeSheetEmployeeId: Int, companyId: Int): TimeSheetEmployee?
    fun getEmployeeByIdFlow(timeSheetEmployeeId: Int, companyId: Int): Flow<TimeSheetEmployee>
    suspend fun getEmployeesById(
        timeSheetEmployeeIds: List<Int>,
        companyId: Int
    ): List<TimeSheetEmployee>

    suspend fun startTrackingEmployees(timesheetEmployeeIds: List<Int>, companyId: Int)
    suspend fun verifyEmployees(
        timesheetEmployeeIds: List<Int>,
        verifiedByEmployeeId: Int,
        injured: Boolean,
        tookBreak: Boolean,
        companyId: Int
    )

    suspend fun getEmployeePositions(
        timeSheetEmployeeId: Int,
        companyId: Int
    ): List<TimesheetEmployeePosition>

    suspend fun deleteTimeSheetEmployeeAsset(timeSheetEmployeeAssetId: Int, companyId: Int)
    suspend fun updateEmployees(employees: List<TimeSheetEmployee>)
    suspend fun setPerDiem(
        timeSheetEmployeeId: Int,
        perDiem: TimeSheetEmployeePerDiem,
        companyId: Int
    )

    suspend fun deletePerDiem(timeSheetEmployeeId: Int, companyId: Int)
    suspend fun updatePositions(
        timeSheetEmployeeId: Int,
        positions: List<TimesheetEmployeePosition>,
        companyId: Int
    )

    suspend fun addAsset(newAsset: TimeSheetEmployeeAsset)
    suspend fun editAsset(editedAsset: TimeSheetEmployeeAsset)

    suspend fun deleteTimeSheetData(companyId: Int)
}
