package com.fieldflo.data.repos.interfaces

import com.fieldflo.data.persistence.db.entities.DailyFieldReportFormData
import java.io.File
import java.util.*

interface IDailyFieldReportRepo {
    suspend fun isFormClosable(internalFormId: Int, companyId: Int): Boolean
    suspend fun deleteForm(internalFormId: Int, companyId: Int)
    suspend fun syncFormClose(internalFormId: Int, closeDate: Date, companyId: Int)
    suspend fun getFormDataForLogging(internalFormId: Int, closeDate: Date, companyId: Int): String

    suspend fun getDailyFieldReport(internalFormId: Int, companyId: Int): DailyFieldReportFormData?
    suspend fun saveDailyForm(dailyFieldReportFormData: DailyFieldReportFormData)
    fun dailyFieldReportPicsDirFiles(
        internalFormId: Int,
        projectId: Int,
        companyId: Int
    ): List<File>

    suspend fun deleteDailyFieldReportData(companyId: Int)
}
