package com.fieldflo.data.repos

import com.fieldflo.data.api.endpoints.CompanyDataApi
import com.fieldflo.data.api.entities.TaskRestObject
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.entities.Task
import com.fieldflo.data.repos.interfaces.ITasksRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class TasksRepo(
    db: FieldFloDatabase,
    private val companyDataApi: CompanyDataApi
) : ITasksRepo {

    private val tasksDao = db.getTaskDao2()

    @Throws(Exception::class)
    override suspend fun loadTasks(companyId: Int) {
        withContext(Dispatchers.IO) {
            val tasksRest = companyDataApi.getAllTasks()
            val tasks = tasksRest.map { it.toTask(companyId) }
            tasksDao.addAllTasks(tasks)
        }
    }

    @Throws(Exception::class)
    override suspend fun replaceAllTasks(companyId: Int) {
        withContext(Dispatchers.IO) {
            val tasksRest = companyDataApi.getAllTasks()
            val tasks = tasksRest.map { it.toTask(companyId) }
            tasksDao.deleteAllTasks(companyId)
            tasksDao.addAllTasks(tasks)
        }
    }

    override suspend fun getAllTasks(companyId: Int): List<Task> {
        Timber.tag("TasksRepo").d("Get all tasks")
        return withContext(Dispatchers.IO) {
            tasksDao.getAllTasks(companyId)
        }
    }

    override suspend fun deleteTasks(companyId: Int) {
        withContext(Dispatchers.IO) {
            tasksDao.deleteAllTasks(companyId)
        }
    }
}

private fun TaskRestObject.toTask(companyId: Int) = Task(
    companyId = companyId,
    taskId = this.taskId!!,
    task = this.task!!,
    hazard = this.hazard!!,
    control = this.control!!,
    risk = this.risk!!
)
