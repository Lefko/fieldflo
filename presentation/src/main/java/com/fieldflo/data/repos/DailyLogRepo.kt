package com.fieldflo.data.repos

import com.fieldflo.data.api.endpoints.ManageFormsApi
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.entities.DailyLogFormData
import com.fieldflo.data.repos.interfaces.IDailyLogRepo
import com.squareup.moshi.Moshi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.*

class DailyLogRepo(
    db: FieldFloDatabase,
    private val manageForms: ManageFormsApi,
    private val moshi: Moshi
) : IDailyLogRepo {

    private val dailyLogDao = db.getDailyLogDao2()

    override suspend fun getDailyLog(
        internalFormId: Int,
        companyId: Int
    ): DailyLogFormData? {
        Timber.tag("DailyLogRepo").d("Get daily field report by internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            dailyLogDao.getDailyLogByInternalFormId(internalFormId, companyId)
        }
    }

    override suspend fun saveDailyLogForm(dailyLogSaveData: DailyLogFormData) {
        Timber.tag("DailyLogRepo").d("Save daily field report data")
        withContext(Dispatchers.IO) {
            val internalFormId = dailyLogSaveData.internalFormId
            val companyId = dailyLogSaveData.companyId
            val savedData = dailyLogDao.getDailyLogByInternalFormId(internalFormId, companyId)
            if (savedData == null) {
                dailyLogDao.addDailyLog(dailyLogSaveData)
            } else {
                dailyLogDao.updateDailyLog(dailyLogSaveData.copy(formDataId = savedData.formDataId))
            }
        }
    }

    override suspend fun isFormClosable(internalFormId: Int, companyId: Int): Boolean {
        Timber.tag("DailyLogRepo").d("Is form closable internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            dailyLogDao.isDailyLogClosable(internalFormId, companyId) == true
        }
    }

    override suspend fun deleteForm(internalFormId: Int, companyId: Int) {
        Timber.tag("DailyLogRepo").d("Delete form internalFormId: $internalFormId")
        withContext(Dispatchers.IO) {
            dailyLogDao.deleteDailyLog(internalFormId, companyId)
        }
    }

    override suspend fun syncFormClose(
        internalFormId: Int,
        closeDate: Date,
        companyId: Int
    ) {
        Timber.tag("DailyLogRepo").d("Sync daily log internalFormId: $internalFormId")
        withContext(Dispatchers.IO) {
            val dailyLogFormData =
                dailyLogDao.getDailyLogByInternalFormId(internalFormId, companyId)!!
            val request = ManageFormsApi.SaveDailyLogRequestBody.fromDailyLogFormData(
                dailyLogFormData,
                closeDate
            )
            manageForms.saveDailyLog(request)
        }
    }

    override suspend fun getFormDataForLogging(
        internalFormId: Int,
        closeDate: Date,
        companyId: Int
    ): String {
        return withContext(Dispatchers.IO) {
            val dailyLogFormData =
                dailyLogDao.getDailyLogByInternalFormId(internalFormId, companyId)!!
            val request = ManageFormsApi.SaveDailyLogRequestBody.fromDailyLogFormData(
                dailyLogFormData,
                closeDate
            )
            moshi.adapter(ManageFormsApi.SaveDailyLogRequestBody::class.java).toJson(request)
        }
    }

    override suspend fun deleteDailyLogData(companyId: Int) {
        withContext(Dispatchers.IO) {
            dailyLogDao.deleteAllDailyLogs(companyId)
        }
    }
}