package com.fieldflo.data.repos

import com.fieldflo.data.api.endpoints.ManageFormsApi
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.entities.ActiveFormData
import com.fieldflo.data.persistence.db.entities.ClosedFormData
import com.fieldflo.data.persistence.db.entities.DeletedFormData
import com.fieldflo.data.persistence.db.entities.ProjectForm
import com.fieldflo.data.repos.interfaces.IProjectFormsRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.withContext
import timber.log.Timber

class ProjectFormsRepo(
    db: FieldFloDatabase,
    private val manageFormsApi: ManageFormsApi
) : IProjectFormsRepo {

    private val projectFormDao = db.getProjectFormsDao2()

    override fun hasUnclosedFormsFlow(companyId: Int): Flow<Boolean> {
        return projectFormDao.hasUnsyncedFormsFlow(companyId).flowOn(Dispatchers.IO)
    }

    override suspend fun hasUnclosedForms(companyId: Int): Boolean {
        return withContext(Dispatchers.IO) {
            projectFormDao.hasUnsyncedForms(companyId)
        }
    }

    override suspend fun isFormSynced(internalFormId: Int, companyId: Int): Boolean {
        return withContext(Dispatchers.IO) {
            projectFormDao.isFormSynced(internalFormId, companyId) == true
        }
    }

    override suspend fun getUnsyncedClosedForms(companyId: Int): List<ProjectForm> {
        return withContext(Dispatchers.IO) {
            projectFormDao.getUnsyncedClosedForms(companyId)
        }
    }

    override suspend fun setHasSyncError(projectForm: ProjectForm) {
        withContext(Dispatchers.IO) {
            val updated = projectForm.copy(hasSyncError = true)
            projectFormDao.updateProjectForm(updated)
        }
    }

    override suspend fun reopenForm(internalFormId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            projectFormDao.reopenForm(internalFormId, companyId)
        }
    }

    override suspend fun logSyncError(
        formDataForLogging: String,
        formTypeId: Int,
        formName: String
    ) {
        withContext(Dispatchers.IO) {
            try {
                manageFormsApi.logSyncError(
                    ManageFormsApi.LogSyncErrorBody(
                        log = formDataForLogging,
                        formTypeId = formTypeId.toString(),
                        formName = formName
                    )
                )
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    override suspend fun getActiveProjectForms(
        projectId: Int,
        companyId: Int
    ): List<ActiveFormData> {
        return withContext(Dispatchers.IO) {
            Timber.tag("ProjectFormsRepo").d("Get active forms for project: $projectId")
            projectFormDao.getActiveProjectForms(projectId, companyId)
        }
    }

    override fun getClosedProjectForms(projectId: Int, companyId: Int): Flow<List<ClosedFormData>> {
        Timber.tag("ProjectFormsRepo").d("Get closed forms for project: $projectId")
        return projectFormDao.getClosedProjectFormsFlow(projectId, companyId).flowOn(Dispatchers.IO)
    }

    override fun getActiveProjectFormsFlow(
        projectId: Int,
        companyId: Int
    ): Flow<List<ActiveFormData>> {
        Timber.tag("ProjectFormsRepo").d("Get active forms for project: $projectId")
        return projectFormDao.getActiveProjectFormsFlow(projectId, companyId).flowOn(Dispatchers.IO)
    }

    override suspend fun closeProjectForm(internalFormId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            Timber.tag("ProjectFormsRepo").d("Closing project form: internalFormId=$internalFormId")
            projectFormDao.closeProjectForm(internalFormId, companyId)
        }
    }

    override suspend fun setProjectClosedSynced(internalFormId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            Timber.tag("ProjectFormsRepo").d("Setting close synced: internalFormId=$internalFormId")
            projectFormDao.setCloseSynced(internalFormId, companyId)
        }
    }

    override fun getDeletedProjectFormsFlow(
        projectId: Int,
        companyId: Int
    ): Flow<List<DeletedFormData>> {
        Timber.tag("ProjectFormsRepo").d("Get deleted forms for project: $projectId")
        return projectFormDao.getDeletedProjectFormsFlow(projectId, companyId)
            .flowOn(Dispatchers.IO)
    }

    override suspend fun deleteProjectForm(internalFormId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            Timber.tag("ProjectFormsRepo")
                .d("Setting project form deleted internalFormId: $internalFormId")
            projectFormDao.deleteProjectForm(internalFormId, companyId)
        }
    }

    override suspend fun getProjectForm(internalFormId: Int, companyId: Int): ProjectForm? {
        return withContext(Dispatchers.IO) {
            Timber.tag("ProjectFormsRepo").d("Get project form internalFormId: $internalFormId")
            projectFormDao.getProjectFormById(internalFormId, companyId)
        }
    }

    override suspend fun triggerFormUpdate(internalFormId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            projectFormDao.getProjectFormById(internalFormId, companyId)?.let { form ->
                projectFormDao.updateProjectForm(form)
            }
        }
    }

    override suspend fun addFormsToProject(projectForms: List<ProjectForm>, companyId: Int) {
        withContext(Dispatchers.IO) {
            Timber.tag("ProjectFormsRepo").d("Add forms to project: $projectForms")
            projectFormDao.addProjectForms(projectForms)
        }
    }

    override suspend fun getPreviousFormId(
        projectId: Int,
        formTypeId: Int,
        internalFormId: Int,
        companyId: Int
    ): Int? {
        return withContext(Dispatchers.IO) {
            Timber.tag("ProjectFormsRepo").d("getPreviousFormInternalFormId")
            projectFormDao.getPreviousFormInternalFormId(
                projectId,
                formTypeId,
                internalFormId,
                companyId
            )
        }
    }

    override suspend fun formHasPrevious(
        projectId: Int,
        formTypeId: Int,
        internalFormId: Int,
        companyId: Int
    ): Boolean {
        return withContext(Dispatchers.IO) {
            getPreviousFormId(projectId, formTypeId, internalFormId, companyId) != null
        }
    }

    override suspend fun getProjectForms(projectId: Int, companyId: Int): List<ProjectForm> {
        return withContext(Dispatchers.IO) {
            projectFormDao.getProjectForms(projectId, companyId)
        }
    }

    override suspend fun deleteProjectForms(companyId: Int) {
        withContext(Dispatchers.IO) {
            projectFormDao.deleteProjectForms(companyId)
        }
    }
}
