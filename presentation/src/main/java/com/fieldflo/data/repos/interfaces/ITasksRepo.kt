package com.fieldflo.data.repos.interfaces

import com.fieldflo.data.persistence.db.entities.Task

interface ITasksRepo {
    @Throws(Exception::class)
    suspend fun loadTasks(companyId: Int)
    suspend fun replaceAllTasks(companyId: Int)

    suspend fun getAllTasks(companyId: Int): List<Task>
    suspend fun deleteTasks(companyId: Int)
}
