package com.fieldflo.data.repos

import com.fieldflo.common.FileFlow
import com.fieldflo.data.api.endpoints.CertsApi
import com.fieldflo.data.api.endpoints.CompanyDataApi
import com.fieldflo.data.api.endpoints.FileDownloader
import com.fieldflo.data.api.endpoints.GetByProjectIdBody
import com.fieldflo.data.api.entities.ProjectCertificateRestObject
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.entities.ProjectCertificate
import com.fieldflo.data.persistence.fileSystem.FileManager
import com.fieldflo.data.repos.interfaces.IProjectCertificatesRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.File

class ProjectCertificatesRepo(
    db: FieldFloDatabase,
    private val companyDataApi: CompanyDataApi,
    private val certApi: CertsApi,
    private val fileManager: FileManager,
    private val filesDownloader: FileDownloader
) : IProjectCertificatesRepo {

    private val projectCertificatesDao = db.getProjectCertificatesDao2()

    override suspend fun loadProjectCertificates(companyId: Int) {
        withContext(Dispatchers.IO) {
            val projectCertsRest = companyDataApi.getProjectCertificates()
            val projectCerts = projectCertsRest.map { it.toProjectCert(companyId) }
            projectCertificatesDao.addAllProjectCertificates(projectCerts)
        }
    }

    override suspend fun loadProjectCertificates(projectId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            val newCerts = companyDataApi.getProjectCertificatesById(GetByProjectIdBody(projectId))
                .map { it.toProjectCert(companyId) }

            projectCertificatesDao.deleteProjectCertificateFromProject(projectId, companyId)
            projectCertificatesDao.addAllProjectCertificates(newCerts)
        }
    }

    override suspend fun saveProjectCerts(projectCerts: List<ProjectCertificate>) {
        withContext(Dispatchers.IO) {
            projectCertificatesDao.addAllProjectCertificates(projectCerts)
        }
    }

    override suspend fun deleteProjectCertificateFromProject(projectId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            projectCertificatesDao.deleteProjectCertificateFromProject(projectId, companyId)
        }
    }

    override suspend fun deleteProjectCertificates(companyId: Int) {
        withContext(Dispatchers.IO) {
            projectCertificatesDao.deleteProjectCertificates(companyId)
        }
    }

    override suspend fun getEmployeesCertificates(
        projectId: Int,
        companyId: Int
    ): List<ProjectCertificate> {
        Timber.tag("EmployeesCertificateRepo").d("Get certs for projectId: $projectId")
        return withContext(Dispatchers.IO) {
            projectCertificatesDao.getProjectCertificatesByProjectId(projectId, companyId)
        }
    }

    override suspend fun printProjectCertificate(
        certificateIds: List<Int>,
        projectId: Int,
        companyId: Int
    ): Boolean {
        return withContext(Dispatchers.IO) {
            try {
                val certResponse =
                    certApi.printCertificate(CertsApi.PrintCertificateRequestBody(certificateIds))
                val dest = fileManager.getProjectCertsFile(
                    companyId,
                    projectId,
                    certResponse.fileName ?: ""
                )
                filesDownloader.downloadFile(certResponse.fullUrl ?: "", dest)
            } catch (e: Exception) {
                Timber.tag("EmployeesCertificateRepo").e(e, "print cert error")
                false
            }
        }
    }

    override fun getProjectCertsDir(projectId: Int, companyId: Int): Flow<List<File>> {
        val projectCertsDir = fileManager.getProjectCertDir(companyId, projectId)
        return FileFlow.observeFile(projectCertsDir)
            .onEach { Timber.d("Dir: $it") }
            .filter { it.isDirectory }
            .map { (it.listFiles()?.toList() ?: emptyList()).filter { it.isFile } }
    }

    override fun changeFileName(fileLocation: String, newName: String): Boolean {
        return fileManager.changeFileName(fileLocation, newName)
    }

    override fun deleteProjectCertificateFile(
        fileName: String,
        projectId: Int,
        companyId: Int
    ): Boolean {
        val projectCert = fileManager.getProjectCertsFile(companyId, projectId, fileName)
        return if (projectCert.exists()) {
            projectCert.delete()
        } else {
            true
        }
    }

    override suspend fun deleteEmployeesCertificates(companyId: Int) {
        withContext(Dispatchers.IO) {
            projectCertificatesDao.deleteProjectCertificates(companyId)
        }
    }
}

private fun ProjectCertificateRestObject.toProjectCert(companyId: Int) = ProjectCertificate(
    companyId = companyId,
    certificateId = this.certificateId!!,
    projectId = this.projectId,
    employeeId = this.employeeId!!,
    certificateName = this.certificateName!!,
    certificateExpirationDate = this.certificateExpirationDate!!
)
