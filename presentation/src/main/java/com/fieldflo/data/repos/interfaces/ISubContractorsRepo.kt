package com.fieldflo.data.repos.interfaces

import com.fieldflo.data.persistence.db.entities.SubContractor

interface ISubContractorsRepo {
    @Throws(Exception::class)
    suspend fun loadSubContractors(companyId: Int)
    suspend fun replaceAllSubContractors(companyId: Int)
    suspend fun loadNewSubcontractor(subcontractorId: Int, companyId: Int)

    suspend fun getAllSubContractors(companyId: Int): List<SubContractor>
    suspend fun getSubcontractorById(subcontractorId: Int, companyId: Int): SubContractor?
    suspend fun setSubcontractorDeleted(subcontractorId: Int, companyId: Int)
    suspend fun deleteSubContractors(companyId: Int)
}