package com.fieldflo.data.repos.interfaces

import com.fieldflo.data.persistence.db.entities.Employee

interface IEmployeesRepo {
    @Throws(Exception::class)
    suspend fun loadEmployees(companyId: Int)
    suspend fun loadNewEmployee(employeeId: Int, companyId: Int)
    suspend fun replaceAllEmployees(companyId: Int)
    suspend fun getEmployeeById(employeeId: Int, companyId: Int): Employee?
    suspend fun getAllEmployees(companyId: Int): List<Employee>
    suspend fun setEmployeeDeleted(employeeId: Int, companyId: Int)
    suspend fun deleteEmployees(companyId: Int)
}
