package com.fieldflo.data.repos.interfaces

import com.fieldflo.data.persistence.db.entities.CompanyCertificate

interface ICompanyCertificatesRepo {
    @Throws(Exception::class)
    suspend fun loadCompanyCertificates(companyId: Int)

    @Throws(Exception::class)
    suspend fun replaceAllCerts(companyId: Int)

    @Throws(Exception::class)
    suspend fun loadNewCert(certId: Int, companyId: Int)
    suspend fun deleteCompanyCert(certId: Int, companyId: Int)
    suspend fun getCompanyCertificatesByEmployeeIds(
        employeeIds: List<Int>,
        companyId: Int
    ): List<CompanyCertificate>

    suspend fun deleteCompanyCertificates(companyId: Int)
}
