package com.fieldflo.data.repos.interfaces

import com.fieldflo.data.persistence.db.entities.Phase

interface IPhasesRepo {
    @Throws(Exception::class)
    suspend fun loadPhases(companyId: Int)
    suspend fun replaceAllPhases(companyId: Int)
    suspend fun loadNewPhase(phaseId: Int, companyId: Int)
    suspend fun getPhaseById(phaseId: Int, companyId: Int): Phase?
    suspend fun getPhasesForProject(projectId: Int, companyId: Int): List<Phase>
    suspend fun setPhaseDeleted(phaseId: Int, companyId: Int)
    suspend fun deletePhases(companyId: Int)
}
