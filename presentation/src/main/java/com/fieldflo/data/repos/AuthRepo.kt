package com.fieldflo.data.repos

import com.fieldflo.common.Analytics
import com.fieldflo.data.api.endpoints.AuthApi
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.entities.CompanyData
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.IAuthRepo
import com.fieldflo.data.repos.interfaces.LoginResponse
import retrofit2.HttpException
import timber.log.Timber

class AuthRepo(
    db: FieldFloDatabase,
    private val localStorage: ILocalStorage,
    private val authApi: AuthApi,
    private val analytics: Analytics
) : IAuthRepo {

    private val companyDao2 = db.getCompanyDao()

    override suspend fun login(
        company: CompanyData,
        email: String,
        password: String,
        fcmToken: String,
        deviceId: String
    ): LoginResponse {
        return try {
            val loginResponse =
                authApi.login(AuthApi.ApiLoginBody(email, password, deviceId, fcmToken))

            localStorage.setUserToken(loginResponse.userToken)
            localStorage.setUserEmail(email)
            localStorage.setUserPassword(password)
            localStorage.setCurrentCompanyId(loginResponse.companyId)
            companyDao2.setCompanyId(loginResponse.companyId, company.securityKey, company.token) // Im a bit unhappy about this, but am leaving for now
            val user = authApi.getLoggedInUser()
            localStorage.setEmployeeId(user.employeeId!!)
            localStorage.setEmployeeFirstName(user.employeeFirstName!!)
            localStorage.setEmployeeLastName(user.employeeLastName!!)
            localStorage.setEmployeePinNumber(user.employeePinNumber!!)
            localStorage.setEmployeePositionId(user.positionId!!)
            localStorage.setUserCanViewProjects(user.canViewProjects)
            localStorage.setUserCanAddEditOpenForms(user.canAddEditOpenForms)
            localStorage.setUserCanViewCerts(user.canViewCerts)
            localStorage.setUserCanEditCerts(user.canEditCerts)
            localStorage.setUserCanEditTimeSheetTime(user.canEditTimeSheetTime)
            localStorage.setUserCanValidateTimeSheetWithPin(user.canValidateTimeSheetWithPin)
            analytics.setUserId(user.employeeId.toString())
            analytics.setUserProperty(
                "User Name",
                "${user.employeeFirstName} ${user.employeeLastName}"
            )
            analytics.setUserProperty("Email", email)
            LoginResponse.Success(loginResponse.companyId)
        } catch (e: Exception) {
            Timber.e(e, "$e")
            logout()
            if (e is HttpException && e.code() == 401) {
                LoginResponse.InvalidPw
            } else {
                LoginResponse.GeneralError
            }
        }
    }

    override fun logout() {
        localStorage.clear()
        analytics.setUserId("")
        analytics.setUserProperty("User Name", "")
        analytics.setUserProperty("Email", "")
    }
}
