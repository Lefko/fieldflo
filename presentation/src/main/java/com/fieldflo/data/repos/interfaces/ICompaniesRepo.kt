package com.fieldflo.data.repos.interfaces

import com.fieldflo.data.persistence.db.entities.CompanyData
import kotlinx.coroutines.flow.Flow

interface ICompaniesRepo {
    suspend fun addCompany(companyName: String, baseUrl: String, token: String, securityKey: String)
    suspend fun deleteCompany(securityKey: String, token: String)
    suspend fun didInitialCompanyLoad(securityKey: String, token: String): Boolean
    suspend fun setInitialCompanyLoadComplete(companyId: Int)
    suspend fun getCompanyBySecurityKeyAndToken(securityKey: String, token: String): CompanyData?
    fun getCompaniesFlow(): Flow<List<CompanyData>>
    suspend fun getAllCompanies(): List<CompanyData>
    suspend fun updateAllCompanies(companies: List<CompanyData>)
}
