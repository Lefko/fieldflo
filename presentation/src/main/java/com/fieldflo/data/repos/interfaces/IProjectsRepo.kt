package com.fieldflo.data.repos.interfaces

import com.fieldflo.data.api.entities.ProjectRestObject
import com.fieldflo.data.persistence.db.entities.*
import kotlinx.coroutines.flow.Flow

interface IProjectsRepo {
    suspend fun loadProject(projectId: Int, companyId: Int): List<ProjectRestObject>
    suspend fun replaceProjectPermits(permits: List<ProjectPermit>, projectId: Int, companyId: Int)
    fun projectDeleted(projectId: Int, companyId: Int): Flow<Boolean>
    suspend fun getAllProjects(companyId: Int): List<BasicProjectData>

    fun getAllActiveProjectsFlow(companyId: Int): Flow<List<BasicProjectData>>

    suspend fun isProjectAutoApplyPerDiem(projectId: Int, companyId: Int): Boolean
    suspend fun getProjectName(projectId: Int, companyId: Int): String
    suspend fun getProjectNumber(projectId: Int, companyId: Int): String
    suspend fun isPhaseRequiredOnProject(projectId: Int, companyId: Int): Boolean
    suspend fun getProjectById(projectId: Int, companyId: Int): Project


    // Data for Project Details sheets
    suspend fun getProjectDetailsGeneralInfo(projectId: Int, companyId: Int): ProjectGeneralInfoData
    suspend fun getProjectDetails(projectId: Int, companyId: Int): ProjectDetailsData
    suspend fun getProjectSite(projectId: Int, companyId: Int): ProjectSiteData
    suspend fun getPermits(projectId: Int, companyId: Int): List<ProjectPermit>
    suspend fun getProjectPerDiemData(projectId: Int, companyId: Int): ProjectPerDiemData
    suspend fun getProjectBuildingInfo(
        projectId: Int,
        companyId: Int
    ): ProjectBuildingInformationData

    suspend fun getProjectOwnerData(projectId: Int, companyId: Int): ProjectOwnerData
    suspend fun getProjectProcedures(projectId: Int, companyId: Int): String
    suspend fun getProjectBillingInfo(projectId: Int, companyId: Int): ProjectBillingInfo
    suspend fun getProjectAirMonitoringData(
        projectId: Int,
        companyId: Int
    ): ProjectAirMonitoringData

    suspend fun getProjectConsultantData(projectId: Int, companyId: Int): ProjectConsultantData
    suspend fun getProjectLienInformation(projectId: Int, companyId: Int): ProjectLienInfo
    suspend fun getProjectEmergencyInfo(projectId: Int, companyId: Int): ProjectEmergencyInfo
    suspend fun getProjectInsuranceCertificateData(
        projectId: Int,
        companyId: Int
    ): ProjectInsuranceCertificateData

    suspend fun getProjectOtherData(projectId: Int, companyId: Int): ProjectOtherData
    suspend fun getProjectPayrollData(projectId: Int, companyId: Int): ProjectPayrollData
    suspend fun getProjectDesignerData(projectId: Int, companyId: Int): ProjectDesignerData


    suspend fun deleteProject(projectId: Int, companyId: Int)
    suspend fun deleteProjects(companyId: Int)
}
