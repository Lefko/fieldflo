package com.fieldflo.data.repos

import com.fieldflo.data.api.endpoints.ManageFormsApi
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.entities.*
import com.fieldflo.data.persistence.fileSystem.FileManager
import com.fieldflo.data.repos.interfaces.IDailyDemoLogRepo
import com.fieldflo.data.sync.WorkScheduler
import com.squareup.moshi.Moshi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.File
import java.util.*

class DailyDemoLogRepo(
    db: FieldFloDatabase,
    private val manageForms: ManageFormsApi,
    private val fileManager: FileManager,
    private val workScheduler: WorkScheduler,
    private val moshi: Moshi
) : IDailyDemoLogRepo {

    private val dailyDemoLogDao = db.getDailyDemoLogDao()

    override suspend fun isFormClosable(internalFormId: Int, companyId: Int): Boolean {
        Timber.tag("DailyDemoLogRepo").d("Is form closable internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            dailyDemoLogDao.isDailyDemoLogClosable(internalFormId, companyId) == true
        }
    }

    override suspend fun getDailyDemoLog(
        internalFormId: Int,
        companyId: Int
    ): DailyDemoLogFormData? {
        Timber.tag("DailyDemoLogRepo")
            .d("Get daily demo field report by internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            dailyDemoLogDao.getDailyDemoLogByInternalFormId(internalFormId, companyId)
        }
    }

    override suspend fun saveDailyDemoLogFormData(dailyDemoLogFormData: DailyDemoLogFormData) {
        Timber.tag("DailyDemoLogRepo")
            .d("Save daily Demo Log form data internalFormId: ${dailyDemoLogFormData.internalFormId}")
        withContext(Dispatchers.IO) {
            if (dailyDemoLogFormData.formDataId == 0) {
                dailyDemoLogDao.addDailyDemoLog(dailyDemoLogFormData)
            } else {
                dailyDemoLogDao.getDailyDemoLogByInternalFormId(
                    internalFormId = dailyDemoLogFormData.internalFormId,
                    companyId = dailyDemoLogFormData.companyId
                )?.let { currentData ->
                    if (currentData.pinOutLocation != dailyDemoLogFormData.pinOutLocation) {
                        File(currentData.pinOutLocation).delete()
                    }
                }

                dailyDemoLogDao.updateDailyDemoLog(dailyDemoLogFormData)

                dailyDemoLogDao.saveDemoLogEmployees(
                    dailyDemoLogFormData.internalFormId,
                    dailyDemoLogFormData.companyId,
                    dailyDemoLogFormData.dailyDemoLogEmployees
                )

                dailyDemoLogDao.saveDemoLogSubContractors(
                    dailyDemoLogFormData.internalFormId,
                    dailyDemoLogFormData.companyId,
                    dailyDemoLogFormData.dailyDemoLogSubContractors
                )

                dailyDemoLogDao.saveDemoLogeEquipment(
                    dailyDemoLogFormData.internalFormId,
                    dailyDemoLogFormData.companyId,
                    dailyDemoLogFormData.dailyDemoLogEquipment
                )

                dailyDemoLogDao.saveDemoLogeMaterials(
                    dailyDemoLogFormData.internalFormId,
                    dailyDemoLogFormData.companyId,
                    dailyDemoLogFormData.dailyDemoLogMaterials
                )

                dailyDemoLogDao.saveDemoLogeTrucking(
                    dailyDemoLogFormData.internalFormId,
                    dailyDemoLogFormData.companyId,
                    dailyDemoLogFormData.dailyDemoLogTrucking
                )
            }
        }
    }

    override suspend fun getDailyDemoLogEmployees(
        internalFormId: Int,
        companyId: Int
    ): List<DailyDemoLogEmployee> {
        Timber.tag("DailyDemoLogRepo")
            .d("Get Daily Demo Log Employees internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            dailyDemoLogDao.getDailyDemoLogEmployees(internalFormId, companyId)
        }
    }

    override suspend fun getDailyDemoLogSubContractors(
        internalFormId: Int,
        companyId: Int
    ): List<DailyDemoLogSubContractor> {
        Timber.tag("DailyDemoLogRepo")
            .d("Get Daily Demo Log SubContractors internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            val subContractors =
                dailyDemoLogDao.getDailyDemoLogSubContractors(internalFormId, companyId)
            subContractors
        }
    }

    override suspend fun getDailyDemoLogEquipment(
        internalFormId: Int,
        companyId: Int
    ): List<DailyDemoLogEquipment> {
        Timber.tag("DailyDemoLogRepo")
            .d("Get Daily Demo Log Equipment internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            dailyDemoLogDao.getDailyDemoLogEquipment(internalFormId, companyId)
        }
    }

    override suspend fun getDailyDemoLogMaterials(
        internalFormId: Int,
        companyId: Int
    ): List<DailyDemoLogMaterials> {
        Timber.tag("DailyDemoLogRepo")
            .d("Get Daily Demo Log Materials internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            dailyDemoLogDao.getDailyDemoLogMaterials(internalFormId, companyId)
        }
    }

    override suspend fun getDailyDemoLogTrucking(
        internalFormId: Int,
        companyId: Int
    ): List<DailyDemoLogTrucking> {
        Timber.tag("DailyDemoLogRepo")
            .d("Get Daily Demo Log Trucking internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            dailyDemoLogDao.getDailyDemoLogTrucking(internalFormId, companyId)
        }
    }

    override suspend fun sameAsPrevious(
        previousFormInternalFormId: Int,
        internalFormId: Int,
        companyId: Int
    ) {
        Timber.tag("DailyDemoLogRepo")
            .d("same as previous for internalFromId: $internalFormId (previousFormInternalFormId: $previousFormInternalFormId)")

        withContext(Dispatchers.IO) {
            val previousDailyDemoData =
                dailyDemoLogDao.getDailyDemoLogByInternalFormId(
                    previousFormInternalFormId,
                    companyId
                )

            deleteForm(internalFormId, companyId)

            previousDailyDemoData?.let { previousDailyDemoForm ->
                val newData = previousDailyDemoForm.copy(
                    formDataId = 0,
                    internalFormId = internalFormId,
                    supervisorVerified = false,
                    supervisorVerifiedDate = Date(0),
                    supervisorVerifiedByEmployeeId = 0,
                    reportableIncident = false,
                    incident = "",
                    incidentDescription = "",
                    stopWorkPerformed = false,
                    stopWorkReason = "",
                    stopWorkBeyondControlDescription = "",
                    stopWorkOutOfScopeDescription = "",
                    stopWorkBackChargeDescription = "",
                    stopWorkVisitorsDescription = "",
                    stopWorkIssuesDescription = ""
                )

                val formDataId = dailyDemoLogDao.addDailyDemoLog(newData).toInt()

                val employees =
                    dailyDemoLogDao.getDailyDemoLogEmployees(previousFormInternalFormId, companyId)
                        .map {
                            it.copy(
                                dailyDemoLogEmployeeId = 0,
                                internalFormId = internalFormId,
                                formDataId = formDataId
                            )
                        }
                val subcontractors =
                    dailyDemoLogDao.getDailyDemoLogSubContractors(
                        previousFormInternalFormId,
                        companyId
                    ).map {
                        it.copy(
                            dailyDemoLogSubContractorId = 0,
                            internalFormId = internalFormId,
                            formDataId = formDataId
                        )
                    }
                val equipment =
                    dailyDemoLogDao.getDailyDemoLogEquipment(previousFormInternalFormId, companyId)
                        .map {
                            it.copy(
                                dailyDemoLogEquipmentId = 0,
                                internalFormId = internalFormId,
                                formDataId = formDataId
                            )
                        }
                val materials =
                    dailyDemoLogDao.getDailyDemoLogMaterials(previousFormInternalFormId, companyId)
                        .map {
                            it.copy(
                                dailyDemoLogMaterialsId = 0,
                                internalFormId = internalFormId,
                                formDataId = formDataId
                            )
                        }
                val trucking =
                    dailyDemoLogDao.getDailyDemoLogTrucking(previousFormInternalFormId, companyId)
                        .map {
                            it.copy(
                                dailyDemoLogTruckingId = 0,
                                internalFormId = internalFormId,
                                formDataId = formDataId
                            )
                        }

                if (employees.isNotEmpty()) {
                    dailyDemoLogDao.saveDemoLogEmployees(internalFormId, companyId, employees)
                }

                if (subcontractors.isNotEmpty()) {
                    dailyDemoLogDao.saveDemoLogSubContractors(
                        internalFormId,
                        companyId,
                        subcontractors
                    )
                }

                if (equipment.isNotEmpty()) {
                    dailyDemoLogDao.saveDemoLogeEquipment(internalFormId, companyId, equipment)
                }

                if (materials.isNotEmpty()) {
                    dailyDemoLogDao.saveDemoLogeMaterials(internalFormId, companyId, materials)
                }

                if (trucking.isNotEmpty()) {
                    dailyDemoLogDao.saveDemoLogeTrucking(internalFormId, companyId, trucking)
                }
            }
        }
    }

    override suspend fun deleteForm(internalFormId: Int, companyId: Int) {
        Timber.tag("DailyDemoLogRepo").d("Delete daily demo log internalFormId: $internalFormId")
        withContext(Dispatchers.IO) {
            dailyDemoLogDao.getDailyDemoLogByInternalFormId(internalFormId, companyId)
                ?.let { savedData ->
                    File(savedData.pinOutLocation).delete()
                }

            dailyDemoLogDao.deleteAllDailyDemoFormData(internalFormId, companyId)
        }
    }

    override suspend fun getFormDataForLogging(
        internalFormId: Int,
        closeDate: Date,
        companyId: Int
    ): String {
        return withContext(Dispatchers.IO) {
            val dailyDemoLogData =
                dailyDemoLogDao.getDailyDemoLogByInternalFormId(internalFormId, companyId)!!
            dailyDemoLogData.dailyDemoLogEmployees =
                dailyDemoLogDao.getDailyDemoLogEmployees(internalFormId, companyId)
            dailyDemoLogData.dailyDemoLogSubContractors =
                dailyDemoLogDao.getDailyDemoLogSubContractors(internalFormId, companyId)
            dailyDemoLogData.dailyDemoLogEquipment =
                dailyDemoLogDao.getDailyDemoLogEquipment(internalFormId, companyId)
            dailyDemoLogData.dailyDemoLogMaterials =
                dailyDemoLogDao.getDailyDemoLogMaterials(internalFormId, companyId)
            dailyDemoLogData.dailyDemoLogTrucking =
                dailyDemoLogDao.getDailyDemoLogTrucking(internalFormId, companyId)
            val request =
                ManageFormsApi.SaveDailyDemoRequestBody.fromData(dailyDemoLogData, closeDate)

            moshi.adapter(ManageFormsApi.SaveDailyDemoRequestBody::class.java).toJson(request)
        }
    }

    override suspend fun syncFormClose(internalFormId: Int, closeDate: Date, companyId: Int) {
        Timber.tag("DailyDemoLogRepo").d("Sync daily demo log internalFormId: $internalFormId")
        withContext(Dispatchers.IO) {
            val dailyDemoLogData =
                dailyDemoLogDao.getDailyDemoLogByInternalFormId(internalFormId, companyId)!!
            dailyDemoLogData.dailyDemoLogEmployees =
                dailyDemoLogDao.getDailyDemoLogEmployees(internalFormId, companyId)
            dailyDemoLogData.dailyDemoLogSubContractors =
                dailyDemoLogDao.getDailyDemoLogSubContractors(internalFormId, companyId)
            dailyDemoLogData.dailyDemoLogEquipment =
                dailyDemoLogDao.getDailyDemoLogEquipment(internalFormId, companyId)
            dailyDemoLogData.dailyDemoLogMaterials =
                dailyDemoLogDao.getDailyDemoLogMaterials(internalFormId, companyId)
            dailyDemoLogData.dailyDemoLogTrucking =
                dailyDemoLogDao.getDailyDemoLogTrucking(internalFormId, companyId)

            val request =
                ManageFormsApi.SaveDailyDemoRequestBody.fromData(dailyDemoLogData, closeDate)
            Timber.tag("DailyDemoLogRepo").d("Save daily demo log request body: $request")
            val response = manageForms.saveDailyDemoLog(request)


            val pinOutImage = File(dailyDemoLogData.pinOutLocation)
            if (pinOutImage.exists() && pinOutImage.isFile) {
                workScheduler.uploadDailyDemoLogSupervisorPinOutImage(
                    imageLocation = pinOutImage.absolutePath,
                    project2formId = response.project2formId
                )
            }

            fileManager.getDailyDemoFormSavedImagesDir(
                companyId,
                dailyDemoLogData.projectId,
                internalFormId
            ).listFiles()?.forEach {
                workScheduler.uploadDailyDemoLogImage(it.absolutePath, response.project2formId)
            }
        }
    }
}