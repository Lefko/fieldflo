package com.fieldflo.data.repos

import com.fieldflo.data.api.endpoints.ManageFormsApi
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.entities.DailyFieldReportFormData
import com.fieldflo.data.persistence.fileSystem.FileManager
import com.fieldflo.data.persistence.fileSystem.IFileProvider
import com.fieldflo.data.repos.interfaces.IDailyFieldReportRepo
import com.fieldflo.data.sync.WorkScheduler
import com.squareup.moshi.Moshi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.File
import java.util.*

@ExperimentalCoroutinesApi
class DailyFieldReportRepo(
    db: FieldFloDatabase,
    private val manageForms: ManageFormsApi,
    private val fileManager: FileManager,
    private val workScheduler: WorkScheduler,
    private val moshi: Moshi
) : IDailyFieldReportRepo {

    private val dailyFieldReportDao = db.getDailyFieldReportDao2()

    override suspend fun isFormClosable(internalFormId: Int, companyId: Int): Boolean {
        Timber.tag("DailyFieldReportRepo").d("Is form closable internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            dailyFieldReportDao.isDailyFieldReportClosable(internalFormId, companyId) == true
        }
    }

    override suspend fun deleteForm(internalFormId: Int, companyId: Int) {
        Timber.tag("DailyFieldReportRepo").d("Delete form internalFormId: $internalFormId")
        withContext(Dispatchers.IO) {
            dailyFieldReportDao.deleteDailyFieldReport(internalFormId, companyId)
        }
    }

    override suspend fun syncFormClose(
        internalFormId: Int,
        closeDate: Date,
        companyId: Int
    ) {
        Timber.tag("DailyFieldReportRepo")
            .d("Sync daily field report internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            val dailyFieldReportFormData =
                dailyFieldReportDao.getDailyFieldReportDataByInternalFormId(
                    internalFormId,
                    companyId
                )!!
            val request =
                ManageFormsApi.SaveDailyFieldReportRequestBody.fromDailyFieldReportFormData(
                    dailyFieldReportFormData,
                    closeDate
                )
            Timber.d("Save daily field report request body: $request")
            val response = manageForms.saveDailyFieldReport(request)
            val savedFiles = fileManager.getDailyFieldReportSavedDir(
                companyId = companyId,
                projectId = dailyFieldReportFormData.projectId,
                internalFormId = internalFormId
            )
            (savedFiles.listFiles() ?: emptyArray())
                .filter { it.isFile && it.exists() }
                .forEach {
                    val imageNumber = IFileProvider.determineImageNumber(it)
                    workScheduler.uploadSavedDailyFieldReportImage(
                        project2formId = response.project2formId,
                        imageNumber = imageNumber,
                        imageLocation = it.absolutePath
                    )
                }
        }
    }

    override suspend fun getFormDataForLogging(
        internalFormId: Int,
        closeDate: Date,
        companyId: Int
    ): String {
        return withContext(Dispatchers.IO) {
            val dailyFieldReportFormData =
                dailyFieldReportDao.getDailyFieldReportDataByInternalFormId(
                    internalFormId,
                    companyId
                )!!
            val request =
                ManageFormsApi.SaveDailyFieldReportRequestBody.fromDailyFieldReportFormData(
                    dailyFieldReportFormData,
                    closeDate
                )
            moshi.adapter(ManageFormsApi.SaveDailyFieldReportRequestBody::class.java)
                .toJson(request)
        }
    }

    override suspend fun getDailyFieldReport(
        internalFormId: Int,
        companyId: Int
    ): DailyFieldReportFormData? {
        Timber.tag("DailyFieldReportRepo")
            .d("Get daily field report data internalFormId: $internalFormId")
        return withContext(Dispatchers.IO) {
            dailyFieldReportDao.getDailyFieldReportDataByInternalFormId(internalFormId, companyId)
        }
    }

    override suspend fun saveDailyForm(dailyFieldReportFormData: DailyFieldReportFormData) {
        Timber.tag("DailyFieldReportRepo").d("Save daily field report data")
        withContext(Dispatchers.IO) {
            val savedData = dailyFieldReportDao.getDailyFieldReportDataByInternalFormId(
                dailyFieldReportFormData.internalFormId,
                dailyFieldReportFormData.companyId
            )
            if (savedData == null) {
                dailyFieldReportDao.addDailyFieldReportData(dailyFieldReportFormData)
            } else {
                dailyFieldReportDao.updateDailyFieldReportDataSuspended(
                    dailyFieldReportFormData.copy(formDataId = savedData.formDataId)
                )
            }
        }
    }

    override fun dailyFieldReportPicsDirFiles(
        internalFormId: Int,
        projectId: Int,
        companyId: Int
    ): List<File> {
        Timber.tag("DailyFieldReportRepo")
            .d("Get daily field report pics internalFormId: $internalFormId")
        return listOf(
            fileManager.getDailyFieldReportPic(
                companyId,
                projectId,
                internalFormId,
                1
            ),
            fileManager.getDailyFieldReportPic(
                companyId,
                projectId,
                internalFormId,
                2
            ),
            fileManager.getDailyFieldReportPic(
                companyId,
                projectId,
                internalFormId,
                3
            ),
            fileManager.getDailyFieldReportPic(
                companyId,
                projectId,
                internalFormId,
                4
            ),
            fileManager.getDailyFieldReportPic(
                companyId,
                projectId,
                internalFormId,
                5
            ),
            fileManager.getDailyFieldReportPic(
                companyId,
                projectId,
                internalFormId,
                6
            )
        )
    }

    override suspend fun deleteDailyFieldReportData(companyId: Int) {
        withContext(Dispatchers.IO) {
            dailyFieldReportDao.deleteDailyFieldReport(companyId)
        }
    }
}