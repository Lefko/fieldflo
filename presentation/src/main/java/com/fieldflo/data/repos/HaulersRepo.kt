package com.fieldflo.data.repos

import com.fieldflo.data.api.endpoints.CompanyDataApi
import com.fieldflo.data.api.endpoints.GetByIdBody
import com.fieldflo.data.api.entities.HaulerRestObject
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.entities.Hauler
import com.fieldflo.data.repos.interfaces.IHaulersRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class HaulersRepo(
    db: FieldFloDatabase,
    private val companyDataApi: CompanyDataApi
) : IHaulersRepo {

    private val haulersDao = db.getHaulers()

    override suspend fun loadHaulers(companyId: Int) {
        withContext(Dispatchers.IO) {
            val haulersRest = companyDataApi.getAllHaulers()
            val haulers = haulersRest.map { it.toHauler(companyId) }
            haulersDao.addAllHaulers(haulers)
        }
    }

    override suspend fun replaceAllHaulers(companyId: Int) {
        withContext(Dispatchers.IO) {
            val haulersRest = companyDataApi.getAllHaulers()
            val haulers = haulersRest.map { it.toHauler(companyId) }
            haulersDao.deleteAllHaulers(companyId)
            haulersDao.addAllHaulers(haulers)
        }
    }

    override suspend fun loadNewHauler(haulerId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            val haulersRest = companyDataApi.getHauler(GetByIdBody(haulerId))
            if (haulersRest.size == 1) {
                val newHauler = haulersRest[0].toHauler(companyId)
                haulersDao.deleteHauler(haulerId, companyId)
                haulersDao.addHauler(newHauler)
            }
        }
    }

    override suspend fun setHaulerDeleted(haulerId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            haulersDao.setHaulerDeleted(haulerId, companyId)
        }
    }

    override suspend fun getAllHaulers(companyId: Int): List<Hauler> {
        Timber.tag("HaulersRepo").d("Get all haulers")
        return withContext(Dispatchers.IO) {
            haulersDao.getAllHaulers(companyId)
        }
    }

    override suspend fun getHaulerById(haulerId: Int, companyId: Int): Hauler? {
        Timber.tag("HaulersRepo").d("Get haulers by id: $haulerId")
        return withContext(Dispatchers.IO) {
            haulersDao.getHaulerById(haulerId, companyId)
        }
    }

    override suspend fun deleteHaulers(companyId: Int) {
        withContext(Dispatchers.IO) {
            haulersDao.deleteAllHaulers(companyId)
        }
    }
}

private fun HaulerRestObject.toHauler(companyId: Int) = Hauler(
    haulerId = haulerId!!,
    companyId = companyId,
    name = name!!
)