package com.fieldflo.data.repos

import com.fieldflo.data.api.endpoints.CompanyDataApi
import com.fieldflo.data.api.endpoints.GetByIdBody
import com.fieldflo.data.api.entities.InventoryItemRestObject
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.entities.InventoryItem
import com.fieldflo.data.repos.interfaces.IInventoryItemsRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class InventoryItemsRepo(
    db: FieldFloDatabase,
    private val companyDataApi: CompanyDataApi
) : IInventoryItemsRepo {

    private val inventoryItemDao = db.getInventoryItemDao2()

    override suspend fun loadInventoryItems(companyId: Int) {
        withContext(Dispatchers.IO) {
            val inventoryItems =
                companyDataApi.getAllInventoryItems().map { it.toInventoryItem(companyId) }
            inventoryItemDao.addInventoryItems(inventoryItems)
        }
    }

    override suspend fun loadNewInventoryItem(inventoryItemId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            val inventoryItems = companyDataApi.getInventoryItem(GetByIdBody(inventoryItemId))
                .map { it.toInventoryItem(companyId) }
            if (inventoryItems.size == 1) {
                inventoryItemDao.deleteInventoryItem(inventoryItemId, companyId)
                inventoryItemDao.addInventoryItem(inventoryItems[0])
            }
        }
    }

    override suspend fun replaceAllInventoryItems(companyId: Int) {
        withContext(Dispatchers.IO) {
            val inventoryItems =
                companyDataApi.getAllInventoryItems().map { it.toInventoryItem(companyId) }
            inventoryItemDao.deleteAllInventoryItems(companyId)
            inventoryItemDao.addInventoryItems(inventoryItems)
        }
    }

    override suspend fun getEquipmentInventoryItems(companyId: Int): List<InventoryItem> {
        Timber.tag(TAG).d("Get inventory items for time sheet asset")
        return withContext(Dispatchers.IO) {
            inventoryItemDao.getEquipmentInventoryItems(companyId)
        }
    }

    override suspend fun getInventoryItem(inventoryItemId: Int, companyId: Int): InventoryItem? {
        return withContext(Dispatchers.IO) {
            inventoryItemDao.getInventoryItem(inventoryItemId, companyId)
        }
    }

    override suspend fun deleteInventoryItem(inventoryItemId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            inventoryItemDao.deleteInventoryItem(inventoryItemId, companyId)
        }
    }

    override suspend fun deleteInventoryItems(companyId: Int) {
        withContext(Dispatchers.IO) {
            inventoryItemDao.deleteAllInventoryItems(companyId)
        }
    }
}

private const val TAG = "InventoryItemsRepo"

private fun InventoryItemRestObject.toInventoryItem(companyId: Int) = InventoryItem(
    companyId = companyId,
    inventoryItemId = inventoryItemId!!,
    inventoryItemName = inventoryItemName!!,
    inventoryItemNumber = inventoryItemNumber!!,
    assetClass = assetClass!!,
    isDeleted = isDeleted!!
)