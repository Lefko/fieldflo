package com.fieldflo.data.repos

import com.fieldflo.data.api.endpoints.CompanyDataApi
import com.fieldflo.data.api.endpoints.GetByProjectIdBody
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.entities.AvailableForm
import com.fieldflo.data.repos.interfaces.IAvailableFormsRepo
import com.fieldflo.usecases.SupportedForm
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AvailableFormRepo(
    db: FieldFloDatabase,
    private val companyDataApi: CompanyDataApi
) : IAvailableFormsRepo {

    private val availableFormDao = db.getAvailableFormDao2()

    override suspend fun replaceAvailableForms(projectId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            val newAvailableForms =
                companyDataApi.getAvailableFormsForProject(GetByProjectIdBody(projectId))
                    .filter { SupportedForm.supportedFormIds.contains(it.formTypeId) }
                    .map { it.toAvailableForm(companyId, projectId) }
            replaceAvailableForms(newAvailableForms, projectId, companyId)
        }
    }

    override suspend fun getAvailableForms(projectId: Int, companyId: Int): List<AvailableForm> {
        return withContext(Dispatchers.IO) {
            availableFormDao.getAvailableForms(projectId, companyId)
                .filter { SupportedForm.supportedFormIds.contains(it.formTypeId) }
                .sortedBy { it.sort }
        }
    }

    override suspend fun replaceAvailableForms(
        newAvailableForms: List<AvailableForm>,
        projectId: Int,
        companyId: Int
    ) {
        withContext(Dispatchers.IO) {
            availableFormDao.deleteAvailableFormsOnProject(projectId, companyId)
            availableFormDao.addAvailableForms(newAvailableForms)
        }
    }

    override suspend fun deleteAvailableFormsOnProject(projectId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            availableFormDao.deleteAvailableFormsOnProject(projectId, companyId)
        }
    }

    override suspend fun deleteAvailableForms(companyId: Int) {
        withContext(Dispatchers.IO) {
            availableFormDao.deleteAvailableFormsOnProject(companyId)
        }
    }
}
