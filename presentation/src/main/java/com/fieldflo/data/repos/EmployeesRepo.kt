package com.fieldflo.data.repos

import com.fieldflo.data.api.endpoints.CompanyDataApi
import com.fieldflo.data.api.endpoints.GetByIdBody
import com.fieldflo.data.api.entities.EmployeeRestObject
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.entities.Employee
import com.fieldflo.data.repos.interfaces.IEmployeesRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class EmployeesRepo(
    db: FieldFloDatabase,
    private val companyDataApi: CompanyDataApi
) : IEmployeesRepo {

    private val employeeDao = db.getEmployeeDao2()

    @Throws(Exception::class)
    override suspend fun loadEmployees(companyId: Int) {
        withContext(Dispatchers.IO) {
            val employeesRest = companyDataApi.getAllEmployees()
            val employees = employeesRest.map { it.toEmployee(companyId) }
            employeeDao.addAllEmployees(employees)
        }
    }

    @Throws(Exception::class)
    override suspend fun loadNewEmployee(employeeId: Int, companyId: Int) {
        withContext(Dispatchers.IO) {
            val employeesRest = companyDataApi.getEmployee(GetByIdBody(employeeId))
            if (employeesRest.size == 1) {
                val newEmployee = employeesRest[0].toEmployee(companyId)
                employeeDao.deleteEmployee(employeeId, companyId)
                employeeDao.addEmployee(newEmployee)
            }
        }
    }

    override suspend fun replaceAllEmployees(companyId: Int) {
        withContext(Dispatchers.IO) {
            val newEmployees = companyDataApi.getAllEmployees().map { it.toEmployee(companyId) }
            val currentEmployees = employeeDao.getAllEmployees(companyId)
            val employeesToSetDelete =
                (currentEmployees - newEmployees).map { it.copy(employeeDeleted = true) }
            employeeDao.deleteAllEmployees(companyId)
            employeeDao.addAllEmployees(newEmployees + employeesToSetDelete)
        }
    }

    override suspend fun getEmployeeById(employeeId: Int, companyId: Int): Employee? {
        Timber.tag("EmployeesRepo").d("Get employee by id: $employeeId")
        return withContext(Dispatchers.IO) {
            employeeDao.getEmployeeById(employeeId, companyId)
        }
    }

    override suspend fun getAllEmployees(companyId: Int): List<Employee> {
        Timber.tag("EmployeesRepo").d("Get all employees")
        return withContext(Dispatchers.IO) {
            employeeDao.getAllEmployees(companyId)
        }
    }

    override suspend fun setEmployeeDeleted(employeeId: Int, companyId: Int) {
        return withContext(Dispatchers.IO) {
            employeeDao.setEmployeeDeleted(employeeId, companyId)
        }
    }

    override suspend fun deleteEmployees(companyId: Int) {
        withContext(Dispatchers.IO) {
            employeeDao.deleteAllEmployees(companyId)
        }
    }
}

private fun EmployeeRestObject.toEmployee(companyId: Int) =
    Employee(
        companyId = companyId,
        employeeId = employeeId!!,
        positionId = positionId!!,
        employeeFirstName = employeeFirstName!!,
        employeeMiddleName = employeeMiddleName!!,
        employeeLastName = employeeLastName!!,
        employeeImageFullUrl = employeeImageFullUrl!!,
        employeePinNumber = employeePinNumber!!,
        employeePhoneNumber = employeePhoneNumber!!,
        employeeActive = employeeActive!!,
        employeeDeleted = employeeDeleted!!,
        admin = admin!!
    )