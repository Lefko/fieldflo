package com.fieldflo.data.repos.interfaces

import com.fieldflo.data.persistence.db.entities.Hauler

interface IHaulersRepo {
    @Throws(Exception::class)
    suspend fun loadHaulers(companyId: Int)
    suspend fun replaceAllHaulers(companyId: Int)
    suspend fun loadNewHauler(haulerId: Int, companyId: Int)
    suspend fun setHaulerDeleted(haulerId: Int, companyId: Int)

    suspend fun getAllHaulers(companyId: Int): List<Hauler>
    suspend fun getHaulerById(haulerId: Int, companyId: Int): Hauler?
    suspend fun deleteHaulers(companyId: Int)
}