package com.fieldflo.data.api.interceptors

import com.fieldflo.data.persistence.keyValue.ILocalStorage
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.Interceptor
import okhttp3.Response
import retrofit2.Invocation
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BaseUrlInterceptor @Inject constructor(private val localStorage: ILocalStorage) :
    Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        return if (originalRequest.url.toString().contains("google")) {

            val invocation = originalRequest.tag(Invocation::class.java)

            val apiVersionAnnotation: ApiVersion? =
                invocation?.method()?.getAnnotation(ApiVersion::class.java)

            val apiVersion: Int = apiVersionAnnotation?.version ?: DEFAULT_API_VERSION

            val baseUrl = localStorage.getBaseUrl()

            val httpUrl = baseUrl.toHttpUrlOrNull()

            val baseScheme = httpUrl?.scheme

            val baseHost = httpUrl?.host

            val basePaths = httpUrl?.pathSegments

            if (baseScheme == null || baseHost == null) {
                throw NullPointerException("Host/Scheme of baseUrl must be set")
            }
            val requestPaths = originalRequest.url.pathSegments

            val builder = HttpUrl.Builder()
                .scheme(baseScheme)
                .host(baseHost)
                .addPathSegment("$API_REST_STRING$apiVersion")

            basePaths?.forEach {
                builder.addPathSegment(it)
            }

            requestPaths.forEach {
                builder.addPathSegment(it)
            }

            val newUrl = builder.build()

            val newRequest = originalRequest.newBuilder()
                .url(newUrl)
                .build()
            chain.proceed(newRequest)
        } else {
            chain.proceed(originalRequest)
        }
    }
}

private const val API_REST_STRING = "apirest_v"
private const val DEFAULT_API_VERSION = 2

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class ApiVersion(val version: Int)

