package com.fieldflo.data.api.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RoleRestObject(

    @field:Json(name = "code")
    val roleCode: String? = ""
)
