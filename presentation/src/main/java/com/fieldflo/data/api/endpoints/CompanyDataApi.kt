package com.fieldflo.data.api.endpoints

import com.fieldflo.data.api.entities.*
import com.fieldflo.data.api.interceptors.ApiVersion
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import retrofit2.http.Body
import retrofit2.http.POST

interface CompanyDataApi {

    @ApiVersion(2)
    @POST("getAllEmployees")
    suspend fun getAllEmployees(@Body body: IsActiveRequestBody = IsActiveRequestBody()): List<EmployeeRestObject>

    @ApiVersion(2)
    @POST("getAllEmployees")
    suspend fun getEmployee(@Body getByIdBody: GetByIdBody): List<EmployeeRestObject>

    @ApiVersion(2)
    @POST("getAllPositions")
    suspend fun getAllPositions(@Body body: IsActiveRequestBody = IsActiveRequestBody()): List<PositionRestObject>

    @ApiVersion(2)
    @POST("getAllPositions")
    suspend fun getPosition(@Body getByIdBody: GetByIdBody): List<PositionRestObject>

    @ApiVersion(2)
    @POST("getAllPhases")
    suspend fun getAllPhases(): List<PhaseRestObject>

    @ApiVersion(2)
    @POST("getAllPhases")
    suspend fun getPhase(@Body getByIdBody: GetByIdBody): List<PhaseRestObject>

    @ApiVersion(2)
    @POST("getAllInventoryItems")
    suspend fun getAllInventoryItems(): List<InventoryItemRestObject>

    @ApiVersion(2)
    @POST("getAllInventoryItems")
    suspend fun getInventoryItem(@Body getByIdBody: GetByIdBody): List<InventoryItemRestObject>

    @ApiVersion(2)
    @POST("getAllCertificates")
    suspend fun getAllCertificates(@Body body: IsActiveRequestBody = IsActiveRequestBody()): List<CompanyCertificateRestObject>

    @ApiVersion(2)
    @POST("getAllCertificates")
    fun getCertificates(@Body getByIdListBody: GetByIdListBody): List<CompanyCertificateRestObject>

    @ApiVersion(2)
    @POST("getAllPSITasks")
    suspend fun getAllTasks(): List<TaskRestObject>

    @ApiVersion(2)
    @POST("getAllSubcontractors")
    suspend fun getAllSubContractors(): List<SubContractorRestObject>

    @ApiVersion(2)
    @POST("getAllSubcontractors")
    suspend fun getSubContractor(@Body getByIdBody: GetByIdBody): List<SubContractorRestObject>

    @ApiVersion(2)
    @POST("getAllHaulers")
    suspend fun getAllHaulers(): List<HaulerRestObject>

    @ApiVersion(2)
    @POST("getAllHaulers")
    suspend fun getHauler(@Body getByIdBody: GetByIdBody): List<HaulerRestObject>

    @ApiVersion(3)
    @POST("getAllProjects")
    suspend fun getAllProjects(@Body getAllProjectsRequestBody: GetAllProjectsRequestBody = GetAllProjectsRequestBody()): List<ProjectRestObject>

    @ApiVersion(3)
    @POST("getAllProjects")
    suspend fun getProjectsByIds(@Body getByIdListBody: GetByIdListBody): List<ProjectRestObject>

    @ApiVersion(3)
    @POST("getAvailableFormsForProject")
    suspend fun getAvailableFormsForProject(@Body getByProjectIdBody: GetByProjectIdBody): List<AvailableFormRestObject>

    @ApiVersion(2)
    @POST("getCertificatesByProjectID")
    suspend fun getProjectCertificates(@Body body: GetProjectCertsRequestBody = GetProjectCertsRequestBody()): List<ProjectCertificateRestObject>

    @ApiVersion(2)
    @POST("getCertificatesByProjectID")
    suspend fun getProjectCertificatesById(@Body getByProjectIdBody: GetByProjectIdBody): List<ProjectCertificateRestObject>
}

@JsonClass(generateAdapter = true)
class IsActiveRequestBody(
    @Json(name = "active")
    val isActive: Boolean = true
)

@JsonClass(generateAdapter = true)
data class GetAllProjectsRequestBody(
    @Json(name = "closed")
    val isClosed: Boolean = false
)

@JsonClass(generateAdapter = true)
data class GetProjectCertsRequestBody(
    @Json(name = "projectID")
    val projectId: Int = 0
)

@JsonClass(generateAdapter = true)
data class GetByIdBody(
    @field:Json(name = "id")
    val id: Int
)

@JsonClass(generateAdapter = true)
data class GetByIdListBody(
    @field:Json(name = "listID")
    val ids: IntArray
)

@JsonClass(generateAdapter = true)
data class GetByProjectIdBody(
    @field:Json(name = "projectID")
    val projectId: Int
)
