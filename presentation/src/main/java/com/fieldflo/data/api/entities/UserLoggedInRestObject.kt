package com.fieldflo.data.api.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserLoggedInRestObject(

    @field:Json(name = "employeeID")
    val employeeId: Int? = 0,

    @field:Json(name = "employeeFirstName")
    val employeeFirstName: String? = "",

    @field:Json(name = "employeeLastName")
    val employeeLastName: String? = "",

    @field:Json(name = "employeePinNumber")
    val employeePinNumber: String? = "",

    @field:Json(name = "positionID")
    val positionId: Int? = 0,

    @field:Json(name = "roles")
    val roles: List<RoleRestObject>? = emptyList()
) {
    val canViewProjects: Boolean
        get() = roles?.any { it.roleCode == "roleprojects" } ?: false

    val canAddEditOpenForms: Boolean
        get() = roles?.any { it.roleCode == "roleaddeditopenforms" } ?: false

    val canViewCerts: Boolean
        get() = roles?.any { it.roleCode == "roleviewcerts" } ?: false

    val canEditCerts: Boolean
        get() = roles?.any { it.roleCode == "roleeditcerts" } ?: false

    val canEditTimeSheetTime: Boolean
        get() = roles?.any { it.roleCode == "roleaccesstimesheetadjuster" } ?: false

    val canValidateTimeSheetWithPin: Boolean
        get() = roles?.any { it.roleCode == "roleTimesheetCanPin" } ?: false
}
