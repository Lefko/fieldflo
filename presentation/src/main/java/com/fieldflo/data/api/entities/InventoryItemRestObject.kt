package com.fieldflo.data.api.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class InventoryItemRestObject(

    @field:Json(name = "itemID")
    val inventoryItemId: Int? = 0,

    @field:Json(name = "itemName")
    val inventoryItemName: String? = "",

    @field:Json(name = "itemNo")
    val inventoryItemNumber: Int? = 0,

    @field:Json(name = "assetClass")
    val assetClass: String? = "",

    @field:Json(name = "isDeleted")
    val isDeleted: Boolean? = false
)
