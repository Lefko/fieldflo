package com.fieldflo.data.api.serialization

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonReader
import javax.inject.Inject

class EmptyStringToStringTypeAdapter @Inject constructor() {

    @FromJson
    fun fromJson(jsonReader: JsonReader): String? {
        if (jsonReader.peek() == JsonReader.Token.NULL) {
            @Suppress("UNUSED_VARIABLE") val nil: Any? = jsonReader.nextNull()
            return ""
        }

        try {
            val string = jsonReader.nextString()
            if (string.isNullOrEmpty()) {
                return ""
            }
            return string
        } catch (e: Exception) {
            return ""
        }
    }
}