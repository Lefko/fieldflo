package com.fieldflo.data.api.serialization

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonReader
import javax.inject.Inject

class EmptyStringToDoubleTypeAdapter @Inject constructor() {

    @FromJson
    fun fromJson(jsonReader: JsonReader): Double? {
        if (jsonReader.peek() == JsonReader.Token.NULL) {
            @Suppress("UNUSED_VARIABLE") val nil: Any? = jsonReader.nextNull()
            return 0.0
        }

        if (jsonReader.peek() == JsonReader.Token.NUMBER) {
            return jsonReader.nextDouble()
        }

        try {
            val numString = jsonReader.nextString()
            if (numString.isNullOrEmpty()) {
                return 0.0
            }
            return numString.toDouble()
        } catch (e: Exception) {
            return 0.0
        }
    }
}