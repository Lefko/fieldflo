package com.fieldflo.data.api.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class HaulerRestObject(

    @field:Json(name = "ID")
    val haulerId: Int? = 0,

    @field:Json(name = "name")
    val name: String? = ""
)
