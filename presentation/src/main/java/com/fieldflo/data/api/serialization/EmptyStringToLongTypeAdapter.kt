package com.fieldflo.data.api.serialization

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonReader
import javax.inject.Inject

class EmptyStringToLongTypeAdapter @Inject constructor() {

    @FromJson
    fun fromJson(jsonReader: JsonReader): Long? {
        if (jsonReader.peek() == JsonReader.Token.NULL) {
            @Suppress("UNUSED_VARIABLE")
            val nil: Any? = jsonReader.nextNull()
            return 0L
        }

        if (jsonReader.peek() == JsonReader.Token.NUMBER) {
            return jsonReader.nextLong()
        }

        try {
            val numString = jsonReader.nextString()
            if (numString.isNullOrEmpty()) {
                return 0L
            }
            return numString.toLong()
        } catch (e: Exception) {
            return 0L
        }
    }
}