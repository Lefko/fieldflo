package com.fieldflo.data.api.interceptors

import com.fieldflo.data.persistence.keyValue.ILocalStorage
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HeadersInterceptor @Inject constructor(private val localStorage: ILocalStorage) :
    Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        val apiToken = localStorage.getApiToken()
        val securityToken = localStorage.getSecurityKey()
        val userToken = localStorage.getUserToken()

        val requestBuilder = originalRequest.newBuilder()
            .addCustomHeaders(apiToken, securityToken, userToken)

        val request = requestBuilder.build()

        return chain.proceed(request)
    }
}

const val API_TOKEN_HEADER_KEY = "APITOKEN"
const val SECURITY_KEY_HEADER_KEY = "SECURITYKEY"
const val USER_TOKEN_HEADER_KEY = "USERTOKEN"

private const val REQUEST_HEADER_KEY = "REQUEST"
private const val REQUEST_HEADER_VALUE = "API"
private const val IDENTIFICATION_HEADER_KEY = "IDENTIFICATION"
private const val IDENTIFICATION_HEADER_VALUE = "mobile"

fun Request.Builder.addCustomHeaders(
    apiToken: String,
    securityToken: String,
    userToken: String
): Request.Builder {
    this.addHeader(API_TOKEN_HEADER_KEY, apiToken)
        .addHeader(SECURITY_KEY_HEADER_KEY, securityToken)
        .addHeader(REQUEST_HEADER_KEY, REQUEST_HEADER_VALUE)
        .addHeader(IDENTIFICATION_HEADER_KEY, IDENTIFICATION_HEADER_VALUE)

    if (userToken.isNotEmpty()) {
        this.addHeader(USER_TOKEN_HEADER_KEY, userToken)
    }

    return this
}