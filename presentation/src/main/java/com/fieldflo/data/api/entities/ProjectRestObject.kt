package com.fieldflo.data.api.entities

import com.fieldflo.data.persistence.db.entities.Project
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class ProjectRestObject(

    @field:Json(name = "projectID")
    val projectId: Int? = 0,

    @field:Json(name = "projectNumber")
    val projectNumber: String? = "",

    @field:Json(name = "projectName")
    val projectName: String? = "",

    @field:Json(name = "projectManagerObj")
    val projectManager: EmbeddedEmployeeRestObject = EmbeddedEmployeeRestObject(),

    @field:Json(name = "supervisorObj")
    val supervisor: EmbeddedEmployeeRestObject = EmbeddedEmployeeRestObject(),

    @field:Json(name = "projectStartDate")
    val projectStartDate: Date? = Date(0),

    @field:Json(name = "projectEndDate")
    val projectEndDate: Date? = Date(0),

    @field:Json(name = "projectBranchNumber")
    val branchNumber: String? = "",

    @field:Json(name = "projectBidNumber")
    val projectBidNumber: String? = "",

    @field:Json(name = "projectTypeObj")
    val projectTypeObject: ProjectTypeObject? = ProjectTypeObject(""),

    @field:Json(name = "lossTypeObj")
    val projectLossTypeObj: ProjectTypeObject? = ProjectTypeObject(""),

    @field:Json(name = "nightShift")
    val hasNightShift: Boolean? = false,

    @field:Json(name = "shiftLength")
    val shiftLength: Int? = 0,

    @field:Json(name = "projectCrewSize")
    val projectCrewSize: Int? = 0,

    @field:Json(name = "supervisorPhone")
    val supervisorPhone: String? = "",

    @field:Json(name = "projectAdministratorObj")
    val projectAdministrator: EmbeddedEmployeeRestObject = EmbeddedEmployeeRestObject(),

    @field:Json(name = "clientObj")
    val client: EmbeddedClientObject = EmbeddedClientObject(),

    @field:Json(name = "scopeOfWork")
    val scopeOfWork: String? = "",

    @field:Json(name = "projectSiteAddress")
    val projectSiteAddress: String? = "",

    @field:Json(name = "projectCrossStreet")
    val projectCrossStreet: String? = "",

    @field:Json(name = "projectCity")
    val projectCity: String? = "",

    @field:Json(name = "projectStateObj")
    val projectState: StateRestObject? = null,

    @field:Json(name = "projectZipCode")
    val projectZipCode: String? = "",

    @field:Json(name = "projectCounty")
    val projectCounty: String? = "",

    @field:Json(name = "projectNameOfSiteContact")
    val projectNameOfSiteContact: String? = "",

    @field:Json(name = "sitePhone")
    val projectSitePhone: String? = "",

    @field:Json(name = "contractType")
    val contractType: String? = "",

    @field:Json(name = "estimatorObj")
    val estimator: EmbeddedEmployeeRestObject = EmbeddedEmployeeRestObject(),

    @field:Json(name = "estimatorContactPhone")
    val estimatorContactPhone: String? = "",

    @field:Json(name = "salePersonObj")
    val salePerson: EmbeddedEmployeeRestObject = EmbeddedEmployeeRestObject(),

    @field:Json(name = "divisionManagerObj")
    val divisionManager: EmbeddedEmployeeRestObject = EmbeddedEmployeeRestObject(),

    @field:Json(name = "perDiem")
    val requiresPerDiem: Boolean? = true,

    @field:Json(name = "perDiemType")
    val perDiemType: String? = "",

    @field:Json(name = "perDiemHourlyAmount")
    val perDiemHourlyAmount: Double? = 0.0,

    @field:Json(name = "perDiemDailyAmount")
    val perDiemDailyAmount: Double? = 0.0,

    @field:Json(name = "perDiemNotes")
    val perDiemNotes: String? = "",

    @field:Json(name = "perDiemAutoApply")
    val perDiemAutoApplyToTimesheet: Boolean? = false,

    @field:Json(name = "buildingSize")
    val buildingSize: String? = "",

    @field:Json(name = "buildingAgeOfYears")
    val buildingAgeOfYears: Int? = 0,

    @field:Json(name = "buildingNumberOfFloors")
    val buildingNumberOfFloors: Int? = 0,

    @field:Json(name = "buildingPresentUse")
    val buildingPresentUse: String? = "",

    @field:Json(name = "buildingPriorUse")
    val buildingPriorUse: String? = "",

    @field:Json(name = "buildingNumberOfDwellingUnits")
    val buildingNumberOfDwellingUnits: Int? = 0,

    @field:Json(name = "buildingWorkLocation")
    val buildingWorkLocation: String? = "",

    @field:Json(name = "buildingWorkDescription")
    val buildingWorkDescription: String? = "",

    @field:Json(name = "ownerCompanyName")
    val ownerCompanyName: String? = "",

    @field:Json(name = "ownerAddress")
    val ownerAddress: String? = "",

    @field:Json(name = "ownerCity")
    val ownerCity: String? = "",

    @field:Json(name = "ownerStateObj")
    val ownerState: StateRestObject? = null,

    @field:Json(name = "ownerZipCode")
    val ownerZipCode: String? = "",

    @field:Json(name = "ownerNameOfContact")
    val ownerNameOfContact: String? = "",

    @field:Json(name = "ownerPhoneNumber")
    val ownerPhoneNumber: String? = "",

    @field:Json(name = "ownerFaxNumber")
    val ownerFaxNumber: String? = "",

    @field:Json(name = "ownerCellPhone")
    val ownerCellPhone: String? = "",

    @field:Json(name = "procedures")
    val procedures: String? = "",

    @field:Json(name = "billingCompanyName")
    val billingCompanyName: String? = "",

    @field:Json(name = "billingPurchaseorWorkOrderNumber")
    val billingPurchaseOrWorkOrderNumber: String? = "",

    @field:Json(name = "billingTermsOfContact")
    val billingTermsOfContact: String? = "",

    @field:Json(name = "billingClientContact")
    val billingClientContact: String? = "",

    @field:Json(name = "billingAddress")
    val billingAddress: String? = "",

    @field:Json(name = "billingCity")
    val billingCity: String? = "",

    @field:Json(name = "billingStateObj")
    val billingState: StateRestObject? = null,

    @field:Json(name = "billingZipCode")
    val billingZipCode: String? = "",

    @field:Json(name = "billingPhoneNumber")
    val billingPhoneNumber: String? = "",

    @field:Json(name = "billingFaxNumber")
    val billingFaxNumber: String? = "",

    @field:Json(name = "lienLender")
    val lienLender: String? = "",

    @field:Json(name = "lienLenderAddress")
    val lienLenderAddress: String? = "",

    @field:Json(name = "lienCity")
    val lienCity: String? = "",

    @field:Json(name = "lienStateObj")
    val lienState: StateRestObject? = null,

    @field:Json(name = "lienZipCode")
    val lienZipCode: String? = "",

    @field:Json(name = "lienLenderValue")
    val lienLenderValue: Double? = 0.0,

    @field:Json(name = "lienMonth")
    val lienMonth: String? = "",

    @field:Json(name = "lienDay")
    val lienDay: String? = "",

    @field:Json(name = "lienYear")
    val lienYear: String? = "",

    @field:Json(name = "designerFirmName")
    val designerFirmName: String? = "",

    @field:Json(name = "designerContact")
    val designerContact: String? = "",

    @field:Json(name = "designerAddress")
    val designerAddress: String? = "",

    @field:Json(name = "designerCity")
    val designerCity: String? = "",

    @field:Json(name = "designerStateObj")
    val designerState: StateRestObject? = null,

    @field:Json(name = "designerZipCode")
    val designerZipCode: String? = "",

    @field:Json(name = "designerPhoneNumber")
    val designerPhoneNumber: String? = "",

    @field:Json(name = "designerFaxNumber")
    val designerFaxNumber: String? = "",

    @field:Json(name = "designerCell")
    val designerCell: String? = "",

    @field:Json(name = "designerCertNumber")
    val designerCertNumber: String? = "",

    @field:Json(name = "consultantFirmName")
    val consultantFirmName: String? = "",

    @field:Json(name = "consultantContact")
    val consultantContact: String? = "",

    @field:Json(name = "consultantAddress")
    val consultantAddress: String? = "",

    @field:Json(name = "consultantCity")
    val consultantCity: String? = "",

    @field:Json(name = "consultantStateObj")
    val consultantState: StateRestObject? = null,

    @field:Json(name = "consultantZipCode")
    val consultantZipCode: String? = "",

    @field:Json(name = "consultantPhoneNumber")
    val consultantPhoneNumber: String? = "",

    @field:Json(name = "consultantFaxNumber")
    val consultantFaxNumber: String? = "",

    @field:Json(name = "airmonitoringFirmName")
    val airMonitoringFirmName: String? = "",

    @field:Json(name = "airmonitoringContact")
    val airMonitoringContact: String? = "",

    @field:Json(name = "airmonitoringAddress")
    val airMonitoringAddress: String? = "",

    @field:Json(name = "airmonitoringCity")
    val airMonitoringCity: String? = "",

    @field:Json(name = "airmonitoringStateObj")
    val airMonitoringState: StateRestObject? = null,

    @field:Json(name = "airmonitoringZipCode")
    val airMonitoringZipCode: String? = "",

    @field:Json(name = "airmonitoringPhoneNumber")
    val airMonitoringPhoneNumber: String? = "",

    @field:Json(name = "airmonitoringFaxNumber")
    val airMonitoringFaxNumber: String? = "",

    @field:Json(name = "airmonitoringCell")
    val airMonitoringCell: String? = "",

    @field:Json(name = "airmonitoringCertNumber")
    val airMonitoringCertNumber: String? = "",

    @field:Json(name = "insuranceAdditionalInsured")
    val insuranceAdditionalInsured: String? = "",

    @field:Json(name = "insuranceAddress")
    val insuranceAddress: String? = "",

    @field:Json(name = "insuranceCity")
    val insuranceCity: String? = "",

    @field:Json(name = "insuranceStateObj")
    val insuranceState: StateRestObject? = null,

    @field:Json(name = "insuranceZipCode")
    val insuranceZipCode: String? = "",

    @field:Json(name = "insurancePrimaryWordingRequest")
    val insurancePrimaryWordingRequest: String? = "",

    @field:Json(name = "insuranceClientProjectNumber")
    val insuranceClientProjectNumber: String? = "",

    @field:Json(name = "insuranceOtherInstructions")
    val insuranceOtherInstructions: String? = "",

    @field:Json(name = "insuranceOtherInstructions2")
    val insuranceOtherInstructions2: String? = "",

    @field:Json(name = "insuranceOtherInstructions3")
    val insuranceOtherInstructions3: String? = "",

    @field:Json(name = "insuranceOtherInstructions4")
    val insuranceOtherInstructions4: String? = "",

    @field:Json(name = "insuranceRegMailOrigToCertHolder")
    val insuranceRegMailOrigToCertHolder: String? = "",

    @field:Json(name = "insuranceFedexOrigToCertHolder")
    val insuranceFedexOrigToCertHolder: String? = "",

    @field:Json(name = "insuranceFaxToClient")
    val insuranceFaxToClient: String? = "",

    @field:Json(name = "insuranceMailCopy")
    val insuranceMailCopy: String? = "",

    @field:Json(name = "insuranceFaxCopy")
    val insuranceFaxCopy: String? = "",

    @field:Json(name = "payrollWeeklyCertifiedPayroll")
    val payrollWeeklyCertifiedPayroll: Boolean? = false,

    @field:Json(name = "payrollUnionJob")
    val payrollUnionJob: Boolean? = false,

    @field:Json(name = "payrollPrevailingWage")
    val payrollPrevailingWage: Boolean? = false,

    @field:Json(name = "fireDepartmentName")
    val fireDepartmentName: String? = "",

    @field:Json(name = "fireDepartmentAddress")
    val fireDepartmentAddress: String? = "",

    @field:Json(name = "fireDepartmentCity")
    val fireDepartmentCity: String? = "",

    @field:Json(name = "fireDepartmentStateObj")
    val fireDepartmentState: StateRestObject? = null,

    @field:Json(name = "fireDepartmentZipCode")
    val fireDepartmentZipCode: String? = "",

    @field:Json(name = "fireDepartmentPhone")
    val fireDepartmentPhone: String? = "",

    @field:Json(name = "policeDepartmentName")
    val policeDepartmentName: String? = "",

    @field:Json(name = "policeDepartmentAddress")
    val policeDepartmentAddress: String? = "",

    @field:Json(name = "policeDepartmentCity")
    val policeDepartmentCity: String? = "",

    @field:Json(name = "policeDepartmentStateObj")
    val policeDepartmentState: StateRestObject? = null,

    @field:Json(name = "policeDepartmentZipCode")
    val policeDepartmentZipCode: String? = "",

    @field:Json(name = "policeDepartmentPhone")
    val policeDepartmentPhone: String? = "",

    @field:Json(name = "medicalDepartmentName")
    val medicalDepartmentName: String? = "",

    @field:Json(name = "medicalDepartmentAddress")
    val medicalDepartmentAddress: String? = "",

    @field:Json(name = "medicalDepartmentCity")
    val medicalDepartmentCity: String? = "",

    @field:Json(name = "medicalDepartmentStateObj")
    val medicalDepartmentState: StateRestObject? = null,

    @field:Json(name = "medicalDepartmentZipCode")
    val medicalDepartmentZipCode: String? = "",

    @field:Json(name = "medicalDepartmentPhone")
    val medicalDepartmentPhone: String? = "",

    @field:Json(name = "customField1")
    val customField1: String? = "",

    @field:Json(name = "customField2")
    val customField2: String? = "",

    @field:Json(name = "customField3")
    val customField3: String? = "",

    @field:Json(name = "customField4")
    val customField4: String? = "",

    @field:Json(name = "customField5")
    val customField5: String? = "",

    @field:Json(name = "customField6")
    val customField6: String? = "",

    @field:Json(name = "usePhases")
    val usePhases: Boolean? = false,

    @field:Json(name = "phasesType")
    val phasesType: String? = "",

    @field:Json(name = "phaseCode")
    val phaseCode: String? = "",

    @field:Json(name = "warehousePending")
    val warehousePending: Int? = 0,

    @field:Json(name = "projectActive")
    val projectActive: Boolean? = true,

    @field:Json(name = "projectClosed")
    val projectClosed: Boolean? = false,

    @field:Json(name = "permits")
    val permits: List<ProjectPermitRestObject>? = listOf(),

    @field:Json(name = "availableForms")
    val availableForms: List<AvailableFormRestObject>? = listOf()
) {

    @JsonClass(generateAdapter = true)
    data class ProjectTypeObject(
        @field:Json(name = "name")
        val name: String? = ""
    )

    @JsonClass(generateAdapter = true)
    data class EmbeddedEmployeeRestObject(
        @field:Json(name = "employeeID")
        val employeeId: Int? = 0,

        @field:Json(name = "employeeFullName")
        val employeeFullName: String? = ""
    )

    @JsonClass(generateAdapter = true)
    data class EmbeddedClientObject(
        @field:Json(name = "companyName")
        val companyName: String? = ""
    )

    fun toProject(companyId: Int) = Project(
        companyId = companyId,
        projectId = this.projectId!!,
        projectNumber = this.projectNumber!!,
        projectName = this.projectName!!,
        projectManagerFullName = this.projectManager.employeeFullName!!,
        projectManagerId = this.projectManager.employeeId!!,
        supervisorFullName = this.supervisor.employeeFullName!!,
        supervisorId = this.supervisor.employeeId!!,
        projectStartDate = this.projectStartDate!!,
        projectEndDate = this.projectEndDate!!,
        branchNumber = this.branchNumber!!,
        projectBidNumber = this.projectBidNumber!!,
        projectTypeName = this.projectTypeObject?.name!!,
        projectLossTypeName = this.projectLossTypeObj?.name!!,
        hasNightShift = this.hasNightShift!!,
        shiftLength = this.shiftLength!!,
        projectCrewSize = this.projectCrewSize!!,
        supervisorPhone = this.supervisorPhone!!,
        projectAdministratorFullName = this.projectAdministrator.employeeFullName!!,
        clientFullName = this.client.companyName!!,
        scopeOfWork = this.scopeOfWork!!,
        projectSiteAddress = this.projectSiteAddress!!,
        projectCrossStreet = this.projectCrossStreet!!,
        projectCity = this.projectCity!!,
        projectState = this.projectState?.stateName!!,
        projectZipCode = this.projectZipCode!!,
        projectCounty = this.projectCounty!!,
        projectNameOfSiteContact = this.projectNameOfSiteContact!!,
        projectSitePhone = this.projectSitePhone!!,
        contractType = this.contractType!!,
        estimatorFullName = this.estimator.employeeFullName!!,
        estimatorContactPhone = this.estimatorContactPhone!!,
        salePersonFullName = this.salePerson.employeeFullName!!,
        divisionManagerFullName = this.divisionManager.employeeFullName!!,
        requiresPerDiem = this.requiresPerDiem!!,
        perDiemType = this.perDiemType!!,
        perDiemHourlyAmount = this.perDiemHourlyAmount!!,
        perDiemDailyAmount = this.perDiemDailyAmount!!,
        perDiemNotes = this.perDiemNotes!!,
        perDiemAutoApplyToTimesheet = this.perDiemAutoApplyToTimesheet!!,
        buildingSize = this.buildingSize!!,
        buildingAgeOfYears = this.buildingAgeOfYears!!,
        buildingNumberOfFloors = this.buildingNumberOfFloors!!,
        buildingPresentUse = this.buildingPresentUse!!,
        buildingPriorUse = this.buildingPriorUse!!,
        buildingNumberOfDwellingUnits = this.buildingNumberOfDwellingUnits!!,
        buildingWorkLocation = this.buildingWorkLocation!!,
        buildingWorkDescription = this.buildingWorkDescription!!,
        ownerCompanyName = this.ownerCompanyName!!,
        ownerAddress = this.ownerAddress!!,
        ownerCity = this.ownerCity!!,
        ownerState = this.ownerState?.stateName!!,
        ownerZipCode = this.ownerZipCode!!,
        ownerNameOfContact = this.ownerNameOfContact!!,
        ownerPhoneNumber = this.ownerPhoneNumber!!,
        ownerFaxNumber = this.ownerFaxNumber!!,
        ownerCellPhone = this.ownerCellPhone!!,
        procedures = this.procedures!!,
        billingCompanyName = this.billingCompanyName!!,
        billingPurchaseOrWorkOrderNumber = this.billingPurchaseOrWorkOrderNumber!!,
        billingTermsOfContact = this.billingTermsOfContact!!,
        billingClientContact = this.billingClientContact!!,
        billingAddress = this.billingAddress!!,
        billingCity = this.billingCity!!,
        billingState = this.billingState?.stateName!!,
        billingZipCode = this.billingZipCode!!,
        billingPhoneNumber = this.billingPhoneNumber!!,
        billingFaxNumber = this.billingFaxNumber!!,
        lienLender = this.lienLender!!,
        lienLenderAddress = this.lienLenderAddress!!,
        lienCity = this.lienCity!!,
        lienState = this.lienState?.stateName!!,
        lienZipCode = this.lienZipCode!!,
        lienLenderValue = this.lienLenderValue!!,
        lienMonth = this.lienMonth!!,
        lienDay = this.lienDay!!,
        lienYear = this.lienYear!!,
        designerFirmName = this.designerFirmName!!,
        designerContact = this.designerContact!!,
        designerAddress = this.designerAddress!!,
        designerCity = this.designerCity!!,
        designerState = this.designerState?.stateName!!,
        designerZipCode = this.designerZipCode!!,
        designerPhoneNumber = this.designerPhoneNumber!!,
        designerFaxNumber = this.designerFaxNumber!!,
        designerCell = this.designerCell!!,
        designerCertNumber = this.designerCertNumber!!,
        consultantFirmName = this.consultantFirmName!!,
        consultantContact = this.consultantContact!!,
        consultantAddress = this.consultantAddress!!,
        consultantCity = this.consultantCity!!,
        consultantState = this.consultantState?.stateName!!,
        consultantZipCode = this.consultantZipCode!!,
        consultantPhoneNumber = this.consultantPhoneNumber!!,
        consultantFaxNumber = this.consultantFaxNumber!!,
        airMonitoringFirmName = this.airMonitoringFirmName!!,
        airMonitoringContact = this.airMonitoringContact!!,
        airMonitoringAddress = this.airMonitoringAddress!!,
        airMonitoringCity = this.airMonitoringCity!!,
        airMonitoringState = this.airMonitoringState?.stateName!!,
        airMonitoringZipCode = this.airMonitoringZipCode!!,
        airMonitoringPhoneNumber = this.airMonitoringPhoneNumber!!,
        airMonitoringFaxNumber = this.airMonitoringFaxNumber!!,
        airMonitoringCell = this.airMonitoringCell!!,
        airMonitoringCertNumber = this.airMonitoringCertNumber!!,
        insuranceAdditionalInsured = this.insuranceAdditionalInsured!!,
        insuranceAddress = this.insuranceAddress!!,
        insuranceCity = this.insuranceCity!!,
        insuranceState = this.insuranceState?.stateName!!,
        insuranceZipCode = this.insuranceZipCode!!,
        insurancePrimaryWordingRequest = this.insurancePrimaryWordingRequest!!,
        insuranceClientProjectNumber = this.insuranceClientProjectNumber!!,
        insuranceOtherInstructions = this.insuranceOtherInstructions!!,
        insuranceOtherInstructions2 = this.insuranceOtherInstructions2!!,
        insuranceOtherInstructions3 = this.insuranceOtherInstructions3!!,
        insuranceOtherInstructions4 = this.insuranceOtherInstructions4!!,
        insuranceRegMailOrigToCertHolder = this.insuranceRegMailOrigToCertHolder!!,
        insuranceFedexOrigToCertHolder = this.insuranceFedexOrigToCertHolder!!,
        insuranceFaxToClient = this.insuranceFaxToClient!!,
        insuranceFaxCopy = this.insuranceFaxCopy!!,
        payrollWeeklyCertifiedPayroll = this.payrollWeeklyCertifiedPayroll!!,
        payrollUnionJob = this.payrollUnionJob!!,
        payrollPrevailingWage = this.payrollPrevailingWage!!,
        fireDepartmentName = this.fireDepartmentName!!,
        fireDepartmentAddress = this.fireDepartmentAddress!!,
        fireDepartmentCity = this.fireDepartmentCity!!,
        fireDepartmentState = this.fireDepartmentState?.stateName!!,
        fireDepartmentZipCode = this.fireDepartmentZipCode!!,
        fireDepartmentPhone = this.fireDepartmentPhone!!,
        policeDepartmentName = this.policeDepartmentName!!,
        policeDepartmentAddress = this.policeDepartmentAddress!!,
        policeDepartmentCity = this.policeDepartmentCity!!,
        policeDepartmentState = this.policeDepartmentState?.stateName!!,
        policeDepartmentZipCode = this.policeDepartmentZipCode!!,
        policeDepartmentPhone = this.policeDepartmentPhone!!,
        medicalDepartmentName = this.medicalDepartmentName!!,
        medicalDepartmentAddress = this.medicalDepartmentAddress!!,
        medicalDepartmentCity = this.medicalDepartmentCity!!,
        medicalDepartmentState = this.medicalDepartmentState?.stateName!!,
        medicalDepartmentZipCode = this.medicalDepartmentZipCode!!,
        medicalDepartmentPhone = this.medicalDepartmentPhone!!,
        customField1 = this.customField1!!,
        customField2 = this.customField2!!,
        customField3 = this.customField3!!,
        customField4 = this.customField4!!,
        customField5 = this.customField5!!,
        customField6 = this.customField6!!,
        usePhases = this.usePhases!!,
        phasesType = this.phasesType!!,
        phaseCode = this.phaseCode!!,
        projectActive = this.projectActive!!,
        projectClosed = this.projectClosed!!,
        warehousePending = this.warehousePending!!
    )
}