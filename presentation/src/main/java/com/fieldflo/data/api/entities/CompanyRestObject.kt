package com.fieldflo.data.api.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CompanyRestObject(

    @field:Json(name = "companyID")
    val companyId: Int? = 0,

    @field:Json(name = "token")
    val token: String? = "",

    @field:Json(name = "securityKey")
    val securityKey: String? = ""
)