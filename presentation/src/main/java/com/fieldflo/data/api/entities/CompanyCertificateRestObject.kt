package com.fieldflo.data.api.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class CompanyCertificateRestObject(

    @field:Json(name = "certificateID")
    val certificateId: Int? = 0,

    @field:Json(name = "certificateName")
    val certificateName: String? = "",

    @field:Json(name = "certificateExpirationDate")
    val certificateExpirationDate: Date? = Date(0),

    @field:Json(name = "certificateArchived")
    val certificateArchived: Boolean = false,

    @field:Json(name = "employeeID")
    val employeeId: Int? = 0,

    @field:Json(name = "employeeFirstName")
    val employeeFirstName: String? = "",

    @field:Json(name = "employeeMiddleName")
    val employeeMiddleName: String? = "",

    @field:Json(name = "employeeLastName")
    val employeeLastName: String? = "",

    @field:Json(name = "employeeAvatar")
    val employeeImageFullUrl: String? = "",

    @field:Json(name = "certificateFile")
    val certificateFileUrl: String? = ""
)
