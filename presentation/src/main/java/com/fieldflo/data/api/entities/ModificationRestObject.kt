package com.fieldflo.data.api.entities

data class ModificationRestObject(
    val node: String,
    val allIds: List<Int>,
    val updatedIds: List<Int>,
    val createdIds: List<Int>,
    val deletedIds: List<Int>
) : Comparable<ModificationRestObject> {
    companion object {

        const val AVAILABLE_PROJECT_FORM_CHANGE = "availableProjectForms"
        const val CLIENT_CHANGE = "clients"
        const val POSITIONS_CHANGE = "positions"
        const val EMPLOYEES_CHANGE = "employees"
        const val CERTS_CHANGE = "certs"
        const val PHASES_CHANGE = "phases"
        const val PROJECT_CHANGE = "project"
        const val PSI_TASK_CHANGE = "psiTasks"
        const val INVENTORY_ITEM_CHANGE = "items"
        const val HAULERS = "haulers"
        const val SUBCONTRACTORS = "subcontractors"
    }

    val replaceAll
        get() = allIds.isEmpty() && updatedIds.isEmpty() && createdIds.isEmpty() && deletedIds.isEmpty()

    override fun compareTo(other: ModificationRestObject): Int {
        if (this.node == other.node) {
            return 0
        }

        if (this.node == CLIENT_CHANGE || this.node == PROJECT_CHANGE) {
            return 1
        }

        if (other.node == CLIENT_CHANGE || other.node == PROJECT_CHANGE) {
            return -1
        }

        return 0
    }
}