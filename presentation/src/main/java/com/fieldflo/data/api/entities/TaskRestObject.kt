package com.fieldflo.data.api.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TaskRestObject(

    @field:Json(name = "id")
    val taskId: Int? = 0,

    @field:Json(name = "task")
    val task: String? = "",

    @field:Json(name = "hazard")
    val hazard: String? = "",

    @field:Json(name = "control")
    val control: String? = "",

    @field:Json(name = "risk")
    val risk: Int? = 0
)