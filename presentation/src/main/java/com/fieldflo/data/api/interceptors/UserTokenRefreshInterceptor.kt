package com.fieldflo.data.api.interceptors

import com.fieldflo.data.api.endpoints.AuthApi
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import dagger.Lazy
import okhttp3.Interceptor
import okhttp3.Response
import org.json.JSONException
import org.json.JSONObject
import timber.log.Timber
import java.util.*

class UserTokenRefreshInterceptor(
    private val localStorage: ILocalStorage,
    private val tokenRefresher: ITokenRefresher
) : Interceptor {

    companion object {
        private const val EXPIRED_RESPONSE = "User Token Expired"
        private const val LOGGED_OUT_RESPONSE = "Access Denied"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val token = localStorage.getUserToken()

        val originalRequest = chain.request()
        val response = chain.proceed(originalRequest.newBuilder().build())

        Timber.d("Original response code: %d", response.code)
        Timber.d("Original url: %s", originalRequest.url.toString())

        if (response.code == 401 && !originalRequest.url.toString().contains("login")) {

            Timber.d("Detected 401 response")

            var error = ""

            try {
                val responseData = response.body?.string()
                if (responseData != null) {
                    val json = JSONObject(responseData)
                    Timber.d("Raw 401 response - $json")
                    error = json.getString("error_message")
                }
            } catch (e: JSONException) {
                Timber.e(e)
            }

            if (error.toLowerCase(Locale.ROOT) == EXPIRED_RESPONSE.toLowerCase(Locale.ROOT) ||
                error.toLowerCase(Locale.ROOT) == LOGGED_OUT_RESPONSE.toLowerCase(Locale.ROOT)
            ) {
                synchronized(this) {
                    val currentToken = localStorage.getUserToken()

                    if (currentToken == token) {
                        // need to refresh token
                        val success = refreshToken()
                        Timber.d("Refresh token success: $success")
                        if (!success) {
                            Timber.d("Refresh token failed, returning original response")
                            return response
                        }
                    }

                    Timber.d("Refresh token success, retrying original request")
                    return chain.proceed(originalRequest)
                }
            } else {
                Timber.d("Dont recognize this type of 401 error, returning original response")
                return response
            }
        } else {
            if (response.code / 100 != 2) {
                Timber.d("Error logging in OR not 401 error, returning original response")
            }
            return response
        }
    }

    private fun refreshToken(): Boolean {
        Timber.d("Refreshing token")
        val securityToken = localStorage.getSecurityKey()
        val apiToken = localStorage.getApiToken()

        if (apiToken.isNotEmpty() && securityToken.isNotEmpty()) {

            val fcmToken = localStorage.getFcmToken()
            val deviceId = localStorage.getDeviceId()
            val email = localStorage.getUserEmail()
            val password = localStorage.getUserPassword()

            val newToken = tokenRefresher.refresh(
                email = email,
                password = password,
                deviceId = deviceId,
                firebaseId = fcmToken
            )

            return if (newToken != null) {
                localStorage.setUserToken(newToken)
                true
            } else {
                false
            }
        }

        return false
    }
}

interface ITokenRefresher {
    fun refresh(email: String, password: String, deviceId: String, firebaseId: String): String?
}

class TokenRefresher(private val authApi: Lazy<AuthApi>) : ITokenRefresher {
    override fun refresh(
        email: String,
        password: String,
        deviceId: String,
        firebaseId: String
    ): String? {
        return try {
            val response =
                authApi.get().loginSync(AuthApi.ApiLoginBody(email, password, deviceId, firebaseId))
                    .execute()
            response.body()?.userToken
        } catch (e: Exception) {
            null
        }
    }
}