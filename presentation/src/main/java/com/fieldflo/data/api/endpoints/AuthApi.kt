package com.fieldflo.data.api.endpoints

import com.fieldflo.data.api.entities.UserLoggedInRestObject
import com.fieldflo.data.api.interceptors.ApiVersion
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthApi {

    @ApiVersion(2)
    @POST("login")
    suspend fun login(@Body loginBody: ApiLoginBody): ApiLoginResponse

    @ApiVersion(2)
    @POST("login")
    fun loginSync(@Body loginBody: ApiLoginBody): Call<ApiLoginResponse>

    @ApiVersion(2)
    @POST("getLoggedInEmployee")
    suspend fun getLoggedInUser(): UserLoggedInRestObject

    @JsonClass(generateAdapter = true)
    data class ApiLoginBody(
        @field:Json(name = "email")
        val email: String,

        @field:Json(name = "password")
        val password: String,

        @field:Json(name = "deviceID")
        val deviceID: String,

        @field:Json(name = "firebaseID")
        val firebaseID: String
    )

    @JsonClass(generateAdapter = true)
    data class ApiLoginResponse(
        @field:Json(name = "userToken")
        val userToken: String,

        @field:Json(name = "companyID")
        val companyId: Int
    )
}