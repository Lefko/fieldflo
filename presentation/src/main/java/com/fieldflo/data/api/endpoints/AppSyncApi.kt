package com.fieldflo.data.api.endpoints

import com.fieldflo.data.api.entities.ModificationRestObject
import com.fieldflo.data.api.interceptors.ApiVersion
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import retrofit2.http.Body
import retrofit2.http.POST

interface AppSyncApi {
    @ApiVersion(2)
    @POST("updateDeviceAndFirebaseID")
    suspend fun updateFirebaseToken(@Body updateFirebaseTokenBody: UpdateFirebaseTokenBody): UpdateFirebaseTokenResponse

    @ApiVersion(2)
    @POST("checkForModifications")
    suspend fun checkForModifications(@Body checkForModificationsBody: CheckForModificationsBody): List<ModificationRestObject>

    @ApiVersion(2)
    @POST("setUpdated")
    suspend fun setUpdated(@Body setUpdatedBody: SetUpdatedBody): SetUpdatedResponse
}

@JsonClass(generateAdapter = true)
data class UpdateFirebaseTokenBody(

    @field:Json(name = "deviceID")
    val deviceId: String,

    @field:Json(name = "firebaseID")
    val fcmToken: String
)

@JsonClass(generateAdapter = true)
data class UpdateFirebaseTokenResponse(
    @field:Json(name = "status")
    val status: Boolean
)

@JsonClass(generateAdapter = true)
data class CheckForModificationsBody(
    @field:Json(name = "deviceID")
    val deviceId: String
)

@JsonClass(generateAdapter = true)
data class SetUpdatedBody(
    @field:Json(name = "deviceID")
    val deviceId: String
)

@JsonClass(generateAdapter = true)
data class SetUpdatedResponse(
    @field:Json(name = "status")
    val status: Boolean
)
