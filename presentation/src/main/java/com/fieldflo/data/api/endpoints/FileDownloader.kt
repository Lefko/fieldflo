package com.fieldflo.data.api.endpoints

import kotlinx.coroutines.suspendCancellableCoroutine
import okhttp3.*
import okio.BufferedSource
import okio.buffer
import okio.sink
import timber.log.Timber
import java.io.File
import java.io.IOException
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class FileDownloader @Inject constructor(
    private val okHttpClient: OkHttpClient
) {
    suspend fun downloadFile(url: String, dest: File): Boolean {
        val request = Request.Builder().url(url).build()
        val response = okHttpClient.newCall(request).executeSuspended()
        return if (!response.isSuccessful || response.body == null || response.body!!.contentLength() <= 0) {
            Timber.e("Error downloading file $url")
            false
        } else {
            val source = response.body!!.source()
            saveResponseToFile(source, dest)
            true
        }
    }

    private suspend fun Call.executeSuspended(): Response {
        return suspendCancellableCoroutine { continuation ->
            this.enqueue(object : Callback {
                override fun onResponse(call: Call, response: Response) {
                    if (continuation.isActive) {
                        continuation.resume(response)
                    }
                }

                override fun onFailure(call: Call, e: IOException) {
                    if (continuation.isActive) {
                        continuation.resumeWithException(e)
                    }
                }
            })
        }
    }

    private fun saveResponseToFile(source: BufferedSource, dest: File) {
        val sink = dest.sink().buffer()
        val sinkBuffer = sink.buffer

        var totalBytesRead: Long = 0
        val bufferSize = 8 * 1024L
        try {
            var bytesRead = source.read(sinkBuffer, bufferSize)
            while (bytesRead != -1L) {
                sink.emit()
                totalBytesRead += bytesRead
                bytesRead = source.read(sinkBuffer, bufferSize)
            }
        } catch (e: Exception) {
            Timber.e(e)
        } finally {
            sink.flush()
            sink.close()
            source.close()
        }
    }
}