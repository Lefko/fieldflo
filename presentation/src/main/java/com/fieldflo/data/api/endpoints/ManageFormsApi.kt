package com.fieldflo.data.api.endpoints

import com.fieldflo.data.api.interceptors.ApiVersion
import com.fieldflo.data.persistence.db.entities.*
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import retrofit2.http.Body
import retrofit2.http.POST
import java.util.*

interface ManageFormsApi {

    @ApiVersion(2)
    @POST("formDailyFieldReportSave")
    suspend fun saveDailyFieldReport(
        @Body saveDailyFieldReportRequestBody: SaveDailyFieldReportRequestBody
    ): DailyFieldReportResponse

    @ApiVersion(2)
    @POST("formDailyLogSave")
    suspend fun saveDailyLog(@Body saveDailyLogRequestBody: SaveDailyLogRequestBody)

    @ApiVersion(2)
    @POST("formPSIJHASave")
    suspend fun savePsi(
        @Body savePsiRequestBody: SavePsiRequestBody
    ): PsiResponse

    @ApiVersion(2)
    @POST("formTimesheetSave")
    suspend fun saveTimeSheet(
        @Body saveTimeSheetRequestBody: SaveTimeSheetRequestBody
    ): TimesheetResponse

    @ApiVersion(3)
    @POST("formDailyDemoLogSave")
    suspend fun saveDailyDemoLog(@Body saveDailyDemoRequestBody: SaveDailyDemoRequestBody): DailyDemoLogResponse

    @ApiVersion(2)
    @POST("saveLog")
    suspend fun logSyncError(@Body logSyncErrorBody: LogSyncErrorBody)

    @JsonClass(generateAdapter = true)
    data class SaveDailyFieldReportRequestBody(

        @field:Json(name = "externalID")
        val uuid: String,

        @field:Json(name = "formDate")
        val formDate: Date,

        @field:Json(name = "closeDate")
        val closeDate: Date,

        @field:Json(name = "projectID")
        val projectId: Int,

        @field:Json(name = "workCompletedToday")
        val workCompletedToday: String,

        // optional fields
        @field:Json(name = "supervisorID")
        val supervisorId: Int? = null,

        @field:Json(name = "projectManagerID")
        val projectManagerId: Int? = null,

        @field:Json(name = "scopeReceived")
        val scopeReceived: String? = null,

        @field:Json(name = "scopeDate")
        val scopeDate: Date? = null,

        @field:Json(name = "scopeStructure")
        val scopeStructure: String? = null,

        @field:Json(name = "whatWentRight")
        val whatWentRight: String? = null,

        @field:Json(name = "whatCanBeImproved")
        val whatCanBeImproved: String? = null,

        @field:Json(name = "reportableIncident")
        val reportableIncident: String? = null,

        @field:Json(name = "incident")
        val incident: String? = null,

        @field:Json(name = "otherIncident")
        val otherIncident: String? = null,

        @field:Json(name = "stopWorkPerformed")
        val stopWorkPerformed: String? = null,

        @field:Json(name = "swpNote")
        val swpNote: String? = null,

        @field:Json(name = "goalsForTomorrow")
        val goalsForTomorrow: String? = null,

        @field:Json(name = "goalsFotTheWeek")
        val goalsForTheWeek: String? = null,

        @field:Json(name = "actionRequired")
        val actionRequired: String? = null,

        @field:Json(name = "projectNotesOrSpecialConsiderations")
        val projectNotesOrSpecialConsiderations: String? = null,

        @field:Json(name = "meetingLog")
        val meetingLog: String? = null,

        @field:Json(name = "visitors")
        val visitors: String? = null,

        @field:Json(name = "workflow")
        val workflow: String? = null,

        @field:Json(name = "bulkEquipmentInUseThisDay")
        val bulkEquipmentInUseThisDay: String? = null,

        @field:Json(name = "acmDetected")
        val acmDetected: String? = null,

        @field:Json(name = "acmBuildDate")
        val acmBuildDate: Date? = null,

        @field:Json(name = "acmSurveyDate")
        val acmSurveyDate: Date? = null,

        @field:Json(name = "leadDetected")
        val leadDetected: String? = null,

        @field:Json(name = "leadTestDate")
        val leadTestDate: Date? = null,

        @field:Json(name = "moldDetected")
        val moldDetected: String? = null,

        @field:Json(name = "moldDateOdProtocol")
        val moldTestDate: Date? = null,

        @field:Json(name = "notesRelatedToEnvironmentalTesting")
        val notesRelatedToEnvironmentalTesting: String? = null,

        @field:Json(name = "estimatedCompletionDate")
        val estimatedCompletionDate: Date? = null,

        @field:Json(name = "daysRemainingOnProject")
        val daysRemainingOnProject: String? = null,

        @field:Json(name = "activitiesDoneTodayInstallCriticals")
        val activitiesDoneTodayInstallCriticals: Boolean? = null,

        @field:Json(name = "activitiesDoneTodayRemoval")
        val activitiesDoneTodayRemoval: Boolean? = null,

        @field:Json(name = "activitiesDoneTodayEstablishNegativePressure")
        val activitiesDoneTodayEstablishNegativePressure: Boolean? = null,

        @field:Json(name = "activitiesDoneTodayBagOut")
        val activitiesDoneTodayBagOut: Boolean? = null,

        @field:Json(name = "activitiesDoneTodayConstructDecon")
        val activitiesDoneTodayConstructDecon: Boolean? = null,

        @field:Json(name = "activitiesDoneTodayFinalCleanOrDecontamination")
        val activitiesDoneTodayFinalCleanOrDecontamination: Boolean? = null,

        @field:Json(name = "activitiesDoneTodayPreClean")
        val activitiesDoneTodayPreClean: Boolean? = null,

        @field:Json(name = "activitiesDoneTodayClearance")
        val activitiesDoneTodayClearance: Boolean? = null,

        @field:Json(name = "activitiesDoneTodayCoverFixedObjects")
        val activitiesDoneTodayCoverFixedObjects: Boolean? = null,

        @field:Json(name = "activitiesDoneTodayTearDown")
        val activitiesDoneTodayTearDown: Boolean? = null,

        @field:Json(name = "activitiesDoneTodayConstructContainment")
        val activitiesDoneTodayConstructContainment: Boolean? = null,

        @field:Json(name = "activitiesDoneTodayDemob")
        val activitiesDoneTodayDemob: Boolean? = null,

        @field:Json(name = "cleaningVerificationExterior")
        val cleaningVerificationExterior: Boolean? = null,

        @field:Json(name = "cleaningVerificationInterior")
        val cleaningVerificationInterior: Boolean? = null,

        @field:Json(name = "workAreaRoomLocations1")
        val workAreaRoomLocations1: String? = null,

        @field:Json(name = "workAreaRoomLocations2")
        val workAreaRoomLocations2: String? = null,

        @field:Json(name = "workAreaRoomLocations3")
        val workAreaRoomLocations3: String? = null,

        @field:Json(name = "numberOfCleaningClothsUtilized1")
        val numberOfCleaningClothsUtilized1: String? = null,

        @field:Json(name = "numberOfCleaningClothsUtilized2")
        val numberOfCleaningClothsUtilized2: String? = null,

        @field:Json(name = "numberOfCleaningClothsUtilized3")
        val numberOfCleaningClothsUtilized3: String? = null,

        @field:Json(name = "whatCleaningVerifiationDidYouPassOn1")
        val whatCleaningVerificationDidYouPassOn1: String? = null,

        @field:Json(name = "whatCleaningVerifiationDidYouPassOn2")
        val whatCleaningVerificationDidYouPassOn2: String? = null,

        @field:Json(name = "whatCleaningVerifiationDidYouPassOn3")
        val whatCleaningVerificationDidYouPassOn3: String? = null
    ) {
        companion object {
            fun fromDailyFieldReportFormData(
                data: DailyFieldReportFormData,
                closeDate: Date
            ): SaveDailyFieldReportRequestBody {
                val uniqueId =
                    "${data.companyId}_${data.projectId}_${data.internalFormId}_${data.formDataId}"
                return SaveDailyFieldReportRequestBody(
                    uuid = uniqueId,
                    formDate = data.formDate,
                    closeDate = closeDate,
                    projectId = data.projectId,
                    workCompletedToday = data.workCompletedToday,
                    scopeReceived = if (data.scopeReceived.isEmpty()) null else data.scopeReceived,
                    scopeDate = if (data.scopeDate.time == 0L) null else data.scopeDate,
                    scopeStructure = if (data.scopeStructure.isEmpty()) null else data.scopeStructure,
                    goalsForTomorrow = if (data.goalsForTomorrow.isEmpty()) null else data.goalsForTomorrow,
                    goalsForTheWeek = if (data.goalsForTheWeek.isEmpty()) null else data.goalsForTheWeek,
                    actionRequired = if (data.actionRequired.isEmpty()) null else data.actionRequired,
                    projectNotesOrSpecialConsiderations = if (data.projectNotesOrSpecialConsiderations.isEmpty()) null else data.projectNotesOrSpecialConsiderations,
                    meetingLog = if (data.meetingLog.isEmpty()) null else data.meetingLog,
                    visitors = if (data.visitors.isEmpty()) null else data.visitors,
                    workflow = if (data.workflow.isEmpty()) null else data.workflow,
                    bulkEquipmentInUseThisDay = if (data.bulkEquipmentInUseThisDay.isEmpty()) null else data.bulkEquipmentInUseThisDay,
                    acmDetected = if (data.acmDetected.isEmpty()) null else data.acmDetected,
                    acmBuildDate = if (data.acmBuildDate.time == 0L) null else data.acmBuildDate,
                    acmSurveyDate = if (data.acmSurveyDate.time == 0L) null else data.acmSurveyDate,
                    leadDetected = if (data.leadDetected.isEmpty()) null else data.leadDetected,
                    leadTestDate = if (data.leadTestDate.time == 0L) null else data.leadTestDate,
                    moldDetected = if (data.moldDetected.isEmpty()) null else data.moldDetected,
                    moldTestDate = if (data.moldTestDate.time == 0L) null else data.moldTestDate,
                    notesRelatedToEnvironmentalTesting = if (data.notesRelatedToEnvironmentalTesting.isEmpty()) null else data.notesRelatedToEnvironmentalTesting,
                    estimatedCompletionDate = if (data.estimatedCompletionDate.time == 0L) null else data.estimatedCompletionDate,
                    daysRemainingOnProject = if (data.daysRemainingOnProject.isEmpty()) null else data.daysRemainingOnProject,
                    activitiesDoneTodayInstallCriticals = data.activitiesDoneTodayInstallCriticals,
                    activitiesDoneTodayRemoval = data.activitiesDoneTodayRemoval,
                    activitiesDoneTodayEstablishNegativePressure = data.activitiesDoneTodayEstablishNegativePressure,
                    activitiesDoneTodayBagOut = data.activitiesDoneTodayBagOut,
                    activitiesDoneTodayConstructDecon = data.activitiesDoneTodayConstructDecon,
                    activitiesDoneTodayFinalCleanOrDecontamination = data.activitiesDoneTodayFinalCleanOrDecontamination,
                    activitiesDoneTodayPreClean = data.activitiesDoneTodayPreClean,
                    activitiesDoneTodayClearance = data.activitiesDoneTodayClearance,
                    activitiesDoneTodayCoverFixedObjects = data.activitiesDoneTodayCoverFixedObjects,
                    activitiesDoneTodayTearDown = data.activitiesDoneTodayTearDown,
                    activitiesDoneTodayConstructContainment = data.activitiesDoneTodayConstructContainment,
                    activitiesDoneTodayDemob = data.activitiesDoneTodayDemob,
                    cleaningVerificationExterior = data.cleaningVerificationExterior,
                    cleaningVerificationInterior = data.cleaningVerificationInterior,
                    workAreaRoomLocations1 = if (data.workAreaRoomLocations1.isEmpty()) null else data.workAreaRoomLocations1,
                    workAreaRoomLocations2 = if (data.workAreaRoomLocations2.isEmpty()) null else data.workAreaRoomLocations2,
                    workAreaRoomLocations3 = if (data.workAreaRoomLocations3.isEmpty()) null else data.workAreaRoomLocations3,
                    numberOfCleaningClothsUtilized1 = data.numberOfCleaningClothsUtilized1.toString(),
                    numberOfCleaningClothsUtilized2 = data.numberOfCleaningClothsUtilized2.toString(),
                    numberOfCleaningClothsUtilized3 = data.numberOfCleaningClothsUtilized3.toString(),
                    whatCleaningVerificationDidYouPassOn1 = if (data.whatCleaningVerificationDidYouPassOn1.isEmpty()) null else data.whatCleaningVerificationDidYouPassOn1,
                    whatCleaningVerificationDidYouPassOn2 = if (data.whatCleaningVerificationDidYouPassOn2.isEmpty()) null else data.whatCleaningVerificationDidYouPassOn2,
                    whatCleaningVerificationDidYouPassOn3 = if (data.whatCleaningVerificationDidYouPassOn3.isEmpty()) null else data.whatCleaningVerificationDidYouPassOn3,
                    whatWentRight = if (data.whatWentRight.isEmpty()) null else data.whatWentRight,
                    whatCanBeImproved = if (data.whatCanBeImproved.isEmpty()) null else data.whatCanBeImproved,
                    reportableIncident = if (data.reportableIncident.isEmpty()) null else data.reportableIncident,
                    incident = if (data.incident.isEmpty()) null else data.incident,
                    otherIncident = if (data.otherIncident.isEmpty()) null else data.otherIncident,
                    stopWorkPerformed = if (data.stopWorkPerformed.isEmpty()) null else data.stopWorkPerformed,
                    swpNote = if (data.swpNote.isEmpty()) null else data.swpNote,
                    supervisorId = if (data.supervisorId == 0) null else data.supervisorId,
                    projectManagerId = if (data.projectManagerId == 0) null else data.projectManagerId
                )
            }
        }
    }

    @JsonClass(generateAdapter = true)
    data class SaveDailyLogRequestBody(

        @field:Json(name = "externalID")
        val uuid: String,

        @field:Json(name = "formDate")
        val formDate: Date? = null,

        @field:Json(name = "closeDate")
        val closeDate: Date,

        @field:Json(name = "projectID")
        val projectId: Int,

        @field:Json(name = "supervisorID")
        val supervisorId: Int,

        @field:Json(name = "supervisorVerifyEmployeeID")
        val supervisorVerifyEmployeeId: Int,

        @field:Json(name = "supervisorVerifyDate")
        val supervisorVerifyDate: Date? = null,

        @field:Json(name = "shiftStart")
        val shiftStart: String? = null,

        @field:Json(name = "shiftEnd")
        val shiftEnd: String? = null,

        @field:Json(name = "dailyLog")
        val dailyLog: String? = null,

        @field:Json(name = "visitorsToSite")
        val visitorsToSite: String? = null,

        @field:Json(name = "statusAtQuittingTime")
        val statusAtQuittingTime: String? = null,

        @field:Json(name = "inspectionsMadeOrTestsPerformed")
        val inspectionsMadeOrTestsPerformed: String? = null,

        @field:Json(name = "dailyBagCount")
        val dailyBagCount: String? = null,

        @field:Json(name = "projectToDateBagCount")
        val projectToDateBagCount: String? = null,

        @field:Json(name = "containerNo")
        val containerNo: String? = null,

        @field:Json(name = "bagsInContainerToDate")
        val bagsInContainerToDate: String? = null,

        @field:Json(name = "unusualConditionsOrProblemsAndActionTaken")
        val unusualConditionsOrProblemsAndActionTaken: String? = null
    ) {
        companion object {
            fun fromDailyLogFormData(
                data: DailyLogFormData,
                closeDate: Date
            ): SaveDailyLogRequestBody {
                val uniqueId =
                    "${data.companyId}_${data.projectId}_${data.internalFormId}_${data.formDataId}"
                return SaveDailyLogRequestBody(
                    uuid = uniqueId,
                    formDate = if (data.formDate.time == 0L) null else data.formDate,
                    closeDate = closeDate,
                    projectId = data.projectId,
                    shiftStart = if (data.shiftStart.isEmpty()) null else data.shiftStart,
                    shiftEnd = if (data.shiftEnd.isEmpty()) null else data.shiftEnd,
                    supervisorId = data.supervisorId,
                    supervisorVerifyEmployeeId = data.supervisorId,
                    supervisorVerifyDate = if (data.supervisorVerifyDate.time == 0L) null else data.supervisorVerifyDate,
                    dailyLog = if (data.dailyLog.isEmpty()) null else data.dailyLog,
                    visitorsToSite = if (data.visitorsToSite.isEmpty()) null else data.visitorsToSite,
                    statusAtQuittingTime = if (data.statusAtQuittingTime.isEmpty()) null else data.statusAtQuittingTime,
                    inspectionsMadeOrTestsPerformed = if (data.inspectionsMadeOrTestsPerformed.isEmpty()) null else data.inspectionsMadeOrTestsPerformed,
                    dailyBagCount = if (data.dailyBagCount.isEmpty()) null else data.dailyBagCount,
                    projectToDateBagCount = if (data.projectToDateBagCount.isEmpty()) null else data.projectToDateBagCount,
                    containerNo = if (data.containerNo.isEmpty()) null else data.containerNo,
                    bagsInContainerToDate = if (data.bagsInContainerToDate.isEmpty()) null else data.bagsInContainerToDate,
                    unusualConditionsOrProblemsAndActionTaken = if (data.unusualConditionsOrProblemsAndActionTaken.isEmpty()) null else data.unusualConditionsOrProblemsAndActionTaken
                )
            }
        }
    }

    @JsonClass(generateAdapter = true)
    data class SavePsiRequestBody(

        @field:Json(name = "externalID")
        val uuid: String,

        @field:Json(name = "formDate")
        val formDate: Date? = null,

        @field:Json(name = "closeDate")
        val closeDate: Date,

        @field:Json(name = "projectID")
        val projectId: Int,

        @field:Json(name = "supervisorID")
        val supervisorId: Int,

        @field:Json(name = "supervisorVerifyEmployeeID")
        val supervisorVerifyEmployeeId: Int,

        @field:Json(name = "supervisorVerifyDate")
        val supervisorVerifiedDate: Date? = null,

        @field:Json(name = "auditorID")
        val auditorId: Int? = null,

        @field:Json(name = "auditorVerifyEmployeeID")
        val auditorVerifyEmployeeId: Int? = null,

        @field:Json(name = "auditorVerifyDate")
        val auditorVerifyDate: Date? = null,

        @field:Json(name = "taskLocation")
        val taskLocation: String? = null,

        @field:Json(name = "muster")
        val musterMeetingPoint: String? = null,

        @field:Json(name = "RV_splitPotential")
        val spillPotential: Boolean,

        @field:Json(name = "RV_hazmatStorage")
        val hazmatStorage: Boolean,

        @field:Json(name = "RV_weather")
        val weather: Boolean,

        @field:Json(name = "RV_SDSReviewedForHazardousMaterials")
        val sdsReviewForHazmat: Boolean,

        @field:Json(name = "RV_ventilationRequired")
        val ventilationRequired: Boolean,

        @field:Json(name = "RV_heatStress")
        val heatStress: Boolean,

        @field:Json(name = "RV_lightningLevelsTooLow")
        val lightningLevelsTooLow: Boolean,

        @field:Json(name = "RV_housekeeping")
        val housekeeping: Boolean,

        @field:Json(name = "RV_workingInaTightArea")
        val workingTightArea: Boolean,

        @field:Json(name = "RV_partOfBodyInLineOfFire")
        val partOfBodyInLineOfFire: Boolean,

        @field:Json(name = "RV_workingAboveYourHead")
        val workingAboveYourHead: Boolean,

        @field:Json(name = "RV_pinchPointsIdentified")
        val pinchPointsIdentified: Boolean,

        @field:Json(name = "RV_repetativeMotion")
        val repetitiveMotion: Boolean,

        @field:Json(name = "RV_barricadesFlaggingAndSignInPlace")
        val barricadesFlaggingAndSignInPlace: Boolean,

        @field:Json(name = "RV_holeCoveringsInPlace")
        val holeCoveringsInPlace: Boolean,

        @field:Json(name = "RV_protectFromFallingItems")
        val protectFromFallingItems: Boolean,

        @field:Json(name = "RV_poweredPlatforms")
        val poweredPlatforms: Boolean,

        @field:Json(name = "RV_othersWorkingOverhead")
        val othersWorkingOverhead: Boolean,

        @field:Json(name = "RV_fallArrestSystems")
        val fallArrestSystems: Boolean,

        @field:Json(name = "RV_ladders")
        val ladders: Boolean,

        @field:Json(name = "RV_welding")
        val welding: Boolean,

        @field:Json(name = "RV_burnSources")
        val burnSources: Boolean,

        @field:Json(name = "RV_compressedGasses")
        val compressedGasses: Boolean,

        @field:Json(name = "RV_workingOnEnergizedEquipment")
        val workingOnEnergizedEquipment: Boolean,

        @field:Json(name = "RV_electricalCordsCondition")
        val electricalCordsCondition: Boolean,

        @field:Json(name = "RV_equipmentInspected")
        val equipmentInspected: Boolean,

        @field:Json(name = "RV_criticalLiftMeetingRequired")
        val criticalLiftMeetingRequired: Boolean,

        @field:Json(name = "RV_energyIsolation")
        val energyIsolation: Boolean,

        @field:Json(name = "RV_airbornParticles")
        val airborneParticles: Boolean,

        @field:Json(name = "RV_openHoles")
        val openHoles: Boolean,

        @field:Json(name = "RV_mobileEquipment")
        val mobileEquipment: Boolean,

        @field:Json(name = "RV_rigging")
        val rigging: Boolean,

        @field:Json(name = "RV_excavation")
        val excavation: Boolean,

        @field:Json(name = "RV_confinedSpace")
        val confinedSpace: Boolean,

        @field:Json(name = "RV_scafold")
        val scaffold: Boolean,

        @field:Json(name = "RV_slipPotentialIdentified")
        val slipPotentialIdentified: Boolean,

        @field:Json(name = "RV_requiredPermitsInPlace")
        val requiredPermitsInPlace: Boolean,

        @field:Json(name = "RV_excavations")
        val excavations: Boolean,

        @field:Json(name = "RV_walkways")
        val walkways: Boolean,

        @field:Json(name = "RV_other")
        val other: String? = null,

        @field:Json(name = "RV_clearInstructionsProvided")
        val clearInstructionsProvided: Boolean,

        @field:Json(name = "RV_trainedToUseToolAndPerformTask")
        val trainedToUseToolAndPerformTask: Boolean,

        @field:Json(name = "RV_distractionsInWorkArea")
        val distractionsInWorkArea: Boolean,

        @field:Json(name = "RV_workingAlone")
        val workingAlone: Boolean,

        @field:Json(name = "RV_liftTooHeavy")
        val liftTooHeavy: Boolean,

        @field:Json(name = "RV_externalNoiseLevel")
        val externalNoiseLevel: Boolean,

        @field:Json(name = "RV_physicalLimitations")
        val physicalLimitations: Boolean,

        @field:Json(name = "RV_firstAidRequirements")
        val firstAidRequirements: Boolean,

        @field:Json(name = "RV_goggles")
        val goggles: Boolean,

        @field:Json(name = "RV_faceShield")
        val faceShield: Boolean,

        @field:Json(name = "RV_gloves")
        val gloves: Boolean,

        @field:Json(name = "RV_coverall")
        val coverall: Boolean,

        @field:Json(name = "RV_hearingProtection")
        val hearingProtection: Boolean,

        @field:Json(name = "RV_respirator")
        val respirator: Boolean,

        @field:Json(name = "RV_respiratorName") //"Half Face" / "Full Face" / "PAPR" / "Supplied Air"
        val respiratorName: String? = null,

        @field:Json(name = "RV_harness")
        val harness: Boolean,

        @field:Json(name = "RV_reflectiveVest")
        val reflectiveVest: Boolean,

        @field:Json(name = "RV_footwear")
        val footwear: Boolean,

        @field:Json(name = "RV_safetyGlasses")
        val safetyGlasses: Boolean,

        @field:Json(name = "RV_weldingHood")
        val weldingHood: Boolean,

        @field:Json(name = "RV_tyvexSuit")
        val tyvexSuit: Boolean,

        @field:Json(name = "RV_retractableLanyard")
        val retractableLanyard: Boolean,

        @field:Json(name = "RV_hardHat")
        val hardHat: Boolean,

        @field:Json(name = "tasks")
        val tasks: List<PsiTaskRequestBody> = emptyList(),

        @field:Json(name = "weatherAccepted")
        val reviewedWeather: Boolean,

        @field:Json(name = "weatherRoad")
        val reviewedRoad: Boolean,

        @field:Json(name = "weatherIndex")
        val reviewedHeatIndex: Boolean,

        @field:Json(name = "signatures")
        val signatures: List<PsiSignatureRequestBody> = emptyList(),

        @field:Json(name = "MS_taskDescription")
        val taskDescription: String? = null,

        @field:Json(name = "MS_hazardIdentification")
        val hazardIdentification: String? = null,

        @field:Json(name = "MS_hazardControls")
        val hazardControls: String? = null,

        @field:Json(name = "MS_allSectionsImplemented")
        val allSectionsImplemented: String? = null,

        @field:Json(name = "MS_workersNamesLegible")
        val workersNamesLegible: String? = null,

        @field:Json(name = "MS_reviewed")
        val reviewedByForeman: String? = null,

        @field:Json(name = "MS_musterPointIdentified")
        val musterPointIdentified: String? = null,

        @field:Json(name = "MS_toolsAndEquipmentInspected")
        val toolsAndEquipmentInspected: String? = null,

        @field:Json(name = "MS_PSIAtTaskLocation")
        val psiAtTaskLocation: String? = null,

        @field:Json(name = "MS_comments")
        val comments: String? = null
    ) {
        companion object {
            fun fromPsiFormData(psiFormData: PsiFormData, closeDate: Date): SavePsiRequestBody {
                val uniqueId =
                    "${psiFormData.companyId}_${psiFormData.projectId}_${psiFormData.internalFormId}_${psiFormData.formDataId}"
                return SavePsiRequestBody(
                    uuid = uniqueId,
                    formDate = if (psiFormData.formDate.time == 0L) null else psiFormData.formDate,
                    closeDate = closeDate,
                    projectId = psiFormData.projectId,
                    supervisorId = psiFormData.supervisorId,
                    supervisorVerifiedDate = if (psiFormData.supervisorVerifiedDate.time == 0L) null else psiFormData.supervisorVerifiedDate,
                    supervisorVerifyEmployeeId = psiFormData.supervisorVerifiedByEmployeeId,
                    auditorId = if (psiFormData.auditorId == 0) null else psiFormData.auditorId,
                    auditorVerifyEmployeeId = if (psiFormData.auditorVerifiedByEmployeeId == 0) null else psiFormData.auditorVerifiedByEmployeeId,
                    auditorVerifyDate = if (psiFormData.auditorVerifiedDate.time == 0L) null else psiFormData.auditorVerifiedDate,
                    taskLocation = if (psiFormData.taskLocation.isEmpty()) null else psiFormData.taskLocation,
                    musterMeetingPoint = if (psiFormData.musterMeetingPoint.isEmpty()) null else psiFormData.musterMeetingPoint,
                    spillPotential = psiFormData.spillPotential,
                    hazmatStorage = psiFormData.hazmatStorage,
                    weather = psiFormData.weather,
                    sdsReviewForHazmat = psiFormData.sdsReviewForHazmat,
                    ventilationRequired = psiFormData.ventilationRequired,
                    heatStress = psiFormData.heatStress,
                    lightningLevelsTooLow = psiFormData.lightningLevelsTooLow,
                    housekeeping = psiFormData.housekeeping,
                    workingTightArea = psiFormData.workingTightArea,
                    partOfBodyInLineOfFire = psiFormData.partOfBodyInLineOfFire,
                    workingAboveYourHead = psiFormData.workingAboveYourHead,
                    pinchPointsIdentified = psiFormData.pinchPointsIdentified,
                    repetitiveMotion = psiFormData.repetitiveMotion,
                    barricadesFlaggingAndSignInPlace = psiFormData.barricadesFlaggingAndSignInPlace,
                    holeCoveringsInPlace = psiFormData.holeCoveringsInPlace,
                    protectFromFallingItems = psiFormData.protectFromFallingItems,
                    poweredPlatforms = psiFormData.poweredPlatforms,
                    othersWorkingOverhead = psiFormData.othersWorkingOverhead,
                    fallArrestSystems = psiFormData.fallArrestSystems,
                    ladders = psiFormData.ladders,
                    welding = psiFormData.welding,
                    burnSources = psiFormData.burnSources,
                    compressedGasses = psiFormData.compressedGasses,
                    workingOnEnergizedEquipment = psiFormData.workingOnEnergizedEquipment,
                    electricalCordsCondition = psiFormData.electricalCordsCondition,
                    equipmentInspected = psiFormData.equipmentInspected,
                    criticalLiftMeetingRequired = psiFormData.criticalLiftMeetingRequired,
                    energyIsolation = psiFormData.energyIsolation,
                    airborneParticles = psiFormData.airborneParticles,
                    openHoles = psiFormData.openHoles,
                    mobileEquipment = psiFormData.mobileEquipment,
                    rigging = psiFormData.rigging,
                    excavation = psiFormData.excavation,
                    confinedSpace = psiFormData.confinedSpace,
                    scaffold = psiFormData.scaffold,
                    slipPotentialIdentified = psiFormData.slipPotentialIdentified,
                    requiredPermitsInPlace = psiFormData.requiredPermitsInPlace,
                    excavations = psiFormData.excavations,
                    walkways = psiFormData.walkways,
                    other = if (psiFormData.other.isEmpty()) null else psiFormData.other,
                    clearInstructionsProvided = psiFormData.clearInstructionsProvided,
                    trainedToUseToolAndPerformTask = psiFormData.trainedToUseToolAndPerformTask,
                    distractionsInWorkArea = psiFormData.distractionsInWorkArea,
                    workingAlone = psiFormData.workingAlone,
                    liftTooHeavy = psiFormData.liftTooHeavy,
                    externalNoiseLevel = psiFormData.externalNoiseLevel,
                    physicalLimitations = psiFormData.physicalLimitations,
                    firstAidRequirements = psiFormData.firstAidRequirements,
                    goggles = psiFormData.goggles,
                    faceShield = psiFormData.faceShield,
                    gloves = psiFormData.gloves,
                    coverall = psiFormData.coverall,
                    hearingProtection = psiFormData.hearingProtection,
                    respirator = psiFormData.respirator,
                    respiratorName = if (psiFormData.respiratorName.isEmpty()) null else psiFormData.respiratorName,
                    harness = psiFormData.harness,
                    reflectiveVest = psiFormData.reflectiveVest,
                    footwear = psiFormData.footwear,
                    safetyGlasses = psiFormData.safetyGlasses,
                    weldingHood = psiFormData.weldingHood,
                    tyvexSuit = psiFormData.tyvexSuit,
                    retractableLanyard = psiFormData.retractableLanyard,
                    hardHat = psiFormData.hardHat,
                    reviewedWeather = psiFormData.reviewedWeather!!,
                    reviewedRoad = psiFormData.reviewedRoad!!,
                    reviewedHeatIndex = psiFormData.reviewedHeatIndex!!,
                    taskDescription = if (psiFormData.taskDescription.isEmpty()) null else psiFormData.taskDescription,
                    hazardIdentification = if (psiFormData.hazardIdentification.isEmpty()) null else psiFormData.hazardIdentification,
                    hazardControls = if (psiFormData.hazardControls.isEmpty()) null else psiFormData.hazardControls,
                    allSectionsImplemented = if (psiFormData.allSectionsImplemented.isEmpty()) null else psiFormData.allSectionsImplemented,
                    workersNamesLegible = if (psiFormData.workersNamesLegible.isEmpty()) null else psiFormData.workersNamesLegible,
                    reviewedByForeman = if (psiFormData.reviewedByForeman.isEmpty()) null else psiFormData.reviewedByForeman,
                    musterPointIdentified = if (psiFormData.musterPointIdentified.isEmpty()) null else psiFormData.musterPointIdentified,
                    toolsAndEquipmentInspected = if (psiFormData.toolsAndEquipmentInspected.isEmpty()) null else psiFormData.toolsAndEquipmentInspected,
                    psiAtTaskLocation = if (psiFormData.psiAtTaskLocation.isEmpty()) null else psiFormData.psiAtTaskLocation,
                    comments = if (psiFormData.comments.isEmpty()) null else psiFormData.comments,
                    tasks = psiFormData.tasks.map { PsiTaskRequestBody.fromPsiTaskData(it) },
                    signatures = psiFormData.employees.map {
                        PsiSignatureRequestBody.fromPsiSignatureData(it)
                    }
                )
            }
        }
    }

    @JsonClass(generateAdapter = true)
    data class PsiTaskRequestBody(

        @field:Json(name = "task")
        val task: String = "",

        @field:Json(name = "hazard")
        val hazard: String = "",

        @field:Json(name = "control")
        val control: String = "",

        @field:Json(name = "risk")
        val risk: String
    ) {
        companion object {
            fun fromPsiTaskData(taskData: PsiTaskData): PsiTaskRequestBody {
                return PsiTaskRequestBody(
                    task = taskData.task,
                    hazard = taskData.hazard,
                    control = taskData.control,
                    risk = if (taskData.risk == 0) "" else taskData.risk.toString()
                )
            }
        }
    }

    @JsonClass(generateAdapter = true)
    data class PsiSignatureRequestBody(

        @field:Json(name = "externalEmployeeID")
        val externalEmployeeId: String,

        @field:Json(name = "employeeID")
        val employeeId: Int,

        @field:Json(name = "verifyEmployeeID")
        val verifyEmployeeId: Int,

        @field:Json(name = "verifyDate")
        val pinVerifiedDate: Date?
    ) {
        companion object {
            fun fromPsiSignatureData(psiEmployeeData: PsiEmployeeData): PsiSignatureRequestBody {
                return PsiSignatureRequestBody(
                    externalEmployeeId = psiEmployeeData.psiEmployeeId.toString(),
                    employeeId = psiEmployeeData.employeeId,
                    verifyEmployeeId = psiEmployeeData.pinVerifiedByEmployeeId,
                    pinVerifiedDate = if (psiEmployeeData.pinDate.time == 0L) null else psiEmployeeData.pinDate
                )
            }
        }
    }

    @JsonClass(generateAdapter = true)
    data class SaveDailyDemoRequestBody(
        @field:Json(name = "externalID")
        val uuid: String,

        @field:Json(name = "formDate")
        val formDate: Date,

        @field:Json(name = "closeDate")
        val closeDate: Date,

        @field:Json(name = "projectID")
        val projectId: Int,

        @field:Json(name = "supervisorID")
        val supervisorId: Int,

        @field:Json(name = "supervisorVerifyEmployeeID")
        val supervisorVerifyEmployeeId: Int,

        @field:Json(name = "supervisorVerifyDate")
        val supervisorVerifyDate: Date,

        @field:Json(name = "safetyTopic")
        val safetyTopic: String,

        @field:Json(name = "productionNarrative")
        val productionNarrative: String,

        @field:Json(name = "safetyMeetingConductedByEmployeeID")
        val safetyMeetingConductedByEmployeeId: Int,

        @field:Json(name = "weatherAM")
        val weatherAM: String,

        @field:Json(name = "weatherPM")
        val weatherPM: String,

        @field:Json(name = "reportableIncident")
        val reportableIncident: Boolean,

        @field:Json(name = "incident")
        val incident: String,

        @field:Json(name = "incidentDescription")
        val incidentDescription: String,

        @field:Json(name = "stopWorkPerformed")
        val stopWorkPerformed: Boolean,

        @field:Json(name = "stopWorkReason")
        val stopWorkReason: String,

        @field:Json(name = "stopWorkBeyondControlDescription")
        val stopWorkBeyondControlDescription: String,

        @field:Json(name = "stopWorkOutOfScopeDescription")
        val stopWorkOutOfScopeDescription: String,

        @field:Json(name = "stopWorkBackChargeDescription")
        val stopWorkBackChargeDescription: String,

        @field:Json(name = "stopWorkVisitorsDescription")
        val stopWorkVisitorsDescription: String,

        @field:Json(name = "stopWorkIssuesDescription")
        val stopWorkIssuesDescription: String,

        @field:Json(name = "comments")
        val comments: String,

        @field:Json(name = "employees")
        val employees: List<DailyDemoEmployeesRequest>,

        @field:Json(name = "subcontractors")
        val subcontractors: List<DailyDemoSubcontractorsRequest>,

        @field:Json(name = "equipment")
        val equipment: List<DailyDemoEquipmentRequest>,

        @field:Json(name = "materials")
        val materials: List<DailyDemoMaterialRequest>,

        @field:Json(name = "trucking")
        val trucking: List<DailyDemoTruckingRequest>
    ) {
        companion object {
            fun fromData(data: DailyDemoLogFormData, closeDate: Date): SaveDailyDemoRequestBody {
                val uniqueId =
                    "${data.companyId}_${data.projectId}_${data.internalFormId}_${data.formDataId}"

                return SaveDailyDemoRequestBody(
                    uuid = uniqueId,
                    formDate = data.insertDate,
                    closeDate = closeDate,
                    projectId = data.projectId,
                    supervisorId = data.supervisorId,
                    supervisorVerifyEmployeeId = data.supervisorVerifiedByEmployeeId,
                    supervisorVerifyDate = data.supervisorVerifiedDate,
                    safetyTopic = data.safetyTopic,
                    productionNarrative = data.productionNarrative,
                    safetyMeetingConductedByEmployeeId = data.safetyMeetingConductedByEmployeeID,
                    weatherAM = data.weatherAM,
                    weatherPM = data.weatherPM,
                    reportableIncident = data.reportableIncident,
                    incident = data.incident,
                    incidentDescription = data.incidentDescription,
                    stopWorkPerformed = data.stopWorkPerformed,
                    stopWorkReason = data.stopWorkReason,
                    stopWorkBeyondControlDescription = data.stopWorkBeyondControlDescription,
                    stopWorkOutOfScopeDescription = data.stopWorkOutOfScopeDescription,
                    stopWorkBackChargeDescription = data.stopWorkBackChargeDescription,
                    stopWorkVisitorsDescription = data.stopWorkVisitorsDescription,
                    stopWorkIssuesDescription = data.stopWorkIssuesDescription,
                    comments = data.comments,
                    employees = data.dailyDemoLogEmployees.map {
                        DailyDemoEmployeesRequest.fromData(
                            it
                        )
                    },
                    subcontractors = data.dailyDemoLogSubContractors.map {
                        DailyDemoSubcontractorsRequest.fromData(
                            it
                        )
                    },
                    equipment = data.dailyDemoLogEquipment.map {
                        DailyDemoEquipmentRequest.fromData(
                            it
                        )
                    },
                    materials = data.dailyDemoLogMaterials.map {
                        DailyDemoMaterialRequest.fromData(
                            it
                        )
                    },
                    trucking = data.dailyDemoLogTrucking.map { DailyDemoTruckingRequest.fromData(it) }
                )
            }
        }
    }

    @JsonClass(generateAdapter = true)
    data class DailyDemoEmployeesRequest(
        @field:Json(name = "employeeID")
        val employeeId: Int,

        @field:Json(name = "descriptionOfWork")
        val descriptionOfWork: String,

        @field:Json(name = "hours")
        val hours: String
    ) {
        companion object {
            fun fromData(data: DailyDemoLogEmployee): DailyDemoEmployeesRequest {
                return DailyDemoEmployeesRequest(
                    employeeId = data.employeeId,
                    descriptionOfWork = data.descriptionOfWork,
                    hours = data.hours
                )
            }
        }
    }

    @JsonClass(generateAdapter = true)
    data class DailyDemoSubcontractorsRequest(
        @field:Json(name = "subcontractorID")
        val subcontractorId: Int,

        @field:Json(name = "subcontractorCustom")
        val subcontractorCustom: String,

        @field:Json(name = "phase")
        val phase: String,

        @field:Json(name = "hours")
        val hours: String
    ) {
        companion object {
            fun fromData(data: DailyDemoLogSubContractor): DailyDemoSubcontractorsRequest {
                return DailyDemoSubcontractorsRequest(
                    subcontractorId = data.subcontractorID,
                    subcontractorCustom = data.subcontractorCustom,
                    phase = data.phase,
                    hours = data.hours
                )
            }
        }
    }

    @JsonClass(generateAdapter = true)
    data class DailyDemoEquipmentRequest(
        @field:Json(name = "equipmentID")
        val equipmentId: Int,

        @field:Json(name = "equipmentCustom")
        val equipmentCustom: String,

        @field:Json(name = "equipmentNo")
        val equipmentNo: String,

        @field:Json(name = "descriptionOfWork")
        val descriptionOfWork: String,

        @field:Json(name = "hours")
        val hours: String
    ) {
        companion object {
            fun fromData(data: DailyDemoLogEquipment): DailyDemoEquipmentRequest {
                return DailyDemoEquipmentRequest(
                    equipmentId = data.equipmentID,
                    equipmentCustom = data.equipmentCustom,
                    equipmentNo = data.equipmentNo,
                    descriptionOfWork = data.descriptionOfWork,
                    hours = data.hours
                )
            }
        }
    }

    @JsonClass(generateAdapter = true)
    data class DailyDemoMaterialRequest(
        @field:Json(name = "materialID")
        val materialId: Int,

        @field:Json(name = "materialCustom")
        val materialCustom: String,

        @field:Json(name = "quantity")
        val quantity: String,

        @field:Json(name = "amount")
        val amount: String,

        @field:Json(name = "total")
        val total: String
    ) {
        companion object {
            fun fromData(data: DailyDemoLogMaterials): DailyDemoMaterialRequest {
                return DailyDemoMaterialRequest(
                    materialId = data.materialID,
                    materialCustom = data.materialCustom,
                    quantity = data.quantity,
                    amount = data.amount,
                    total = data.total
                )
            }
        }
    }

    @JsonClass(generateAdapter = true)
    data class DailyDemoTruckingRequest(
        @field:Json(name = "truckingID")
        val truckingId: Int,

        @field:Json(name = "truckingCustom")
        val truckingCustom: String,

        @field:Json(name = "material")
        val material: String,

        @field:Json(name = "loads")
        val loads: String,

        @field:Json(name = "truckno")
        val truckNumber: String,

        @field:Json(name = "hours")
        val hours: String,

        @field:Json(name = "hourlyRate")
        val hourlyRate: String,

        @field:Json(name = "total")
        val total: String
    ) {
        companion object {
            fun fromData(data: DailyDemoLogTrucking): DailyDemoTruckingRequest {
                return DailyDemoTruckingRequest(
                    truckingId = data.truckingID,
                    truckingCustom = data.truckingCustom,
                    material = data.material,
                    loads = data.loads,
                    truckNumber = data.truckno,
                    hours = data.hours,
                    hourlyRate = data.hourlyRate,
                    total = data.total
                )
            }
        }
    }

    @JsonClass(generateAdapter = true)
    data class DailyDemoLogResponse(
        @field:Json(name = "project2formID")
        val project2formId: Int
    )

    @JsonClass(generateAdapter = true)
    data class DailyFieldReportResponse(

        @field:Json(name = "project2formID")
        val project2formId: Int
    )

    @JsonClass(generateAdapter = true)
    data class PsiResponse(

        @field:Json(name = "project2formID")
        val project2formId: Int,

        @field:Json(name = "employeesMapping")
        val employeesMapping: List<EmployeeMapping>
    )

    @JsonClass(generateAdapter = true)
    data class TimesheetResponse(

        @field:Json(name = "project2formID")
        val project2formId: Int,

        @field:Json(name = "employeesMapping")
        val employeesMapping: List<EmployeeMapping>
    )

    @JsonClass(generateAdapter = true)
    data class EmployeeMapping(

        @field:Json(name = "externalEmployeeID")
        val externalEmployeeId: String,

        @field:Json(name = "fieldfloID")
        val fieldFloId: Int
    )

    @JsonClass(generateAdapter = true)
    data class SaveTimeSheetRequestBody(

        @field:Json(name = "externalID")
        val uuid: String,

        @field:Json(name = "formDate")
        val formDate: Date? = null,

        @field:Json(name = "closeDate")
        val closeDate: Date,

        @field:Json(name = "projectID")
        val projectId: Int,

        @field:Json(name = "formData")
        val formData: SaveTimeSheetFormData
    ) {
        companion object {
            fun fromTimesheetFormData(
                timesheetData: TimeSheetFormData,
                closeDate: Date
            ): SaveTimeSheetRequestBody {
                val uniqueId =
                    "${timesheetData.companyId}_${timesheetData.projectId}_${timesheetData.internalFormId}_${timesheetData.formDataId}"
                return SaveTimeSheetRequestBody(
                    uuid = uniqueId,
                    formDate = if (timesheetData.formDate.time == 0L) null else timesheetData.formDate,
                    closeDate = closeDate,
                    projectId = timesheetData.projectId,
                    formData = SaveTimeSheetFormData.fromTimesheetFormData(timesheetData)
                )
            }
        }
    }

    @JsonClass(generateAdapter = true)
    data class SaveTimeSheetFormData(

        @field:Json(name = "global")
        val global: TimeSheetGlobalData,

        @field:Json(name = "employees")
        val employees: List<TimeSheetEmployeeData>
    ) {
        companion object {
            fun fromTimesheetFormData(timesheetData: TimeSheetFormData): SaveTimeSheetFormData {
                return SaveTimeSheetFormData(
                    global = TimeSheetGlobalData.fromTimesheetFormData(timesheetData),
                    employees = timesheetData.employees.map {
                        TimeSheetEmployeeData.fromTimesheetEmployee(
                            it
                        )
                    }
                )
            }
        }
    }

    @JsonClass(generateAdapter = true)
    data class TimeSheetGlobalData(

        @field:Json(name = "supervisorID")
        val supervisorId: Int? = null,

        @field:Json(name = "notes")
        val notes: String? = null
    ) {
        companion object {
            fun fromTimesheetFormData(timesheetData: TimeSheetFormData): TimeSheetGlobalData {
                return TimeSheetGlobalData(
                    supervisorId = if (timesheetData.supervisorId == 0) null else timesheetData.supervisorId,
                    notes = if (timesheetData.notes.isEmpty()) null else timesheetData.notes
                )
            }
        }
    }

    @JsonClass(generateAdapter = true)
    data class TimeSheetEmployeeData(

        @field:Json(name = "externalEmployeeID")
        var timeSheetEmployeeId: String,

        @field:Json(name = "employeeID")
        val employeeId: Int,

        @field:Json(name = "clockIn")
        val clockIn: Date,

        @field:Json(name = "clockOut")
        val clockOut: Date,

        @field:Json(name = "totalWorkTime")
        val totalWorkTime: Int,

        @field:Json(name = "timeVerifiedBy")
        val timeVerifiedById: Int,

        @field:Json(name = "timeVerifiedByDate")
        val timeVerifiedByDate: Date,

        @field:Json(name = "injured")
        val injured: Boolean,

        @field:Json(name = "injuredVerifiedBy")
        val injuredVerifiedById: Int,

        @field:Json(name = "injuredVerifiedByDate")
        val injuredVerifiedByDate: Date,

        @field:Json(name = "breakTime")
        val breakTime: Int,

        @field:Json(name = "federalBreak")
        val federalBreak: Boolean,

        @field:Json(name = "federalBreakVerifyDate")
        val federalBreakVerifyDate: Date,

        @field:Json(name = "federalBreakVerifyEmployeeID")
        val federalBreakVerifyEmployeeId: Int,

        @field:Json(name = "positions")
        val positions: List<EmployeePositionData> = listOf(),

        @field:Json(name = "assets")
        val assets: List<EmployeeAssetData> = listOf(),

        @field:Json(name = "perDiem")
        val perDiem: EmployeePerDiemData?
    ) {
        companion object {
            fun fromTimesheetEmployee(timesheetEmployee: TimeSheetEmployee): TimeSheetEmployeeData {
                return TimeSheetEmployeeData(
                    timeSheetEmployeeId = timesheetEmployee.timeSheetEmployeeId.toString(),
                    employeeId = timesheetEmployee.employeeId,
                    clockIn = timesheetEmployee.clockIn,
                    clockOut = timesheetEmployee.clockOut,
                    totalWorkTime = timesheetEmployee.totalWorkTime,
                    timeVerifiedById = timesheetEmployee.timeVerifiedById,
                    timeVerifiedByDate = timesheetEmployee.timeVerifiedByDate,
                    injured = timesheetEmployee.employeeStatus == TimesheetEmployeeStatus.VERIFIED_WITH_INJURY,
                    injuredVerifiedById = timesheetEmployee.injuredVerifiedById,
                    injuredVerifiedByDate = timesheetEmployee.injuredVerifiedByDate,
                    breakTime = timesheetEmployee.breakTime,
                    federalBreak = timesheetEmployee.federalBreak,
                    federalBreakVerifyDate = timesheetEmployee.federalBreakVerifyDate,
                    federalBreakVerifyEmployeeId = timesheetEmployee.federalBreakVerifyEmployeeId,
                    positions = timesheetEmployee.positions.map {
                        EmployeePositionData.fromTimesheetEmployeePosition(it)
                    },
                    assets = timesheetEmployee.assets.map {
                        EmployeeAssetData.fromTimeSheetEmployeeAsset(it)
                    },
                    perDiem = EmployeePerDiemData.fromTimeSheetEmployeePerDiem(timesheetEmployee.perDiem)
                )
            }
        }
    }

    @JsonClass(generateAdapter = true)
    data class EmployeePositionData(
        @field:Json(name = "positionID")
        val positionId: Int,

        @field:Json(name = "phaseID")
        val phaseId: Int,

        @field:Json(name = "defaultPosition")
        val defaultPosition: Boolean,

        @field:Json(name = "totalWorkTime")
        val totalWorkTime: Int
    ) {
        companion object {
            fun fromTimesheetEmployeePosition(position: TimesheetEmployeePosition): EmployeePositionData {
                return EmployeePositionData(
                    positionId = position.positionId,
                    phaseId = position.phaseId,
                    defaultPosition = position.defaultPosition,
                    totalWorkTime = position.totalWorkTime
                )
            }
        }
    }

    @JsonClass(generateAdapter = true)
    data class EmployeeAssetData(

        @field:Json(name = "itemID")
        val itemId: Int,

        @field:Json(name = "totalWorkTime")
        val totalWorkTime: Int,

        @field:Json(name = "notes")
        val notes: String
    ) {
        companion object {
            fun fromTimeSheetEmployeeAsset(asset: TimeSheetEmployeeAsset): EmployeeAssetData {
                return EmployeeAssetData(
                    itemId = asset.itemId,
                    totalWorkTime = asset.totalWorkTime,
                    notes = asset.notes
                )
            }
        }
    }

    @JsonClass(generateAdapter = true)
    data class EmployeePerDiemData(

        @field:Json(name = "perDiemType")
        val perDiemType: String,

        @field:Json(name = "dailyAmount")
        val dailyAmount: Double,

        @field:Json(name = "hourlyAmount")
        val hourlyAmount: Double,

        @field:Json(name = "cashGivenOnSite")
        val cashGivenOnSite: Double,

        @field:Json(name = "notes")
        val notes: String
    ) {
        companion object {
            fun fromTimeSheetEmployeePerDiem(perDiem: TimeSheetEmployeePerDiem?): EmployeePerDiemData? {
                if (perDiem == null) return null
                return EmployeePerDiemData(
                    perDiemType = perDiem.perDiemType,
                    dailyAmount = perDiem.dailyAmount,
                    hourlyAmount = perDiem.hourlyAmount,
                    cashGivenOnSite = perDiem.cashGivenOnSite,
                    notes = perDiem.notes
                )
            }
        }
    }

    @JsonClass(generateAdapter = true)
    data class LogSyncErrorBody(
        @field:Json(name = "log")
        val log: String,

        @field:Json(name = "platform")
        val platform: String = "Android",

        @field:Json(name = "formID")
        val formTypeId: String,

        @field:Json(name = "formName")
        val formName: String
    )
}