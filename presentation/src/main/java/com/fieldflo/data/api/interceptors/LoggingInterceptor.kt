package com.fieldflo.data.api.interceptors

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import okio.Buffer
import timber.log.Timber
import java.io.EOFException
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.util.*
import javax.inject.Inject

class LoggingInterceptor @Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val curl = request.toCurl()
        Timber.i("\n$curl")

        return chain.proceed(request)
    }
}

fun Request.toCurl(): String {
    val sb = StringBuilder("curl -D -")
    sb.append(" -X ")
        .append(method.toUpperCase(Locale.ROOT))
        .append(" '")
        .append(escape(url.toString(), '\''))
        .append("'")

    for ((key, value) in headers) {
        sb.append("\\\n -H '")
            .append(escape(key, '\''))
            .append(": ")
            .append(escape(value, '\''))
            .append("'")
    }

    body?.let {
        val buffer = Buffer()
        it.writeTo(buffer)

        val contentType = it.contentType()
        val charset: Charset =
            contentType?.charset(StandardCharsets.UTF_8) ?: StandardCharsets.UTF_8
        if (buffer.isProbablyUtf8()) {
            sb.append("\\\n -d '")
                .append(escape(buffer.readString(charset), '\''))
                .append("'")
        } else {
            sb.append("\\\n -d '")
                .append("Body missing")
        }
    }

    return sb.toString()
}

private fun Buffer.isProbablyUtf8(): Boolean {
    try {
        val prefix = Buffer()
        val byteCount = size.coerceAtMost(64)
        copyTo(prefix, 0, byteCount)
        for (i in 0 until 16) {
            if (prefix.exhausted()) {
                break
            }
            val codePoint = prefix.readUtf8CodePoint()
            if (Character.isISOControl(codePoint) && !Character.isWhitespace(codePoint)) {
                return false
            }
        }
        return true
    } catch (_: EOFException) {
        return false // Truncated UTF-8 sequence.
    }
}

@Suppress("SameParameterValue")
private fun escape(str: String, delimiter: Char): String {
    return if (str.indexOf(delimiter) > -1) {
        str.replace(delimiter.toString(), "\\" + delimiter)
    } else {
        str
    }
}