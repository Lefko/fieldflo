package com.fieldflo.data.api.endpoints

import com.fieldflo.data.api.entities.PrintCertificateRestObject
import com.fieldflo.data.api.interceptors.ApiVersion
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import retrofit2.http.Body
import retrofit2.http.POST

interface CertsApi {
    @ApiVersion(2)
    @POST("printCerts")
    suspend fun printCertificate(@Body body: PrintCertificateRequestBody): PrintCertificateRestObject

    @JsonClass(generateAdapter = true)
    data class PrintCertificateRequestBody(
        @field:Json(name = "certs")
        val certs: List<Int>
    )
}