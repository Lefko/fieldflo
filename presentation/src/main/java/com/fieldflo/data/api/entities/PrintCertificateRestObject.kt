package com.fieldflo.data.api.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PrintCertificateRestObject(
    @field:Json(name = "fileName")
    val fileName: String? = "",

    @field:Json(name = "fullURL")
    val fullUrl: String? = ""
)