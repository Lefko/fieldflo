package com.fieldflo.data.api.entities

import com.fieldflo.data.persistence.db.entities.AvailableForm
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AvailableFormRestObject(

    @field:Json(name = "formID")
    val formTypeId: Int? = 0,

    @field:Json(name = "formName")
    val formName: String? = "",

    @field:Json(name = "sort")
    val sort: Int? = 0,

    @field:Json(name = "mandatory")
    val mandatory: Boolean? = false,

    @field:Json(name = "settings")
    val settings: FormSettingsRestObject? = null
) {
    fun toAvailableForm(companyId: Int, projectId: Int) = AvailableForm(
        companyId = companyId,
        projectId = projectId,
        formTypeId = this.formTypeId!!,
        mandatory = this.mandatory!!,
        formName = this.formName!!,
        sort = this.sort!!,
        tabs = this.settings?.tabs.orEmpty()
    )
}

@JsonClass(generateAdapter = true)
data class FormSettingsRestObject(
    @field:Json(name = "tabs")
    val tabs: List<String>? = emptyList()
)
