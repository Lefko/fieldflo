package com.fieldflo.data.api.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PositionRestObject(

    @field:Json(name = "positionID")
    val positionId: Int? = 0,

    @field:Json(name = "positionName")
    val positionName: String? = "",

    @field:Json(name = "positionCode")
    val positionCode: String? = "",

    @field:Json(name = "projectID")
    val projectId: Int? = 0,

    @field:Json(name = "positionActive")
    val positionActive: Boolean? = false,

    @field:Json(name = "positionDeleted")
    val positionDeleted: Boolean? = false
)
