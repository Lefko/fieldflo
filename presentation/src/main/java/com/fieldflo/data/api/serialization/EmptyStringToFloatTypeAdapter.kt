package com.fieldflo.data.api.serialization

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonReader
import javax.inject.Inject

class EmptyStringToFloatTypeAdapter @Inject constructor() {

    @FromJson
    fun fromJson(jsonReader: JsonReader): Float? {
        if (jsonReader.peek() == JsonReader.Token.NULL) {
            @Suppress("UNUSED_VARIABLE") val nil: Any? = jsonReader.nextNull()
            return 0F
        }

        try {
            val numString = jsonReader.nextString()
            if (numString.isNullOrEmpty()) {
                return 0F
            }
            return numString.toFloat()
        } catch (e: Exception) {
            return 0F
        }
    }
}