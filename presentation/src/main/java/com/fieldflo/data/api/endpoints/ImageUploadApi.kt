package com.fieldflo.data.api.endpoints

import com.fieldflo.data.api.interceptors.ApiVersion
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface ImageUploadApi {
    @ApiVersion(2)
    @Multipart
    @POST("formTimesheetUploadPINOutImage")
    fun uploadTimeSheetPinOutImage(
        @Part("project2formID") project2formId: RequestBody,
        @Part("employeeAutoID[]") employeeAutoIds: RequestBody,
        @Part image: MultipartBody.Part
    ): Call<ResponseBody>

    @ApiVersion(2)
    @Multipart
    @POST("formPSIJHAUploadPINOutImage")
    fun uploadPsiPinOutImage(
        @Part("project2formID") project2formId: RequestBody,
        @Part("employeeIDs[]") employeeId: RequestBody,
        @Part image: MultipartBody.Part
    ): Call<ResponseBody>

    @ApiVersion(2)
    @Multipart
    @POST("formDailyFieldReportUploadImage")
    fun uploadDailyFieldReportImage(
        @Part("project2formID") project2formId: RequestBody,
        @Part images: Array<MultipartBody.Part>
    ): Call<UploadDailyFieldReportImagesResponse>

    @ApiVersion(3)
    @Multipart
    @POST("formDailyDemoLogUploadImages")
    fun uploadDailyDemoLogImage(
        @Part("project2formID") project2formId: RequestBody,
        @Part image: Array<MultipartBody.Part>
    ): Call<ResponseBody>

    @ApiVersion(3)
    @Multipart
    @POST("formDailyDemoLogUploadPINOutImage")
    fun uploadDailyDemoLogSupervisorPinOutImage(
        @Part("project2formID") project2formId: RequestBody,
        @Part image: Array<MultipartBody.Part>
    ): Call<ResponseBody>

    @JsonClass(generateAdapter = true)
    data class UploadDailyFieldReportImagesResponse(
        @field:Json(name = "success")
        val result: Boolean? = false,

        @field:Json(name = "images")
        val images: List<UploadDailyFieldReportImage>? = emptyList()
    )

    @JsonClass(generateAdapter = true)
    data class UploadDailyFieldReportImage(

        @field:Json(name = "success")
        val result: Boolean? = false,

        @field:Json(name = "URL")
        val imageUrl: String? = ""
    )
}
