package com.fieldflo.data.api.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class EmployeeRestObject(

    @field:Json(name = "employeeID")
    val employeeId: Int? = 0,

    @field:Json(name = "positionID")
    val positionId: Int? = 0,

    @field:Json(name = "employeeFirstName")
    val employeeFirstName: String? = "",

    @field:Json(name = "employeeMiddleName")
    val employeeMiddleName: String? = "",

    @field:Json(name = "employeeLastName")
    val employeeLastName: String? = "",

    @field:Json(name = "employeeFullName")
    val employeeFullName: String? = "",

    @field:Json(name = "employeePhoneNumber")
    val employeePhoneNumber: String? = "",

    @field:Json(name = "employeePinNumber")
    val employeePinNumber: String? = "",

    @field:Json(name = "employeeImage")
    val employeeImageFullUrl: String? = "",

    @field:Json(name = "employeeActive")
    val employeeActive: Boolean? = true,

    @field:Json(name = "admin")
    val admin: Boolean? = false,

    @field:Json(name = "employeeDeleted")
    val employeeDeleted: Boolean? = false
)
