package com.fieldflo.data.api.serialization

import com.fieldflo.data.api.entities.ModificationRestObject
import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonReader
import javax.inject.Inject

class ModificationsDataTypeAdapter @Inject constructor() {

    @FromJson
    fun fromJson(reader: JsonReader): List<ModificationRestObject> {
        reader.isLenient = true

        val modifications = mutableListOf<ModificationRestObject>()

        reader.beginObject()
        var node: String

        while (reader.hasNext()) {
            node = reader.nextName()
            reader.beginObject()


            val allIds = mutableListOf<Int>()
            val updatedIds = mutableListOf<Int>()
            val createdIds = mutableListOf<Int>()
            val deletedIds = mutableListOf<Int>()

            while (reader.hasNext()) {
                when (reader.nextName()) {
                    "params" -> allIds.addAll(extractParams(reader))
                    "updated" -> updatedIds.addAll(reader.extractIntList())
                    "created" -> createdIds.addAll(reader.extractIntList())
                    "deleted" -> deletedIds.addAll(reader.extractIntList())
                    else -> reader.skipValue()
                }
            }

            modifications.add(
                ModificationRestObject(
                    node = node,
                    allIds = allIds,
                    updatedIds = updatedIds,
                    createdIds = createdIds,
                    deletedIds = deletedIds
                )
            )

            reader.endObject()
        }

        reader.endObject()

        return modifications
    }

    private fun extractParams(reader: JsonReader): List<Int> {

        val token: JsonReader.Token = reader.peek()
        return if (token == JsonReader.Token.BEGIN_ARRAY) {
            reader.extractIntList()
        } else {
            emptyList()
        }

    }

    private fun JsonReader.extractIntList(): List<Int> {
        val intList = mutableListOf<Int>()

        try {
            beginArray()

            while (hasNext()) {
                val nextInt = nextIntOrNull()
                nextInt?.let {
                    intList.add(it)
                }
            }

            endArray()
        } catch (e: Exception) {

        }

        return intList.toList()
    }

    private fun JsonReader.nextIntOrNull(): Int? {
        return try {
            nextInt()
        } catch (e: Exception) {
            null
        }
    }
}