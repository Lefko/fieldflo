package com.fieldflo.data.api.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class StateRestObject(

    @field:Json(name = "stateID")
    val stateId: Int? = 0,

    @field:Json(name = "stateCode")
    val stateCode: String? = "",

    @field:Json(name = "stateName")
    val stateName: String? = ""
)