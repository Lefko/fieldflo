package com.fieldflo.data.api.entities

import com.fieldflo.data.persistence.db.entities.ProjectPermit
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class ProjectPermitRestObject(

    @field:Json(name = "ID")
    val permitId: Int? = 0,

    @field:Json(name = "type")
    val type: String? = "",

    @field:Json(name = "fee")
    val fee: Double? = 0.0,

    @field:Json(name = "number")
    val number: String? = "",

    @field:Json(name = "expirationDate")
    val expireDate: Date? = Date(0),

    @field:Json(name = "note")
    val note: String? = "",

    @field:Json(name = "file")
    val permitImageUrl: String? = ""
) {
    fun toPermit(companyId: Int, projectId: Int): ProjectPermit {
        val permitFileParts = this.permitImageUrl!!.split("/")
        val fileName = permitFileParts.last()
        return ProjectPermit(
            companyId = companyId,
            projectId = projectId,
            permitId = this.permitId!!,
            permitType = this.type!!,
            fee = this.fee!!,
            number = this.number!!,
            expireDate = this.expireDate!!,
            notes = this.note!!,
            permitImageUrl = this.permitImageUrl,
            documentFile = fileName
        )
    }
}
