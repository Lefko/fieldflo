package com.fieldflo.data.api.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SubContractorRestObject(

    @field:Json(name = "ID")
    val subContractorId: Int? = 0,

    @field:Json(name = "name")
    val name: String? = ""
)