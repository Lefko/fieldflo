package com.fieldflo.data.api.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PhaseRestObject(

    @field:Json(name = "phaseID")
    val phaseId: Int? = 0,

    @field:Json(name = "phaseName")
    val phaseName: String? = "",

    @field:Json(name = "phaseCode")
    val phaseCode: String? = "",

    @field:Json(name = "projectID")
    val projectId: Int? = 0,

    @field:Json(name = "phaseActive")
    val phaseActive: Boolean? = true,

    @field:Json(name = "phaseDeleted")
    val phaseDeleted: Boolean? = false
)
