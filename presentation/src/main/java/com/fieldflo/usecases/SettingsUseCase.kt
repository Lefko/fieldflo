package com.fieldflo.usecases

import com.fieldflo.data.persistence.keyValue.ILocalStorage
import java.util.*
import javax.inject.Inject

class SettingsUseCase @Inject constructor(
    private val localStorage: ILocalStorage
) {
    var usesPhotoPin: Boolean
        get() = localStorage.usesPhotoPin()
        set(value) = localStorage.setUsePhotoPin(value)

    var deletesPhotoPin: Boolean
        get() = localStorage.deletesPhotoPin()
        set(value) = localStorage.setDeletePhotoPin(value)

    val language: String
        get() = Locale.getDefault().language
}
