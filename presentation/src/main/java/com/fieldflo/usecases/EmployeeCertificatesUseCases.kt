package com.fieldflo.usecases

import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.ICompanyCertificatesRepo
import com.fieldflo.data.repos.interfaces.IEmployeesRepo
import com.fieldflo.data.repos.interfaces.IProjectCertificatesRepo
import com.fieldflo.data.repos.interfaces.ITimesheetRepo
import kotlinx.coroutines.flow.Flow
import java.io.File
import java.util.*
import javax.inject.Inject

class EmployeeCertificatesUseCases @Inject constructor(
    private val localStorage: ILocalStorage,
    private val employeesRepo: IEmployeesRepo,
    private val companyCertificatesRepo: ICompanyCertificatesRepo,
    private val timeSheetRepo: ITimesheetRepo,
    private val projectCertificateRepo: IProjectCertificatesRepo
) {
    val companyId: Int get() = localStorage.getCurrentCompanyId()

    suspend fun getCertsForProject(projectId: Int): List<EmployeeCertData> {

        val savedProjectCerts =
            projectCertificateRepo.getEmployeesCertificates(projectId, companyId).map {
                val employee = employeesRepo.getEmployeeById(it.employeeId, companyId)
                EmployeeCertData(
                    certificateId = it.certificateId,
                    projectId = it.projectId,
                    companyId = it.companyId,
                    employeeId = it.employeeId,
                    employeeFullName = employee?.employeeFullName ?: "",
                    employeeImageUrl = employee?.employeeImageFullUrl ?: "",
                    certificateName = it.certificateName,
                    certificateExpirationDate = it.certificateExpirationDate
                )
            }

        val timeSheetEmployeesOnProject =
            timeSheetRepo.getTimeSheetEmployeesByProjectId(projectId, companyId)
        val timeSheetEmployeesEmployeeIds = timeSheetEmployeesOnProject
            .fold(mutableSetOf<Int>()) { employeeIds, employee ->
                employeeIds.add(employee.employeeId)
                employeeIds
            }
            .toList()

        val newProjectCerts = companyCertificatesRepo
            .getCompanyCertificatesByEmployeeIds(timeSheetEmployeesEmployeeIds, companyId)
            .map {
                val employee = employeesRepo.getEmployeeById(it.employeeId, companyId)
                EmployeeCertData(
                    certificateId = it.certificateId,
                    projectId = projectId,
                    companyId = companyId,
                    employeeId = it.employeeId,
                    employeeFullName = employee?.employeeFullName ?: "",
                    employeeImageUrl = employee?.employeeImageFullUrl ?: "",
                    certificateName = it.certificateName,
                    certificateExpirationDate = it.certificateExpirationDate
                )
            }

        val allProjectCerts = savedProjectCerts.union(newProjectCerts)


        return allProjectCerts.toList()
    }

    suspend fun printCertificates(certificateIds: List<Int>, projectId: Int): Boolean {
        val companyId = localStorage.getCurrentCompanyId()
        return projectCertificateRepo.printProjectCertificate(certificateIds, projectId, companyId)
    }

    fun getProjectCertsDir(projectId: Int): Flow<List<File>> {
        val companyId = localStorage.getCurrentCompanyId()
        return projectCertificateRepo.getProjectCertsDir(projectId, companyId)
    }

    fun changeFileName(fileLocation: String, newName: String): Boolean {
        return projectCertificateRepo.changeFileName(fileLocation, newName)
    }

    fun deleteProjectCertificateFile(fileName: String, projectId: Int): Boolean {
        val companyId = localStorage.getCurrentCompanyId()
        return projectCertificateRepo.deleteProjectCertificateFile(fileName, projectId, companyId)
    }
}

data class EmployeeCertData(
    val certificateId: Int,
    val projectId: Int,
    val companyId: Int,

    val employeeId: Int,
    val employeeFullName: String,
    val employeeImageUrl: String,
    val certificateName: String,
    val certificateExpirationDate: Date
)
