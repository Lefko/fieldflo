package com.fieldflo.usecases

import com.fieldflo.data.persistence.db.entities.Project
import com.fieldflo.data.persistence.db.entities.TimeSheetEmployee
import com.fieldflo.data.persistence.db.entities.TimeSheetEmployeePerDiem
import com.fieldflo.data.persistence.db.entities.TimesheetEmployeeStatus

object TimeSheetPerDiemFactory {

    fun employeeRequiresPerDiem(
        employeeId: Int,
        perDiemInfo: PerDiemInfo,
        allEmployees: List<TimeSheetEmployee>
    ): Boolean {
        return if (!perDiemInfo.autoApply) {
            false
        } else if (!perDiemInfo.isHourly) {
            allEmployees.none {
                it.employeeId == employeeId &&
                        it.employeeStatus.ordinal >= TimesheetEmployeeStatus.CLOCKED_OUT.ordinal &&
                        it.perDiem != null
            }
        } else {
            true
        }
    }

    fun createPerDiem(
        perDiemInfo: PerDiemInfo,
        totalTimeSeconds: Double,
        notes: String = AUTO_APPLY_PER_DIEM_NOTES,
        cashGiven: Double = 0.0
    ): TimeSheetEmployeePerDiem {
        return if (perDiemInfo.isHourly) {
            val totalTimeHours = totalTimeSeconds / SECONDS_IN_HOUR
            createHourlyPerDiem(
                perDiemInfo,
                totalTimeSeconds.toInt(),
                totalTimeHours,
                notes,
                cashGiven
            )
        } else {
            createDailyPerDiem(
                perDiemInfo,
                totalTimeSeconds.toInt(),
                notes,
                cashGiven
            )
        }
    }

    private fun createHourlyPerDiem(
        perDiemInfo: PerDiemInfo,
        totalTimeSeconds: Int,
        totalTimeHours: Double,
        notes: String,
        cashGiven: Double
    ): TimeSheetEmployeePerDiem {
        return TimeSheetEmployeePerDiem(
            perDiemType = "hourly",
            dailyAmount = 0.0,
            hourlyAmount = perDiemInfo.amount,
            cashGivenOnSite = cashGiven,
            totalWorkTime = totalTimeSeconds,
            notes = notes,
            totalEarned = totalTimeHours * perDiemInfo.amount,
            totalOwed = (totalTimeHours * perDiemInfo.amount) - cashGiven
        )
    }

    private fun createDailyPerDiem(
        perDiemInfo: PerDiemInfo,
        totalTimeSeconds: Int,
        notes: String,
        cashGiven: Double
    ): TimeSheetEmployeePerDiem {
        return TimeSheetEmployeePerDiem(
            perDiemType = "daily",
            dailyAmount = perDiemInfo.amount,
            hourlyAmount = 0.0,
            cashGivenOnSite = 0.0,
            totalWorkTime = totalTimeSeconds,
            notes = notes,
            totalEarned = perDiemInfo.amount,
            totalOwed = perDiemInfo.amount - cashGiven
        )
    }
}

data class PerDiemInfo(
    val autoApply: Boolean,
    val isHourly: Boolean,
    val amount: Double
) {
    companion object {
        fun createFromProject(project: Project): PerDiemInfo {
            return PerDiemInfo(
                autoApply = project.perDiemAutoApplyToTimesheet,
                isHourly = project.perDiemType == "hourly",
                amount = if (project.perDiemType == "hourly") {
                    project.perDiemHourlyAmount
                } else {
                    project.perDiemDailyAmount
                }
            )
        }
    }
}

private const val MILLIS_IN_SECOND = 1_000
private const val SECONDS_IN_MINUTE = 60
private const val MINUTES_IN_HOUR = 60
private const val SECONDS_IN_HOUR = SECONDS_IN_MINUTE * MINUTES_IN_HOUR
private const val AUTO_APPLY_PER_DIEM_NOTES = "Auto Applied based on project settings"