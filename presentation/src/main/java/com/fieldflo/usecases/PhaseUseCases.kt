package com.fieldflo.usecases

import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.IPhasesRepo
import javax.inject.Inject

class PhaseUseCases @Inject constructor(
    private val localStorage: ILocalStorage,
    private val phaseRepo: IPhasesRepo
) {
    private val companyId: Int get() = localStorage.getCurrentCompanyId()

    suspend fun getPhasesForProject(projectId: Int) =
        phaseRepo.getPhasesForProject(projectId, companyId).sortedBy { it.phaseName }
}
