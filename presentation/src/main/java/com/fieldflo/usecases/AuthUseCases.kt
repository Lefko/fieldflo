package com.fieldflo.usecases

import android.util.Patterns
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import javax.inject.Inject

class AuthUseCases @Inject constructor(
    private val localStorage: ILocalStorage,
    private val projectFormsRepo: IProjectFormsRepo,
    private val companiesRepo: ICompaniesRepo,
    private val serverUpdatesRepo: IServerUpdatesRepo,
    private val authRepo: IAuthRepo
) {
    suspend fun login(loginParams: LoginParams): LoginResponse {
        if (!localStorage.isV2MigrationComplete()) {
            sanitizeBaseUrl()
        }

        val companyData = companiesRepo.getCompanyBySecurityKeyAndToken(
            loginParams.securityKey,
            loginParams.token
        ) ?: return LoginResponse.GeneralError

        localStorage.setBaseUrl(companyData.url)
        localStorage.setApiToken(companyData.token)
        localStorage.setSecurityKey(companyData.securityKey)
        localStorage.setCurrentCompanyName(companyData.companyName)

        val email = loginParams.email
        val password = loginParams.password
        val fcmToken = localStorage.getFcmToken()
        val deviceId = localStorage.getDeviceId()
        return authRepo.login(companyData, email, password, fcmToken, deviceId)
    }

    suspend fun getLoggedInState(): LoggedInState {
        val userToken = localStorage.getUserToken()
        val securityKey = localStorage.getSecurityKey()
        val apiToken = localStorage.getApiToken()
        return when {
            // first priority is to handle migration
            !localStorage.isV2MigrationComplete() -> {
                localStorage.clear()
                LoggedInState.LoggedOut
            }

            // next check if logged out
            userToken.isEmpty() -> LoggedInState.LoggedOut

            // logged in, just need to check if initial download was completed
            companiesRepo.didInitialCompanyLoad(
                securityKey = securityKey,
                token = apiToken
            ) -> LoggedInState.Projects

            else -> LoggedInState.InitialDownload(securityKey = securityKey, token = apiToken)
        }
    }

    fun canLogout(): Flow<Boolean> {
        val companyId = localStorage.getCurrentCompanyId()
        return serverUpdatesRepo.hasPendingWorkFlow()
            .combine(projectFormsRepo.hasUnclosedFormsFlow(companyId)) { hasWork, hasUnclosed ->
                !hasUnclosed && !hasWork
            }
    }

    fun logout() {
        authRepo.logout()
    }

    fun isEmailValid(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun isPasswordValid(password: String): Boolean {
        return password.isNotEmpty()
    }

    private suspend fun sanitizeBaseUrl() {
        val sanitized = companiesRepo.getAllCompanies().map {
            if (it.url.contains("/apirest_v")) {
                it.copy(url = it.url.substringBeforeLast("/"))
            } else {
                it
            }
        }

        companiesRepo.updateAllCompanies(sanitized)
    }
}

data class LoginParams(
    val securityKey: String,
    val token: String,
    val email: String,
    val password: String
)

sealed class LoggedInState {
    object LoggedOut : LoggedInState()
    data class InitialDownload(val securityKey: String, val token: String) : LoggedInState()
    object Projects : LoggedInState()
}