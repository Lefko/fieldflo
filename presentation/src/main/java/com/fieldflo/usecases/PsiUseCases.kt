package com.fieldflo.usecases

import com.fieldflo.data.persistence.db.entities.Employee
import com.fieldflo.data.persistence.db.entities.PsiFormData
import com.fieldflo.data.persistence.db.entities.PsiTaskData
import com.fieldflo.data.persistence.fileSystem.FileManager
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import java.util.*
import javax.inject.Inject

class PsiUseCases @Inject constructor(
    timeSheetRepo: ITimesheetRepo,
    private val employeeRepo: IEmployeesRepo,
    private val projectsRepo: IProjectsRepo,
    private val psiRepo: IPsiRepo,
    private val projectFormsRepo: IProjectFormsRepo,
    private val fileManager: FileManager,
    private val localStorage: ILocalStorage
) {

    private val companyId: Int
        get() = localStorage.getCurrentCompanyId()

    private val defaultPsiEmployeesHelper = DefaultPsiEmployeesHelper(
        employeeRepo,
        projectFormsRepo,
        timeSheetRepo,
        fileManager,
        companyId
    )

    suspend fun getPsiFormData(internalFormId: Int, projectId: Int): PsiUiData {
        val project = projectsRepo.getProjectById(projectId, companyId)
        val savedData = psiRepo.getPsiFormData(internalFormId, companyId)
        return if (savedData == null) {
            val newData = PsiFormData(
                internalFormId = internalFormId,
                projectId = projectId,
                companyId = companyId,
                formDate = Date(),
                reviewedWeather = null,
                reviewedRoad = null,
                reviewedHeatIndex = null
            )
            psiRepo.savePsiFormData(newData)
            PsiUiData(
                projectName = project.projectName,
                projectNumber = project.projectNumber,
                psiFormData = newData,
                supervisor = null,
                auditor = null,
                tasks = emptyList(),
                employees = defaultPsiEmployeesHelper.getDefaultPsiEmployees(
                    internalFormId,
                    projectId
                )
            )
        } else {
            val tasks = psiRepo.getPsiTasks(internalFormId, companyId)
            val employees = psiRepo.getPsiEmployees(internalFormId, companyId)
            PsiUiData(
                projectName = project.projectName,
                projectNumber = project.projectNumber,
                psiFormData = savedData,
                supervisor = employeeRepo.getEmployeeById(savedData.supervisorId, companyId),
                auditor = employeeRepo.getEmployeeById(savedData.auditorId, companyId),
                tasks = tasks,
                employees = employees.map {
                    val employee = employeeRepo.getEmployeeById(it.employeeId, companyId)
                    PsiEmployeeUiData(
                        psiEmployeeId = it.psiEmployeeId,
                        employeeId = it.employeeId,
                        employeeFullName = employee?.employeeFullName ?: "",
                        employeePin = employee?.employeePinNumber ?: "",
                        pinUserName = it.pinVerifiedByName,
                        pinEmployeeId = it.pinVerifiedByEmployeeId,
                        pinDate = it.pinDate,
                        pinImageFileLocation = fileManager.getPsiPhotoPinFileLocation(
                            companyId,
                            projectId,
                            it.employeeId,
                            savedData.insertDate,
                            internalFormId
                        )
                    )
                }
            )
        }
    }

    suspend fun getPsiPhotoPinFileLocation(
        projectId: Int,
        employeeId: Int,
        internalFormId: Int
    ): File {
        val savedData = psiRepo.getPsiFormData(internalFormId, companyId)
        return fileManager.getPsiPhotoPinFileLocation(
            companyId, projectId, employeeId, savedData?.insertDate ?: Date(), internalFormId
        )
    }

    suspend fun savePsiFormData(psiFormData: PsiFormData) {
        val saveData = withContext(Dispatchers.Default) {
            psiFormData.copy(
                companyId = companyId,
                employees = psiFormData.employees.map { it.copy(companyId = companyId) },
                tasks = psiFormData.tasks.map { it.copy(companyId = companyId) }
            )
        }
        psiRepo.savePsiFormData(saveData)
        projectFormsRepo.triggerFormUpdate(psiFormData.internalFormId, companyId)
    }
}

data class PsiUiData(
    val projectName: String,
    val projectNumber: String,
    val psiFormData: PsiFormData,
    val supervisor: Employee?,
    val auditor: Employee?,
    val tasks: List<PsiTaskData>,
    val employees: List<PsiEmployeeUiData>
)

data class PsiEmployeeUiData(
    val psiEmployeeId: Int,
    val employeeId: Int,
    val employeeFullName: String,
    val employeePin: String,
    val pinUserName: String,
    val pinEmployeeId: Int,
    val pinDate: Date,
    val pinImageFileLocation: File
)
