package com.fieldflo.usecases

import android.os.Parcelable
import com.fieldflo.common.FileFlow
import com.fieldflo.data.persistence.db.entities.*
import com.fieldflo.data.persistence.fileSystem.FileManager
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.*
import com.fieldflo.screens.formDailyDemoLog.DailyDemoLogSaveData
import com.fieldflo.screens.formDailyDemoLog.DailyDemoLogScreen
import com.fieldflo.screens.formDailyDemoLog.equipment.addEquipment.EquipmentUiModel
import com.fieldflo.screens.formDailyDemoLog.materials.addMaterial.MaterialUiModel
import com.fieldflo.screens.formDailyDemoLog.subContractors.addSubcontractor.SubcontractorUiModel
import com.fieldflo.screens.formDailyDemoLog.trucking.addTruck.HaulerUiModel
import kotlinx.android.parcel.Parcelize
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import java.io.File
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DailyDemoLogUseCases @Inject constructor(
    private val localStorage: ILocalStorage,
    private val projectsRepo: IProjectsRepo,
    private val projectFormsRepo: IProjectFormsRepo,
    private val availableFormsRepo: IAvailableFormsRepo,
    private val dailyDemoLogRepo: IDailyDemoLogRepo,
    private val employeesRepo: IEmployeesRepo,
    private val fileManager: FileManager,
    private val subcontractorsRepo: ISubContractorsRepo,
    private val inventoryItemsRepo: IInventoryItemsRepo,
    private val haulersRepo: IHaulersRepo,
    timeSheetRepo: ITimesheetRepo
) {

    private val companyId: Int
        get() = localStorage.getCurrentCompanyId()

    private val mutex = Mutex()

    private val defaultDemoLogEmployeesHelper = DefaultDemoLogEmployeesHelper(
        projectFormsRepo,
        timeSheetRepo,
        companyId
    )

    suspend fun getDailyDemoLogActivityUiData(
        internalFormId: Int,
        projectId: Int
    ): DailyDemoLogActivityUiData {
        val project = projectsRepo.getProjectById(projectId, companyId)
        val form = projectFormsRepo.getProjectForm(internalFormId, companyId)
        return DailyDemoLogActivityUiData(
            projectName = project.projectName,
            projectNumber = project.projectNumber,
            formDate = form?.formDate ?: Date()
        )
    }

    suspend fun getDailyDemoLogTabs(projectId: Int): List<DailyDemoLogScreen> {
        val dailyDemoLog = availableFormsRepo.getAvailableForms(projectId, companyId)
            .find { it.formTypeId == SupportedForm.DAILY_DEMO_LOG_ID.formTypeId }

        val mandatoryTabs = listOf(
            DailyDemoLogScreen.GeneralInformation,
            DailyDemoLogScreen.ProductionNarrative
        )
        val tabs = dailyDemoLog?.tabs.orEmpty()
            .map {
                when (it) {
                    "employees" -> DailyDemoLogScreen.Employees
                    "subcontractors" -> DailyDemoLogScreen.SubContractors
                    "equipment" -> DailyDemoLogScreen.Equipment
                    "materials" -> DailyDemoLogScreen.Materials
                    "trucking" -> DailyDemoLogScreen.Trucking
                    "miscitems" -> DailyDemoLogScreen.MiscItems
                    "misccomments" -> DailyDemoLogScreen.MiscComments
                    else -> throw IllegalArgumentException("Dont recognize tab $it")
                }
            }

        return mandatoryTabs + tabs
    }

    fun getDailyDemoLogSupervisorPinOutLocation(internalFormId: Int, projectId: Int): File {
        return fileManager.getDailyDemoSupervisorPinFile(
            companyId = companyId,
            internalFormId = internalFormId,
            projectId = projectId,
            formDate = Date()
        )
    }

    suspend fun getGeneralInfoData(
        internalFormId: Int,
        projectId: Int
    ): DailyDemoLogGeneralInfoUiData {
        val project = projectsRepo.getProjectById(projectId, companyId)
        val dailyDemoLogData = getDailyDemoLogData(internalFormId, projectId)
        val supervisor = employeesRepo.getEmployeeById(dailyDemoLogData.supervisorId, companyId)

        return DailyDemoLogGeneralInfoUiData(
            projectName = project.projectName,
            projectNumber = project.projectNumber,
            supervisor = supervisor,
            supervisorVerified = dailyDemoLogData.supervisorVerified,
            supervisorVerifiedByEmployeeId = dailyDemoLogData.supervisorVerifiedByEmployeeId,
            supervisorVerifiedDate = dailyDemoLogData.supervisorVerifiedDate,
            pinOutLocation = File(dailyDemoLogData.pinOutLocation),
            formDate = dailyDemoLogData.insertDate,
            safetyTopic = dailyDemoLogData.safetyTopic
        )
    }

    suspend fun getProductionNarrativeUiData(
        internalFormId: Int,
        projectId: Int
    ): DailyDemoLogProductionNarrativeUiData {
        val dailyDemoLogData = getDailyDemoLogData(internalFormId, projectId)
        val safetyMeetingConductedBy = employeesRepo.getEmployeeById(
            dailyDemoLogData.safetyMeetingConductedByEmployeeID,
            companyId
        )
        return DailyDemoLogProductionNarrativeUiData(
            productionNarrative = dailyDemoLogData.productionNarrative,
            safetyMeetingConductedById = dailyDemoLogData.safetyMeetingConductedByEmployeeID,
            safetyMeetingConductedBy = safetyMeetingConductedBy?.employeeFullName.orEmpty(),
            weatherAM = dailyDemoLogData.weatherAM,
            weatherPM = dailyDemoLogData.weatherPM
        )
    }

    suspend fun getDailyDemoLogEmployees(
        internalFormId: Int,
        projectId: Int
    ): List<DailyDemoLogEmployeeUiData> {
        val dailyDemoLogData = getDailyDemoLogData(internalFormId, projectId)
        return dailyDemoLogData.dailyDemoLogEmployees.map {
            val employee = employeesRepo.getEmployeeById(it.employeeId, companyId)
            DailyDemoLogEmployeeUiData(
                dailyDemoLogEmployeeId = it.dailyDemoLogEmployeeId,
                employeeId = it.employeeId,
                employeeFullName = employee?.employeeFullName.orEmpty(),
                descriptionOfWork = it.descriptionOfWork,
                hours = it.hours
            )
        }
    }

    suspend fun getDailyDemoLogSubcontractors(
        internalFormId: Int
    ): List<DailyDemoLogSubcontractorUiData> {
        return dailyDemoLogRepo.getDailyDemoLogSubContractors(internalFormId, companyId).map {
            val subcontractor = if (it.subcontractorID > 0) {
                subcontractorsRepo.getSubcontractorById(it.subcontractorID, companyId)
            } else {
                null
            }
            DailyDemoLogSubcontractorUiData(
                it.dailyDemoLogSubContractorId,
                it.subcontractorID,
                subcontractor?.name ?: it.subcontractorCustom,
                it.phase,
                it.hours
            )
        }
    }

    suspend fun getDailyDemoLogEquipment(
        internalFormId: Int,
    ): List<DailyDemoLogEquipmentUiData> {
        return dailyDemoLogRepo.getDailyDemoLogEquipment(internalFormId, companyId).map {
            val equipment = if (it.equipmentID > 0) {
                inventoryItemsRepo.getInventoryItem(it.equipmentID, companyId)
            } else {
                null
            }
            DailyDemoLogEquipmentUiData(
                dailyDemoLogEquipmentId = it.dailyDemoLogEquipmentId,
                equipmentId = it.equipmentID,
                equipmentName = equipment?.inventoryItemName ?: it.equipmentCustom,
                equipmentNumber = it.equipmentNo,
                descriptionOfWork = it.descriptionOfWork,
                hours = it.hours,
            )
        }
    }

    suspend fun getAllEquipment(): List<InventoryItem> {
        return inventoryItemsRepo.getEquipmentInventoryItems(companyId)
    }

    suspend fun getDailyDemoMaterials(
        internalFormId: Int,
    ): List<DailyDemoLogMaterialUiData> {
        return dailyDemoLogRepo.getDailyDemoLogMaterials(internalFormId, companyId).map {
            val equipment = if (it.materialID > 0) {
                inventoryItemsRepo.getInventoryItem(it.materialID, companyId)
            } else {
                null
            }
            DailyDemoLogMaterialUiData(
                dailyDemoLogMaterialsId = it.dailyDemoLogMaterialsId,
                materialID = it.materialID,
                materialName = equipment?.inventoryItemName ?: it.materialCustom,
                quantity = it.quantity,
                amount = it.amount,
                total = it.total
            )
        }
    }

    suspend fun getDailyDemoTrucking(internalFormId: Int): List<DailyDemoLogTruckingUiData> {
        return dailyDemoLogRepo.getDailyDemoLogTrucking(internalFormId, companyId).map {
            val trucker = if (it.truckingID > 0) {
                haulersRepo.getHaulerById(it.truckingID, companyId)
            } else {
                null
            }
            DailyDemoLogTruckingUiData(
                dailyDemoLogTruckingId = it.dailyDemoLogTruckingId,
                truckingId = it.truckingID,
                truckingName = trucker?.name ?: it.truckingCustom,
                material = it.material,
                loads = it.loads,
                truckNumber = it.truckno,
                hours = it.hours,
                hourlyRate = it.hourlyRate,
                total = it.total
            )
        }
    }

    suspend fun getDailyDemoLogMiscItems(
        internalFormId: Int,
        projectId: Int
    ): DailyDemoLogMiscItems {
        val dailyDemoLogData = getDailyDemoLogData(internalFormId, projectId)
        return DailyDemoLogMiscItems(
            reportableIncident = dailyDemoLogData.reportableIncident,
            incident = dailyDemoLogData.incident,
            incidentDescription = dailyDemoLogData.incidentDescription,
            stopWorkPerformed = dailyDemoLogData.stopWorkPerformed,
            stopWorkReason = dailyDemoLogData.stopWorkReason,
            stopWorkBeyondControlDescription = dailyDemoLogData.stopWorkBeyondControlDescription,
            stopWorkOutOfScopeDescription = dailyDemoLogData.stopWorkOutOfScopeDescription,
            stopWorkBackChargeDescription = dailyDemoLogData.stopWorkBackChargeDescription,
            stopWorkVisitorsDescription = dailyDemoLogData.stopWorkVisitorsDescription,
            stopWorkIssuesDescription = dailyDemoLogData.stopWorkIssuesDescription
        )
    }

    suspend fun getDailyDemoLogMiscComments(internalFormId: Int, projectId: Int): String {
        val dailyDemoLogData = getDailyDemoLogData(internalFormId, projectId)
        return dailyDemoLogData.comments
    }

    fun getDailyDemoLogImagesDirFlow(
        projectId: Int,
        internalFormId: Int
    ): Flow<File> {
        return FileFlow.observeFile(
            fileManager.getDailyDemoFormSavedImagesDir(
                companyId = companyId,
                projectId = projectId,
                internalFormId = internalFormId
            )
        )
    }

    suspend fun saveDailyDemoLogFormData(dailyDemoSaveData: DailyDemoLogSaveData) {
        val currentData =
            getDailyDemoLogData(dailyDemoSaveData.internalFormId, dailyDemoSaveData.projectId)
        val saveData = withContext(Dispatchers.Default) {
            currentData.copy(
                insertDate = dailyDemoSaveData.generalInfoSaveData?.formDate
                    ?: currentData.insertDate,
                supervisorId = dailyDemoSaveData.generalInfoSaveData?.supervisorId
                    ?: currentData.supervisorId,
                supervisorVerified = dailyDemoSaveData.generalInfoSaveData?.supervisorVerified
                    ?: currentData.supervisorVerified,
                supervisorVerifiedDate = dailyDemoSaveData.generalInfoSaveData?.supervisorVerifiedDate
                    ?: currentData.supervisorVerifiedDate,
                supervisorVerifiedByEmployeeId = dailyDemoSaveData.generalInfoSaveData?.supervisorVerifiedByEmployeeId
                    ?: currentData.supervisorVerifiedByEmployeeId,
                pinOutLocation = dailyDemoSaveData.generalInfoSaveData?.pinOutLocation
                    ?: currentData.pinOutLocation,
                safetyTopic = dailyDemoSaveData.generalInfoSaveData?.safetyTopic
                    ?: currentData.safetyTopic,
                productionNarrative = dailyDemoSaveData.productionNarrativeUiData?.productionNarrative
                    ?: currentData.productionNarrative,
                safetyMeetingConductedByEmployeeID = dailyDemoSaveData.productionNarrativeUiData?.safetyMeetingConductedById
                    ?: currentData.safetyMeetingConductedByEmployeeID,
                weatherAM = dailyDemoSaveData.productionNarrativeUiData?.weatherAM
                    ?: currentData.weatherAM,
                weatherPM = dailyDemoSaveData.productionNarrativeUiData?.weatherPM
                    ?: currentData.weatherPM,
                reportableIncident = dailyDemoSaveData.miscItems?.reportableIncident
                    ?: currentData.reportableIncident,
                incident = dailyDemoSaveData.miscItems?.incident ?: currentData.incident,
                incidentDescription = dailyDemoSaveData.miscItems?.incidentDescription
                    ?: currentData.incidentDescription,
                stopWorkPerformed = dailyDemoSaveData.miscItems?.stopWorkPerformed
                    ?: currentData.stopWorkPerformed,
                stopWorkReason = dailyDemoSaveData.miscItems?.stopWorkReason
                    ?: currentData.stopWorkReason,
                stopWorkBeyondControlDescription = dailyDemoSaveData.miscItems?.stopWorkBeyondControlDescription
                    ?: currentData.stopWorkBeyondControlDescription,
                stopWorkOutOfScopeDescription = dailyDemoSaveData.miscItems?.stopWorkOutOfScopeDescription
                    ?: currentData.stopWorkOutOfScopeDescription,
                stopWorkBackChargeDescription = dailyDemoSaveData.miscItems?.stopWorkBackChargeDescription
                    ?: currentData.stopWorkBackChargeDescription,
                stopWorkVisitorsDescription = dailyDemoSaveData.miscItems?.stopWorkVisitorsDescription
                    ?: currentData.stopWorkVisitorsDescription,
                stopWorkIssuesDescription = dailyDemoSaveData.miscItems?.stopWorkIssuesDescription
                    ?: currentData.stopWorkIssuesDescription,
                comments = dailyDemoSaveData.comments ?: currentData.comments,
                dailyDemoLogEmployees = dailyDemoSaveData.employees?.map {
                    DailyDemoLogEmployee(
                        formDataId = currentData.formDataId,
                        internalFormId = currentData.internalFormId,
                        projectId = currentData.projectId,
                        companyId = companyId,
                        employeeId = it.employeeId,
                        descriptionOfWork = it.descriptionOfWork,
                        hours = it.hours
                    )
                } ?: currentData.dailyDemoLogEmployees,
                dailyDemoLogSubContractors = dailyDemoSaveData.subContractors?.map {
                    DailyDemoLogSubContractor(
                        formDataId = currentData.formDataId,
                        internalFormId = currentData.internalFormId,
                        projectId = currentData.projectId,
                        companyId = companyId,
                        subcontractorID = it.subcontractorId,
                        subcontractorCustom = if (it.subcontractorId == 0) it.subcontractorName else "",
                        phase = it.phase,
                        hours = it.hours
                    )
                } ?: currentData.dailyDemoLogSubContractors,
                dailyDemoLogEquipment = dailyDemoSaveData.equipment?.map {
                    DailyDemoLogEquipment(
                        formDataId = currentData.formDataId,
                        internalFormId = currentData.internalFormId,
                        projectId = currentData.projectId,
                        companyId = companyId,
                        equipmentID = it.equipmentId,
                        equipmentCustom = if (it.equipmentId == 0) it.equipmentName else "",
                        equipmentNo = it.equipmentNumber,
                        descriptionOfWork = it.descriptionOfWork,
                        hours = it.hours
                    )
                } ?: currentData.dailyDemoLogEquipment,
                dailyDemoLogMaterials = dailyDemoSaveData.materials?.map {
                    DailyDemoLogMaterials(
                        formDataId = currentData.formDataId,
                        internalFormId = currentData.internalFormId,
                        projectId = currentData.projectId,
                        companyId = companyId,
                        materialID = it.materialID,
                        materialCustom = if (it.materialID == 0) it.materialName else "",
                        quantity = it.quantity,
                        amount = it.amount,
                        total = it.total
                    )
                } ?: currentData.dailyDemoLogMaterials,
                dailyDemoLogTrucking = dailyDemoSaveData.trucking?.map {
                    DailyDemoLogTrucking(
                        formDataId = currentData.formDataId,
                        internalFormId = currentData.internalFormId,
                        projectId = currentData.projectId,
                        companyId = companyId,
                        truckingID = it.truckingId,
                        truckingCustom = if (it.truckingId == 0) it.truckingName else "",
                        material = it.material,
                        loads = it.loads,
                        truckno = it.truckNumber,
                        hours = it.hours,
                        hourlyRate = it.hourlyRate,
                        total = it.total
                    )
                } ?: currentData.dailyDemoLogTrucking
            )
        }

        dailyDemoLogRepo.saveDailyDemoLogFormData(saveData)
        projectFormsRepo.triggerFormUpdate(dailyDemoSaveData.internalFormId, companyId)
    }

    private suspend fun getDailyDemoLogData(
        internalFormId: Int,
        projectId: Int
    ): DailyDemoLogFormData {
        return mutex.withLock {
            val savedData = dailyDemoLogRepo.getDailyDemoLog(internalFormId, companyId)
            if (savedData == null) {
                val newData = DailyDemoLogFormData(
                    companyId = companyId,
                    internalFormId = internalFormId,
                    projectId = projectId,
                    insertDate = Date(),
                    reportableIncident = false,
                    stopWorkPerformed = false
                )
                dailyDemoLogRepo.saveDailyDemoLogFormData(newData)
                val employees = defaultDemoLogEmployeesHelper.getDefaultDailyDemoLogEmployees(
                    projectId,
                    internalFormId
                )
                newData.dailyDemoLogEmployees = employees
                newData
            } else {
                val employees = dailyDemoLogRepo.getDailyDemoLogEmployees(internalFormId, companyId)
                savedData.dailyDemoLogEmployees = employees
                savedData
            }
        }
    }

    fun saveDailyDailyDemoLogPic(projectId: Int, internalFormId: Int, sourcePath: String) {
        val pic = File(sourcePath)
        val picName = pic.nameWithoutExtension + Date().time
        val dest =
            fileManager.getDailyDemoFormPicFile(companyId, projectId, internalFormId, picName)
        pic.copyTo(dest, overwrite = true)
    }
}

data class DailyDemoLogActivityUiData(
    val projectName: String,
    val projectNumber: String,
    val formDate: Date
)

data class DailyDemoLogGeneralInfoUiData(
    val projectName: String,
    val projectNumber: String,
    val supervisor: Employee?,
    val supervisorVerified: Boolean,
    val supervisorVerifiedByEmployeeId: Int,
    val supervisorVerifiedDate: Date,
    val pinOutLocation: File,
    val formDate: Date,
    val safetyTopic: String
)

data class DailyDemoLogProductionNarrativeUiData(
    val productionNarrative: String,
    val safetyMeetingConductedById: Int,
    val safetyMeetingConductedBy: String,
    val weatherAM: String,
    val weatherPM: String,
)

@Parcelize
data class DailyDemoLogEmployeeUiData(
    val dailyDemoLogEmployeeId: Int,
    val employeeId: Int,
    val employeeFullName: String,
    val descriptionOfWork: String,
    val hours: String
) : Parcelable

data class DailyDemoLogSubcontractorUiData(
    val dailyDemoLogSubcontractorId: Int,
    val subcontractorId: Int,
    val subcontractorName: String,
    val phase: String,
    val hours: String
) {
    companion object {
        fun createCustom(customSubcontractorName: String): DailyDemoLogSubcontractorUiData {
            return DailyDemoLogSubcontractorUiData(
                dailyDemoLogSubcontractorId = 0,
                subcontractorId = 0,
                subcontractorName = customSubcontractorName,
                phase = "",
                hours = ""
            )
        }

        fun create(subcontractor: SubcontractorUiModel): DailyDemoLogSubcontractorUiData {
            return DailyDemoLogSubcontractorUiData(
                dailyDemoLogSubcontractorId = 0,
                subcontractorId = subcontractor.subcontractorId,
                subcontractorName = subcontractor.name,
                phase = "",
                hours = ""
            )
        }
    }
}

data class DailyDemoLogEquipmentUiData(
    val dailyDemoLogEquipmentId: Int,
    val equipmentId: Int,
    val equipmentName: String,
    val equipmentNumber: String,
    val descriptionOfWork: String,
    val hours: String
) {
    companion object {
        fun createCustom(customEquipmentName: String): DailyDemoLogEquipmentUiData {
            return DailyDemoLogEquipmentUiData(
                dailyDemoLogEquipmentId = 0,
                equipmentId = 0,
                equipmentName = customEquipmentName,
                equipmentNumber = "",
                descriptionOfWork = "",
                hours = ""
            )
        }

        fun create(equipment: EquipmentUiModel): DailyDemoLogEquipmentUiData {
            return DailyDemoLogEquipmentUiData(
                dailyDemoLogEquipmentId = 0,
                equipmentId = equipment.equipmentId,
                equipmentName = "${equipment.equipmentName} / ${equipment.equipmentNumber}",
                equipmentNumber = "",
                descriptionOfWork = "",
                hours = ""
            )
        }
    }
}

data class DailyDemoLogMaterialUiData(
    val dailyDemoLogMaterialsId: Int,
    val materialID: Int,
    val materialName: String,
    val quantity: String,
    val amount: String,
    val total: String,
) {
    companion object {
        fun createCustom(customEquipmentName: String): DailyDemoLogMaterialUiData {
            return DailyDemoLogMaterialUiData(
                dailyDemoLogMaterialsId = 0,
                materialID = 0,
                materialName = customEquipmentName,
                quantity = "",
                amount = "",
                total = ""

            )
        }

        fun create(material: MaterialUiModel): DailyDemoLogMaterialUiData {
            return DailyDemoLogMaterialUiData(
                dailyDemoLogMaterialsId = 0,
                materialID = material.materialId,
                materialName = material.materialName,
                quantity = "",
                amount = "",
                total = ""
            )
        }
    }
}

data class DailyDemoLogTruckingUiData(
    val dailyDemoLogTruckingId: Int,
    val truckingId: Int,
    val truckingName: String,
    val material: String,
    val loads: String,
    val truckNumber: String,
    val hours: String,
    val hourlyRate: String,
    val total: String
) {
    companion object {
        fun createCustom(customHaulerName: String): DailyDemoLogTruckingUiData {
            return DailyDemoLogTruckingUiData(
                dailyDemoLogTruckingId = 0,
                truckingId = 0,
                truckingName = customHaulerName,
                material = "",
                loads = "",
                truckNumber = "",
                hours = "",
                hourlyRate = "",
                total = ""
            )
        }

        fun create(hauler: HaulerUiModel): DailyDemoLogTruckingUiData {
            return DailyDemoLogTruckingUiData(
                dailyDemoLogTruckingId = 0,
                truckingId = hauler.truckingId,
                truckingName = hauler.name,
                material = "",
                loads = "",
                truckNumber = "",
                hours = "",
                hourlyRate = "",
                total = ""
            )
        }
    }
}

data class DailyDemoLogMiscItems(
    val reportableIncident: Boolean,
    val incident: String,
    val incidentDescription: String,
    val stopWorkPerformed: Boolean,
    val stopWorkReason: String,
    val stopWorkBeyondControlDescription: String,
    val stopWorkOutOfScopeDescription: String,
    val stopWorkBackChargeDescription: String,
    val stopWorkVisitorsDescription: String,
    val stopWorkIssuesDescription: String
)
