package com.fieldflo.usecases

import com.fieldflo.data.persistence.fileSystem.FileManager
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.IEmployeesRepo
import java.math.BigInteger
import java.security.MessageDigest
import javax.inject.Inject

class PinUseCases @Inject constructor(
    private val employeesRepo: IEmployeesRepo,
    private val localStorage: ILocalStorage,
    private val fileManager: FileManager
) {

    val companyId: Int get() = localStorage.getCurrentCompanyId()

    fun deletePhotoPins() {
        if (localStorage.deletesPhotoPin()) {
            fileManager.deleteExpiredPhotoPins(localStorage.getCurrentCompanyId())
        }
    }

    suspend fun validatePinByEmployeeAndSupervisor(
        employeeId: Int,
        enteredPin: String,
        supervisorId: Int?
    ): EmployeeVerificationResult {
        // first check against employee pin
        employeesRepo.getEmployeeById(employeeId, companyId)?.let { employee ->
            if (validatePin(enteredPin, employee.employeePinNumber)) {
                return EmployeeVerificationResult(
                    employeeId = employeeId,
                    verifiedByName = employee.employeeFullName,
                    verifiedById = employeeId,
                    isVerified = true
                )
            }
        }

        if (supervisorId != null) {
            employeesRepo.getEmployeeById(supervisorId, companyId)?.let { supervisor ->
                if (validatePin(enteredPin, supervisor.employeePinNumber)) {
                    return EmployeeVerificationResult(
                        employeeId = employeeId,
                        verifiedByName = supervisor.employeeFullName,
                        verifiedById = supervisor.employeeId,
                        isVerified = true
                    )
                }
            }
        }

        return EmployeeVerificationResult(
            employeeId = employeeId,
            verifiedByName = "",
            verifiedById = employeeId,
            isVerified = false
        )
    }

    companion object {
        fun validatePin(enteredPin: String, savedPinHash: String): Boolean {
            if (savedPinHash.length == 4 && savedPinHash.isInt()) {
                return enteredPin == savedPinHash
            }

            val enteredMd5 = enteredPin.toMd5()
            return enteredMd5 == savedPinHash
        }

        private fun String.toMd5(): String {
            val md = MessageDigest.getInstance("MD5")
            return String.format("%032x", BigInteger(1, md.digest(toByteArray())))
        }

        private fun String.isInt(): Boolean {
            return try {
                toInt()
                true
            } catch (e: Exception) {
                false
            }
        }
    }

    data class EmployeeVerificationResult(
        val employeeId: Int,
        val verifiedByName: String,
        val verifiedById: Int,
        val isVerified: Boolean
    )
}
