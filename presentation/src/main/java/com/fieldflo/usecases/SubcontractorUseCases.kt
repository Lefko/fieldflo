package com.fieldflo.usecases

import com.fieldflo.data.persistence.db.entities.SubContractor
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.ISubContractorsRepo
import javax.inject.Inject

class SubcontractorUseCases @Inject constructor(
    private val localStorage: ILocalStorage,
    private val subcontractorsRepo: ISubContractorsRepo
) {

    private val companyId: Int
        get() = localStorage.getCurrentCompanyId()

    suspend fun getAllSubcontractors(): List<SubContractor> {
        return subcontractorsRepo.getAllSubContractors(companyId)
    }
}