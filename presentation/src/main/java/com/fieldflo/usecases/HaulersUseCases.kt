package com.fieldflo.usecases

import com.fieldflo.data.persistence.db.entities.Hauler
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.IHaulersRepo
import javax.inject.Inject

class HaulersUseCases @Inject constructor(
    private val localStorage: ILocalStorage,
    private val haulersRepo: IHaulersRepo
) {

    private val companyId: Int
        get() = localStorage.getCurrentCompanyId()

    suspend fun getAllHaulers(): List<Hauler> {
        return haulersRepo.getAllHaulers(companyId)
    }
}