package com.fieldflo.usecases

import com.fieldflo.common.addDays
import com.fieldflo.data.persistence.db.entities.*
import com.fieldflo.data.persistence.fileSystem.FileManager
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.*
import com.fieldflo.screens.formTimesheet.TimeSheetEmployeeState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class TimeSheetUseCases @Inject constructor(
    private val localStorage: ILocalStorage,
    private val fileManager: FileManager,
    private val employeesRepo: IEmployeesRepo,
    private val positionsRepo: IPositionsRepo,
    private val phasesRepo: IPhasesRepo,
    private val inventoryItemsRepo: IInventoryItemsRepo,
    private val companyCertificatesRepo: ICompanyCertificatesRepo,
    private val projectCertsRepo: IProjectCertificatesRepo,
    private val projectsRepo: IProjectsRepo,
    private val timeSheetRepo: ITimesheetRepo
) {
    private val companyId get() = localStorage.getCurrentCompanyId()

    suspend fun getTimeSheetDataFlow(internalFormId: Int, projectId: Int): Flow<TimeSheetUiData> {
        val projectName = projectsRepo.getProjectName(projectId, companyId)
        return timeSheetRepo.getTimeSheetDataFlow(projectId, internalFormId, companyId)
            .map {
                val supervisor = employeesRepo.getEmployeeById(it.supervisorId, companyId)
                it.toUiData(projectName, supervisor)
            }
    }

    suspend fun getTimeSheetEmployeesFlow(
        internalFormId: Int,
        projectId: Int
    ): Flow<List<TimeSheetEmployeeUiData>> {
        val project = projectsRepo.getProjectById(projectId, companyId)
        return timeSheetRepo.getTimesheetEmployeesFlow(internalFormId, companyId)
            .map {
                it.map { timeSheetEmployee ->
                    val employee =
                        employeesRepo.getEmployeeById(timeSheetEmployee.employeeId, companyId)
                    val isTracking = timeSheetRepo.isEmployeeClockedInOnAnyTimesheet(
                        timeSheetEmployee.employeeId,
                        companyId
                    )
                    val defaultPosition = timeSheetEmployee.positions.find { it.defaultPosition }
                    val position = if (defaultPosition != null) {
                        positionsRepo.getPositionById(defaultPosition.positionId, companyId)
                    } else {
                        null
                    }
                    val phase = if (defaultPosition != null) {
                        phasesRepo.getPhaseById(defaultPosition.phaseId, companyId)
                    } else {
                        null
                    }
                    TimeSheetEmployeeUiData(
                        timeSheetEmployeeId = timeSheetEmployee.timeSheetEmployeeId,
                        employeeId = timeSheetEmployee.employeeId,
                        employeeImageFullUrl = employee?.employeeImageFullUrl ?: "",
                        employeeFullName = employee?.employeeFullName ?: "",
                        positionName = position?.positionName ?: "",
                        phaseName = if (project.multiplePhaseType) {
                            if (phase != null) {
                                "${phase.phaseName} - ${phase.phaseCode}"
                            } else {
                                ""
                            }
                        } else {
                            project.phaseCode
                        },
                        usePhases = project.usePhases,
                        fixedPhases = !project.multiplePhaseType,
                        employeeState = TimeSheetEmployeeState.create(
                            project = project,
                            isTrackingOnTimesheet = isTracking,
                            defaultPosition = defaultPosition,
                            timeSheetEmployee = timeSheetEmployee
                        ),
                        isTracking = isTracking
                    )
                }
            }
    }

    suspend fun getTimeSheetNotes(internalFormId: Int): String {
        return timeSheetRepo.getTimeSheetNotes(internalFormId, companyId)
    }

    suspend fun getFullTimeSheetEmployeeFlow(timeSheetEmployeeId: Int): Flow<FullTimeSheetEmployeeUiData> {
        return timeSheetRepo.getEmployeeByIdFlow(timeSheetEmployeeId, companyId)
            .map {
                it.toFullUiData(
                    projectsRepo,
                    timeSheetRepo,
                    employeesRepo,
                    inventoryItemsRepo,
                    positionsRepo,
                    phasesRepo,
                    fileManager
                )
            }
    }

    suspend fun getFullTimeSheetEmployee(timeSheetEmployeeId: Int): FullTimeSheetEmployeeUiData? {
        return timeSheetRepo.getEmployeeById(timeSheetEmployeeId, companyId)
            ?.let {
                it.toFullUiData(
                    projectsRepo,
                    timeSheetRepo,
                    employeesRepo,
                    inventoryItemsRepo,
                    positionsRepo,
                    phasesRepo,
                    fileManager
                )
            }
    }

    suspend fun saveTimeSheetNotes(internalFormId: Int, newNotes: String) {
        timeSheetRepo.saveTimeSheetNotes(internalFormId, newNotes, companyId)
    }

    suspend fun addEmployeesToTimeSheet(
        employeeIdsToAdd: List<Int>,
        internalFormId: Int
    ): List<String> {
        // convert the employeeIds to Employees
        val allEmployeesToAdd = employeeIdsToAdd.mapNotNull {
            employeesRepo.getEmployeeById(it, companyId)
        }
        Timber.d("allEmployeesToAdd: $allEmployeesToAdd")

        // get employeeIds already on this timesheet and not stopped
        val employeesNotStoppedOnTimeSheet = timeSheetRepo
            .getTimeSheetEmployeesByInternalFormId(internalFormId, companyId)
            .filter { it.employeeStatus.ordinal < TimesheetEmployeeStatus.CLOCKED_OUT.ordinal }
            .mapNotNull { employeesRepo.getEmployeeById(it.employeeId, companyId) }
        // filter out employees who are already on this timesheet AND not stopped
        val employeesToAdd = allEmployeesToAdd.filter { it !in employeesNotStoppedOnTimeSheet }
        Timber.d("employeesToAdd: $employeesToAdd")

        // add employees to time sheet
        val timeSheetEmployees =
            timeSheetRepo.addEmployeesToTimeSheet(employeesToAdd, internalFormId, companyId)
        Timber.d("timeSheetEmployees: $timeSheetEmployees")

        // employee ids of the newly added time sheet employees
        val timeSheetEmployeeEmployeeIds = timeSheetEmployees.map { it.employeeId }
        Timber.d("timeSheetEmployeeEmployeeIds: $timeSheetEmployeeEmployeeIds")

        // determine which of the employees to add were actually added
        val employeesAdded = employeesToAdd.filter {
            it.employeeId in timeSheetEmployeeEmployeeIds
        }
        Timber.d("employeesAdded: $employeesAdded")

        // add time sheet employee default position when we have one
        timeSheetEmployees.forEach { timeSheetEmployee ->
            Timber.d("TimeSheetEmployee: $timeSheetEmployee")
            employeesToAdd.find { it.employeeId == timeSheetEmployee.employeeId }?.let { employee ->
                Timber.d("TimeSheetEmployee Employee: $employee ")
                positionsRepo.getPositionById(employee.positionId, companyId)?.let { position ->
                    Timber.d("Position: $position")
                    Timber.d("setEmployeeDefaultPosition(${timeSheetEmployee.timeSheetEmployeeId}, $position, $companyId")
                    timeSheetRepo.setEmployeeDefaultPosition(
                        timeSheetEmployee.timeSheetEmployeeId,
                        position,
                        companyId
                    )
                }
            }
        }

        val employeesNotAdded = (allEmployeesToAdd - employeesAdded)
        Timber.d("employeesNotAdded: $employeesNotAdded")

        return employeesNotAdded.map { it.employeeFullName }
    }

    suspend fun removeEmployeeFromTimeSheet(timeSheetEmployeeId: Int) {
        timeSheetRepo.removeEmployeeFromTimesheet(timeSheetEmployeeId, companyId)
    }

    suspend fun updateSupervisor(employeeId: Int, internalFormId: Int) {
        timeSheetRepo.updateSupervisor(employeeId, internalFormId, companyId)
    }

    suspend fun setEmployeePosition(timeSheetEmployeeId: Int, positionId: Int) {
        positionsRepo.getPositionById(positionId, companyId)?.let { position ->
            timeSheetRepo.setEmployeeDefaultPosition(timeSheetEmployeeId, position, companyId)
        }
    }

    suspend fun getPositionsForTimeSheetAdditionalPosition(
        projectId: Int,
        timeSheetEmployeeId: Int
    ): List<Position> {
        val allPositions = positionsRepo.getPositionsForProject(projectId, companyId)
        val employee = timeSheetRepo.getEmployeeById(timeSheetEmployeeId, companyId)
        val employeePositionIds = (employee?.positions ?: emptyList()).map { it.positionId }
        return allPositions.filter {
            it.positionId !in employeePositionIds
        }.sortedBy { it.positionName }
    }

    suspend fun getTimeSheetEmployeePosition(
        timeSheetEmployeeId: Int,
        timeSheetEmployeePositionId: Int
    ): TimesheetEmployeePosition? {
        return timeSheetRepo.getEmployeeById(timeSheetEmployeeId, companyId)
            ?.positions?.firstOrNull { it.timeSheetEmployeePositionId == timeSheetEmployeePositionId }
    }

    suspend fun getAvailableTimeForPosition(
        timeSheetEmployeeId: Int,
        timeSheetEmployeePositionId: Int? = null
    ): Int {
        return timeSheetRepo.getEmployeeById(timeSheetEmployeeId, companyId)?.let {
            val defaultPosition: TimesheetEmployeePosition? =
                it.positions.firstOrNull { it.defaultPosition }
            when {
                defaultPosition == null -> 0

                timeSheetEmployeePositionId == null ->
                    // not editing a position so can only use up time left over in default position
                    defaultPosition.totalWorkTime

                else -> {
                    // editing time so can also use time in position itself
                    val positionEditing: TimesheetEmployeePosition? = it.positions.firstOrNull {
                        it.timeSheetEmployeePositionId == timeSheetEmployeePositionId
                    }
                    defaultPosition.totalWorkTime + (positionEditing?.totalWorkTime ?: 0)
                }
            }
        } ?: 0
    }

    suspend fun addPosition(
        timeSheetEmployeeId: Int,
        positionId: Int,
        phaseId: Int,
        positionWorkTimeSeconds: Int
    ): Boolean {
        val employee = timeSheetRepo.getEmployeeById(timeSheetEmployeeId, companyId) ?: return false
        val employeePositions = employee.positions
        val defaultPosition = employeePositions.firstOrNull { it.defaultPosition } ?: return false

        val positionIds = employeePositions.map { it.positionId }
        if (positionId !in positionIds) {
            val newPosition = TimesheetEmployeePosition(
                internalFormId = employee.internalFormId,
                timeSheetFormDataId = employee.timeSheetFormDataId,
                companyId = companyId,
                projectId = employee.projectId,
                timeSheetEmployeeId = timeSheetEmployeeId,
                positionId = positionId,
                defaultPosition = false,
                totalWorkTime = positionWorkTimeSeconds,
                phaseId = phaseId
            )

            val newPositions = employeePositions.map {
                if (it.timeSheetEmployeePositionId == defaultPosition.timeSheetEmployeePositionId) {
                    it.copy(totalWorkTime = defaultPosition.totalWorkTime - positionWorkTimeSeconds)
                } else {
                    it
                }
            } + newPosition

            timeSheetRepo.updatePositions(timeSheetEmployeeId, newPositions, companyId)

            return true
        } else {
            return false
        }
    }

    suspend fun editPosition(
        timeSheetEmployeeId: Int,
        timeSheetEmployeePositionId: Int,
        newPositionId: Int,
        phaseId: Int,
        newPositionWorkTimeSeconds: Int
    ): Boolean {
        val employee = timeSheetRepo.getEmployeeById(timeSheetEmployeeId, companyId) ?: return false
        val employeePositions = employee.positions
        val defaultPosition = employeePositions.firstOrNull { it.defaultPosition } ?: return false
        val positionToEdit = employeePositions.firstOrNull {
            it.timeSheetEmployeePositionId == timeSheetEmployeePositionId
        } ?: return false
        val positionIds = employeePositions.map { it.positionId }

        val validPosition =
            positionToEdit.positionId == newPositionId || !positionIds.contains(newPositionId)
        if (!validPosition) {
            return false
        }

        val time = defaultPosition.totalWorkTime + positionToEdit.totalWorkTime
        if (time > newPositionWorkTimeSeconds) {
            val newPositions = employeePositions.map {
                when (it.timeSheetEmployeePositionId) {
                    defaultPosition.timeSheetEmployeePositionId ->
                        it.copy(totalWorkTime = defaultPosition.totalWorkTime + positionToEdit.totalWorkTime - newPositionWorkTimeSeconds)

                    timeSheetEmployeePositionId -> it.copy(
                        positionId = newPositionId,
                        phaseId = phaseId,
                        totalWorkTime = newPositionWorkTimeSeconds
                    )

                    else -> it
                }
            }
            timeSheetRepo.updatePositions(timeSheetEmployeeId, newPositions, companyId)
            return true
        } else {
            return false
        }
    }

    suspend fun setEmployeePhase(timeSheetEmployeeId: Int, phaseId: Int) {
        timeSheetRepo.setEmployeePhase(phaseId, timeSheetEmployeeId, companyId)
    }

    suspend fun verifyTimeSheetPin(
        internalFormId: Int,
        employeeId: Int = 0,
        pin: String
    ): VerifyTimeSheetPinResult {
        if (employeeId > 0) {
            employeesRepo.getEmployeeById(employeeId, companyId)?.let { employee ->
                if (PinUseCases.validatePin(pin, employee.employeePinNumber)) {
                    return VerifyTimeSheetPinResult.VerifySuccess(employeeId)
                }
            }
        }

        val supervisorId = timeSheetRepo.getTimeSheetSupervisorId(internalFormId, companyId)
        if (supervisorId > 0) {
            employeesRepo.getEmployeeById(supervisorId, companyId)?.let { supervisor ->
                if (PinUseCases.validatePin(pin, supervisor.employeePinNumber)) {
                    return VerifyTimeSheetPinResult.VerifySuccess(supervisorId)
                }
            }
        }

        if (localStorage.canUserCanValidateTimeSheetWithPin()) {
            if (PinUseCases.validatePin(pin, localStorage.getEmployeePinNumber())) {
                return VerifyTimeSheetPinResult.VerifySuccess(localStorage.getEmployeeId())
            }
        }

        return VerifyTimeSheetPinResult.VerifyError
    }

    suspend fun setEmployeesVerified(
        timeSheetEmployeeIds: List<Int>,
        injured: Boolean,
        tookBreak: Boolean,
        verifiedByEmployeeId: Int
    ) {
        timeSheetRepo.verifyEmployees(
            timeSheetEmployeeIds,
            verifiedByEmployeeId,
            injured,
            tookBreak,
            companyId
        )
    }

    suspend fun getTimeSheetEmployeePerDiem(timeSheetEmployeeId: Int): TimeSheetEmployeePerDiem? {
        val employee = timeSheetRepo.getEmployeeById(timeSheetEmployeeId, companyId) ?: return null

        val perDiem = employee.perDiem ?: return null

        return perDiem.copy(totalWorkTime = employee.totalWorkTime)
    }

    suspend fun setPerDiem(
        timeSheetEmployeeId: Int,
        isHourly: Boolean,
        amount: Double,
        cashGiven: Double,
        notes: String
    ) {
        timeSheetRepo.getEmployeeById(timeSheetEmployeeId, companyId)?.let { timeSheetEmployee ->
            val timeSheetPerDiem = TimeSheetPerDiemFactory.createPerDiem(
                perDiemInfo = PerDiemInfo(false, isHourly, amount),
                totalTimeSeconds = timeSheetEmployee.totalWorkTime.toDouble(),
                cashGiven = cashGiven,
                notes = notes
            )
            timeSheetRepo.setPerDiem(timeSheetEmployeeId, timeSheetPerDiem, companyId)
        }
    }

    suspend fun deletePerDiem(timeSheetEmployeeId: Int) {
        timeSheetRepo.deletePerDiem(timeSheetEmployeeId, companyId)
    }

    suspend fun getInventoryItemsForTimeSheetAsset(
        timeSheetEmployeeId: Int,
        timeSheetEmployeeAssetId: Int?
    ): TimeSheetAssetData {
        val items = inventoryItemsRepo.getEquipmentInventoryItems(companyId).toMutableList()
        val containsSpecial = items.find { it.inventoryItemId == 0 } != null
        if (!containsSpecial) {
            items.add(InventoryItem(companyId, 0, "Special Item", 0, ""))
        }

        val allAssets =
            (timeSheetRepo.getEmployeeById(timeSheetEmployeeId, companyId)?.assets ?: emptyList())
        val excludingCurrent =
            allAssets.filter { it.timeSheetEmployeeAssetId != timeSheetEmployeeAssetId }
        val employeeItemIds = excludingCurrent.map { it.itemId }

        val employeeAsset = timeSheetEmployeeAssetId?.let {
            allAssets.find { it.timeSheetEmployeeAssetId == timeSheetEmployeeAssetId }
        }

        val assetData = employeeAsset?.let { timeSheetEmployeeAsset ->
            val item = items.find { it.inventoryItemId == timeSheetEmployeeAsset.itemId }
            TimeSheetEmployeeAssetData(
                timeSheetEmployeeAssetId = timeSheetEmployeeAsset.timeSheetEmployeeAssetId,
                assetId = timeSheetEmployeeAsset.itemId,
                assetName = item?.inventoryItemName ?: "",
                assetNumber = item?.inventoryItemNumber ?: 0,
                totalWorkTime = timeSheetEmployeeAsset.totalWorkTime,
                notes = timeSheetEmployeeAsset.notes
            )
        }
        return TimeSheetAssetData(
            allItems = items.filter { it.inventoryItemId !in employeeItemIds },
            employeeAsset = assetData
        )
    }

    suspend fun addTimeSheetEmployeeAsset(
        timeSheetEmployeeId: Int,
        inventoryItemId: Int,
        totalWorkTimeSeconds: Int,
        notes: String
    ) {
        timeSheetRepo.getEmployeeById(timeSheetEmployeeId, companyId)?.let { timeSheetEmployee ->
            val newAsset = TimeSheetEmployeeAsset(
                companyId = companyId,
                projectId = timeSheetEmployee.projectId,
                internalFormId = timeSheetEmployee.internalFormId,
                timeSheetFormDataId = timeSheetEmployee.timeSheetFormDataId,
                timeSheetEmployeeId = timeSheetEmployee.timeSheetEmployeeId,
                employeeId = timeSheetEmployee.employeeId,
                itemId = inventoryItemId,
                totalWorkTime = totalWorkTimeSeconds,
                notes = notes
            )
            timeSheetRepo.addAsset(newAsset)
        }
    }

    suspend fun editTimeSheetEmployeeAsset(
        timeSheetEmployeeId: Int,
        timeSheetEmployeeAssetId: Int,
        newInventoryItemId: Int,
        newTotalWorkTimeSeconds: Int,
        newNotes: String
    ) {
        timeSheetRepo.getEmployeeById(timeSheetEmployeeId, companyId)?.let { timeSheetEmployee ->
            timeSheetEmployee.assets.find { it.timeSheetEmployeeAssetId == timeSheetEmployeeAssetId }
                ?.let { oldAsset ->
                    val editedAsset = oldAsset.copy(
                        itemId = newInventoryItemId,
                        totalWorkTime = newTotalWorkTimeSeconds,
                        notes = newNotes
                    )
                    timeSheetRepo.editAsset(editedAsset)
                }
        }
    }

    suspend fun deleteTimeSheetEmployeeAsset(timeSheetEmployeeAssetId: Int) {
        timeSheetRepo.deleteTimeSheetEmployeeAsset(timeSheetEmployeeAssetId, companyId)
    }

    suspend fun deletePosition(timeSheetEmployeeId: Int, timeSheetEmployeePositionId: Int) {
        val positions = timeSheetRepo
            .getEmployeePositions(timeSheetEmployeeId, companyId).toMutableList()

        val positionToDelete = positions.find {
            it.timeSheetEmployeePositionId == timeSheetEmployeePositionId
        } ?: return
        val defaultPosition = positions.find { it.defaultPosition } ?: return

        positions.remove(positionToDelete)
        positions.remove(defaultPosition)

        val updatedDefaultPosition = defaultPosition.copy(
            totalWorkTime = defaultPosition.totalWorkTime + positionToDelete.totalWorkTime
        )

        positions.add(updatedDefaultPosition)

        timeSheetRepo.updatePositions(timeSheetEmployeeId, positions, companyId)
    }

    suspend fun getTimeSheetEmployeePinOutLocation(
        internalFormId: Int,
        timeSheetEmployeeId: Int
    ): String {
        val timeSheetData = timeSheetRepo.getTimeSheetData(internalFormId, companyId)
        return fileManager.getTimesheetPhotoPinFileLocation(
            companyId = companyId,
            projectId = timeSheetData.projectId,
            timeSheetEmployeeId = timeSheetEmployeeId,
            timeSheetDate = timeSheetData.formDate,
            internalFormId = internalFormId
        ).absolutePath
    }
}

sealed class VerifyTimeSheetPinResult {
    data class VerifySuccess(val verifiedByEmployeeId: Int) : VerifyTimeSheetPinResult()
    object VerifyError : VerifyTimeSheetPinResult()
}

data class TimeSheetUiData(
    val formDataId: Int,
    val formDate: Date,
    val projectName: String,
    val supervisorId: Int,
    val supervisorName: String
)

data class TimeSheetAssetData(
    val allItems: List<InventoryItem>,
    val employeeAsset: TimeSheetEmployeeAssetData?
)

data class TimeSheetEmployeeAssetData(
    val timeSheetEmployeeAssetId: Int,
    val assetId: Int,
    val assetName: String,
    val assetNumber: Int,
    val totalWorkTime: Int,
    val notes: String
)

data class TimeSheetEmployeePositionData(
    val timeSheetEmployeePositionId: Int,
    val positionId: Int,
    val positionName: String,
    val defaultPosition: Boolean,
    val totalWorkTime: Int,
    val phaseId: Int,
    val phaseName: String
)

data class TimeSheetEmployeeUiData(
    val timeSheetEmployeeId: Int,
    val employeeId: Int,
    val employeeImageFullUrl: String,
    val employeeFullName: String,
    val phaseName: String,
    val positionName: String,
    val usePhases: Boolean,
    val fixedPhases: Boolean,
    val employeeState: TimeSheetEmployeeState,
    val isTracking: Boolean
)

data class FullTimeSheetEmployeeUiData(
    val formDate: Date,
    val usePhases: Boolean,
    val phaseCode: String,
    val fixedPhases: Boolean,

    val employeeId: Int,
    val timeSheetEmployeeId: Int,
    val timeSheetEmployeeImageUrl: String,
    val timeSheetEmployeeName: String,
    val employeeStatus: TimeSheetEmployeeState,
    val isTrackingOnAnyTimeSheet: Boolean,

    val clockIn: Date,
    val clockOut: Date,
    val totalWorkTime: Int,
    val timeVerifiedDate: Date,
    val timeVerifiedByName: String,
    val injuryVerifiedDate: Date,
    val injuryVerifiedByName: String,

    val tookBreak: Boolean,
    val breakTime: Int,
    val breakVerifiedDate: Date,
    val breakVerifiedByName: String,

    val pinImageLocation: String = "",
    val pinImageDeletingOn: Date,

    val perDiem: TimeSheetEmployeePerDiem? = null,
    val assets: List<TimeSheetEmployeeAssetData>,
    val positions: List<TimeSheetEmployeePositionData>
)

private fun TimeSheetFormData.toUiData(
    projectName: String,
    supervisor: Employee?
): TimeSheetUiData {
    return TimeSheetUiData(
        formDataId = formDataId,
        formDate = formDate,
        projectName = projectName,
        supervisorId = supervisorId,
        supervisorName = supervisor?.employeeFullName ?: ""
    )
}

private fun TimeSheetEmployeeAsset.toUiData(inventoryItem: InventoryItem?) =
    TimeSheetEmployeeAssetData(
        timeSheetEmployeeAssetId = this.timeSheetEmployeeAssetId,
        assetId = this.itemId,
        assetName = inventoryItem?.inventoryItemName ?: "",
        assetNumber = inventoryItem?.inventoryItemNumber ?: 0,
        totalWorkTime = this.totalWorkTime,
        notes = this.notes
    )

private fun TimesheetEmployeePosition.toUiData(position: Position?, phase: Phase?) =
    TimeSheetEmployeePositionData(
        timeSheetEmployeePositionId = this.timeSheetEmployeePositionId,
        positionId = this.positionId,
        positionName = position?.positionName ?: "",
        defaultPosition = this.defaultPosition,
        totalWorkTime = this.totalWorkTime,
        phaseId = this.phaseId,
        phaseName =  if (phase != null) { "${phase.phaseName} - ${phase.phaseCode}" } else ""
    )

private suspend fun TimeSheetEmployee.toFullUiData(
    projectsRepo: IProjectsRepo,
    timeSheetRepo: ITimesheetRepo,
    employeesRepo: IEmployeesRepo,
    inventoryItemsRepo: IInventoryItemsRepo,
    positionsRepo: IPositionsRepo,
    phasesRepo: IPhasesRepo,
    fileManager: FileManager
): FullTimeSheetEmployeeUiData {
    val project = projectsRepo.getProjectById(this.projectId, companyId)
    val timeSheetFormData = timeSheetRepo.getTimeSheetData(this.internalFormId, companyId)
    val isTracking = timeSheetRepo.isEmployeeClockedInOnAnyTimesheet(
        this.employeeId,
        companyId
    )
    val employee = employeesRepo.getEmployeeById(this.employeeId, companyId)
    val timeVerifiedByEmployee = employeesRepo.getEmployeeById(this.timeVerifiedById, companyId)
    val injuryVerifiedByEmployee =
        employeesRepo.getEmployeeById(this.injuredVerifiedById, companyId)
    val breakVerifiedByEmployee =
        employeesRepo.getEmployeeById(this.federalBreakVerifyEmployeeId, companyId)
    return FullTimeSheetEmployeeUiData(
        formDate = timeSheetFormData.formDate,
        usePhases = project.usePhases,
        phaseCode = project.phaseCode,
        fixedPhases = !project.multiplePhaseType,

        employeeId = this.employeeId,
        timeSheetEmployeeId = timeSheetEmployeeId,
        timeSheetEmployeeImageUrl = employee?.employeeImageFullUrl ?: "",
        timeSheetEmployeeName = employee?.employeeFullName ?: "",
        employeeStatus = TimeSheetEmployeeState.create(
            project = project,
            isTrackingOnTimesheet = isTracking,
            defaultPosition = this.positions.firstOrNull { it.defaultPosition },
            timeSheetEmployee = this
        ),
        isTrackingOnAnyTimeSheet = isTracking,

        clockIn = this.clockIn,
        clockOut = this.clockOut,
        totalWorkTime = this.totalWorkTime,
        timeVerifiedDate = this.timeVerifiedByDate,
        timeVerifiedByName = timeVerifiedByEmployee?.employeeFullName ?: "",
        injuryVerifiedDate = this.timeVerifiedByDate,
        injuryVerifiedByName = injuryVerifiedByEmployee?.employeeFullName ?: "",

        tookBreak = this.federalBreak,
        breakTime = this.breakTime,
        breakVerifiedDate = this.federalBreakVerifyDate,
        breakVerifiedByName = breakVerifiedByEmployee?.employeeFullName ?: "",

        pinImageLocation = fileManager.getTimesheetPhotoPinFileLocation(
            companyId = companyId,
            projectId = timeSheetFormData.projectId,
            timeSheetEmployeeId = timeSheetEmployeeId,
            timeSheetDate = timeSheetFormData.formDate,
            internalFormId = this.internalFormId
        ).absolutePath,
        pinImageDeletingOn = timeSheetFormData.formDate.addDays(30),

        perDiem = this.perDiem,
        assets = this.assets.map {
            val inventoryItem =
                inventoryItemsRepo.getInventoryItem(it.itemId, companyId)
            it.toUiData(inventoryItem)
        },
        positions = this.positions.map {
            val position = positionsRepo.getPositionById(it.positionId, companyId)
            val phase = phasesRepo.getPhaseById(it.phaseId, companyId)
            it.toUiData(position, phase)
        }
    )
}
