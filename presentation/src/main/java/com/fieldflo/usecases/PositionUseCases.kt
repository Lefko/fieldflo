package com.fieldflo.usecases

import com.fieldflo.data.persistence.db.entities.Position
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.IPositionsRepo
import com.fieldflo.data.repos.interfaces.ITimesheetRepo
import javax.inject.Inject

class PositionUseCases @Inject constructor(
    private val positionsRepo: IPositionsRepo,
    private val timesheetRepo: ITimesheetRepo,
    private val localStorage: ILocalStorage
) {
    private val companyId: Int get() = localStorage.getCurrentCompanyId()

    suspend fun getPositionsForTimeSheetDefaultPosition(
        projectId: Int,
        timeSheetEmployeeId: Int
    ): List<Position> {
        val allPositions = positionsRepo.getPositionsForProject(projectId, companyId)
        val timeSheetEmployeePositions =
            timesheetRepo.getEmployeePositions(timeSheetEmployeeId, companyId)
        val defaultPosition = timeSheetEmployeePositions.find { it.defaultPosition }

        val currentPositionIds = timeSheetEmployeePositions.filter {
            defaultPosition == null || it.positionId != defaultPosition.positionId
        }.map { it.positionId }

        return allPositions.filter {
            !currentPositionIds.contains(it.positionId)
        }.sortedBy { it.positionName.toLowerCase() }
    }
}