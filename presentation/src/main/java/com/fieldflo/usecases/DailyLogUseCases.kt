package com.fieldflo.usecases

import com.fieldflo.data.persistence.db.entities.DailyLogFormData
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.IDailyLogRepo
import com.fieldflo.data.repos.interfaces.IProjectFormsRepo
import com.fieldflo.data.repos.interfaces.IProjectsRepo
import java.util.*
import javax.inject.Inject

class DailyLogUseCases @Inject constructor(
    private val localStorage: ILocalStorage,
    private val projectsRepo: IProjectsRepo,
    private val projectFormsRepo: IProjectFormsRepo,
    private val dailyLogRepo: IDailyLogRepo
) {
    private val companyId: Int
        get() = localStorage.getCurrentCompanyId()

    suspend fun getDailyLogData(internalFormId: Int, projectId: Int): DailyLogFormData {
        val savedData = dailyLogRepo.getDailyLog(internalFormId, companyId)
        return if (savedData == null) {
            val project = projectsRepo.getProjectById(projectId, companyId)
            DailyLogFormData(
                projectManagerFullName = project.projectManagerFullName,
                companyId = companyId,
                projectId = projectId,
                internalFormId = internalFormId,
                formDate = Date()
            )
        } else {
            savedData
        }
    }

    suspend fun saveDailyLogData(dailyLogForm: SaveDailyLogData) {
        val project = projectsRepo.getProjectById(dailyLogForm.projectId, companyId)
        val formData = dailyLogForm.toDailyLogFormData(companyId, project.projectManagerFullName)
        dailyLogRepo.saveDailyLogForm(formData)
        projectFormsRepo.triggerFormUpdate(dailyLogForm.internalFormId, companyId)
    }

    data class SaveDailyLogData(
        val projectId: Int,
        val internalFormId: Int,

        val date: Date,
        val shiftStart: String,
        val shiftEnd: String,

        val supervisorId: Int,
        val supervisorFullName: String,
        val supervisorVerify: Boolean,

        val dailyLog: String,
        val visitorsToSite: String,
        val statusAtQuittingTime: String,
        val inspectionsMadeOrTestsPerformed: String,
        val dailyBagCount: String,
        val projectToDateBagCount: String,
        val containerNo: String,
        val bagsInContainerToDate: String,
        val unusualConditionsOrProblemsAndActionTaken: String
    )
}

private fun DailyLogUseCases.SaveDailyLogData.toDailyLogFormData(
    companyId: Int,
    projectManagerFullName: String
): DailyLogFormData {
    return DailyLogFormData(
        companyId = companyId,
        projectId = this.projectId,
        internalFormId = this.internalFormId,
        formDate = this.date,
        shiftStart = this.shiftStart,
        shiftEnd = this.shiftEnd,
        supervisorId = this.supervisorId,
        supervisorFullName = this.supervisorFullName,
        supervisorVerify = this.supervisorVerify,
        supervisorVerifyEmployeeId = this.supervisorId,
        supervisorVerifyDate = Date(),
        dailyLog = this.dailyLog,
        visitorsToSite = this.visitorsToSite,
        statusAtQuittingTime = this.statusAtQuittingTime,
        inspectionsMadeOrTestsPerformed = this.inspectionsMadeOrTestsPerformed,
        dailyBagCount = this.dailyBagCount,
        projectToDateBagCount = this.projectToDateBagCount,
        containerNo = this.containerNo,
        bagsInContainerToDate = this.bagsInContainerToDate,
        unusualConditionsOrProblemsAndActionTaken = this.unusualConditionsOrProblemsAndActionTaken,
        projectManagerFullName = projectManagerFullName,
    )
}
