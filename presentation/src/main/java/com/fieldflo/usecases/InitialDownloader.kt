package com.fieldflo.usecases

import com.fieldflo.common.Atomic
import com.fieldflo.common.networkConnection.IConnectionData
import com.fieldflo.data.api.endpoints.CompanyDataApi
import com.fieldflo.data.api.endpoints.FileDownloader
import com.fieldflo.data.persistence.db.FieldFloDatabase
import com.fieldflo.data.persistence.db.entities.ProjectPermit
import com.fieldflo.data.persistence.fileSystem.FileManager
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import timber.log.Timber
import javax.inject.Inject

class InitialDownloader2 @Inject constructor(
    db: FieldFloDatabase,
    private val localStorage: ILocalStorage,
    private val companiesRepo: ICompaniesRepo,
    private val employeesRepo: IEmployeesRepo,
    private val positionsRepo: IPositionsRepo,
    private val phasesRepo: IPhasesRepo,
    private val inventoryItemsRepo: IInventoryItemsRepo,
    private val companyCertificatesRepo: ICompanyCertificatesRepo,
    private val tasksRepo: ITasksRepo,
    private val subContractorsRepo: ISubContractorsRepo,
    private val haulerRepo: IHaulersRepo,
    private val projectCertificatesRepo: IProjectCertificatesRepo,
    private val projectsRepo: IProjectsRepo,
    private val availableFormsRepo: IAvailableFormsRepo,
    private val fileDownloader: FileDownloader,
    private val fileManager: FileManager,
    private val companyDataApi: CompanyDataApi,
    private val networkConnection: IConnectionData
) {
    private val projectDao = db.getProjectDao2()
    private val projectPermitDao = db.getProjectPermitDao2()
    private val availableFormDao = db.getAvailableFormDao2()

    private val progress = Atomic<DownloadProgress>(DownloadProgress.NotStarted(false))

    private val progressUpdateFlow =
        MutableStateFlow<DownloadProgress>(DownloadProgress.NotStarted(false))

    fun getProgressUpdatesFlow(): Flow<DownloadProgress> {
        return progressUpdateFlow
    }

    suspend fun startInitialDownload() {
        setProgress(DownloadProgress.Started(true))
        val companyId = localStorage.getCurrentCompanyId()
        val securityKey = localStorage.getSecurityKey()
        val apiToken = localStorage.getApiToken()
        val didV1Download =
            companiesRepo.didInitialCompanyLoad(securityKey = securityKey, token = apiToken)
        val didV2Download = localStorage.isV2MigrationComplete()

        if (!didV1Download) downloadGeneral(companyId)
        if (!didV2Download) downloadGeneralV2(companyId)
        setProgress(DownloadProgress.DownloadedGeneral(true))

        if (!didV1Download) downloadProjects(companyId)
        if (didV1Download && !didV2Download) replaceAvailableForms(companyId)
        setProgress(DownloadProgress.DownloadedProjects(true))

        companiesRepo.setInitialCompanyLoadComplete(companyId)
        localStorage.setV2MigrationComplete(true)
        setProgress(DownloadProgress.Complete(true))
    }

    suspend fun pauseInitialDownload() {
        val currentProgress = progress.getValue()
        setProgress(currentProgress.makeCopy(isDownloading = false))
    }

    suspend fun resumeInitialDownload() {
        val currentProgress = progress.getValue()
        setProgress(currentProgress.makeCopy(isDownloading = true))
    }

    private suspend fun setProgress(newProgress: DownloadProgress) {
        progress.setValue(newProgress)
        progressUpdateFlow.emit(newProgress)
    }

    private suspend fun downloadGeneral(companyId: Int) {
        retry { employeesRepo.loadEmployees(companyId) }
        retry { positionsRepo.loadPositions(companyId) }
        retry { phasesRepo.loadPhases(companyId) }
        retry { inventoryItemsRepo.loadInventoryItems(companyId) }
        retry { companyCertificatesRepo.loadCompanyCertificates(companyId) }
        retry { tasksRepo.loadTasks(companyId) }

    }

    private suspend fun downloadGeneralV2(companyId: Int) {
        retry { subContractorsRepo.loadSubContractors(companyId) }
        retry { haulerRepo.loadHaulers(companyId) }
    }

    private suspend fun downloadProjects(companyId: Int) {
        val allPermits = mutableListOf<ProjectPermit>()

        retry {
            val projectsRest = companyDataApi.getAllProjects().sortedBy { it.projectId }
            val projects = projectsRest.map { it.toProject(companyId) }
            projectDao.addAllProjects(projects)

            projectsRest.forEach { projectRest ->
                val permits =
                    projectRest.permits?.map { it.toPermit(companyId, projectRest.projectId!!) }
                        ?: emptyList()
                projectPermitDao.addProjectPermitList(permits)
                allPermits.addAll(permits.filter { it.permitImageUrl.isNotEmpty() })

                val availableForms =
                    projectRest.availableForms?.filter { SupportedForm.supportedFormIds.contains(it.formTypeId) }
                        ?.map { it.toAvailableForm(companyId, projectRest.projectId!!) }
                        ?: emptyList()
                availableFormDao.addAvailableForms(availableForms)
            }
        }

        allPermits.forEach { permit ->
            retry(retries = 3) {
                val dest = fileManager.getProjectPermitFile(
                    companyId,
                    permit.projectId,
                    permit.documentFile
                )
                val success = fileDownloader.downloadFile(permit.permitImageUrl, dest)
                if (!success) {
                    throw RuntimeException("Failed downloading project permit ${permit.permitImageUrl}")
                }
            }
        }

        retry { projectCertificatesRepo.loadProjectCertificates(companyId) }
    }

    private suspend fun replaceAvailableForms(companyId: Int) {
        val allProjects = projectsRepo.getAllProjects(companyId)
        allProjects.forEach { projectData ->
            retry {
                availableFormsRepo.replaceAvailableForms(projectData.projectId, companyId)
            }
        }
    }

    private suspend fun retry(retries: Int = -1, block: suspend () -> Unit) {
        var running = true
        var retriesRemaining = retries
        while (running) {
            // Only bother with code block if resumed
            // Otherwise just delay and then check again
            if (progress.getValue().isDownloading) {
                // Only download if have wifi connection
                if (networkConnection.isWifiConnected) {
                    if (retriesRemaining != 0) {
                        Timber.d(
                            "Attempting lambda block, retries remaining = %s",
                            if (retries == -1) "UNLIMITED" else retriesRemaining.toString()
                        )
                        try {
                            retriesRemaining -= 1
                            // Attempt code block
                            block()
                            // we were successful, break out of this function
                            running = false
                        } catch (e: Exception) {
                            // swallow the exception
                            Timber.e(e)
                        }
                    } else {
                        Timber.d("No more retries. Time to move on")
                        running = false
                    }
                }
            }
            delay(500)
        }
    }
}

sealed class DownloadProgress : IDownloadStatus {
    data class NotStarted(override val isDownloading: Boolean) : DownloadProgress()
    data class Started(override val isDownloading: Boolean) : DownloadProgress()
    data class DownloadedGeneral(override val isDownloading: Boolean) : DownloadProgress()
    data class DownloadedProjects(override val isDownloading: Boolean) : DownloadProgress()
    data class Complete(override val isDownloading: Boolean) : DownloadProgress()

    fun makeCopy(isDownloading: Boolean = this.isDownloading): DownloadProgress {
        return when (this) {
            is NotStarted -> this.copy(isDownloading = isDownloading)
            is Started -> this.copy(isDownloading = isDownloading)
            is DownloadedGeneral -> this.copy(isDownloading = isDownloading)
            is DownloadedProjects -> this.copy(isDownloading = isDownloading)
            is Complete -> this.copy(isDownloading = isDownloading)
        }
    }
}

interface IDownloadStatus {
    val isDownloading: Boolean
}
