package com.fieldflo.usecases

import com.fieldflo.data.persistence.db.entities.Task
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.ITasksRepo
import javax.inject.Inject

class TaskUseCases @Inject constructor(
    private val localStorage: ILocalStorage,
    private val tasksRepo: ITasksRepo
) {
    private val companyId: Int
        get() = localStorage.getCurrentCompanyId()

    suspend fun getAllCompanyTasks(): List<Task> {
        return tasksRepo.getAllTasks(companyId)
    }
}
