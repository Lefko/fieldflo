package com.fieldflo.usecases

import com.fieldflo.data.persistence.db.entities.ActiveFormData
import com.fieldflo.data.persistence.db.entities.DailyDemoLogEmployee
import com.fieldflo.data.repos.interfaces.IProjectFormsRepo
import com.fieldflo.data.repos.interfaces.ITimesheetRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DefaultDemoLogEmployeesHelper(
    private val projectFormsRepo: IProjectFormsRepo,
    private val timeSheetRepo: ITimesheetRepo,
    private val companyId: Int
) {
    suspend fun getDefaultDailyDemoLogEmployees(
        projectId: Int,
        internalFormId: Int
    ): List<DailyDemoLogEmployee> {
        // first get all active timesheets on this project
        val timesheets = projectFormsRepo.getActiveProjectForms(projectId, companyId)
            .filter { it.formTypeId == SupportedForm.TIMESHEET_FORM_ID.formTypeId }

        // extract the latest timesheet
        val timeSheetInternalFormId = when {
            timesheets.size > 1 -> latestTimeSheetId(timesheets)
            timesheets.size == 1 -> timesheets[0].internalFormId
            else -> -1
        }

        // get the employees on that timesheet
        val timehseetEmployees =
            timeSheetRepo.getTimeSheetEmployeesByInternalFormId(timeSheetInternalFormId, companyId)

        return timehseetEmployees
            .distinctBy { it.employeeId }
            .map {
                DailyDemoLogEmployee(
                    dailyDemoLogEmployeeId = 0,
                    formDataId = 0,
                    projectId = projectId,
                    internalFormId = internalFormId,
                    companyId = companyId,
                    employeeId = it.employeeId,
                    descriptionOfWork = "",
                    hours = ""
                )
            }
    }

    private suspend fun latestTimeSheetId(timesheets: List<ActiveFormData>): Int {
        return withContext(Dispatchers.Default) {
            var latestFormDateMillis = 0L
            var latestFormDateInternalFormId = 0

            timesheets.forEach {
                if (it.formDate.time > latestFormDateMillis) {
                    latestFormDateMillis = it.formDate.time
                    latestFormDateInternalFormId = it.internalFormId
                }
            }

            latestFormDateInternalFormId
        }
    }
}