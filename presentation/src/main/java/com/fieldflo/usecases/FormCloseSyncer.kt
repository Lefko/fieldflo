package com.fieldflo.usecases

import com.fieldflo.data.persistence.db.entities.ProjectForm
import com.fieldflo.data.repos.interfaces.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import retrofit2.HttpException
import timber.log.Timber

object FormCloseSyncer {

    private val mutex = Mutex()

    suspend fun performSyncClose(
        projectForm: ProjectForm,
        projectFormsRepo: IProjectFormsRepo,
        timeSheetRepo: ITimesheetRepo,
        dailyFieldReportRepo: IDailyFieldReportRepo,
        dailyLogRepo: IDailyLogRepo,
        psiRepo: IPsiRepo,
        dailyDemoLogRepo: IDailyDemoLogRepo
    ) {
        mutex.withLock {
            val isFormSynced = projectFormsRepo
                .isFormSynced(projectForm.internalFormId, projectForm.companyId)

            if (isFormSynced) return

            try {
                when (SupportedForm.fromFormTypeId(projectForm.formTypeId)) {
                    SupportedForm.DAILY_LOG_FORM_ID ->
                        dailyLogRepo.syncFormClose(
                            projectForm.internalFormId,
                            projectForm.closeDate,
                            projectForm.companyId
                        )

                    SupportedForm.FIELD_REPORT_FORM_ID ->
                        dailyFieldReportRepo.syncFormClose(
                            projectForm.internalFormId,
                            projectForm.closeDate,
                            projectForm.companyId
                        )

                    SupportedForm.TIMESHEET_FORM_ID ->
                        timeSheetRepo.syncFormClose(
                            projectForm.internalFormId,
                            projectForm.closeDate,
                            projectForm.companyId
                        )

                    SupportedForm.PSI_FORM_ID ->
                        psiRepo.syncFormClose(
                            projectForm.internalFormId,
                            projectForm.closeDate,
                            projectForm.companyId
                        )

                    SupportedForm.DAILY_DEMO_LOG_ID -> {
                        dailyDemoLogRepo.syncFormClose(
                            projectForm.internalFormId,
                            projectForm.closeDate,
                            projectForm.companyId
                        )
                    }
                }
                projectFormsRepo.setProjectClosedSynced(
                    projectForm.internalFormId,
                    projectForm.companyId
                )
            } catch (e: Exception) {
                Timber.e(e)
                if (e is HttpException && e.code() != 401) {
                    // we successfully communicated with server but got an error response,
                    // since the error wasn't 401 we know that user had authorization... there was
                    // something wrong with this form (or server has a bug)
                    handleSyncError(
                        projectForm,
                        projectFormsRepo,
                        timeSheetRepo,
                        dailyFieldReportRepo,
                        dailyLogRepo,
                        psiRepo,
                        dailyDemoLogRepo
                    )
                } else {
                    throw e
                }
            }
        }
    }

    private suspend fun handleSyncError(
        projectForm: ProjectForm,
        projectFormsRepo: IProjectFormsRepo,
        timeSheetRepo: ITimesheetRepo,
        dailyFieldReportRepo: IDailyFieldReportRepo,
        dailyLogRepo: IDailyLogRepo,
        psiRepo: IPsiRepo,
        dailyDemoLogRepo: IDailyDemoLogRepo
    ) {
        projectFormsRepo.setHasSyncError(projectForm)
        when (val supportedForm = SupportedForm.fromFormTypeId(projectForm.formTypeId)) {
            SupportedForm.DAILY_LOG_FORM_ID -> projectFormsRepo.logSyncError(
                dailyLogRepo.getFormDataForLogging(
                    projectForm.internalFormId,
                    projectForm.closeDate,
                    projectForm.companyId
                ),
                projectForm.formTypeId,
                supportedForm.name
            )

            SupportedForm.FIELD_REPORT_FORM_ID -> projectFormsRepo.logSyncError(
                dailyFieldReportRepo.getFormDataForLogging(
                    projectForm.internalFormId,
                    projectForm.closeDate,
                    projectForm.companyId
                ),
                projectForm.formTypeId,
                supportedForm.name
            )

            SupportedForm.TIMESHEET_FORM_ID -> projectFormsRepo.logSyncError(
                timeSheetRepo.getFormDataForLogging(
                    projectForm.internalFormId,
                    projectForm.closeDate,
                    projectForm.companyId
                ),
                projectForm.formTypeId,
                supportedForm.name
            )

            SupportedForm.PSI_FORM_ID -> projectFormsRepo.logSyncError(
                psiRepo.getFormDataForLogging(
                    projectForm.internalFormId,
                    projectForm.closeDate,
                    projectForm.companyId
                ),
                projectForm.formTypeId,
                supportedForm.name
            )

            SupportedForm.DAILY_DEMO_LOG_ID -> projectFormsRepo.logSyncError(
                dailyDemoLogRepo.getFormDataForLogging(
                    projectForm.internalFormId,
                    projectForm.closeDate,
                    projectForm.companyId
                ),
                projectForm.formTypeId,
                supportedForm.name
            )
        }
    }
}
