package com.fieldflo.usecases

import com.fieldflo.common.networkConnection.IConnectionData
import com.fieldflo.data.persistence.db.entities.*
import com.fieldflo.data.persistence.fileSystem.FileManager
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import timber.log.Timber
import javax.inject.Inject

class ProjectFormsUseCase @Inject constructor(
    employeeRepo: IEmployeesRepo,
    private val localStorage: ILocalStorage,
    private val projectsRepo: IProjectsRepo,
    private val projectFormsRepo: IProjectFormsRepo,
    private val availableFormRepo: IAvailableFormsRepo,
    private val timeSheetRepo: ITimesheetRepo,
    private val dailyFieldReportRepo: IDailyFieldReportRepo,
    private val dailyLogRepo: IDailyLogRepo,
    private val psiRepo: IPsiRepo,
    private val fileManager: FileManager,
    private val connectionData: IConnectionData,
    private val dailyDemoLogRepo: IDailyDemoLogRepo
) {

    private val companyId: Int get() = localStorage.getCurrentCompanyId()

    private val defaultPsiEmployeesHelper = DefaultPsiEmployeesHelper(
        employeeRepo,
        projectFormsRepo,
        timeSheetRepo,
        fileManager,
        companyId
    )

    fun getActiveFormsFlow(projectId: Int): Flow<List<ActiveForm>> {
        return projectFormsRepo.getActiveProjectFormsFlow(projectId, companyId)
            .map {
                it.map {
                    ActiveForm(
                        it,
                        it.isClosable(companyId),
                        it.hasPrevious(companyId)
                    )
                }
            }
    }

    fun getClosedFormsFlow(projectId: Int): Flow<List<ClosedFormData>> {
        return projectFormsRepo.getClosedProjectForms(projectId, companyId)
    }

    fun getDeletedFormsFlow(projectId: Int): Flow<List<DeletedFormData>> {
        return projectFormsRepo.getDeletedProjectFormsFlow(projectId, companyId)
    }

    suspend fun endDay(projectId: Int) {
        projectFormsRepo.getActiveProjectForms(projectId, companyId)
            .filter { it.isClosable(companyId) }
            .forEach { closeForm(it.internalFormId) }
    }

    suspend fun closeForm(internalFormId: Int) {
        projectFormsRepo.closeProjectForm(internalFormId, companyId)
        if (connectionData.isInternetConnected) {
            projectFormsRepo.getProjectForm(internalFormId, companyId)?.let { projectForm ->
                FormCloseSyncer.performSyncClose(
                    projectForm,
                    projectFormsRepo,
                    timeSheetRepo,
                    dailyFieldReportRepo,
                    dailyLogRepo,
                    psiRepo,
                    dailyDemoLogRepo
                )
            }
        }
    }

    suspend fun reopenForm(internalFormId: Int) {
        projectFormsRepo.reopenForm(internalFormId, companyId)
    }

    suspend fun startDay(projectId: Int) {
        if (localStorage.canUserCanAddEditOpenForms()) {
            val hasActiveTimeSheet = projectHasActiveTimeSheet(projectId)

            val formsToAdd = availableFormRepo.getAvailableForms(projectId, companyId)
                .filter { it.mandatory }
                .filter { it.formTypeId != SupportedForm.TIMESHEET_FORM_ID.formTypeId || !hasActiveTimeSheet }
                .map { it.toProjectForm() }

            projectFormsRepo.addFormsToProject(formsToAdd, companyId)
        }
    }

    suspend fun addFormsToProject(projectId: Int, formTypesToAdd: List<Int>) {
        if (localStorage.canUserCanAddEditOpenForms()) {
            val hasActiveTimeSheet = projectHasActiveTimeSheet(projectId)
            val availableForms = availableFormRepo.getAvailableForms(projectId, companyId)
            Timber.d("available forms: $availableForms")
            val formsToAdd = availableForms
                .filter { it.formTypeId in formTypesToAdd }
                .filter { !hasActiveTimeSheet || it.formTypeId != SupportedForm.TIMESHEET_FORM_ID.formTypeId }
                .map { it.toProjectForm() }

            projectFormsRepo.addFormsToProject(formsToAdd, companyId)
        }
    }

    suspend fun deleteForm(internalFormId: Int) {
        val formToDelete = projectFormsRepo.getProjectForm(internalFormId, companyId)
        if (formToDelete != null) {
            projectFormsRepo.deleteProjectForm(internalFormId, companyId)
            deleteFormData(formToDelete)
        }
    }

    suspend fun sameAsPrevious(
        projectId: Int,
        formTypeId: Int,
        internalFormId: Int,
        perDiemNote: String
    ): Boolean {
        val previousFormInternalFormId = projectFormsRepo
            .getPreviousFormId(projectId, formTypeId, internalFormId, companyId)
        return if (previousFormInternalFormId != null) {
            when (SupportedForm.fromFormTypeId(formTypeId)) {
                SupportedForm.TIMESHEET_FORM_ID -> {
                    val autoApplyPerDiem =
                        projectsRepo.isProjectAutoApplyPerDiem(projectId, companyId)
                    timeSheetRepo.sameAsPrevious(
                        previousFormInternalFormId,
                        internalFormId,
                        autoApplyPerDiem,
                        perDiemNote,
                        companyId
                    )
                    true
                }
                SupportedForm.PSI_FORM_ID -> {
                    val employees =
                        defaultPsiEmployeesHelper.getDefaultPsiEmployees(internalFormId, projectId)
                    psiRepo.sameAsPrevious(
                        employees,
                        previousFormInternalFormId,
                        internalFormId,
                        companyId
                    )
                    true
                }
                SupportedForm.DAILY_DEMO_LOG_ID -> {
                    dailyDemoLogRepo.sameAsPrevious(
                        previousFormInternalFormId,
                        internalFormId,
                        companyId
                    )
                    true
                }

                else -> false
            }
        } else {
            false
        }
    }

    suspend fun getAvailableForms(projectId: Int): List<AvailableForm> {
        val hasActiveTimeSheet = projectHasActiveTimeSheet(projectId)
        return availableFormRepo.getAvailableForms(projectId, companyId)
            .filter { !hasActiveTimeSheet || it.formTypeId != SupportedForm.TIMESHEET_FORM_ID.formTypeId }
            .sortedBy { it.sort }
    }

    private suspend fun projectHasActiveTimeSheet(projectId: Int): Boolean {
        return projectFormsRepo.getActiveProjectForms(projectId, companyId)
            .any { it.formTypeId == SupportedForm.TIMESHEET_FORM_ID.formTypeId }
    }

    private suspend fun ActiveFormData.isClosable(companyId: Int): Boolean {
        Timber.d("Determining if $this is closable")
        return when (SupportedForm.fromFormTypeId(this.formTypeId)) {
            SupportedForm.FIELD_REPORT_FORM_ID ->
                dailyFieldReportRepo.isFormClosable(internalFormId, companyId)

            SupportedForm.TIMESHEET_FORM_ID ->
                timeSheetRepo.isFormClosable(internalFormId, companyId)

            SupportedForm.DAILY_LOG_FORM_ID ->
                dailyLogRepo.isFormClosable(internalFormId, companyId)

            SupportedForm.PSI_FORM_ID -> psiRepo.isFormClosable(internalFormId, companyId)

            SupportedForm.DAILY_DEMO_LOG_ID -> dailyDemoLogRepo.isFormClosable(
                internalFormId,
                companyId
            )
        }
    }

    private suspend fun ActiveFormData.hasPrevious(companyId: Int): Boolean {
        return projectFormsRepo.formHasPrevious(
            this.projectId,
            this.formTypeId,
            this.internalFormId,
            companyId
        )
    }

    private suspend fun deleteFormData(formToDelete: ProjectForm) {
        when (SupportedForm.fromFormTypeId(formToDelete.formTypeId)) {
            SupportedForm.FIELD_REPORT_FORM_ID -> {
                dailyFieldReportRepo.deleteForm(formToDelete.internalFormId, formToDelete.companyId)
                fileManager.getDailyFieldReportSavedDir(
                    formToDelete.companyId,
                    formToDelete.projectId,
                    formToDelete.internalFormId
                ).deleteRecursively()
            }
            SupportedForm.TIMESHEET_FORM_ID -> {
                timeSheetRepo.deleteForm(formToDelete.internalFormId, formToDelete.companyId)

                fileManager.getPinOutDirForForm(
                    companyId = formToDelete.companyId,
                    formDate = formToDelete.formDate,
                    projectId = formToDelete.projectId,
                    internalFormId = formToDelete.internalFormId
                ).deleteRecursively()
            }

            SupportedForm.DAILY_LOG_FORM_ID ->
                dailyLogRepo.deleteForm(formToDelete.internalFormId, formToDelete.companyId)

            SupportedForm.PSI_FORM_ID -> {
                psiRepo.deleteForm(formToDelete.internalFormId, formToDelete.companyId)
                fileManager.getPinOutDirForForm(
                    companyId = formToDelete.companyId,
                    formDate = formToDelete.formDate,
                    projectId = formToDelete.projectId,
                    internalFormId = formToDelete.internalFormId
                ).deleteRecursively()
            }

            SupportedForm.DAILY_DEMO_LOG_ID -> {
                dailyDemoLogRepo.deleteForm(formToDelete.internalFormId, formToDelete.companyId)
                fileManager.getDailyDemoFormSavedImagesDir(
                    companyId = formToDelete.companyId,
                    projectId = formToDelete.projectId,
                    internalFormId = formToDelete.internalFormId
                ).deleteRecursively()
            }
        }
    }
}

private fun AvailableForm.toProjectForm() = ProjectForm(
    companyId = this.companyId,
    projectId = this.projectId,
    formTypeId = this.formTypeId,
    formName = this.formName
)

data class ActiveForm(
    val activeFormData: ActiveFormData,
    val isClosable: Boolean,
    val formHasPreviousData: Boolean
)

enum class SupportedForm(val formTypeId: Int) {
    DAILY_LOG_FORM_ID(5),
    FIELD_REPORT_FORM_ID(13),
    TIMESHEET_FORM_ID(30),
    DAILY_DEMO_LOG_ID(35),
    PSI_FORM_ID(46);

    companion object {
        fun fromFormTypeId(formTypeId: Int): SupportedForm {
            return when (formTypeId) {
                DAILY_LOG_FORM_ID.formTypeId -> DAILY_LOG_FORM_ID
                FIELD_REPORT_FORM_ID.formTypeId -> FIELD_REPORT_FORM_ID
                TIMESHEET_FORM_ID.formTypeId -> TIMESHEET_FORM_ID
                DAILY_DEMO_LOG_ID.formTypeId -> DAILY_DEMO_LOG_ID
                PSI_FORM_ID.formTypeId -> PSI_FORM_ID
                else -> throw IllegalArgumentException("Dont know how to handle form type id: $formTypeId")
            }
        }

        val supportedFormIds = SupportedForm.values().map { it.formTypeId }.toSet()
    }
}

