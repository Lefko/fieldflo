package com.fieldflo.usecases

import com.fieldflo.data.persistence.db.entities.CompanyData
import com.fieldflo.data.persistence.fileSystem.FileManager
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.*
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class CompanyUseCases @Inject constructor(
    private val localStorage: ILocalStorage,
    private val authRepo: IAuthRepo,
    private val companiesRepo: ICompaniesRepo,
    private val availableFormsRepo: IAvailableFormsRepo,
    private val companyCertsRepo: ICompanyCertificatesRepo,
    private val dailyFieldReportRepo: IDailyFieldReportRepo,
    private val dailyLogRepo: IDailyLogRepo,
    private val employeesRepo: IEmployeesRepo,
    private val inventoryItemsRepo: IInventoryItemsRepo,
    private val phasesRepo: IPhasesRepo,
    private val positionsRepo: IPositionsRepo,
    private val projectCertificatesRepo: IProjectCertificatesRepo,
    private val projectFormsRepo: IProjectFormsRepo,
    private val projectsRepo: IProjectsRepo,
    private val psiRepo: IPsiRepo,
    private val tasksRepo: ITasksRepo,
    private val timeSheetRepo: ITimesheetRepo,
    private val fileManager: FileManager
) {
    fun getCompaniesListFlow(): Flow<List<CompanyData>> = companiesRepo.getCompaniesFlow()

    suspend fun didInitialDownload(securityKey: String, token: String): Boolean {
        return companiesRepo.didInitialCompanyLoad(
            securityKey = securityKey,
            token = token
        ) && localStorage.isV2MigrationComplete()
    }

    suspend fun addCompany(addCompanyParams: AddCompanyParams) {
        companiesRepo.addCompany(
            companyName = addCompanyParams.companyName,
            baseUrl = addCompanyParams.baseUrl.substringBeforeLast("/"),
            token = addCompanyParams.token,
            securityKey = addCompanyParams.securityKey
        )
    }

    suspend fun deleteCompanyData(securityKey: String, token: String) {
        authRepo.logout()
        companiesRepo.getCompanyBySecurityKeyAndToken(securityKey, token)?.let { company ->
            with(company) {
                availableFormsRepo.deleteAvailableForms(companyId)
                companyCertsRepo.deleteCompanyCertificates(companyId)
                dailyFieldReportRepo.deleteDailyFieldReportData(companyId)
                dailyLogRepo.deleteDailyLogData(companyId)
                employeesRepo.deleteEmployees(companyId)
                inventoryItemsRepo.deleteInventoryItems(companyId)
                phasesRepo.deletePhases(companyId)
                positionsRepo.deletePositions(companyId)
                projectCertificatesRepo.deleteProjectCertificates(companyId)
                projectFormsRepo.deleteProjectForms(companyId)
                projectsRepo.deleteProjects(companyId)
                psiRepo.deleteForms(companyId)
                tasksRepo.deleteTasks(companyId)
                timeSheetRepo.deleteTimeSheetData(companyId)
                fileManager.deleteCompanyFiles(companyId)
            }
        }
    }

    suspend fun deleteCompany(securityKey: String, token: String) {
        authRepo.logout()
        companiesRepo.getCompanyBySecurityKeyAndToken(securityKey, token)?.let { company ->
            companiesRepo.deleteCompany(securityKey, token)
            with(company) {
                availableFormsRepo.deleteAvailableForms(companyId)
                companyCertsRepo.deleteCompanyCertificates(companyId)
                dailyFieldReportRepo.deleteDailyFieldReportData(companyId)
                dailyLogRepo.deleteDailyLogData(companyId)
                employeesRepo.deleteEmployees(companyId)
                inventoryItemsRepo.deleteInventoryItems(companyId)
                phasesRepo.deletePhases(companyId)
                positionsRepo.deletePositions(companyId)
                projectCertificatesRepo.deleteProjectCertificates(companyId)
                projectFormsRepo.deleteProjectForms(companyId)
                projectsRepo.deleteProjects(companyId)
                psiRepo.deleteForms(companyId)
                tasksRepo.deleteTasks(companyId)
                timeSheetRepo.deleteTimeSheetData(companyId)
                fileManager.deleteCompanyFiles(companyId)
            }
        }
    }

    fun getCurrentCompanyName() = localStorage.getCurrentCompanyName()
}

data class AddCompanyParams(
    val companyName: String,
    val baseUrl: String,
    val token: String,
    val securityKey: String
)
