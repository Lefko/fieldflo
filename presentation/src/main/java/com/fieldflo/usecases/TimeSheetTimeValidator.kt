package com.fieldflo.usecases

import com.fieldflo.data.persistence.db.entities.TimeSheetEmployee
import com.fieldflo.data.persistence.db.entities.TimesheetEmployeeStatus
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit

object TimeSheetTimeValidator {

    fun validateNewTimes(
        timeSheetEmployee: TimeSheetEmployee,
        newStartDate: Date?,
        newEndDate: Date?,
        breakDurationSeconds: Int?,
        timeSheetEmployees: List<TimeSheetEmployee>
    ): Boolean {
        // not allowed to edit end time if startTime not set
        if (timeSheetEmployee.employeeStatus.ordinal < TimesheetEmployeeStatus.CLOCKED_IN.ordinal
            && newStartDate == null
        ) {
            Timber.d("TimeEdit - Cant edit end time if startTime not set")
            return false
        }

        // didn't enter anything
        if (newStartDate == null && newEndDate == null && breakDurationSeconds == null) {
            Timber.d("TimeEdit - Didnt enter any Dates")
            return false
        }

        val actualStart: Long = newStartDate?.time ?: timeSheetEmployee.clockIn.time
        val actualEnd: Long = newEndDate?.time ?: timeSheetEmployee.clockOut.time
        val actualBreak = breakDurationSeconds ?: timeSheetEmployee.breakTime
        val breakDurationMillis = actualBreak * 1000

        return if (actualStart > 0 && actualEnd > 0) {
            validateStartAndEnd(
                actualStart,
                actualEnd,
                breakDurationMillis,
                timeSheetEmployee,
                timeSheetEmployees
            )
        } else {
            validateStart(actualStart, timeSheetEmployee, timeSheetEmployees)
        }
    }

    private fun validateStartAndEnd(
        actualStart: Long,
        actualEnd: Long,
        breakDurationMillis: Int,
        timeSheetEmployee: TimeSheetEmployee,
        timeSheetEmployees: List<TimeSheetEmployee>
    ): Boolean {
        // validate start AND end
        Timber.d("TimeEdit - validate start AND end")

        // start must be BEFORE end!
        if (actualStart >= actualEnd) {
            Timber.d("TimeEdit - start must be BEFORE end!")
            return false
        }

        // not enough working time
        if (actualEnd - actualStart - breakDurationMillis <= 0) {
            Timber.d("TimeEdit - not enough working time")
            return false
        }

        // cant work more than one day
        if (actualEnd - actualStart > TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS)) {
            Timber.d("TimeEdit - cant work more than one day")
            return false
        }

        timeSheetEmployees.forEach {
            if (it.timeSheetEmployeeId != timeSheetEmployee.timeSheetEmployeeId) {
                val otherStartMillis = it.clockIn.time
                val otherEndMillis = it.clockOut.time
                if (otherStartMillis > 0 && otherEndMillis > 0) {
                    if (overlap(actualStart, actualEnd, otherStartMillis, otherEndMillis)) {
                        Timber.d("TimeEdit - edit time start/stop overlaps another start/stop")
                        return false
                    }
                } else {
                    if (between(otherStartMillis, actualStart, actualEnd)) {
                        Timber.d("TimeEdit - edit time start/stop overlaps another start")
                        return false
                    }
                }
            }
        }
        return true
    }

    private fun validateStart(
        actualStart: Long,
        timeSheetEmployee: TimeSheetEmployee,
        timeSheetEmployees: List<TimeSheetEmployee>
    ): Boolean {
        Timber.d("TimeEdit - validate start")
        timeSheetEmployees.forEach {
            if (it.timeSheetEmployeeId != timeSheetEmployee.timeSheetEmployeeId) {
                val otherStartMillis = it.clockIn.time
                val otherEndMillis = it.clockOut.time
                if (otherEndMillis > 0 && otherStartMillis > 0) {
                    if (between(actualStart, otherStartMillis, otherEndMillis)) {
                        Timber.d("TimeEdit - edit time start is between another start/stop")
                        return false
                    }
                }
            }
        }
        return true
    }

    private fun overlap(start1: Long, end1: Long, start2: Long, end2: Long): Boolean {
        return start1 <= end2 && start2 <= end1
    }

    private fun between(toCheck: Long, start: Long, end: Long): Boolean {
        return toCheck in start..end
    }
}