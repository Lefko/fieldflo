package com.fieldflo.usecases

import com.fieldflo.data.persistence.db.entities.DailyFieldReportFormData
import com.fieldflo.data.persistence.fileSystem.FileManager
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.IDailyFieldReportRepo
import com.fieldflo.data.repos.interfaces.IProjectFormsRepo
import com.fieldflo.data.repos.interfaces.IProjectsRepo
import java.io.File
import java.util.*
import javax.inject.Inject

class DailyFieldReportUseCases @Inject constructor(
    private val localStorage: ILocalStorage,
    private val projectsRepo: IProjectsRepo,
    private val projectFormsRepo: IProjectFormsRepo,
    private val dailyFieldReportRepo: IDailyFieldReportRepo,
    private val fileManager: FileManager
) {

    private val companyId: Int
        get() = localStorage.getCurrentCompanyId()

    suspend fun getFieldReportData(internalFormId: Int, projectId: Int): DailyFieldReportFormData {
        val savedDailyFieldReportData =
            dailyFieldReportRepo.getDailyFieldReport(internalFormId, companyId)
        return if (savedDailyFieldReportData == null) {
            val project = projectsRepo.getProjectById(projectId, companyId)
            DailyFieldReportFormData.createEmptyFormData(
                companyId = companyId,
                projectId = projectId,
                internalFormId = internalFormId,
                supervisorFullName = project.supervisorFullName,
                supervisorId = project.supervisorId,
                projectManagerFullName = project.projectManagerFullName,
                projectManagerId = project.projectManagerId
            )
        } else {
            savedDailyFieldReportData
        }
    }

    suspend fun saveDailyForm(
        projectId: Int,
        internalFormId: Int,
        dailyFieldReportFormData: DailyFieldReportSaveData
    ) {
        dailyFieldReportRepo.saveDailyForm(
            dailyFieldReportFormData.toDailyFieldReportFormData(
                companyId,
                projectId,
                internalFormId
            )
        )
        projectFormsRepo.triggerFormUpdate(internalFormId, companyId)
    }

    fun dailyFieldReportPics(
        internalFormId: Int,
        projectId: Int
    ): List<File> {
        return dailyFieldReportRepo.dailyFieldReportPicsDirFiles(
            internalFormId,
            projectId,
            companyId
        )
    }

    fun saveDailyFieldReportPic(
        projectId: Int,
        internalFormId: Int,
        sourcePath: String,
        picNumber: Int
    ) {
        val pic = File(sourcePath)
        val dest =
            fileManager.getDailyFieldReportPic(companyId, projectId, internalFormId, picNumber)
        pic.copyTo(dest, overwrite = true)
    }

    fun deleteSavedDailyFieldReportPic(
        projectId: Int,
        internalFormId: Int,
        imageNumber: Int
    ) {
        val savedPic =
            fileManager.getDailyFieldReportPic(companyId, projectId, internalFormId, imageNumber)
        savedPic.delete()
    }

    data class DailyFieldReportSaveData(
        val internalFormId: Int,
        val formDate: Date,
        val scopeReceived: String,
        val scopeDate: Date,
        val scopeStructure: String,
        val workCompletedToday: String,

        val supervisorId: Int,
        val supervisorFullName: String,
        val projectManagerId: Int,
        val projectManagerFullName: String,

        val goalsForTomorrow: String,
        val goalsForTheWeek: String,
        val actionRequired: String,
        val projectNotesOrSpecialConsiderations: String,
        val meetingLog: String,
        val visitors: String,
        val workflow: String,
        val bulkEquipmentInUseThisDay: String,
        val acmDetected: String,
        val acmBuildDate: Date,
        val acmSurveyDate: Date,
        val leadDetected: String,
        val leadTestDate: Date,
        val moldDetected: String,
        val moldTestDate: Date,
        val notesRelatedToEnvironmentalTesting: String,
        val estimatedCompletionDate: Date,
        val daysRemainingOnProject: String,
        val activitiesDoneTodayInstallCriticals: Boolean,
        val activitiesDoneTodayRemoval: Boolean,
        val activitiesDoneTodayEstablishNegativePressure: Boolean,
        val activitiesDoneTodayBagOut: Boolean,
        val activitiesDoneTodayConstructDecon: Boolean,
        val activitiesDoneTodayFinalCleanOrDecontamination: Boolean,
        val activitiesDoneTodayPreClean: Boolean,
        val activitiesDoneTodayClearance: Boolean,
        val activitiesDoneTodayCoverFixedObjects: Boolean,
        val activitiesDoneTodayTearDown: Boolean,
        val activitiesDoneTodayConstructContainment: Boolean,
        val activitiesDoneTodayDemob: Boolean,
        val cleaningVerificationExterior: Boolean,
        val cleaningVerificationInterior: Boolean,
        val workAreaRoomLocations1: String,
        val workAreaRoomLocations2: String,
        val workAreaRoomLocations3: String,
        val numberOfCleaningClothsUtilized1: Int,
        val numberOfCleaningClothsUtilized2: Int,
        val numberOfCleaningClothsUtilized3: Int,
        val whatCleaningVerificationDidYouPassOn1: String,
        val whatCleaningVerificationDidYouPassOn2: String,
        val whatCleaningVerificationDidYouPassOn3: String,
        val whatWentRight: String,
        val whatCanBeImproved: String,
        val reportableIncident: String,
        val incident: String,
        val otherIncident: String,
        val stopWorkPerformed: String,
        val swpNote: String
    )
}

private fun DailyFieldReportUseCases.DailyFieldReportSaveData.toDailyFieldReportFormData(
    companyId: Int,
    projectId: Int,
    internalFormId: Int
) = DailyFieldReportFormData(
    companyId = companyId,
    projectId = projectId,
    internalFormId = internalFormId,
    formDate = this.formDate,
    scopeReceived = this.scopeReceived,
    scopeDate = this.scopeDate,
    scopeStructure = this.scopeStructure,
    workCompletedToday = this.workCompletedToday,
    supervisorId = this.supervisorId,
    supervisorFullName = this.supervisorFullName,
    projectManagerId = this.projectManagerId,
    projectManagerFullName = this.projectManagerFullName,
    goalsForTomorrow = this.goalsForTomorrow,
    goalsForTheWeek = this.goalsForTheWeek,
    actionRequired = this.actionRequired,
    projectNotesOrSpecialConsiderations = this.projectNotesOrSpecialConsiderations,
    meetingLog = this.meetingLog,
    visitors = this.visitors,
    workflow = this.workflow,
    bulkEquipmentInUseThisDay = this.bulkEquipmentInUseThisDay,
    acmDetected = this.acmDetected,
    acmBuildDate = this.acmBuildDate,
    acmSurveyDate = this.acmSurveyDate,
    leadDetected = this.leadDetected,
    leadTestDate = this.leadTestDate,
    moldDetected = this.moldDetected,
    moldTestDate = this.moldTestDate,
    notesRelatedToEnvironmentalTesting = this.notesRelatedToEnvironmentalTesting,
    estimatedCompletionDate = this.estimatedCompletionDate,
    daysRemainingOnProject = this.daysRemainingOnProject,
    activitiesDoneTodayInstallCriticals = this.activitiesDoneTodayInstallCriticals,
    activitiesDoneTodayRemoval = this.activitiesDoneTodayRemoval,
    activitiesDoneTodayEstablishNegativePressure = this.activitiesDoneTodayEstablishNegativePressure,
    activitiesDoneTodayBagOut = this.activitiesDoneTodayBagOut,
    activitiesDoneTodayConstructDecon = this.activitiesDoneTodayConstructDecon,
    activitiesDoneTodayFinalCleanOrDecontamination = this.activitiesDoneTodayFinalCleanOrDecontamination,
    activitiesDoneTodayPreClean = this.activitiesDoneTodayPreClean,
    activitiesDoneTodayClearance = this.activitiesDoneTodayClearance,
    activitiesDoneTodayCoverFixedObjects = this.activitiesDoneTodayCoverFixedObjects,
    activitiesDoneTodayTearDown = this.activitiesDoneTodayTearDown,
    activitiesDoneTodayConstructContainment = this.activitiesDoneTodayConstructContainment,
    activitiesDoneTodayDemob = this.activitiesDoneTodayDemob,
    cleaningVerificationExterior = this.cleaningVerificationExterior,
    cleaningVerificationInterior = this.cleaningVerificationInterior,
    workAreaRoomLocations1 = this.workAreaRoomLocations1,
    workAreaRoomLocations2 = this.workAreaRoomLocations2,
    workAreaRoomLocations3 = this.workAreaRoomLocations3,
    numberOfCleaningClothsUtilized1 = this.numberOfCleaningClothsUtilized1,
    numberOfCleaningClothsUtilized2 = this.numberOfCleaningClothsUtilized2,
    numberOfCleaningClothsUtilized3 = this.numberOfCleaningClothsUtilized3,
    whatCleaningVerificationDidYouPassOn1 = this.whatCleaningVerificationDidYouPassOn1,
    whatCleaningVerificationDidYouPassOn2 = this.whatCleaningVerificationDidYouPassOn2,
    whatCleaningVerificationDidYouPassOn3 = this.whatCleaningVerificationDidYouPassOn3,
    whatWentRight = this.whatWentRight,
    whatCanBeImproved = this.whatCanBeImproved,
    reportableIncident = this.reportableIncident,
    incident = this.incident,
    otherIncident = this.otherIncident,
    stopWorkPerformed = this.stopWorkPerformed,
    swpNote = this.swpNote,
    image1Url = "",
    caption1 = "",
    image2Url = "",
    caption2 = "",
    image3Url = "",
    caption3 = "",
    image4Url = "",
    caption4 = "",
    image5Url = "",
    caption5 = "",
    image6Url = "",
    caption6 = ""
)