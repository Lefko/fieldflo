package com.fieldflo.usecases

import com.fieldflo.data.api.endpoints.FileDownloader
import com.fieldflo.data.api.entities.ModificationRestObject
import com.fieldflo.data.persistence.db.entities.ProjectForm
import com.fieldflo.data.persistence.fileSystem.FileManager
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.*
import timber.log.Timber
import javax.inject.Inject

class ServerUpdatesUseCases @Inject constructor(
    private val serverUpdatesRepo: IServerUpdatesRepo,
    private val localStorage: ILocalStorage,
    private val fileManager: FileManager,
    private val fileDownloader: FileDownloader,
    private val taskRepo: ITasksRepo,
    private val positionRepo: IPositionsRepo,
    private val phaseRepo: IPhasesRepo,
    private val employeesRepo: IEmployeesRepo,
    private val inventoryItemsRepo: IInventoryItemsRepo,
    private val companyCertificatesRepo: ICompanyCertificatesRepo,
    private val projectRepo: IProjectsRepo,
    private val availableFormRepo: IAvailableFormsRepo,
    private val projectCertificatesRepo: IProjectCertificatesRepo,
    private val projectFormsRepo: IProjectFormsRepo,
    private val companiesRepo: ICompaniesRepo,
    private val timeSheetRepo: ITimesheetRepo,
    private val dailyFieldReportRepo: IDailyFieldReportRepo,
    private val dailyLogRepo: IDailyLogRepo,
    private val psiRepo: IPsiRepo,
    private val dailyDemoLogRepo: IDailyDemoLogRepo,
    private val haulersRepo: IHaulersRepo,
    private val subcontractorsRepo: ISubContractorsRepo
) {

    private val companyId: Int get() = localStorage.getCurrentCompanyId()

    fun saveNewFcmToken(fcmToken: String) {
        if (localStorage.getFcmToken() != fcmToken) {
            localStorage.setFcmToken(fcmToken)
            if (localStorage.getUserToken().isNotEmpty()) {
                val deviceId = localStorage.getDeviceId()
                serverUpdatesRepo.onNewFcmToken(fcmToken, deviceId)
            }
        }
    }

    suspend fun performServerSync() {
        val securityKey = localStorage.getSecurityKey()
        val token = localStorage.getApiToken()
        if (securityKey.isEmpty() || token.isEmpty()) return

        val initialDownloadComplete = companiesRepo.didInitialCompanyLoad(securityKey, token)

        if (companyId != 0 && localStorage.getUserToken().isNotEmpty() && initialDownloadComplete) {
            try {
                if (projectFormsRepo.hasUnclosedForms(companyId)) {
                    val formsToCloseOnServer = projectFormsRepo.getUnsyncedClosedForms(companyId)
                    formsToCloseOnServer.forEach { projectForm ->
                        FormCloseSyncer.performSyncClose(
                            projectForm,
                            projectFormsRepo,
                            timeSheetRepo,
                            dailyFieldReportRepo,
                            dailyLogRepo,
                            psiRepo,
                            dailyDemoLogRepo
                        )
                    }
                }

                val deviceId = localStorage.getDeviceId()

                serverUpdatesRepo.getModifications(deviceId).sorted()
                    .forEach { handleModification(it) }
                serverUpdatesRepo.setUpdated(deviceId)
            } catch (e: Exception) {
                Timber.e(e, "Error performing server sync")
            }
        }
    }

    fun startAppToServerWorker() = serverUpdatesRepo.startServerSyncWorker()

    private suspend fun handleModification(modification: ModificationRestObject) {
        Timber.d("Handling modification: $modification")
        when (modification.node) {
            ModificationRestObject.PSI_TASK_CHANGE -> taskRepo.replaceAllTasks(companyId)

            ModificationRestObject.POSITIONS_CHANGE -> {
                if (modification.replaceAll) {
                    positionRepo.replaceAllPositions(companyId)
                } else {
                    modification.createdIds.forEach { positionRepo.loadNewPosition(it, companyId) }
                    modification.updatedIds.forEach { positionRepo.loadNewPosition(it, companyId) }
                    modification.deletedIds.forEach { positionRepo.deletePosition(it, companyId) }
                }
            }

            ModificationRestObject.CLIENT_CHANGE -> {
                val relevantIds = (modification.deletedIds + modification.updatedIds)
                relevantIds.forEach { createUpdateProject(it, companyId) }
            }

            ModificationRestObject.EMPLOYEES_CHANGE -> {
                if (modification.replaceAll) {
                    employeesRepo.replaceAllEmployees(companyId)
                } else {
                    modification.createdIds.forEach { employeesRepo.loadNewEmployee(it, companyId) }
                    modification.updatedIds.forEach { employeesRepo.loadNewEmployee(it, companyId) }
                    modification.deletedIds.forEach {
                        employeesRepo.setEmployeeDeleted(
                            it,
                            companyId
                        )
                    }
                }
            }

            ModificationRestObject.CERTS_CHANGE -> {
                if (modification.replaceAll) {
                    companyCertificatesRepo.replaceAllCerts(companyId)
                } else {
                    modification.createdIds.forEach {
                        companyCertificatesRepo.loadNewCert(
                            it,
                            companyId
                        )
                    }
                    modification.updatedIds.forEach {
                        companyCertificatesRepo.loadNewCert(
                            it,
                            companyId
                        )
                    }
                    modification.deletedIds.forEach {
                        companyCertificatesRepo.deleteCompanyCert(
                            it,
                            companyId
                        )
                    }
                }
            }

            ModificationRestObject.HAULERS -> {
                if (modification.replaceAll) {
                    haulersRepo.replaceAllHaulers(companyId)
                } else {
                    modification.createdIds.forEach { haulersRepo.loadNewHauler(it, companyId) }
                    modification.updatedIds.forEach { haulersRepo.loadNewHauler(it, companyId) }
                    modification.deletedIds.forEach { haulersRepo.setHaulerDeleted(it, companyId) }
                }
            }

            ModificationRestObject.SUBCONTRACTORS -> if (modification.replaceAll) {
                subcontractorsRepo.replaceAllSubContractors(companyId)
            } else {
                modification.createdIds.forEach {
                    subcontractorsRepo.loadNewSubcontractor(
                        it,
                        companyId
                    )
                }
                modification.updatedIds.forEach {
                    subcontractorsRepo.loadNewSubcontractor(
                        it,
                        companyId
                    )
                }
                modification.deletedIds.forEach {
                    subcontractorsRepo.setSubcontractorDeleted(
                        it,
                        companyId
                    )
                }
            }

            ModificationRestObject.PHASES_CHANGE -> {
                if (modification.replaceAll) {
                    phaseRepo.replaceAllPhases(companyId)
                } else {
                    modification.createdIds.forEach { phaseRepo.loadNewPhase(it, companyId) }
                    modification.updatedIds.forEach { phaseRepo.loadNewPhase(it, companyId) }
                    modification.deletedIds.forEach { phaseRepo.setPhaseDeleted(it, companyId) }
                }
            }

            ModificationRestObject.INVENTORY_ITEM_CHANGE -> {
                if (modification.replaceAll) {
                    inventoryItemsRepo.replaceAllInventoryItems(companyId)
                } else {
                    modification.createdIds.forEach {
                        inventoryItemsRepo.loadNewInventoryItem(
                            it,
                            companyId
                        )
                    }
                    modification.updatedIds.forEach {
                        inventoryItemsRepo.loadNewInventoryItem(
                            it,
                            companyId
                        )
                    }
                    modification.deletedIds.forEach {
                        inventoryItemsRepo.deleteInventoryItem(
                            it,
                            companyId
                        )
                    }
                }
            }

            ModificationRestObject.AVAILABLE_PROJECT_FORM_CHANGE -> {
                // getAllProjects - filter by projectID
                modification.allIds.forEach {
                    availableFormRepo.replaceAvailableForms(
                        it,
                        companyId
                    )
                }
            }

            ModificationRestObject.PROJECT_CHANGE -> {
                if (modification.replaceAll) throw IllegalArgumentException()
                modification.createdIds.forEach { createUpdateProject(it, companyId) }
                modification.updatedIds.forEach { createUpdateProject(it, companyId) }
                modification.deletedIds.forEach { deleteProject(it, companyId) }
            }
        }
    }

    private suspend fun createUpdateProject(projectId: Int, companyId: Int) {
        val projects = projectRepo.loadProject(projectId, companyId = companyId)
        if (projects.size == 1) {
            val project = projects[0]
            val permits = project.permits?.map { it.toPermit(companyId, projectId) } ?: emptyList()
            projectRepo.replaceProjectPermits(permits, projectId, companyId)
            val projectPermitDir = fileManager.getProjectPermitsDir(companyId, projectId)
            if (projectPermitDir.exists()) {
                projectPermitDir.deleteRecursively()
            }

            permits.asSequence()
                .filter { it.permitImageUrl.isNotEmpty() && it.documentFile.isNotEmpty() }
                .forEach { permit ->
                    val dest = fileManager.getProjectPermitFile(
                        companyId,
                        permit.projectId,
                        permit.documentFile
                    )
                    try {
                        fileDownloader.downloadFile(permit.permitImageUrl, dest)
                    } catch (e: Exception) {
                        // ignored
                    }
                }

            val availableForms =
                project.availableForms?.filter { SupportedForm.supportedFormIds.contains(it.formTypeId) }
                    ?.map { it.toAvailableForm(companyId, projectId) } ?: emptyList()

            availableFormRepo.replaceAvailableForms(availableForms, projectId, companyId)
            projectCertificatesRepo.loadProjectCertificates(projectId, companyId)
        }
    }

    private suspend fun deleteProject(projectId: Int, companyId: Int) {
        projectRepo.deleteProject(projectId, companyId)
        projectFormsRepo.getProjectForms(projectId, companyId).forEach {
            handleProjectFormDelete(it.internalFormId, companyId)
        }
        availableFormRepo.deleteAvailableFormsOnProject(projectId, companyId)
        projectCertificatesRepo.deleteProjectCertificateFromProject(projectId, companyId)
        fileManager.deleteProjectFiles(companyId, projectId)
    }

    private suspend fun handleProjectFormDelete(internalFormId: Int, companyId: Int) {
        projectFormsRepo.getProjectForm(internalFormId, companyId)?.let { savedForm ->
            projectFormsRepo.deleteProjectForm(internalFormId, companyId)
            deleteFormData(savedForm)
        }
    }

    private suspend fun deleteFormData(formToDelete: ProjectForm) {
        when (SupportedForm.fromFormTypeId(formToDelete.formTypeId)) {
            SupportedForm.FIELD_REPORT_FORM_ID -> dailyFieldReportRepo.deleteForm(
                formToDelete.internalFormId,
                formToDelete.companyId
            )
            SupportedForm.TIMESHEET_FORM_ID -> timeSheetRepo.deleteForm(
                formToDelete.internalFormId,
                formToDelete.companyId
            )
            SupportedForm.DAILY_LOG_FORM_ID -> dailyLogRepo.deleteForm(
                formToDelete.internalFormId,
                formToDelete.companyId
            )
            SupportedForm.PSI_FORM_ID -> psiRepo.deleteForm(
                formToDelete.internalFormId,
                formToDelete.companyId
            )
            SupportedForm.DAILY_DEMO_LOG_ID -> dailyDemoLogRepo.deleteForm(
                formToDelete.internalFormId,
                formToDelete.companyId
            )
        }
    }
}