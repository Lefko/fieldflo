package com.fieldflo.usecases

import com.fieldflo.data.persistence.keyValue.ILocalStorage
import javax.inject.Inject

class UserRolesUseCases @Inject constructor(private val localStorage: ILocalStorage) {
    fun hasCertificateRole() = localStorage.canUserCanViewCerts()

    fun hasCreateEditCertRole() = localStorage.canUserCanEditCerts()

    fun canUserViewProjects() = localStorage.canUserViewProjects()

    fun hasViewEditFormsRole() = localStorage.canUserCanAddEditOpenForms()

    fun hasEditTimesheetTimeRole() = localStorage.canUserCanEditTimeSheetTime()
}
