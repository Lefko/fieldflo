package com.fieldflo.usecases

import com.fieldflo.common.roundToSecond
import com.fieldflo.data.persistence.db.entities.TimeSheetEmployee
import com.fieldflo.data.persistence.db.entities.TimesheetEmployeeStatus
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.IEmployeesRepo
import com.fieldflo.data.repos.interfaces.IProjectsRepo
import com.fieldflo.data.repos.interfaces.ITimesheetRepo
import com.fieldflo.screens.formTimesheet.editTime.EditTimeData
import java.util.*
import javax.inject.Inject

class TimeSheetTimeClockUseCases @Inject constructor(
    private val localStorage: ILocalStorage,
    private val employeesRepo: IEmployeesRepo,
    private val projectsRepo: IProjectsRepo,
    private val timeSheetRepo: ITimesheetRepo
) {
    private val companyId get() = localStorage.getCurrentCompanyId()

    suspend fun editTime(editTimeData: EditTimeData): EditTimeResult {
        val allEmployeesOnProject = timeSheetRepo
            .getTimeSheetEmployeesByProjectId(editTimeData.projectId, companyId)
        // validate new times for each employee
        val allTimeSheetEmployees = allEmployeesOnProject
            .filter { it.internalFormId == editTimeData.internalFormId }

        val employeesToEdit =
            allTimeSheetEmployees.filter { it.timeSheetEmployeeId in editTimeData.timeSheetEmployeeIds }
        val employeesWithValidTimes = mutableListOf<TimeSheetEmployee>()
        val employeesWithoutValidTimes = mutableListOf<TimeSheetEmployee>()

        employeesToEdit.forEach { timeSheetEmployee ->
            if (TimeSheetTimeValidator.validateNewTimes(
                    timeSheetEmployee,
                    editTimeData.startDate,
                    editTimeData.endDate,
                    editTimeData.breakDurationSeconds,
                    allEmployeesOnProject.filter { it.employeeId == timeSheetEmployee.employeeId }
                )
            ) {
                employeesWithValidTimes.add(timeSheetEmployee)
            } else {
                employeesWithoutValidTimes.add(timeSheetEmployee)
            }
        }

        // if we have any employees with valid times perform edit
        if (employeesWithValidTimes.isNotEmpty()) {
            val project = projectsRepo.getProjectById(editTimeData.projectId, companyId)
            val perDiemInfo = PerDiemInfo.createFromProject(project)
            val editedEmployees = mutableListOf<TimeSheetEmployee>()
            employeesWithValidTimes.forEach {
                val result = if (editTimeData.startDate != null && editTimeData.endDate != null) {
                    editStartAndStop(
                        perDiemInfo,
                        it,
                        allTimeSheetEmployees,
                        editTimeData.startDate,
                        editTimeData.endDate,
                        editTimeData.breakDurationSeconds ?: it.breakTime
                    )
                } else if (editTimeData.startDate != null) {
                    editStart(
                        perDiemInfo,
                        it,
                        allTimeSheetEmployees,
                        editTimeData.startDate,
                        editTimeData.breakDurationSeconds ?: it.breakTime
                    )
                } else if (editTimeData.endDate != null) {
                    editStop(
                        perDiemInfo,
                        it,
                        allTimeSheetEmployees,
                        editTimeData.endDate,
                        editTimeData.breakDurationSeconds ?: it.breakTime
                    )
                } else {
                    editBreakInternal(
                        perDiemInfo,
                        it,
                        allTimeSheetEmployees,
                        editTimeData.breakDurationSeconds
                    )
                }
                if (result == it) {
                    // if the employee is the same then it couldnt be edited
                    employeesWithoutValidTimes.add(result)
                } else {
                    editedEmployees.add(result)
                }
            }
            timeSheetRepo.updateEmployees(editedEmployees)
        }

        return if (employeesWithoutValidTimes.isEmpty()) {
            EditTimeResult.Success
        } else {
            EditTimeResult.EditFailedFor(employeesWithoutValidTimes.mapNotNull {
                employeesRepo.getEmployeeById(it.employeeId, companyId)?.employeeFullName
            })
        }
    }

    /**
     * starts tracking the employees identified by their id
     *
     * @return Names of employees who could not be started
     */
    suspend fun startTrackingEmployees(timeSheetEmployeeIds: List<Int>): List<String> {
        val timeSheetEmployees = timeSheetRepo.getEmployeesById(timeSheetEmployeeIds, companyId)

        val alreadyStartedEmployees = timeSheetEmployees.filter {
            it.employeeStatus.ordinal >= TimesheetEmployeeStatus.CLOCKED_IN.ordinal
        }.mapNotNull {
            employeesRepo.getEmployeeById(it.employeeId, companyId)?.employeeFullName
        }

        val employeesToStartTracking = timeSheetEmployees.filter {
            it.employeeStatus == TimesheetEmployeeStatus.NOT_TRACKING
        }.map { it.timeSheetEmployeeId }

        timeSheetRepo.startTrackingEmployees(employeesToStartTracking, companyId)

        return alreadyStartedEmployees
    }

    /**
     * stops tracking the employees identified by their id
     *
     * @return Names of employees who could not be stopped (because break was too long)
     */
    suspend fun stopTrackingEmployees(
        projectId: Int,
        internalFormId: Int,
        timeSheetEmployeeIds: List<Int>,
        breakDurationSeconds: Int
    ): List<String> {
        val now: Date = Date().roundToSecond()

        val allTimeSheetEmployees = timeSheetRepo.getTimeSheetEmployeesByInternalFormId(
            internalFormId,
            companyId
        )
        val employeesToStopTracking =
            allTimeSheetEmployees.filter { it.timeSheetEmployeeId in timeSheetEmployeeIds }
        val project = projectsRepo.getProjectById(projectId, companyId)
        val perDiemInfo = PerDiemInfo.createFromProject(project)

        val cantStop = mutableListOf<TimeSheetEmployee>()
        val stoppable = mutableListOf<TimeSheetEmployee>()

        employeesToStopTracking.forEach {
            if (it.employeeStatus.ordinal != TimesheetEmployeeStatus.CLOCKED_IN.ordinal || it.clockIn.time == 0L) {
                cantStop.add(it)
            } else {
                val totalTimeSeconds = (now.time - it.clockIn.time) / 1_000L
                val workTimeSeconds = (totalTimeSeconds - breakDurationSeconds).toDouble()
                if (workTimeSeconds <= 0) {
                    cantStop.add(it)
                } else {
                    val requiresPerDiem = TimeSheetPerDiemFactory.employeeRequiresPerDiem(
                        it.employeeId,
                        perDiemInfo,
                        allTimeSheetEmployees
                    )
                    val newPerDiem = if (requiresPerDiem &&
                        it.employeeStatus.ordinal < TimesheetEmployeeStatus.CLOCKED_OUT.ordinal
                    ) {
                        TimeSheetPerDiemFactory.createPerDiem(perDiemInfo, workTimeSeconds)
                    } else {
                        null
                    }

                    val positions = it.positions.toMutableList()
                    val defaultPosition = it.positions.first { it.defaultPosition }
                    positions.remove(defaultPosition)
                    positions.add(defaultPosition.copy(totalWorkTime = workTimeSeconds.toInt()))

                    stoppable.add(
                        it.copy(
                            employeeStatus = TimesheetEmployeeStatus.CLOCKED_OUT,
                            clockOut = now,
                            perDiem = newPerDiem,
                            breakTime = breakDurationSeconds,
                            totalWorkTime = workTimeSeconds.toInt(),
                            positions = positions
                        )
                    )
                }
            }
        }

        timeSheetRepo.updateEmployees(stoppable)

        return cantStop.mapNotNull {
            employeesRepo.getEmployeeById(it.employeeId, companyId)?.employeeFullName
        }
    }

    suspend fun getTimeSheetEmployeeTimeData(timeSheetEmployeeId: Int): TimeSheetEmployeeTimeData {
        return timeSheetRepo.getEmployeeById(timeSheetEmployeeId, companyId)?.let { employee ->
            when (employee.employeeStatus) {
                TimesheetEmployeeStatus.CLOCKED_OUT,
                TimesheetEmployeeStatus.VERIFIED,
                TimesheetEmployeeStatus.VERIFIED_WITH_INJURY ->
                    TimeSheetEmployeeTimeData(
                        employee.clockIn,
                        employee.clockOut,
                        employee.breakTime
                    )

                TimesheetEmployeeStatus.CLOCKED_IN ->
                    TimeSheetEmployeeTimeData(employee.clockIn, null, null)

                TimesheetEmployeeStatus.NOT_TRACKING ->
                    TimeSheetEmployeeTimeData(null, null, null)
            }
        } ?: TimeSheetEmployeeTimeData(null, null, null)
    }

    private fun editStartAndStop(
        perDiemInfo: PerDiemInfo,
        timeSheetEmployee: TimeSheetEmployee,
        allEmployees: List<TimeSheetEmployee>,
        startDate: Date,
        endDate: Date,
        breakDurationSeconds: Int
    ): TimeSheetEmployee {
        val totalTimeSeconds = (endDate.time - startDate.time) / 1_000
        val workTimeSeconds = (totalTimeSeconds - breakDurationSeconds).toDouble()

        if (workTimeSeconds > 0) {
            val requiresPerDiem = TimeSheetPerDiemFactory.employeeRequiresPerDiem(
                timeSheetEmployee.employeeId,
                perDiemInfo,
                allEmployees
            )

            val newPerDiem = if (requiresPerDiem) {
                TimeSheetPerDiemFactory.createPerDiem(perDiemInfo, workTimeSeconds)
            } else {
                null
            }
            val positions = timeSheetEmployee.positions.toMutableList()
            val defaultPosition = timeSheetEmployee.positions.find { it.defaultPosition }!!
            positions.remove(defaultPosition)
            positions.add(defaultPosition.copy(totalWorkTime = workTimeSeconds.toInt()))
            return timeSheetEmployee.copy(
                employeeStatus = if (timeSheetEmployee.employeeStatus.ordinal < TimesheetEmployeeStatus.CLOCKED_OUT.ordinal) {
                    TimesheetEmployeeStatus.CLOCKED_OUT
                } else {
                    timeSheetEmployee.employeeStatus
                },
                clockIn = startDate,
                clockOut = endDate,
                perDiem = newPerDiem,
                breakTime = breakDurationSeconds,
                totalWorkTime = workTimeSeconds.toInt(),
                positions = positions
            )
        }
        return timeSheetEmployee
    }

    private fun editStart(
        perDiemInfo: PerDiemInfo,
        timeSheetEmployee: TimeSheetEmployee,
        allEmployees: List<TimeSheetEmployee>,
        startDate: Date,
        breakDurationSeconds: Int
    ): TimeSheetEmployee {
        return if (timeSheetEmployee.employeeStatus.ordinal >= TimesheetEmployeeStatus.CLOCKED_OUT.ordinal) {
            val end = timeSheetEmployee.clockOut
            val totalTimeSeconds = (end.time - startDate.time) / 1_000L
            val breakTimeActual = breakDurationSeconds
            val workTimeSeconds = (totalTimeSeconds - breakTimeActual).toInt()

            // update default position total work time
            val positions = timeSheetEmployee.positions.toMutableList()
            val defaultPosition = timeSheetEmployee.positions.first { it.defaultPosition }
            positions.remove(defaultPosition)
            positions.add(defaultPosition.copy(totalWorkTime = workTimeSeconds))

            val requiresPerDiem = TimeSheetPerDiemFactory.employeeRequiresPerDiem(
                timeSheetEmployee.employeeId,
                perDiemInfo,
                allEmployees
            )
            val newPerDiem = if (requiresPerDiem) {
                TimeSheetPerDiemFactory.createPerDiem(perDiemInfo, workTimeSeconds.toDouble())
            } else {
                null
            }

            timeSheetEmployee.copy(
                clockIn = startDate,
                breakTime = breakTimeActual,
                totalWorkTime = workTimeSeconds,
                perDiem = newPerDiem,
                positions = positions
            )
        } else {
            timeSheetEmployee.copy(
                employeeStatus = TimesheetEmployeeStatus.CLOCKED_IN,
                clockIn = startDate
            )
        }
    }

    private fun editStop(
        perDiemInfo: PerDiemInfo,
        timeSheetEmployee: TimeSheetEmployee,
        allEmployees: List<TimeSheetEmployee>,
        endDate: Date,
        breakDurationSeconds: Int
    ): TimeSheetEmployee {
        val requiresPerDiemCalculation = TimeSheetPerDiemFactory.employeeRequiresPerDiem(
            employeeId = timeSheetEmployee.employeeId,
            perDiemInfo = perDiemInfo,
            allEmployees = allEmployees
        )
        val start = timeSheetEmployee.clockIn
        val totalTimeSeconds = (endDate.time - start.time) / 1_000L
        val workTimeSeconds = (totalTimeSeconds - breakDurationSeconds).toDouble()
        if (workTimeSeconds <= 0) {
            return timeSheetEmployee
        }

        // calculate per diem (if applicable)
        val newPerDiem = if (requiresPerDiemCalculation) {
            TimeSheetPerDiemFactory.createPerDiem(perDiemInfo, workTimeSeconds)
        } else {
            null
        }

        // update default position total work time
        val positions = timeSheetEmployee.positions.toMutableList()
        val defaultPosition = timeSheetEmployee.positions.first { it.defaultPosition }
        positions.remove(defaultPosition)
        positions.add(defaultPosition.copy(totalWorkTime = workTimeSeconds.toInt()))

        // update timesheet employee
        return timeSheetEmployee.copy(
            employeeStatus = if (timeSheetEmployee.employeeStatus.ordinal < TimesheetEmployeeStatus.CLOCKED_OUT.ordinal) {
                TimesheetEmployeeStatus.CLOCKED_OUT
            } else {
                timeSheetEmployee.employeeStatus
            },
            clockOut = endDate,
            perDiem = newPerDiem,
            breakTime = breakDurationSeconds,
            totalWorkTime = workTimeSeconds.toInt(),
            positions = positions
        )
    }

    private fun editBreakInternal(
        perDiemInfo: PerDiemInfo,
        timeSheetEmployee: TimeSheetEmployee,
        allEmployees: List<TimeSheetEmployee>,
        newBreakDurationSeconds: Int?
    ): TimeSheetEmployee {
        return if (timeSheetEmployee.employeeStatus.ordinal < TimesheetEmployeeStatus.CLOCKED_OUT.ordinal) {
            timeSheetEmployee
        } else if (newBreakDurationSeconds == null) {
            timeSheetEmployee
        } else if (newBreakDurationSeconds == timeSheetEmployee.breakTime) {
            timeSheetEmployee
        } else {
            editStop(
                perDiemInfo,
                timeSheetEmployee,
                allEmployees,
                timeSheetEmployee.clockOut,
                newBreakDurationSeconds
            )
        }
    }
}

sealed class EditTimeResult {
    object Success : EditTimeResult()
    object PinFailure : EditTimeResult()
    data class EditFailedFor(
        val timeSheetEmployeeNames: List<String>
    ) : EditTimeResult()
}

data class TimeSheetEmployeeTimeData(
    val startDate: Date?,
    val endDate: Date?,
    val breakDurationSeconds: Int?
)
