package com.fieldflo.usecases

import com.fieldflo.data.persistence.db.entities.Employee
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.IEmployeesRepo
import javax.inject.Inject

class EmployeeUseCases @Inject constructor(
    private val localStorage: ILocalStorage,
    private val employeesRepo: IEmployeesRepo
) {

    private val companyId: Int
        get() = localStorage.getCurrentCompanyId()

    suspend fun getEmployeeById(employeeId: Int): Employee? {
        return employeesRepo.getEmployeeById(employeeId, companyId)
    }

    suspend fun getAllEmployees(): List<Employee> {
        return employeesRepo.getAllEmployees(companyId)
    }
}
