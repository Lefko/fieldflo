package com.fieldflo.usecases

import com.fieldflo.data.persistence.db.entities.ActiveFormData
import com.fieldflo.data.persistence.fileSystem.FileManager
import com.fieldflo.data.repos.interfaces.IEmployeesRepo
import com.fieldflo.data.repos.interfaces.IProjectFormsRepo
import com.fieldflo.data.repos.interfaces.ITimesheetRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

class DefaultPsiEmployeesHelper(
    private val employeeRepo: IEmployeesRepo,
    private val projectFormsRepo: IProjectFormsRepo,
    private val timeSheetRepo: ITimesheetRepo,
    private val fileManager: FileManager,
    private val companyId: Int
) {

    suspend fun getDefaultPsiEmployees(
        internalFormId: Int,
        projectId: Int
    ): List<PsiEmployeeUiData> {
        // first get all active timesheets on this project
        val timesheets = projectFormsRepo.getActiveProjectForms(projectId, companyId)
            .filter { it.formTypeId == SupportedForm.TIMESHEET_FORM_ID.formTypeId }

        // extract the latest timesheet
        val timeSheetInternalFormId = when {
            timesheets.size > 1 -> latestTimeSheetId(timesheets)
            timesheets.size == 1 -> timesheets[0].internalFormId
            else -> -1
        }

        // get the employees on that timesheet
        val timehseetEmployees =
            timeSheetRepo.getTimeSheetEmployeesByInternalFormId(timeSheetInternalFormId, companyId)

        return timehseetEmployees
            .distinctBy { it.employeeId }
            .map {
                val employee = employeeRepo.getEmployeeById(it.employeeId, companyId)
                PsiEmployeeUiData(
                    psiEmployeeId = 0,
                    employeeId = it.employeeId,
                    employeeFullName = employee?.employeeFullName ?: "",
                    employeePin = employee?.employeePinNumber ?: "",
                    pinUserName = "",
                    pinEmployeeId = 0,
                    pinDate = Date(0),
                    pinImageFileLocation = fileManager.getPsiPhotoPinFileLocation(
                        companyId,
                        projectId,
                        it.employeeId,
                        Date(),
                        internalFormId
                    )
                )
            }
    }

    private suspend fun latestTimeSheetId(timesheets: List<ActiveFormData>): Int {
        return withContext(Dispatchers.Default) {
            var latestFormDateMillis = 0L
            var latestFormDateInternalFormId = 0

            timesheets.forEach {
                if (it.formDate.time > latestFormDateMillis) {
                    latestFormDateMillis = it.formDate.time
                    latestFormDateInternalFormId = it.internalFormId
                }
            }

            latestFormDateInternalFormId
        }
    }
}