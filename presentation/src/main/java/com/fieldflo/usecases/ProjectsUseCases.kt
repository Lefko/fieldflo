package com.fieldflo.usecases

import com.fieldflo.common.daysInTheFuture
import com.fieldflo.data.persistence.db.entities.*
import com.fieldflo.data.persistence.fileSystem.FileManager
import com.fieldflo.data.persistence.keyValue.ILocalStorage
import com.fieldflo.data.repos.interfaces.IProjectsRepo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.util.*
import javax.inject.Inject

class ProjectsUseCases @Inject constructor(
    private val localStorage: ILocalStorage,
    private val projectsRepo: IProjectsRepo,
    private val fileManager: FileManager
) {
    private val companyId: Int
        get() = localStorage.getCurrentCompanyId()

    fun getAllProjectsFlow(): Flow<List<BasicProjectData>> {
        return projectsRepo.getAllActiveProjectsFlow(companyId)
    }

    fun getProjectStatusFlow(projectId: Int): Flow<ProjectStatus> {
        return projectsRepo.projectDeleted(projectId, companyId).map {
            if (it) ProjectStatus.DELETED
            else ProjectStatus.OPEN
        }
    }

    suspend fun getProjectName(projectId: Int): String {
        return projectsRepo.getProjectName(projectId, companyId)
    }

    suspend fun getProjectNumber(projectId: Int): String {
        return projectsRepo.getProjectNumber(projectId, companyId)
    }

    suspend fun isPhaseRequiredOnProject(projectId: Int): Boolean {
        return projectsRepo.isPhaseRequiredOnProject(projectId, companyId)
    }

    suspend fun getProjectGeneralInfo(projectId: Int): ProjectGeneralInfoData {
        return projectsRepo.getProjectDetailsGeneralInfo(projectId, companyId)
    }

    suspend fun getProjectDetails(projectId: Int): ProjectDetailsData {
        return projectsRepo.getProjectDetails(projectId, companyId)
    }

    suspend fun getProjectSite(projectId: Int): ProjectSiteData {
        return projectsRepo.getProjectSite(projectId, companyId)
    }

    suspend fun getProjectPermits(projectId: Int): List<ProjectPermitData> {
        return projectsRepo.getPermits(projectId, companyId).map {
            val fileLocation =
                fileManager.getProjectPermitFile(companyId, projectId, it.documentFile)
            ProjectPermitData(
                permitType = it.permitType,
                expireDate = it.expireDate,
                expiresWithinThirty = it.expireDate.daysInTheFuture() <= 30,
                permitFileLocation = if (fileLocation.exists()) {
                    fileLocation.absolutePath
                } else {
                    ""
                },
                permitFileExtension = if (fileLocation.exists()) {
                    fileLocation.extension
                } else {
                    ""
                }
            )
        }
    }

    suspend fun getProjectPerDiem(projectId: Int): ProjectPerDiemData {
        return projectsRepo.getProjectPerDiemData(projectId, companyId)
    }

    suspend fun getProjectBuildingInto(projectId: Int): ProjectBuildingInformationData {
        return projectsRepo.getProjectBuildingInfo(projectId, companyId)
    }

    suspend fun getProjectOwnerData(projectId: Int): ProjectOwnerData {
        return projectsRepo.getProjectOwnerData(projectId, companyId)
    }

    suspend fun getProjectProcedures(projectId: Int): String {
        return projectsRepo.getProjectProcedures(projectId, companyId)
    }

    suspend fun getProjectBillingInfo(projectId: Int): ProjectBillingInfo {
        return projectsRepo.getProjectBillingInfo(projectId, companyId)
    }

    suspend fun getProjectAirMonitoringData(projectId: Int): ProjectAirMonitoringData {
        return projectsRepo.getProjectAirMonitoringData(projectId, companyId)
    }

    suspend fun getProjectConsultantData(projectId: Int): ProjectConsultantData {
        return projectsRepo.getProjectConsultantData(projectId, companyId)
    }

    suspend fun getProjectLienInformation(projectId: Int): ProjectLienInfo {
        return projectsRepo.getProjectLienInformation(projectId, companyId)
    }

    suspend fun getProjectEmergencyInfo(projectId: Int): ProjectEmergencyInfo {
        return projectsRepo.getProjectEmergencyInfo(projectId, companyId)
    }

    suspend fun getProjectInsuranceCertificateData(projectId: Int): ProjectInsuranceCertificateData {
        return projectsRepo.getProjectInsuranceCertificateData(projectId, companyId)
    }

    suspend fun getProjectOtherData(projectId: Int): ProjectOtherData {
        return projectsRepo.getProjectOtherData(projectId, companyId)
    }

    suspend fun getProjectPayrollData(projectId: Int): ProjectPayrollData {
        return projectsRepo.getProjectPayrollData(projectId, companyId)
    }

    suspend fun getProjectDesignerData(projectId: Int): ProjectDesignerData {
        return projectsRepo.getProjectDesignerData(projectId, companyId)
    }
}

enum class ProjectStatus {
    OPEN, CLOSED, DELETED
}

data class ProjectPermitData(
    val permitType: String,
    val expireDate: Date,
    val expiresWithinThirty: Boolean,
    val permitFileLocation: String,
    val permitFileExtension: String
)
