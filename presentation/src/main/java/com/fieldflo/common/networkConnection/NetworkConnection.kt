package com.fieldflo.common.networkConnection

import android.content.Context
import android.os.Build
import kotlinx.coroutines.flow.Flow

class NetworkConnection(context: Context) : IConnectionData {

    private val connectivityDataStrategy: IConnectionData =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            NougatConnectionDataStrategy(context)
        } else {
            LegacyConnectionDataStrategy(context)
        }

    override val isInternetConnected: Boolean
        get() = connectivityDataStrategy.isInternetConnected
    override val isWifiConnected: Boolean
        get() = connectivityDataStrategy.isWifiConnected

    override fun getWifiConnected(): Flow<Boolean> {
        return connectivityDataStrategy.getWifiConnected()
    }

    override fun getInternetConnected(): Flow<Boolean> {
        return connectivityDataStrategy.getInternetConnected()
    }
}

interface IConnectionData {
    /**
     * Is this device connected to the internet
     */
    val isInternetConnected: Boolean

    /**
     * Is this device connected via wifi. Assumes isInternetConnected == true
     */
    val isWifiConnected: Boolean

    fun getWifiConnected(): Flow<Boolean>
    fun getInternetConnected(): Flow<Boolean>
}