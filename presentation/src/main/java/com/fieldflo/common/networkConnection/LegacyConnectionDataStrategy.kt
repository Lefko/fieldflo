package com.fieldflo.common.networkConnection

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import android.os.Build
import android.os.PowerManager
import com.fieldflo.common.interval
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged

class LegacyConnectionDataStrategy(
    private val context: Context
) : IConnectionData {

    private val wifiManager: WifiManager =
        context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager

    private val wifiOnFlow =
        MutableSharedFlow<Boolean>(
            extraBufferCapacity = 2,
            onBufferOverflow = BufferOverflow.DROP_OLDEST
        )
    private val internetConnectedFlow =
        MutableSharedFlow<Boolean>(
            extraBufferCapacity = 2,
            onBufferOverflow = BufferOverflow.DROP_OLDEST
        )

    init {
        wifiOnFlow.tryEmit(isWifiConnected)
        observeWifiState()

        internetConnectedFlow.tryEmit(isInternetConnected)
        observeInternetOn()
    }

    override val isInternetConnected: Boolean
        get() = isNetworkOnline(context)

    override val isWifiConnected: Boolean
        get() = isInternetConnected && wifiManager.isWifiEnabled

    override fun getWifiConnected(): Flow<Boolean> {
        return wifiOnFlow
            .distinctUntilChanged()
            .combine(interval(5_000, true)) { wifiOn, _ ->
                if (wifiOn) {
                    !isIdleMode(context) && isWifiConnected
                } else {
                    false
                }
            }
    }

    override fun getInternetConnected(): Flow<Boolean> {
        return internetConnectedFlow.distinctUntilChanged()
    }

    private fun observeWifiState() {
        val filter = IntentFilter()
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION)

        val receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                wifiOnFlow.tryEmit(isWifiConnected)
            }
        }

        context.registerReceiver(receiver, filter)
    }

    private fun observeInternetOn() {
        val filter = IntentFilter()
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        val receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                internetConnectedFlow.tryEmit(isInternetConnected)
            }
        }

        context.registerReceiver(receiver, filter)
    }

    private fun isIdleMode(context: Context): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true
        }
        val packageName = context.packageName
        val manager = context.getSystemService(Context.POWER_SERVICE) as PowerManager
        val isIgnoringOptimizations = manager.isIgnoringBatteryOptimizations(packageName)
        return manager.isDeviceIdleMode && !isIgnoringOptimizations
    }
}
