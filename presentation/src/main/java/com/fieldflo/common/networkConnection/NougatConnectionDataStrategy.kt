package com.fieldflo.common.networkConnection

import android.annotation.TargetApi
import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.wifi.WifiManager
import android.os.Build
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow

@TargetApi(Build.VERSION_CODES.N)
class NougatConnectionDataStrategy(
    context: Context
) : IConnectionData {

    private val connectivityManager: ConnectivityManager =
        context.getSystemService(ConnectivityManager::class.java)
    private val wifiManager: WifiManager = context.getSystemService(WifiManager::class.java)

    private var currentNetwork: Network? = null
    private val wifiOnFlow = MutableStateFlow(wifiManager.isWifiEnabled)
    private val internetConnectedFlow = MutableStateFlow(false)

    private val networkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            currentNetwork = network
            internetConnectedFlow.tryEmit(isInternetConnected)
            wifiOnFlow.tryEmit(isWifiConnected)
        }

        override fun onLost(network: Network) {
            currentNetwork = null
            internetConnectedFlow.tryEmit(isInternetConnected)
            wifiOnFlow.tryEmit(isWifiConnected)
        }
    }

    init {
        connectivityManager.registerDefaultNetworkCallback(networkCallback)
    }

    override val isInternetConnected: Boolean
        get() = currentNetwork != null &&
                connectivityManager.getNetworkCapabilities(currentNetwork)
                    ?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) == true

    override val isWifiConnected: Boolean
        get() = isInternetConnected &&
                currentNetwork != null &&
                connectivityManager.getNetworkCapabilities(currentNetwork)
                    ?.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) == true

    override fun getWifiConnected(): Flow<Boolean> {
        return wifiOnFlow
    }

    override fun getInternetConnected(): Flow<Boolean> {
        return internetConnectedFlow
    }
}
