package com.fieldflo.common

import android.content.Context
import com.fieldflo.R
import java.util.*
import javax.inject.Inject

class StringProvider @Inject constructor(context: Context) {

    private val context = context.applicationContext

    fun getEmployeesNotAddedErrorMessage(employeeCount: Int): String {
        return when (employeeCount) {
            0 -> ""
            1 -> context.getString(R.string.activity_daily_timesheet_single_employee_not_added)
            else -> context.getString(R.string.activity_daily_timesheet_employees_not_added, employeeCount)
        }
    }

    fun currentDayOfWeek(): String {
        return when (Calendar.getInstance().get(Calendar.DAY_OF_WEEK)) {
            Calendar.SUNDAY -> context.getString(R.string.activity_daily_log_form_sunday)
            Calendar.MONDAY -> context.getString(R.string.activity_daily_log_form_monday)
            Calendar.TUESDAY -> context.getString(R.string.activity_daily_log_form_tuesday)
            Calendar.WEDNESDAY -> context.getString(R.string.activity_daily_log_form_wednesday)
            Calendar.THURSDAY -> context.getString(R.string.activity_daily_log_form_thursday)
            Calendar.FRIDAY -> context.getString(R.string.activity_daily_log_form_friday)
            Calendar.SATURDAY -> context.getString(R.string.activity_daily_log_form_saturday)
            else -> context.getString(R.string.activity_daily_log_form_monday)
        }
    }

    fun getPleaseSelectMessage() = context.getString(R.string.please_select)

    fun getShiftLengthMessage(hours: Int) =
        context.getString(R.string.activity_project_details_shift_length_value, hours)
}