package com.fieldflo.common.hiddenCamera.config

import androidx.annotation.IntDef

class CameraFacing private constructor() {

    companion object {
        /**
         * Rear facing camera id.
         *
         * @see android.hardware.Camera.CameraInfo.CAMERA_FACING_BACK
         */
        const val REAR_FACING_CAMERA = 0

        /**
         * Front facing camera id.
         *
         * @see android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT
         */
        const val FRONT_FACING_CAMERA = 1
    }

    @Retention(AnnotationRetention.SOURCE)
    @IntDef(REAR_FACING_CAMERA, FRONT_FACING_CAMERA)
    annotation class SupportedCameraFacing
}