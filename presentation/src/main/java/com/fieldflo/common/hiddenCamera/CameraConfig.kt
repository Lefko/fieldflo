@file:Suppress("DEPRECATION")

package com.fieldflo.common.hiddenCamera

import android.content.Context
import android.hardware.Camera
import com.fieldflo.common.hiddenCamera.config.*
import java.io.File


data class CameraConfig(
    @CameraResolution.SupportedResolution
    val resolution: Int = CameraResolution.MEDIUM_RESOLUTION,

    @CameraFacing.SupportedCameraFacing
    val facing: Int = CameraFacing.REAR_FACING_CAMERA,

    @CameraImageFormat.SupportedImageFormat
    val imageFormat: Int = CameraImageFormat.FORMAT_JPEG,

    @CameraRotation.SupportedRotation
    val imageRotation: Int = CameraRotation.ROTATION_0,

    @CameraFocus.SupportedCameraFocus
    val cameraFocus: Int = CameraFocus.AUTO,

    val imageFile: File
) {

    val focusMode: String?
        get() {
            return when (cameraFocus) {
                CameraFocus.AUTO -> Camera.Parameters.FOCUS_MODE_AUTO
                CameraFocus.CONTINUOUS_PICTURE -> Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE
                CameraFocus.NO_FOCUS -> null
                else -> throw IllegalArgumentException("Invalid camera focus mode.")
            }
        }

    data class Builder(
        private val context: Context,

        @CameraResolution.SupportedResolution
        var resolution: Int = CameraResolution.MEDIUM_RESOLUTION,

        @CameraFacing.SupportedCameraFacing
        var facing: Int = CameraFacing.REAR_FACING_CAMERA,

        @CameraImageFormat.SupportedImageFormat
        var imageFormat: Int = CameraImageFormat.FORMAT_JPEG,

        @CameraRotation.SupportedRotation
        var imageRotation: Int = CameraRotation.ROTATION_0,

        @CameraFocus.SupportedCameraFocus
        var cameraFocus: Int = CameraFocus.AUTO,

        var imageFile: File = File(
            HiddenCameraUtils.getCacheDir(context),
            "IMG_" + System.currentTimeMillis() +
                    if (imageFormat == CameraImageFormat.FORMAT_JPEG) ".jpeg" else ".png"
        )
    ) {

        /**
         * Set the resolution of the output camera image. If you don't specify any resolution,
         * default image resolution will set to {@link CameraResolution#MEDIUM_RESOLUTION}.
         *
         * @param resolution Any resolution from:
         *                   <li>{@link CameraResolution#HIGH_RESOLUTION}</li>
         *                   <li>{@link CameraResolution#MEDIUM_RESOLUTION}</li>
         *                   <li>{@link CameraResolution#LOW_RESOLUTION}</li>
         * @return {@link Builder}
         * @see CameraResolution
         */
        fun resolution(@CameraResolution.SupportedResolution resolution: Int) = apply { this.resolution = resolution }

        /**
         * Set the camera facing with which you want to capture image.
         * Either rear facing camera or front facing camera. If you don't provide any camera facing,
         * default camera facing will be {@link CameraFacing#FRONT_FACING_CAMERA}.
         *
         * @param facing Any camera facing from:
         *                     <li>{@link CameraFacing#REAR_FACING_CAMERA}</li>
         *                     <li>{@link CameraFacing#FRONT_FACING_CAMERA}</li>
         * @return {@link Builder}
         * @see CameraFacing
         */
        fun facing(@CameraFacing.SupportedCameraFacing facing: Int) = apply { this.facing = facing }

        /**
         * Set the camera focus mode. If you don't provide any camera focus mode,
         * default focus mode will be {@link CameraFocus#AUTO}.
         *
         * @param cameraFocus Any camera focus mode from:
         *                  <li>{@link CameraFocus#AUTO}</li>
         *                  <li>{@link CameraFocus#CONTINUOUS_PICTURE}</li>
         *                  <li>{@link CameraFocus#NO_FOCUS}</li>
         * @return {@link Builder}
         * @see CameraFocus
         */
        fun focus(@CameraFocus.SupportedCameraFocus cameraFocus: Int) = apply { this.cameraFocus = cameraFocus }

        /**
         * Specify the image format for the output image. If you don't specify any output format,
         * default output format will be {@link CameraImageFormat#FORMAT_JPEG}.
         *
         * @param imageFormat Any supported image format from:
         *                    <li>{@link CameraImageFormat#FORMAT_JPEG}</li>
         *                    <li>{@link CameraImageFormat#FORMAT_PNG}</li>
         * @return {@link Builder}
         * @see CameraImageFormat
         */
        fun imageFormat(@CameraImageFormat.SupportedImageFormat imageFormat: Int) =
            apply { this.imageFormat = imageFormat }

        /**
         * Specify the output image rotation. The output image will be rotated by amount of degree specified
         * before stored to the output file. By default there is no rotation applied.
         *
         * @param imageRotation Any supported rotation from:
         *                 <li>{@link CameraRotation#ROTATION_0}</li>
         *                 <li>{@link CameraRotation#ROTATION_90}</li>
         *                 <li>{@link CameraRotation#ROTATION_180}</li>
         *                 <li>{@link CameraRotation#ROTATION_270}</li>
         * @return {@link Builder}
         * @see CameraRotation
         */
        fun imageRotation(@CameraRotation.SupportedRotation imageRotation: Int) =
            apply { this.imageRotation = imageRotation }

        /**
         * Set the location of the out put image. If you do not set any file for the output image, by
         * default image will be stored in the application's cache directory.
         *
         * @param imageFile {@link File} where you want to store the image.
         * @return {@link Builder}
         */
        fun imageFile(imageFile: File) = apply { this.imageFile = imageFile }

        fun build() = CameraConfig(
            resolution = resolution,
            facing = facing,
            imageFormat = imageFormat,
            imageRotation = imageRotation,
            cameraFocus = cameraFocus,
            imageFile = imageFile
        )
    }
}