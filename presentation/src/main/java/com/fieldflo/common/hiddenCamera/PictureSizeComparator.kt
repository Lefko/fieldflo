@file:Suppress("DEPRECATION")

package com.fieldflo.common.hiddenCamera

import android.hardware.Camera

class PictureSizeComparator : Comparator<Camera.Size> {

    override fun compare(a: Camera.Size?, b: Camera.Size?): Int {
        if (a == null && b == null) {
            return 0
        } else if (a == null) {
            return -1
        } else if (b == null) {
            return 1
        }
        return (b.height * b.width) - (a.height * a.width)
    }
}