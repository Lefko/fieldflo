package com.fieldflo.common.hiddenCamera.config

import androidx.annotation.IntDef


class CameraImageFormat private constructor() {

    companion object {
        /**
         * Image format for .jpg/.jpeg.
         */
        const val FORMAT_JPEG = 849

        /**
         * Image format for .png.
         */
        const val FORMAT_PNG = 545
        /**
         * Image format for .png.
         */
        const val FORMAT_WEBP = 563
    }

    @Retention(AnnotationRetention.SOURCE)
    @IntDef(FORMAT_JPEG, FORMAT_PNG, FORMAT_WEBP)
    annotation class SupportedImageFormat
}