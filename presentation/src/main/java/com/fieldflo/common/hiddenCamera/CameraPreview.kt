@file:Suppress("DEPRECATION")

package com.fieldflo.common.hiddenCamera

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.hardware.Camera
import android.os.Handler
import android.os.Looper
import android.view.SurfaceHolder
import android.view.SurfaceView
import com.fieldflo.common.hiddenCamera.config.CameraResolution
import com.fieldflo.common.hiddenCamera.config.CameraRotation
import timber.log.Timber
import java.io.File
import java.io.IOException
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread

@SuppressLint("ViewConstructor")
class CameraPreview(context: Context, private val cameraCallbacks: CameraCallbacks) : SurfaceView(context),
    SurfaceHolder.Callback {

    private var camera: Camera? = null
    private var cameraConfig: CameraConfig? = null

    private var safeToTakePicture = AtomicBoolean(false)

    init {
        holder.addCallback(this)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        // do nothing
    }

    override fun surfaceCreated(holder: SurfaceHolder) {
        // do nothing
    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
        if (camera == null) {
            cameraCallbacks.onCameraError(CameraError.ERROR_CAMERA_OPEN_FAILED)
            return
        } else if (holder.surface == null) {
            cameraCallbacks.onCameraError(CameraError.ERROR_CAMERA_OPEN_FAILED)
            return
        }
        camera?.let {
            try {
                it.stopPreview()
            } catch (e: Exception) {
                // Ignore: tried to stop a non-existent preview
            }

            val parameters = it.parameters
            val pictureSizes = parameters.supportedPictureSizes

            pictureSizes.sortWith(PictureSizeComparator())

            val cameraSize = when (cameraConfig!!.resolution) {
                CameraResolution.HIGH_RESOLUTION -> pictureSizes[0] // Highest resolution
                CameraResolution.MEDIUM_RESOLUTION -> pictureSizes[pictureSizes.size / 2]  // Middle resolution
                CameraResolution.LOW_RESOLUTION -> pictureSizes[pictureSizes.size - 1] // Lowest resolution
                else -> throw IllegalArgumentException("Invalid camera resolution.")
            }
            parameters.setPictureSize(cameraSize.width, cameraSize.height)

            val supportedFocusMode = parameters.supportedFocusModes
            if (supportedFocusMode.contains(cameraConfig!!.focusMode)) {
                parameters.focusMode = cameraConfig!!.focusMode
            }

            requestLayout()

            it.parameters = parameters

            try {
                it.setDisplayOrientation(90)
                it.setPreviewDisplay(holder)
                it.startPreview()

                safeToTakePicture.set(true)
            } catch (e: IOException) {
                cameraCallbacks.onCameraError(CameraError.ERROR_CAMERA_OPEN_FAILED)
            }
        }
    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {
        camera?.stopPreview()
    }

    /**
     * Initialize the camera and start the preview of the camera.
     *
     * @param cameraConfig camera config builder.
     */
    fun startCameraInternal(cameraConfig: CameraConfig) {
        this.cameraConfig = cameraConfig

        if (safeCameraOpen(cameraConfig.facing)) {
            camera?.let {
                requestLayout()

                try {
                    it.setPreviewDisplay(holder)
                    it.startPreview()
                } catch (e: IOException) {
                    e.printStackTrace()
                    cameraCallbacks.onCameraError(CameraError.ERROR_CAMERA_OPEN_FAILED)
                }

            }
        } else {
            cameraCallbacks.onCameraError(CameraError.ERROR_CAMERA_OPEN_FAILED)
        }
    }

    private fun safeCameraOpen(id: Int): Boolean {
        var qOpened = false

        try {
            stopPreviewAndFreeCamera()

            camera = Camera.open(id)
            qOpened = camera != null
        } catch (e: Exception) {
            Timber.d(e, "failed to open Camera")
        }

        return qOpened
    }

    fun takePictureInternal() {
        safeToTakePicture.set(false)
        if (camera != null) {
            camera?.let { notNullCamera ->
                try {
                    notNullCamera.takePicture(null, null, Camera.PictureCallback { bytes, _ ->
                        thread {
                            // Convert byte array to bitmap
                            var bitmap: Bitmap? =
                                BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
                            bitmap?.let { notNullBitmap ->
                                // Rotate the bitmap
                                cameraConfig?.let { notNullConfig ->
                                    val rotatedBitmap: Bitmap =
                                        if (notNullConfig.imageRotation != CameraRotation.ROTATION_0) {
                                            HiddenCameraUtils.rotateBitmap(
                                                notNullBitmap,
                                                notNullConfig.imageRotation
                                            )
                                        } else {
                                            notNullBitmap
                                        }

                                    bitmap = null

                                    // Save image to the file.
                                    if (HiddenCameraUtils.saveImageFromFile(
                                            rotatedBitmap,
                                            notNullConfig.imageFile,
                                            notNullConfig.imageFormat
                                        )
                                    ) {
                                        //Post image file to the main thread
                                        Handler(Looper.getMainLooper())
                                            .post { cameraCallbacks.onImageCapture(notNullConfig.imageFile) }
                                    } else {
                                        //Post error to the main thread
                                        Handler(Looper.getMainLooper())
                                            .post { cameraCallbacks.onCameraError(CameraError.ERROR_IMAGE_WRITE_FAILED) }
                                    }

                                    notNullCamera.startPreview()
                                }

                            }
                            safeToTakePicture.set(true)
                        }
                    })
                } catch (e: RuntimeException) {
                    cameraCallbacks.onCameraError(CameraError.ERROR_CAMERA_OPEN_FAILED)
                }
            }
        } else {
            cameraCallbacks.onCameraError(CameraError.ERROR_CAMERA_OPEN_FAILED)
            safeToTakePicture.set(true)
        }
    }

    fun takePictureInternal(dest: File) {
        cameraConfig = cameraConfig?.copy(
            imageFile = dest
        )
        takePictureInternal()
    }

    /**
     * When this function returns, camera will be null.
     */
    fun stopPreviewAndFreeCamera() {
        safeToTakePicture.set(false)
        camera?.let {
            it.stopPreview()
            it.release()
            camera = null
        }
    }
}