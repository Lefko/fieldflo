package com.fieldflo.common.timberTrees

import android.util.Log.*
import com.google.firebase.crashlytics.FirebaseCrashlytics
import io.shipbook.shipbooksdk.Log
import io.shipbook.shipbooksdk.ShipBook
import timber.log.Timber

private const val MAX_LOG_LENGTH = 4000

class DebugLogTree : Timber.DebugTree() {

    init {
        ShipBook.addWrapperClass(this::class.java.name)
    }

    private val crashlytics = FirebaseCrashlytics.getInstance()

    override fun createStackElementTag(element: StackTraceElement): String? {
        return super.createStackElementTag(element) + " - " + element.lineNumber
    }

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        t?.let {
            crashlytics.recordException(it)
        }

        when (priority) {
            VERBOSE -> Log.v(tag ?: "", message, t)
            INFO -> Log.i(tag ?: "", message, t)
            WARN -> Log.w(tag ?: "", message, t)
            ERROR -> Log.e(tag ?: "", message, t)
            else -> Log.d(tag ?: "", message, t)
        }

        // Message is short enough, doesn't need to be broken into chunks
        if (message.length < MAX_LOG_LENGTH) {
            crashlytics.log("${tag ?: ""} - $message")
            return
        }

        // Split by line, then ensure each line can fit into Log's max length
        var i = 0
        val length = message.length
        while (i < length) {
            var newline = message.indexOf('\n', i)
            newline = if (newline != -1) newline else length
            do {
                val end = Math.min(newline, i + MAX_LOG_LENGTH)
                val part = message.substring(i, end)
                crashlytics.log("${tag ?: ""} - $part")
                i = end
            } while (i < newline)
            i++
        }
    }
}