package com.fieldflo.common

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.cancellable
import kotlinx.coroutines.flow.flow
import java.util.concurrent.TimeUnit

fun interval(intervalMillis: Long, isImmediate: Boolean): Flow<Long> {
    return flow {
        var intervalCount = 0L
        if (isImmediate) emit(intervalCount++)
        while (true) {
            delay(intervalMillis)
            emit(intervalCount++)
        }
    }.cancellable()
        .buffer()
}

fun interval(interval: Long, unit: TimeUnit, isImmediate: Boolean): Flow<Long> {
    return interval(unit.toMillis(interval), isImmediate)
}

@ExperimentalCoroutinesApi
fun <T> SendChannel<T>.safeOffer(element: T) {
    if (!this.isClosedForSend) {
        trySend(element)
    }
}