package com.fieldflo.common.dateTimePickers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.appeaser.sublimepickerlibrary.SublimePicker
import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate
import com.appeaser.sublimepickerlibrary.helpers.SublimeListenerAdapter
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker
import java.util.*

class DatePickerDialog : DialogFragment() {

    private val initialDate by lazy { Date(arguments!!.getLong(INITIAL_DATE)) }
    private val listener = object : SublimeListenerAdapter() {
        override fun onDateTimeRecurrenceSet(
            sublimeMaterialPicker: SublimePicker?,
            selectedDate: SelectedDate?,
            hourOfDay: Int,
            minute: Int,
            recurrenceOption: SublimeRecurrencePicker.RecurrenceOption?,
            recurrenceRule: String?
        ) {
            if (selectedDate != null) {
                callback.onDateSelected(selectedDate.firstDate.time)
            }
            dismiss()
        }

        override fun onCancelled() {
            dismiss()
        }
    }
    lateinit var callback: DateSelectedListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val picker = SublimePicker(context)
        val initialCalendar = Calendar.getInstance().apply {
            time = initialDate
        }

        val options = SublimeOptions().apply {
            pickerToShow = SublimeOptions.Picker.DATE_PICKER
            setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER)
            setDateParams(initialCalendar)
        }

        picker.initializePicker(options, listener)

        return picker
    }

    companion object {
        private const val INITIAL_DATE = "initial_date"
        const val TAG = "DatePickerDialog"

        fun newInstance(initialDate: Date, callback: DateSelectedListener): DatePickerDialog {
            return DatePickerDialog().apply {
                arguments = Bundle().apply {
                    putLong(INITIAL_DATE, initialDate.time)
                }
                this.callback = callback
            }
        }
    }

    interface DateSelectedListener {
        fun onDateSelected(newDate: Date)
    }
}