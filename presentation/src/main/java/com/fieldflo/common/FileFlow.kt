package com.fieldflo.common

import android.os.FileObserver
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import java.io.File

object FileFlow {

    fun observeFile(filePath: String): Flow<File> {
        return callbackFlow {
            val fileToObserve = File(filePath)

            val observer = object : FileObserver(filePath, mask) {
                override fun onEvent(event: Int, path: String?) {
                    trySend(fileToObserve)
                }
            }

            trySend(fileToObserve)
            observer.startWatching()

            awaitClose {
                observer.stopWatching()
            }
        }

    }

    fun observeFile(fileToObserve: File): Flow<File> {
        return observeFile(fileToObserve.absolutePath)
    }

    private const val mask = FileObserver.CREATE or
            FileObserver.DELETE or
            FileObserver.MODIFY or
            FileObserver.MOVED_FROM or
            FileObserver.MOVED_TO
}
