package com.fieldflo.common

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

class Atomic<T>(private var value: T) : Mutex by Mutex() {

    suspend fun setValue(newValue: T) {
        this.withLock {
            value = newValue
        }
    }

    suspend fun getValue(): T {
        return this.withLock { value }
    }

    suspend fun getThenSet(newValue: T): T {
        return this.withLock {
            val prev = value
            value = newValue
            prev
        }
    }
}