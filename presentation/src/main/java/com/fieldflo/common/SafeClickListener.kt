package com.fieldflo.common

import android.os.SystemClock
import android.view.View

class SafeClickListener(
    private val defaultInterval: Int = 500,
    private val onSafeClick: (View) -> Unit
) : View.OnClickListener {
    private var lastTimeClicked: Long = 0
    override fun onClick(v: View) {
        if (SystemClock.elapsedRealtime() - lastTimeClicked < defaultInterval) {
            return
        }
        lastTimeClicked = SystemClock.elapsedRealtime()
        onSafeClick(v)
    }
}

fun View.safeClickListener(defaultInterval: Int = 500, onSafeClick: (View) -> Unit) {
    this.setOnClickListener(SafeClickListener(defaultInterval, onSafeClick))
}

fun View.safeClickListener(defaultInterval: Int = 500, clickListener: View.OnClickListener) {
    this.setOnClickListener(SafeClickListener(defaultInterval, { clickListener.onClick(it) }))
}