package com.fieldflo.common

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewAnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.appcompat.widget.SearchView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import timber.log.Timber
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

fun Context.toast(text: String, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, text, length).show()
}

fun Context.toast(@StringRes textRes: Int, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, textRes, length).show()
}

fun Fragment.toast(text: String, length: Int = Toast.LENGTH_SHORT) {
    this.activity?.toast(text, length)
}

fun Fragment.toast(@StringRes textRes: Int, length: Int = Toast.LENGTH_SHORT) {
    this.activity?.toast(textRes, length)
}

fun Activity.hideKeyboard() {
    val view = this.findViewById<View>(android.R.id.content)
    if (view != null) {
        val imm = ActivityCompat.getSystemService(this, InputMethodManager::class.java)
        imm?.hideSoftInputFromWindow(view.windowToken, 0) ?: Timber.e("InputMethodManager is null")
    }
}

fun View.hideKeyboard() {
    val imm = ActivityCompat.getSystemService(this.context, InputMethodManager::class.java)
    imm?.hideSoftInputFromWindow(this.windowToken, 0) ?: Timber.e("InputMethodManager is null")
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.hidden() {
    this.visibility = View.INVISIBLE
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.animateVisibility(duration: Long, visibleAlpha: Float) {
    val view = this

    val animator = if (view.visibility == View.VISIBLE) {
        ValueAnimator.ofFloat(visibleAlpha, 0f)
    } else {
        ValueAnimator.ofFloat(0f, visibleAlpha)
    }
    animator.addUpdateListener {
        val alpha = it.animatedValue as Float
        view.alpha = alpha
    }
    animator.duration = duration

    if (view.visibility == View.VISIBLE) {

        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                view.gone()
                animator.removeAllListeners()
                animator.removeAllUpdateListeners()
            }
        })
    } else {
        view.visible()
        view.alpha = 0f
        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                animator.removeAllListeners()
                animator.removeAllUpdateListeners()
            }
        })
    }

    animator.start()
}

fun Date.addDays(days: Int): Date {
    val cal = Calendar.getInstance()
    cal.timeInMillis = this.time
    cal.add(Calendar.DATE, days)
    return Date(cal.timeInMillis)
}

fun Date.roundToSecond(): Date {
    return Date((this.time / 1000) * 1000)
}

fun Date.toSimpleString(): String {
    if (this.time == 0L) return ""
    return SimpleDateFormat("MM/dd/yyyy", Locale.US).format(this)
}

fun String.toDate(): Date {
    return try {
        SimpleDateFormat("MM/dd/yyyy", Locale.US).parse(this) ?: Date(0)
    } catch (e: java.lang.Exception) {
        Date(0)
    }
}

fun Date.toDateString(): String {
    if (this.time == 0L) return ""
    return SimpleDateFormat("MM/dd/yyyy ", Locale.US).format(this)
}

fun Date.toFullString(): String {
    if (this.time == 0L) return ""
    return SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US).format(this)
}

fun Date.toOnlyTimeString(): String {
    if (this.time == 0L) return ""
    return SimpleDateFormat("hh:mm a", Locale.US).format(this)
}

fun Date.daysInTheFuture(): Int {
    val diff = this.time - Date().time

    val millisInSeconds = 1_000
    val secondsInMinute = 60
    val minutesInHour = 60
    val hoursInDay = 24

    val days = diff / (millisInSeconds * secondsInMinute * minutesInHour * hoursInDay)
    return (days + 0.5).roundToInt()
}

fun Int.toTimeString(): String {
    val hour = this / 3600
    val min = this / 60 % 60
    val sec = this / 1 % 60
    return String.format("%02d:%02d:%02d", hour, min, sec)
}

fun Date.toStringWithDay(): String {
    if (this.time == 0L) return ""
    return SimpleDateFormat("EEEE, MM/dd/yyyy", Locale.US).format(this)
}

fun Date.toDay(): String {
    val sdf = SimpleDateFormat("EEEE")
    return sdf.format(this)
}

fun Double.roundOffDecimal(): String? {
    val df = DecimalFormat("#.##")
    df.roundingMode = RoundingMode.FLOOR
    return df.format(this)
}

fun Double.round(places: Int): Double {
    if (places < 0) throw IllegalArgumentException("Cannot round to negative places")

    return BigDecimal(this.toString())
        .setScale(places, RoundingMode.HALF_UP)
        .toDouble()
}

fun Double.formatPlaces(places: Int): String {
    val formatString = "%.$places" + "f"
    return String.format(formatString, this)
}

fun Boolean.toGeneralString(): String {
    return if (this) "YES" else "NO"
}

fun View.circularReveal(show: Boolean) {
    val view = this
    this.post {
        val cx = view.width /// 2
        val cy = view.height /// 2
        val radius = Math.hypot(cx.toDouble(), cy.toDouble()).toFloat()

        try {
            if (show && view.visibility != View.VISIBLE) {
                val animator = ViewAnimationUtils.createCircularReveal(view, cx, -cy, 0f, radius)
                animator.duration = 500
                view.visible()
                animator.start()
            } else if (!show && view.visibility == View.VISIBLE) {
                val animator = ViewAnimationUtils.createCircularReveal(view, cx, -cy, radius, 0f)
                animator.duration = 400
                animator.addListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator?) {
                        view.gone()
                        animator.removeAllListeners()
                    }
                })
                animator.start()
            }

        } catch (e: Exception) {
            //Timber.e(e, "Unable to perform circular reveal")
        }
    }
}

fun TextView.drawableStart(@DrawableRes start: Int) {
    this.setCompoundDrawablesRelativeWithIntrinsicBounds(start, 0, 0, 0)
}

fun TextView.drawableTop(@DrawableRes top: Int) {
    this.setCompoundDrawablesRelativeWithIntrinsicBounds(0, top, 0, 0)
}

fun TextView.drawableEnd(@DrawableRes end: Int) {
    this.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, end, 0)
}

fun TextView.drawableBottom(@DrawableRes bottom: Int) {
    this.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, bottom)
}

fun TextView.textColor(@ColorRes color: Int) {
    this.setTextColor(ContextCompat.getColor(this.context, color))
}

fun TextView.colorStateList(@ColorRes colors: Int) {
    this.setTextColor(ContextCompat.getColorStateList(this.context, colors))
}

fun EditText.afterTextChanges(onAfterTextChanged: (String, EditText) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            onAfterTextChanged.invoke(s.toString(), this@afterTextChanges)
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    })
}

fun ViewPager.onPageChanges(onPageChange: (Int) -> Unit) {
    this.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {
        }

        override fun onPageSelected(position: Int) {
            onPageChange(position)
        }

        override fun onPageScrollStateChanged(state: Int) {}
    })
}

fun String.safeToInt(): Int {
    return try {
        this.toInt()
    } catch (e: NumberFormatException) {
        -1
    }
}

fun Context.hasAllPermissions(permissions: Array<String>): Boolean {
    permissions.forEach {
        if (!this.hasPermission(it)) return false
    }
    return true
}

fun Context.hasPermission(permission: String): Boolean {
    return ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
}

fun Activity.getPermissions(permissions: Array<String>, requestCode: Int) {
    ActivityCompat.requestPermissions(this, permissions, requestCode)
}

fun Activity.getPermission(permission: String, requestCode: Int) {
    ActivityCompat.requestPermissions(this, arrayOf(permission), requestCode)
}

fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {
    val safeClickListener = SafeClickListener {
        onSafeClick(it)
    }
    setOnClickListener(safeClickListener)
}

fun SearchView.onQueryTextChanged(onTextChanged: (String) -> Boolean) {
    this.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String) = false

        override fun onQueryTextChange(newText: String): Boolean {
            return onTextChanged(newText)
        }
    })
}
