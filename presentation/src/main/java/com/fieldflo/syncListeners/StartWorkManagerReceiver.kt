package com.fieldflo.syncListeners

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.fieldflo.di.injector
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class StartWorkManagerReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        context.injector.serverUpdatesUseCase().startAppToServerWorker()

    }
}
