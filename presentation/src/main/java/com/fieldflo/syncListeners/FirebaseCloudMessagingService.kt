package com.fieldflo.syncListeners

import com.fieldflo.di.injector
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import timber.log.Timber

@ExperimentalCoroutinesApi
class FirebaseCloudMessagingService : FirebaseMessagingService() {

    private val serverUpdatesUseCases by lazy {
        injector.serverUpdatesUseCase()
    }

    private val appScope by lazy {
        injector.appScopeHolder().scope
    }

    override fun onCreate() {
        super.onCreate()
        Timber.d("onCreate()")
        initFcm()
    }

    override fun onNewToken(newToken: String) {
        super.onNewToken(newToken)
        saveToken(newToken)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Timber.d("From: %s", remoteMessage.from)

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            val data = remoteMessage.data
            Timber.d("FCM Message data payload: %s", data.toString())
            appScope.launch { serverUpdatesUseCases.performServerSync() }
        }

        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {
            Timber.d("FCM Message Notification Body: %s", remoteMessage.notification!!.body)
        }
    }

    private fun initFcm() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
            if (!task.isSuccessful || task.result == null) {
                Timber.w(task.exception, "getInstanceId failed")
            } else {
                val newToken = task.result!!
                Timber.d("Initial FCM Token - %s", newToken)
                saveToken(newToken)
            }
        }
    }

    private fun saveToken(newToken: String) {
        serverUpdatesUseCases.saveNewFcmToken(newToken)
    }
}